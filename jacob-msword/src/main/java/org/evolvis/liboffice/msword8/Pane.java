/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Pane extends Dispatch {

	public static final String componentName = "Word.Pane";

	public Pane() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Pane(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Pane(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Document
	 */
	public Document getDocument() {
		return new Document(Dispatch.get(this, "Document").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Selection
	 */
	public Selection getSelection() {
		return new Selection(Dispatch.get(this, "Selection").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayRulers() {
		return Dispatch.get(this, "DisplayRulers").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayRulers(boolean lastParam) {
		Dispatch.call(this, "DisplayRulers", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayVerticalRuler() {
		return Dispatch.get(this, "DisplayVerticalRuler").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayVerticalRuler(boolean lastParam) {
		Dispatch.call(this, "DisplayVerticalRuler", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Zooms
	 */
	public Zooms getZooms() {
		return new Zooms(Dispatch.get(this, "Zooms").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getIndex() {
		return Dispatch.get(this, "Index").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type View
	 */
	public View getView() {
		return new View(Dispatch.get(this, "View").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Pane
	 */
	public Pane getNext() {
		return new Pane(Dispatch.get(this, "Next").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Pane
	 */
	public Pane getPrevious() {
		return new Pane(Dispatch.get(this, "Previous").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHorizontalPercentScrolled() {
		return Dispatch.get(this, "HorizontalPercentScrolled").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHorizontalPercentScrolled(int lastParam) {
		Dispatch.call(this, "HorizontalPercentScrolled", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getVerticalPercentScrolled() {
		return Dispatch.get(this, "VerticalPercentScrolled").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setVerticalPercentScrolled(int lastParam) {
		Dispatch.call(this, "VerticalPercentScrolled", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getMinimumFontSize() {
		return Dispatch.get(this, "MinimumFontSize").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setMinimumFontSize(int lastParam) {
		Dispatch.call(this, "MinimumFontSize", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getBrowseToWindow() {
		return Dispatch.get(this, "BrowseToWindow").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setBrowseToWindow(boolean lastParam) {
		Dispatch.call(this, "BrowseToWindow", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getBrowseWidth() {
		return Dispatch.get(this, "BrowseWidth").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void activate() {
		Dispatch.call(this, "Activate");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void close() {
		Dispatch.call(this, "Close");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void largeScroll(Variant down, Variant up, Variant toRight, Variant lastParam) {
		Dispatch.call(this, "LargeScroll", down, up, toRight, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 */
	public void largeScroll(Variant down, Variant up, Variant toRight) {
		Dispatch.call(this, "LargeScroll", down, up, toRight);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 */
	public void largeScroll(Variant down, Variant up) {
		Dispatch.call(this, "LargeScroll", down, up);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 */
	public void largeScroll(Variant down) {
		Dispatch.call(this, "LargeScroll", down);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void largeScroll() {
		Dispatch.call(this, "LargeScroll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void smallScroll(Variant down, Variant up, Variant toRight, Variant lastParam) {
		Dispatch.call(this, "SmallScroll", down, up, toRight, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 */
	public void smallScroll(Variant down, Variant up, Variant toRight) {
		Dispatch.call(this, "SmallScroll", down, up, toRight);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 */
	public void smallScroll(Variant down, Variant up) {
		Dispatch.call(this, "SmallScroll", down, up);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 */
	public void smallScroll(Variant down) {
		Dispatch.call(this, "SmallScroll", down);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void smallScroll() {
		Dispatch.call(this, "SmallScroll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void autoScroll(int lastParam) {
		Dispatch.call(this, "AutoScroll", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void pageScroll(Variant down, Variant lastParam) {
		Dispatch.call(this, "PageScroll", down, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 */
	public void pageScroll(Variant down) {
		Dispatch.call(this, "PageScroll", down);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void pageScroll() {
		Dispatch.call(this, "PageScroll");
	}

}
