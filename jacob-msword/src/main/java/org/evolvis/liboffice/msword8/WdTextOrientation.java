/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdTextOrientation {

	public static final int wdTextOrientationHorizontal = 0;
	public static final int wdTextOrientationUpward = 2;
	public static final int wdTextOrientationDownward = 3;
}
