/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdCustomLabelPageSize {

	public static final int wdCustomLabelLetter = 0;
	public static final int wdCustomLabelLetterLS = 1;
	public static final int wdCustomLabelA4 = 2;
	public static final int wdCustomLabelA4LS = 3;
	public static final int wdCustomLabelA5 = 4;
	public static final int wdCustomLabelA5LS = 5;
	public static final int wdCustomLabelB5 = 6;
	public static final int wdCustomLabelMini = 7;
	public static final int wdCustomLabelFanfold = 8;
}
