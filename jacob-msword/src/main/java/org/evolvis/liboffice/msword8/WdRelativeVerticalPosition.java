/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdRelativeVerticalPosition {

	public static final int wdRelativeVerticalPositionMargin = 0;
	public static final int wdRelativeVerticalPositionPage = 1;
	public static final int wdRelativeVerticalPositionParagraph = 2;
}
