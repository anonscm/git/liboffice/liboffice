/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdWordDialogHID {

	public static final int wdDialogToolsOptionsFuzzy = 790;
	public static final int wdDialogToolsOptionsTypography = 739;
	public static final int wdDialogToolsOptionsAutoFormatAsYouType = 778;
}
