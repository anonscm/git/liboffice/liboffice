/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdOLEPlacement {

	public static final int wdInLine = 0;
	public static final int wdFloatOverText = 1;
}
