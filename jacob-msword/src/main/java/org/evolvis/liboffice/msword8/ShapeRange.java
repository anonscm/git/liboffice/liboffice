/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class ShapeRange extends Dispatch {

	public static final String componentName = "Word.ShapeRange";

	public ShapeRange() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public ShapeRange(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public ShapeRange(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCount() {
		return Dispatch.get(this, "Count").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant get_NewEnum() {
		return Dispatch.get(this, "_NewEnum");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Adjustments
	 */
	public Adjustments getAdjustments() {
		return new Adjustments(Dispatch.get(this, "Adjustments").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoAutoShapeType
	 */
	/*
	public MsoAutoShapeType getAutoShapeType() {
		return new MsoAutoShapeType(Dispatch.get(this, "AutoShapeType").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoAutoShapeType
	 */
	/*
	public void setAutoShapeType(MsoAutoShapeType lastParam) {
		Dispatch.call(this, "AutoShapeType", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type CalloutFormat
	 */
	public CalloutFormat getCallout() {
		return new CalloutFormat(Dispatch.get(this, "Callout").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	/*
	public int getConnectionSiteCount() {
		return Dispatch.get(this, "ConnectionSiteCount").toInt();
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getConnector() {
		return new MsoTriState(Dispatch.get(this, "Connector").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ConnectorFormat
	 */
	public ConnectorFormat getConnectorFormat() {
		return new ConnectorFormat(Dispatch.get(this, "ConnectorFormat").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FillFormat
	 */
	public FillFormat getFill() {
		return new FillFormat(Dispatch.get(this, "Fill").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type GroupShapes
	 */
	public GroupShapes getGroupItems() {
		return new GroupShapes(Dispatch.get(this, "GroupItems").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getHeight() {
		return Dispatch.get(this, "Height").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setHeight(float lastParam) {
		Dispatch.call(this, "Height", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getHorizontalFlip() {
		return new MsoTriState(Dispatch.get(this, "HorizontalFlip").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getLeft() {
		return Dispatch.get(this, "Left").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setLeft(float lastParam) {
		Dispatch.call(this, "Left", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type LineFormat
	 */
	public LineFormat getLine() {
		return new LineFormat(Dispatch.get(this, "Line").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getLockAspectRatio() {
		return new MsoTriState(Dispatch.get(this, "LockAspectRatio").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setLockAspectRatio(MsoTriState lastParam) {
		Dispatch.call(this, "LockAspectRatio", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getName() {
		return Dispatch.get(this, "Name").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setName(String lastParam) {
		Dispatch.call(this, "Name", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ShapeNodes
	 */
	public ShapeNodes getNodes() {
		return new ShapeNodes(Dispatch.get(this, "Nodes").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getRotation() {
		return Dispatch.get(this, "Rotation").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setRotation(float lastParam) {
		Dispatch.call(this, "Rotation", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type PictureFormat
	 */
	public PictureFormat getPictureFormat() {
		return new PictureFormat(Dispatch.get(this, "PictureFormat").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ShadowFormat
	 */
	public ShadowFormat getShadow() {
		return new ShadowFormat(Dispatch.get(this, "Shadow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type TextEffectFormat
	 */
	public TextEffectFormat getTextEffect() {
		return new TextEffectFormat(Dispatch.get(this, "TextEffect").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type TextFrame
	 */
	public TextFrame getTextFrame() {
		return new TextFrame(Dispatch.get(this, "TextFrame").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ThreeDFormat
	 */
	public ThreeDFormat getThreeD() {
		return new ThreeDFormat(Dispatch.get(this, "ThreeD").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getTop() {
		return Dispatch.get(this, "Top").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setTop(float lastParam) {
		Dispatch.call(this, "Top", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoShapeType
	 */
	/*
	public MsoShapeType getType() {
		return new MsoShapeType(Dispatch.get(this, "Type").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getVerticalFlip() {
		return new MsoTriState(Dispatch.get(this, "VerticalFlip").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant getVertices() {
		return Dispatch.get(this, "Vertices");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getVisible() {
		return new MsoTriState(Dispatch.get(this, "Visible").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setVisible(MsoTriState lastParam) {
		Dispatch.call(this, "Visible", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getWidth() {
		return Dispatch.get(this, "Width").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setWidth(float lastParam) {
		Dispatch.call(this, "Width", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getZOrderPosition() {
		return Dispatch.get(this, "ZOrderPosition").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Hyperlink
	 */
	public Hyperlink getHyperlink() {
		return new Hyperlink(Dispatch.get(this, "Hyperlink").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getRelativeHorizontalPosition() {
		return Dispatch.get(this, "RelativeHorizontalPosition").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setRelativeHorizontalPosition(int lastParam) {
		Dispatch.call(this, "RelativeHorizontalPosition", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getRelativeVerticalPosition() {
		return Dispatch.get(this, "RelativeVerticalPosition").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setRelativeVerticalPosition(int lastParam) {
		Dispatch.call(this, "RelativeVerticalPosition", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLockAnchor() {
		return Dispatch.get(this, "LockAnchor").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLockAnchor(int lastParam) {
		Dispatch.call(this, "LockAnchor", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type WrapFormat
	 */
	public WrapFormat getWrapFormat() {
		return new WrapFormat(Dispatch.get(this, "WrapFormat").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range getAnchor() {
		return new Range(Dispatch.get(this, "Anchor").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Shape
	 */
	public Shape item(Variant lastParam) {
		return new Shape(Dispatch.call(this, "Item", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param align an input-parameter of type MsoAlignCmd
	 * @param lastParam an input-parameter of type int
	 */
	/*
	public void align(MsoAlignCmd align, int lastParam) {
		Dispatch.call(this, "Align", align, new Variant(lastParam));
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void apply() {
		Dispatch.call(this, "Apply");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void delete() {
		Dispatch.call(this, "Delete");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param distribute an input-parameter of type MsoDistributeCmd
	 * @param lastParam an input-parameter of type int
	 */
	/*
	public void distribute(MsoDistributeCmd distribute, int lastParam) {
		Dispatch.call(this, "Distribute", distribute, new Variant(lastParam));
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ShapeRange
	 */
	public ShapeRange duplicate() {
		return new ShapeRange(Dispatch.call(this, "Duplicate").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoFlipCmd
	 */
	/*
	public void flip(MsoFlipCmd lastParam) {
		Dispatch.call(this, "Flip", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void incrementLeft(float lastParam) {
		Dispatch.call(this, "IncrementLeft", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void incrementRotation(float lastParam) {
		Dispatch.call(this, "IncrementRotation", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void incrementTop(float lastParam) {
		Dispatch.call(this, "IncrementTop", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Shape
	 */
	public Shape group() {
		return new Shape(Dispatch.call(this, "Group").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void pickUp() {
		Dispatch.call(this, "PickUp");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Shape
	 */
	public Shape regroup() {
		return new Shape(Dispatch.call(this, "Regroup").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void rerouteConnections() {
		Dispatch.call(this, "RerouteConnections");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param factor an input-parameter of type float
	 * @param relativeToOriginalSize an input-parameter of type MsoTriState
	 * @param lastParam an input-parameter of type MsoScaleFrom
	 */
	/*
	public void scaleHeight(float factor, MsoTriState relativeToOriginalSize, MsoScaleFrom lastParam) {
		Dispatch.call(this, "ScaleHeight", new Variant(factor), relativeToOriginalSize, lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param factor an input-parameter of type float
	 * @param relativeToOriginalSize an input-parameter of type MsoTriState
	 */
	/*
	public void scaleHeight(float factor, MsoTriState relativeToOriginalSize) {
		Dispatch.call(this, "ScaleHeight", new Variant(factor), relativeToOriginalSize);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param factor an input-parameter of type float
	 * @param relativeToOriginalSize an input-parameter of type MsoTriState
	 * @param lastParam an input-parameter of type MsoScaleFrom
	 */
	/*
	public void scaleWidth(float factor, MsoTriState relativeToOriginalSize, MsoScaleFrom lastParam) {
		Dispatch.call(this, "ScaleWidth", new Variant(factor), relativeToOriginalSize, lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param factor an input-parameter of type float
	 * @param relativeToOriginalSize an input-parameter of type MsoTriState
	 */
	/*
	public void scaleWidth(float factor, MsoTriState relativeToOriginalSize) {
		Dispatch.call(this, "ScaleWidth", new Variant(factor), relativeToOriginalSize);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void select(Variant lastParam) {
		Dispatch.call(this, "Select", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void select() {
		Dispatch.call(this, "Select");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void setShapesDefaultProperties() {
		Dispatch.call(this, "SetShapesDefaultProperties");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ShapeRange
	 */
	public ShapeRange ungroup() {
		return new ShapeRange(Dispatch.call(this, "Ungroup").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoZOrderCmd
	 */
	/*
	public void zOrder(MsoZOrderCmd lastParam) {
		Dispatch.call(this, "ZOrder", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Frame
	 */
	public Frame convertToFrame() {
		return new Frame(Dispatch.call(this, "ConvertToFrame").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type InlineShape
	 */
	public InlineShape convertToInlineShape() {
		return new InlineShape(Dispatch.call(this, "ConvertToInlineShape").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void activate() {
		Dispatch.call(this, "Activate");
	}

}
