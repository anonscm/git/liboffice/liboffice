/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class LetterContent extends _LetterContent {

	public static final String componentName = "Word.LetterContent";

	public LetterContent() {
		super(componentName);
	}

	public LetterContent(Dispatch d) {
		super(d);
	}
}
