/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdHeaderFooterIndex {

	public static final int wdHeaderFooterPrimary = 1;
	public static final int wdHeaderFooterFirstPage = 2;
	public static final int wdHeaderFooterEvenPages = 3;
}
