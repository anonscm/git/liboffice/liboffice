/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdWindowState {

	public static final int wdWindowStateNormal = 0;
	public static final int wdWindowStateMaximize = 1;
	public static final int wdWindowStateMinimize = 2;
}
