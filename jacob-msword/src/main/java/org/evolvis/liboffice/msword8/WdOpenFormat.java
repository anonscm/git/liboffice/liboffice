/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdOpenFormat {

	public static final int wdOpenFormatAuto = 0;
	public static final int wdOpenFormatDocument = 1;
	public static final int wdOpenFormatTemplate = 2;
	public static final int wdOpenFormatRTF = 3;
	public static final int wdOpenFormatText = 4;
	public static final int wdOpenFormatUnicodeText = 5;
}
