/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Options extends Dispatch {

	public static final String componentName = "Word.Options";

	public Options() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Options(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Options(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAllowAccentedUppercase() {
		return Dispatch.get(this, "AllowAccentedUppercase").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAllowAccentedUppercase(boolean lastParam) {
		Dispatch.call(this, "AllowAccentedUppercase", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getWPHelp() {
		return Dispatch.get(this, "WPHelp").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setWPHelp(boolean lastParam) {
		Dispatch.call(this, "WPHelp", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getWPDocNavKeys() {
		return Dispatch.get(this, "WPDocNavKeys").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setWPDocNavKeys(boolean lastParam) {
		Dispatch.call(this, "WPDocNavKeys", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPagination() {
		return Dispatch.get(this, "Pagination").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPagination(boolean lastParam) {
		Dispatch.call(this, "Pagination", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getBlueScreen() {
		return Dispatch.get(this, "BlueScreen").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setBlueScreen(boolean lastParam) {
		Dispatch.call(this, "BlueScreen", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getEnableSound() {
		return Dispatch.get(this, "EnableSound").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setEnableSound(boolean lastParam) {
		Dispatch.call(this, "EnableSound", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getConfirmConversions() {
		return Dispatch.get(this, "ConfirmConversions").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setConfirmConversions(boolean lastParam) {
		Dispatch.call(this, "ConfirmConversions", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getUpdateLinksAtOpen() {
		return Dispatch.get(this, "UpdateLinksAtOpen").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setUpdateLinksAtOpen(boolean lastParam) {
		Dispatch.call(this, "UpdateLinksAtOpen", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSendMailAttach() {
		return Dispatch.get(this, "SendMailAttach").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSendMailAttach(boolean lastParam) {
		Dispatch.call(this, "SendMailAttach", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getMeasurementUnit() {
		return Dispatch.get(this, "MeasurementUnit").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setMeasurementUnit(int lastParam) {
		Dispatch.call(this, "MeasurementUnit", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getButtonFieldClicks() {
		return Dispatch.get(this, "ButtonFieldClicks").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setButtonFieldClicks(int lastParam) {
		Dispatch.call(this, "ButtonFieldClicks", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getShortMenuNames() {
		return Dispatch.get(this, "ShortMenuNames").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setShortMenuNames(boolean lastParam) {
		Dispatch.call(this, "ShortMenuNames", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getRTFInClipboard() {
		return Dispatch.get(this, "RTFInClipboard").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setRTFInClipboard(boolean lastParam) {
		Dispatch.call(this, "RTFInClipboard", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getUpdateFieldsAtPrint() {
		return Dispatch.get(this, "UpdateFieldsAtPrint").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setUpdateFieldsAtPrint(boolean lastParam) {
		Dispatch.call(this, "UpdateFieldsAtPrint", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintProperties() {
		return Dispatch.get(this, "PrintProperties").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintProperties(boolean lastParam) {
		Dispatch.call(this, "PrintProperties", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintFieldCodes() {
		return Dispatch.get(this, "PrintFieldCodes").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintFieldCodes(boolean lastParam) {
		Dispatch.call(this, "PrintFieldCodes", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintComments() {
		return Dispatch.get(this, "PrintComments").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintComments(boolean lastParam) {
		Dispatch.call(this, "PrintComments", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintHiddenText() {
		return Dispatch.get(this, "PrintHiddenText").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintHiddenText(boolean lastParam) {
		Dispatch.call(this, "PrintHiddenText", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getEnvelopeFeederInstalled() {
		return Dispatch.get(this, "EnvelopeFeederInstalled").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getUpdateLinksAtPrint() {
		return Dispatch.get(this, "UpdateLinksAtPrint").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setUpdateLinksAtPrint(boolean lastParam) {
		Dispatch.call(this, "UpdateLinksAtPrint", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintBackground() {
		return Dispatch.get(this, "PrintBackground").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintBackground(boolean lastParam) {
		Dispatch.call(this, "PrintBackground", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintDrawingObjects() {
		return Dispatch.get(this, "PrintDrawingObjects").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintDrawingObjects(boolean lastParam) {
		Dispatch.call(this, "PrintDrawingObjects", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getDefaultTray() {
		return Dispatch.get(this, "DefaultTray").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setDefaultTray(String lastParam) {
		Dispatch.call(this, "DefaultTray", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDefaultTrayID() {
		return Dispatch.get(this, "DefaultTrayID").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDefaultTrayID(int lastParam) {
		Dispatch.call(this, "DefaultTrayID", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCreateBackup() {
		return Dispatch.get(this, "CreateBackup").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCreateBackup(boolean lastParam) {
		Dispatch.call(this, "CreateBackup", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAllowFastSave() {
		return Dispatch.get(this, "AllowFastSave").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAllowFastSave(boolean lastParam) {
		Dispatch.call(this, "AllowFastSave", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSavePropertiesPrompt() {
		return Dispatch.get(this, "SavePropertiesPrompt").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSavePropertiesPrompt(boolean lastParam) {
		Dispatch.call(this, "SavePropertiesPrompt", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSaveNormalPrompt() {
		return Dispatch.get(this, "SaveNormalPrompt").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSaveNormalPrompt(boolean lastParam) {
		Dispatch.call(this, "SaveNormalPrompt", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getSaveInterval() {
		return Dispatch.get(this, "SaveInterval").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setSaveInterval(int lastParam) {
		Dispatch.call(this, "SaveInterval", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getBackgroundSave() {
		return Dispatch.get(this, "BackgroundSave").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setBackgroundSave(boolean lastParam) {
		Dispatch.call(this, "BackgroundSave", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getInsertedTextMark() {
		return Dispatch.get(this, "InsertedTextMark").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setInsertedTextMark(int lastParam) {
		Dispatch.call(this, "InsertedTextMark", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDeletedTextMark() {
		return Dispatch.get(this, "DeletedTextMark").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDeletedTextMark(int lastParam) {
		Dispatch.call(this, "DeletedTextMark", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getRevisedLinesMark() {
		return Dispatch.get(this, "RevisedLinesMark").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setRevisedLinesMark(int lastParam) {
		Dispatch.call(this, "RevisedLinesMark", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getInsertedTextColor() {
		return Dispatch.get(this, "InsertedTextColor").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setInsertedTextColor(int lastParam) {
		Dispatch.call(this, "InsertedTextColor", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDeletedTextColor() {
		return Dispatch.get(this, "DeletedTextColor").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDeletedTextColor(int lastParam) {
		Dispatch.call(this, "DeletedTextColor", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getRevisedLinesColor() {
		return Dispatch.get(this, "RevisedLinesColor").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setRevisedLinesColor(int lastParam) {
		Dispatch.call(this, "RevisedLinesColor", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type String
	 */
	public String getDefaultFilePath(int lastParam) {
		return Dispatch.call(this, "DefaultFilePath", new Variant(lastParam)).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param path an input-parameter of type int
	 * @param lastParam an input-parameter of type String
	 */
	public void setDefaultFilePath(int path, String lastParam) {
		Dispatch.call(this, "DefaultFilePath", new Variant(path), lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getOvertype() {
		return Dispatch.get(this, "Overtype").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setOvertype(boolean lastParam) {
		Dispatch.call(this, "Overtype", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getReplaceSelection() {
		return Dispatch.get(this, "ReplaceSelection").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setReplaceSelection(boolean lastParam) {
		Dispatch.call(this, "ReplaceSelection", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAllowDragAndDrop() {
		return Dispatch.get(this, "AllowDragAndDrop").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAllowDragAndDrop(boolean lastParam) {
		Dispatch.call(this, "AllowDragAndDrop", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoWordSelection() {
		return Dispatch.get(this, "AutoWordSelection").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoWordSelection(boolean lastParam) {
		Dispatch.call(this, "AutoWordSelection", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getINSKeyForPaste() {
		return Dispatch.get(this, "INSKeyForPaste").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setINSKeyForPaste(boolean lastParam) {
		Dispatch.call(this, "INSKeyForPaste", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSmartCutPaste() {
		return Dispatch.get(this, "SmartCutPaste").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSmartCutPaste(boolean lastParam) {
		Dispatch.call(this, "SmartCutPaste", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getTabIndentKey() {
		return Dispatch.get(this, "TabIndentKey").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setTabIndentKey(boolean lastParam) {
		Dispatch.call(this, "TabIndentKey", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getPictureEditor() {
		return Dispatch.get(this, "PictureEditor").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setPictureEditor(String lastParam) {
		Dispatch.call(this, "PictureEditor", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAnimateScreenMovements() {
		return Dispatch.get(this, "AnimateScreenMovements").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAnimateScreenMovements(boolean lastParam) {
		Dispatch.call(this, "AnimateScreenMovements", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getVirusProtection() {
		return Dispatch.get(this, "VirusProtection").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setVirusProtection(boolean lastParam) {
		Dispatch.call(this, "VirusProtection", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getRevisedPropertiesMark() {
		return Dispatch.get(this, "RevisedPropertiesMark").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setRevisedPropertiesMark(int lastParam) {
		Dispatch.call(this, "RevisedPropertiesMark", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getRevisedPropertiesColor() {
		return Dispatch.get(this, "RevisedPropertiesColor").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setRevisedPropertiesColor(int lastParam) {
		Dispatch.call(this, "RevisedPropertiesColor", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSnapToGrid() {
		return Dispatch.get(this, "SnapToGrid").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSnapToGrid(boolean lastParam) {
		Dispatch.call(this, "SnapToGrid", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSnapToShapes() {
		return Dispatch.get(this, "SnapToShapes").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSnapToShapes(boolean lastParam) {
		Dispatch.call(this, "SnapToShapes", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getGridDistanceHorizontal() {
		return Dispatch.get(this, "GridDistanceHorizontal").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setGridDistanceHorizontal(float lastParam) {
		Dispatch.call(this, "GridDistanceHorizontal", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getGridDistanceVertical() {
		return Dispatch.get(this, "GridDistanceVertical").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setGridDistanceVertical(float lastParam) {
		Dispatch.call(this, "GridDistanceVertical", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getGridOriginHorizontal() {
		return Dispatch.get(this, "GridOriginHorizontal").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setGridOriginHorizontal(float lastParam) {
		Dispatch.call(this, "GridOriginHorizontal", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getGridOriginVertical() {
		return Dispatch.get(this, "GridOriginVertical").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setGridOriginVertical(float lastParam) {
		Dispatch.call(this, "GridOriginVertical", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getInlineConversion() {
		return Dispatch.get(this, "InlineConversion").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setInlineConversion(boolean lastParam) {
		Dispatch.call(this, "InlineConversion", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getIMEAutomaticControl() {
		return Dispatch.get(this, "IMEAutomaticControl").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setIMEAutomaticControl(boolean lastParam) {
		Dispatch.call(this, "IMEAutomaticControl", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatApplyHeadings() {
		return Dispatch.get(this, "AutoFormatApplyHeadings").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatApplyHeadings(boolean lastParam) {
		Dispatch.call(this, "AutoFormatApplyHeadings", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatApplyLists() {
		return Dispatch.get(this, "AutoFormatApplyLists").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatApplyLists(boolean lastParam) {
		Dispatch.call(this, "AutoFormatApplyLists", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatApplyBulletedLists() {
		return Dispatch.get(this, "AutoFormatApplyBulletedLists").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatApplyBulletedLists(boolean lastParam) {
		Dispatch.call(this, "AutoFormatApplyBulletedLists", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatApplyOtherParas() {
		return Dispatch.get(this, "AutoFormatApplyOtherParas").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatApplyOtherParas(boolean lastParam) {
		Dispatch.call(this, "AutoFormatApplyOtherParas", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatReplaceQuotes() {
		return Dispatch.get(this, "AutoFormatReplaceQuotes").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatReplaceQuotes(boolean lastParam) {
		Dispatch.call(this, "AutoFormatReplaceQuotes", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatReplaceSymbols() {
		return Dispatch.get(this, "AutoFormatReplaceSymbols").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatReplaceSymbols(boolean lastParam) {
		Dispatch.call(this, "AutoFormatReplaceSymbols", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatReplaceOrdinals() {
		return Dispatch.get(this, "AutoFormatReplaceOrdinals").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatReplaceOrdinals(boolean lastParam) {
		Dispatch.call(this, "AutoFormatReplaceOrdinals", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatReplaceFractions() {
		return Dispatch.get(this, "AutoFormatReplaceFractions").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatReplaceFractions(boolean lastParam) {
		Dispatch.call(this, "AutoFormatReplaceFractions", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatReplacePlainTextEmphasis() {
		return Dispatch.get(this, "AutoFormatReplacePlainTextEmphasis").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatReplacePlainTextEmphasis(boolean lastParam) {
		Dispatch.call(this, "AutoFormatReplacePlainTextEmphasis", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatPreserveStyles() {
		return Dispatch.get(this, "AutoFormatPreserveStyles").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatPreserveStyles(boolean lastParam) {
		Dispatch.call(this, "AutoFormatPreserveStyles", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyHeadings() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyHeadings").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyHeadings(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyHeadings", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyBorders() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyBorders").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyBorders(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyBorders", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyBulletedLists() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyBulletedLists").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyBulletedLists(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyBulletedLists", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyNumberedLists() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyNumberedLists").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyNumberedLists(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyNumberedLists", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeReplaceQuotes() {
		return Dispatch.get(this, "AutoFormatAsYouTypeReplaceQuotes").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeReplaceQuotes(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeReplaceQuotes", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeReplaceSymbols() {
		return Dispatch.get(this, "AutoFormatAsYouTypeReplaceSymbols").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeReplaceSymbols(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeReplaceSymbols", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeReplaceOrdinals() {
		return Dispatch.get(this, "AutoFormatAsYouTypeReplaceOrdinals").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeReplaceOrdinals(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeReplaceOrdinals", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeReplaceFractions() {
		return Dispatch.get(this, "AutoFormatAsYouTypeReplaceFractions").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeReplaceFractions(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeReplaceFractions", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeReplacePlainTextEmphasis() {
		return Dispatch.get(this, "AutoFormatAsYouTypeReplacePlainTextEmphasis").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeReplacePlainTextEmphasis(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeReplacePlainTextEmphasis", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeFormatListItemBeginning() {
		return Dispatch.get(this, "AutoFormatAsYouTypeFormatListItemBeginning").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeFormatListItemBeginning(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeFormatListItemBeginning", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeDefineStyles() {
		return Dispatch.get(this, "AutoFormatAsYouTypeDefineStyles").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeDefineStyles(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeDefineStyles", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatPlainTextWordMail() {
		return Dispatch.get(this, "AutoFormatPlainTextWordMail").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatPlainTextWordMail(boolean lastParam) {
		Dispatch.call(this, "AutoFormatPlainTextWordMail", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeReplaceHyperlinks() {
		return Dispatch.get(this, "AutoFormatAsYouTypeReplaceHyperlinks").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeReplaceHyperlinks(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeReplaceHyperlinks", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatReplaceHyperlinks() {
		return Dispatch.get(this, "AutoFormatReplaceHyperlinks").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatReplaceHyperlinks(boolean lastParam) {
		Dispatch.call(this, "AutoFormatReplaceHyperlinks", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDefaultHighlightColorIndex() {
		return Dispatch.get(this, "DefaultHighlightColorIndex").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDefaultHighlightColorIndex(int lastParam) {
		Dispatch.call(this, "DefaultHighlightColorIndex", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDefaultBorderLineStyle() {
		return Dispatch.get(this, "DefaultBorderLineStyle").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDefaultBorderLineStyle(int lastParam) {
		Dispatch.call(this, "DefaultBorderLineStyle", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCheckSpellingAsYouType() {
		return Dispatch.get(this, "CheckSpellingAsYouType").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCheckSpellingAsYouType(boolean lastParam) {
		Dispatch.call(this, "CheckSpellingAsYouType", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCheckGrammarAsYouType() {
		return Dispatch.get(this, "CheckGrammarAsYouType").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCheckGrammarAsYouType(boolean lastParam) {
		Dispatch.call(this, "CheckGrammarAsYouType", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getIgnoreInternetAndFileAddresses() {
		return Dispatch.get(this, "IgnoreInternetAndFileAddresses").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setIgnoreInternetAndFileAddresses(boolean lastParam) {
		Dispatch.call(this, "IgnoreInternetAndFileAddresses", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getShowReadabilityStatistics() {
		return Dispatch.get(this, "ShowReadabilityStatistics").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setShowReadabilityStatistics(boolean lastParam) {
		Dispatch.call(this, "ShowReadabilityStatistics", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getIgnoreUppercase() {
		return Dispatch.get(this, "IgnoreUppercase").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setIgnoreUppercase(boolean lastParam) {
		Dispatch.call(this, "IgnoreUppercase", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getIgnoreMixedDigits() {
		return Dispatch.get(this, "IgnoreMixedDigits").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setIgnoreMixedDigits(boolean lastParam) {
		Dispatch.call(this, "IgnoreMixedDigits", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSuggestFromMainDictionaryOnly() {
		return Dispatch.get(this, "SuggestFromMainDictionaryOnly").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSuggestFromMainDictionaryOnly(boolean lastParam) {
		Dispatch.call(this, "SuggestFromMainDictionaryOnly", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSuggestSpellingCorrections() {
		return Dispatch.get(this, "SuggestSpellingCorrections").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSuggestSpellingCorrections(boolean lastParam) {
		Dispatch.call(this, "SuggestSpellingCorrections", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDefaultBorderLineWidth() {
		return Dispatch.get(this, "DefaultBorderLineWidth").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDefaultBorderLineWidth(int lastParam) {
		Dispatch.call(this, "DefaultBorderLineWidth", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCheckGrammarWithSpelling() {
		return Dispatch.get(this, "CheckGrammarWithSpelling").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCheckGrammarWithSpelling(boolean lastParam) {
		Dispatch.call(this, "CheckGrammarWithSpelling", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDefaultOpenFormat() {
		return Dispatch.get(this, "DefaultOpenFormat").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDefaultOpenFormat(int lastParam) {
		Dispatch.call(this, "DefaultOpenFormat", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintDraft() {
		return Dispatch.get(this, "PrintDraft").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintDraft(boolean lastParam) {
		Dispatch.call(this, "PrintDraft", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintReverse() {
		return Dispatch.get(this, "PrintReverse").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintReverse(boolean lastParam) {
		Dispatch.call(this, "PrintReverse", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMapPaperSize() {
		return Dispatch.get(this, "MapPaperSize").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMapPaperSize(boolean lastParam) {
		Dispatch.call(this, "MapPaperSize", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyTables() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyTables").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyTables(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyTables", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatApplyFirstIndents() {
		return Dispatch.get(this, "AutoFormatApplyFirstIndents").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatApplyFirstIndents(boolean lastParam) {
		Dispatch.call(this, "AutoFormatApplyFirstIndents", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatMatchParentheses() {
		return Dispatch.get(this, "AutoFormatMatchParentheses").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatMatchParentheses(boolean lastParam) {
		Dispatch.call(this, "AutoFormatMatchParentheses", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatReplaceFarEastDashes() {
		return Dispatch.get(this, "AutoFormatReplaceFarEastDashes").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatReplaceFarEastDashes(boolean lastParam) {
		Dispatch.call(this, "AutoFormatReplaceFarEastDashes", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatDeleteAutoSpaces() {
		return Dispatch.get(this, "AutoFormatDeleteAutoSpaces").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatDeleteAutoSpaces(boolean lastParam) {
		Dispatch.call(this, "AutoFormatDeleteAutoSpaces", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyFirstIndents() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyFirstIndents").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyFirstIndents(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyFirstIndents", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyDates() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyDates").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyDates(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyDates", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeApplyClosings() {
		return Dispatch.get(this, "AutoFormatAsYouTypeApplyClosings").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeApplyClosings(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeApplyClosings", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeMatchParentheses() {
		return Dispatch.get(this, "AutoFormatAsYouTypeMatchParentheses").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeMatchParentheses(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeMatchParentheses", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeReplaceFarEastDashes() {
		return Dispatch.get(this, "AutoFormatAsYouTypeReplaceFarEastDashes").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeReplaceFarEastDashes(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeReplaceFarEastDashes", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeDeleteAutoSpaces() {
		return Dispatch.get(this, "AutoFormatAsYouTypeDeleteAutoSpaces").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeDeleteAutoSpaces(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeDeleteAutoSpaces", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeInsertClosings() {
		return Dispatch.get(this, "AutoFormatAsYouTypeInsertClosings").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeInsertClosings(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeInsertClosings", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeAutoLetterWizard() {
		return Dispatch.get(this, "AutoFormatAsYouTypeAutoLetterWizard").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeAutoLetterWizard(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeAutoLetterWizard", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAutoFormatAsYouTypeInsertOvers() {
		return Dispatch.get(this, "AutoFormatAsYouTypeInsertOvers").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAutoFormatAsYouTypeInsertOvers(boolean lastParam) {
		Dispatch.call(this, "AutoFormatAsYouTypeInsertOvers", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayGridLines() {
		return Dispatch.get(this, "DisplayGridLines").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayGridLines(boolean lastParam) {
		Dispatch.call(this, "DisplayGridLines", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyCase() {
		return Dispatch.get(this, "MatchFuzzyCase").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyCase(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyCase", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyByte() {
		return Dispatch.get(this, "MatchFuzzyByte").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyByte(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyByte", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyHiragana() {
		return Dispatch.get(this, "MatchFuzzyHiragana").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyHiragana(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyHiragana", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzySmallKana() {
		return Dispatch.get(this, "MatchFuzzySmallKana").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzySmallKana(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzySmallKana", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyDash() {
		return Dispatch.get(this, "MatchFuzzyDash").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyDash(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyDash", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyIterationMark() {
		return Dispatch.get(this, "MatchFuzzyIterationMark").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyIterationMark(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyIterationMark", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyKanji() {
		return Dispatch.get(this, "MatchFuzzyKanji").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyKanji(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyKanji", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyOldKana() {
		return Dispatch.get(this, "MatchFuzzyOldKana").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyOldKana(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyOldKana", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyProlongedSoundMark() {
		return Dispatch.get(this, "MatchFuzzyProlongedSoundMark").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyProlongedSoundMark(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyProlongedSoundMark", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyDZ() {
		return Dispatch.get(this, "MatchFuzzyDZ").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyDZ(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyDZ", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyBV() {
		return Dispatch.get(this, "MatchFuzzyBV").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyBV(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyBV", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyTC() {
		return Dispatch.get(this, "MatchFuzzyTC").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyTC(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyTC", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyHF() {
		return Dispatch.get(this, "MatchFuzzyHF").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyHF(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyHF", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyZJ() {
		return Dispatch.get(this, "MatchFuzzyZJ").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyZJ(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyZJ", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyAY() {
		return Dispatch.get(this, "MatchFuzzyAY").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyAY(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyAY", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyKiKu() {
		return Dispatch.get(this, "MatchFuzzyKiKu").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyKiKu(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyKiKu", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzyPunctuation() {
		return Dispatch.get(this, "MatchFuzzyPunctuation").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzyPunctuation(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzyPunctuation", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzySpace() {
		return Dispatch.get(this, "MatchFuzzySpace").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzySpace(boolean lastParam) {
		Dispatch.call(this, "MatchFuzzySpace", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getApplyFarEastFontsToAscii() {
		return Dispatch.get(this, "ApplyFarEastFontsToAscii").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setApplyFarEastFontsToAscii(boolean lastParam) {
		Dispatch.call(this, "ApplyFarEastFontsToAscii", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getConvertHighAnsiToFarEast() {
		return Dispatch.get(this, "ConvertHighAnsiToFarEast").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setConvertHighAnsiToFarEast(boolean lastParam) {
		Dispatch.call(this, "ConvertHighAnsiToFarEast", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintOddPagesInAscendingOrder() {
		return Dispatch.get(this, "PrintOddPagesInAscendingOrder").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintOddPagesInAscendingOrder(boolean lastParam) {
		Dispatch.call(this, "PrintOddPagesInAscendingOrder", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintEvenPagesInAscendingOrder() {
		return Dispatch.get(this, "PrintEvenPagesInAscendingOrder").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintEvenPagesInAscendingOrder(boolean lastParam) {
		Dispatch.call(this, "PrintEvenPagesInAscendingOrder", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDefaultBorderColorIndex() {
		return Dispatch.get(this, "DefaultBorderColorIndex").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDefaultBorderColorIndex(int lastParam) {
		Dispatch.call(this, "DefaultBorderColorIndex", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getEnableMisusedWordsDictionary() {
		return Dispatch.get(this, "EnableMisusedWordsDictionary").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setEnableMisusedWordsDictionary(boolean lastParam) {
		Dispatch.call(this, "EnableMisusedWordsDictionary", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getAllowCombinedAuxiliaryForms() {
		return Dispatch.get(this, "AllowCombinedAuxiliaryForms").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setAllowCombinedAuxiliaryForms(boolean lastParam) {
		Dispatch.call(this, "AllowCombinedAuxiliaryForms", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getHangulHanjaFastConversion() {
		return Dispatch.get(this, "HangulHanjaFastConversion").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setHangulHanjaFastConversion(boolean lastParam) {
		Dispatch.call(this, "HangulHanjaFastConversion", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCheckHangulEndings() {
		return Dispatch.get(this, "CheckHangulEndings").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCheckHangulEndings(boolean lastParam) {
		Dispatch.call(this, "CheckHangulEndings", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getEnableHangulHanjaRecentOrdering() {
		return Dispatch.get(this, "EnableHangulHanjaRecentOrdering").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setEnableHangulHanjaRecentOrdering(boolean lastParam) {
		Dispatch.call(this, "EnableHangulHanjaRecentOrdering", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getMultipleWordConversionsMode() {
		return Dispatch.get(this, "MultipleWordConversionsMode").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setMultipleWordConversionsMode(int lastParam) {
		Dispatch.call(this, "MultipleWordConversionsMode", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param commandKeyHelp an input-parameter of type Variant
	 * @param docNavigationKeys an input-parameter of type Variant
	 * @param mouseSimulation an input-parameter of type Variant
	 * @param demoGuidance an input-parameter of type Variant
	 * @param demoSpeed an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void setWPHelpOptions(Variant commandKeyHelp, Variant docNavigationKeys, Variant mouseSimulation, Variant demoGuidance, Variant demoSpeed, Variant lastParam) {
		Dispatch.call(this, "SetWPHelpOptions", commandKeyHelp, docNavigationKeys, mouseSimulation, demoGuidance, demoSpeed, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param commandKeyHelp an input-parameter of type Variant
	 * @param docNavigationKeys an input-parameter of type Variant
	 * @param mouseSimulation an input-parameter of type Variant
	 * @param demoGuidance an input-parameter of type Variant
	 * @param demoSpeed an input-parameter of type Variant
	 */
	public void setWPHelpOptions(Variant commandKeyHelp, Variant docNavigationKeys, Variant mouseSimulation, Variant demoGuidance, Variant demoSpeed) {
		Dispatch.call(this, "SetWPHelpOptions", commandKeyHelp, docNavigationKeys, mouseSimulation, demoGuidance, demoSpeed);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param commandKeyHelp an input-parameter of type Variant
	 * @param docNavigationKeys an input-parameter of type Variant
	 * @param mouseSimulation an input-parameter of type Variant
	 * @param demoGuidance an input-parameter of type Variant
	 */
	public void setWPHelpOptions(Variant commandKeyHelp, Variant docNavigationKeys, Variant mouseSimulation, Variant demoGuidance) {
		Dispatch.call(this, "SetWPHelpOptions", commandKeyHelp, docNavigationKeys, mouseSimulation, demoGuidance);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param commandKeyHelp an input-parameter of type Variant
	 * @param docNavigationKeys an input-parameter of type Variant
	 * @param mouseSimulation an input-parameter of type Variant
	 */
	public void setWPHelpOptions(Variant commandKeyHelp, Variant docNavigationKeys, Variant mouseSimulation) {
		Dispatch.call(this, "SetWPHelpOptions", commandKeyHelp, docNavigationKeys, mouseSimulation);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param commandKeyHelp an input-parameter of type Variant
	 * @param docNavigationKeys an input-parameter of type Variant
	 */
	public void setWPHelpOptions(Variant commandKeyHelp, Variant docNavigationKeys) {
		Dispatch.call(this, "SetWPHelpOptions", commandKeyHelp, docNavigationKeys);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param commandKeyHelp an input-parameter of type Variant
	 */
	public void setWPHelpOptions(Variant commandKeyHelp) {
		Dispatch.call(this, "SetWPHelpOptions", commandKeyHelp);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void setWPHelpOptions() {
		Dispatch.call(this, "SetWPHelpOptions");
	}

}
