/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdSaveFormat {

	public static final int wdFormatDocument = 0;
	public static final int wdFormatTemplate = 1;
	public static final int wdFormatText = 2;
	public static final int wdFormatTextLineBreaks = 3;
	public static final int wdFormatDOSText = 4;
	public static final int wdFormatDOSTextLineBreaks = 5;
	public static final int wdFormatRTF = 6;
	public static final int wdFormatUnicodeText = 7;
}
