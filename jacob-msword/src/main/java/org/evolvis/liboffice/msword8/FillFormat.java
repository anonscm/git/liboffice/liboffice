/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class FillFormat extends Dispatch {

	public static final String componentName = "Word.FillFormat";

	public FillFormat() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public FillFormat(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public FillFormat(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ColorFormat
	 */
	public ColorFormat getBackColor() {
		return new ColorFormat(Dispatch.get(this, "BackColor").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ColorFormat
	 */
	public ColorFormat getForeColor() {
		return new ColorFormat(Dispatch.get(this, "ForeColor").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoGradientColorType
	 */
	/*
	public MsoGradientColorType getGradientColorType() {
		return new MsoGradientColorType(Dispatch.get(this, "GradientColorType").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getGradientDegree() {
		return Dispatch.get(this, "GradientDegree").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoGradientStyle
	 */
	/*
	public MsoGradientStyle getGradientStyle() {
		return new MsoGradientStyle(Dispatch.get(this, "GradientStyle").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getGradientVariant() {
		return Dispatch.get(this, "GradientVariant").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPatternType
	 */
	/*
	public MsoPatternType getPattern() {
		return new MsoPatternType(Dispatch.get(this, "Pattern").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetGradientType
	 */
	/*
	public MsoPresetGradientType getPresetGradientType() {
		return new MsoPresetGradientType(Dispatch.get(this, "PresetGradientType").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetTexture
	 */
	/*
	public MsoPresetTexture getPresetTexture() {
		return new MsoPresetTexture(Dispatch.get(this, "PresetTexture").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getTextureName() {
		return Dispatch.get(this, "TextureName").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTextureType
	 */
	/*
	public MsoTextureType getTextureType() {
		return new MsoTextureType(Dispatch.get(this, "TextureType").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getTransparency() {
		return Dispatch.get(this, "Transparency").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setTransparency(float lastParam) {
		Dispatch.call(this, "Transparency", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoFillType
	 */
	/*
	public MsoFillType getType() {
		return new MsoFillType(Dispatch.get(this, "Type").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getVisible() {
		return new MsoTriState(Dispatch.get(this, "Visible").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setVisible(MsoTriState lastParam) {
		Dispatch.call(this, "Visible", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void background() {
		Dispatch.call(this, "Background");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param style an input-parameter of type MsoGradientStyle
	 * @param variant an input-parameter of type int
	 * @param lastParam an input-parameter of type float
	 */
	/*
	public void oneColorGradient(MsoGradientStyle style, int variant, float lastParam) {
		Dispatch.call(this, "OneColorGradient", style, new Variant(variant), new Variant(lastParam));
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPatternType
	 */
	/*
	public void patterned(MsoPatternType lastParam) {
		Dispatch.call(this, "Patterned", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param style an input-parameter of type MsoGradientStyle
	 * @param variant an input-parameter of type int
	 * @param lastParam an input-parameter of type MsoPresetGradientType
	 */
	/*
	public void presetGradient(MsoGradientStyle style, int variant, MsoPresetGradientType lastParam) {
		Dispatch.call(this, "PresetGradient", style, new Variant(variant), lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetTexture
	 */
	/*
	public void presetTextured(MsoPresetTexture lastParam) {
		Dispatch.call(this, "PresetTextured", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void solid() {
		Dispatch.call(this, "Solid");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param style an input-parameter of type MsoGradientStyle
	 * @param lastParam an input-parameter of type int
	 */
	/*
	public void twoColorGradient(MsoGradientStyle style, int lastParam) {
		Dispatch.call(this, "TwoColorGradient", style, new Variant(lastParam));
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void userPicture(String lastParam) {
		Dispatch.call(this, "UserPicture", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void userTextured(String lastParam) {
		Dispatch.call(this, "UserTextured", lastParam);
	}

}
