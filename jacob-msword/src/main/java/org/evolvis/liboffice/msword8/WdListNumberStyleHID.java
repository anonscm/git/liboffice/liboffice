/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdListNumberStyleHID {

	public static final int wdListNumberStyleKanji = 10;
	public static final int wdListNumberStyleKanjiDigit = 11;
	public static final int wdListNumberStyleAiueoHalfWidth = 12;
	public static final int wdListNumberStyleIrohaHalfWidth = 13;
	public static final int wdListNumberStyleArabicFullWidth = 14;
	public static final int wdListNumberStyleKanjiTraditional = 16;
	public static final int wdListNumberStyleKanjiTraditional2 = 17;
	public static final int wdListNumberStyleNumberInCircle = 18;
	public static final int wdListNumberStyleAiueo = 20;
	public static final int wdListNumberStyleIroha = 21;
	public static final int wdListNumberStyleGanada = 24;
	public static final int wdListNumberStyleChosung = 25;
	public static final int wdListNumberStyleGBNum1 = 26;
	public static final int wdListNumberStyleGBNum2 = 27;
	public static final int wdListNumberStyleGBNum3 = 28;
	public static final int wdListNumberStyleGBNum4 = 29;
	public static final int wdListNumberStyleZodiac1 = 30;
	public static final int wdListNumberStyleZodiac2 = 31;
	public static final int wdListNumberStyleZodiac3 = 32;
	public static final int wdListNumberStyleTradChinNum1 = 33;
	public static final int wdListNumberStyleTradChinNum2 = 34;
	public static final int wdListNumberStyleTradChinNum3 = 35;
	public static final int wdListNumberStyleTradChinNum4 = 36;
	public static final int wdListNumberStyleSimpChinNum1 = 37;
	public static final int wdListNumberStyleSimpChinNum2 = 38;
	public static final int wdListNumberStyleSimpChinNum3 = 39;
	public static final int wdListNumberStyleSimpChinNum4 = 40;
	public static final int wdListNumberStyleHanjaRead = 41;
	public static final int wdListNumberStyleHanjaReadDigit = 42;
	public static final int wdListNumberStyleHangul = 43;
	public static final int wdListNumberStyleHanja = 44;
}
