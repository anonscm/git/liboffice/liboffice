/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class AutoCorrect extends Dispatch {

	public static final String componentName = "Word.AutoCorrect";

	public AutoCorrect() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public AutoCorrect(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public AutoCorrect(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCorrectDays() {
		return Dispatch.get(this, "CorrectDays").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCorrectDays(boolean lastParam) {
		Dispatch.call(this, "CorrectDays", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCorrectInitialCaps() {
		return Dispatch.get(this, "CorrectInitialCaps").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCorrectInitialCaps(boolean lastParam) {
		Dispatch.call(this, "CorrectInitialCaps", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCorrectSentenceCaps() {
		return Dispatch.get(this, "CorrectSentenceCaps").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCorrectSentenceCaps(boolean lastParam) {
		Dispatch.call(this, "CorrectSentenceCaps", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getReplaceText() {
		return Dispatch.get(this, "ReplaceText").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setReplaceText(boolean lastParam) {
		Dispatch.call(this, "ReplaceText", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type AutoCorrectEntries
	 */
	public AutoCorrectEntries getEntries() {
		return new AutoCorrectEntries(Dispatch.get(this, "Entries").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FirstLetterExceptions
	 */
	public FirstLetterExceptions getFirstLetterExceptions() {
		return new FirstLetterExceptions(Dispatch.get(this, "FirstLetterExceptions").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getFirstLetterAutoAdd() {
		return Dispatch.get(this, "FirstLetterAutoAdd").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setFirstLetterAutoAdd(boolean lastParam) {
		Dispatch.call(this, "FirstLetterAutoAdd", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type TwoInitialCapsExceptions
	 */
	public TwoInitialCapsExceptions getTwoInitialCapsExceptions() {
		return new TwoInitialCapsExceptions(Dispatch.get(this, "TwoInitialCapsExceptions").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getTwoInitialCapsAutoAdd() {
		return Dispatch.get(this, "TwoInitialCapsAutoAdd").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setTwoInitialCapsAutoAdd(boolean lastParam) {
		Dispatch.call(this, "TwoInitialCapsAutoAdd", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCorrectCapsLock() {
		return Dispatch.get(this, "CorrectCapsLock").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCorrectCapsLock(boolean lastParam) {
		Dispatch.call(this, "CorrectCapsLock", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCorrectHangulAndAlphabet() {
		return Dispatch.get(this, "CorrectHangulAndAlphabet").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCorrectHangulAndAlphabet(boolean lastParam) {
		Dispatch.call(this, "CorrectHangulAndAlphabet", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type HangulAndAlphabetExceptions
	 */
	public HangulAndAlphabetExceptions getHangulAndAlphabetExceptions() {
		return new HangulAndAlphabetExceptions(Dispatch.get(this, "HangulAndAlphabetExceptions").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getHangulAndAlphabetAutoAdd() {
		return Dispatch.get(this, "HangulAndAlphabetAutoAdd").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setHangulAndAlphabetAutoAdd(boolean lastParam) {
		Dispatch.call(this, "HangulAndAlphabetAutoAdd", new Variant(lastParam));
	}

}
