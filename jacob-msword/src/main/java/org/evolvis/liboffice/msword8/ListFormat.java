/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class ListFormat extends Dispatch {

	public static final String componentName = "Word.ListFormat";

	public ListFormat() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public ListFormat(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public ListFormat(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getListLevelNumber() {
		return Dispatch.get(this, "ListLevelNumber").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setListLevelNumber(int lastParam) {
		Dispatch.call(this, "ListLevelNumber", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type List
	 */
	public List getList() {
		return new List(Dispatch.get(this, "List").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ListTemplate
	 */
	public ListTemplate getListTemplate() {
		return new ListTemplate(Dispatch.get(this, "ListTemplate").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getListValue() {
		return Dispatch.get(this, "ListValue").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSingleList() {
		return Dispatch.get(this, "SingleList").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSingleListTemplate() {
		return Dispatch.get(this, "SingleListTemplate").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getListType() {
		return Dispatch.get(this, "ListType").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getListString() {
		return Dispatch.get(this, "ListString").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type ListTemplate
	 * @return the result is of type int
	 */
	public int canContinuePreviousList(ListTemplate lastParam) {
		return Dispatch.call(this, "CanContinuePreviousList", lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void removeNumbers(Variant lastParam) {
		Dispatch.call(this, "RemoveNumbers", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void removeNumbers() {
		Dispatch.call(this, "RemoveNumbers");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void convertNumbersToText(Variant lastParam) {
		Dispatch.call(this, "ConvertNumbersToText", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void convertNumbersToText() {
		Dispatch.call(this, "ConvertNumbersToText");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param numberType an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int countNumberedItems(Variant numberType, Variant lastParam) {
		return Dispatch.call(this, "CountNumberedItems", numberType, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param numberType an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int countNumberedItems(Variant numberType) {
		return Dispatch.call(this, "CountNumberedItems", numberType).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int countNumberedItems() {
		return Dispatch.call(this, "CountNumberedItems").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void applyBulletDefault() {
		Dispatch.call(this, "ApplyBulletDefault");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void applyNumberDefault() {
		Dispatch.call(this, "ApplyNumberDefault");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void applyOutlineNumberDefault() {
		Dispatch.call(this, "ApplyOutlineNumberDefault");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param listTemplate an input-parameter of type ListTemplate
	 * @param continuePreviousList an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void applyListTemplate(ListTemplate listTemplate, Variant continuePreviousList, Variant lastParam) {
		Dispatch.call(this, "ApplyListTemplate", listTemplate, continuePreviousList, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param listTemplate an input-parameter of type ListTemplate
	 * @param continuePreviousList an input-parameter of type Variant
	 */
	public void applyListTemplate(ListTemplate listTemplate, Variant continuePreviousList) {
		Dispatch.call(this, "ApplyListTemplate", listTemplate, continuePreviousList);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param listTemplate an input-parameter of type ListTemplate
	 */
	public void applyListTemplate(ListTemplate listTemplate) {
		Dispatch.call(this, "ApplyListTemplate", listTemplate);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void listOutdent() {
		Dispatch.call(this, "ListOutdent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void listIndent() {
		Dispatch.call(this, "ListIndent");
	}

}
