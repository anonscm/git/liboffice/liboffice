/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdWindowType {

	public static final int wdWindowDocument = 0;
	public static final int wdWindowTemplate = 1;
}
