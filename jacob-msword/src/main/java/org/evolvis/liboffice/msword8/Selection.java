/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Selection extends Dispatch {

	public static final String componentName = "Word.Selection";

	public Selection() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Selection(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Selection(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getText() {
		return Dispatch.get(this, "Text").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setText(String lastParam) {
		Dispatch.call(this, "Text", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range getFormattedText() {
		return new Range(Dispatch.get(this, "FormattedText").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 */
	public void setFormattedText(Range lastParam) {
		Dispatch.call(this, "FormattedText", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getStart() {
		return Dispatch.get(this, "Start").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setStart(int lastParam) {
		Dispatch.call(this, "Start", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getEnd() {
		return Dispatch.get(this, "End").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setEnd(int lastParam) {
		Dispatch.call(this, "End", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Font
	 */
	public Font getFont() {
		return new Font(Dispatch.get(this, "Font").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Font
	 */
	public void setFont(Font lastParam) {
		Dispatch.call(this, "Font", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getType() {
		return Dispatch.get(this, "Type").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getStoryType() {
		return Dispatch.get(this, "StoryType").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant getStyle() {
		return Dispatch.get(this, "Style");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void setStyle(Variant lastParam) {
		//Dispatch.call(this, "Style", lastParam);
		Dispatch.invokeSubv(this, "Style", Dispatch.Put, new Variant[] { lastParam }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Tables
	 */
	public Tables getTables() {
		return new Tables(Dispatch.get(this, "Tables").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Words
	 */
	public Words getWords() {
		return new Words(Dispatch.get(this, "Words").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Sentences
	 */
	public Sentences getSentences() {
		return new Sentences(Dispatch.get(this, "Sentences").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Characters
	 */
	public Characters getCharacters() {
		return new Characters(Dispatch.get(this, "Characters").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Footnotes
	 */
	public Footnotes getFootnotes() {
		return new Footnotes(Dispatch.get(this, "Footnotes").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Endnotes
	 */
	public Endnotes getEndnotes() {
		return new Endnotes(Dispatch.get(this, "Endnotes").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Comments
	 */
	public Comments getComments() {
		return new Comments(Dispatch.get(this, "Comments").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Cells
	 */
	public Cells getCells() {
		return new Cells(Dispatch.get(this, "Cells").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Sections
	 */
	public Sections getSections() {
		return new Sections(Dispatch.get(this, "Sections").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Paragraphs
	 */
	public Paragraphs getParagraphs() {
		return new Paragraphs(Dispatch.get(this, "Paragraphs").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Borders
	 */
	public Borders getBorders() {
		return new Borders(Dispatch.get(this, "Borders").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Borders
	 */
	public void setBorders(Borders lastParam) {
		Dispatch.call(this, "Borders", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Shading
	 */
	public Shading getShading() {
		return new Shading(Dispatch.get(this, "Shading").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Fields
	 */
	public Fields getFields() {
		return new Fields(Dispatch.get(this, "Fields").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FormFields
	 */
	public FormFields getFormFields() {
		return new FormFields(Dispatch.get(this, "FormFields").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Frames
	 */
	public Frames getFrames() {
		return new Frames(Dispatch.get(this, "Frames").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ParagraphFormat
	 */
	public ParagraphFormat getParagraphFormat() {
		return new ParagraphFormat(Dispatch.get(this, "ParagraphFormat").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type ParagraphFormat
	 */
	public void setParagraphFormat(ParagraphFormat lastParam) {
		Dispatch.call(this, "ParagraphFormat", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type PageSetup
	 */
	public PageSetup getPageSetup() {
		return new PageSetup(Dispatch.get(this, "PageSetup").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type PageSetup
	 */
	public void setPageSetup(PageSetup lastParam) {
		Dispatch.call(this, "PageSetup", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Bookmarks
	 */
	public Bookmarks getBookmarks() {
		return new Bookmarks(Dispatch.get(this, "Bookmarks").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getStoryLength() {
		return Dispatch.get(this, "StoryLength").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLanguageID() {
		return Dispatch.get(this, "LanguageID").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLanguageID(int lastParam) {
		Dispatch.call(this, "LanguageID", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLanguageIDFarEast() {
		return Dispatch.get(this, "LanguageIDFarEast").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLanguageIDFarEast(int lastParam) {
		Dispatch.call(this, "LanguageIDFarEast", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLanguageIDOther() {
		return Dispatch.get(this, "LanguageIDOther").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLanguageIDOther(int lastParam) {
		Dispatch.call(this, "LanguageIDOther", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Hyperlinks
	 */
	public Hyperlinks getHyperlinks() {
		return new Hyperlinks(Dispatch.get(this, "Hyperlinks").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Columns
	 */
	public Columns getColumns() {
		return new Columns(Dispatch.get(this, "Columns").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Rows
	 */
	public Rows getRows() {
		return new Rows(Dispatch.get(this, "Rows").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type HeaderFooter
	 */
	public HeaderFooter getHeaderFooter() {
		return new HeaderFooter(Dispatch.get(this, "HeaderFooter").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getIsEndOfRowMark() {
		return Dispatch.get(this, "IsEndOfRowMark").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getBookmarkID() {
		return Dispatch.get(this, "BookmarkID").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getPreviousBookmarkID() {
		return Dispatch.get(this, "PreviousBookmarkID").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Find
	 */
	public Find getFind() {
		return new Find(Dispatch.get(this, "Find").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range getRange() {
		return new Range(Dispatch.get(this, "Range").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type Variant
	 */
	public Variant getInformation(int lastParam) {
		return Dispatch.call(this, "Information", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getFlags() {
		return Dispatch.get(this, "Flags").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setFlags(int lastParam) {
		Dispatch.call(this, "Flags", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getActive() {
		return Dispatch.get(this, "Active").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getStartIsActive() {
		return Dispatch.get(this, "StartIsActive").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setStartIsActive(boolean lastParam) {
		Dispatch.call(this, "StartIsActive", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getIPAtEndOfLine() {
		return Dispatch.get(this, "IPAtEndOfLine").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getExtendMode() {
		return Dispatch.get(this, "ExtendMode").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setExtendMode(boolean lastParam) {
		Dispatch.call(this, "ExtendMode", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getColumnSelectMode() {
		return Dispatch.get(this, "ColumnSelectMode").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setColumnSelectMode(boolean lastParam) {
		Dispatch.call(this, "ColumnSelectMode", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getOrientation() {
		return Dispatch.get(this, "Orientation").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setOrientation(int lastParam) {
		Dispatch.call(this, "Orientation", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type InlineShapes
	 */
	public InlineShapes getInlineShapes() {
		return new InlineShapes(Dispatch.get(this, "InlineShapes").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Document
	 */
	public Document getDocument() {
		return new Document(Dispatch.get(this, "Document").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ShapeRange
	 */
	public ShapeRange getShapeRange() {
		return new ShapeRange(Dispatch.get(this, "ShapeRange").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void select() {
		Dispatch.call(this, "Select");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param start an input-parameter of type int
	 * @param lastParam an input-parameter of type int
	 */
	public void setRange(int start, int lastParam) {
		Dispatch.call(this, "SetRange", new Variant(start), new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void collapse(Variant lastParam) {
		Dispatch.call(this, "Collapse", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void collapse() {
		Dispatch.call(this, "Collapse");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void insertBefore(String lastParam) {
		Dispatch.call(this, "InsertBefore", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void insertAfter(String lastParam) {
		Dispatch.call(this, "InsertAfter", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range next(Variant unit, Variant lastParam) {
		return new Range(Dispatch.call(this, "Next", unit, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range next(Variant unit) {
		return new Range(Dispatch.call(this, "Next", unit).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range next() {
		return new Range(Dispatch.call(this, "Next").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range previous(Variant unit, Variant lastParam) {
		return new Range(Dispatch.call(this, "Previous", unit, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range previous(Variant unit) {
		return new Range(Dispatch.call(this, "Previous", unit).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range previous() {
		return new Range(Dispatch.call(this, "Previous").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int startOf(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "StartOf", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int startOf(Variant unit) {
		return Dispatch.call(this, "StartOf", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int startOf() {
		return Dispatch.call(this, "StartOf").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int endOf(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "EndOf", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int endOf(Variant unit) {
		return Dispatch.call(this, "EndOf", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int endOf() {
		return Dispatch.call(this, "EndOf").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int move(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "Move", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int move(Variant unit) {
		return Dispatch.call(this, "Move", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int move() {
		return Dispatch.call(this, "Move").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveStart(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "MoveStart", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveStart(Variant unit) {
		return Dispatch.call(this, "MoveStart", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int moveStart() {
		return Dispatch.call(this, "MoveStart").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveEnd(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "MoveEnd", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveEnd(Variant unit) {
		return Dispatch.call(this, "MoveEnd", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int moveEnd() {
		return Dispatch.call(this, "MoveEnd").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveWhile(Variant cset, Variant lastParam) {
		return Dispatch.call(this, "MoveWhile", cset, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveWhile(Variant cset) {
		return Dispatch.call(this, "MoveWhile", cset).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveStartWhile(Variant cset, Variant lastParam) {
		return Dispatch.call(this, "MoveStartWhile", cset, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveStartWhile(Variant cset) {
		return Dispatch.call(this, "MoveStartWhile", cset).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveEndWhile(Variant cset, Variant lastParam) {
		return Dispatch.call(this, "MoveEndWhile", cset, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveEndWhile(Variant cset) {
		return Dispatch.call(this, "MoveEndWhile", cset).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveUntil(Variant cset, Variant lastParam) {
		return Dispatch.call(this, "MoveUntil", cset, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveUntil(Variant cset) {
		return Dispatch.call(this, "MoveUntil", cset).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveStartUntil(Variant cset, Variant lastParam) {
		return Dispatch.call(this, "MoveStartUntil", cset, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveStartUntil(Variant cset) {
		return Dispatch.call(this, "MoveStartUntil", cset).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveEndUntil(Variant cset, Variant lastParam) {
		return Dispatch.call(this, "MoveEndUntil", cset, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param cset an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveEndUntil(Variant cset) {
		return Dispatch.call(this, "MoveEndUntil", cset).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void cut() {
		Dispatch.call(this, "Cut");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void copy() {
		Dispatch.call(this, "Copy");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void paste() {
		Dispatch.call(this, "Paste");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertBreak(Variant lastParam) {
		Dispatch.call(this, "InsertBreak", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertBreak() {
		Dispatch.call(this, "InsertBreak");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @param range an input-parameter of type Variant
	 * @param confirmConversions an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertFile(String fileName, Variant range, Variant confirmConversions, Variant link, Variant lastParam) {
		Dispatch.call(this, "InsertFile", fileName, range, confirmConversions, link, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @param range an input-parameter of type Variant
	 * @param confirmConversions an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 */
	public void insertFile(String fileName, Variant range, Variant confirmConversions, Variant link) {
		Dispatch.call(this, "InsertFile", fileName, range, confirmConversions, link);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @param range an input-parameter of type Variant
	 * @param confirmConversions an input-parameter of type Variant
	 */
	public void insertFile(String fileName, Variant range, Variant confirmConversions) {
		Dispatch.call(this, "InsertFile", fileName, range, confirmConversions);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @param range an input-parameter of type Variant
	 */
	public void insertFile(String fileName, Variant range) {
		Dispatch.call(this, "InsertFile", fileName, range);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 */
	public void insertFile(String fileName) {
		Dispatch.call(this, "InsertFile", fileName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 * @return the result is of type boolean
	 */
	public boolean inStory(Range lastParam) {
		return Dispatch.call(this, "InStory", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 * @return the result is of type boolean
	 */
	public boolean inRange(Range lastParam) {
		return Dispatch.call(this, "InRange", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int delete(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "Delete", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int delete(Variant unit) {
		return Dispatch.call(this, "Delete", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int delete() {
		return Dispatch.call(this, "Delete").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int expand(Variant lastParam) {
		return Dispatch.call(this, "Expand", lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int expand() {
		return Dispatch.call(this, "Expand").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertParagraph() {
		Dispatch.call(this, "InsertParagraph");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertParagraphAfter() {
		Dispatch.call(this, "InsertParagraphAfter");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 * @param applyFirstColumn an input-parameter of type Variant
	 * @param applyLastColumn an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow, Variant applyFirstColumn, Variant applyLastColumn, Variant lastParam) {
		return new Table(Dispatch.callN(this, "ConvertToTable", new Object[] { separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow, applyFirstColumn, applyLastColumn, lastParam}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 * @param applyFirstColumn an input-parameter of type Variant
	 * @param applyLastColumn an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow, Variant applyFirstColumn, Variant applyLastColumn) {
		return new Table(Dispatch.callN(this, "ConvertToTable", new Object[] { separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow, applyFirstColumn, applyLastColumn}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 * @param applyFirstColumn an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow, Variant applyFirstColumn) {
		return new Table(Dispatch.callN(this, "ConvertToTable", new Object[] { separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow, applyFirstColumn}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow) {
		return new Table(Dispatch.callN(this, "ConvertToTable", new Object[] { separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows) {
		return new Table(Dispatch.callN(this, "ConvertToTable", new Object[] { separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor) {
		return new Table(Dispatch.callN(this, "ConvertToTable", new Object[] { separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading, applyFont, applyColor}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading, Variant applyFont) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading, applyFont).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders, Variant applyShading) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator, numRows, numColumns, initialColumnWidth, format, applyBorders, applyShading).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format, Variant applyBorders) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator, numRows, numColumns, initialColumnWidth, format, applyBorders).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth, Variant format) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator, numRows, numColumns, initialColumnWidth, format).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @param initialColumnWidth an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns, Variant initialColumnWidth) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator, numRows, numColumns, initialColumnWidth).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @param numColumns an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows, Variant numColumns) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator, numRows, numColumns).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @param numRows an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator, Variant numRows) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator, numRows).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param separator an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table convertToTable(Variant separator) {
		return new Table(Dispatch.call(this, "ConvertToTable", separator).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Table
	 */
	public Table convertToTable() {
		return new Table(Dispatch.call(this, "ConvertToTable").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param dateTimeFormat an input-parameter of type Variant
	 * @param insertAsField an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertDateTime(Variant dateTimeFormat, Variant insertAsField, Variant lastParam) {
		Dispatch.call(this, "InsertDateTime", dateTimeFormat, insertAsField, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param dateTimeFormat an input-parameter of type Variant
	 * @param insertAsField an input-parameter of type Variant
	 */
	public void insertDateTime(Variant dateTimeFormat, Variant insertAsField) {
		Dispatch.call(this, "InsertDateTime", dateTimeFormat, insertAsField);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param dateTimeFormat an input-parameter of type Variant
	 */
	public void insertDateTime(Variant dateTimeFormat) {
		Dispatch.call(this, "InsertDateTime", dateTimeFormat);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertDateTime() {
		Dispatch.call(this, "InsertDateTime");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param characterNumber an input-parameter of type int
	 * @param font an input-parameter of type Variant
	 * @param unicode an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertSymbol(int characterNumber, Variant font, Variant unicode, Variant lastParam) {
		Dispatch.call(this, "InsertSymbol", new Variant(characterNumber), font, unicode, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param characterNumber an input-parameter of type int
	 * @param font an input-parameter of type Variant
	 * @param unicode an input-parameter of type Variant
	 */
	public void insertSymbol(int characterNumber, Variant font, Variant unicode) {
		Dispatch.call(this, "InsertSymbol", new Variant(characterNumber), font, unicode);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param characterNumber an input-parameter of type int
	 * @param font an input-parameter of type Variant
	 */
	public void insertSymbol(int characterNumber, Variant font) {
		Dispatch.call(this, "InsertSymbol", new Variant(characterNumber), font);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param characterNumber an input-parameter of type int
	 */
	public void insertSymbol(int characterNumber) {
		Dispatch.call(this, "InsertSymbol", new Variant(characterNumber));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param referenceType an input-parameter of type Variant
	 * @param referenceKind an input-parameter of type int
	 * @param referenceItem an input-parameter of type Variant
	 * @param insertAsHyperlink an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertCrossReference(Variant referenceType, int referenceKind, Variant referenceItem, Variant insertAsHyperlink, Variant lastParam) {
		Dispatch.call(this, "InsertCrossReference", referenceType, new Variant(referenceKind), referenceItem, insertAsHyperlink, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param referenceType an input-parameter of type Variant
	 * @param referenceKind an input-parameter of type int
	 * @param referenceItem an input-parameter of type Variant
	 * @param insertAsHyperlink an input-parameter of type Variant
	 */
	public void insertCrossReference(Variant referenceType, int referenceKind, Variant referenceItem, Variant insertAsHyperlink) {
		Dispatch.call(this, "InsertCrossReference", referenceType, new Variant(referenceKind), referenceItem, insertAsHyperlink);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param referenceType an input-parameter of type Variant
	 * @param referenceKind an input-parameter of type int
	 * @param referenceItem an input-parameter of type Variant
	 */
	public void insertCrossReference(Variant referenceType, int referenceKind, Variant referenceItem) {
		Dispatch.call(this, "InsertCrossReference", referenceType, new Variant(referenceKind), referenceItem);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param label an input-parameter of type Variant
	 * @param title an input-parameter of type Variant
	 * @param titleAutoText an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertCaption(Variant label, Variant title, Variant titleAutoText, Variant lastParam) {
		Dispatch.call(this, "InsertCaption", label, title, titleAutoText, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param label an input-parameter of type Variant
	 * @param title an input-parameter of type Variant
	 * @param titleAutoText an input-parameter of type Variant
	 */
	public void insertCaption(Variant label, Variant title, Variant titleAutoText) {
		Dispatch.call(this, "InsertCaption", label, title, titleAutoText);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param label an input-parameter of type Variant
	 * @param title an input-parameter of type Variant
	 */
	public void insertCaption(Variant label, Variant title) {
		Dispatch.call(this, "InsertCaption", label, title);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param label an input-parameter of type Variant
	 */
	public void insertCaption(Variant label) {
		Dispatch.call(this, "InsertCaption", label);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void copyAsPicture() {
		Dispatch.call(this, "CopyAsPicture");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 * @param sortColumn an input-parameter of type Variant
	 * @param separator an input-parameter of type Variant
	 * @param caseSensitive an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3, Variant sortColumn, Variant separator, Variant caseSensitive, Variant lastParam) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3, sortColumn, separator, caseSensitive, lastParam});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 * @param sortColumn an input-parameter of type Variant
	 * @param separator an input-parameter of type Variant
	 * @param caseSensitive an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3, Variant sortColumn, Variant separator, Variant caseSensitive) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3, sortColumn, separator, caseSensitive});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 * @param sortColumn an input-parameter of type Variant
	 * @param separator an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3, Variant sortColumn, Variant separator) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3, sortColumn, separator});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 * @param sortColumn an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3, Variant sortColumn) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3, sortColumn});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader) {
		Dispatch.call(this, "Sort", excludeHeader);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void sort() {
		Dispatch.call(this, "Sort");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void sortAscending() {
		Dispatch.call(this, "SortAscending");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void sortDescending() {
		Dispatch.call(this, "SortDescending");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 * @return the result is of type boolean
	 */
	public boolean isEqual(Range lastParam) {
		return Dispatch.call(this, "IsEqual", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float calculate() {
		return Dispatch.call(this, "Calculate").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param what an input-parameter of type Variant
	 * @param which an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range m_goTo(Variant what, Variant which, Variant count, Variant lastParam) {
		return new Range(Dispatch.call(this, "GoTo", what, which, count, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param what an input-parameter of type Variant
	 * @param which an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range m_goTo(Variant what, Variant which, Variant count) {
		return new Range(Dispatch.call(this, "GoTo", what, which, count).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param what an input-parameter of type Variant
	 * @param which an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range m_goTo(Variant what, Variant which) {
		return new Range(Dispatch.call(this, "GoTo", what, which).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param what an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range m_goTo(Variant what) {
		return new Range(Dispatch.call(this, "GoTo", what).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range m_goTo() {
		return new Range(Dispatch.call(this, "GoTo").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type Range
	 */
	public Range goToNext(int lastParam) {
		return new Range(Dispatch.call(this, "GoToNext", new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type Range
	 */
	public Range goToPrevious(int lastParam) {
		return new Range(Dispatch.call(this, "GoToPrevious", new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param iconIndex an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 * @param placement an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param dataType an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void pasteSpecial(Variant iconIndex, Variant link, Variant placement, Variant displayAsIcon, Variant dataType, Variant iconFileName, Variant lastParam) {
		Dispatch.call(this, "PasteSpecial", iconIndex, link, placement, displayAsIcon, dataType, iconFileName, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param iconIndex an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 * @param placement an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param dataType an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 */
	public void pasteSpecial(Variant iconIndex, Variant link, Variant placement, Variant displayAsIcon, Variant dataType, Variant iconFileName) {
		Dispatch.call(this, "PasteSpecial", iconIndex, link, placement, displayAsIcon, dataType, iconFileName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param iconIndex an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 * @param placement an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param dataType an input-parameter of type Variant
	 */
	public void pasteSpecial(Variant iconIndex, Variant link, Variant placement, Variant displayAsIcon, Variant dataType) {
		Dispatch.call(this, "PasteSpecial", iconIndex, link, placement, displayAsIcon, dataType);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param iconIndex an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 * @param placement an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 */
	public void pasteSpecial(Variant iconIndex, Variant link, Variant placement, Variant displayAsIcon) {
		Dispatch.call(this, "PasteSpecial", iconIndex, link, placement, displayAsIcon);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param iconIndex an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 * @param placement an input-parameter of type Variant
	 */
	public void pasteSpecial(Variant iconIndex, Variant link, Variant placement) {
		Dispatch.call(this, "PasteSpecial", iconIndex, link, placement);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param iconIndex an input-parameter of type Variant
	 * @param link an input-parameter of type Variant
	 */
	public void pasteSpecial(Variant iconIndex, Variant link) {
		Dispatch.call(this, "PasteSpecial", iconIndex, link);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param iconIndex an input-parameter of type Variant
	 */
	public void pasteSpecial(Variant iconIndex) {
		Dispatch.call(this, "PasteSpecial", iconIndex);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void pasteSpecial() {
		Dispatch.call(this, "PasteSpecial");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Field
	 */
	public Field previousField() {
		return new Field(Dispatch.call(this, "PreviousField").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Field
	 */
	public Field nextField() {
		return new Field(Dispatch.call(this, "NextField").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertParagraphBefore() {
		Dispatch.call(this, "InsertParagraphBefore");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertCells(Variant lastParam) {
		Dispatch.call(this, "InsertCells", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertCells() {
		Dispatch.call(this, "InsertCells");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void extend(Variant lastParam) {
		Dispatch.call(this, "Extend", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void extend() {
		Dispatch.call(this, "Extend");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void shrink() {
		Dispatch.call(this, "Shrink");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveLeft(Variant unit, Variant count, Variant lastParam) {
		return Dispatch.call(this, "MoveLeft", unit, count, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveLeft(Variant unit, Variant count) {
		return Dispatch.call(this, "MoveLeft", unit, count).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveLeft(Variant unit) {
		return Dispatch.call(this, "MoveLeft", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int moveLeft() {
		return Dispatch.call(this, "MoveLeft").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveRight(Variant unit, Variant count, Variant lastParam) {
		return Dispatch.call(this, "MoveRight", unit, count, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveRight(Variant unit, Variant count) {
		return Dispatch.call(this, "MoveRight", unit, count).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveRight(Variant unit) {
		return Dispatch.call(this, "MoveRight", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int moveRight() {
		return Dispatch.call(this, "MoveRight").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveUp(Variant unit, Variant count, Variant lastParam) {
		return Dispatch.call(this, "MoveUp", unit, count, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveUp(Variant unit, Variant count) {
		return Dispatch.call(this, "MoveUp", unit, count).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveUp(Variant unit) {
		return Dispatch.call(this, "MoveUp", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int moveUp() {
		return Dispatch.call(this, "MoveUp").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveDown(Variant unit, Variant count, Variant lastParam) {
		return Dispatch.call(this, "MoveDown", unit, count, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param count an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveDown(Variant unit, Variant count) {
		return Dispatch.call(this, "MoveDown", unit, count).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int moveDown(Variant unit) {
		return Dispatch.call(this, "MoveDown", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int moveDown() {
		return Dispatch.call(this, "MoveDown").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int homeKey(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "HomeKey", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int homeKey(Variant unit) {
		return Dispatch.call(this, "HomeKey", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int homeKey() {
		return Dispatch.call(this, "HomeKey").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int endKey(Variant unit, Variant lastParam) {
		return Dispatch.call(this, "EndKey", unit, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unit an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int endKey(Variant unit) {
		return Dispatch.call(this, "EndKey", unit).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int endKey() {
		return Dispatch.call(this, "EndKey").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void escapeKey() {
		Dispatch.call(this, "EscapeKey");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void typeText(String lastParam) {
		Dispatch.call(this, "TypeText", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void copyFormat() {
		Dispatch.call(this, "CopyFormat");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void pasteFormat() {
		Dispatch.call(this, "PasteFormat");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void typeParagraph() {
		Dispatch.call(this, "TypeParagraph");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void typeBackspace() {
		Dispatch.call(this, "TypeBackspace");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void nextSubdocument() {
		Dispatch.call(this, "NextSubdocument");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void previousSubdocument() {
		Dispatch.call(this, "PreviousSubdocument");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectColumn() {
		Dispatch.call(this, "SelectColumn");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectCurrentFont() {
		Dispatch.call(this, "SelectCurrentFont");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectCurrentAlignment() {
		Dispatch.call(this, "SelectCurrentAlignment");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectCurrentSpacing() {
		Dispatch.call(this, "SelectCurrentSpacing");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectCurrentIndent() {
		Dispatch.call(this, "SelectCurrentIndent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectCurrentTabs() {
		Dispatch.call(this, "SelectCurrentTabs");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectCurrentColor() {
		Dispatch.call(this, "SelectCurrentColor");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void createTextbox() {
		Dispatch.call(this, "CreateTextbox");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void wholeStory() {
		Dispatch.call(this, "WholeStory");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void selectRow() {
		Dispatch.call(this, "SelectRow");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void splitTable() {
		Dispatch.call(this, "SplitTable");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertRows(Variant lastParam) {
		Dispatch.call(this, "InsertRows", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertRows() {
		Dispatch.call(this, "InsertRows");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertColumns() {
		Dispatch.call(this, "InsertColumns");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param formula an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void insertFormula(Variant formula, Variant lastParam) {
		Dispatch.call(this, "InsertFormula", formula, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param formula an input-parameter of type Variant
	 */
	public void insertFormula(Variant formula) {
		Dispatch.call(this, "InsertFormula", formula);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void insertFormula() {
		Dispatch.call(this, "InsertFormula");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Revision
	 */
	public Revision nextRevision(Variant lastParam) {
		return new Revision(Dispatch.call(this, "NextRevision", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Revision
	 */
	public Revision nextRevision() {
		return new Revision(Dispatch.call(this, "NextRevision").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Revision
	 */
	public Revision previousRevision(Variant lastParam) {
		return new Revision(Dispatch.call(this, "PreviousRevision", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Revision
	 */
	public Revision previousRevision() {
		return new Revision(Dispatch.call(this, "PreviousRevision").toDispatch());
	}

}
