/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Table extends Dispatch {

	public static final String componentName = "Word.Table";

	public Table() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Table(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Table(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range getRange() {
		return new Range(Dispatch.get(this, "Range").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Columns
	 */
	public Columns getColumns() {
		return new Columns(Dispatch.get(this, "Columns").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Rows
	 */
	public Rows getRows() {
		return new Rows(Dispatch.get(this, "Rows").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Borders
	 */
	public Borders getBorders() {
		return new Borders(Dispatch.get(this, "Borders").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Borders
	 */
	public void setBorders(Borders lastParam) {
		Dispatch.call(this, "Borders", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Shading
	 */
	public Shading getShading() {
		return new Shading(Dispatch.get(this, "Shading").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getUniform() {
		return Dispatch.get(this, "Uniform").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getAutoFormatType() {
		return Dispatch.get(this, "AutoFormatType").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void select() {
		Dispatch.call(this, "Select");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void delete() {
		Dispatch.call(this, "Delete");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 * @param caseSensitive an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3, Variant caseSensitive, Variant lastParam) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3, caseSensitive, lastParam});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 * @param caseSensitive an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3, Variant caseSensitive) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3, caseSensitive});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 * @param sortOrder3 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3, Variant sortOrder3) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3, sortOrder3});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 * @param sortFieldType3 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3, Variant sortFieldType3) {
		Dispatch.callN(this, "Sort", new Object[] { excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3, sortFieldType3});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 * @param fieldNumber3 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2, Variant fieldNumber3) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2, fieldNumber3);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 * @param sortOrder2 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2, Variant sortOrder2) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2, sortOrder2);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 * @param sortFieldType2 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2, Variant sortFieldType2) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2, sortFieldType2);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 * @param fieldNumber2 an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder, Variant fieldNumber2) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder, fieldNumber2);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 * @param sortOrder an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType, Variant sortOrder) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType, sortOrder);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 * @param sortFieldType an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber, Variant sortFieldType) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber, sortFieldType);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 * @param fieldNumber an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader, Variant fieldNumber) {
		Dispatch.call(this, "Sort", excludeHeader, fieldNumber);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param excludeHeader an input-parameter of type Variant
	 */
	public void sort(Variant excludeHeader) {
		Dispatch.call(this, "Sort", excludeHeader);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void sort() {
		Dispatch.call(this, "Sort");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void sortAscending() {
		Dispatch.call(this, "SortAscending");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void sortDescending() {
		Dispatch.call(this, "SortDescending");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 * @param applyFirstColumn an input-parameter of type Variant
	 * @param applyLastColumn an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow, Variant applyFirstColumn, Variant applyLastColumn, Variant lastParam) {
		Dispatch.callN(this, "AutoFormat", new Object[] { format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow, applyFirstColumn, applyLastColumn, lastParam});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 * @param applyFirstColumn an input-parameter of type Variant
	 * @param applyLastColumn an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow, Variant applyFirstColumn, Variant applyLastColumn) {
		Dispatch.callN(this, "AutoFormat", new Object[] { format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow, applyFirstColumn, applyLastColumn});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 * @param applyFirstColumn an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow, Variant applyFirstColumn) {
		Dispatch.call(this, "AutoFormat", format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow, applyFirstColumn);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 * @param applyLastRow an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows, Variant applyLastRow) {
		Dispatch.call(this, "AutoFormat", format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows, applyLastRow);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 * @param applyHeadingRows an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor, Variant applyHeadingRows) {
		Dispatch.call(this, "AutoFormat", format, applyBorders, applyShading, applyFont, applyColor, applyHeadingRows);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 * @param applyColor an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading, Variant applyFont, Variant applyColor) {
		Dispatch.call(this, "AutoFormat", format, applyBorders, applyShading, applyFont, applyColor);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 * @param applyFont an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading, Variant applyFont) {
		Dispatch.call(this, "AutoFormat", format, applyBorders, applyShading, applyFont);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 * @param applyShading an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders, Variant applyShading) {
		Dispatch.call(this, "AutoFormat", format, applyBorders, applyShading);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 * @param applyBorders an input-parameter of type Variant
	 */
	public void autoFormat(Variant format, Variant applyBorders) {
		Dispatch.call(this, "AutoFormat", format, applyBorders);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param format an input-parameter of type Variant
	 */
	public void autoFormat(Variant format) {
		Dispatch.call(this, "AutoFormat", format);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void autoFormat() {
		Dispatch.call(this, "AutoFormat");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void updateAutoFormat() {
		Dispatch.call(this, "UpdateAutoFormat");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Range
	 */
	public Range convertToText(Variant lastParam) {
		return new Range(Dispatch.call(this, "ConvertToText", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range convertToText() {
		return new Range(Dispatch.call(this, "ConvertToText").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param row an input-parameter of type int
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type Cell
	 */
	public Cell cell(int row, int lastParam) {
		return new Cell(Dispatch.call(this, "Cell", new Variant(row), new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Table
	 */
	public Table split(Variant lastParam) {
		return new Table(Dispatch.call(this, "Split", lastParam).toDispatch());
	}

}
