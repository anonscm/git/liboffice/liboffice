/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdPageNumberStyleHID {

	public static final int wdPageNumberStyleArabicFullWidth = 14;
	public static final int wdPageNumberStyleKanji = 10;
	public static final int wdPageNumberStyleKanjiDigit = 11;
	public static final int wdPageNumberStyleKanjiTraditional = 16;
	public static final int wdPageNumberStyleNumberInCircle = 18;
	public static final int wdPageNumberStyleHanjaRead = 41;
	public static final int wdPageNumberStyleHanjaReadDigit = 42;
	public static final int wdPageNumberStyleTradChinNum1 = 33;
	public static final int wdPageNumberStyleTradChinNum2 = 34;
	public static final int wdPageNumberStyleSimpChinNum1 = 37;
	public static final int wdPageNumberStyleSimpChinNum2 = 38;
}
