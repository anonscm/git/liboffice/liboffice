/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Find extends Dispatch {

	public static final String componentName = "Word.Find";

	public Find() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Find(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Find(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getForward() {
		return Dispatch.get(this, "Forward").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setForward(boolean lastParam) {
		//Dispatch.call(this, "Forward", new Variant(lastParam));
		Dispatch.invokeSubv(this, "Forward", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Font
	 */
	public Font getFont() {
		return new Font(Dispatch.get(this, "Font").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Font
	 */
	public void setFont(Font lastParam) {
		Dispatch.call(this, "Font", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getFound() {
		return Dispatch.get(this, "Found").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchAllWordForms() {
		return Dispatch.get(this, "MatchAllWordForms").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchAllWordForms(boolean lastParam) {
		//Dispatch.call(this, "MatchAllWordForms", new Variant(lastParam));
		Dispatch.invokeSubv(this, "MatchWildcards", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchCase() {
		return Dispatch.get(this, "MatchCase").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchCase(boolean lastParam) {
		//Dispatch.call(this, "MatchCase", new Variant(lastParam));
		Dispatch.invokeSubv(this, "MatchCase", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchWildcards() {
		return Dispatch.get(this, "MatchWildcards").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchWildcards(boolean lastParam) {
		//Dispatch.call(this, "MatchWildcards", new Variant(lastParam));
		Dispatch.invokeSubv(this, "MatchWildcards", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchSoundsLike() {
		return Dispatch.get(this, "MatchSoundsLike").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchSoundsLike(boolean lastParam) {
		//Dispatch.call(this, "MatchSoundsLike", new Variant(lastParam));
		Dispatch.invokeSubv(this, "MatchSoundsLike", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchWholeWord() {
		return Dispatch.get(this, "MatchWholeWord").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchWholeWord(boolean lastParam) {
		//Dispatch.call(this, "MatchWholeWord", new Variant(lastParam));
		Dispatch.invokeSubv(this, "MatchWholeWord", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchFuzzy() {
		return Dispatch.get(this, "MatchFuzzy").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchFuzzy(boolean lastParam) {
		//Dispatch.call(this, "MatchFuzzy", new Variant(lastParam));
		Dispatch.invokeSubv(this, "MatchFuzzy", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMatchByte() {
		return Dispatch.get(this, "MatchByte").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setMatchByte(boolean lastParam) {
		//Dispatch.call(this, "MatchByte", new Variant(lastParam));
		Dispatch.invokeSubv(this, "MatchByte", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ParagraphFormat
	 */
	public ParagraphFormat getParagraphFormat() {
		return new ParagraphFormat(Dispatch.get(this, "ParagraphFormat").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type ParagraphFormat
	 */
	public void setParagraphFormat(ParagraphFormat lastParam) {
		Dispatch.call(this, "ParagraphFormat", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant getStyle() {
		return Dispatch.get(this, "Style");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void setStyle(Variant lastParam) {
		//Dispatch.call(this, "Style", lastParam);
		Dispatch.invokeSubv(this, "Style", Dispatch.Put, new Variant[] { lastParam }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getText() {
		return Dispatch.get(this, "Text").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setText(String lastParam) {
		//Dispatch.call(this, "Text", lastParam);
		Dispatch.invokeSubv(this, "Text", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLanguageID() {
		return Dispatch.get(this, "LanguageID").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLanguageID(int lastParam) {
		Dispatch.call(this, "LanguageID", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHighlight() {
		return Dispatch.get(this, "Highlight").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHighlight(int lastParam) {
		Dispatch.call(this, "Highlight", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Replacement
	 */
	public Replacement getReplacement() {
		return new Replacement(Dispatch.get(this, "Replacement").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Frame
	 */
	public Frame getFrame() {
		return new Frame(Dispatch.get(this, "Frame").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWrap() {
		return Dispatch.get(this, "Wrap").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setWrap(int lastParam) {
		//Dispatch.call(this, "Wrap", new Variant(lastParam));
		Dispatch.invokeSubv(this, "Wrap", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getFormat() {
		return Dispatch.get(this, "Format").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setFormat(boolean lastParam) {
		//Dispatch.call(this, "Format", new Variant(lastParam));
		Dispatch.invokeSubv(this, "Format", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLanguageIDFarEast() {
		return Dispatch.get(this, "LanguageIDFarEast").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLanguageIDFarEast(int lastParam) {
		Dispatch.call(this, "LanguageIDFarEast", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLanguageIDOther() {
		return Dispatch.get(this, "LanguageIDOther").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLanguageIDOther(int lastParam) {
		Dispatch.call(this, "LanguageIDOther", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCorrectHangulEndings() {
		return Dispatch.get(this, "CorrectHangulEndings").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setCorrectHangulEndings(boolean lastParam) {
		Dispatch.call(this, "CorrectHangulEndings", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @param matchSoundsLike an input-parameter of type Variant
	 * @param matchAllWordForms an input-parameter of type Variant
	 * @param forward an input-parameter of type Variant
	 * @param wrap an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param replaceWith an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards, Variant matchSoundsLike, Variant matchAllWordForms, Variant forward, Variant wrap, Variant format, Variant replaceWith, Variant lastParam) {
		return Dispatch.callN(this, "Execute", new Object[] { findText, matchCase, matchWholeWord, matchWildcards, matchSoundsLike, matchAllWordForms, forward, wrap, format, replaceWith, lastParam}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @param matchSoundsLike an input-parameter of type Variant
	 * @param matchAllWordForms an input-parameter of type Variant
	 * @param forward an input-parameter of type Variant
	 * @param wrap an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @param replaceWith an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards, Variant matchSoundsLike, Variant matchAllWordForms, Variant forward, Variant wrap, Variant format, Variant replaceWith) {
		return Dispatch.callN(this, "Execute", new Object[] { findText, matchCase, matchWholeWord, matchWildcards, matchSoundsLike, matchAllWordForms, forward, wrap, format, replaceWith}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @param matchSoundsLike an input-parameter of type Variant
	 * @param matchAllWordForms an input-parameter of type Variant
	 * @param forward an input-parameter of type Variant
	 * @param wrap an input-parameter of type Variant
	 * @param format an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards, Variant matchSoundsLike, Variant matchAllWordForms, Variant forward, Variant wrap, Variant format) {
		return Dispatch.callN(this, "Execute", new Object[] { findText, matchCase, matchWholeWord, matchWildcards, matchSoundsLike, matchAllWordForms, forward, wrap, format}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @param matchSoundsLike an input-parameter of type Variant
	 * @param matchAllWordForms an input-parameter of type Variant
	 * @param forward an input-parameter of type Variant
	 * @param wrap an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards, Variant matchSoundsLike, Variant matchAllWordForms, Variant forward, Variant wrap) {
		return Dispatch.call(this, "Execute", findText, matchCase, matchWholeWord, matchWildcards, matchSoundsLike, matchAllWordForms, forward, wrap).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @param matchSoundsLike an input-parameter of type Variant
	 * @param matchAllWordForms an input-parameter of type Variant
	 * @param forward an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards, Variant matchSoundsLike, Variant matchAllWordForms, Variant forward) {
		return Dispatch.call(this, "Execute", findText, matchCase, matchWholeWord, matchWildcards, matchSoundsLike, matchAllWordForms, forward).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @param matchSoundsLike an input-parameter of type Variant
	 * @param matchAllWordForms an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards, Variant matchSoundsLike, Variant matchAllWordForms) {
		return Dispatch.call(this, "Execute", findText, matchCase, matchWholeWord, matchWildcards, matchSoundsLike, matchAllWordForms).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @param matchSoundsLike an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards, Variant matchSoundsLike) {
		return Dispatch.call(this, "Execute", findText, matchCase, matchWholeWord, matchWildcards, matchSoundsLike).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @param matchWildcards an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord, Variant matchWildcards) {
		return Dispatch.call(this, "Execute", findText, matchCase, matchWholeWord, matchWildcards).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @param matchWholeWord an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase, Variant matchWholeWord) {
		return Dispatch.call(this, "Execute", findText, matchCase, matchWholeWord).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @param matchCase an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText, Variant matchCase) {
		return Dispatch.call(this, "Execute", findText, matchCase).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param findText an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean execute(Variant findText) {
		return Dispatch.call(this, "Execute", findText).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean execute() {
		return Dispatch.call(this, "Execute").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void clearFormatting() {
		Dispatch.call(this, "ClearFormatting");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void setAllFuzzyOptions() {
		Dispatch.call(this, "SetAllFuzzyOptions");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void clearAllFuzzyOptions() {
		Dispatch.call(this, "ClearAllFuzzyOptions");
	}

}
