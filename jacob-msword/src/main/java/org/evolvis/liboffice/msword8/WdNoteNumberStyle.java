/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdNoteNumberStyle {

	public static final int wdNoteNumberStyleArabic = 0;
	public static final int wdNoteNumberStyleUppercaseRoman = 1;
	public static final int wdNoteNumberStyleLowercaseRoman = 2;
	public static final int wdNoteNumberStyleUppercaseLetter = 3;
	public static final int wdNoteNumberStyleLowercaseLetter = 4;
	public static final int wdNoteNumberStyleSymbol = 9;
}
