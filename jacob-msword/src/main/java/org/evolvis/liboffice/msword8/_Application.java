/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class _Application extends Dispatch {

	public static final String componentName = "Word._Application";

	public _Application() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public _Application(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public _Application(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getName() {
		return Dispatch.get(this, "Name").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Documents
	 */
	public Documents getDocuments() {
		return new Documents(Dispatch.get(this, "Documents").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Windows
	 */
	public Windows getWindows() {
		return new Windows(Dispatch.get(this, "Windows").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Document
	 */
	public Document getActiveDocument() {
		return new Document(Dispatch.get(this, "ActiveDocument").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window getActiveWindow() {
		return new Window(Dispatch.get(this, "ActiveWindow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Selection
	 */
	public Selection getSelection() {
		return new Selection(Dispatch.get(this, "Selection").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getWordBasic() {
		return Dispatch.get(this, "WordBasic");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type RecentFiles
	 */
	public RecentFiles getRecentFiles() {
		return new RecentFiles(Dispatch.get(this, "RecentFiles").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Template
	 */
	public Template getNormalTemplate() {
		return new Template(Dispatch.get(this, "NormalTemplate").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type System
	 */
	public System getSystem() {
		return new System(Dispatch.get(this, "System").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type AutoCorrect
	 */
	public AutoCorrect getAutoCorrect() {
		return new AutoCorrect(Dispatch.get(this, "AutoCorrect").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FontNames
	 */
	public FontNames getFontNames() {
		return new FontNames(Dispatch.get(this, "FontNames").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FontNames
	 */
	public FontNames getLandscapeFontNames() {
		return new FontNames(Dispatch.get(this, "LandscapeFontNames").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FontNames
	 */
	public FontNames getPortraitFontNames() {
		return new FontNames(Dispatch.get(this, "PortraitFontNames").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Languages
	 */
	public Languages getLanguages() {
		return new Languages(Dispatch.get(this, "Languages").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Assistant
	 */
	/*
	public Assistant getAssistant() {
		return new Assistant(Dispatch.get(this, "Assistant").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Browser
	 */
	public Browser getBrowser() {
		return new Browser(Dispatch.get(this, "Browser").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FileConverters
	 */
	public FileConverters getFileConverters() {
		return new FileConverters(Dispatch.get(this, "FileConverters").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MailingLabel
	 */
	public MailingLabel getMailingLabel() {
		return new MailingLabel(Dispatch.get(this, "MailingLabel").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Dialogs
	 */
	public Dialogs getDialogs() {
		return new Dialogs(Dispatch.get(this, "Dialogs").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type CaptionLabels
	 */
	public CaptionLabels getCaptionLabels() {
		return new CaptionLabels(Dispatch.get(this, "CaptionLabels").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type AutoCaptions
	 */
	public AutoCaptions getAutoCaptions() {
		return new AutoCaptions(Dispatch.get(this, "AutoCaptions").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type AddIns
	 */
	public AddIns getAddIns() {
		return new AddIns(Dispatch.get(this, "AddIns").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getVisible() {
		return Dispatch.get(this, "Visible").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setVisible(boolean lastParam) {
		//Dispatch.call(this, "Visible", new Variant(lastParam));
		Dispatch.invokeSubv(this, "Visible", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getVersion() {
		return Dispatch.get(this, "Version").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getScreenUpdating() {
		return Dispatch.get(this, "ScreenUpdating").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setScreenUpdating(boolean lastParam) {
		Dispatch.call(this, "ScreenUpdating", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintPreview() {
		return Dispatch.get(this, "PrintPreview").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintPreview(boolean lastParam) {
		Dispatch.call(this, "PrintPreview", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Tasks
	 */
	public Tasks getTasks() {
		return new Tasks(Dispatch.get(this, "Tasks").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayStatusBar() {
		return Dispatch.get(this, "DisplayStatusBar").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayStatusBar(boolean lastParam) {
		Dispatch.call(this, "DisplayStatusBar", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSpecialMode() {
		return Dispatch.get(this, "SpecialMode").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getUsableWidth() {
		return Dispatch.get(this, "UsableWidth").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getUsableHeight() {
		return Dispatch.get(this, "UsableHeight").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMathCoprocessorAvailable() {
		return Dispatch.get(this, "MathCoprocessorAvailable").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMouseAvailable() {
		return Dispatch.get(this, "MouseAvailable").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type Variant
	 */
	public Variant getInternational(int lastParam) {
		return Dispatch.call(this, "International", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getBuild() {
		return Dispatch.get(this, "Build").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getCapsLock() {
		return Dispatch.get(this, "CapsLock").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getNumLock() {
		return Dispatch.get(this, "NumLock").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getUserName() {
		return Dispatch.get(this, "UserName").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setUserName(String lastParam) {
		Dispatch.call(this, "UserName", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getUserInitials() {
		return Dispatch.get(this, "UserInitials").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setUserInitials(String lastParam) {
		Dispatch.call(this, "UserInitials", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getUserAddress() {
		return Dispatch.get(this, "UserAddress").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setUserAddress(String lastParam) {
		Dispatch.call(this, "UserAddress", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getMacroContainer() {
		return Dispatch.get(this, "MacroContainer");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayRecentFiles() {
		return Dispatch.get(this, "DisplayRecentFiles").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayRecentFiles(boolean lastParam) {
		Dispatch.call(this, "DisplayRecentFiles", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type _CommandBars
	 */
	/*
	public _CommandBars getCommandBars() {
		return new _CommandBars(Dispatch.get(this, "CommandBars").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type SynonymInfo
	 */
	public SynonymInfo getSynonymInfo(String word, Variant lastParam) {
		return new SynonymInfo(Dispatch.call(this, "SynonymInfo", word, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @return the result is of type SynonymInfo
	 */
	public SynonymInfo getSynonymInfo(String word) {
		return new SynonymInfo(Dispatch.call(this, "SynonymInfo", word).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type VBE
	 */
	public VBE getVBE() {
		return new VBE(Dispatch.get(this, "VBE").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getDefaultSaveFormat() {
		return Dispatch.get(this, "DefaultSaveFormat").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setDefaultSaveFormat(String lastParam) {
		Dispatch.call(this, "DefaultSaveFormat", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ListGalleries
	 */
	public ListGalleries getListGalleries() {
		return new ListGalleries(Dispatch.get(this, "ListGalleries").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getActivePrinter() {
		return Dispatch.get(this, "ActivePrinter").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setActivePrinter(String lastParam) {
		//Dispatch.call(this, "ActivePrinter", lastParam);
		Dispatch.invokeSubv(this, "ActivePrinter", Dispatch.Put, new Variant[] { new Variant(lastParam) }, new int[0] );
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Templates
	 */
	public Templates getTemplates() {
		return new Templates(Dispatch.get(this, "Templates").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getCustomizationContext() {
		return Dispatch.get(this, "CustomizationContext");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Object
	 */
	public void setCustomizationContext(Object lastParam) {
		Dispatch.call(this, "CustomizationContext", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type KeyBindings
	 */
	public KeyBindings getKeyBindings() {
		return new KeyBindings(Dispatch.get(this, "KeyBindings").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCategory an input-parameter of type int
	 * @param command an input-parameter of type String
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type KeysBoundTo
	 */
	public KeysBoundTo getKeysBoundTo(int keyCategory, String command, Variant lastParam) {
		return new KeysBoundTo(Dispatch.call(this, "KeysBoundTo", new Variant(keyCategory), command, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCategory an input-parameter of type int
	 * @param command an input-parameter of type String
	 * @return the result is of type KeysBoundTo
	 */
	public KeysBoundTo getKeysBoundTo(int keyCategory, String command) {
		return new KeysBoundTo(Dispatch.call(this, "KeysBoundTo", new Variant(keyCategory), command).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type KeyBinding
	 */
	public KeyBinding getFindKey(int keyCode, Variant lastParam) {
		return new KeyBinding(Dispatch.call(this, "FindKey", new Variant(keyCode), lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @return the result is of type KeyBinding
	 */
	public KeyBinding getFindKey(int keyCode) {
		return new KeyBinding(Dispatch.call(this, "FindKey", new Variant(keyCode)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getCaption() {
		return Dispatch.get(this, "Caption").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setCaption(String lastParam) {
		Dispatch.call(this, "Caption", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getPath() {
		return Dispatch.get(this, "Path").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayScrollBars() {
		return Dispatch.get(this, "DisplayScrollBars").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayScrollBars(boolean lastParam) {
		Dispatch.call(this, "DisplayScrollBars", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getStartupPath() {
		return Dispatch.get(this, "StartupPath").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setStartupPath(String lastParam) {
		Dispatch.call(this, "StartupPath", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getBackgroundSavingStatus() {
		return Dispatch.get(this, "BackgroundSavingStatus").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getBackgroundPrintingStatus() {
		return Dispatch.get(this, "BackgroundPrintingStatus").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLeft() {
		return Dispatch.get(this, "Left").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLeft(int lastParam) {
		Dispatch.call(this, "Left", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getTop() {
		return Dispatch.get(this, "Top").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setTop(int lastParam) {
		Dispatch.call(this, "Top", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWidth() {
		return Dispatch.get(this, "Width").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setWidth(int lastParam) {
		Dispatch.call(this, "Width", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHeight() {
		return Dispatch.get(this, "Height").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHeight(int lastParam) {
		Dispatch.call(this, "Height", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWindowState() {
		return Dispatch.get(this, "WindowState").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setWindowState(int lastParam) {
		Dispatch.call(this, "WindowState", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayAutoCompleteTips() {
		return Dispatch.get(this, "DisplayAutoCompleteTips").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayAutoCompleteTips(boolean lastParam) {
		Dispatch.call(this, "DisplayAutoCompleteTips", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Options
	 */
	public Options getOptions() {
		return new Options(Dispatch.get(this, "Options").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDisplayAlerts() {
		return Dispatch.get(this, "DisplayAlerts").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDisplayAlerts(int lastParam) {
		Dispatch.call(this, "DisplayAlerts", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Dictionaries
	 */
	public Dictionaries getCustomDictionaries() {
		return new Dictionaries(Dispatch.get(this, "CustomDictionaries").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getPathSeparator() {
		return Dispatch.get(this, "PathSeparator").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setStatusBar(String lastParam) {
		Dispatch.call(this, "StatusBar", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getMAPIAvailable() {
		return Dispatch.get(this, "MAPIAvailable").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayScreenTips() {
		return Dispatch.get(this, "DisplayScreenTips").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayScreenTips(boolean lastParam) {
		Dispatch.call(this, "DisplayScreenTips", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getEnableCancelKey() {
		return Dispatch.get(this, "EnableCancelKey").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setEnableCancelKey(int lastParam) {
		Dispatch.call(this, "EnableCancelKey", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getUserControl() {
		return Dispatch.get(this, "UserControl").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FileSearch
	 */
	/*
	public FileSearch getFileSearch() {
		return new FileSearch(Dispatch.get(this, "FileSearch").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getMailSystem() {
		return Dispatch.get(this, "MailSystem").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getDefaultTableSeparator() {
		return Dispatch.get(this, "DefaultTableSeparator").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setDefaultTableSeparator(String lastParam) {
		Dispatch.call(this, "DefaultTableSeparator", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getShowVisualBasicEditor() {
		return Dispatch.get(this, "ShowVisualBasicEditor").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setShowVisualBasicEditor(boolean lastParam) {
		Dispatch.call(this, "ShowVisualBasicEditor", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getBrowseExtraFileTypes() {
		return Dispatch.get(this, "BrowseExtraFileTypes").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setBrowseExtraFileTypes(String lastParam) {
		Dispatch.call(this, "BrowseExtraFileTypes", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Object
	 * @return the result is of type boolean
	 */
	public boolean getIsObjectValid(Object lastParam) {
		return Dispatch.call(this, "IsObjectValid", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type HangulHanjaConversionDictionaries
	 */
	public HangulHanjaConversionDictionaries getHangulHanjaDictionaries() {
		return new HangulHanjaConversionDictionaries(Dispatch.get(this, "HangulHanjaDictionaries").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MailMessage
	 */
	public MailMessage getMailMessage() {
		return new MailMessage(Dispatch.get(this, "MailMessage").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getFocusInMailHeader() {
		return Dispatch.get(this, "FocusInMailHeader").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param saveChanges an input-parameter of type Variant
	 * @param originalFormat an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void quit(Variant saveChanges, Variant originalFormat, Variant lastParam) {
		Dispatch.call(this, "Quit", saveChanges, originalFormat, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param saveChanges an input-parameter of type Variant
	 * @param originalFormat an input-parameter of type Variant
	 */
	public void quit(Variant saveChanges, Variant originalFormat) {
		Dispatch.call(this, "Quit", saveChanges, originalFormat);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param saveChanges an input-parameter of type Variant
	 */
	public void quit(Variant saveChanges) {
		Dispatch.call(this, "Quit", saveChanges);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void quit() {
		Dispatch.call(this, "Quit");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void screenRefresh() {
		Dispatch.call(this, "ScreenRefresh");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 * @param collate an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param activePrinterMacGX an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile, Variant collate, Variant fileName, Variant activePrinterMacGX, Variant lastParam) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile, collate, fileName, activePrinterMacGX, lastParam});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 * @param collate an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param activePrinterMacGX an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile, Variant collate, Variant fileName, Variant activePrinterMacGX) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile, collate, fileName, activePrinterMacGX});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 * @param collate an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile, Variant collate, Variant fileName) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile, collate, fileName});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 * @param collate an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile, Variant collate) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile, collate});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from, to, item, copies);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from, to, item);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from, to);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range) {
		Dispatch.call(this, "PrintOut", background, append, range);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append) {
		Dispatch.call(this, "PrintOut", background, append);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 */
	public void printOut(Variant background) {
		Dispatch.call(this, "PrintOut", background);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void printOut() {
		Dispatch.call(this, "PrintOut");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void lookupNameProperties(String lastParam) {
		Dispatch.call(this, "LookupNameProperties", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param unavailableFont an input-parameter of type String
	 * @param lastParam an input-parameter of type String
	 */
	public void substituteFont(String unavailableFont, String lastParam) {
		Dispatch.call(this, "SubstituteFont", unavailableFont, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean repeat(Variant lastParam) {
		return Dispatch.call(this, "Repeat", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean repeat() {
		return Dispatch.call(this, "Repeat").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param channel an input-parameter of type int
	 * @param lastParam an input-parameter of type String
	 */
	public void dDEExecute(int channel, String lastParam) {
		Dispatch.call(this, "DDEExecute", new Variant(channel), lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param app an input-parameter of type String
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type int
	 */
	public int dDEInitiate(String app, String lastParam) {
		return Dispatch.call(this, "DDEInitiate", app, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param channel an input-parameter of type int
	 * @param item an input-parameter of type String
	 * @param lastParam an input-parameter of type String
	 */
	public void dDEPoke(int channel, String item, String lastParam) {
		Dispatch.call(this, "DDEPoke", new Variant(channel), item, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param channel an input-parameter of type int
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type String
	 */
	public String dDERequest(int channel, String lastParam) {
		return Dispatch.call(this, "DDERequest", new Variant(channel), lastParam).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void dDETerminate(int lastParam) {
		Dispatch.call(this, "DDETerminate", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void dDETerminateAll() {
		Dispatch.call(this, "DDETerminateAll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @param arg2 an input-parameter of type Variant
	 * @param arg3 an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1, Variant arg2, Variant arg3, Variant lastParam) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1), arg2, arg3, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @param arg2 an input-parameter of type Variant
	 * @param arg3 an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1, Variant arg2, Variant arg3) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1), arg2, arg3).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @param arg2 an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1, Variant arg2) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1), arg2).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1)).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String keyString(int keyCode, Variant lastParam) {
		return Dispatch.call(this, "KeyString", new Variant(keyCode), lastParam).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @return the result is of type String
	 */
	public String keyString(int keyCode) {
		return Dispatch.call(this, "KeyString", new Variant(keyCode)).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param source an input-parameter of type String
	 * @param destination an input-parameter of type String
	 * @param name an input-parameter of type String
	 * @param lastParam an input-parameter of type int
	 */
	public void organizerCopy(String source, String destination, String name, int lastParam) {
		Dispatch.call(this, "OrganizerCopy", source, destination, name, new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param source an input-parameter of type String
	 * @param name an input-parameter of type String
	 * @param lastParam an input-parameter of type int
	 */
	public void organizerDelete(String source, String name, int lastParam) {
		Dispatch.call(this, "OrganizerDelete", source, name, new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param source an input-parameter of type String
	 * @param name an input-parameter of type String
	 * @param newName an input-parameter of type String
	 * @param lastParam an input-parameter of type int
	 */
	public void organizerRename(String source, String name, String newName, int lastParam) {
		Dispatch.call(this, "OrganizerRename", source, name, newName, new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param tagID an input-parameter of type SafeArray
	 * @param lastParam an input-parameter of type SafeArray
	 */
	public void addAddress(SafeArray tagID, SafeArray lastParam) {
		Dispatch.call(this, "AddAddress", tagID, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @param addressProperties an input-parameter of type Variant
	 * @param useAutoText an input-parameter of type Variant
	 * @param displaySelectDialog an input-parameter of type Variant
	 * @param selectDialog an input-parameter of type Variant
	 * @param checkNamesDialog an input-parameter of type Variant
	 * @param recentAddressesChoice an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name, Variant addressProperties, Variant useAutoText, Variant displaySelectDialog, Variant selectDialog, Variant checkNamesDialog, Variant recentAddressesChoice, Variant lastParam) {
		return Dispatch.call(this, "GetAddress", name, addressProperties, useAutoText, displaySelectDialog, selectDialog, checkNamesDialog, recentAddressesChoice, lastParam).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @param addressProperties an input-parameter of type Variant
	 * @param useAutoText an input-parameter of type Variant
	 * @param displaySelectDialog an input-parameter of type Variant
	 * @param selectDialog an input-parameter of type Variant
	 * @param checkNamesDialog an input-parameter of type Variant
	 * @param recentAddressesChoice an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name, Variant addressProperties, Variant useAutoText, Variant displaySelectDialog, Variant selectDialog, Variant checkNamesDialog, Variant recentAddressesChoice) {
		return Dispatch.call(this, "GetAddress", name, addressProperties, useAutoText, displaySelectDialog, selectDialog, checkNamesDialog, recentAddressesChoice).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @param addressProperties an input-parameter of type Variant
	 * @param useAutoText an input-parameter of type Variant
	 * @param displaySelectDialog an input-parameter of type Variant
	 * @param selectDialog an input-parameter of type Variant
	 * @param checkNamesDialog an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name, Variant addressProperties, Variant useAutoText, Variant displaySelectDialog, Variant selectDialog, Variant checkNamesDialog) {
		return Dispatch.call(this, "GetAddress", name, addressProperties, useAutoText, displaySelectDialog, selectDialog, checkNamesDialog).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @param addressProperties an input-parameter of type Variant
	 * @param useAutoText an input-parameter of type Variant
	 * @param displaySelectDialog an input-parameter of type Variant
	 * @param selectDialog an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name, Variant addressProperties, Variant useAutoText, Variant displaySelectDialog, Variant selectDialog) {
		return Dispatch.call(this, "GetAddress", name, addressProperties, useAutoText, displaySelectDialog, selectDialog).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @param addressProperties an input-parameter of type Variant
	 * @param useAutoText an input-parameter of type Variant
	 * @param displaySelectDialog an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name, Variant addressProperties, Variant useAutoText, Variant displaySelectDialog) {
		return Dispatch.call(this, "GetAddress", name, addressProperties, useAutoText, displaySelectDialog).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @param addressProperties an input-parameter of type Variant
	 * @param useAutoText an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name, Variant addressProperties, Variant useAutoText) {
		return Dispatch.call(this, "GetAddress", name, addressProperties, useAutoText).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @param addressProperties an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name, Variant addressProperties) {
		return Dispatch.call(this, "GetAddress", name, addressProperties).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param name an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String getAddress(Variant name) {
		return Dispatch.call(this, "GetAddress", name).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getAddress() {
		return Dispatch.call(this, "GetAddress").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type boolean
	 */
	public boolean checkGrammar(String lastParam) {
		return Dispatch.call(this, "CheckGrammar", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9, Variant lastParam) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9, lastParam}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word) {
		return Dispatch.call(this, "CheckSpelling", word).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void resetIgnoreAll() {
		Dispatch.call(this, "ResetIgnoreAll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9, Variant lastParam) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9, lastParam}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void goBack() {
		Dispatch.call(this, "GoBack");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void help(Variant lastParam) {
		Dispatch.call(this, "Help", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void automaticChange() {
		Dispatch.call(this, "AutomaticChange");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void showMe() {
		Dispatch.call(this, "ShowMe");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void helpTool() {
		Dispatch.call(this, "HelpTool");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window newWindow() {
		return new Window(Dispatch.call(this, "NewWindow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void listCommands(boolean lastParam) {
		Dispatch.call(this, "ListCommands", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void showClipboard() {
		Dispatch.call(this, "ShowClipboard");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param when an input-parameter of type Variant
	 * @param name an input-parameter of type String
	 * @param lastParam an input-parameter of type Variant
	 */
	public void onTime(Variant when, String name, Variant lastParam) {
		Dispatch.call(this, "OnTime", when, name, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param when an input-parameter of type Variant
	 * @param name an input-parameter of type String
	 */
	public void onTime(Variant when, String name) {
		Dispatch.call(this, "OnTime", when, name);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void nextLetter() {
		Dispatch.call(this, "NextLetter");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param zone an input-parameter of type String
	 * @param server an input-parameter of type String
	 * @param volume an input-parameter of type String
	 * @param user an input-parameter of type Variant
	 * @param userPassword an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type short
	 */
	public short mountVolume(String zone, String server, String volume, Variant user, Variant userPassword, Variant lastParam) {
		return Dispatch.call(this, "MountVolume", zone, server, volume, user, userPassword, lastParam).toShort();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param zone an input-parameter of type String
	 * @param server an input-parameter of type String
	 * @param volume an input-parameter of type String
	 * @param user an input-parameter of type Variant
	 * @param userPassword an input-parameter of type Variant
	 * @return the result is of type short
	 */
	public short mountVolume(String zone, String server, String volume, Variant user, Variant userPassword) {
		return Dispatch.call(this, "MountVolume", zone, server, volume, user, userPassword).toShort();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param zone an input-parameter of type String
	 * @param server an input-parameter of type String
	 * @param volume an input-parameter of type String
	 * @param user an input-parameter of type Variant
	 * @return the result is of type short
	 */
	public short mountVolume(String zone, String server, String volume, Variant user) {
		return Dispatch.call(this, "MountVolume", zone, server, volume, user).toShort();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param zone an input-parameter of type String
	 * @param server an input-parameter of type String
	 * @param volume an input-parameter of type String
	 * @return the result is of type short
	 */
	public short mountVolume(String zone, String server, String volume) {
		return Dispatch.call(this, "MountVolume", zone, server, volume).toShort();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type String
	 */
	public String cleanString(String lastParam) {
		return Dispatch.call(this, "CleanString", lastParam).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void sendFax() {
		Dispatch.call(this, "SendFax");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void changeFileOpenDirectory(String lastParam) {
		Dispatch.call(this, "ChangeFileOpenDirectory", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void run(String lastParam) {
		Dispatch.call(this, "Run", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void goForward() {
		Dispatch.call(this, "GoForward");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param left an input-parameter of type int
	 * @param lastParam an input-parameter of type int
	 */
	public void move(int left, int lastParam) {
		Dispatch.call(this, "Move", new Variant(left), new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param width an input-parameter of type int
	 * @param lastParam an input-parameter of type int
	 */
	public void resize(int width, int lastParam) {
		Dispatch.call(this, "Resize", new Variant(width), new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float inchesToPoints(float lastParam) {
		return Dispatch.call(this, "InchesToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float centimetersToPoints(float lastParam) {
		return Dispatch.call(this, "CentimetersToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float millimetersToPoints(float lastParam) {
		return Dispatch.call(this, "MillimetersToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float picasToPoints(float lastParam) {
		return Dispatch.call(this, "PicasToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float linesToPoints(float lastParam) {
		return Dispatch.call(this, "LinesToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToInches(float lastParam) {
		return Dispatch.call(this, "PointsToInches", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToCentimeters(float lastParam) {
		return Dispatch.call(this, "PointsToCentimeters", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToMillimeters(float lastParam) {
		return Dispatch.call(this, "PointsToMillimeters", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToPicas(float lastParam) {
		return Dispatch.call(this, "PointsToPicas", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToLines(float lastParam) {
		return Dispatch.call(this, "PointsToLines", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void activate() {
		Dispatch.call(this, "Activate");
	}

}
