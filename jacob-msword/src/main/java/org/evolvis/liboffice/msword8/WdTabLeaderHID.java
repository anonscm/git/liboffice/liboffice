/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdTabLeaderHID {

	public static final int wdTabLeaderHeavy = 4;
	public static final int wdTabLeaderMiddleDot = 5;
}
