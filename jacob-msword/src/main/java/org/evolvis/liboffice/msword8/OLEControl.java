/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class OLEControl extends _OLEControl {

	public static final String componentName = "Word.OLEControl";

	public OLEControl() {
		super(componentName);
	}

	public OLEControl(Dispatch d) {
		super(d);
	}
}
