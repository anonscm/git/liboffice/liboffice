/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class InlineShapes extends Dispatch {

	public static final String componentName = "Word.InlineShapes";

	public InlineShapes() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public InlineShapes(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public InlineShapes(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCount() {
		return Dispatch.get(this, "Count").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant get_NewEnum() {
		return Dispatch.get(this, "_NewEnum");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type InlineShape
	 */
	public InlineShape item(int lastParam) {
		return new InlineShape(Dispatch.call(this, "Item", new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @param linkToFile an input-parameter of type Variant
	 * @param saveWithDocument an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addPicture(String fileName, Variant linkToFile, Variant saveWithDocument, Variant lastParam) {
		return new InlineShape(Dispatch.call(this, "AddPicture", fileName, linkToFile, saveWithDocument, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @param linkToFile an input-parameter of type Variant
	 * @param saveWithDocument an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addPicture(String fileName, Variant linkToFile, Variant saveWithDocument) {
		return new InlineShape(Dispatch.call(this, "AddPicture", fileName, linkToFile, saveWithDocument).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @param linkToFile an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addPicture(String fileName, Variant linkToFile) {
		return new InlineShape(Dispatch.call(this, "AddPicture", fileName, linkToFile).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param fileName an input-parameter of type String
	 * @return the result is of type InlineShape
	 */
	public InlineShape addPicture(String fileName) {
		return new InlineShape(Dispatch.call(this, "AddPicture", fileName).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param linkToFile an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 * @param iconIndex an input-parameter of type Variant
	 * @param iconLabel an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType, Variant fileName, Variant linkToFile, Variant displayAsIcon, Variant iconFileName, Variant iconIndex, Variant iconLabel, Variant lastParam) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType, fileName, linkToFile, displayAsIcon, iconFileName, iconIndex, iconLabel, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param linkToFile an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 * @param iconIndex an input-parameter of type Variant
	 * @param iconLabel an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType, Variant fileName, Variant linkToFile, Variant displayAsIcon, Variant iconFileName, Variant iconIndex, Variant iconLabel) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType, fileName, linkToFile, displayAsIcon, iconFileName, iconIndex, iconLabel).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param linkToFile an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 * @param iconIndex an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType, Variant fileName, Variant linkToFile, Variant displayAsIcon, Variant iconFileName, Variant iconIndex) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType, fileName, linkToFile, displayAsIcon, iconFileName, iconIndex).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param linkToFile an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType, Variant fileName, Variant linkToFile, Variant displayAsIcon, Variant iconFileName) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType, fileName, linkToFile, displayAsIcon, iconFileName).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param linkToFile an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType, Variant fileName, Variant linkToFile, Variant displayAsIcon) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType, fileName, linkToFile, displayAsIcon).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @param linkToFile an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType, Variant fileName, Variant linkToFile) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType, fileName, linkToFile).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param fileName an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType, Variant fileName) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType, fileName).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject(Variant classType) {
		return new InlineShape(Dispatch.call(this, "AddOLEObject", classType).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEObject() {
		return new InlineShape(Dispatch.call(this, "AddOLEObject").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEControl(Variant classType, Variant lastParam) {
		return new InlineShape(Dispatch.call(this, "AddOLEControl", classType, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEControl(Variant classType) {
		return new InlineShape(Dispatch.call(this, "AddOLEControl", classType).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type InlineShape
	 */
	public InlineShape addOLEControl() {
		return new InlineShape(Dispatch.call(this, "AddOLEControl").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 * @return the result is of type InlineShape
	 */
	public InlineShape m_new(Range lastParam) {
		return new InlineShape(Dispatch.call(this, "New", lastParam).toDispatch());
	}

}
