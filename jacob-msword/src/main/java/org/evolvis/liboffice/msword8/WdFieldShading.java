/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdFieldShading {

	public static final int wdFieldShadingNever = 0;
	public static final int wdFieldShadingAlways = 1;
	public static final int wdFieldShadingWhenSelected = 2;
}
