/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class TablesOfContents extends Dispatch {

	public static final String componentName = "Word.TablesOfContents";

	public TablesOfContents() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public TablesOfContents(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public TablesOfContents(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant get_NewEnum() {
		return Dispatch.get(this, "_NewEnum");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCount() {
		return Dispatch.get(this, "Count").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getFormat() {
		return Dispatch.get(this, "Format").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setFormat(int lastParam) {
		Dispatch.call(this, "Format", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents item(int lastParam) {
		return new TableOfContents(Dispatch.call(this, "Item", new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @param upperHeadingLevel an input-parameter of type Variant
	 * @param lowerHeadingLevel an input-parameter of type Variant
	 * @param useFields an input-parameter of type Variant
	 * @param tableID an input-parameter of type Variant
	 * @param rightAlignPageNumbers an input-parameter of type Variant
	 * @param includePageNumbers an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles, Variant upperHeadingLevel, Variant lowerHeadingLevel, Variant useFields, Variant tableID, Variant rightAlignPageNumbers, Variant includePageNumbers, Variant lastParam) {
		return new TableOfContents(Dispatch.callN(this, "Add", new Object[] { range, useHeadingStyles, upperHeadingLevel, lowerHeadingLevel, useFields, tableID, rightAlignPageNumbers, includePageNumbers, lastParam}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @param upperHeadingLevel an input-parameter of type Variant
	 * @param lowerHeadingLevel an input-parameter of type Variant
	 * @param useFields an input-parameter of type Variant
	 * @param tableID an input-parameter of type Variant
	 * @param rightAlignPageNumbers an input-parameter of type Variant
	 * @param includePageNumbers an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles, Variant upperHeadingLevel, Variant lowerHeadingLevel, Variant useFields, Variant tableID, Variant rightAlignPageNumbers, Variant includePageNumbers) {
		return new TableOfContents(Dispatch.call(this, "Add", range, useHeadingStyles, upperHeadingLevel, lowerHeadingLevel, useFields, tableID, rightAlignPageNumbers, includePageNumbers).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @param upperHeadingLevel an input-parameter of type Variant
	 * @param lowerHeadingLevel an input-parameter of type Variant
	 * @param useFields an input-parameter of type Variant
	 * @param tableID an input-parameter of type Variant
	 * @param rightAlignPageNumbers an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles, Variant upperHeadingLevel, Variant lowerHeadingLevel, Variant useFields, Variant tableID, Variant rightAlignPageNumbers) {
		return new TableOfContents(Dispatch.call(this, "Add", range, useHeadingStyles, upperHeadingLevel, lowerHeadingLevel, useFields, tableID, rightAlignPageNumbers).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @param upperHeadingLevel an input-parameter of type Variant
	 * @param lowerHeadingLevel an input-parameter of type Variant
	 * @param useFields an input-parameter of type Variant
	 * @param tableID an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles, Variant upperHeadingLevel, Variant lowerHeadingLevel, Variant useFields, Variant tableID) {
		return new TableOfContents(Dispatch.call(this, "Add", range, useHeadingStyles, upperHeadingLevel, lowerHeadingLevel, useFields, tableID).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @param upperHeadingLevel an input-parameter of type Variant
	 * @param lowerHeadingLevel an input-parameter of type Variant
	 * @param useFields an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles, Variant upperHeadingLevel, Variant lowerHeadingLevel, Variant useFields) {
		return new TableOfContents(Dispatch.call(this, "Add", range, useHeadingStyles, upperHeadingLevel, lowerHeadingLevel, useFields).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @param upperHeadingLevel an input-parameter of type Variant
	 * @param lowerHeadingLevel an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles, Variant upperHeadingLevel, Variant lowerHeadingLevel) {
		return new TableOfContents(Dispatch.call(this, "Add", range, useHeadingStyles, upperHeadingLevel, lowerHeadingLevel).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @param upperHeadingLevel an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles, Variant upperHeadingLevel) {
		return new TableOfContents(Dispatch.call(this, "Add", range, useHeadingStyles, upperHeadingLevel).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param useHeadingStyles an input-parameter of type Variant
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range, Variant useHeadingStyles) {
		return new TableOfContents(Dispatch.call(this, "Add", range, useHeadingStyles).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @return the result is of type TableOfContents
	 */
	public TableOfContents add(Range range) {
		return new TableOfContents(Dispatch.call(this, "Add", range).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param entry an input-parameter of type Variant
	 * @param entryAutoText an input-parameter of type Variant
	 * @param tableID an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Field
	 */
	public Field markEntry(Range range, Variant entry, Variant entryAutoText, Variant tableID, Variant lastParam) {
		return new Field(Dispatch.call(this, "MarkEntry", range, entry, entryAutoText, tableID, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param entry an input-parameter of type Variant
	 * @param entryAutoText an input-parameter of type Variant
	 * @param tableID an input-parameter of type Variant
	 * @return the result is of type Field
	 */
	public Field markEntry(Range range, Variant entry, Variant entryAutoText, Variant tableID) {
		return new Field(Dispatch.call(this, "MarkEntry", range, entry, entryAutoText, tableID).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param entry an input-parameter of type Variant
	 * @param entryAutoText an input-parameter of type Variant
	 * @return the result is of type Field
	 */
	public Field markEntry(Range range, Variant entry, Variant entryAutoText) {
		return new Field(Dispatch.call(this, "MarkEntry", range, entry, entryAutoText).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param entry an input-parameter of type Variant
	 * @return the result is of type Field
	 */
	public Field markEntry(Range range, Variant entry) {
		return new Field(Dispatch.call(this, "MarkEntry", range, entry).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @return the result is of type Field
	 */
	public Field markEntry(Range range) {
		return new Field(Dispatch.call(this, "MarkEntry", range).toDispatch());
	}

}
