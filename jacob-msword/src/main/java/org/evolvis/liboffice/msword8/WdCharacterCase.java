/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdCharacterCase {

	public static final int wdNextCase = -1;
	public static final int wdLowerCase = 0;
	public static final int wdUpperCase = 1;
	public static final int wdTitleWord = 2;
	public static final int wdTitleSentence = 4;
	public static final int wdToggleCase = 5;
}
