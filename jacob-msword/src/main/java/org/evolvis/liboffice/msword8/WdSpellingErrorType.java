/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdSpellingErrorType {

	public static final int wdSpellingCorrect = 0;
	public static final int wdSpellingNotInDictionary = 1;
	public static final int wdSpellingCapitalization = 2;
}
