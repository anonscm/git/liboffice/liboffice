/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdDeletedTextMark {

	public static final int wdDeletedTextMarkHidden = 0;
	public static final int wdDeletedTextMarkStrikeThrough = 1;
	public static final int wdDeletedTextMarkCaret = 2;
	public static final int wdDeletedTextMarkPound = 3;
}
