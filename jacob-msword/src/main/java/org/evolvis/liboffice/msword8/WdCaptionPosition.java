/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdCaptionPosition {

	public static final int wdCaptionPositionAbove = 0;
	public static final int wdCaptionPositionBelow = 1;
}
