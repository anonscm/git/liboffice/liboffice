/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Hyperlinks extends Dispatch {

	public static final String componentName = "Word.Hyperlinks";

	public Hyperlinks() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Hyperlinks(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Hyperlinks(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCount() {
		return Dispatch.get(this, "Count").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant get_NewEnum() {
		return Dispatch.get(this, "_NewEnum");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Hyperlink
	 */
	public Hyperlink item(Variant lastParam) {
		return new Hyperlink(Dispatch.call(this, "Item", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param anchor an input-parameter of type Object
	 * @param address an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Hyperlink
	 */
	public Hyperlink add(Object anchor, Variant address, Variant lastParam) {
		return new Hyperlink(Dispatch.call(this, "Add", anchor, address, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param anchor an input-parameter of type Object
	 * @param address an input-parameter of type Variant
	 * @return the result is of type Hyperlink
	 */
	public Hyperlink add(Object anchor, Variant address) {
		return new Hyperlink(Dispatch.call(this, "Add", anchor, address).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param anchor an input-parameter of type Object
	 * @return the result is of type Hyperlink
	 */
	public Hyperlink add(Object anchor) {
		return new Hyperlink(Dispatch.call(this, "Add", anchor).toDispatch());
	}

}
