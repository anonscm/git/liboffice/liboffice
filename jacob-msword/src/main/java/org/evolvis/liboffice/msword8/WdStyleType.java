/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdStyleType {

	public static final int wdStyleTypeParagraph = 1;
	public static final int wdStyleTypeCharacter = 2;
}
