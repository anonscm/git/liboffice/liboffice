/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdSortFieldTypeHID {

	public static final int wdSortFieldSyllable = 3;
	public static final int wdSortFieldJapanJIS = 4;
	public static final int wdSortFieldStroke = 5;
	public static final int wdSortFieldKoreaKS = 6;
}
