/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdEditionType {

	public static final int wdPublisher = 0;
	public static final int wdSubscriber = 1;
}
