/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdMailMergeActiveRecord {

	public static final int wdNoActiveRecord = -1;
	public static final int wdNextRecord = -2;
	public static final int wdPreviousRecord = -3;
	public static final int wdFirstRecord = -4;
	public static final int wdLastRecord = -5;
}
