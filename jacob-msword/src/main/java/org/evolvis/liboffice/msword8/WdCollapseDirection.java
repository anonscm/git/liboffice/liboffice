/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdCollapseDirection {

	public static final int wdCollapseStart = 1;
	public static final int wdCollapseEnd = 0;
}
