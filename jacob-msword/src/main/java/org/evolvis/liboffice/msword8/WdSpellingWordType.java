/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdSpellingWordType {

	public static final int wdSpellword = 0;
	public static final int wdWildcard = 1;
	public static final int wdAnagram = 2;
}
