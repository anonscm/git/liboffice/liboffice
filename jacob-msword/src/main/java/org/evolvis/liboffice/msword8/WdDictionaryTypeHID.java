/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdDictionaryTypeHID {

	public static final int wdHangulHanjaConversion = 8;
	public static final int wdHangulHanjaConversionCustom = 9;
}
