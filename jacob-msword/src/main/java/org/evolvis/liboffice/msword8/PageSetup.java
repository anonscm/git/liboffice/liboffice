/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class PageSetup extends Dispatch {

	public static final String componentName = "Word.PageSetup";

	public PageSetup() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public PageSetup(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public PageSetup(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getTopMargin() {
		return Dispatch.get(this, "TopMargin").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setTopMargin(float lastParam) {
		Dispatch.call(this, "TopMargin", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getBottomMargin() {
		return Dispatch.get(this, "BottomMargin").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setBottomMargin(float lastParam) {
		Dispatch.call(this, "BottomMargin", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getLeftMargin() {
		return Dispatch.get(this, "LeftMargin").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setLeftMargin(float lastParam) {
		Dispatch.call(this, "LeftMargin", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getRightMargin() {
		return Dispatch.get(this, "RightMargin").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setRightMargin(float lastParam) {
		Dispatch.call(this, "RightMargin", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getGutter() {
		return Dispatch.get(this, "Gutter").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setGutter(float lastParam) {
		Dispatch.call(this, "Gutter", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getPageWidth() {
		return Dispatch.get(this, "PageWidth").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setPageWidth(float lastParam) {
		Dispatch.call(this, "PageWidth", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getPageHeight() {
		return Dispatch.get(this, "PageHeight").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setPageHeight(float lastParam) {
		Dispatch.call(this, "PageHeight", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getOrientation() {
		return Dispatch.get(this, "Orientation").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setOrientation(int lastParam) {
		Dispatch.call(this, "Orientation", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getFirstPageTray() {
		return Dispatch.get(this, "FirstPageTray").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setFirstPageTray(int lastParam) {
		Dispatch.call(this, "FirstPageTray", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getOtherPagesTray() {
		return Dispatch.get(this, "OtherPagesTray").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setOtherPagesTray(int lastParam) {
		Dispatch.call(this, "OtherPagesTray", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getVerticalAlignment() {
		return Dispatch.get(this, "VerticalAlignment").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setVerticalAlignment(int lastParam) {
		Dispatch.call(this, "VerticalAlignment", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getMirrorMargins() {
		return Dispatch.get(this, "MirrorMargins").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setMirrorMargins(int lastParam) {
		Dispatch.call(this, "MirrorMargins", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getHeaderDistance() {
		return Dispatch.get(this, "HeaderDistance").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setHeaderDistance(float lastParam) {
		Dispatch.call(this, "HeaderDistance", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getFooterDistance() {
		return Dispatch.get(this, "FooterDistance").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setFooterDistance(float lastParam) {
		Dispatch.call(this, "FooterDistance", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getSectionStart() {
		return Dispatch.get(this, "SectionStart").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setSectionStart(int lastParam) {
		Dispatch.call(this, "SectionStart", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getOddAndEvenPagesHeaderFooter() {
		return Dispatch.get(this, "OddAndEvenPagesHeaderFooter").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setOddAndEvenPagesHeaderFooter(int lastParam) {
		Dispatch.call(this, "OddAndEvenPagesHeaderFooter", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDifferentFirstPageHeaderFooter() {
		return Dispatch.get(this, "DifferentFirstPageHeaderFooter").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDifferentFirstPageHeaderFooter(int lastParam) {
		Dispatch.call(this, "DifferentFirstPageHeaderFooter", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getSuppressEndnotes() {
		return Dispatch.get(this, "SuppressEndnotes").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setSuppressEndnotes(int lastParam) {
		Dispatch.call(this, "SuppressEndnotes", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type LineNumbering
	 */
	public LineNumbering getLineNumbering() {
		return new LineNumbering(Dispatch.get(this, "LineNumbering").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type LineNumbering
	 */
	public void setLineNumbering(LineNumbering lastParam) {
		Dispatch.call(this, "LineNumbering", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type TextColumns
	 */
	public TextColumns getTextColumns() {
		return new TextColumns(Dispatch.get(this, "TextColumns").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type TextColumns
	 */
	public void setTextColumns(TextColumns lastParam) {
		Dispatch.call(this, "TextColumns", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getPaperSize() {
		return Dispatch.get(this, "PaperSize").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setPaperSize(int lastParam) {
		Dispatch.call(this, "PaperSize", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getTwoPagesOnOne() {
		return Dispatch.get(this, "TwoPagesOnOne").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setTwoPagesOnOne(boolean lastParam) {
		Dispatch.call(this, "TwoPagesOnOne", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getGutterOnTop() {
		return Dispatch.get(this, "GutterOnTop").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setGutterOnTop(boolean lastParam) {
		Dispatch.call(this, "GutterOnTop", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getCharsLine() {
		return Dispatch.get(this, "CharsLine").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setCharsLine(float lastParam) {
		Dispatch.call(this, "CharsLine", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getLinesPage() {
		return Dispatch.get(this, "LinesPage").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setLinesPage(float lastParam) {
		Dispatch.call(this, "LinesPage", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getShowGrid() {
		return Dispatch.get(this, "ShowGrid").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setShowGrid(boolean lastParam) {
		Dispatch.call(this, "ShowGrid", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void togglePortrait() {
		Dispatch.call(this, "TogglePortrait");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void setAsTemplateDefault() {
		Dispatch.call(this, "SetAsTemplateDefault");
	}

}
