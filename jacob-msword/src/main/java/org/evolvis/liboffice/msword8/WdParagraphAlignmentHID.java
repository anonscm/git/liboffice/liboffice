/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdParagraphAlignmentHID {

	public static final int wdAlignParagraphDistribute = 4;
}
