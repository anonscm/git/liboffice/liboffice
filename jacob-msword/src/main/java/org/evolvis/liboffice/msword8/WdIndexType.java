/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdIndexType {

	public static final int wdIndexIndent = 0;
	public static final int wdIndexRunin = 1;
}
