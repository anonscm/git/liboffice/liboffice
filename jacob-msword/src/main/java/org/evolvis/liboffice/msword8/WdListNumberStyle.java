/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdListNumberStyle {

	public static final int wdListNumberStyleArabic = 0;
	public static final int wdListNumberStyleUppercaseRoman = 1;
	public static final int wdListNumberStyleLowercaseRoman = 2;
	public static final int wdListNumberStyleUppercaseLetter = 3;
	public static final int wdListNumberStyleLowercaseLetter = 4;
	public static final int wdListNumberStyleOrdinal = 5;
	public static final int wdListNumberStyleCardinalText = 6;
	public static final int wdListNumberStyleOrdinalText = 7;
	public static final int wdListNumberStyleArabicLZ = 22;
	public static final int wdListNumberStyleBullet = 23;
	public static final int wdListNumberStyleLegal = 253;
	public static final int wdListNumberStyleLegalLZ = 254;
	public static final int wdListNumberStyleNone = 255;
}
