/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class OLEFormat extends Dispatch {

	public static final String componentName = "Word.OLEFormat";

	public OLEFormat() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public OLEFormat(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public OLEFormat(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getClassType() {
		return Dispatch.get(this, "ClassType").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setClassType(String lastParam) {
		Dispatch.call(this, "ClassType", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayAsIcon() {
		return Dispatch.get(this, "DisplayAsIcon").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayAsIcon(boolean lastParam) {
		Dispatch.call(this, "DisplayAsIcon", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getIconName() {
		return Dispatch.get(this, "IconName").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setIconName(String lastParam) {
		Dispatch.call(this, "IconName", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getIconPath() {
		return Dispatch.get(this, "IconPath").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getIconIndex() {
		return Dispatch.get(this, "IconIndex").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setIconIndex(int lastParam) {
		Dispatch.call(this, "IconIndex", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getIconLabel() {
		return Dispatch.get(this, "IconLabel").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setIconLabel(String lastParam) {
		Dispatch.call(this, "IconLabel", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getLabel() {
		return Dispatch.get(this, "Label").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getObject() {
		return Dispatch.get(this, "Object");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getProgID() {
		return Dispatch.get(this, "ProgID").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void activate() {
		Dispatch.call(this, "Activate");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void edit() {
		Dispatch.call(this, "Edit");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void open() {
		Dispatch.call(this, "Open");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void doVerb(Variant lastParam) {
		Dispatch.call(this, "DoVerb", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void doVerb() {
		Dispatch.call(this, "DoVerb");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 * @param iconIndex an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void convertTo(Variant classType, Variant displayAsIcon, Variant iconFileName, Variant iconIndex, Variant lastParam) {
		Dispatch.call(this, "ConvertTo", classType, displayAsIcon, iconFileName, iconIndex, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 * @param iconIndex an input-parameter of type Variant
	 */
	public void convertTo(Variant classType, Variant displayAsIcon, Variant iconFileName, Variant iconIndex) {
		Dispatch.call(this, "ConvertTo", classType, displayAsIcon, iconFileName, iconIndex);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 * @param iconFileName an input-parameter of type Variant
	 */
	public void convertTo(Variant classType, Variant displayAsIcon, Variant iconFileName) {
		Dispatch.call(this, "ConvertTo", classType, displayAsIcon, iconFileName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 * @param displayAsIcon an input-parameter of type Variant
	 */
	public void convertTo(Variant classType, Variant displayAsIcon) {
		Dispatch.call(this, "ConvertTo", classType, displayAsIcon);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param classType an input-parameter of type Variant
	 */
	public void convertTo(Variant classType) {
		Dispatch.call(this, "ConvertTo", classType);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void convertTo() {
		Dispatch.call(this, "ConvertTo");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void activateAs(String lastParam) {
		Dispatch.call(this, "ActivateAs", lastParam);
	}

}
