/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class _Global extends Dispatch {

	public static final String componentName = "Word._Global";

	public _Global() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public _Global(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public _Global(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getName() {
		return Dispatch.get(this, "Name").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Documents
	 */
	public Documents getDocuments() {
		return new Documents(Dispatch.get(this, "Documents").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Windows
	 */
	public Windows getWindows() {
		return new Windows(Dispatch.get(this, "Windows").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Document
	 */
	public Document getActiveDocument() {
		return new Document(Dispatch.get(this, "ActiveDocument").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window getActiveWindow() {
		return new Window(Dispatch.get(this, "ActiveWindow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Selection
	 */
	public Selection getSelection() {
		return new Selection(Dispatch.get(this, "Selection").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getWordBasic() {
		return Dispatch.get(this, "WordBasic");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getPrintPreview() {
		return Dispatch.get(this, "PrintPreview").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setPrintPreview(boolean lastParam) {
		Dispatch.call(this, "PrintPreview", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type RecentFiles
	 */
	public RecentFiles getRecentFiles() {
		return new RecentFiles(Dispatch.get(this, "RecentFiles").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Template
	 */
	public Template getNormalTemplate() {
		return new Template(Dispatch.get(this, "NormalTemplate").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type System
	 */
	public System getSystem() {
		return new System(Dispatch.get(this, "System").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type AutoCorrect
	 */
	public AutoCorrect getAutoCorrect() {
		return new AutoCorrect(Dispatch.get(this, "AutoCorrect").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FontNames
	 */
	public FontNames getFontNames() {
		return new FontNames(Dispatch.get(this, "FontNames").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FontNames
	 */
	public FontNames getLandscapeFontNames() {
		return new FontNames(Dispatch.get(this, "LandscapeFontNames").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FontNames
	 */
	public FontNames getPortraitFontNames() {
		return new FontNames(Dispatch.get(this, "PortraitFontNames").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Languages
	 */
	public Languages getLanguages() {
		return new Languages(Dispatch.get(this, "Languages").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Assistant
	 */
	/*
	public Assistant getAssistant() {
		return new Assistant(Dispatch.get(this, "Assistant").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type FileConverters
	 */
	public FileConverters getFileConverters() {
		return new FileConverters(Dispatch.get(this, "FileConverters").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Dialogs
	 */
	public Dialogs getDialogs() {
		return new Dialogs(Dispatch.get(this, "Dialogs").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type CaptionLabels
	 */
	public CaptionLabels getCaptionLabels() {
		return new CaptionLabels(Dispatch.get(this, "CaptionLabels").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type AutoCaptions
	 */
	public AutoCaptions getAutoCaptions() {
		return new AutoCaptions(Dispatch.get(this, "AutoCaptions").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type AddIns
	 */
	public AddIns getAddIns() {
		return new AddIns(Dispatch.get(this, "AddIns").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Tasks
	 */
	public Tasks getTasks() {
		return new Tasks(Dispatch.get(this, "Tasks").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getMacroContainer() {
		return Dispatch.get(this, "MacroContainer");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type _CommandBars
	 */
	/*
	public _CommandBars getCommandBars() {
		return new _CommandBars(Dispatch.get(this, "CommandBars").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type SynonymInfo
	 */
	public SynonymInfo getSynonymInfo(String word, Variant lastParam) {
		return new SynonymInfo(Dispatch.call(this, "SynonymInfo", word, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @return the result is of type SynonymInfo
	 */
	public SynonymInfo getSynonymInfo(String word) {
		return new SynonymInfo(Dispatch.call(this, "SynonymInfo", word).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type VBE
	 */
	public VBE getVBE() {
		return new VBE(Dispatch.get(this, "VBE").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ListGalleries
	 */
	public ListGalleries getListGalleries() {
		return new ListGalleries(Dispatch.get(this, "ListGalleries").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getActivePrinter() {
		return Dispatch.get(this, "ActivePrinter").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setActivePrinter(String lastParam) {
		Dispatch.call(this, "ActivePrinter", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Templates
	 */
	public Templates getTemplates() {
		return new Templates(Dispatch.get(this, "Templates").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getCustomizationContext() {
		return Dispatch.get(this, "CustomizationContext");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Object
	 */
	public void setCustomizationContext(Object lastParam) {
		Dispatch.call(this, "CustomizationContext", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type KeyBindings
	 */
	public KeyBindings getKeyBindings() {
		return new KeyBindings(Dispatch.get(this, "KeyBindings").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCategory an input-parameter of type int
	 * @param command an input-parameter of type String
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type KeysBoundTo
	 */
	public KeysBoundTo getKeysBoundTo(int keyCategory, String command, Variant lastParam) {
		return new KeysBoundTo(Dispatch.call(this, "KeysBoundTo", new Variant(keyCategory), command, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCategory an input-parameter of type int
	 * @param command an input-parameter of type String
	 * @return the result is of type KeysBoundTo
	 */
	public KeysBoundTo getKeysBoundTo(int keyCategory, String command) {
		return new KeysBoundTo(Dispatch.call(this, "KeysBoundTo", new Variant(keyCategory), command).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type KeyBinding
	 */
	public KeyBinding getFindKey(int keyCode, Variant lastParam) {
		return new KeyBinding(Dispatch.call(this, "FindKey", new Variant(keyCode), lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @return the result is of type KeyBinding
	 */
	public KeyBinding getFindKey(int keyCode) {
		return new KeyBinding(Dispatch.call(this, "FindKey", new Variant(keyCode)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Options
	 */
	public Options getOptions() {
		return new Options(Dispatch.get(this, "Options").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Dictionaries
	 */
	public Dictionaries getCustomDictionaries() {
		return new Dictionaries(Dispatch.get(this, "CustomDictionaries").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setStatusBar(String lastParam) {
		Dispatch.call(this, "StatusBar", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getShowVisualBasicEditor() {
		return Dispatch.get(this, "ShowVisualBasicEditor").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setShowVisualBasicEditor(boolean lastParam) {
		Dispatch.call(this, "ShowVisualBasicEditor", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Object
	 * @return the result is of type boolean
	 */
	public boolean getIsObjectValid(Object lastParam) {
		return Dispatch.call(this, "IsObjectValid", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type HangulHanjaConversionDictionaries
	 */
	public HangulHanjaConversionDictionaries getHangulHanjaDictionaries() {
		return new HangulHanjaConversionDictionaries(Dispatch.get(this, "HangulHanjaDictionaries").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean repeat(Variant lastParam) {
		return Dispatch.call(this, "Repeat", lastParam).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean repeat() {
		return Dispatch.call(this, "Repeat").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param channel an input-parameter of type int
	 * @param lastParam an input-parameter of type String
	 */
	public void dDEExecute(int channel, String lastParam) {
		Dispatch.call(this, "DDEExecute", new Variant(channel), lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param app an input-parameter of type String
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type int
	 */
	public int dDEInitiate(String app, String lastParam) {
		return Dispatch.call(this, "DDEInitiate", app, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param channel an input-parameter of type int
	 * @param item an input-parameter of type String
	 * @param lastParam an input-parameter of type String
	 */
	public void dDEPoke(int channel, String item, String lastParam) {
		Dispatch.call(this, "DDEPoke", new Variant(channel), item, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param channel an input-parameter of type int
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type String
	 */
	public String dDERequest(int channel, String lastParam) {
		return Dispatch.call(this, "DDERequest", new Variant(channel), lastParam).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void dDETerminate(int lastParam) {
		Dispatch.call(this, "DDETerminate", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void dDETerminateAll() {
		Dispatch.call(this, "DDETerminateAll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @param arg2 an input-parameter of type Variant
	 * @param arg3 an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1, Variant arg2, Variant arg3, Variant lastParam) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1), arg2, arg3, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @param arg2 an input-parameter of type Variant
	 * @param arg3 an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1, Variant arg2, Variant arg3) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1), arg2, arg3).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @param arg2 an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1, Variant arg2) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1), arg2).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param arg1 an input-parameter of type int
	 * @return the result is of type int
	 */
	public int buildKeyCode(int arg1) {
		return Dispatch.call(this, "BuildKeyCode", new Variant(arg1)).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type String
	 */
	public String keyString(int keyCode, Variant lastParam) {
		return Dispatch.call(this, "KeyString", new Variant(keyCode), lastParam).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param keyCode an input-parameter of type int
	 * @return the result is of type String
	 */
	public String keyString(int keyCode) {
		return Dispatch.call(this, "KeyString", new Variant(keyCode)).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9, Variant lastParam) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9, lastParam}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6) {
		return Dispatch.callN(this, "CheckSpelling", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6}).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4, customDictionary5).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3, customDictionary4).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2, Variant customDictionary3) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2, customDictionary3).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant customDictionary2) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary, customDictionary2).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase, mainDictionary).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary, Variant ignoreUppercase) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary, ignoreUppercase).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word, Variant customDictionary) {
		return Dispatch.call(this, "CheckSpelling", word, customDictionary).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @return the result is of type boolean
	 */
	public boolean checkSpelling(String word) {
		return Dispatch.call(this, "CheckSpelling", word).toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9, Variant lastParam) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9, lastParam}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @param customDictionary9 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8, Variant customDictionary9) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8, customDictionary9}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @param customDictionary8 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7, Variant customDictionary8) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7, customDictionary8}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @param customDictionary7 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6, Variant customDictionary7) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6, customDictionary7}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @param customDictionary6 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5, Variant customDictionary6) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5, customDictionary6}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @param customDictionary5 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4, Variant customDictionary5) {
		return new SpellingSuggestions(Dispatch.callN(this, "GetSpellingSuggestions", new Object[] { word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4, customDictionary5}).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @param customDictionary4 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3, Variant customDictionary4) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3, customDictionary4).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @param customDictionary3 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2, Variant customDictionary3) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2, customDictionary3).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @param customDictionary2 an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode, Variant customDictionary2) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode, customDictionary2).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @param suggestionMode an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary, Variant suggestionMode) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary, suggestionMode).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @param mainDictionary an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase, Variant mainDictionary) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase, mainDictionary).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @param ignoreUppercase an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary, Variant ignoreUppercase) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary, ignoreUppercase).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @param customDictionary an input-parameter of type Variant
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word, Variant customDictionary) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word, customDictionary).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param word an input-parameter of type String
	 * @return the result is of type SpellingSuggestions
	 */
	public SpellingSuggestions getSpellingSuggestions(String word) {
		return new SpellingSuggestions(Dispatch.call(this, "GetSpellingSuggestions", word).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void help(Variant lastParam) {
		Dispatch.call(this, "Help", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window newWindow() {
		return new Window(Dispatch.call(this, "NewWindow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type String
	 */
	public String cleanString(String lastParam) {
		return Dispatch.call(this, "CleanString", lastParam).toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void changeFileOpenDirectory(String lastParam) {
		Dispatch.call(this, "ChangeFileOpenDirectory", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float inchesToPoints(float lastParam) {
		return Dispatch.call(this, "InchesToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float centimetersToPoints(float lastParam) {
		return Dispatch.call(this, "CentimetersToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float millimetersToPoints(float lastParam) {
		return Dispatch.call(this, "MillimetersToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float picasToPoints(float lastParam) {
		return Dispatch.call(this, "PicasToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float linesToPoints(float lastParam) {
		return Dispatch.call(this, "LinesToPoints", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToInches(float lastParam) {
		return Dispatch.call(this, "PointsToInches", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToCentimeters(float lastParam) {
		return Dispatch.call(this, "PointsToCentimeters", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToMillimeters(float lastParam) {
		return Dispatch.call(this, "PointsToMillimeters", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToPicas(float lastParam) {
		return Dispatch.call(this, "PointsToPicas", new Variant(lastParam)).toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type float
	 */
	public float pointsToLines(float lastParam) {
		return Dispatch.call(this, "PointsToLines", new Variant(lastParam)).toFloat();
	}

}
