/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdSortSeparator {

	public static final int wdSortSeparateByTabs = 0;
	public static final int wdSortSeparateByCommas = 1;
	public static final int wdSortSeparateByDefaultTableSeparator = 2;
}
