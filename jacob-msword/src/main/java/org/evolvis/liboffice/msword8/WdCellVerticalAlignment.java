/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdCellVerticalAlignment {

	public static final int wdCellAlignVerticalTop = 0;
	public static final int wdCellAlignVerticalCenter = 1;
	public static final int wdCellAlignVerticalBottom = 3;
}
