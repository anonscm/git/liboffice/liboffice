/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class ParagraphFormat extends _ParagraphFormat {

	public static final String componentName = "Word.ParagraphFormat";

	public ParagraphFormat() {
		super(componentName);
	}

	public ParagraphFormat(Dispatch d) {
		super(d);
	}
}
