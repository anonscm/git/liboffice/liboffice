/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Global extends _Global {

	public static final String componentName = "Word.Global";

	public Global() {
		super(componentName);
	}

	public Global(Dispatch d) {
		super(d);
	}
}
