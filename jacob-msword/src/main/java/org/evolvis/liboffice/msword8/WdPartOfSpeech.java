/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdPartOfSpeech {

	public static final int wdAdjective = 0;
	public static final int wdNoun = 1;
	public static final int wdAdverb = 2;
	public static final int wdVerb = 3;
}
