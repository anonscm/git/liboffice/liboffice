/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdOriginalFormat {

	public static final int wdWordDocument = 0;
	public static final int wdOriginalDocumentFormat = 1;
	public static final int wdPromptUser = 2;
}
