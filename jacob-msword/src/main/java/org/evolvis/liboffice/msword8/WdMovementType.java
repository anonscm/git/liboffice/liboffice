/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdMovementType {

	public static final int wdMove = 0;
	public static final int wdExtend = 1;
}
