/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class VBE extends Dispatch {

	public static final String componentName = "VBIDE.VBE";

	public VBE() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public VBE(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public VBE(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getVersion() {
		return Dispatch.get(this, "Version").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type VBProjects
	 */
	/*
	public VBProjects getVBProjects() {
		return new VBProjects(Dispatch.get(this, "VBProjects").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type _CommandBars
	 */
	/*
	public _CommandBars getCommandBars() {
		return new _CommandBars(Dispatch.get(this, "CommandBars").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type CodePanes
	 */
	/*
	public CodePanes getCodePanes() {
		return new CodePanes(Dispatch.get(this, "CodePanes").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Windows
	 */
	public Windows getWindows() {
		return new Windows(Dispatch.get(this, "Windows").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Events
	 */
	/*
	public Events getEvents() {
		return new Events(Dispatch.get(this, "Events").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type VBProject
	 */
	/*
	public VBProject getActiveVBProject() {
		return new VBProject(Dispatch.get(this, "ActiveVBProject").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type VBProject
	 */
	/*
	public void setActiveVBProject(VBProject lastParam) {
		Dispatch.call(this, "ActiveVBProject", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type VBComponent
	 */
	/*
	public VBComponent getSelectedVBComponent() {
		return new VBComponent(Dispatch.get(this, "SelectedVBComponent").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window getMainWindow() {
		return new Window(Dispatch.get(this, "MainWindow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window getActiveWindow() {
		return new Window(Dispatch.get(this, "ActiveWindow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type CodePane
	 */
	/*
	public CodePane getActiveCodePane() {
		return new CodePane(Dispatch.get(this, "ActiveCodePane").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type CodePane
	 */
	/*
	public void setActiveCodePane(CodePane lastParam) {
		Dispatch.call(this, "ActiveCodePane", lastParam);
	}
	*/

}
