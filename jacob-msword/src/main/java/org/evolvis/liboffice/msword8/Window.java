/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Window extends Dispatch {

	public static final String componentName = "Word.Window";

	public Window() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Window(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Window(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Pane
	 */
	public Pane getActivePane() {
		return new Pane(Dispatch.get(this, "ActivePane").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Document
	 */
	public Document getDocument() {
		return new Document(Dispatch.get(this, "Document").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Panes
	 */
	public Panes getPanes() {
		return new Panes(Dispatch.get(this, "Panes").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Selection
	 */
	public Selection getSelection() {
		return new Selection(Dispatch.get(this, "Selection").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLeft() {
		return Dispatch.get(this, "Left").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLeft(int lastParam) {
		Dispatch.call(this, "Left", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getTop() {
		return Dispatch.get(this, "Top").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setTop(int lastParam) {
		Dispatch.call(this, "Top", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWidth() {
		return Dispatch.get(this, "Width").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setWidth(int lastParam) {
		Dispatch.call(this, "Width", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHeight() {
		return Dispatch.get(this, "Height").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHeight(int lastParam) {
		Dispatch.call(this, "Height", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSplit() {
		return Dispatch.get(this, "Split").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setSplit(boolean lastParam) {
		Dispatch.call(this, "Split", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getSplitVertical() {
		return Dispatch.get(this, "SplitVertical").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setSplitVertical(int lastParam) {
		Dispatch.call(this, "SplitVertical", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getCaption() {
		return Dispatch.get(this, "Caption").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setCaption(String lastParam) {
		Dispatch.call(this, "Caption", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWindowState() {
		return Dispatch.get(this, "WindowState").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setWindowState(int lastParam) {
		Dispatch.call(this, "WindowState", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayRulers() {
		return Dispatch.get(this, "DisplayRulers").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayRulers(boolean lastParam) {
		Dispatch.call(this, "DisplayRulers", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayVerticalRuler() {
		return Dispatch.get(this, "DisplayVerticalRuler").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayVerticalRuler(boolean lastParam) {
		Dispatch.call(this, "DisplayVerticalRuler", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type View
	 */
	public View getView() {
		return new View(Dispatch.get(this, "View").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getType() {
		return Dispatch.get(this, "Type").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window getNext() {
		return new Window(Dispatch.get(this, "Next").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window getPrevious() {
		return new Window(Dispatch.get(this, "Previous").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWindowNumber() {
		return Dispatch.get(this, "WindowNumber").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayVerticalScrollBar() {
		return Dispatch.get(this, "DisplayVerticalScrollBar").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayVerticalScrollBar(boolean lastParam) {
		Dispatch.call(this, "DisplayVerticalScrollBar", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayHorizontalScrollBar() {
		return Dispatch.get(this, "DisplayHorizontalScrollBar").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayHorizontalScrollBar(boolean lastParam) {
		Dispatch.call(this, "DisplayHorizontalScrollBar", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getStyleAreaWidth() {
		return Dispatch.get(this, "StyleAreaWidth").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setStyleAreaWidth(float lastParam) {
		Dispatch.call(this, "StyleAreaWidth", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDisplayScreenTips() {
		return Dispatch.get(this, "DisplayScreenTips").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDisplayScreenTips(boolean lastParam) {
		Dispatch.call(this, "DisplayScreenTips", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHorizontalPercentScrolled() {
		return Dispatch.get(this, "HorizontalPercentScrolled").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHorizontalPercentScrolled(int lastParam) {
		Dispatch.call(this, "HorizontalPercentScrolled", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getVerticalPercentScrolled() {
		return Dispatch.get(this, "VerticalPercentScrolled").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setVerticalPercentScrolled(int lastParam) {
		Dispatch.call(this, "VerticalPercentScrolled", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getDocumentMap() {
		return Dispatch.get(this, "DocumentMap").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type boolean
	 */
	public void setDocumentMap(boolean lastParam) {
		Dispatch.call(this, "DocumentMap", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getActive() {
		return Dispatch.get(this, "Active").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDocumentMapPercentWidth() {
		return Dispatch.get(this, "DocumentMapPercentWidth").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDocumentMapPercentWidth(int lastParam) {
		Dispatch.call(this, "DocumentMapPercentWidth", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getIndex() {
		return Dispatch.get(this, "Index").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getIMEMode() {
		return Dispatch.get(this, "IMEMode").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setIMEMode(int lastParam) {
		Dispatch.call(this, "IMEMode", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void activate() {
		Dispatch.call(this, "Activate");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param saveChanges an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void close(Variant saveChanges, Variant lastParam) {
		Dispatch.call(this, "Close", saveChanges, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param saveChanges an input-parameter of type Variant
	 */
	public void close(Variant saveChanges) {
		Dispatch.call(this, "Close", saveChanges);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void close() {
		Dispatch.call(this, "Close");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void largeScroll(Variant down, Variant up, Variant toRight, Variant lastParam) {
		Dispatch.call(this, "LargeScroll", down, up, toRight, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 */
	public void largeScroll(Variant down, Variant up, Variant toRight) {
		Dispatch.call(this, "LargeScroll", down, up, toRight);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 */
	public void largeScroll(Variant down, Variant up) {
		Dispatch.call(this, "LargeScroll", down, up);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 */
	public void largeScroll(Variant down) {
		Dispatch.call(this, "LargeScroll", down);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void largeScroll() {
		Dispatch.call(this, "LargeScroll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void smallScroll(Variant down, Variant up, Variant toRight, Variant lastParam) {
		Dispatch.call(this, "SmallScroll", down, up, toRight, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 * @param toRight an input-parameter of type Variant
	 */
	public void smallScroll(Variant down, Variant up, Variant toRight) {
		Dispatch.call(this, "SmallScroll", down, up, toRight);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param up an input-parameter of type Variant
	 */
	public void smallScroll(Variant down, Variant up) {
		Dispatch.call(this, "SmallScroll", down, up);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 */
	public void smallScroll(Variant down) {
		Dispatch.call(this, "SmallScroll", down);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void smallScroll() {
		Dispatch.call(this, "SmallScroll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Window
	 */
	public Window newWindow() {
		return new Window(Dispatch.call(this, "NewWindow").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 * @param collate an input-parameter of type Variant
	 * @param activePrinterMacGX an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile, Variant collate, Variant activePrinterMacGX, Variant lastParam) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile, collate, activePrinterMacGX, lastParam});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 * @param collate an input-parameter of type Variant
	 * @param activePrinterMacGX an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile, Variant collate, Variant activePrinterMacGX) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile, collate, activePrinterMacGX});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 * @param collate an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile, Variant collate) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile, collate});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 * @param printToFile an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType, Variant printToFile) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType, printToFile});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 * @param pageType an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages, Variant pageType) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages, pageType});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 * @param pages an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies, Variant pages) {
		Dispatch.callN(this, "PrintOut", new Object[] { background, append, range, outputFileName, from, to, item, copies, pages});
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 * @param copies an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item, Variant copies) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from, to, item, copies);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 * @param item an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to, Variant item) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from, to, item);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 * @param to an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from, Variant to) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from, to);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 * @param from an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName, Variant from) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName, from);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 * @param outputFileName an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range, Variant outputFileName) {
		Dispatch.call(this, "PrintOut", background, append, range, outputFileName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 * @param range an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append, Variant range) {
		Dispatch.call(this, "PrintOut", background, append, range);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 * @param append an input-parameter of type Variant
	 */
	public void printOut(Variant background, Variant append) {
		Dispatch.call(this, "PrintOut", background, append);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param background an input-parameter of type Variant
	 */
	public void printOut(Variant background) {
		Dispatch.call(this, "PrintOut", background);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void printOut() {
		Dispatch.call(this, "PrintOut");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 */
	public void pageScroll(Variant down, Variant lastParam) {
		Dispatch.call(this, "PageScroll", down, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param down an input-parameter of type Variant
	 */
	public void pageScroll(Variant down) {
		Dispatch.call(this, "PageScroll", down);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void pageScroll() {
		Dispatch.call(this, "PageScroll");
	}

}
