/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdEndnoteLocation {

	public static final int wdEndOfSection = 0;
	public static final int wdEndOfDocument = 1;
}
