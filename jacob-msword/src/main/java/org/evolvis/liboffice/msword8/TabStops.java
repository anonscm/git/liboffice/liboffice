/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class TabStops extends Dispatch {

	public static final String componentName = "Word.TabStops";

	public TabStops() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public TabStops(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public TabStops(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant get_NewEnum() {
		return Dispatch.get(this, "_NewEnum");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCount() {
		return Dispatch.get(this, "Count").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type TabStop
	 */
	public TabStop item(Variant lastParam) {
		return new TabStop(Dispatch.call(this, "Item", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param position an input-parameter of type float
	 * @param alignment an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type TabStop
	 */
	public TabStop add(float position, Variant alignment, Variant lastParam) {
		return new TabStop(Dispatch.call(this, "Add", new Variant(position), alignment, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param position an input-parameter of type float
	 * @param alignment an input-parameter of type Variant
	 * @return the result is of type TabStop
	 */
	public TabStop add(float position, Variant alignment) {
		return new TabStop(Dispatch.call(this, "Add", new Variant(position), alignment).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param position an input-parameter of type float
	 * @return the result is of type TabStop
	 */
	public TabStop add(float position) {
		return new TabStop(Dispatch.call(this, "Add", new Variant(position)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void clearAll() {
		Dispatch.call(this, "ClearAll");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type TabStop
	 */
	public TabStop before(float lastParam) {
		return new TabStop(Dispatch.call(this, "Before", new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 * @return the result is of type TabStop
	 */
	public TabStop after(float lastParam) {
		return new TabStop(Dispatch.call(this, "After", new Variant(lastParam)).toDispatch());
	}

}
