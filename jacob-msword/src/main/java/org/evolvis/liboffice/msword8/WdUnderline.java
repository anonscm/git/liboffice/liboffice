/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdUnderline {

	public static final int wdUnderlineNone = 0;
	public static final int wdUnderlineSingle = 1;
	public static final int wdUnderlineWords = 2;
	public static final int wdUnderlineDouble = 3;
	public static final int wdUnderlineDotted = 4;
	public static final int wdUnderlineThick = 6;
	public static final int wdUnderlineDash = 7;
	public static final int wdUnderlineDotDash = 9;
	public static final int wdUnderlineDotDotDash = 10;
	public static final int wdUnderlineWavy = 11;
}
