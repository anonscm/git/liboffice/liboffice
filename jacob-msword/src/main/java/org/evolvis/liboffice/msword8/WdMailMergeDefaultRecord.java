/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdMailMergeDefaultRecord {

	public static final int wdDefaultFirstRecord = 1;
	public static final int wdDefaultLastRecord = -16;
}
