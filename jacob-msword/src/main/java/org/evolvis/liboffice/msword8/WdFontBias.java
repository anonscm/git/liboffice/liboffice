/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdFontBias {

	public static final int wdFontBiasDontCare = 255;
	public static final int wdFontBiasDefault = 0;
	public static final int wdFontBiasFareast = 1;
}
