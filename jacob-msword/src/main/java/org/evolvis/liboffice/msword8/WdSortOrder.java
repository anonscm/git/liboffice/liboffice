/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdSortOrder {

	public static final int wdSortOrderAscending = 0;
	public static final int wdSortOrderDescending = 1;
}
