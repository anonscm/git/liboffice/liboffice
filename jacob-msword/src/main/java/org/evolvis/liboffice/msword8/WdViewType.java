/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdViewType {

	public static final int wdNormalView = 1;
	public static final int wdOutlineView = 2;
	public static final int wdPageView = 3;
	public static final int wdPrintPreview = 4;
	public static final int wdMasterView = 5;
	public static final int wdOnlineView = 6;
}
