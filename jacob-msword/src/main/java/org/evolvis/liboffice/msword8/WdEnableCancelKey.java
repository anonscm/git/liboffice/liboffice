/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdEnableCancelKey {

	public static final int wdCancelDisabled = 0;
	public static final int wdCancelInterrupt = 1;
}
