/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdParagraphAlignment {

	public static final int wdAlignParagraphLeft = 0;
	public static final int wdAlignParagraphCenter = 1;
	public static final int wdAlignParagraphRight = 2;
	public static final int wdAlignParagraphJustify = 3;
}
