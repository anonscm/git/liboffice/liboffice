/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdWordDialogTabHID {

	public static final int wdDialogToolsOptionsTabTypography = 739;
	public static final int wdDialogToolsOptionsTabFuzzy = 790;
	public static final int wdDialogToolsOptionsTabHangulHanjaConversion = 786;
	public static final int wdDialogFilePageSetupTabCharsLines = 150004;
	public static final int wdDialogFormatParagraphTabTeisai = 1000002;
	public static final int wdDialogToolsAutoCorrectExceptionsTabHangulAndAlphabet = 1400002;
}
