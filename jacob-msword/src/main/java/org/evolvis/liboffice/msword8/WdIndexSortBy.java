/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdIndexSortBy {

	public static final int wdIndexSortByStroke = 0;
	public static final int wdIndexSortBySyllable = 1;
}
