/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdPageFit {

	public static final int wdPageFitNone = 0;
	public static final int wdPageFitFullPage = 1;
	public static final int wdPageFitBestFit = 2;
}
