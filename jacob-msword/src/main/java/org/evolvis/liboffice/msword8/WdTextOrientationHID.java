/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdTextOrientationHID {

	public static final int wdTextOrientationVerticalFarEast = 1;
	public static final int wdTextOrientationHorizontalRotatedFarEast = 4;
}
