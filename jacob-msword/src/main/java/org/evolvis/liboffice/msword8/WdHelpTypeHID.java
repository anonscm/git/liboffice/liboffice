/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdHelpTypeHID {

	public static final int wdHelpIchitaro = 11;
	public static final int wdHelpPE2 = 12;
}
