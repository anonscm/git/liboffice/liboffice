/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Paragraphs extends Dispatch {

	public static final String componentName = "Word.Paragraphs";

	public Paragraphs() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public Paragraphs(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public Paragraphs(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant get_NewEnum() {
		return Dispatch.get(this, "_NewEnum");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCount() {
		return Dispatch.get(this, "Count").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Paragraph
	 */
	public Paragraph getFirst() {
		return new Paragraph(Dispatch.get(this, "First").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Paragraph
	 */
	public Paragraph getLast() {
		return new Paragraph(Dispatch.get(this, "Last").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ParagraphFormat
	 */
	public ParagraphFormat getFormat() {
		return new ParagraphFormat(Dispatch.get(this, "Format").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type ParagraphFormat
	 */
	public void setFormat(ParagraphFormat lastParam) {
		Dispatch.call(this, "Format", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type TabStops
	 */
	public TabStops getTabStops() {
		return new TabStops(Dispatch.get(this, "TabStops").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type TabStops
	 */
	public void setTabStops(TabStops lastParam) {
		Dispatch.call(this, "TabStops", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Borders
	 */
	public Borders getBorders() {
		return new Borders(Dispatch.get(this, "Borders").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Borders
	 */
	public void setBorders(Borders lastParam) {
		Dispatch.call(this, "Borders", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant getStyle() {
		return Dispatch.get(this, "Style");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void setStyle(Variant lastParam) {
		Dispatch.call(this, "Style", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getAlignment() {
		return Dispatch.get(this, "Alignment").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setAlignment(int lastParam) {
		Dispatch.call(this, "Alignment", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getKeepTogether() {
		return Dispatch.get(this, "KeepTogether").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setKeepTogether(int lastParam) {
		Dispatch.call(this, "KeepTogether", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getKeepWithNext() {
		return Dispatch.get(this, "KeepWithNext").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setKeepWithNext(int lastParam) {
		Dispatch.call(this, "KeepWithNext", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getPageBreakBefore() {
		return Dispatch.get(this, "PageBreakBefore").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setPageBreakBefore(int lastParam) {
		Dispatch.call(this, "PageBreakBefore", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getNoLineNumber() {
		return Dispatch.get(this, "NoLineNumber").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setNoLineNumber(int lastParam) {
		Dispatch.call(this, "NoLineNumber", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getRightIndent() {
		return Dispatch.get(this, "RightIndent").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setRightIndent(float lastParam) {
		Dispatch.call(this, "RightIndent", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getLeftIndent() {
		return Dispatch.get(this, "LeftIndent").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setLeftIndent(float lastParam) {
		Dispatch.call(this, "LeftIndent", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getFirstLineIndent() {
		return Dispatch.get(this, "FirstLineIndent").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setFirstLineIndent(float lastParam) {
		Dispatch.call(this, "FirstLineIndent", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getLineSpacing() {
		return Dispatch.get(this, "LineSpacing").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setLineSpacing(float lastParam) {
		Dispatch.call(this, "LineSpacing", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getLineSpacingRule() {
		return Dispatch.get(this, "LineSpacingRule").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setLineSpacingRule(int lastParam) {
		Dispatch.call(this, "LineSpacingRule", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getSpaceBefore() {
		return Dispatch.get(this, "SpaceBefore").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setSpaceBefore(float lastParam) {
		Dispatch.call(this, "SpaceBefore", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getSpaceAfter() {
		return Dispatch.get(this, "SpaceAfter").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setSpaceAfter(float lastParam) {
		Dispatch.call(this, "SpaceAfter", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHyphenation() {
		return Dispatch.get(this, "Hyphenation").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHyphenation(int lastParam) {
		Dispatch.call(this, "Hyphenation", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWidowControl() {
		return Dispatch.get(this, "WidowControl").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setWidowControl(int lastParam) {
		Dispatch.call(this, "WidowControl", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Shading
	 */
	public Shading getShading() {
		return new Shading(Dispatch.get(this, "Shading").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getFarEastLineBreakControl() {
		return Dispatch.get(this, "FarEastLineBreakControl").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setFarEastLineBreakControl(int lastParam) {
		Dispatch.call(this, "FarEastLineBreakControl", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getWordWrap() {
		return Dispatch.get(this, "WordWrap").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setWordWrap(int lastParam) {
		Dispatch.call(this, "WordWrap", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHangingPunctuation() {
		return Dispatch.get(this, "HangingPunctuation").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHangingPunctuation(int lastParam) {
		Dispatch.call(this, "HangingPunctuation", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getHalfWidthPunctuationOnTopOfLine() {
		return Dispatch.get(this, "HalfWidthPunctuationOnTopOfLine").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setHalfWidthPunctuationOnTopOfLine(int lastParam) {
		Dispatch.call(this, "HalfWidthPunctuationOnTopOfLine", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getAddSpaceBetweenFarEastAndAlpha() {
		return Dispatch.get(this, "AddSpaceBetweenFarEastAndAlpha").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setAddSpaceBetweenFarEastAndAlpha(int lastParam) {
		Dispatch.call(this, "AddSpaceBetweenFarEastAndAlpha", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getAddSpaceBetweenFarEastAndDigit() {
		return Dispatch.get(this, "AddSpaceBetweenFarEastAndDigit").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setAddSpaceBetweenFarEastAndDigit(int lastParam) {
		Dispatch.call(this, "AddSpaceBetweenFarEastAndDigit", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getBaseLineAlignment() {
		return Dispatch.get(this, "BaseLineAlignment").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setBaseLineAlignment(int lastParam) {
		Dispatch.call(this, "BaseLineAlignment", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getAutoAdjustRightIndent() {
		return Dispatch.get(this, "AutoAdjustRightIndent").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setAutoAdjustRightIndent(int lastParam) {
		Dispatch.call(this, "AutoAdjustRightIndent", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getDisableLineHeightGrid() {
		return Dispatch.get(this, "DisableLineHeightGrid").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setDisableLineHeightGrid(int lastParam) {
		Dispatch.call(this, "DisableLineHeightGrid", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getOutlineLevel() {
		return Dispatch.get(this, "OutlineLevel").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 */
	public void setOutlineLevel(int lastParam) {
		Dispatch.call(this, "OutlineLevel", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type Paragraph
	 */
	public Paragraph item(int lastParam) {
		return new Paragraph(Dispatch.call(this, "Item", new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type Paragraph
	 */
	public Paragraph add(Variant lastParam) {
		return new Paragraph(Dispatch.call(this, "Add", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Paragraph
	 */
	public Paragraph add() {
		return new Paragraph(Dispatch.call(this, "Add").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void closeUp() {
		Dispatch.call(this, "CloseUp");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void openUp() {
		Dispatch.call(this, "OpenUp");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void openOrCloseUp() {
		Dispatch.call(this, "OpenOrCloseUp");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type short
	 */
	public void tabHangingIndent(short lastParam) {
		Dispatch.call(this, "TabHangingIndent", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type short
	 */
	public void tabIndent(short lastParam) {
		Dispatch.call(this, "TabIndent", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void reset() {
		Dispatch.call(this, "Reset");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void space1() {
		Dispatch.call(this, "Space1");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void space15() {
		Dispatch.call(this, "Space15");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void space2() {
		Dispatch.call(this, "Space2");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type short
	 */
	public void indentCharWidth(short lastParam) {
		Dispatch.call(this, "IndentCharWidth", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type short
	 */
	public void indentFirstLineCharWidth(short lastParam) {
		Dispatch.call(this, "IndentFirstLineCharWidth", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void outlinePromote() {
		Dispatch.call(this, "OutlinePromote");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void outlineDemote() {
		Dispatch.call(this, "OutlineDemote");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void outlineDemoteToBody() {
		Dispatch.call(this, "OutlineDemoteToBody");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void indent() {
		Dispatch.call(this, "Indent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void outdent() {
		Dispatch.call(this, "Outdent");
	}

}
