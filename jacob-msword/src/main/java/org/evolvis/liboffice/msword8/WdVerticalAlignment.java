/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdVerticalAlignment {

	public static final int wdAlignVerticalTop = 0;
	public static final int wdAlignVerticalCenter = 1;
	public static final int wdAlignVerticalJustify = 2;
	public static final int wdAlignVerticalBottom = 3;
}
