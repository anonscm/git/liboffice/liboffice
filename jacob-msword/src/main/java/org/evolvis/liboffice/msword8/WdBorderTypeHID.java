/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdBorderTypeHID {

	public static final int wdBorderDiagonalDown = -7;
	public static final int wdBorderDiagonalUp = -8;
}
