/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class MailMergeFields extends Dispatch {

	public static final String componentName = "Word.MailMergeFields";

	public MailMergeFields() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public MailMergeFields(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public MailMergeFields(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Variant
	 */
	public Variant get_NewEnum() {
		return Dispatch.get(this, "_NewEnum");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCount() {
		return Dispatch.get(this, "Count").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type int
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField item(int lastParam) {
		return new MailMergeField(Dispatch.call(this, "Item", new Variant(lastParam)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param lastParam an input-parameter of type String
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField add(Range range, String lastParam) {
		return new MailMergeField(Dispatch.call(this, "Add", range, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param name an input-parameter of type String
	 * @param prompt an input-parameter of type Variant
	 * @param defaultAskText an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addAsk(Range range, String name, Variant prompt, Variant defaultAskText, Variant lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddAsk", range, name, prompt, defaultAskText, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param name an input-parameter of type String
	 * @param prompt an input-parameter of type Variant
	 * @param defaultAskText an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addAsk(Range range, String name, Variant prompt, Variant defaultAskText) {
		return new MailMergeField(Dispatch.call(this, "AddAsk", range, name, prompt, defaultAskText).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param name an input-parameter of type String
	 * @param prompt an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addAsk(Range range, String name, Variant prompt) {
		return new MailMergeField(Dispatch.call(this, "AddAsk", range, name, prompt).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param name an input-parameter of type String
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addAsk(Range range, String name) {
		return new MailMergeField(Dispatch.call(this, "AddAsk", range, name).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param prompt an input-parameter of type Variant
	 * @param defaultFillInText an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addFillIn(Range range, Variant prompt, Variant defaultFillInText, Variant lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddFillIn", range, prompt, defaultFillInText, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param prompt an input-parameter of type Variant
	 * @param defaultFillInText an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addFillIn(Range range, Variant prompt, Variant defaultFillInText) {
		return new MailMergeField(Dispatch.call(this, "AddFillIn", range, prompt, defaultFillInText).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param prompt an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addFillIn(Range range, Variant prompt) {
		return new MailMergeField(Dispatch.call(this, "AddFillIn", range, prompt).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addFillIn(Range range) {
		return new MailMergeField(Dispatch.call(this, "AddFillIn", range).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @param compareTo an input-parameter of type Variant
	 * @param trueAutoText an input-parameter of type Variant
	 * @param trueText an input-parameter of type Variant
	 * @param falseAutoText an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addIf(Range range, String mergeField, int comparison, Variant compareTo, Variant trueAutoText, Variant trueText, Variant falseAutoText, Variant lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddIf", range, mergeField, new Variant(comparison), compareTo, trueAutoText, trueText, falseAutoText, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @param compareTo an input-parameter of type Variant
	 * @param trueAutoText an input-parameter of type Variant
	 * @param trueText an input-parameter of type Variant
	 * @param falseAutoText an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addIf(Range range, String mergeField, int comparison, Variant compareTo, Variant trueAutoText, Variant trueText, Variant falseAutoText) {
		return new MailMergeField(Dispatch.call(this, "AddIf", range, mergeField, new Variant(comparison), compareTo, trueAutoText, trueText, falseAutoText).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @param compareTo an input-parameter of type Variant
	 * @param trueAutoText an input-parameter of type Variant
	 * @param trueText an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addIf(Range range, String mergeField, int comparison, Variant compareTo, Variant trueAutoText, Variant trueText) {
		return new MailMergeField(Dispatch.call(this, "AddIf", range, mergeField, new Variant(comparison), compareTo, trueAutoText, trueText).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @param compareTo an input-parameter of type Variant
	 * @param trueAutoText an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addIf(Range range, String mergeField, int comparison, Variant compareTo, Variant trueAutoText) {
		return new MailMergeField(Dispatch.call(this, "AddIf", range, mergeField, new Variant(comparison), compareTo, trueAutoText).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @param compareTo an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addIf(Range range, String mergeField, int comparison, Variant compareTo) {
		return new MailMergeField(Dispatch.call(this, "AddIf", range, mergeField, new Variant(comparison), compareTo).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addIf(Range range, String mergeField, int comparison) {
		return new MailMergeField(Dispatch.call(this, "AddIf", range, mergeField, new Variant(comparison)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addMergeRec(Range lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddMergeRec", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addMergeSeq(Range lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddMergeSeq", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Range
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addNext(Range lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddNext", lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addNextIf(Range range, String mergeField, int comparison, Variant lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddNextIf", range, mergeField, new Variant(comparison), lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addNextIf(Range range, String mergeField, int comparison) {
		return new MailMergeField(Dispatch.call(this, "AddNextIf", range, mergeField, new Variant(comparison)).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param name an input-parameter of type String
	 * @param valueText an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addSet(Range range, String name, Variant valueText, Variant lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddSet", range, name, valueText, lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param name an input-parameter of type String
	 * @param valueText an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addSet(Range range, String name, Variant valueText) {
		return new MailMergeField(Dispatch.call(this, "AddSet", range, name, valueText).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param name an input-parameter of type String
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addSet(Range range, String name) {
		return new MailMergeField(Dispatch.call(this, "AddSet", range, name).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addSkipIf(Range range, String mergeField, int comparison, Variant lastParam) {
		return new MailMergeField(Dispatch.call(this, "AddSkipIf", range, mergeField, new Variant(comparison), lastParam).toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param range an input-parameter of type Range
	 * @param mergeField an input-parameter of type String
	 * @param comparison an input-parameter of type int
	 * @return the result is of type MailMergeField
	 */
	public MailMergeField addSkipIf(Range range, String mergeField, int comparison) {
		return new MailMergeField(Dispatch.call(this, "AddSkipIf", range, mergeField, new Variant(comparison)).toDispatch());
	}

}
