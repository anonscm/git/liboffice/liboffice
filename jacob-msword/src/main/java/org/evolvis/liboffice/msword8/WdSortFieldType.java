/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdSortFieldType {

	public static final int wdSortFieldAlphanumeric = 0;
	public static final int wdSortFieldNumeric = 1;
	public static final int wdSortFieldDate = 2;
}
