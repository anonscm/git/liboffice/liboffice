/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdListGalleryType {

	public static final int wdBulletGallery = 1;
	public static final int wdNumberGallery = 2;
	public static final int wdOutlineNumberGallery = 3;
}
