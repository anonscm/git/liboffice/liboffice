/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Application extends _Application {

	public static final String componentName = "Word.Application";

	public Application() {
		super(componentName);
	}

	public Application(Dispatch d) {
		super(d);
	}
}
