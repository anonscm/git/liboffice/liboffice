/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdFindWrap {

	public static final int wdFindStop = 0;
	public static final int wdFindContinue = 1;
	public static final int wdFindAsk = 2;
}
