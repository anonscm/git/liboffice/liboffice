/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdCharacterCaseHID {

	public static final int wdHalfWidth = 6;
	public static final int wdFullWidth = 7;
	public static final int wdKatakana = 8;
	public static final int wdHiragana = 9;
}
