/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdPageNumberStyle {

	public static final int wdPageNumberStyleArabic = 0;
	public static final int wdPageNumberStyleUppercaseRoman = 1;
	public static final int wdPageNumberStyleLowercaseRoman = 2;
	public static final int wdPageNumberStyleUppercaseLetter = 3;
	public static final int wdPageNumberStyleLowercaseLetter = 4;
}
