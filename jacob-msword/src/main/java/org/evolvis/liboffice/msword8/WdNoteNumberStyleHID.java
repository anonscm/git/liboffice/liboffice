/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdNoteNumberStyleHID {

	public static final int wdNoteNumberStyleArabicFullWidth = 14;
	public static final int wdNoteNumberStyleKanji = 10;
	public static final int wdNoteNumberStyleKanjiDigit = 11;
	public static final int wdNoteNumberStyleKanjiTraditional = 16;
	public static final int wdNoteNumberStyleNumberInCircle = 18;
	public static final int wdNoteNumberStyleHanjaRead = 41;
	public static final int wdNoteNumberStyleHanjaReadDigit = 42;
	public static final int wdNoteNumberStyleTradChinNum1 = 33;
	public static final int wdNoteNumberStyleTradChinNum2 = 34;
	public static final int wdNoteNumberStyleSimpChinNum1 = 37;
	public static final int wdNoteNumberStyleSimpChinNum2 = 38;
}
