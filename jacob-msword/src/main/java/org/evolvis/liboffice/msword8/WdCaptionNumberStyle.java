/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdCaptionNumberStyle {

	public static final int wdCaptionNumberStyleArabic = 0;
	public static final int wdCaptionNumberStyleUppercaseRoman = 1;
	public static final int wdCaptionNumberStyleLowercaseRoman = 2;
	public static final int wdCaptionNumberStyleUppercaseLetter = 3;
	public static final int wdCaptionNumberStyleLowercaseLetter = 4;
}
