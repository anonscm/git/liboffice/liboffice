/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdOrientation {

	public static final int wdOrientPortrait = 0;
	public static final int wdOrientLandscape = 1;
}
