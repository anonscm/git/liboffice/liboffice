/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class ThreeDFormat extends Dispatch {

	public static final String componentName = "Word.ThreeDFormat";

	public ThreeDFormat() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public ThreeDFormat(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public ThreeDFormat(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getDepth() {
		return Dispatch.get(this, "Depth").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setDepth(float lastParam) {
		Dispatch.call(this, "Depth", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ColorFormat
	 */
	public ColorFormat getExtrusionColor() {
		return new ColorFormat(Dispatch.get(this, "ExtrusionColor").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoExtrusionColorType
	 */
	/*
	public MsoExtrusionColorType getExtrusionColorType() {
		return new MsoExtrusionColorType(Dispatch.get(this, "ExtrusionColorType").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoExtrusionColorType
	 */
	/*
	public void setExtrusionColorType(MsoExtrusionColorType lastParam) {
		Dispatch.call(this, "ExtrusionColorType", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getPerspective() {
		return new MsoTriState(Dispatch.get(this, "Perspective").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setPerspective(MsoTriState lastParam) {
		Dispatch.call(this, "Perspective", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetExtrusionDirection
	 */
	/*
	public MsoPresetExtrusionDirection getPresetExtrusionDirection() {
		return new MsoPresetExtrusionDirection(Dispatch.get(this, "PresetExtrusionDirection").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetLightingDirection
	 */
	/*
	public MsoPresetLightingDirection getPresetLightingDirection() {
		return new MsoPresetLightingDirection(Dispatch.get(this, "PresetLightingDirection").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetLightingDirection
	 */
	/*
	public void setPresetLightingDirection(MsoPresetLightingDirection lastParam) {
		Dispatch.call(this, "PresetLightingDirection", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetLightingSoftness
	 */
	/*
	public MsoPresetLightingSoftness getPresetLightingSoftness() {
		return new MsoPresetLightingSoftness(Dispatch.get(this, "PresetLightingSoftness").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetLightingSoftness
	 */
	/*
	public void setPresetLightingSoftness(MsoPresetLightingSoftness lastParam) {
		Dispatch.call(this, "PresetLightingSoftness", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetMaterial
	 */
	/*
	public MsoPresetMaterial getPresetMaterial() {
		return new MsoPresetMaterial(Dispatch.get(this, "PresetMaterial").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetMaterial
	 */
	/*
	public void setPresetMaterial(MsoPresetMaterial lastParam) {
		Dispatch.call(this, "PresetMaterial", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetThreeDFormat
	 */
	/*
	public MsoPresetThreeDFormat getPresetThreeDFormat() {
		return new MsoPresetThreeDFormat(Dispatch.get(this, "PresetThreeDFormat").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getRotationX() {
		return Dispatch.get(this, "RotationX").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setRotationX(float lastParam) {
		Dispatch.call(this, "RotationX", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getRotationY() {
		return Dispatch.get(this, "RotationY").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setRotationY(float lastParam) {
		Dispatch.call(this, "RotationY", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getVisible() {
		return new MsoTriState(Dispatch.get(this, "Visible").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setVisible(MsoTriState lastParam) {
		Dispatch.call(this, "Visible", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void incrementRotationX(float lastParam) {
		Dispatch.call(this, "IncrementRotationX", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void incrementRotationY(float lastParam) {
		Dispatch.call(this, "IncrementRotationY", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void resetRotation() {
		Dispatch.call(this, "ResetRotation");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetExtrusionDirection
	 */
	/*
	public void setExtrusionDirection(MsoPresetExtrusionDirection lastParam) {
		Dispatch.call(this, "SetExtrusionDirection", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetThreeDFormat
	 */
	/*
	public void setThreeDFormat(MsoPresetThreeDFormat lastParam) {
		Dispatch.call(this, "SetThreeDFormat", lastParam);
	}
	*/

}
