/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdFootnoteLocation {

	public static final int wdBottomOfPage = 0;
	public static final int wdBeneathText = 1;
}
