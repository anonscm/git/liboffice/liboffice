/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class TextEffectFormat extends Dispatch {

	public static final String componentName = "Word.TextEffectFormat";

	public TextEffectFormat() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public TextEffectFormat(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public TextEffectFormat(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTextEffectAlignment
	 */
	/*
	public MsoTextEffectAlignment getAlignment() {
		return new MsoTextEffectAlignment(Dispatch.get(this, "Alignment").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTextEffectAlignment
	 */
	/*
	public void setAlignment(MsoTextEffectAlignment lastParam) {
		Dispatch.call(this, "Alignment", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getFontBold() {
		return new MsoTriState(Dispatch.get(this, "FontBold").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setFontBold(MsoTriState lastParam) {
		Dispatch.call(this, "FontBold", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getFontItalic() {
		return new MsoTriState(Dispatch.get(this, "FontItalic").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setFontItalic(MsoTriState lastParam) {
		Dispatch.call(this, "FontItalic", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getFontName() {
		return Dispatch.get(this, "FontName").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setFontName(String lastParam) {
		Dispatch.call(this, "FontName", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getFontSize() {
		return Dispatch.get(this, "FontSize").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setFontSize(float lastParam) {
		Dispatch.call(this, "FontSize", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getKernedPairs() {
		return new MsoTriState(Dispatch.get(this, "KernedPairs").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setKernedPairs(MsoTriState lastParam) {
		Dispatch.call(this, "KernedPairs", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getNormalizedHeight() {
		return new MsoTriState(Dispatch.get(this, "NormalizedHeight").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setNormalizedHeight(MsoTriState lastParam) {
		Dispatch.call(this, "NormalizedHeight", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetTextEffectShape
	 */
	/*
	public MsoPresetTextEffectShape getPresetShape() {
		return new MsoPresetTextEffectShape(Dispatch.get(this, "PresetShape").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetTextEffectShape
	 */
	/*
	public void setPresetShape(MsoPresetTextEffectShape lastParam) {
		Dispatch.call(this, "PresetShape", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetTextEffect
	 */
	/*
	public MsoPresetTextEffect getPresetTextEffect() {
		return new MsoPresetTextEffect(Dispatch.get(this, "PresetTextEffect").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoPresetTextEffect
	 */
	/*
	public void setPresetTextEffect(MsoPresetTextEffect lastParam) {
		Dispatch.call(this, "PresetTextEffect", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	/*
	public MsoTriState getRotatedChars() {
		return new MsoTriState(Dispatch.get(this, "RotatedChars").toDispatch());
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type MsoTriState
	 */
	/*
	public void setRotatedChars(MsoTriState lastParam) {
		Dispatch.call(this, "RotatedChars", lastParam);
	}
	*/

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getText() {
		return Dispatch.get(this, "Text").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type String
	 */
	public void setText(String lastParam) {
		Dispatch.call(this, "Text", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getTracking() {
		return Dispatch.get(this, "Tracking").toFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type float
	 */
	public void setTracking(float lastParam) {
		Dispatch.call(this, "Tracking", new Variant(lastParam));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void toggleVerticalText() {
		Dispatch.call(this, "ToggleVerticalText");
	}

}
