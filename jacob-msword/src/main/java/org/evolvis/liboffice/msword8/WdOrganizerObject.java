/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdOrganizerObject {

	public static final int wdOrganizerObjectStyles = 0;
	public static final int wdOrganizerObjectAutoText = 1;
	public static final int wdOrganizerObjectCommandBars = 2;
	public static final int wdOrganizerObjectProjectItems = 3;
}
