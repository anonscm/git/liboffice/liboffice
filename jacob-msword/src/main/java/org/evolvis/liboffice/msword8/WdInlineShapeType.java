/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdInlineShapeType {

	public static final int wdInlineShapeEmbeddedOLEObject = 1;
	public static final int wdInlineShapeLinkedOLEObject = 2;
	public static final int wdInlineShapePicture = 3;
	public static final int wdInlineShapeLinkedPicture = 4;
	public static final int wdInlineShapeOLEControlObject = 5;
}
