/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class Font extends _Font {

	public static final String componentName = "Word.Font";

	public Font() {
		super(componentName);
	}

	public Font(Dispatch d) {
		super(d);
	}
}
