/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public class List extends Dispatch {

	public static final String componentName = "Word.List";

	public List() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public List(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public List(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Range
	 */
	public Range getRange() {
		return new Range(Dispatch.get(this, "Range").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ListParagraphs
	 */
	public ListParagraphs getListParagraphs() {
		return new ListParagraphs(Dispatch.get(this, "ListParagraphs").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type boolean
	 */
	public boolean getSingleListTemplate() {
		return Dispatch.get(this, "SingleListTemplate").toBoolean();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void convertNumbersToText(Variant lastParam) {
		Dispatch.call(this, "ConvertNumbersToText", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void convertNumbersToText() {
		Dispatch.call(this, "ConvertNumbersToText");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type Variant
	 */
	public void removeNumbers(Variant lastParam) {
		Dispatch.call(this, "RemoveNumbers", lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void removeNumbers() {
		Dispatch.call(this, "RemoveNumbers");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param numberType an input-parameter of type Variant
	 * @param lastParam an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int countNumberedItems(Variant numberType, Variant lastParam) {
		return Dispatch.call(this, "CountNumberedItems", numberType, lastParam).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param numberType an input-parameter of type Variant
	 * @return the result is of type int
	 */
	public int countNumberedItems(Variant numberType) {
		return Dispatch.call(this, "CountNumberedItems", numberType).toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int countNumberedItems() {
		return Dispatch.call(this, "CountNumberedItems").toInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param listTemplate an input-parameter of type ListTemplate
	 * @param lastParam an input-parameter of type Variant
	 */
	public void applyListTemplate(ListTemplate listTemplate, Variant lastParam) {
		Dispatch.call(this, "ApplyListTemplate", listTemplate, lastParam);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param listTemplate an input-parameter of type ListTemplate
	 */
	public void applyListTemplate(ListTemplate listTemplate) {
		Dispatch.call(this, "ApplyListTemplate", listTemplate);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param lastParam an input-parameter of type ListTemplate
	 * @return the result is of type int
	 */
	public int canContinuePreviousList(ListTemplate lastParam) {
		return Dispatch.call(this, "CanContinuePreviousList", lastParam).toInt();
	}

}
