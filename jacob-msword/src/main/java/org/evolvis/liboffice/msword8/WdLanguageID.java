/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword8;

import com.jacob.com.*;

public interface WdLanguageID {

	public static final int wdLanguageNone = 0;
	public static final int wdNoProofing = 1024;
	public static final int wdDanish = 1030;
	public static final int wdGerman = 1031;
	public static final int wdSwissGerman = 2055;
	public static final int wdEnglishAUS = 3081;
	public static final int wdEnglishUK = 2057;
	public static final int wdEnglishUS = 1033;
	public static final int wdEnglishCanadian = 4105;
	public static final int wdEnglishNewZealand = 5129;
	public static final int wdEnglishSouthAfrica = 7177;
	public static final int wdSpanish = 1034;
	public static final int wdFrench = 1036;
	public static final int wdFrenchCanadian = 3084;
	public static final int wdItalian = 1040;
	public static final int wdDutch = 1043;
	public static final int wdNorwegianBokmol = 1044;
	public static final int wdNorwegianNynorsk = 2068;
	public static final int wdBrazilianPortuguese = 1046;
	public static final int wdPortuguese = 2070;
	public static final int wdFinnish = 1035;
	public static final int wdSwedish = 1053;
	public static final int wdCatalan = 1027;
	public static final int wdGreek = 1032;
	public static final int wdTurkish = 1055;
	public static final int wdRussian = 1049;
	public static final int wdCzech = 1029;
	public static final int wdHungarian = 1038;
	public static final int wdPolish = 1045;
	public static final int wdSlovenian = 1060;
	public static final int wdBasque = 1069;
	public static final int wdMalaysian = 1086;
	public static final int wdJapanese = 1041;
	public static final int wdKorean = 1042;
	public static final int wdSimplifiedChinese = 2052;
	public static final int wdTraditionalChinese = 1028;
	public static final int wdSwissFrench = 4108;
	public static final int wdSesotho = 1072;
	public static final int wdTsonga = 1073;
	public static final int wdTswana = 1074;
	public static final int wdVenda = 1075;
	public static final int wdXhosa = 1076;
	public static final int wdZulu = 1077;
	public static final int wdAfrikaans = 1078;
	public static final int wdArabic = 1025;
	public static final int wdHebrew = 1037;
	public static final int wdSlovak = 1051;
	public static final int wdFarsi = 1065;
	public static final int wdRomanian = 1048;
	public static final int wdCroatian = 1050;
	public static final int wdUkrainian = 1058;
	public static final int wdByelorussian = 1059;
	public static final int wdEstonian = 1061;
	public static final int wdLatvian = 1062;
	public static final int wdMacedonian = 1071;
	public static final int wdSerbianLatin = 2074;
	public static final int wdSerbianCyrillic = 3098;
	public static final int wdIcelandic = 1039;
	public static final int wdBelgianFrench = 2060;
	public static final int wdBelgianDutch = 2067;
	public static final int wdBulgarian = 1026;
	public static final int wdMexicanSpanish = 2058;
	public static final int wdSpanishModernSort = 3082;
	public static final int wdSwissItalian = 2064;
}
