/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdStyleType {

	public static final int wdStyleTypeParagraph = 1;
	public static final int wdStyleTypeCharacter = 2;
	public static final int wdStyleTypeTable = 3;
	public static final int wdStyleTypeList = 4;
}
