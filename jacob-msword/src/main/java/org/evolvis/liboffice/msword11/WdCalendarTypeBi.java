/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdCalendarTypeBi {

	public static final int wdCalendarTypeBidi = 99;
	public static final int wdCalendarTypeGregorian = 100;
}
