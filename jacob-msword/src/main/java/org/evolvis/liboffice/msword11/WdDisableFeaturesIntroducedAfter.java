/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdDisableFeaturesIntroducedAfter {

	public static final int wd70 = 0;
	public static final int wd70FE = 1;
	public static final int wd80 = 2;
}
