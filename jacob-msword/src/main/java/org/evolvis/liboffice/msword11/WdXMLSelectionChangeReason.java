/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdXMLSelectionChangeReason {

	public static final int wdXMLSelectionChangeReasonMove = 0;
	public static final int wdXMLSelectionChangeReasonInsert = 1;
	public static final int wdXMLSelectionChangeReasonDelete = 2;
}
