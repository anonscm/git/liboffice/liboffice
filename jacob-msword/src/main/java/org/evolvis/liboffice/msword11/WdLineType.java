/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdLineType {

	public static final int wdTextLine = 0;
	public static final int wdTableRow = 1;
}
