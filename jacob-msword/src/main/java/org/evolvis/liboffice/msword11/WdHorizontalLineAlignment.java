/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdHorizontalLineAlignment {

	public static final int wdHorizontalLineAlignLeft = 0;
	public static final int wdHorizontalLineAlignCenter = 1;
	public static final int wdHorizontalLineAlignRight = 2;
}
