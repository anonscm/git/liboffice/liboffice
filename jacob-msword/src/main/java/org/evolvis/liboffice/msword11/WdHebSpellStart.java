/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdHebSpellStart {

	public static final int wdFullScript = 0;
	public static final int wdPartialScript = 1;
	public static final int wdMixedScript = 2;
	public static final int wdMixedAuthorizedScript = 3;
}
