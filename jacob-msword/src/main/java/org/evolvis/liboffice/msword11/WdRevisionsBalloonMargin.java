/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRevisionsBalloonMargin {

	public static final int wdLeftMargin = 0;
	public static final int wdRightMargin = 1;
}
