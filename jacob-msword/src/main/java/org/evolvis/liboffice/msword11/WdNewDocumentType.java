/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdNewDocumentType {

	public static final int wdNewBlankDocument = 0;
	public static final int wdNewWebPage = 1;
	public static final int wdNewEmailMessage = 2;
	public static final int wdNewFrameset = 3;
	public static final int wdNewXMLDocument = 4;
}
