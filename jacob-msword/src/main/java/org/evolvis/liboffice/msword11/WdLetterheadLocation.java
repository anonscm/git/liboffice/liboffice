/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdLetterheadLocation {

	public static final int wdLetterTop = 0;
	public static final int wdLetterBottom = 1;
	public static final int wdLetterLeft = 2;
	public static final int wdLetterRight = 3;
}
