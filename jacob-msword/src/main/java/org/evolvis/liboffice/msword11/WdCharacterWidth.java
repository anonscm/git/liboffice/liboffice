/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdCharacterWidth {

	public static final int wdWidthHalfWidth = 6;
	public static final int wdWidthFullWidth = 7;
}
