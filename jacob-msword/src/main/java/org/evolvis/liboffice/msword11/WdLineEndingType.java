/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdLineEndingType {

	public static final int wdCRLF = 0;
	public static final int wdCROnly = 1;
	public static final int wdLFOnly = 2;
	public static final int wdLFCR = 3;
	public static final int wdLSPS = 4;
}
