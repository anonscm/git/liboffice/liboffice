/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdTemplateType {

	public static final int wdNormalTemplate = 0;
	public static final int wdGlobalTemplate = 1;
	public static final int wdAttachedTemplate = 2;
}
