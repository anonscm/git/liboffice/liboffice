/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdDocumentMedium {

	public static final int wdEmailMessage = 0;
	public static final int wdDocument = 1;
	public static final int wdWebPage = 2;
}
