/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdJustificationMode {

	public static final int wdJustificationModeExpand = 0;
	public static final int wdJustificationModeCompress = 1;
	public static final int wdJustificationModeCompressKana = 2;
}
