/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdEmailHTMLFidelity {

	public static final int wdEmailHTMLFidelityLow = 1;
	public static final int wdEmailHTMLFidelityMedium = 2;
	public static final int wdEmailHTMLFidelityHigh = 3;
}
