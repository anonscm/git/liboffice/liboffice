/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRevisionsBalloonWidthType {

	public static final int wdBalloonWidthPercent = 0;
	public static final int wdBalloonWidthPoints = 1;
}
