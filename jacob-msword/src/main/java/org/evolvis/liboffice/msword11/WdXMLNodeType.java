/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdXMLNodeType {

	public static final int wdXMLNodeElement = 1;
	public static final int wdXMLNodeAttribute = 2;
}
