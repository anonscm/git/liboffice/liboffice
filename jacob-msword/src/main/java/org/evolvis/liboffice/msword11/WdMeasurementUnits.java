/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdMeasurementUnits {

	public static final int wdInches = 0;
	public static final int wdCentimeters = 1;
	public static final int wdMillimeters = 2;
	public static final int wdPoints = 3;
	public static final int wdPicas = 4;
}
