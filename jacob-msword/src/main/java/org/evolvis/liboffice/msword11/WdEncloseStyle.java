/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdEncloseStyle {

	public static final int wdEncloseStyleNone = 0;
	public static final int wdEncloseStyleSmall = 1;
	public static final int wdEncloseStyleLarge = 2;
}
