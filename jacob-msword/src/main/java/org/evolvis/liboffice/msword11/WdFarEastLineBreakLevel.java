/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdFarEastLineBreakLevel {

	public static final int wdFarEastLineBreakLevelNormal = 0;
	public static final int wdFarEastLineBreakLevelStrict = 1;
	public static final int wdFarEastLineBreakLevelCustom = 2;
}
