/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdMailerPriority {

	public static final int wdPriorityNormal = 1;
	public static final int wdPriorityLow = 2;
	public static final int wdPriorityHigh = 3;
}
