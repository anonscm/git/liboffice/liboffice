/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdStyleSheetPrecedence {

	public static final int wdStyleSheetPrecedenceHigher = -1;
	public static final int wdStyleSheetPrecedenceLower = -2;
	public static final int wdStyleSheetPrecedenceHighest = 1;
	public static final int wdStyleSheetPrecedenceLowest = 0;
}
