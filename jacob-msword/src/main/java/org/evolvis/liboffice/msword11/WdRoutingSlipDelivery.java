/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRoutingSlipDelivery {

	public static final int wdOneAfterAnother = 0;
	public static final int wdAllAtOnce = 1;
}
