/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdProofreadingErrorType {

	public static final int wdSpellingError = 0;
	public static final int wdGrammaticalError = 1;
}
