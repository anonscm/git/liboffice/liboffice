/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdCollapseDirection {

	public static final int wdCollapseStart = 1;
	public static final int wdCollapseEnd = 0;
}
