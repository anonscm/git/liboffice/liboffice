/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdStyleSheetLinkType {

	public static final int wdStyleSheetLinkTypeLinked = 0;
	public static final int wdStyleSheetLinkTypeImported = 1;
}
