/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdEnclosureType {

	public static final int wdEnclosureCircle = 0;
	public static final int wdEnclosureSquare = 1;
	public static final int wdEnclosureTriangle = 2;
	public static final int wdEnclosureDiamond = 3;
}
