/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdProtectionType {

	public static final int wdNoProtection = -1;
	public static final int wdAllowOnlyRevisions = 0;
	public static final int wdAllowOnlyComments = 1;
	public static final int wdAllowOnlyFormFields = 2;
	public static final int wdAllowOnlyReading = 3;
}
