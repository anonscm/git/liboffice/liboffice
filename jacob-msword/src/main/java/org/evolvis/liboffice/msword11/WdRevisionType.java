/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRevisionType {

	public static final int wdNoRevision = 0;
	public static final int wdRevisionInsert = 1;
	public static final int wdRevisionDelete = 2;
	public static final int wdRevisionProperty = 3;
	public static final int wdRevisionParagraphNumber = 4;
	public static final int wdRevisionDisplayField = 5;
	public static final int wdRevisionReconcile = 6;
	public static final int wdRevisionConflict = 7;
	public static final int wdRevisionStyle = 8;
	public static final int wdRevisionReplace = 9;
	public static final int wdRevisionParagraphProperty = 10;
	public static final int wdRevisionTableProperty = 11;
	public static final int wdRevisionSectionProperty = 12;
	public static final int wdRevisionStyleDefinition = 13;
}
