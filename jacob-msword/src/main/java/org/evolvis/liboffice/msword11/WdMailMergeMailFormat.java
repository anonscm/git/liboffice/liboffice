/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdMailMergeMailFormat {

	public static final int wdMailFormatPlainText = 0;
	public static final int wdMailFormatHTML = 1;
}
