/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdCaptionLabelID {

	public static final int wdCaptionFigure = -1;
	public static final int wdCaptionTable = -2;
	public static final int wdCaptionEquation = -3;
}
