/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdIndexSortBy {

	public static final int wdIndexSortByStroke = 0;
	public static final int wdIndexSortBySyllable = 1;
}
