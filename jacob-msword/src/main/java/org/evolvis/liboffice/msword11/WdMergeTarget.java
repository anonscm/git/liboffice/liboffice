/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdMergeTarget {

	public static final int wdMergeTargetSelected = 0;
	public static final int wdMergeTargetCurrent = 1;
	public static final int wdMergeTargetNew = 2;
}
