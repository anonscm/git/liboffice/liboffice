/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdPrintOutPages {

	public static final int wdPrintAllPages = 0;
	public static final int wdPrintOddPagesOnly = 1;
	public static final int wdPrintEvenPagesOnly = 2;
}
