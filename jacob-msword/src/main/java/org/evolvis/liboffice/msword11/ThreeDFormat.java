/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class ThreeDFormat extends Dispatch {

	public static final String componentName = "Word.ThreeDFormat";

	public ThreeDFormat() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public ThreeDFormat(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public ThreeDFormat(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").changeType(Variant.VariantInt).getInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getDepth() {
		return Dispatch.get(this, "Depth").changeType(Variant.VariantFloat).getFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param depth an input-parameter of type float
	 */
	public void setDepth(float depth) {
		Dispatch.put(this, "Depth", new Variant(depth));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ColorFormat
	 */
	public ColorFormat getExtrusionColor() {
		return new ColorFormat(Dispatch.get(this, "ExtrusionColor").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoExtrusionColorType
	 */
	//TODO:
//	public MsoExtrusionColorType getExtrusionColorType() {
//		return new MsoExtrusionColorType(Dispatch.get(this, "ExtrusionColorType").toDispatch());
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @param extrusionColorType an input-parameter of type MsoExtrusionColorType
//	 */
//	public void setExtrusionColorType(MsoExtrusionColorType extrusionColorType) {
//		Dispatch.put(this, "ExtrusionColorType", extrusionColorType);
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @return the result is of type MsoTriState
//	 */
//	public MsoTriState getPerspective() {
//		return new MsoTriState(Dispatch.get(this, "Perspective").toDispatch());
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @param perspective an input-parameter of type MsoTriState
//	 */
//	public void setPerspective(MsoTriState perspective) {
//		Dispatch.put(this, "Perspective", perspective);
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @return the result is of type MsoPresetExtrusionDirection
//	 */
//	public MsoPresetExtrusionDirection getPresetExtrusionDirection() {
//		return new MsoPresetExtrusionDirection(Dispatch.get(this, "PresetExtrusionDirection").toDispatch());
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @return the result is of type MsoPresetLightingDirection
//	 */
//	public MsoPresetLightingDirection getPresetLightingDirection() {
//		return new MsoPresetLightingDirection(Dispatch.get(this, "PresetLightingDirection").toDispatch());
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @param presetLightingDirection an input-parameter of type MsoPresetLightingDirection
//	 */
//	public void setPresetLightingDirection(MsoPresetLightingDirection presetLightingDirection) {
//		Dispatch.put(this, "PresetLightingDirection", presetLightingDirection);
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @return the result is of type MsoPresetLightingSoftness
//	 */
//	public MsoPresetLightingSoftness getPresetLightingSoftness() {
//		return new MsoPresetLightingSoftness(Dispatch.get(this, "PresetLightingSoftness").toDispatch());
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @param presetLightingSoftness an input-parameter of type MsoPresetLightingSoftness
//	 */
//	public void setPresetLightingSoftness(MsoPresetLightingSoftness presetLightingSoftness) {
//		Dispatch.put(this, "PresetLightingSoftness", presetLightingSoftness);
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @return the result is of type MsoPresetMaterial
//	 */
//	public MsoPresetMaterial getPresetMaterial() {
//		return new MsoPresetMaterial(Dispatch.get(this, "PresetMaterial").toDispatch());
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @param presetMaterial an input-parameter of type MsoPresetMaterial
//	 */
//	public void setPresetMaterial(MsoPresetMaterial presetMaterial) {
//		Dispatch.put(this, "PresetMaterial", presetMaterial);
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @return the result is of type MsoPresetThreeDFormat
//	 */
//	public MsoPresetThreeDFormat getPresetThreeDFormat() {
//		return new MsoPresetThreeDFormat(Dispatch.get(this, "PresetThreeDFormat").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getRotationX() {
		return Dispatch.get(this, "RotationX").changeType(Variant.VariantFloat).getFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param rotationX an input-parameter of type float
	 */
	public void setRotationX(float rotationX) {
		Dispatch.put(this, "RotationX", new Variant(rotationX));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getRotationY() {
		return Dispatch.get(this, "RotationY").changeType(Variant.VariantFloat).getFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param rotationY an input-parameter of type float
	 */
	public void setRotationY(float rotationY) {
		Dispatch.put(this, "RotationY", new Variant(rotationY));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	//TODO:
//	public MsoTriState getVisible() {
//		return new MsoTriState(Dispatch.get(this, "Visible").toDispatch());
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @param visible an input-parameter of type MsoTriState
//	 */
//	public void setVisible(MsoTriState visible) {
//		Dispatch.put(this, "Visible", visible);
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param increment an input-parameter of type float
	 */
	public void incrementRotationX(float increment) {
		Dispatch.call(this, "IncrementRotationX", new Variant(increment));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param increment an input-parameter of type float
	 */
	public void incrementRotationY(float increment) {
		Dispatch.call(this, "IncrementRotationY", new Variant(increment));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void resetRotation() {
		Dispatch.call(this, "ResetRotation");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param presetExtrusionDirection an input-parameter of type MsoPresetExtrusionDirection
	 */
	//TODO:
//	public void setExtrusionDirection(MsoPresetExtrusionDirection presetExtrusionDirection) {
//		Dispatch.call(this, "SetExtrusionDirection", presetExtrusionDirection);
//	}
//
//	/**
//	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
//	 * @param presetThreeDFormat an input-parameter of type MsoPresetThreeDFormat
//	 */
//	public void setThreeDFormat(MsoPresetThreeDFormat presetThreeDFormat) {
//		Dispatch.call(this, "SetThreeDFormat", presetThreeDFormat);
//	}

}
