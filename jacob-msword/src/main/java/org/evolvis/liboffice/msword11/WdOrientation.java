/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdOrientation {

	public static final int wdOrientPortrait = 0;
	public static final int wdOrientLandscape = 1;
}
