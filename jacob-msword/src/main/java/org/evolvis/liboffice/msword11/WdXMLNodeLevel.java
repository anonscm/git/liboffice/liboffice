/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdXMLNodeLevel {

	public static final int wdXMLNodeLevelInline = 0;
	public static final int wdXMLNodeLevelParagraph = 1;
	public static final int wdXMLNodeLevelRow = 2;
	public static final int wdXMLNodeLevelCell = 3;
}
