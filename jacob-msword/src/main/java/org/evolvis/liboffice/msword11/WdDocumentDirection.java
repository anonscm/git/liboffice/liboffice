/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdDocumentDirection {

	public static final int wdLeftToRight = 0;
	public static final int wdRightToLeft = 1;
}
