/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdXMLValidationStatus {

	public static final int wdXMLValidationStatusOK = 0;
	public static final int wdXMLValidationStatusCustom = -1072898048;
}
