/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdPreferredWidthType {

	public static final int wdPreferredWidthAuto = 1;
	public static final int wdPreferredWidthPercent = 2;
	public static final int wdPreferredWidthPoints = 3;
}
