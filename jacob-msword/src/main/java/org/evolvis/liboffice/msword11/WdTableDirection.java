/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdTableDirection {

	public static final int wdTableDirectionRtl = 0;
	public static final int wdTableDirectionLtr = 1;
}
