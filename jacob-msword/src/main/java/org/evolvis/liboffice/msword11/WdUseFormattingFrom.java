/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdUseFormattingFrom {

	public static final int wdFormattingFromCurrent = 0;
	public static final int wdFormattingFromSelected = 1;
	public static final int wdFormattingFromPrompt = 2;
}
