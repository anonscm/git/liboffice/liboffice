/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdCompareTarget {

	public static final int wdCompareTargetSelected = 0;
	public static final int wdCompareTargetCurrent = 1;
	public static final int wdCompareTargetNew = 2;
}
