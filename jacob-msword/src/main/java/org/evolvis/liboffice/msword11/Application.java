/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;

import com.jacob.com.Dispatch;

public class Application extends _Application {

	public static final String componentName = "clsid:{000209FF-0000-0000-C000-000000000046}";

	public Application() {
		super(componentName);
	}

	public Application(Dispatch d) {
		super(d);
	}
}
