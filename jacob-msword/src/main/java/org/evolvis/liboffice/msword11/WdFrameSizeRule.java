/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdFrameSizeRule {

	public static final int wdFrameAuto = 0;
	public static final int wdFrameAtLeast = 1;
	public static final int wdFrameExact = 2;
}
