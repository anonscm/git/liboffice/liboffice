/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdContinue {

	public static final int wdContinueDisabled = 0;
	public static final int wdResetList = 1;
	public static final int wdContinueList = 2;
}
