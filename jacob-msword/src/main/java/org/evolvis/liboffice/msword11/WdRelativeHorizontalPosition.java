/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRelativeHorizontalPosition {

	public static final int wdRelativeHorizontalPositionMargin = 0;
	public static final int wdRelativeHorizontalPositionPage = 1;
	public static final int wdRelativeHorizontalPositionColumn = 2;
	public static final int wdRelativeHorizontalPositionCharacter = 3;
}
