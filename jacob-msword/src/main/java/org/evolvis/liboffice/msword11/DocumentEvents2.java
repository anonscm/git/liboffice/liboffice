/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class DocumentEvents2 extends Dispatch {

	public static final String componentName = "Word.DocumentEvents2";

	public DocumentEvents2() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public DocumentEvents2(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public DocumentEvents2(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void m_new() {
		Dispatch.call(this, "New");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void open() {
		Dispatch.call(this, "Open");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void close() {
		Dispatch.call(this, "Close");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param syncEventType an input-parameter of type MsoSyncEventType
	 */
	//TODO:
//	public void sync(MsoSyncEventType syncEventType) {
//		Dispatch.call(this, "Sync", syncEventType);
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param newXMLNode an input-parameter of type XMLNode
	 * @param inUndoRedo an input-parameter of type boolean
	 */
	public void xMLAfterInsert(XMLNode newXMLNode, boolean inUndoRedo) {
		Dispatch.call(this, "XMLAfterInsert", newXMLNode, new Variant(inUndoRedo));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param deletedRange an input-parameter of type Range
	 * @param oldXMLNode an input-parameter of type XMLNode
	 * @param inUndoRedo an input-parameter of type boolean
	 */
	public void xMLBeforeDelete(Range deletedRange, XMLNode oldXMLNode, boolean inUndoRedo) {
		Dispatch.call(this, "XMLBeforeDelete", deletedRange, oldXMLNode, new Variant(inUndoRedo));
	}

}
