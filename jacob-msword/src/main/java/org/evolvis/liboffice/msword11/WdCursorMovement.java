/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdCursorMovement {

	public static final int wdCursorMovementLogical = 0;
	public static final int wdCursorMovementVisual = 1;
}
