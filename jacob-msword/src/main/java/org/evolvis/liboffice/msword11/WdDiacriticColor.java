/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdDiacriticColor {

	public static final int wdDiacriticColorBidi = 0;
	public static final int wdDiacriticColorLatin = 1;
}
