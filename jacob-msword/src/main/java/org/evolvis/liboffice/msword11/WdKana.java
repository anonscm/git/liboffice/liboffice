/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdKana {

	public static final int wdKanaKatakana = 8;
	public static final int wdKanaHiragana = 9;
}
