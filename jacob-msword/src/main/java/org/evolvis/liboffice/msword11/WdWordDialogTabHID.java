/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdWordDialogTabHID {

	public static final int wdDialogFilePageSetupTabPaperSize = 150001;
	public static final int wdDialogFilePageSetupTabPaperSource = 150002;
}
