/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;

import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class FillFormat extends Dispatch {

	public static final String componentName = "Word.FillFormat";

	public FillFormat() {
		super(componentName);
	}

	/**
	* This constructor is used instead of a case operation to
	* turn a Dispatch object into a wider object - it must exist
	* in every wrapper class whose instances may be returned from
	* method calls wrapped in VT_DISPATCH Variants.
	*/
	public FillFormat(Dispatch d) {
		// take over the IDispatch pointer
		m_pDispatch = d.m_pDispatch;
		// null out the input's pointer
		d.m_pDispatch = 0;
	}

	public FillFormat(String compName) {
		super(compName);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Application
	 */
	public Application getApplication() {
		return new Application(Dispatch.get(this, "Application").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getCreator() {
		return Dispatch.get(this, "Creator").changeType(Variant.VariantInt).getInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type Object
	 */
	public Object getParent() {
		return Dispatch.get(this, "Parent");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ColorFormat
	 */
	public ColorFormat getBackColor() {
		return new ColorFormat(Dispatch.get(this, "BackColor").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type ColorFormat
	 */
	public ColorFormat getForeColor() {
		return new ColorFormat(Dispatch.get(this, "ForeColor").toDispatch());
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoGradientColorType
	 */
	//TODO:
//	public MsoGradientColorType getGradientColorType() {
//		return new MsoGradientColorType(Dispatch.get(this, "GradientColorType").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getGradientDegree() {
		return Dispatch.get(this, "GradientDegree").changeType(Variant.VariantFloat).getFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoGradientStyle
	 */
	//TODO:
//	public MsoGradientStyle getGradientStyle() {
//		return new MsoGradientStyle(Dispatch.get(this, "GradientStyle").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type int
	 */
	public int getGradientVariant() {
		return Dispatch.get(this, "GradientVariant").changeType(Variant.VariantInt).getInt();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPatternType
	 */
	//TODO:
//	public MsoPatternType getPattern() {
//		return new MsoPatternType(Dispatch.get(this, "Pattern").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetGradientType
	 */
	//TODO:
//	public MsoPresetGradientType getPresetGradientType() {
//		return new MsoPresetGradientType(Dispatch.get(this, "PresetGradientType").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoPresetTexture
	 */
	//TODO:
//	public MsoPresetTexture getPresetTexture() {
//		return new MsoPresetTexture(Dispatch.get(this, "PresetTexture").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type String
	 */
	public String getTextureName() {
		return Dispatch.get(this, "TextureName").toString();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTextureType
	 */
	//TODO:
//	public MsoTextureType getTextureType() {
//		return new MsoTextureType(Dispatch.get(this, "TextureType").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type float
	 */
	public float getTransparency() {
		return Dispatch.get(this, "Transparency").changeType(Variant.VariantFloat).getFloat();
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param transparency an input-parameter of type float
	 */
	public void setTransparency(float transparency) {
		Dispatch.put(this, "Transparency", new Variant(transparency));
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoFillType
	 */
	//TODO:
//	public MsoFillType getType() {
//		return new MsoFillType(Dispatch.get(this, "Type").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @return the result is of type MsoTriState
	 */
	//TODO:
//	public MsoTriState getVisible() {
//		return new MsoTriState(Dispatch.get(this, "Visible").toDispatch());
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param visible an input-parameter of type MsoTriState
	 */
	//TODO:
//	public void setVisible(MsoTriState visible) {
//		Dispatch.put(this, "Visible", visible);
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void background() {
		Dispatch.call(this, "Background");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param style an input-parameter of type MsoGradientStyle
	 * @param variant an input-parameter of type int
	 * @param degree an input-parameter of type float
	 */
	//TODO:
//	public void oneColorGradient(MsoGradientStyle style, int variant, float degree) {
//		Dispatch.call(this, "OneColorGradient", style, new Variant(variant), new Variant(degree));
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param pattern an input-parameter of type MsoPatternType
	 */
	//TODO:
//	public void patterned(MsoPatternType pattern) {
//		Dispatch.call(this, "Patterned", pattern);
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param style an input-parameter of type MsoGradientStyle
	 * @param variant an input-parameter of type int
	 * @param presetGradientType an input-parameter of type MsoPresetGradientType
	 */
	//TODO:
//	public void presetGradient(MsoGradientStyle style, int variant, MsoPresetGradientType presetGradientType) {
//		Dispatch.call(this, "PresetGradient", style, new Variant(variant), presetGradientType);
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param presetTexture an input-parameter of type MsoPresetTexture
	 */
	//TODO:
//	public void presetTextured(MsoPresetTexture presetTexture) {
//		Dispatch.call(this, "PresetTextured", presetTexture);
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 */
	public void solid() {
		Dispatch.call(this, "Solid");
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param style an input-parameter of type MsoGradientStyle
	 * @param variant an input-parameter of type int
	 */
	//TODO:
//	public void twoColorGradient(MsoGradientStyle style, int variant) {
//		Dispatch.call(this, "TwoColorGradient", style, new Variant(variant));
//	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param pictureFile an input-parameter of type String
	 */
	public void userPicture(String pictureFile) {
		Dispatch.call(this, "UserPicture", pictureFile);
	}

	/**
	 * Wrapper for calling the ActiveX-Method with input-parameter(s).
	 * @param textureFile an input-parameter of type String
	 */
	public void userTextured(String textureFile) {
		Dispatch.call(this, "UserTextured", textureFile);
	}

}
