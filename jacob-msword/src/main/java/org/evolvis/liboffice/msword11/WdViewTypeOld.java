/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdViewTypeOld {

	public static final int wdPageView = 3;
	public static final int wdOnlineView = 6;
}
