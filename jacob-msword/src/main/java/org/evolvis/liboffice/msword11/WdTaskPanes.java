/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdTaskPanes {

	public static final int wdTaskPaneFormatting = 0;
	public static final int wdTaskPaneRevealFormatting = 1;
	public static final int wdTaskPaneMailMerge = 2;
	public static final int wdTaskPaneTranslate = 3;
	public static final int wdTaskPaneSearch = 4;
	public static final int wdTaskPaneXMLStructure = 5;
	public static final int wdTaskPaneDocumentProtection = 6;
	public static final int wdTaskPaneDocumentActions = 7;
	public static final int wdTaskPaneSharedWorkspace = 8;
	public static final int wdTaskPaneHelp = 9;
	public static final int wdTaskPaneResearch = 10;
	public static final int wdTaskPaneFaxService = 11;
	public static final int wdTaskPaneXMLDocument = 12;
	public static final int wdTaskPaneDocumentUpdates = 13;
}
