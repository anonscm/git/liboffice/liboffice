/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdWrapTypeMerged {

	public static final int wdWrapMergeInline = 0;
	public static final int wdWrapMergeSquare = 1;
	public static final int wdWrapMergeTight = 2;
	public static final int wdWrapMergeBehind = 3;
	public static final int wdWrapMergeFront = 4;
	public static final int wdWrapMergeThrough = 5;
	public static final int wdWrapMergeTopBottom = 6;
}
