/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdHorizontalLineWidthType {

	public static final int wdHorizontalLinePercentWidth = -1;
	public static final int wdHorizontalLineFixedWidth = -2;
}
