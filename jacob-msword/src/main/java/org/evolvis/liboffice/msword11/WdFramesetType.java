/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdFramesetType {

	public static final int wdFramesetTypeFrameset = 0;
	public static final int wdFramesetTypeFrame = 1;
}
