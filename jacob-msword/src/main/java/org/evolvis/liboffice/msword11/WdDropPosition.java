/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdDropPosition {

	public static final int wdDropNone = 0;
	public static final int wdDropNormal = 1;
	public static final int wdDropMargin = 2;
}
