/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdListApplyTo {

	public static final int wdListApplyToWholeList = 0;
	public static final int wdListApplyToThisPointForward = 1;
	public static final int wdListApplyToSelection = 2;
}
