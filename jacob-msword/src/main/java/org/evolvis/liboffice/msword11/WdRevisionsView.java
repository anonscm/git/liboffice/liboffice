/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRevisionsView {

	public static final int wdRevisionsViewFinal = 0;
	public static final int wdRevisionsViewOriginal = 1;
}
