/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdPrintOutRange {

	public static final int wdPrintAllDocument = 0;
	public static final int wdPrintSelection = 1;
	public static final int wdPrintCurrentPage = 2;
	public static final int wdPrintFromTo = 3;
	public static final int wdPrintRangeOfPages = 4;
}
