/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdReadingOrder {

	public static final int wdReadingOrderRtl = 0;
	public static final int wdReadingOrderLtr = 1;
}
