/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRevisedPropertiesMark {

	public static final int wdRevisedPropertiesMarkNone = 0;
	public static final int wdRevisedPropertiesMarkBold = 1;
	public static final int wdRevisedPropertiesMarkItalic = 2;
	public static final int wdRevisedPropertiesMarkUnderline = 3;
	public static final int wdRevisedPropertiesMarkDoubleUnderline = 4;
	public static final int wdRevisedPropertiesMarkColorOnly = 5;
	public static final int wdRevisedPropertiesMarkStrikeThrough = 6;
}
