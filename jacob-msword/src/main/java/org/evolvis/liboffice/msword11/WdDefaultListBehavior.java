/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdDefaultListBehavior {

	public static final int wdWord8ListBehavior = 0;
	public static final int wdWord9ListBehavior = 1;
	public static final int wdWord10ListBehavior = 2;
}
