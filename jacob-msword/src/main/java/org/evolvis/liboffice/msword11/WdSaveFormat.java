/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdSaveFormat {

	public static final int wdFormatDocument = 0;
	public static final int wdFormatTemplate = 1;
	public static final int wdFormatText = 2;
	public static final int wdFormatTextLineBreaks = 3;
	public static final int wdFormatDOSText = 4;
	public static final int wdFormatDOSTextLineBreaks = 5;
	public static final int wdFormatRTF = 6;
	public static final int wdFormatUnicodeText = 7;
	public static final int wdFormatEncodedText = 7;
	public static final int wdFormatHTML = 8;
	public static final int wdFormatWebArchive = 9;
	public static final int wdFormatFilteredHTML = 10;
	public static final int wdFormatXML = 11;
}
