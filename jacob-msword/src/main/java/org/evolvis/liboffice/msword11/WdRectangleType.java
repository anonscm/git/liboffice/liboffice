/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdRectangleType {

	public static final int wdTextRectangle = 0;
	public static final int wdShapeRectangle = 1;
	public static final int wdMarkupRectangle = 2;
	public static final int wdMarkupRectangleButton = 3;
	public static final int wdPageBorderRectangle = 4;
	public static final int wdLineBetweenColumnRectangle = 5;
	public static final int wdSelection = 6;
	public static final int wdSystem = 7;
}
