/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdTrailingCharacter {

	public static final int wdTrailingTab = 0;
	public static final int wdTrailingSpace = 1;
	public static final int wdTrailingNone = 2;
}
