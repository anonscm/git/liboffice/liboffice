/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdTCSCConverterDirection {

	public static final int wdTCSCConverterDirectionSCTC = 0;
	public static final int wdTCSCConverterDirectionTCSC = 1;
	public static final int wdTCSCConverterDirectionAuto = 2;
}
