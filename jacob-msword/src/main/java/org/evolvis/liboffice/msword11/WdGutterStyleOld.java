/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdGutterStyleOld {

	public static final int wdGutterStyleLatin = -10;
	public static final int wdGutterStyleBidi = 2;
}
