/**
 * JacobGen generated file --- do not edit
 *
 * (http://www.sourceforge.net/projects/jacob-project */
package org.evolvis.liboffice.msword11;


public interface WdSubscriberFormats {

	public static final int wdSubscriberBestFormat = 0;
	public static final int wdSubscriberRTF = 1;
	public static final int wdSubscriberText = 2;
	public static final int wdSubscriberPict = 4;
}
