/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.ui;

import java.util.List;

import org.evolvis.liboffice.configuration.office.OfficeConfiguration;


/**
 * 
 * This interface describes methods for an initial-configuration dialog
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface FirstRunWizard
{
	/**
	 * Return value for methodChooseDialog if user selected the automatic-configuration
	 */
	public final static int RETURN_AUTO = 0;
	
	/**
	 * Return value for methodChooseDialog if user selected the manual-configuration
	 */
	public final static int RETURN_MANU = 1;
	
	/**
	 * Return value for methodChooseDialog if user canceled the process
	 */
	public final static int RETURN_CANCEL = 2;
	
	/**
	 * Shows a dialog which asks the user how to configure the office/printer-system
	 * @return one of RETURN_AUTO, RETURN_MANU or RETURN_CANCEL
	 */
	public int methodChooseDialog();
	
	/**
	 * Opens a dialog which tells the user that the automatic configuration did not found an office
	 *
	 */
	public void noInitSuccessDialog();
	
	/**
	 * Opens a dialog giving the user some information about the automatically found office-configuration
	 * @param pConf the configuration containing information about the found office 
	 *
	 */
	public void foundOfficeDialog(OfficeConfiguration pConf);
	
	/**
	 * Is called when the user selected manual-configuration before and creates an OfficeConfiguration-object manually by asking the user for relevant parameters
	 * @return an OfficeConfiguration-Object based on user-input
	 */
	public OfficeConfiguration manualInputDialog();
	
	/**
	 * If more than one office-system was found during automatic-configuration this dialog asks the user which one to prefer
	 * @param pOfficeSystems the available office-systems
	 * @return the preferred office-configuration the user has chosen
	 */
	public OfficeConfiguration officeSelectionDialog(List pOfficeSystems);
	
	/**
	 * Shows a dialog which asks the user if he wants to configure additional options
	 * @return the users selection
	 */
	public int additionalOptionsMessage();
	
	/**
	 * Opens a dialog in which the user can configure the connection-method
	 * @param pConf a OfficeConfiguration which should be set up
	 * @return the modified OfficeConfiguration
	 */
	public OfficeConfiguration connMethodChooseDialog(OfficeConfiguration pConf);
}
