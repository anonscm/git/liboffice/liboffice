/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 22.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.text;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualTextFont
{
  private String m_sFontName;
  private double m_iFontHeight = 10.0;
  private boolean m_bIsBold = false;
  private boolean m_bIsItalic = false;
  private boolean m_bIsUnderline = false;
  
 
  // ------------------------------------------
    
  public VirtualTextFont()
  {    
  }
  
  public VirtualTextFont(String fontname)
  {    
    setFontName(fontname);
  }
  
  public VirtualTextFont(String fontname, double size)
  {    
    setFontName(fontname);
    setFontHeight(size);
  }
  
  // ------------------------------------------
  
  
  public void setFontName(String name)
  {
    m_sFontName = name;
  }
  
  public void setFontHeight(double height)
  {
    m_iFontHeight = height;
  }
    
  public double getFontHeight()
  {
    return m_iFontHeight;
  }
  
  public String getFontName()
  {
    return m_sFontName;
  }
  
  
  public boolean isBold()
  {
    return m_bIsBold;
  }

  public boolean isItalic()
  {
    return m_bIsItalic;
  }
  
  public boolean isUnderline()
  {
    return m_bIsUnderline;
  }
  
  public void setBold(boolean bold)
  {
    m_bIsBold = bold;
  }

  public void setItalic(boolean italic)
  {
    m_bIsItalic = italic;
  }
  
  public void setUnderline(boolean underline)
  {
    m_bIsUnderline = underline;
  }
  
  
  public String toString()
  {
    return "<font name=\"" + m_sFontName + "\" height=\"" + m_iFontHeight + "\" bold=\"" + m_bIsBold + "\" italic=\"" + m_bIsItalic + "\" underline=\"" + m_bIsUnderline + "\"/>";
  }
  
  /**
	 * This method implements the <code>Cloneable</code>-Interface
	 * 
	 */
	
	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch(Exception pExcp)
		{
			throw new InternalError();
		}
	}
}
