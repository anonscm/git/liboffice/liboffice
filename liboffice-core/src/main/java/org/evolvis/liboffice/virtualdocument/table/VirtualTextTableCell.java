/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 22.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.table;

import org.evolvis.liboffice.virtualdocument.Styleable;
import org.evolvis.liboffice.virtualdocument.VirtualBorder;
import org.evolvis.liboffice.virtualdocument.VirtualMargin;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualTextTableCell implements VirtualTextComponent
{
	
	private VirtualStyledText m_oText                    = null;
	
	private VirtualBorder m_oBorderTop                   = null;
	private VirtualBorder m_oBorderLeft                  = null;
	private VirtualBorder m_oBorderRight                 = null;
	private VirtualBorder m_oBorderBottom                = null;
	
	private VirtualMargin m_oMarginTop                   = null;
	private VirtualMargin m_oMarginLeft                  = null;
	private VirtualMargin m_oMarginRight                 = null;
	private VirtualMargin m_oMarginBottom                = null;
	
	private int rowspan					               = 1;
	private int colspan					               = 1;
	
	private Object m_oHorizontalJustification            = HORIZONTAL_JUSTIFY_NONE;
	private Object m_oVerticalJustification              = VERTICAL_JUSTIFY_NONE;
	
	public final static Object HORIZONTAL_JUSTIFY_NONE   = "HORIZONTAL_JUSTIFY_NONE";
	public final static Object HORIZONTAL_JUSTIFY_LEFT   = "HORIZONTAL_JUSTIFY_LEFT";
	public final static Object HORIZONTAL_JUSTIFY_RIGHT  = "HORIZONTAL_JUSTIFY_RIGHT";
	public final static Object HORIZONTAL_JUSTIFY_CENTER = "HORIZONTAL_JUSTIFY_CENTER";
	
	public final static Object VERTICAL_JUSTIFY_NONE     = "VERTICAL_JUSTIFY_NONE";
	public final static Object VERTICAL_JUSTIFY_TOP      = "VERTICAL_JUSTIFY_TOP";
	public final static Object VERTICAL_JUSTIFY_BOTTOM   = "VERTICAL_JUSTIFY_BOTTOM";
	public final static Object VERTICAL_JUSTIFY_CENTER   = "VERTICAL_JUSTIFY_CENTER";
	
	// ----------------------------------------
	
	public VirtualTextTableCell()
	{    
	}
	
	public VirtualTextTableCell(VirtualStyledText styledtext)
	{    
		setStyledText(styledtext);
	}
	
	public VirtualTextTableCell(String text)
	{    
		setStyledText(new VirtualStyledText(text));
	}
	
	public VirtualTextTableCell(String text, String stylename)
	{    
		setStyledText(new VirtualStyledText(text, stylename));
	}
	
	public VirtualTextTableCell(String text, String styleName, int rowspan, int colspan)
	{
		setStyledText(new VirtualStyledText(text, styleName));
		setRowspan( rowspan );
		setColspan( colspan );
	}
	
	public VirtualTextTableCell(String text, VirtualTextStyle style)
	{    
		setStyledText(new VirtualStyledText(text, style));
	}
	
	
	public VirtualTextTableCell(VirtualTextPortionMarker marker)
	{    
		VirtualStyledText text = new VirtualStyledText(marker);
		setStyledText(text);
	}
	
	// ----------------------------------------
	// ----------------------------------------
	// ----------------------------------------
	
	public VirtualBorder getBorderTop()
	{
		return m_oBorderTop;
	}
	
	public VirtualBorder getBorderLeft()
	{
		return m_oBorderLeft;
	}
	
	public VirtualBorder getBorderRight()
	{
		return m_oBorderRight;
	}
	
	public VirtualBorder getBorderBottom()
	{
		return m_oBorderBottom;
	}
	
	// ----------------------------------------
	
	public void setBorderTop(VirtualBorder border)
	{
		m_oBorderTop = border;
	}
	
	public void setBorderLeft(VirtualBorder border)
	{
		m_oBorderLeft = border;
	}
	
	public void setBorderRight(VirtualBorder border)
	{
		m_oBorderRight = border;
	}
	
	public void setBorderBottom(VirtualBorder border)
	{
		m_oBorderBottom = border;
	}
	
	// ----------------------------------------
	// ----------------------------------------
	
	public VirtualMargin getMarginTop()
	{
		return m_oMarginTop;
	}
	
	public VirtualMargin getMarginLeft()
	{
		return m_oMarginLeft;
	}
	
	public VirtualMargin getMarginRight()
	{
		return m_oMarginRight;
	}
	
	public VirtualMargin getMarginBottom()
	{
		return m_oMarginBottom;
	}
	
	// ----------------------------------------
	
	public void setMarginTop(VirtualMargin border)
	{
		m_oMarginTop = border;
	}
	
	public void setMarginLeft(VirtualMargin border)
	{
		m_oMarginLeft = border;
	}
	
	public void setMarginRight(VirtualMargin border)
	{
		m_oMarginRight = border;
	}
	
	public void setMarginBottom(VirtualMargin border)
	{
		m_oMarginBottom = border;
	}
	
	// ----------------------------------------
	// ----------------------------------------
	
	
	public VirtualStyledText getStyledText()
	{
		return m_oText;
	}
	
	public void setStyledText(VirtualStyledText st)
	{
		m_oText = st;
	}    
	
	public void setHorizontalJustification(Object justification)
	{
		m_oHorizontalJustification = justification;    
	}
	
	public void setVerticalJustification(Object justification)
	{
		m_oVerticalJustification = justification;    
	}
	
	public Object getHorizontalJustification()
	{
		return m_oHorizontalJustification;    
	}
	
	public Object getVerticalJustification()
	{
		return m_oVerticalJustification;    
	}
	
	
	
	/**
	 * @return Returns the colspan.
	 */
	public int getColspan() 
	{
		return colspan;
	}
	
	/**
	 * @param colspan The colspan to set.
	 */
	public void setColspan(int colspan) 
	{
		this.colspan = colspan;
	}
	
	/**
	 * @return Returns the rowspan.
	 */
	public int getRowspan() 
	{
		return rowspan;
	}
	
	/**
	 * @param rowspan The rowspan to set.
	 */
	public void setRowspan(int rowspan) 
	{
		this.rowspan = rowspan;
	}
	
	
	public void setCellBorderLineVisible(boolean show)
	{
		setCellBorderLineVisible(show, show, show, show);
	}
	
	
	public void setCellBorderLineVisible(boolean showTop, boolean showLeft, boolean showRight, boolean showBottom)
	{
		if (getBorderTop() == null)    setBorderTop(new VirtualBorder(showTop));
		else getBorderTop().setVisible(showTop);
		
		if (getBorderLeft() == null)   setBorderLeft(new VirtualBorder(showLeft));
		else getBorderLeft().setVisible(showLeft);
		
		if (getBorderRight() == null)  setBorderRight(new VirtualBorder(showRight));
		else getBorderRight().setVisible(showRight);
		
		if (getBorderBottom() == null) setBorderBottom(new VirtualBorder(showBottom));
		else getBorderBottom().setVisible(showBottom);    
	}
	
	
	public String toString()
	{
		String text = "";
		text += "<VirtualTextTableCell horizontaljustify=\"" + m_oHorizontalJustification + "\" verticaljustify=\"" + m_oVerticalJustification + "\"";
		if (m_oBorderTop != null) text += " borderTop=\"" + m_oBorderTop + "\"";
		if (m_oBorderLeft != null) text += " borderLeft=\"" + m_oBorderLeft + "\"";
		if (m_oBorderRight != null) text += " borderRight=\"" + m_oBorderRight + "\"";
		if (m_oBorderBottom != null) text += " borderBottom=\"" + m_oBorderBottom + "\"";
		
		if (m_oMarginTop != null) text += " marginTop=\"" + m_oMarginTop + "\"";
		if (m_oMarginLeft != null) text += " marginLeft=\"" + m_oMarginLeft + "\"";
		if (m_oMarginRight != null) text += " marginRight=\"" + m_oMarginRight + "\"";
		if (m_oMarginBottom != null) text += " marginBottom=\"" + m_oMarginBottom + "\"";
		
		text += ">";
		if(m_oText != null) text += m_oText.toString();
		text += "</VirtualTextTableCell>";
		return text;
	}
	
	public boolean containsStyle(String styleName)
	{
		VirtualStyledText text   = getStyledText();
		for(int i=0; i<(text.getNumberOfParagraphs()); i++)
		{
			Styleable style = text.getParagraph(i);
			if (style != null)
			{
				if (null != styleName &&  style.containsStyle( styleName )) return true;                		
			}
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#findComponentByStyle(java.lang.String)
	 */
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		VirtualStyledText text   = getStyledText();
		for(int i=0; i<(text.getNumberOfParagraphs()); i++)
		{
			VirtualParagraph style = text.getParagraph(i);
			if (style != null)
			{
				if (null != styleName &&  style.containsStyle( styleName )) return style;                		
			}
		}
		return null;
	}
}
