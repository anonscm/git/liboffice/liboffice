/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 04.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.text;

import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualTextPortionMarker implements VirtualTextPortion
{
	private String m_sName;
	private String m_sHint;
	
	public VirtualTextPortionMarker(String name, String hint)
	{
		m_sName = name;
		m_sHint = hint;
	}
	
	public VirtualTextPortionMarker(String pName)
	{
		m_sName = pName;
	}
	
	public String getHint()
	{
		return m_sHint;
	}
	
	public void setHint(String pHint)
	{
		m_sHint = pHint;
	}
	
	public String getName()
	{
		return m_sName;
	}
	
	public void setName(String pName)
	{
		m_sName = pName;
	}
	
	public String toString()
	{
		return "<VirtualTextPortionMarker name=\"" + m_sName + "\" hint=\"" + m_sHint + "\"/>";   
	}
	
	
	public boolean containsStyle(String styleName)
	{
		return false;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#findComponentByStyle(java.lang.String)
	 */
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		return null;
	}
	
	/**
	 * This method implements the <code>Cloneable</code>-Interface
	 * 
	 */
	
	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch(Exception pExcp)
		{
			throw new InternalError();
		}
	}
}
