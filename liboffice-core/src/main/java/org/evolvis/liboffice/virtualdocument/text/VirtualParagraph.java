/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 22.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualParagraph implements VirtualTextComponent
{
	private List m_oPortions = new ArrayList();
	private VirtualTextStyle m_oVirtualTextStyle = null;
	private final static Logger logger = Logger.getLogger(VirtualParagraph.class.getName());
	// ---------------------------------
	
	public VirtualParagraph()
	{    
	}
	
	public VirtualParagraph(String text)
	{    
		this.addTextPortion(new VirtualTextPortionText(text));    
	}
	
	public VirtualParagraph(VirtualTextStyle style)
	{
		this.setStyle(style);
	}
	
	public VirtualParagraph(String text, VirtualTextStyle style)
	{
		this.addTextPortion(new VirtualTextPortionText( text ));
		this.setStyle( style );
	}
	
	public VirtualParagraph(String markername, String markerhint)  
	{    
		this.addTextPortion(new VirtualTextPortionMarker(markername, markerhint));    
	}
	
	public VirtualParagraph(VirtualTextPortion portion)
	{    
		this.addTextPortion(portion);    
	}
	
	// ---------------------------------
	
	public void addTextPortion(VirtualTextPortion textportion)
	{
		m_oPortions.add(textportion);
	}
	
	public void removeTextPortion(VirtualTextPortion textportion)
	{
		m_oPortions.remove(textportion);    
	}
	
	public void removeTextPortion(int index)
	{
		m_oPortions.remove( index );
	}
	
	public int getNumberOfTextPortions()
	{
		return m_oPortions.size();
	}
	
	public VirtualTextPortion getTextPortion(int index)
	{
		return (VirtualTextPortion)(m_oPortions.get(index));
	}
	
	/** liefert einen Iterator der TextPortions
	 * @return Iterator
	 */
	public Iterator iterator()
	{
		return m_oPortions.iterator();
	}
	
	public void setStyle(VirtualTextStyle style)
	{
		m_oVirtualTextStyle = style;
	}
	
	public VirtualTextStyle getStyle()
	{
		return m_oVirtualTextStyle;
	}
	
	public String getText()
	{
		String text = "";
		for(int i=0; i<(m_oPortions.size()); i++)
		{
			VirtualTextPortion vPortion = getTextPortion(i);
			if (vPortion instanceof VirtualTextPortionText)
			{
				text += (((VirtualTextPortionText)vPortion).getText()) + " ";
			}
		}
		return text.trim();
	}
	
	public void removeParagraphBreaks()
	{
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualTextPortion portion = (VirtualTextPortion)(it.next());
			if (portion instanceof VirtualParagraphBreak)
			{
				it.remove(); 
			}
		}
	}
	
	public boolean removeLastParagraphBreak()
	{
		for(int i=getNumberOfTextPortions()-1; i>=0; i--)
		{
			VirtualTextPortion portion = getTextPortion(i);
			if (portion instanceof VirtualParagraphBreak)
			{
				removeTextPortion(portion);
				return true;
			}
		}
		return false;
	}
	
	
	public boolean cleanParagraphBreaks()
	{
		if (getNumberOfTextPortions() > 0)
		{
			VirtualTextPortion portion = getTextPortion(getNumberOfTextPortions() - 1);
			if (portion instanceof VirtualParagraphBreak)
			{
				removeTextPortion(portion);
				return true;
			}
		}
		return false;
	}
	
	
	public String toString()
	{
		String text = "<VirtualParagraph>";
		if (m_oVirtualTextStyle != null) text += m_oVirtualTextStyle.toString();
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualTextPortion portion = (VirtualTextPortion)(it.next());
			text += portion.toString();
		}
		
		text += "</VirtualParagraph>";
		return text;
	}
	
	public boolean containsStyle(String styleName)
	{
		if(null != m_oVirtualTextStyle && m_oVirtualTextStyle.getStyleName().equalsIgnoreCase(styleName))
		{
			return true;
			
		}
		
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualTextComponent style = (VirtualTextComponent)(it.next());
			if(style.containsStyle(styleName))
			{
				return true;
			}
		}
		return false;
	}
	
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		if(null != m_oVirtualTextStyle && m_oVirtualTextStyle.getStyleName().equals(styleName))
		{
			if(m_oPortions.size() > 0)
			{
				VirtualTextComponent cmp =  (VirtualTextComponent) m_oPortions.get( 0 );
				if(null != cmp)
				{
					return cmp;
				}
			}
			
		}
		
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualTextComponent cmp = (VirtualTextComponent)(it.next());
			logger.fine("cmp: " + cmp);
			if(cmp.containsStyle(styleName))
			{
				return cmp;
			}
		}
		return null;
	}
	
	/**
	 * This method implements the <code>Cloneable</code>-Interface
	 * 
	 */
	
	public Object clone()
	{
		try
		{
			VirtualParagraph copy = new VirtualParagraph();
			
			for(int i=0; i < this.getNumberOfTextPortions(); i++)
			{
				copy.addTextPortion((VirtualTextPortion)this.getTextPortion(i).clone());
			}
			
			return copy;
		}
		catch(Exception pExcp)
		{
			throw new InternalError();
		}
	}
	
}