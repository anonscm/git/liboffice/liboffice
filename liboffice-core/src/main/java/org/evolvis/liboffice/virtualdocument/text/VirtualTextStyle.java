/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 22.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.text;



/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualTextStyle
{
	//public final static String STYLE_PARAGRAPH = "STYLE_PARAGRAPH";
	//public final static String STYLE_CHARACTER = "STYLE_CHARACTER";
	
	private String m_sStyleName;
	private VirtualTextStyleType m_sStyleType = null;
	private VirtualTextFont m_oVirtualTextFont = null;
	
	// ------------------------------------------
	
	public VirtualTextStyle()
	{    
	}
	
	public VirtualTextStyle(String stylename)
	{    
		setStyleName(stylename);
		m_sStyleType = VirtualTextStyleType.STYLE_PARAGRAPH;
	}
	
	public VirtualTextStyle(String stylename, VirtualTextStyleType type)
	{    
		setStyleName(stylename);
		m_sStyleType = type;
	}
	
	public VirtualTextStyle(String styleName, VirtualTextFont font)
	{
		setStyleName( styleName );
		setFont( font );
		m_sStyleType = VirtualTextStyleType.STYLE_PARAGRAPH; // default!
	}
	
	/**
	 * @param styleName der Name des Styles
	 * @param font der @see{VirtualTextFont}
	 * @param type gibt an ob es sich um einen Paragraph- oder einen CharacterStyle handelt
	 */
	public VirtualTextStyle(String styleName, VirtualTextFont font, VirtualTextStyleType type)
	{
		setStyleName( styleName );
		m_sStyleType = type;
		setFont( font );
	}
	
	// ------------------------------------------
	
	
	public void setStyleName(String name)
	{
		m_sStyleName = name;
	}
	
	public String getStyleName()
	{
		return m_sStyleName;
	}
	
	public VirtualTextStyleType getStyleType()
	{
		return m_sStyleType;
	}
	
	public VirtualTextFont getFont()
	{
		return m_oVirtualTextFont;  
	}
	
	public void setFont(VirtualTextFont font)
	{
		m_oVirtualTextFont = font;  
	}
	
	
	public String toString()
	{
		if (m_oVirtualTextFont != null) return "<style name=\"" + m_sStyleName + "\" type=\"" + m_sStyleType + "\" font=\"" + m_oVirtualTextFont + "\"/ >";      
		else                            return "<style name=\"" + m_sStyleName + "\" type=\"" + m_sStyleType + "\"/ >";
	}
	
	/**
	 * This method implements the <code>Cloneable</code>-Interface
	 * 
	 */
	
	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch(Exception pExcp)
		{
			throw new InternalError();
		}
	}
}

