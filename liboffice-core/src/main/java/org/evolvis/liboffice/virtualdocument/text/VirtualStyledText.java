/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 21.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualStyledText implements VirtualTextComponent
{
	private List m_oParagraphs = new ArrayList();
	
	// ---------------------------------------
	
	public VirtualStyledText()
	{    
	}
	
	public VirtualStyledText(String text)
	{
		this.addParagraph(new VirtualParagraph(text));
	}
	
	public VirtualStyledText(String text, String stylename)
	{
		VirtualParagraph para = new VirtualParagraph(text);
		para.setStyle(new VirtualTextStyle(stylename));    
		this.addParagraph(para);
	}
	
	
	public VirtualStyledText(VirtualStyledText text, String stylename)
	{
		for (int i=0; i < text.getNumberOfParagraphs(); i++)
		{
			VirtualParagraph para = text.getParagraph( i );
			para.setStyle( new VirtualTextStyle(stylename) );
			addParagraph(para);
		}
	}
	
	public VirtualStyledText(String text, VirtualTextStyle style)
	{
		VirtualParagraph para = new VirtualParagraph(text);
		para.setStyle(style);    
		this.addParagraph(para);
	}
	
	public VirtualStyledText(boolean addParagraphBreak)
	{
		if (addParagraphBreak) this.addParagraph(new VirtualParagraph(new VirtualParagraphBreak()));
	}
	
	public VirtualStyledText(String text, boolean addParagraphBreak)
	{
		this.addParagraph(new VirtualParagraph(text));
		if (addParagraphBreak) this.addParagraph(new VirtualParagraph(new VirtualParagraphBreak()));
	}
	
	public VirtualStyledText(String text, String stylename, boolean addParagraphBreak)
	{
		VirtualParagraph para = new VirtualParagraph(text);
		para.setStyle(new VirtualTextStyle(stylename));    
		this.addParagraph(para);
		if (addParagraphBreak) this.addParagraph(new VirtualParagraph(new VirtualParagraphBreak()));
	}
	
	public VirtualStyledText(String text, VirtualTextStyle style, boolean addParagraphBreak)
	{
		VirtualParagraph para = new VirtualParagraph(text);
		para.setStyle(style);    
		this.addParagraph(para);
		if (addParagraphBreak) this.addParagraph(new VirtualParagraph(new VirtualParagraphBreak()));
	}
	
	
	public VirtualStyledText(VirtualTextPortionMarker marker)
	{
		VirtualParagraph para = new VirtualParagraph(marker);
		this.addParagraph(para);
	}
	
	
	
	// ----------------------------------------------------------
	
	public VirtualStyledText(VirtualTextPortionText portion)
	{     
		this.addParagraph(new VirtualParagraph(portion));
	}
	
	public VirtualStyledText(VirtualParagraph para)
	{
		this.addParagraph(para);
	}
	
	// ---------------------------------------
	
	public void addParagraph(VirtualParagraph paragraph)
	{
		m_oParagraphs.add(paragraph);
	}
	
	public void removeParagraph(VirtualParagraph paragraph)
	{
		m_oParagraphs.remove(paragraph);    
	}
	
	
	public int getNumberOfParagraphs()
	{
		return m_oParagraphs.size();    
	}
	
	public VirtualParagraph getParagraph(int index)
	{
		return (VirtualParagraph)(m_oParagraphs.get(index));
	}
	
	public String getText()
	{
		String text = "";
		
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualParagraph para = (VirtualParagraph)(it.next());
			text += para.getText() + " ";
		}
		
		return text.trim();
	}
	
	/** liefert einen Iterator der Paragraphen
	 * @return Iterator 
	 */
	public Iterator iterator()
	{
		return m_oParagraphs.iterator();
	}
	
	public String toString()
	{
		String text = "<VirtualStyledText>";
		
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualParagraph para = (VirtualParagraph)(it.next());
			text += para.toString();
		}
		
		text += "</VirtualStyledText>";
		return text;
	}
	
	
	/**
	 * entfernt ALLE ParagraphBreaks aus dem gesamten Text
	 */
	public void removeAllParagraphBreaks()
	{
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualParagraph para = (VirtualParagraph)(it.next());
			para.removeParagraphBreaks();
		}
	}
	
	
	/**
	 * entfernt den letzten in diesem Text vorkommenden ParagraphBreak 
	 * @return true wenn ein ParagraphBreak entfernt wurde
	 */
	public boolean removeLastParagraphBreakOfText()
	{
		for(int i=getNumberOfParagraphs()-1; i>=0; i--)
		{
			VirtualParagraph para = getParagraph(i);
			if (para.removeLastParagraphBreak()) return true;
		}
		return false;
	}
	
	
	/**
	 * entfernt den letzten ParagraphBreak aus jedem im 
	 * Text enthaltenem Paragraph
	 */
	public void removeAllLastParagraphBreaks()
	{
		Iterator it = this.iterator();
		while(it.hasNext())
		{
			VirtualParagraph para = (VirtualParagraph)(it.next());
			para.removeLastParagraphBreak();
		}
	}
	
	
	/**
	 * entfernt den letzten ParagraphBreak wenn danach kein Text mehr kommt
	 */
	public boolean cleanParagraphBreaks()
	{
		if (getNumberOfParagraphs() > 0)
		{
			VirtualParagraph para = getParagraph(getNumberOfParagraphs() - 1);
			return para.cleanParagraphBreaks();      
		}
		return false;
	}
	
	
	
	public boolean containsStyle(String styleName)
	{
		return (null != findComponentByStyle( styleName ));
	}
	
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		for(int i=0; i<(getNumberOfParagraphs()); i++)
		{
			VirtualParagraph para = getParagraph(i);
			if (para.containsStyle( styleName ))
			{
				return para;                        
			}
		}
		return null;
	}
	
	/**
	 * This method implements the <code>Cloneable</code>-Interface
	 * 
	 */
	
	public Object clone()
	{
		try
		{
			VirtualStyledText copy = new VirtualStyledText();
			
			for(int i=0; i < this.getNumberOfParagraphs(); i++)
			{
				copy.addParagraph((VirtualParagraph)this.getParagraph(i).clone());
			}
			
			return copy;
		}
		catch(Exception pExcp)
		{
			throw new InternalError();
		}
	}
}
