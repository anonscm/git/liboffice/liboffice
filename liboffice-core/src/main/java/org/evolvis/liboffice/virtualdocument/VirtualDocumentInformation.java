/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 */
public class VirtualDocumentInformation
{
  private String m_sKey;
  private String m_sValue;
  private VirtualDocumentInformationStorage m_oStorageLocation;
  
  public VirtualDocumentInformation(String key, String value)
  {
    m_sKey = key;
    m_sValue = value;
    m_oStorageLocation = VirtualDocumentInformationStorage.STORAGE_ANY;    
  }

  public VirtualDocumentInformation(String key, String value, VirtualDocumentInformationStorage storagelocation)
  {
    m_sKey = key;
    m_sValue = value;
    m_oStorageLocation = storagelocation;    
  }
  
  public Object getStorageLocation()
  {
    return m_oStorageLocation;
  }
  
  public void setStorageLocation(VirtualDocumentInformationStorage storageLocation)
  {
    m_oStorageLocation = storageLocation;
  }
  
  public String getKey()
  {
    return m_sKey;
  }
  
  public void setKey(String key)
  {
    m_sKey = key;
  }
  
  public String getValue()
  {
    return m_sValue;
  }

  public void setValue(String value)
  {
    m_sValue = value;
  }
}
