/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument;

import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 */
public class VirtualTextFrame implements VirtualTextComponent 
{
	private float positionTop, positionLeft, width, height;
	private VirtualStyledText text = null;
	
	public VirtualTextFrame()
	{
	}
  
  public VirtualTextFrame(float positionTop, float positionLeft, float width, float height) 
  {
    super();
    this.positionTop    = positionTop;
    this.positionLeft   = positionLeft;
    this.width          = width;
    this.height         = height;
  }
	
	/**
	 * @param positionTop
	 * @param positionLeft
	 * @param width
	 * @param height
	 * @param text
	 */
	public VirtualTextFrame(float positionTop, float positionLeft, float width, float height, VirtualStyledText text) 
	{
		super();
		this.positionTop = positionTop;
		this.positionLeft = positionLeft;
		this.width = width;
		this.height = height;
		this.text = text;
	}
		
	public VirtualTextFrame(VirtualStyledText text)
	{
		this.text = text;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#containsStyle(java.lang.String)
	 */
	public boolean containsStyle(String styleName) 
	{
		return (null != findComponentByStyle( styleName ));
	}
	
//	 
	
	/**
	 * @return Returns the height.
	 */
	public float getHeight() 
	{
		return height;
	}
	
	/**
	 * @param height The height to set in Nano-Meters.
	 */
	public void setHeight(float height) 
	{
		this.height = height;
	}
	
	/**
	 * @return Returns the positionLeft in Nano-Meters.
	 */
	public float getPositionLeft() 
	{
		return positionLeft;
	}
	
	/**
	 * @param positionLeft The positionLeft to set in Nano-Meters.
	 */
	public void setPositionLeft(float positionLeft) 
	{
		this.positionLeft = positionLeft;
	}
	
	/**
	 * @return Returns the positionTop in Nano-Meters.
	 */
	public float getPositionTop() 
	{
		return positionTop;
	}
	
	/**
	 * @param positionTop The positionTop to set in Nano-Meters.
	 */
	public void setPositionTop(float positionTop) 
	{
		this.positionTop =  positionTop ;
	}
	
	/**
	 * @return Returns the text.
	 */
	public VirtualStyledText getText() 
	{
		return text;
	}
	
	/**
	 * @param text The text to set.
	 */
	public void setText(VirtualStyledText text) 
	{
		this.text = text;
	}
	
	/**
	 * @return Returns the width in Nano-Meters.
	 */
	public float getWidth() 
	{
		return width;
	}
	
	/**
	 * @param width The width to set in Nano-Meters.
	 */
	public void setWidth(float width) 
	{
		this.width =  width ;
	}
	
	public String toString()
	{
		StringBuffer b = new StringBuffer();
		b.append("<VirtualTextFrame ");
		b.append(" width=\"");
		b.append(getWidth());
		
		b.append("\" height=\"");
		b.append(getWidth());
		
		b.append("\" top=\"");
		b.append(getPositionTop());
		
		b.append("\" left=\"");
		b.append(getPositionLeft());
		b.append("\">");
		b.append(getText().toString());
		b.append("</VirtualTextFrame>");
		return b.toString();
	}


	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#findComponentByStyle(java.lang.String)
	 */
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		if (null != text && text.containsStyle(styleName))
		{
			return text;
		}
		return null;
	}
}
