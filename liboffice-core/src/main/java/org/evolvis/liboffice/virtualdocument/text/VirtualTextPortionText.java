/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 22.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.text;

import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualTextPortionText implements VirtualTextPortion
{
	private String m_sText = "";
	private VirtualTextStyle m_oVirtualTextStyle = null;
	private VirtualTextFont m_oVirtualTextFont = null;
	private boolean m_bIsBold = false;
	private boolean m_bIsItalic = false;
	private boolean m_bIsUnderline = false;
	
	private boolean m_bUseBold = false;
	private boolean m_bUseItalic = false;
	private boolean m_bUseUnderline = false;
	
	// ---------------------------------------
	
	public VirtualTextPortionText()
	{    
	}
	
	public VirtualTextPortionText(String text)
	{    
		setText(text);
	}
	
	public VirtualTextPortionText(String text, VirtualTextStyle style)
	{    
		setText(text);
		setStyle(style);
	}    
	
	public VirtualTextPortionText(String text, String stylename)
	{
		this(text, new VirtualTextStyle(stylename));
	}    
	
	public VirtualTextPortionText(String text, VirtualTextStyle style, boolean bold, boolean italic, boolean underline)
	{    
		setText(text);
		setStyle(style);
		setBold(bold);
		setItalic(italic);
		setUnderline(underline);
		m_bUseBold = true;
		m_bUseItalic = true;
		m_bUseUnderline = true;
	}    
	
	public VirtualTextPortionText(String text, String stylename, boolean bold, boolean italic, boolean underline)
	{
		this(text, new VirtualTextStyle(stylename), bold, italic, underline);
	}    
	
	public VirtualTextPortionText(String text, boolean bold, boolean italic, boolean underline)
	{
		this(text, (VirtualTextStyle)null, bold, italic, underline);
	}    
	
	// ---------------------------------------
	
	
	public void setText(String text)
	{
		m_sText = text;
	}
	
	public String getText()
	{
		if (m_sText == null) return "";
		return m_sText;
	}
	
	public void setStyle(VirtualTextStyle style)
	{
		m_oVirtualTextStyle = style;
	}
	
	public VirtualTextStyle getStyle()
	{
		return m_oVirtualTextStyle;
	}
	
	public void setFont(VirtualTextFont font)
	{
		m_oVirtualTextFont = font;
	}
	
	public VirtualTextFont getFont()
	{
		return m_oVirtualTextFont;
	}
	
	public void setBold(boolean isbold)
	{
		m_bIsBold = isbold;
		m_bUseBold = true;
	}
	
	public boolean isBold()
	{
		return m_bIsBold;
	}
	
	public void setItalic(boolean isitalic)
	{
		m_bIsItalic = isitalic;
		m_bUseItalic = true;
	}
	
	public boolean isItalic()
	{
		return m_bIsItalic;
	}
	
	public void setUnderline(boolean isunderline)
	{
		m_bIsUnderline = isunderline;
		m_bUseUnderline = true;
	}
	
	public boolean isUnderline()
	{
		return m_bIsUnderline;
	}
	
	public void setUseBold(boolean usebold)
	{
		m_bUseBold = usebold;
	}
	
	public void setUseItalic(boolean useitalic)
	{
		m_bUseItalic = useitalic;
	}
	
	public void setUseUnderline(boolean useunderline)
	{
		m_bUseUnderline = useunderline;
	}
	
	
	public boolean useBold()
	{
		return m_bUseBold;
	}
	
	public boolean useItalic()
	{
		return m_bUseItalic;
	}
	
	public boolean useUnderline()
	{
		return m_bUseUnderline;
	}
	
	
	
	public String toString()
	{
		String text = "<VirtualTextPortionText bold=\"" + m_bIsBold + "\" italic=\"" + m_bIsItalic + "\" underline=\"" + m_bIsUnderline + "\">";
		
		if (m_oVirtualTextStyle != null)
		{
			text += m_oVirtualTextStyle.toString();
		}    
		
		if (m_oVirtualTextFont != null)
		{
			text += m_oVirtualTextFont.toString();
		}    
		
		
		text += "<text>" + m_sText + "</text>";
		
		text += "</VirtualTextPortionText>";
		
		return text;
	}
	
	
	public boolean containsStyle(String styleName)
	{
		if (m_oVirtualTextStyle != null)
		{
			if (styleName.equals(m_oVirtualTextStyle.getStyleName())) return true;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#findComponentByStyle(java.lang.String)
	 */
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		
		if (getStyle().getStyleName() != null && getStyle().getStyleName().equalsIgnoreCase( styleName ))
		{
			return this;
		}
		else return null;
	}
	
	/**
	 * This method implements the <code>Cloneable</code>-Interface
	 * 
	 */
	
	public Object clone()
	{
		try
		{
			VirtualTextPortionText copy = new VirtualTextPortionText();
			
			copy.setBold(this.isBold());
			if(this.getFont() != null)
				copy.setFont((VirtualTextFont)this.getFont().clone());
			copy.setItalic(this.isItalic());
			if(this.getStyle() != null)
				copy.setStyle((VirtualTextStyle)this.getStyle().clone());
			copy.setText(this.getText());
			copy.setUnderline(this.isUnderline());
			copy.setUseBold(this.useBold());
			copy.setUseItalic(this.useItalic());
			copy.setUseUnderline(this.useUnderline());
			
			return copy;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			throw new InternalError();
		}
	}
}
