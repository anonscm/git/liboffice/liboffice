/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 21.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.table;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.evolvis.liboffice.virtualdocument.VirtualBorder;
import org.evolvis.liboffice.virtualdocument.VirtualMargin;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn, Steffen Kriese, tarent GmbH Bonn
 *
 */
public class VirtualTextTable implements VirtualTextComponent
{
	public final static Object COLUMNWIDTH_NONE      = "WIDTH_NONE"; 
	public final static Object COLUMNWIDTH_AUTOMATIC = "WIDTH_AUTOMATIC"; 
	
	public final static int alighment_left = 0;
	public final static int alighment_center = 1;
	public final static int alighment_right = 2;
	private int alighment = 0;

	private int m_iRows;
	private int m_iColumns;
	private Map m_oCells 		                       = new LinkedHashMap();
	private Map m_oColumnWidths                = new LinkedHashMap();
	private boolean m_bShowLines               = false;
	
	private VirtualMargin marginLeft 				= null;
	
	private float fWidth = -1;
	
	public VirtualTextTable()
	{
		super();
	}
	
	public VirtualTextTable(int pRows, int pColumns)
	{
		super();
		m_iRows = pRows;
		m_iColumns = pColumns;
	}
	
	public void setSize(int rows, int columns)
	{
		m_iRows = rows;
		m_iColumns = columns;
	}
	
	public int getNumberOfRows()
	{
		return m_iRows;
	}
	
	public int getNumberOfColumns()
	{
		return m_iColumns;
	}
	
	
	public void setShowLines(boolean show)
	{
		m_bShowLines = show;
	}
	
	public boolean getShowLines()
	{
		return m_bShowLines;
	}
	
	public void setCellBorderLinesVisible(boolean show)
	{
		setCellBorderLinesVisible(show, show, show, show);
	}
	
	public void setCellBorderLinesVisible(boolean showTop, boolean showLeft, boolean showRight, boolean showBottom)
	{
		Iterator it = m_oCells.values().iterator();
		while(it.hasNext())
		{
			VirtualTextTableCell cell = (VirtualTextTableCell)(it.next());
			
			if (cell.getBorderTop() == null)    cell.setBorderTop(new VirtualBorder(showTop));
			else cell.getBorderTop().setVisible(showTop);
			
			if (cell.getBorderLeft() == null)   cell.setBorderLeft(new VirtualBorder(showLeft));
			else cell.getBorderLeft().setVisible(showLeft);
			
			if (cell.getBorderRight() == null)  cell.setBorderRight(new VirtualBorder(showRight));
			else cell.getBorderRight().setVisible(showRight);
			
			if (cell.getBorderBottom() == null) cell.setBorderBottom(new VirtualBorder(showBottom));
			else cell.getBorderBottom().setVisible(showBottom);
		}    
	}
	
	
	/**
	 * @param row    - die Zeile
	 * @param column - die Spalte
	 * @param cell   - VirtualTextTableCell die gesetzt werden soll 
	 */
	public void setTextTableCell(int row, int column, VirtualTextTableCell cell)
	{
		m_oCells.put(row + "," + column, cell);
	}
	
	public VirtualTextTableCell getTextTableCell(int row, int column)
	{
		return (VirtualTextTableCell)(m_oCells.get(row + "," + column));    
	}
	
	
	public void setColumnWidth(int column, Object width)
	{
		m_oColumnWidths.put(new Integer(column), width);
	}
	
	public Object getColumnWidth(int column)
	{
		Object val = m_oColumnWidths.get(new Integer(column));
		if (val == null) return COLUMNWIDTH_NONE;
		return val;
	}
	
	
	
	
	public String toString()
	{
		String text = "<VirtualTextTable rows=\""+ m_iRows +"\" cols=\""+   m_iColumns +"\">";
		text += "<VirtualMargin>"+getMarginLeft()+"</VirtualMargin>";
		Iterator it = m_oColumnWidths.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry entry = (Map.Entry)(it.next());
			text += "<ColumnWidth column=\"" + entry.getKey() + "\">" + entry.getValue() + "</ColumnWidth>";
		}
		
		for(int row=0; row<(getNumberOfRows()); row++)
		{
			for(int col=0; col<(getNumberOfColumns()); col++)
			{
				VirtualTextTableCell cell = getTextTableCell(row, col);
				if (cell != null)
				{
					text += cell.toString();
				}
				else
				{
					text += "<VirtualTextTableCell/>";          
				}
			}
		}
		text += "</VirtualTextTable>";
		return text;
	}
	
	public boolean containsStyle(String styleName)
	{
		return (findComponentByStyle( styleName ) != null);
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#findComponentByStyle(java.lang.String)
	 */
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		for(int i=0; i < getNumberOfRows(); i++)
		{
			for(int j=0; j < getNumberOfColumns(); j++)
			{
				VirtualTextTableCell cell = getTextTableCell(i, j);
				//logger.debug("find comp by style " + cell);
				if(null != cell && cell.containsStyle( styleName ))
				{
					return cell;
				}
			}
		}
		return null;
	}
	
	public VirtualMargin getMarginLeft() 
	{
		return marginLeft;
	}
	
	public void setMarginLeft(VirtualMargin marginLeft) 
	{
		this.marginLeft = marginLeft;
	}
	
	/**
	 * @return Returns the width.
	 */
	public float getWidth()
	{
		return fWidth;
	}
	
	/**
	 * @param width The width to set.
	 */
	public void setWidth(float width)
	{
		this.fWidth = width;
	}
	
	public void setAlighment(int alighment){
		this.alighment = alighment;
	}
	
	public int getAlighment(){
		return alighment;
	}
	
}
