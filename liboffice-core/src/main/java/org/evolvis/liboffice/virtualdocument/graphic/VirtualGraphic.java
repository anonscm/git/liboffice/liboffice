/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 21.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument.graphic;

import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualGraphic implements VirtualTextComponent
{
  private float     m_iWidth;
  private float     m_iHeight;
  private String  m_sFilename;
  private boolean m_bIsPrintable;
  
  public void setPrintable(boolean printable)
  {
    m_bIsPrintable = printable;
  }
  
  public boolean isPrintable()
  {
    return m_bIsPrintable;
  }
  
  
  public void setFilename(String filename)
  {
    m_sFilename = filename;
  }
  
  public String getFilename()
  {
    return m_sFilename;
  }
  
  public void setSize(int width, int height)
  {
    m_iWidth = width;
    m_iHeight = height;
  }
  
  public float getWidth()
  {
    return m_iWidth;
  }

  public float getHeight()
  {
    return m_iHeight;
  }
  
  
  
  public String toString()
  {
    String text = "<VirtualGraphic>";
    text += "<width>" + m_iWidth + "</width>";
    text += "<height>" + m_iHeight + "</height>";
    text += "<filename>" + m_sFilename + "</filename>";
    text += "<printable>" + m_bIsPrintable + "</printable>";
    text += "</VirtualGraphic>";
    return text;
  }


  public boolean containsStyle(String styleName)
  {
    return false;
  }

	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#findComponentByStyle(java.lang.String)
	 */
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		return null;
	}
  
  
}
