/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 21.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.virtualdocument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class VirtualTextDocument implements VirtualTextComponent
{
  private List m_oTextComponents = new ArrayList();
  private VirtualTextComponent m_oComponentContainingCursor = null;
  
  public VirtualTextDocument()
  {    
  }
  
  public VirtualTextDocument(VirtualStyledText text)
  {
    super();
    for(int i=0; i<(text.getNumberOfParagraphs()); i++)
    {
      VirtualParagraph para = text.getParagraph(i);
      this.addTextComponent(para);
    }
  }
  
  
  public void setComponentContainingCursor(VirtualTextComponent comp)
  {
    m_oComponentContainingCursor = comp;
  }
  
  public VirtualTextComponent getComponentContainingCursor()
  {
    return m_oComponentContainingCursor;
  }
  
  
  /**
   * f�gt eine Komponente ans Dokument an...
   * @param component - die VirtualTextComponent'e die angef�gt werden soll
   */
  public void addTextComponent(VirtualTextComponent component)
  {
    m_oTextComponents.add(component);
  }
  
  /**
   * eintfernt eine Komponente aus dem Dokument...
   * @param component - die VirtualTextComponent'e die entfernt werden soll
   */
  public void removeTextComponent(VirtualTextComponent component)
  {
    m_oTextComponents.remove(component);    
  }
  
  public int getNumberOfTextComponents()
  {
    return m_oTextComponents.size();
  }
  
  public VirtualTextComponent getTextComponent(int index)
  {
    return (VirtualTextComponent) m_oTextComponents.get(index);
  }
  
  /**
   * liefert eine Iteration von VirtualTextComponent's
   * @return iterator
   */
  public Iterator iterator()
  {
    return m_oTextComponents.iterator();
  }
  
  public String toString()
  {
    String text = "<VirtualTextDocument>";
    Iterator it = this.iterator();
    while(it.hasNext())
    {
      VirtualTextComponent component = (VirtualTextComponent)(it.next());
      text += component.toString();
    }
    
    text += "</VirtualTextDocument>";
    return text;
  }


  public boolean containsStyle(String styleName)
  {
    return (findComponentByStyle( styleName ) != null);
  }

	/* (non-Javadoc)
	 * @see de.tarent.documents.virtualdocument.Styleable#findComponentByStyle()
	 */
	public VirtualTextComponent findComponentByStyle(String styleName) 
	{
		Iterator it = this.iterator();
	    while(it.hasNext())
	    {
	      VirtualTextComponent component = (VirtualTextComponent)(it.next());
	      if (null != component && component.containsStyle(styleName)) return component;
	    }
	    return null;
	}
}
