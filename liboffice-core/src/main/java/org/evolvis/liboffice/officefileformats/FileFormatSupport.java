/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.officefileformats;

/**
 * 
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class FileFormatSupport
{
	private OfficeFileFormat fileFormat;
	private boolean canRead, canWrite;
	
	public FileFormatSupport(OfficeFileFormat pFileFormat, boolean pCanRead, boolean pCanWrite)
	{
		fileFormat = pFileFormat;
		canRead = pCanRead;
		canWrite = pCanWrite;
	}
	
	public FileFormatSupport()
	{
		fileFormat = null;
		canRead = false;
		canWrite = false;
	}
	
	/* Getters */
	
	public OfficeFileFormat getOfficeFileFormat()
	{
		return fileFormat;
	}
	
	public boolean canRead()
	{
		return canRead;
	}
	
	public boolean canWrite()
	{
		return canWrite;
	}
	
	/* Setters */
	
	public void setOfficeFileFormat(OfficeFileFormat pOfficeFileFormat)
	{
		fileFormat = pOfficeFileFormat;
	}
	
	public void setCanRead(boolean pCanRead)
	{
		canRead = pCanRead;
	}
	
	public void setCanWrite(boolean pCanWrite)
	{
		canWrite = pCanWrite;
	}
}
