/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 26.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.officefileformats;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.filechooser.FileFilter;

//import org.apache.log4j.Logger;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OfficeFileFormatAdapter implements OfficeFileFormat
{
	private String    m_oKey;
	private String    m_sShortName;
	private String    m_sLongName;
	private String    m_sDescription;

	private ImageIcon m_oIcon = null;
	private List      m_oSuffixes;
	//private Logger logger = Logger.getLogger(getClass());

	// -----------------------------------

	/**
	 * Erzeugt ein OfficeFileFormat
	 * @param key         - der Schl�ssel �ber den auf das FileFormat zugegriffen werden kann
	 * @param shortname   - eine Kurzbezeichnung des Dateiformats
	 * @param longname    - die vollst&auml;ndige Bezeichnung des Dateiformats
	 * @param description - eine genaue Erkl&auml;rung des Dateiformats
	 * @param icon        - ein Icon f�r das Dateiformat
	 * @param suffixes    - ein String oder ein String[] mit de(r/n) Dateiendung(en) 
	 */
	public OfficeFileFormatAdapter(String key, String shortname, String longname, String description, Object icon, Object suffixes)
	{
		this(key, shortname, longname, description, icon, suffixes, true, true);
	}


	/**
	 * Erzeugt ein OfficeFileFormat
	 * @param key         - der Schl�ssel �ber den auf das FileFormat zugegriffen werden kann
	 * @param shortname   - eine Kurzbezeichnung des Dateiformats
	 * @param longname    - die vollst&auml;ndige Bezeichnung des Dateiformats
	 * @param description - eine genaue Erkl&auml;rung des Dateiformats
	 * @param icon        - ein Icon f�r das Dateiformat
	 * @param suffixes    - ein String oder ein String[] mit de(r/n) Dateiendung(en) 
	 * @param canLoad     - gibt an ob die API dieses Format lesen kann 
	 * @param canSave     - gibt an ob die API dieses Format schreiben kann
	 */
	public OfficeFileFormatAdapter(String key, String shortname, String longname, String description, Object icon, Object suffixes, boolean canLoad, boolean canSave)
	{
		m_oKey = key;
		m_sShortName = shortname;
		m_sLongName = longname;
		m_sDescription = description;

//		if (icon instanceof ImageIcon)
//		{
//		m_oIcon = (ImageIcon)icon;      
//		}
//		else if (icon instanceof String)
//		{
//		m_oIcon = loadIcon((String)icon);      
//		}
//		else m_oIcon = null;

		m_oSuffixes = new ArrayList();
		if (suffixes != null)
		{
			if (suffixes instanceof String[])
			{
				for(int i=0; i<(((String[])suffixes).length); i++)
				{
					addSuffix(((String[])suffixes)[i]);
				}
			}
			else if (suffixes instanceof String)
			{
				addSuffix((String)suffixes);
			}
		}
	}

	/*
  private ImageIcon loadIcon(String filename)
  {
    ImageIcon icon = null;
    URL imagebase = this.getClass().getResource("/de/tarent/documents/resources/");
    try
    {      
      icon   = new ImageIcon(new URL(imagebase, "next.gif"));
    } catch (MalformedURLException e) 
    {
        logger.error("Error", e );
    }
    return icon;
  }  
	 */


	// -----------------------------------

	public String getKey()
	{
		return m_oKey;
	}

	public void setKey(String key)
	{
		m_oKey = key;
	}

	public String getDescription()
	{
		return m_sDescription;
	}

	public void setDescription(String description)
	{
		m_sDescription = description;
	}

	public ImageIcon getIcon()
	{
		return m_oIcon;
	}

	public void setIcon(ImageIcon icon)
	{
		m_oIcon = icon;
	}

	public String getLongName()
	{
		return m_sLongName;
	}

	public void setLongName(String longName)
	{
		m_sLongName = longName;
	}

	public String getShortName()
	{
		return m_sShortName;
	}

	public void setShortName(String shortName)
	{
		m_sShortName = shortName;
	}

	public String getSuffix(int index)
	{
		return (String)(m_oSuffixes.get(index));
	}

	public void addSuffix(String suffix)
	{
		m_oSuffixes.add(suffix);
	}

	public int getNumberOfSuffixes()
	{
		return m_oSuffixes.size();
	}  


	public boolean endsWithSuffix(String filename)
	{
		Iterator it = m_oSuffixes.iterator();
		while(it.hasNext())
		{
			String suffix = (String)(it.next());
			if (filename.toLowerCase().endsWith(suffix)) return true;
		}
		return false;
	}

	public String getCommaSeperatedSuffixes()
	{
		StringBuffer buf = new StringBuffer();

		for(int i=0; i < getNumberOfSuffixes(); i++)
		{
			if(i == 0) buf.append(getSuffix(i));
			else buf.append(", "+getSuffix(i));
		}

		return buf.toString();
	}



	private class OfficeFileFilter extends FileFilter
	{
		public boolean accept(File f)
		{
			for(int i=0; i<(getNumberOfSuffixes()); i++)
			{
				String suffix = getSuffix(i).toUpperCase();        
				if (f.getName().toUpperCase().endsWith(suffix)) return true;
			}

			return false;
		}

		public String getDescription()
		{
			return OfficeFileFormatAdapter.this.getDescription();
		}    
	}

	public FileFilter getFileFilter()
	{
		return new OfficeFileFilter();
	}


	@Override
	public boolean equals(Object obj) {
		
		return getClass().equals(obj.getClass());
	}
}
