/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 */
package org.evolvis.liboffice.descriptors;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Beschreibt einen Text anhand seiner Auszeichnungen
 */
public class FormattedTextDescriptor extends AbstractSectionDescriptor
{
  
  private Boolean m_oBold      = null;
  private Boolean m_oItalic    = null;
  private Boolean m_oUnderline = null;
  
  /**
   * Beschreibt einen Text anhand seines Textes und seiner Auszeichnungen
   * @param text - der zu suchende Text
   */
  public FormattedTextDescriptor(String text, Boolean bold, Boolean italic, Boolean underline)
  {
    super( text );
    m_oBold = bold;
    m_oItalic = italic;
    m_oUnderline = underline;
  }
  
  /**
   * Beschreibt einen Text anhand seines Textes und seiner Auszeichnungen
   * @param text - der zu suchende Text
   */
  public FormattedTextDescriptor(String text, Boolean bold, Boolean italic, Boolean underline, int sections)
  {
    super( text );
    m_oBold = bold;
    m_oItalic = italic;
    m_oUnderline = underline;
    this.sections = sections;
  }

  public FormattedTextDescriptor(String text, boolean bold, boolean italic, boolean underline)
  {
    super( text );
    m_oBold = new Boolean(bold);
    m_oItalic = new Boolean(italic);
    m_oUnderline = new Boolean(underline);
  }
  
  public FormattedTextDescriptor(String text, boolean bold, boolean useBold, boolean italic, boolean useItalic, boolean underline, boolean useUnderline)
  {
    super( text );
    if (useBold)      m_oBold      = new Boolean(bold);      else m_oBold      = null; 
    if (useItalic)    m_oItalic    = new Boolean(italic);    else m_oItalic    = null;
    if (useUnderline) m_oUnderline = new Boolean(underline); else m_oUnderline = null;
  }
  
  public boolean isBold()
  {
    return m_oBold.booleanValue();   
  }
  
  public boolean isItalic()
  {
    return m_oItalic.booleanValue();   
  }
  
  public boolean isUnderline()
  {
    return m_oUnderline.booleanValue();   
  }

  
  public boolean useText()
  {
    return getText() != null;
  }  
  
  public boolean useBold()
  {
    return m_oBold != null;   
  }
  
  public boolean useItalic()
  {
    return m_oItalic != null;   
  }
  
  public boolean useUnderline()
  {
    return m_oUnderline != null;   
  }  
}
