/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 */
package org.evolvis.liboffice.descriptors;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * Interface f&uml;r alle eine Graphik beschreibende Descriptoren
 */
public class GraphicDescriptor implements TextContentDescriptor
{
    private String name        = null;
    private String fileName    = null;
    private Boolean printable = null;
    private Float width          = null;
    private Float height         = null;
    
    public GraphicDescriptor()
    {
        
    }
    
    /**
     * @param name
     */
    public GraphicDescriptor(String name)
    {
        super();
        this.name = name;
    }


    /**
     * 
     * @param name Der Name der Grafik oder null wenn der Wert ignoriert werden soll
     * @param printable true oder false wenn nach dem Wert gesucht werden soll, null wenn dieser ignoriert werden soll
     * @param width die Breite der Grafik -1 wenn der Wert ignoriert werden soll
     * @param height die Hoehe der Grafik -1 wenn der Wert ignoriert werden soll
     */
    public GraphicDescriptor(String name, Boolean printable, Float width, Float height, String fileName)
    {
        this.name = name;
        this.printable = printable;
        this.width = width;
        this.height = height;
        this.fileName = fileName;
    }

    public String getFileName()
    {
        return this.fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public Float getHeight()
    {
        return this.height;
    }

    public void setHeight(Float height)
    {
        this.height = height;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Boolean getPrintable()
    {
        return this.printable;
    }

    public void setPrintable(Boolean printable)
    {
        this.printable = printable;
    }

    public Float getWidth()
    {
        return this.width;
    }

    public void setWidth(Float width)
    {
        this.width = width;
    }
}
