/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 */
package org.evolvis.liboffice.descriptors;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Ein Descriptor um Text in Tabellenzellen zu beschreiben
 */
public class CellTextTableDescriptor extends AbstractTableDescriptor
{
  private String m_sText;
  
  /**
   * Ein Descriptor um Text in Tabellenzellen zu beschreiben
   * @param text   - der zu suchende Text
   * @param row    - die Zeile in der gesucht werden soll 
   *                 (wenn -1 wird in allen Zeilen gesucht)
   * @param column - die Spalte in der gesucht werden soll
   *                 (wenn -1 wird in allen Spalten gesucht)
   */
  public CellTextTableDescriptor(String text, int row, int column)
  {
      super(row,column);
      m_sText = text;
  }

  /**
   * Ein Descriptor um Text in Tabellenzellen zu beschreiben
   * @param text   - der zu suchende Text
   * es wird in allen Zellen gesucht.
   */
  public CellTextTableDescriptor(String text)
  {
    m_sText = text;
  }
  
  public String getText()
  {
    return m_sText;
  }
  
}
