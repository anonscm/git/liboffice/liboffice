package org.evolvis.liboffice.descriptors;

public class VariableDescriptor implements TextContentDescriptor {

	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
}
