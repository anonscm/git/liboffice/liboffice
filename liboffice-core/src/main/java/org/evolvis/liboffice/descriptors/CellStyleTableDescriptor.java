/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 09.06.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.descriptors;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 */
public class CellStyleTableDescriptor extends AbstractTableDescriptor 
{
	private String styleName;
	
	/** Ein Descriptor um Styles in Tabellenzellen zu beschreiben.
	 ** @param styleName der zu suchende Style
	 */
	public CellStyleTableDescriptor(String styleName)
	{
		this.styleName = styleName;
	}
    
    
	public CellStyleTableDescriptor(int row, int column, String styleName)
    {
        super(row, column);
        this.styleName = styleName;
    }


    /**
	 * @return Returns the styleName.
	 */
	public String getStyleName() 
	{
		return styleName;
	}
}
