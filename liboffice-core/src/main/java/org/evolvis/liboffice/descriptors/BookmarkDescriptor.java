/**
 * 
 */
package org.evolvis.liboffice.descriptors;

/**
 * 
 * This descriptor describes a MS-Office "Bookmark" (german: "Textmaker")
 * 
 * @author Andrei Boulgakov tarent GmbH Bonn
 *
 */
public class BookmarkDescriptor implements TextContentDescriptor {

	String name;
	String content;
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public BookmarkDescriptor(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
