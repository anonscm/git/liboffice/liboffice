/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 */
package org.evolvis.liboffice.descriptors;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Beschreibt eine Tabelle anhand eines darin enthaltenen Styles
 */
public class StyleTableDescriptor extends AbstractTableDescriptor
{
  private String m_sStyle;
  
  /**
   * Beschreibt eine Tabelle anhand eines darin enthaltenen Styles
   * @param stylename - der zu suchende Style oder null wenn dieser 
   *                    ignoriert werden soll
   * @param row       - die Zeile in der gesucht werden soll
   *                    (oder -1 wenn in allen Zeilen gesucht werden soll)
   * @param column    - die Spalte in der gesucht werden soll
   *                    (oder -1 wenn in allen Spalten gesucht werden soll)
   */
  public StyleTableDescriptor(String stylename, int row, int column)
  {
    super(row, column);
      m_sStyle = stylename;
  }
  
  /**
   * Beschreibt eine Tabelle anhand eines darin enthaltenen Styles
   * @param stylename - der zu suchende Style oder null wenn dieser 
   *                    ignoriert werden soll
   * es wird in allen Zellen gesucht.
   */
  public StyleTableDescriptor(String stylename)
  {
	  super();
    m_sStyle = stylename;
  }
  
  public String getStyleName()
  {
    return m_sStyle;
  }
}
