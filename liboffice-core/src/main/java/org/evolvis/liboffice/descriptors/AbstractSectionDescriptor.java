/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.descriptors;

public abstract class AbstractSectionDescriptor implements TextDescriptor
{
    public final static int SECTION_PARAS   = 100;
    public final static int SECTION_FRAMES = 200;
    public final static int SECTION_TABLES  = 400;
    
    protected int sections                            = 0;
    private String text                                 = null;
    
    public AbstractSectionDescriptor()
    {
        
    }
    
    public AbstractSectionDescriptor(String text)
    {
        super();
        this.text = text;
    }

    public boolean searchForParagraphs()
    {
        return (SECTION_PARAS ==  (SECTION_PARAS &  sections));
    }
    
    public boolean searchForFrames()
    {
        return (SECTION_FRAMES ==  (SECTION_FRAMES &  sections));
    }
      
    public boolean searchForTables()
    {
        return (SECTION_TABLES ==  (SECTION_TABLES &  sections));
    }

    public boolean searchForSection(int i)
    {
        return (i ==  (i &  sections));
    }
    
    public boolean searchForAllSections()
    {
        return (0 == sections);
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }
}
