/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 */
package org.evolvis.liboffice.descriptors;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Beschreibt einen Paragraphen anhand eines darin enthaltenen 
 * Textes oder Styles.
 * 
 */
public class StyledParagraphDescriptor extends AbstractSectionDescriptor
{
  private String m_sStyle;
  
  /**
   * Beschreibt einen Paragraphen anhand eines darin enthaltenen Styles
   * @param style - der zu suchende Style oder null wenn dieser 
   *                ignoriert werden soll
   */
  public StyledParagraphDescriptor(String style)
  {
      m_sStyle = style;
  }
  
  public StyledParagraphDescriptor(String style, int sections)
  {
      this(style);
      this.sections = sections;
  }
  
  
  /**
   * Beschreibt einen Paragraphen anhand eines darin enthaltenen 
   * Textes oder Styles
   * @param text  - der zu suchende Text oder null wenn dieser 
   *                ignoriert werden soll
   * @param style - der zu suchende Style oder null wenn dieser 
   *                ignoriert werden soll
   */
  public StyledParagraphDescriptor(String text, String style)
  {
    super( text );
    m_sStyle = style;     
  }
  
  public StyledParagraphDescriptor(String text, String style, int sections)
  {
      this(text, style);
      this.sections = sections;
  }

  public String getStyle()
  {
    return m_sStyle;
  }
}
