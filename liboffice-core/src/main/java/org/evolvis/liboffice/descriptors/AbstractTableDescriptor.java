/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 */
package org.evolvis.liboffice.descriptors;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * Interface f&uml;r alle eine Tabelle beschreibenden Descriptoren
 */
public abstract class AbstractTableDescriptor implements TextContentDescriptor
{
    private int row = -1;
    private int column = -1;
    
    public AbstractTableDescriptor()
    {
        
    }
    
    public AbstractTableDescriptor(int row, int column)
    {
        super();
        this.row = row;
        this.column = column;
    }
    
    public int getColumn()
    {
        return this.column;
    }
    
    public void setColumn(int column)
    {
        this.column = column;
    }
    
    public int getRow()
    {
        return this.row;
    }
    
    public void setRow(int row)
    {
        this.row = row;
    }
}
