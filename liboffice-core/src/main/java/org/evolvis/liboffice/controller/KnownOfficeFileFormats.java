/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 26.10.2004
 *
 */
package org.evolvis.liboffice.controller;

import java.lang.reflect.Field;

import org.evolvis.liboffice.officefileformats.AmbiguousExtensionException;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatDOC;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatDOCX;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatDOT;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatDOTX;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatHTML;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatODT;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatOTT;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatPDF;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatRTF;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatSTW;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatSXW;



/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 * Dieses Klasse enth&auml;lt feste Schl&uml;ssel f&uml;r unterst&uml;tzte
 * Dateiformate.
 */
public class KnownOfficeFileFormats
{ 
	public final static OfficeFileFormat FILE_TYPE_DOC = new OfficeFileFormatDOC();
	public final static OfficeFileFormat FILE_TYPE_DOCX = new OfficeFileFormatDOCX();
	public final static OfficeFileFormat FILE_TYPE_DOT = new OfficeFileFormatDOT();
	public final static OfficeFileFormat FILE_TYPE_DOTX = new OfficeFileFormatDOTX();
	public final static OfficeFileFormat FILE_TYPE_HTML = new OfficeFileFormatHTML();
	public final static OfficeFileFormat FILE_TYPE_ODT = new OfficeFileFormatODT();
	public final static OfficeFileFormat FILE_TYPE_OTT = new OfficeFileFormatOTT();
	public final static OfficeFileFormat FILE_TYPE_PDF = new OfficeFileFormatPDF();
	public final static OfficeFileFormat FILE_TYPE_RTF = new OfficeFileFormatRTF();
	public final static OfficeFileFormat FILE_TYPE_STW = new OfficeFileFormatSTW();
	public final static OfficeFileFormat FILE_TYPE_SXW = new OfficeFileFormatSXW();

	private KnownOfficeFileFormats()
	{
		// Do not instantiate
	}

	/**
	 * 
	 * Retrieves the <code>OfficeFileFormat</code>-Object for a given filename-extension
	 * 
	 * @param pExtension the extension to be retrieved
	 * @return the <code>OfficeFileFormat</code>-Object which fits the given filename-extension or <code>null</code> if none found
	 * @throws AmbiguousExtensionException is thrown if more than one <code>OfficeFileFormat</code> fits the given extension
	 */

	public static OfficeFileFormat getOfficeFileFormatForExtension(String pExtension) throws AmbiguousExtensionException
	{
		// Get attributes of this class
		Field [] fields = KnownOfficeFileFormats.class.getFields();

		// If an appropriate OfficeFileFormat for the extension is found, it will set to this variable
		OfficeFileFormat result = null;

		for(int i=0; i < fields.length; i++)
		{
			// Only search attributes which names begin with "FILE_TYPE" 
			if(fields[i].getName().startsWith("FILE_TYPE"))
			{
				try
				{
					OfficeFileFormat fileFormat = (OfficeFileFormat)fields[i].get(null);
					if(fileFormat.endsWithSuffix(pExtension.toLowerCase()))
					{
						// If the OfficeFileFormat has got the extension, check if we already found one. if not, set result
						if(result == null) result = fileFormat;
						else throw new AmbiguousExtensionException(); // if there has already been found an OfficeFileFormat for this extension, throw an Exception
					}
				}
				catch(Exception pExcp)
				{
					pExcp.printStackTrace();
					// TODO
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * Retrieves the <code>OfficeFileFormat</code>-Object for a given unique key
	 * 
	 * @param pKey the unique key
	 * @return a <code>OfficeFileFormat</code> that fits the given key or <code>null</code> if none found
	 */

	public static OfficeFileFormat getOfficeFileFormatByKey(String pKey)
	{
		// Get attributes of this class
		Field [] fields = KnownOfficeFileFormats.class.getFields();

		for(int i=0; i < fields.length; i++)
		{
			// Only search attributes which names begin with "FILE_TYPE" 
			if(fields[i].getName().startsWith("FILE_TYPE"))
			{
				try
				{
					OfficeFileFormat fileFormat = (OfficeFileFormat)fields[i].get(null);
					if(fileFormat.getKey().equals(pKey)) return fileFormat;
				}
				catch(Exception pExcp)
				{
					// TODO
				}
			}
		}
		return null;
	}
}
