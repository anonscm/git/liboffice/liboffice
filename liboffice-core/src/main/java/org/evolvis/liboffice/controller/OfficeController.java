/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 21.10.2004
 *
 */
package org.evolvis.liboffice.controller;

import java.util.List;

import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcher;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;


/**
 * Dieses Interface enth&auml;lt alle Grundfunktionen
 * die ein Office-Programm unterst&uml;tzen mu� um
 * von einem DocumentGenerator benutzt werden zu
 * k�nnen und mu� daher f&uml;r jedes unterst&uml;tzte 
 * Office-Programm implementiert werden.
 *  
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 */
public interface OfficeController extends MainOfficeFactoryFetcher, OfficeVersion
{  
	
  /**
   * f&uml;hrt abschlie�ende Operationen aus.
   * z.B. das Schliessen von Dokumenten die durch ein
   * closeDocument() nicht geschlossen werden konnten
   * weil irgend ein Prozess noch damit besch&auml;ftigt war.  
   */
  public void cleanUp(); 
    
  
  /**
   * liefert einen Handle auf das Dokument das zum Start des Generators 
   * aktiv war.
   * @return einen Handle auf das Dokument oder null im Fehlerfall
   */
  public TextDocument getStarterDocument() throws NoOfficeException; 
  
  /**
   * liefert eine Liste mit Handles auf alle offenen Dokumente 
   * @return eine Liste mit Handles auf alle offenen Dokumente
   */
  public List getOpenDocuments() throws NoOfficeException; 
  
  
  
  
  /**
   * l&auml;dt ein Dokument
   * @param filename - der Dateiname des Dokuments das geladen werden soll
   * @return einen Handle auf das geladene Dokument oder null im Fehlerfall
   */
  public TextDocument loadDocument(String filename); 
  
  /**
   * l&auml;dt ein Dokument
   * @param pFilename - der Dateiname des Dokuments das geladen werden sol
   * @param pVisible - ob das Dokument angezeigt werden soll
   * @return einen Handle auf das geladene Dokument oder null im Fehlerfall
   */
  
  public TextDocument loadDocument(String pFilename, boolean pVisible);
  
  /**
   * Opens a new document (usefull for open and edit templates)
   * @param pFilename - name of the file to load
   * @param pVisible - show document or not
   * @param readOnly - open the document in only read mode (user can make any changes but can't save them)
   * @return TextDocument
   */
  public TextDocument openDocument(String pFilename, boolean pVisible, boolean readOnly);
  
  /**
   * l&auml;dt ein Dokument als Kopie der Datei (wie ein Template)
   * @param filename - der Dateiname des Dokuments das geladen werden soll
   * @return einen Handle auf das geladene Dokument oder null im Fehlerfall
   */
  public TextDocument loadDocumentAsCopy(String filename); 
  
  /**
   * l&auml;dt ein Dokument als Kopie der Datei (wie ein Template)
   * @param pFilename - der Dateiname des Dokuments das geladen werden soll
   * @param pVisible - ob das Dokument angezeigt werden soll
   * @return einen Handle auf das geladene Dokument oder null im Fehlerfall
   */
  
  public TextDocument loadDocumentAsCopy(String pFilename, boolean pVisible);
  
  /**
   * �ffnet ein neues Dokument
   * @return einen Handle auf das neue Dokument oder null im Fehlerfall
   */
  public TextDocument newDocument();
  
  /**
   * �ffnet ein neues Dokument
   * @return einen Handle auf das neue Dokument oder null im Fehlerfall
   */
  public TextDocument newDocument(boolean visible);
  
  
  /**
   * liefert eine Liste mit allen von diesem Office-Interface unterst&uml;tzten Dateiformaten
   * @return - eine Liste mit OfficeFileFormat's 
   * @deprecated @deprecated replaced by more flexible method de.tarent.documents.configuration.OfficeTypeGroup.getSupportedOfficeFileFormats()
   */
  public List getSupportedFileFormats();

  
  /**
   * testet ob ein bekanntes Dateiformat von diesem Office-Interface unterst&uml;tzt wird
   * @return - true wenn das Format unterst&uml;tzt wird 
   * @deprecated replaced by more flexible method de.tarent.documents.configuration.OfficeTypeGroup.isSupportedFileFormatRead() / ~Write()
   */
  public boolean isFileFormatSupported(OfficeFileFormat pFileFormat);
  
  /**
   * testet ob ein bekanntes Dateiformat von diesem Office-Interface unterst&uml;tzt wird
   * @return - true wenn das Format unterst&uml;tzt wird 
   * @deprecated replaced by more flexible method de.tarent.documents.configuration.OfficeTypeGroup.isSupportedFileFormatRead() / ~Write()
   */
  public boolean isFileFormatSupported(String pKey);
  
  /**
   * testet ob ein bekanntes Dateiformat von diesem Office-Interface unterst&uml;tzt wird
   * @return - true wenn das Format unterst&uml;tzt wird
   * @deprecated replaced by more flexible method de.tarent.documents.configuration.OfficeTypeGroup.isSupportedFileFormatRead() / ~Write() 
   */
  public boolean isFileFormatSupported(Object key);
  
  /**
   * liefert eine Beschreibung eines bekannten Dateiformats
   * @return - OfficeFileFormat wenn das Format unterst&uml;tzt wird - null falls nicht.
   * @deprecated 
   */
  public OfficeFileFormat getFileFormat(String pKey);
  
  /** 
   * liefert eine Beschreibung eines bekannten Dateiformats
   * @return - OfficeFileFormat wenn das Format unterst&uml;tzt wird - null falls nicht.
   * @deprecated 
   */
  public OfficeFileFormat getFileFormat(Object key);
  
  
  /**
   * liefert die ControllerFactory &uml;ber die dieser Controller bezogen wurde...
   * @return - die ControllerFactory 
   */
  public ControllerFactory getControllerFactory();  
  
  /**
   * Retruns the active document as TextDocument
   * @return - the active document
   */
  public TextDocument getActiveDocument();
}
