/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 */
package org.evolvis.liboffice.controller;

import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcher;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Dieses Interface enth&auml;lt Hilfsfunktionen die den Umgang mit dem
 * OfficeController vereinfachen sollen.
 */
public interface OfficeControllerToolKit extends MainOfficeFactoryFetcher, OfficeVersion
{   
  /**
   * ermittelt die Anzahl der im Dokument vorhandenen Marker mit angegebenem Namen
   * @param document - der Handle auf das Dokument
   * @param markername - der Name des zu z&auml;hlenden Markers
   * @param hint - zus&auml;tzlicher Name des zu z&auml;hlenden Markers
   * @return - die Anzahl der Marker
   */
  public int getNumberOfMarkerOccurrences(TextDocument document, String markername, String hint);
    
  
  /**
   * ermittelt die Anzahl der im Dokument vorhandenen Marker
   * @param document - der Handle auf das Dokument
   * @return - die Anzahl der Marker
   */
  public int getNumberOfMarkers(TextDocument document);
  
  /**
   * f&uml;gt ein Dokument an der aktuellen Cursorposition ein
   * @param documenttoinsert - der Handle auf das Dokument in das eingef&uml;gt werden soll
   * @return - true bei Erfolg
   */
  public boolean insertDocument(TextDocument document, VirtualTextDocument documenttoinsert);  
 
  /**
   * f&uml;gt eine Komponente an der aktuellen Cursorposition ein
   * @param componenttoinsert - der Handle auf das Component in das eingef&uml;gt werden soll
   * @return - true bei Erfolg
   */
  public boolean insertVirtualTextComponent(TextDocument document, VirtualTextComponent componenttoinsert);  
 
  /**
   * springt mit dem Cursor an die Position des Markers (Platzhalter)
   * @param document - der Handle auf das Dokument
   * @param markername - der Name des Markers zu dem gesprungen werden soll
   * @return - true bei Erfolg (Marker wurde gefunden)
   */
  public boolean jumpToMarker(TextDocument document, String markername);
  
  /**
   * springt mit dem Cursor in eine spezielle Zelle einer Tabelle
   * @param document - der Handle auf das Dokument in dem die Tabelle liegt
   * @param table - ein Handle auf die Tabelle
   * @param row - die Zeile der Zelle
   * @param column - die Spalte der Zelle
   * @return - true bei Erfolg
   */
  public boolean jumpToTableCell(TextDocument document, TextTable table, int row, int column); 
  
  /**
   * springt mit dem Cursor vor eine Tabelle
   * @param document - der Handle auf das Dokument in dem die Tabelle liegt
   * @param table - ein Handle auf die Tabelle
   * @return - true bei Erfolg
   */
  public boolean jumpToTable(TextDocument document, TextTable table);
    
    
  /**
   * l�scht alle Marker aus dem Dokument
   * @param document - das Dokument aus dem Gel�scht werden soll
   * @return - true bei Erfolg
   */
  public boolean removeAllMarkers(TextDocument document);

  /**
   * f&uml;gt unformatierten Text an die aktuelle Cursorposition ein
   * @param document - das Dokument in das Eingef&uml;gt werden soll
   * @param text - der Text der eingef&uml;gt werden soll  
   * @return - true bei Erfolg
   */
  public boolean insertText(TextDocument document, String text);
  
  
  /**
   * l�scht einen Marker aus dem Dokument
   * @param document - das Dokument aus dem Gel�scht werden soll
   * @param markername - der Name des Markers der gel�scht werden soll
   * @param option - ein (optionaler) zweiter Name des Markers der gel�scht werden soll (ACHTUNG: wird momentan nur in OpenOffice unterst�tzt)
   * @return - true bei Erfolg
   */
  public boolean removeMarker(TextDocument document, String markername, String option);
  

  /**
   * liefert den Wert eines VirtualDocumentInformation-Objekts
   * unter einem angegebenen Schl&uml;ssel
   * wenn mehrere Objekte mit dem Selben Schl&uml;ssel vorhanden sind wird das erste gefundene �bergeben.
   * @param document - das Dokument dessen Dokumenteninformationen gelesen werden sollen.
   * @param key - der Schl&uml;ssel unter dem im Dokument gesucht werden soll
   * @return - der Wert der VirtualDocumentInformation oder null wenn nicht gefunden 
   */  
  public String getDocumentInformation(TextDocument document, String key);
  

  /**
   * setzt die R&auml;nder einer Tabelle so das sie einem Mitzeichnungskreuz 
   * entsprechen...
   * @param table - die Tabelle die R&auml;nder eines Mitzeichnungskreuzes bekommen soll
   * @return - true wenn erfolgreich 
   */    
  public boolean setCrossTableBorders(TextTable table);
  
  public boolean closeFirstDocument();
  
  public boolean closeAllDocuments();
}
