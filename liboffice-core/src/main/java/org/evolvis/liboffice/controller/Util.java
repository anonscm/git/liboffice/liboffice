/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 02.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.controller;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.locator.TextContentLocation;


/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Util 
{
	/** Umrechnungsfaktor von Nano-Metern in Pica */
	private final static double NANO_TO_PICA = 35.1;
    private final static Logger logger = Logger.getLogger(Util.class.getName());
    private static Transferable clipboardContents;

    /** Rechnet Nano-Meter in Pica um
	 * @param nanoMeters
	 * @return pica-Wert
	 */
	public static double convertNanoMetersToPica(double nanoMeters)
	{
		return (nanoMeters/NANO_TO_PICA);
	}
    
    public static double convertPicaToNanoMeters(double picaMeters)
    {
        double result = picaMeters * NANO_TO_PICA;
        return result ;
    }
    
    
    public static List mergeComponents(  List i, List j)
    {
        List out = new ArrayList();
        out.addAll( i );
        out.addAll( j );
        Collections.sort(out, new ComponentComparator());
        return out;
    }
    
    public static List sortComponents(  List l)
    {
        Collections.sort(l, new ComponentComparator());
        return l;
    }

    
    public static List mergeComponents(List[] lists)
    {
        List out = new ArrayList();
        for(int i=0; i<(lists.length); i++)
        {
          out.addAll(lists[i]);  
        }
        Collections.sort(out, new ComponentComparator());
        return out;
    }
    
    
    private static class ComponentComparator implements Comparator
    {
        public int compare(Object first, Object second)
        {
            if(first instanceof TextDocumentComponent && second instanceof TextDocumentComponent)
            {
                TextContentLocation locFirst  = ((TextDocumentComponent) first).getTextContentLocation() ;
                TextContentLocation locSecond = ((TextDocumentComponent) second).getTextContentLocation();
                return locSecond.compare( locFirst );
            }
            return 0;
        }
    }
    
    
    public static void saveClipboardContents()
    {
        Clipboard  clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboardContents = clipboard.getContents(null);
        logger.fine("saveClipboardContents");
    }
    
    public static void restoreClipboardContents()
    {
        logger.fine("restoreClipboardContents");
        if (null != clipboardContents)
        {
            Clipboard  clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(clipboardContents, null);
        }
    }
    
    
}
