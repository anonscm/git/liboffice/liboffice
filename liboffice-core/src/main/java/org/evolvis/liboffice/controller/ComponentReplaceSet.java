/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 */
package org.evolvis.liboffice.controller;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Ein ComponentReplaceSet kann ComponentReplaceJob's aufnehmen
 * um mehrere Marker durch Komponeten erstezen zu k�nnen
 */
public class ComponentReplaceSet
{
  private List m_oReplaceJobs = new LinkedList();
  
  
  /**
   * liefert einen Iterator &uml;ber im Set enthaltene ComponentReplaceJob's
   * @return ein Job-Iterator
   */
  public Iterator iterator()
  {
    return m_oReplaceJobs.iterator();
  }
  
  /**
   * liefert die Anzahl der enthaltenen ComponentReplaceJob's
   * @return - die Anzahl der ComponentReplaceJob's
   */
  public int getNumberOfJobs()
  {
    return m_oReplaceJobs.size();
  }
  
  /**
   * liefert einen ComponentReplaceJob
   * @param index - der Index des zu liefernden Jobs
   * @return - der Job oder null wenn der Index ung&uml;ltig war
   */
  public ComponentReplaceJob getJob(int index)
  {
    try
    {
      return (ComponentReplaceJob)(m_oReplaceJobs.get(index));   
    }
    catch(IndexOutOfBoundsException iobe)
    {
      return null;
    }
  }
  
  /**
   * liefert einen Replace-Job mit dem angegebenen Key
   * @param key - der Schl&uml;ssel nach dem gesucht werden soll
   * @return - der Job oder null wenn der Schl&uml;ssel ung&uml;ltig 
   * oder nicht vorhanden war
   */
  public ComponentReplaceJob getJob(String key)
  {
    Iterator it = iterator();
    while(it.hasNext())
    {
      ComponentReplaceJob job = (ComponentReplaceJob)(it.next());
      if (job.getKey().equals(key)) return job;
    }
    return null;   
  }
  
  /**
   * f&uml;gt einen Job in das ReplaceSet ein
   * @param job - der einzuf&uml;gende Job
   */
  public void addJob(ComponentReplaceJob job)
  {
    m_oReplaceJobs.add(job);
  }

  /**
   * f&uml;gt einen Job in das ReplaceSet ein
   * @param key - der Schl&uml;ssel des neuen Jobs
   * @param component - die Komponente des neuen Jobs
   */
  public void addJob(String key, VirtualTextComponent component)
  {
    m_oReplaceJobs.add(new ComponentReplaceJob(key, component));
  }
  
  
  /**
   * f&uml;gt einen Job in das ReplaceSet ein
   * @param key - der Schl&uml;ssel des neuen Jobs
   * @param component - die Komponente des neuen Jobs
   * @param removeLine - soll die komplette Zeile in der der
   *                     Marker stand gel�scht werden? 
   */
  public void addJob(String key, VirtualTextComponent component, boolean removeLine)
  {
    m_oReplaceJobs.add(new ComponentReplaceJob(key, component, removeLine));
  }
  
  
  /**
   * entfernt einen Job aus dem ReplaceSet
   * @param job - der zu entfernende Job
   */
  public void removeJob(ComponentReplaceJob job)
  {
    m_oReplaceJobs.remove(job);
  }
  
  /**
   * entfernt einen Job aus dem ReplaceSet
   * @param key - der zu entfernende Job
   */
  public void removeJob(String key)
  {
    Iterator it = m_oReplaceJobs.iterator();
    while (it.hasNext())
    {
        ComponentReplaceJob job = (ComponentReplaceJob)it.next();
        if(job.getKey().equals(key))
        {
            it.remove();
        }
    }
  }

}
