/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.controller;

import org.evolvis.liboffice.document.Selectable;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;

/**
 * 
 * This abstract class contains common methods for replacing tags in documents
 * 
 * Standard-Methods are: Replacement by one document per entity, one page for each entity or label-replacement 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface ReplacementToolKit
{
	// TODO recreate proper api for this without tarent-commons types 'Entity' and 'EntityList'
//	public boolean replaceAllMarkersByEntity(TextDocument pDocument, Entity pEntity, boolean pRemoveLine);
//	
//	public boolean replaceAllMarkersByEntityListInSingleDocument(TextDocument pDocument, String pFileName, EntityList pEntityList, boolean pRemoveLine, boolean pPageBreak, Context pProgressStatus);
//	
//	public boolean replaceAllMarkersByEntityListInLabelDocument(TextDocument pDocument, String pFileName, EntityList pEntityList, boolean pRemoveLine, Context pProgressStatus);
	
	public boolean replaceFirstMarkerByComponent(TextDocument document, String markername, VirtualTextComponent component);
	
	public boolean replaceFirstMarkerByComponent(TextDocument document, String markername, Selectable s);
	
	public boolean replaceAllMarkersByComponent(TextDocument pDocument, ComponentReplaceSet pReplaceSet);
}