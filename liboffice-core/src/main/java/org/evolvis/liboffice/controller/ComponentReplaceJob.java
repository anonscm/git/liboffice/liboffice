/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 06.12.2004
 *
 */
package org.evolvis.liboffice.controller;

import java.util.List;

import org.evolvis.liboffice.document.Selectable;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Ein ComponentReplaceJob ist ein Element des ComponentReplaceSet's
 * und beschreibt einen Ersetzungsvorgang.
 * Der Schl&uml;ssel ist der Name eines Markers der beim Ersetzungsvorgang 
 * durch die Textkomponente ausgetauscht wird.
 */
public class ComponentReplaceJob
{
  private final static int COMP_TYPE_VIRTUALTEXTCOMPONENT = 0;
  private final static int COMP_TYPE_SINGLE_SELECTABLE        = 1;
  private final static int COMP_TYPE_MULTI_SELECTABLE          = 2;
  
  private VirtualTextComponent m_oVirtualTextComponent    = null;
  private String m_sKey                                                      = null;
  private boolean m_bRemoveLine                                      = false;
  private byte selectableMode                                              = COMP_TYPE_VIRTUALTEXTCOMPONENT;
  private Selectable selectableComp                                      = null;
  private List selectableComps                                              = null;
  
  /**
   * @param key - der Name des Markers welcher ersetzt werden soll
   * @param component - die Komponente durch die ersetzt werden soll
   */
  public ComponentReplaceJob(String key, VirtualTextComponent component, boolean removeLine)
  {
    this(key, component);
    m_bRemoveLine = removeLine;
  }
  
  public ComponentReplaceJob(String key, VirtualTextComponent component)
  {
    m_oVirtualTextComponent = component;
    m_sKey = key;
    m_bRemoveLine = false;
  }
  
  
  /**
   * @param key - der Name des Markers welcher ersetzt werden soll
   * 
   */
  public ComponentReplaceJob(String key, Selectable sel, boolean removeLine)
  {
    this(key, sel);
    m_bRemoveLine = removeLine;
  }
  
  public ComponentReplaceJob(String key, Selectable sel)
  {
    selectableMode = COMP_TYPE_SINGLE_SELECTABLE;
    selectableComp = sel;
    m_sKey = key;
    m_bRemoveLine = false;
  }
  
  /**
   * @param key - der Name des Markers welcher ersetzt werden soll
   * 
   */
  public ComponentReplaceJob(String key, List sels, boolean removeLine)
  {
    this(key, sels);
    m_bRemoveLine = removeLine;
  }
  
  public ComponentReplaceJob(String key, List sels)
  {
    selectableMode = COMP_TYPE_MULTI_SELECTABLE;
    selectableComps = sels;
    m_sKey = key;
    m_bRemoveLine = false;
  }
  
  /**
   * liefert den Schl&uml;ssel des ReplaceJobs
   * @return der Name des Schl&uml;ssels (Marker-Name)
   */
  public String getKey()
  {
    return m_sKey;
  }
  
  /**
   * liefert die Komponente welche den Marker ersetzen soll
   * @return - die Komponete die den Marker ersetzen soll
   */
  public Object getComponent()
  {
      switch(selectableMode)
      {
          case COMP_TYPE_VIRTUALTEXTCOMPONENT: return m_oVirtualTextComponent;
          case COMP_TYPE_SINGLE_SELECTABLE: return selectableComp;
          case COMP_TYPE_MULTI_SELECTABLE: return selectableComps;
      }
      return null;
  }
  
  public boolean removeLine()
  {
      return m_bRemoveLine;
  }
  
}
