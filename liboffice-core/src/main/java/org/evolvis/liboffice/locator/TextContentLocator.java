/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator;

import java.util.List;
import java.util.Map;

import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcher;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public interface TextContentLocator extends MainOfficeFactoryFetcher
{
  
   /**
   * lokalisiert eine Komponente im Dokument
   * @param document - das zu durchsuchende Dokument
   * @param descriptor - eine Beschreibung der zu suchenden Komponente 
   * @return die Komponete oder null wenn nicht gefunden
   */
  public TextDocumentComponent locateComponent(TextDocument document, TextContentDescriptor descriptor);  
  
  
  /**
   * lokalisiert eine Liste von Komponenten im angegebenen Dokument.
   * Als Parameter wird eine Map erwartet, die als Schluessel einen eindeutigen Namen und als Wert einen @link{ TextContentDescriptor} enthaelt. 
   * Das Ergebnis ist wiederum eine Map, die als Schluessel die selben keys hat wie die Eingabemap. Die Werte der Ausgabemap sind die gefundenen TextDocumentsComponents. 
   * @param document - das zu durchsuchende Dokument
   * @param container - eine Map von Beschreibungen der zu suchenden Komponenten 
   * @return eine Map der gefundenen Komponenten oder null wenn nichts gefunden wurde
   */
  public Map getComponents(TextDocument document, TextContentDescriptorContainer container);

  /**
   * �berpr�ft ob die verwendete Version des Dokumentengenerators
   * eine spezielle Komponenten-Beschreibung unterst�tzt.
   * @param descriptor - die zu testende Komponenten-Beschreibung 
   * @return true wenn unterst�tzt sonst false
   */
  public boolean supportsDescriptor(TextContentDescriptor descriptor);
  
    

  /**
   * sammelt alle Komponenten die auf die angegebene Beschreibung passen
   * @param document - das Dokument das durchsucht werden soll
   * @param descriptor - die zu testende Komponenten-Beschreibung
   * @return eine Liste mit TextDocumentComponent's
   */
  public List collectComponents(TextDocument document, TextContentDescriptor descriptor);
  
  public TextMarker getNextTextMarker(TextDocument pDocument);
}
