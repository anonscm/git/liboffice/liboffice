/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.locator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.evolvis.liboffice.descriptors.AbstractTableDescriptor;
import org.evolvis.liboffice.descriptors.FrameDescriptor;
import org.evolvis.liboffice.descriptors.GraphicDescriptor;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.descriptors.TextDescriptor;


public class TextContentDescriptorContainer
{
    private Map tables;
    private Map paragraphs;
    private Map frames;
    private Map markers;
    private Map graphics;
    
    private Set all;
    //TODO ueberpruefen ob die keys eindeutig sind
    
    public TextContentDescriptorContainer()
    {
        
        tables          = new HashMap();
        paragraphs  = new HashMap();
        frames         = new HashMap();
        markers       = new HashMap();
        graphics       = new HashMap();
        all                = new HashSet();
    }
    
    public void addItem(Object key, TextContentDescriptor desc, boolean locate)
    {
        
        if (all.contains(key)) throw new IllegalArgumentException("Key already exists");
        all.add( key );
        
        if (desc instanceof AbstractTableDescriptor)
        {
            tables.put(key, new DescriptorWrapper(desc, locate));
        }
        else if (desc instanceof FrameDescriptor)
        {
            frames.put(key, new DescriptorWrapper(desc, locate));
        }
        else if (desc instanceof TextDescriptor)
        {
            paragraphs.put(key, new DescriptorWrapper(desc, locate));
        }
        else if (desc instanceof NameHintMarkerDescriptor)
        {
            markers.put(key, new DescriptorWrapper(desc, locate));
        }
        else if(desc instanceof GraphicDescriptor)
        {
            graphics.put(key, new DescriptorWrapper(desc, locate));
        }
        else throw new RuntimeException("Unknown descriptor: " + desc.getClass());
    }
    
    public void addItem(String key, TextContentDescriptor desc)
    {
      this.addItem(key, desc, true);  
    }

    public Map getFrames()
    {
        return this.frames;
    }

    public Map getParagraphs()
    {
        return this.paragraphs;
    }

    public Map getTables()
    {
        return this.tables;
    }

    public Map getGraphics()
    {
        return this.graphics;
    }

    public Map getMarkers()
    {
        return this.markers;
    }
    
    
    public class DescriptorWrapper
    {
        private TextContentDescriptor desc;
        private boolean locate;
        public DescriptorWrapper(TextContentDescriptor desc, boolean locate)
        {
            this.desc   = desc;
            this.locate = locate;
        }
        
        public boolean isLocate()
        {
            return ( this.locate );
        }
        
        public TextContentDescriptor getDescriptor()
        {
            return this.desc;
        }
    }
}
