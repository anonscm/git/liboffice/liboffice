/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator;

import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public interface TextContentLocation
{
  /**
   * reduziert die Selektion auf ihren Anfang
   */
  public void collapseToStart(); 

  /**
   * reduziert die Selektion auf ihr Ende
   */
  public void collapseToEnd();
  
  /**
   * springt zum Anfang der Selektion
   * @param collapse - soll der Bereich dorthin selektiert werden
   */
  public void gotoStart(boolean collapse); 

  /**
   * springt zum Ende der Selektion
   * @param collapse - soll der Bereich dorthin selektiert werden
   */
  public void gotoEnd(boolean collapse); 

  
  /**
   * bewegt die Selektion nach Links
   * @param steps - Anzahl der Zeichen
   * @param collapse - soll der neu erfasste Bereich mit in die Selektion
   */
  public void goLeft(int steps, boolean collapse); 
  
  /**
   * bewegt die Selektion nach Rechts
   * @param steps - Anzahl der Zeichen
   * @param collapse - soll der neu erfasste Bereich mit in die Selektion
   */
  public void goRight(int steps, boolean collapse); 

  
  /**
   * bewegt die Selektion nach oben
   * @param steps - Anzahl der Zeichen
   * @param collapse - soll der neu erfasste Bereich mit in die Selektion
   */
  public boolean goUp(int steps, boolean collapse); 

  /**
   * bewegt die Selektion nach Unten
   * @param steps - Anzahl der Zeichen
   * @param collapse - soll der neu erfasste Bereich mit in die Selektion
   */
  public boolean goDown(int steps, boolean collapse); 

  
  
  /**
   * vergleicht die Position zweier Selektionen
   * @param location - die Selektion mit der die 
   *                   aktuelle Selektion verglichen 
   *                   werden soll
   * @return - 0 wenn beide Selektionen an der selben Position stehen
   *          -1 wenn die angegebene Selektion VOR der eigenen liegt 
   *           1 wenn die angegebene Selektion NACH der eigenen liegt 
   */
  public int compare(TextContentLocation location);

  
  /**
   * positioniert sich _hinter_ die angegebene Komponente
   * @param component - die Komponente hinter der sich 
   *                    positioniert werden soll
   * @return - true wenn Erfolg sonst false
   */
  public boolean positionAfterComponent(TextDocumentComponent component, TextDocument doc);
  
  
}
