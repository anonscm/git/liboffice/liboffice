/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 27.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document;

import org.evolvis.liboffice.locator.TextContentLocation;



/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public interface TextFrame extends TextDocumentComponent
{
  /**
   * liefert die Breite eines Frames
   * @return - die Breite des Frames 
   */
  public float getWidth();

  /**
   * liefert die H�he eines Frames
   * @return - die H�he des Frames 
   */
  public float getHeight();

  /**
   * liefert die Y-Koordinate der Position eines Frames
   * @return - die Y-Koordinate der Position des Frames 
   */
  public float getPositionTop();

  /**
   * liefert die X-Koordinate der Position eines Frames
   * @return - die X-Koordinate der Position des Frames 
   */
  public float getPositionLeft();
  
  /**
   * setzt die Breite eines Frames
   * @param width - die neue Breite des Frames 
   * @return - true wenn erfolgreich 
   */
  public boolean setWidth(float width);

  /**
   * setzt die H�he eines Frames
   * @param height - die neue H�he des Frames 
   * @return - true wenn erfolgreich 
   */
  public boolean setHeight(float height);

  /**
   * setzt die Y-Koordinate der Position eines Frames
   * @param top - die neue Y-Koordinate der Position 
   * @return - true wenn erfolgreich 
   */
  public boolean setPositionTop(float top);

  /**
   * setzt die X-Koordinate der Position eines Frames
   * @param left - die neue X-Koordinate der Position 
   * @return - true wenn erfolgreich 
   */
  public boolean setPositionLeft(float left);
  
  /**
   * setzt die Gr��e eines Frames
   * @param width - die neue Breite des Frames 
   * @param height - die neue H�he des Frames 
   * @return - true wenn erfolgreich 
   */
  public boolean setSize(float width, float height);

  /**
   * setzt die Position eines Frames
   * @param top - die neue Y-Koordinate der Position des Frames 
   * @param left - die neue X-Koordinate der Position des Frames 
   * @return - true wenn erfolgreich 
   */
  public boolean setPosition(float top, float left);
  
  /**
   * legt fest ob das Frame ausgedruckt werden soll
   * @param printable - true wenn das Frame ausgedruckt 
   * werden soll, false wenn nicht
   * @return - true bei Erfolg, false im Fehlerfall 
   */
  public boolean setPrintable(boolean printable);
  
  /**
   * liefert die TextContentLocation des Frames
   * @return - TextContentLocation des Frames 
   */
  public TextContentLocation getFrameContentLocation();
}
