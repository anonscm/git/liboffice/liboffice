/**
 * 
 */
package org.evolvis.liboffice.document;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface Span {

	public int getStart();
	public int getEnd();
	
	public void setStart(int start);
	public void setEnd(int start);
}
