/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.03.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document;

import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public interface TextStyle
{
  public final static Object STYLE_PARAGRAPH = "STYLE_PARAGRAPH";
  public final static Object STYLE_CHARACTER = "STYLE_CHARACTER";
  
  
  /**
   * liefert den Namen des Styles
   * @return - der Name des Styles
   */
  public String getStyleName();
  
  /**
   * liefert den Typ des Styles
   * @return - der Typ des Styles
   */
  public Object getStyleType();
  
  /**
   * liefert den Font des Styles
   * @return - ein Objekt des Typs TextFont das  
   *           die Beschreibung des Fonts enth&auml;lt
   */
  public TextFont getTextFont();
  
  /**
   * liefert die virtuelle Beschreibung eines Styles
   * @return - die virtuelle Beschreibung des Styles
   */
  public VirtualTextStyle parseStyle();
  
}
