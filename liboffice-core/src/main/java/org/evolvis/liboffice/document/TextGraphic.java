/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 27.10.2004
 *
 */
package org.evolvis.liboffice.document;




/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * Dieses Interface stellt die gemeinsame Basis f�r in Textdokumente
 * eingebettete Graphiken
 */
public interface TextGraphic extends TextDocumentComponent
{
	/**
	 * ermittelt die Breite der Graphik (in 1000stel Zentimeter)
	 * @return - die Breite der Graphic (1000 = 1cm)
	 */
	public float getWidth();

	/**
	 * ermittelt die H�he der Graphik (in 1000stel Zentimeter)
	 * @return - die H�he der Graphic (1000 = 1cm)
	 */
	public float getHeight();

	/**
	 * ermittelt den Dateinamen der Graphik
	 * diese Methode funktioniert nur wenn 
	 * die Graphik als verkn�pftes Objekt angelegt wurde!
	 * @return - der Dateiname der Graphik
	 */
	public String getURL();

	/**
	 * liefert den Namen der Graphik
	 * ACHTUNG: diese Eigenschaft geht bei der 
	 * Konvertierung zwischen Word und OpenOffice verloren
	 * @return - der Name der Graphik
	 */
	public String getName();

	/**
	 * ermittelt ob die Graphik vom Drucken ausgenommen wurde
	 * @return - true wenn die Graphik gedruckt werden soll, false wenn nicht
	 */
	public boolean isPrintable();

	/**
	 * legt fest ob die Graphik ausgedruckt werden soll
	 * @param printable - true wenn die Graphik ausgedruckt 
	 * werden soll, false wenn nicht
	 * @return - true bei Erfolg, false im Fehlerfall 
	 */
	public boolean setPrintable(boolean printable);

	/**
	 * setzt den Kontrast der Graphik
	 * Wertebereich: 0.0 bis 1.0
	 * 0.5 f�r Normaleinstellung.
	 * @param contrast - der Kontrast der Graphik
	 * @return - true bei Erfolg, false im Fehlerfall
	 */
	public boolean setContrast(double contrast);

	/**
	 * setzt die Helligkeit der Graphik
	 * Wertebereich: 0.0 bis 1.0
	 * 0.5 f�r Normaleinstellung.
	 * @param brightness - die Helligkeit der Graphik
	 * @return - true bei Erfolg, false im Fehlerfall
	 */
	public boolean setBrightness(double brightness);

	/**
	 * ermittelt den Kontrast der Graphik
	 * @return - der Kontrast der Graphik oder NaN im Fehlerfall
	 */
	public double getContrast();

	/**
	 * ermittelt die Helligkeit der Graphik
	 * @return - die Helligkeit der Graphik oder NaN im Fehlerfall
	 */
	public double getBrightness();
}
