/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.document;

import java.util.List;
import java.util.Map;

import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.locator.TextContentDescriptorContainer;
import org.evolvis.liboffice.locator.TextContentLocator;


public abstract class AbstractTextDocument implements TextDocument
{
    protected TextContentLocator locator;
    
    public TextDocumentComponent locateTextDocumentComponent(TextContentDescriptor descriptor)
    {
        return getTextContentLocator().locateComponent(this, descriptor);
    }

    public List collectTextDocumentComponents(TextContentDescriptor descriptor)
    {
        return getTextContentLocator().collectComponents(this, descriptor);
    }
    
    public Map getTextDocumentComponents(TextContentDescriptorContainer container)
    {
       return getTextContentLocator().getComponents(this, container);
    }
    
    public TextMarker getNextTextMarker()
    {
    	return getTextContentLocator().getNextTextMarker(this);
    }
    
    protected abstract TextContentLocator getTextContentLocator(); 
}
