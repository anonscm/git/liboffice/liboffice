package org.evolvis.liboffice.document;


public interface TextVariable  {

	public String getName();
	
	public String getValue();
	
	public void setValue(String value);
	
	public void delete();



}
