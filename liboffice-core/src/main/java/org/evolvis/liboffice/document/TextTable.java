/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 26.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document;

import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;



/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public abstract class TextTable implements TextDocumentComponent, Selectable
{
  
  /**
   * f�gt Zeilen einer Tabelle hinzu 
   * @param index - Zeile hinter der die neuen Zeilen 
   *                eingef�gt werden sollen
   *                Die erste Zeile hat den Index 0  
   * @param count - Anzahl der hinzuzuf�genden Zeilen
   */
  public abstract void addRows(int index, int count);
  
  public abstract void addColumns(int index, int count);
  
  /**
   * entfernet Zeilen aus einer Tabelle
   * @param index - erste Zeile die gel�scht werden soll
   *                Die erste Zeile hat den Index 0
   * @param count - Anzahl der zu l�schenden Zeilen
   */
  public abstract void removeRows(int index, int count);
  
  
  /**
   * liefert die Anzahl der Zeilen einer Tabelle
   * @return die Anzahl der Zeilen
   */
  public abstract int getNumberOfRows();

  /**
   * liefert die Anzahl der Spalten einer Tabelle
   * @return die Anzahl der Spalten
   */  
  public abstract int getNumberOfColumns();
  
  /**
   * f�gt den Inhalt einer Zelle in eine Tabelle ein
   * @param row - die Zeile der Zelle
   *              Die erste Zeile hat den Index 0
   * @param column - die Spalte der Zelle
   *                 Die erste Spalte hat den Index 0
   * @param cell - die virtuelle Beschreibung der einzuf�genden Tabelle
   */
  public abstract void insertCell(int row, int column, VirtualTextTableCell cell);

  
  /**
   * l�scht den Inhalt einer Zelle
   * @param row - die Zeile der Zelle
   *              Die erste Zeile hat den Index 0
   * @param column - die Spalte der Zelle
   *                 Die erste Spalte hat den Index 0
   */
  public abstract void clearCell(int row, int column);
  
  public abstract TextTableCell getCell(int row, int column);
  
  /**
   * &auml;ndert die Sichtbarkeit von einzelnen Zellen in einer Tabelle
   * @param row - die Zeile der Zelle
   * @param col - die Spalte der Zelle
   * @param line - gibt an welche Linie der Zelle ge&auml;ndert werden soll
   *               erlaubte Werte: TextTableCell.BORDER_LEFT, TextTableCell.BORDER_RIGHT, TextTableCell.BORDER_TOP, TextTableCell.BORDER_BOTTOM
   * @return - true bei Erfolg
   */
  public abstract boolean setCellBorderLineVisible(int row, int col, Object line, boolean visible);
  
  
  /**
   * setzt den linken Rand einer Tabelle 
   * @param margin - der neue linke Rand (1000 = 1cm)
   * @return - true bei Erfolg
   */    
  public abstract boolean setTableLeftMargin(float margin);

  
  /**
   * setzt die Breite einer Tabelle 
   * @param width - die neue Breite (1000 = 1cm)
   * @return - true bei Erfolg
   */    
  public abstract boolean setTableWidth(float width);

  
  
  
  
  public boolean containsStyle(int row, int col, String styleName)
  {
      if ((row == -1) || (col == -1))
      {
        int numrows = getNumberOfRows();
        int numcols = getNumberOfColumns();

        for(int rowIndex=0; rowIndex<numrows; rowIndex++)
        {
          for(int colIndex=0; colIndex<numcols; colIndex++)
          {
              if (getCell(rowIndex, colIndex).containsStyle( styleName )) return true;
          }          
        }
        return false;
      }
      return getCell(row, col).containsStyle( styleName );
  }
  
  // TODO what is this method for?
//  public String getComponentName(XTextContent comp)
//  {
//    XNamed xNamed = (XNamed) UnoRuntime.queryInterface(XNamed.class, comp); 
//    if (xNamed != null) return xNamed.getName();
//    return("*?*");
//  }
  
  public boolean containsText(int iRow, int iCol, String text)
  {
        int numrows = getNumberOfRows();
        int numcols   = getNumberOfColumns();
        
        if ((iRow == -1) || (iCol == -1))
        {
          for(int row = 0; row<(numrows); row++)
          {
            for(int col = 0; col<(numcols); col++)
            {
                String cellText = getCell(row, col).getPlainText();
                if (cellText.equalsIgnoreCase(text)) return true;
            }            
          }          
        }
        else
        {
          if ((numcols > iCol) && (numrows > iRow))
          {
              String cellText = getCell(iRow, iCol).getPlainText();
              if (cellText.equalsIgnoreCase(text)) return true;
          }          
        }        
      return false;  
  }

  public abstract void removeColumns(int index, int count);

}