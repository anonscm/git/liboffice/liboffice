/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 22.10.2004
 *
 */
package org.evolvis.liboffice.document;

import java.util.List;
import java.util.Map;

import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.controller.OfficeWindowListener;
import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.locator.TextContentDescriptorContainer;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.TextContentRange;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.officefileformats.UnsupportedOfficeFileTypeException;
import org.evolvis.liboffice.virtualdocument.VirtualTextFrame;
import org.evolvis.liboffice.virtualdocument.graphic.VirtualGraphic;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;



/**
 * 
 * An interface for Text-Documents
 * 
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 */
public interface TextDocument extends TextDocumentComponent
{
	/**
	 * Registers an OfficeWindowListener
	 * 
	 * @param listener the listener which should be registered
	 * @return - true if successful
	 */    
	public boolean addOfficeWindowListener(OfficeWindowListener listener);

	/**
	 * Generates a copy of the document
	 * @return the document-copy
	 */
	public TextDocument cloneDocument(OfficeController controller);

	/**
	 * Closes the document. If it has changed, a dialog asks the user to confirm
	 * 
	 * @return true if successful
	 */
	public boolean close();

	/**
	 * Closes the document
	 * 
	 * @param forceCloseOnChanges If the document should be closed even if there are changes
	 * @return true if successful
	 */
	public boolean close(boolean forceCloseOnChanges);

	/**
	 * TODO what does this method?? (Fabian)
	 * 
	 * @param collapseToEnd
	 */
	public void collapseSelection(boolean collapseToEnd);

	/**
	 * Seeks the document for Components
	 *  
	 * @param descriptor a description for the components to be searched 
	 * @return a List of @link{TextDocumentComponent}s containing all components found 
	 */
	public List collectTextDocumentComponents(TextContentDescriptor descriptor);

	/**
	 * Copys the current selection to the clipboard
	 * @return true if successful
	 */
	public boolean copy();

	/**
	 * Copys a style from this document
	 * 
	 * TODO does not make much sense to me (Fabian)
	 * 
	 * @param oldStyle the style to be copied
	 * @param newStyleName the new name of the copied style
	 * @return the appropriate TextStyle or null if not found
	 */    
	public TextStyle copyTextStyle(VirtualTextStyle oldStyle, String newStyleName);

	/**
	 * Checks if the given style exists in this document
	 * 
	 * @param style the style to be checked for existence
	 * @return true if the given style is contained in this document
	 */
	public boolean existsStyle(VirtualTextStyle style);

	/**
	 * 
	 * Delivers the name of the current printer
	 * 
	 * TODO does not make much sense to me here (Fabian)
	 * 
	 * @return the name of the printer or null if no printer found/selected
	 */
	public String getActivePrinterName();

	/**
	 * Delivers all available font-names
	 * 
	 * @return an String-Array containing the names of all available fonts 
	 */
	public String[] getAvailableFontNames();

	/**
	 * Delivers the botton-margin of this document
	 * 
	 * @return the bottom-margin in millimeters
	 */  
	public float getBottomMargin();


	/**
	 * Delivers the current cursor-position in this document
	 * 
	 * @return a TextContentLocation of the cursor oder null if unsuccessful
	 */
	public TextContentLocation getCurrentCursorLocation();


	/**
	 * Delivers the current document-filename or null if it has not been saved yet
	 * 
	 * @return the filename of this document
	 */
	public String getDocumentFilename();


	/**
	 * liest die Liste von VirtualDocumentInformation-Objekten
	 * eines Dokuments. Dabei wird das Element StorageLocation gef&uml;llt
	 * welches den Leseort angibt.
	 * 
	 * @return - eine Liste mit Elementen des Typs VirtualDocumentInformation 
	 */  
	public List getDocumentInformations();


	/**
	 * Delivers a Range covering the whole document
	 * 
	 * @return a TextContentRange covering the complete document
	 */
	public TextContentRange getDocumentRange();


	/**
	 * Delivers a TextContentLocation which describes the end of this document
	 * 
	 * @return a TextContentLocation describing the end of this document
	 */
	public TextContentLocation getEnd();


	/**
	 * Delivers the left margin of this document
	 * 
	 * @return the left margin in millimeters
	 */
	public float getLeftMargin();

	/**
	 * TODO what does this method exactly? (fabian)
	 * 
	 * @return
	 */
	public TextMarker getNextTextMarker();

	/**
	 * Returns the height of this document
	 * 
	 * @return the height of this document in millimeters
	 */  
	public float getPageHeight();

	/**
	 * Spots the page on which a given TextContentLocation is located
	 * 
	 * @param location the TextContentLocation describing the position
	 * @return the number of the page or -1 if out of document-range
	 */
	public int getPageNumber(TextContentLocation location);


	/**
	 * Delivers the width of this document
	 * 
	 * @return the width of this document in millimeters
	 */  
	public float getPageWidth();

	/**
	 * Delivers the right margin of this document
	 * 
	 * @return the right margin of this document in millimeters
	 */
	public float getRightMargin();

	/**
	 * Delivers a TextContentLocation describing the start of this document
	 * 
	 * @return a TextContentLocation describing the start of this document
	 */
	public TextContentLocation getStart();

	/**
	 * Delivers a TextContenLocation describing the start of the given page
	 * 
	 * TODO what happens if I specify a page-number out of document-range? (Fabian)
	 * 
	 * @param page the page-number
	 * @return a TextContentLocation describing the start of the given page
	 */
	public TextContentLocation getStartOfPage(int page);

	/**
	 * TODO What does this method exactly do? (fabian)
	 * 
	 * @param container
	 * @return
	 */
	public Map getTextDocumentComponents(TextContentDescriptorContainer container);

	/**
	 * Delivers the appropriate (concrete) TextStyle-Object for a given stylename
	 * 
	 * @param stylename the name of the desired style
	 * @return the corresponding TextStyle or null if not found
	 */  
	public TextStyle getTextStyle(String stylename);

	/**
	 * Delivers the top-margin of this document
	 * 
	 * @return the top-margin of this document in millimeters
	 */  
	public float getTopMargin();

	/**
	 * Inserts a document at the end of this document, with page-break before
	 * 
	 * @param filename the filename of the document to be inserted
	 * @return true if successful
	 */
	public boolean insertDocumentAtEnd(String filename);

	/**
	 * Inserts a document at the start of this document, with page-break after
	 * 
	 * @param filename the filename of the document to be inserted
	 * @return true if successful
	 */
	public boolean insertDocumentAtStart(String filename);

	/**
	 * Inserts a document at the current cursor-position
	 * 
	 * @param filename the filename of the document to be inserted
	 * @return true if successful
	 */
	public boolean insertDocumentFromFile(String filename);

	/**
	 * Inserts a graphic at the current cursor-position
	 *  
	 * @param graphic the virtual graphic to be inserted
	 * @return true if successful
	 */
	public TextGraphic insertGraphic(VirtualGraphic graphic);


	/**
	 * Inserts a line-break at the current cursor-position
	 * 
	 * @return true if succcessful
	 */
	public boolean insertLineBreak();


	/**
	 * Inserts a marker at the current cursor-position
	 * 
	 * @param marker the marker to be inserted 
	 * @return true if successful
	 */
	public TextMarker insertMarker(VirtualTextPortionMarker marker);


	/**
	 * Inserts a page-break at the current cursor-position
	 * 
	 * @return true if successful
	 */
	public boolean insertPageBreak();

	/**
	 * Inserts a paragraph-break at the current cursor-position
	 * 
	 * @return - true bei Erfolg
	 */
	public void insertParaBreak();

	/**
	 * Adds a style to this documents
	 * 
	 * @param style the virtual style to be added
	 * @return true if successful
	 */
	public boolean insertStyle(VirtualTextStyle style);


	/**
	 * Inserts a table at the current cursor-position
	 * 
	 * @param table the table to be inserted
	 * @return true if successful
	 */
	public TextTable insertTable(VirtualTextTable table);

	/**
	 * Inserts a text at the current cursor-position
	 * 
	 * @param text the virtual text to be inserted
	 * @return true if successful
	 */
	public boolean insertText(VirtualStyledText text);

	/**
	 * Inserts a TextFrame at the current cursor-position
	 * 
	 * @param frame the virtual frame to be inserted
	 * @return the frame which has been inserted or null if unsuccessful
	 */
	public TextFrame insertTextFrame(VirtualTextFrame frame);
	
	/**
	 * Inserts a bookmark at the current cursor-position
	 * @return
	 */
	public TextBookmark insertTextBookmark(String name);
	
	/**
	 * Inserts a bookmark at the given range
	 * @return
	 */
	public void insertTextBookmark(String name, Span range);


	/**
	 * Sets the cursor to the given position
	 * 
	 * @param location the postion for the cursor to be set to
	 * @return true if successful
	 */
	public boolean jumpToLocation(TextContentLocation location);


	/**
	 * Seeks the document for a component
	 * 
	 * @param descriptor a description for the component to be searched
	 * @return the found component or null if none found
	 */
	public TextDocumentComponent locateTextDocumentComponent(TextContentDescriptor descriptor);

	/**
	 * Pastes from the clipboard
	 * 
	 * TODO what format is used? (fabian)
	 * 
	 * @return true if successful
	 */
	public boolean paste();

	/**
	 * Pastes from the clipboard
	 * 
	 * TODO where the heck are the paste-formats documented?? (fabian)
	 * 
	 * @param pPasteFormat the desired paste-format
	 * @return true if successful
	 */
	public boolean pasteSpecial(int pPasteFormat);

	/**
	 * <p>Prints this document</p>
	 * 
	 * <p><b>Note</b>: The index of the first page is 1, not 0 !<br />
	 * If one of the parameters frompage or topage is 0, the complete document will be printed
	 * </p>
	 * 
	 * @param printer the name of the desired printer (null if the current printer should be used)
	 * @param frompage the first page of the print-range (inclusively)
	 * @param topage the last page of the print-range (inclusively)
	 * @return true if successful
	 */
	public boolean print(String printer, int frompage, int topage);

	/**
	 * Removes a marker from this document
	 * 
	 * @param marker the marker to be removed
	 * @return true if successful
	 */
	public boolean removeMarker(TextMarker marker);

	/**
	 * Removes a marker from this document
	 * 
	 * @param marker the marker to be removed
	 * @return true if successful
	 */
	public boolean removeMarker(VirtualTextPortionMarker marker);

	/**
	 * Removes a previously registerd OfficeWindowListener
	 * 
	 * @param listener the listener to be removed
	 * @return true if successful
	 */    
	public boolean removeOfficeWindowListener(OfficeWindowListener listener);

	/**
	 * Removes a style from this document
	 * 
	 * @param style the style to be removed
	 */    
	public void removeTextStyle(VirtualTextStyle style);

	/**
	 * Saves this document
	 * 
	 * @param filename the filename to be saved to
	 * @return true if successful
	 */
	public boolean save(String filename, OfficeFileFormat filetype, boolean overwrite) throws UnsupportedOfficeFileTypeException;

	/**
	 * Selects the complete document
	 */
	public void select();

	/**
	 * schreibt eine Liste von VirtualDocumentInformation-Objekten
	 * in das Dokument. Dabei wird das Element StorageLocation beachtet
	 * um den Speicherort zu bestimmen.
	 * @param virtualdocumentinformations - eine Liste mit Elementen des Typs VirtualDocumentInformation 
	 * @return - true bei Erfolg
	 */
	public boolean setDocumentInformations(List virtualdocumentinformations);


	/**
	 * Sets visibility of the document's window
	 * 
	 * @param visible if this document's window should be visible 
	 */
	public void setVisible(boolean visible);

	/**
	 * <p>Sets whether the office-GUI should be updated</p>
	 * 
	 * <p>If set to true the user can see all processes done by the document-generator and maybe also interact with the document<br />
	 * So If you want to avoid the user manipulating the document while the automized GDG-processes are working on it, set to false<br />
	 * Can also be used to hide the flickering process from the user.</p>
	 * <p>
	 * See also the setVisible-Method, which completely hides the window.
	 * </p>
	 * 
	 * @param active whether the office-GUI should be updated
	 */
	public void setVisualUpdateActive(boolean active);

	/**
	 * Puts this document's window to the background
	 */  
	public void toBack();

	/**
	 * Puts this document's window to the foreground.
	 * 
	 * Same as toFront(false);
	 */
	public void toFront();
	
	/**
	 * 
	 * Puts this document's window to the foreground
	 * 
	 * You can force the window to pop up using the force-flag.
	 * 
	 * For example Windows XP normally does not allow minimized windows to pop up. 
	 * 
	 * @param force - if the document should be forced to pop up. 
	 */
	public void toFront(boolean force);
	
	public TextBookmark getBookmark(String name);
	
	/**
	 * Retruns the list of all bookmarks as Ms of Oo Bookmark objects
	 * @param sortByName Sort by name if true or by location if false
	 * @return
	 */
	public List getBookmarks(boolean sortByName);
	
	/**
	 * Retruns the list of the book mark names.
	 * @param sortByName
	 * @return
	 */
	public List<String> getBookmarkNames (boolean sortByName);
	
	public boolean existsBookmark(String bookmarkName);
	
	public void copyContent(TextBookmark startBookmark, TextBookmark endBookmark);
	
	public void insertVariable(String name, String value);
	
	
	public TextVariable getTextVariable(String name);

	public void copyContentAfterBookmark(String bookmarkName);
	
	/**
	 * Delete at the current cursor position (same as pushing "del"-Button)
	 */
	public void delete();
	
	/**
	 * Selects the text in the whole document and returns it as a string.
	 * @return
	 */
	public String getDocumentText();
	
	/**
	 * This method returns all founded Variables in the document
	 * @return
	 */
	public List<TextVariable> getTextVariables();
	
	/**
	 * Deletes an paragraph containing a parameter book mark. If book mark was not found
	 * then the method returns false.
	 * @param bookmarkName 
	 * @return true if bookmark was found and paragraph was deleted
	 */
	public boolean deleteParagraph(String bookmarkName);
	
	/**
	 * Sets the document as active. This is needen if you working with more then one document at the same time
	 * (and in the same word instance). 
	 */
	public void activate();
	
	/**
	 * Inserts a section break. There are several break types that can be used. 
	 * TODO: Need to add parameter to specify the needed break.
	 */
	public void insertSectionBreakNextPage();
}
