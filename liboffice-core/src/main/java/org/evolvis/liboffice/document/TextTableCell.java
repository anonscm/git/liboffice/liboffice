/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 26.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document;




/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public interface TextTableCell extends TextDocumentComponent
{
  public final static Object BORDER_LEFT = "BORDER_LEFT";
  public final static Object BORDER_RIGHT = "BORDER_RIGHT";
  public final static Object BORDER_TOP = "BORDER_TOP";
  public final static Object BORDER_BOTTOM = "BORDER_BOTTOM";
  
  public String getPlainText();
  public boolean containsStyle(String styleName);
}
