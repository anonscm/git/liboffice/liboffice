/**
 * 
 */
package org.evolvis.liboffice.document;



/**
 * Represents book mark (German: Textmarke) used in text documents. You can imagine the book mark as a
 * named location in the text document. 
 * @author aboulg
 *
 */
public interface TextBookmark extends TextDocumentComponent {

	public String getName();
	
	public String getText();
	
	public void setText(String content);
	
	public Span getSpan();	
	
	public int getStart();
	
	public void setStart(int position);
	
	public int getEnd();
	
	public void setEnd(int position);
	
	public void select();
	
	public void collapse();
	
	public boolean getEmpty();
	
	public void setCursorAtStart();
	
	public void setCursorAtEnd();

	public void setName(String newName);
	
	public void insertTextBefore(String text);
	
	public void insertTextAfter(String text);
	
	
}
