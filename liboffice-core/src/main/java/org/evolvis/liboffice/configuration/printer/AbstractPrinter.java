/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.printer;

import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.ServiceUIFactory;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintServiceAttribute;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.event.PrintServiceAttributeListener;

import org.evolvis.liboffice.configuration.Preferrable;


/**
 * An abstract representation of a printer-device.<br />
 * <br />
 * Basically this class merges the PrintService-Interface which is widely used by the Java-Printing-API and the Preferrable-Interface which adds the ability to set this printer to be the preferred one 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public abstract class AbstractPrinter implements PrintService, Preferrable
{
	boolean isPreferred;
	
	public abstract void addPrintServiceAttributeListener(PrintServiceAttributeListener arg0);
	
	public abstract DocPrintJob createPrintJob();

	public abstract PrintServiceAttribute getAttribute(Class arg0);

	public abstract PrintServiceAttributeSet getAttributes();

	public abstract Object getDefaultAttributeValue(Class arg0);

	public abstract String getName();

	public abstract ServiceUIFactory getServiceUIFactory();

	public abstract Class[] getSupportedAttributeCategories();

	public abstract Object getSupportedAttributeValues(Class arg0, DocFlavor arg1, AttributeSet arg2);

	public abstract DocFlavor[] getSupportedDocFlavors();

	public abstract AttributeSet getUnsupportedAttributes(DocFlavor arg0, AttributeSet arg1);

	public abstract boolean isAttributeCategorySupported(Class arg0);

	public abstract boolean isAttributeValueSupported(Attribute arg0, DocFlavor arg1, AttributeSet arg2);

	public abstract boolean isDocFlavorSupported(DocFlavor arg0);

	public abstract void removePrintServiceAttributeListener(PrintServiceAttributeListener arg0);
	
	/**
	 * If this printer is the preferred one
	 * 
	 * @return true if this printer is preferred
	 */
	
	public boolean isPreferred()
	{
		return isPreferred;
	}
	
	/**
	 * sets the preferred-status of this printer
	 * 
	 * @param pPreferred wether this printer should be the preferred one
	 * @return true if the value changed
	 */
	
	public boolean setPreferred(boolean pPreferred)
	{
		boolean valueChanged = pPreferred != isPreferred;
		isPreferred = pPreferred;
		
		return valueChanged;
	}
	
	public String toString()
	{
		return getName();
	}
}
