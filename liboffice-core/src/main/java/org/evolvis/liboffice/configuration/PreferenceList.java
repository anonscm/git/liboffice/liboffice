/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 
 * <p>There can be only one! (..preferred object)</p>
 * 
 * <p>This implementation of a {@link java.util.List} ensures that only one object in the list can be the preferred one. All objects in the list therefore must implement the {@link Preferrable}-Interface</p>
 * 
 * 
 * TODO cache preferred object for performance !?
 * TODO check if newly added objects are preferred 
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class PreferenceList extends ArrayList
{
	/**
	 * Return-value if given object is not in list or is null
	 */
	public final static int RETURN_OBJECT_NOT_CONTAINED = -2;
	
	/**
	 * Return-value if given object was not preferred and is now preferred
	 */
	public final static int RETURN_PREF_CHANGED = 0;
	
	/**
	 * Return-value if given object was already preferred before
	 */
	public final static int RETURN_PREF_UNCHANGED = -1;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9201368363113170277L;
	
	/**
	 * Constructs an empty list with an initial capacity of ten.
	 *
	 */
	public PreferenceList()
	{
		super();
	}
	
	/**
	 * Constructs an empty list with the specified initial capacity.
	 * 
	 * @param pInitSize the initial capacity of the list.
	 */
	public PreferenceList(int pInitSize)
	{
		super(pInitSize);
	}
	
	/**
	 * Inserts the specified element at the specified position in this list. Shifts the element currently at that position (if any) and any subsequent elements to the right (adds one to their indices).
	 * 
	 * @param pIndex index at which the specified element is to be inserted.
	 * @param pObject element to be inserted. Must implement the {@link Preferrable}-Interface. Otherwise throws {@link java.lang.RuntimeException}
	 * 
	 * @throws RuntimeException if given object does not implement the {@link Preferrable}-Interface
	 */
	
	public void add(int pIndex, Object pObject)
	{
		if(pObject instanceof Preferrable)
			super.add(pIndex, pObject);
		else
			throw new RuntimeException("Passed object does not implement the Preferrable-Interface");
	}
	
	/**
	 * Appends the specified element to the end of this list.
	 * 
	 * @param pObject element to be appended to this list. Must implement the {@link Preferrable}-Interface. Otherwise throws {@link java.lang.RuntimeException} 
	 * @return true (as per the general contract of Collection.add).
	 */
	
	public boolean add(Object pObject)
	{
		if(pObject instanceof Preferrable)
			return super.add(pObject);
		else
			throw new RuntimeException("Passed object does not implement the Preferrable-Interface");
	}
	
	/**
	 * Appends all of the elements in the specified Collection to the end of this list, in the order that they are returned by the specified Collection's Iterator. The behavior of this operation is undefined if the specified Collection is modified while the operation is in progress. (This implies that the behavior of this call is undefined if the specified Collection is this list, and this list is nonempty.)
	 *  
	 * @param pCollection the elements to be inserted into this list. Must be an instance of {@link PreferenceList}
	 * @return true if this list changed as a result of the call.
	 * @throws NullPointerException if the specified collection is null.
	 * @throws RuntimeException if given Collection is no instance of {@link PreferenceList}
	 */
	
	public boolean addAll(Collection pCollection)
	{
		if(pCollection instanceof PreferenceList)
			return super.addAll(pCollection);
		else
			throw new RuntimeException("Passed object is not a PreferenceList-Object");
	}
	
	/**
	 * Inserts all of the elements in the specified Collection into this list, starting at the specified position. Shifts the element currently at that position (if any) and any subsequent elements to the right (increases their indices). The new elements will appear in the list in the order that they are returned by the specified Collection's iterator.
	 * 
	 * @param pIndex index at which to insert first element from the specified collection.
	 * @param pCollection elements to be inserted into this list. Must be an instance of {@link PreferenceList}
	 * @return true if this list changed as a result of the call.
	 * @throws IndexOutOfBoundsException if index out of range (index < 0 || index > size()).
	 * @throws NullPointerException if the specified Collection is null.
	 * @throws RuntimeException if given Collection is no instance of {@link PreferenceList} 
	 */
	
	public boolean addAll(int pIndex, Collection pCollection)
	{
		if(pCollection instanceof PreferenceList)
			return super.addAll(pIndex, pCollection);
		else
			throw new RuntimeException("Passed object is not a PreferenceList-Object");
	}
	
	/**
	 * Returns the index of the preferred object in this list
	 * 
	 * @return the index of the preferred object in this list; if none object previously defined as preferred the first index (0) is returned; if size of list is zero, -1 is returned
	 */

	public int getPreferredIndex()
	{
		return indexOf(getPreferredObject());
	}
	
	/**
	 * Returns the preferred Object of this list
	 * 
	 * @return the preferred Object of this list; if none object previously defined as preferred, the first object in the list is returned; If the size of the list is zero, null is returned
	 */

	public Preferrable getPreferredObject()
	{
		Iterator it = iterator();
		if(size() > 0)
		{
			while(it.hasNext())
			{
				Preferrable pref = (Preferrable)it.next();
				if(pref.isPreferred()) return pref;
			}
			
			// If none in list is preferred, set first in list preferred
			
			((Preferrable)get(0)).setPreferred(true);
			return (Preferrable) get(0);			
		}
		else return null;
	}
	
	/**
	 * Replaces the element at the specified position in this list with the specified element.
	 * 
	 * @param pIndex index of element to replace.
	 * @param pObject element to be stored at the specified position. Must implement the {@link Preferrable}-Interface. Otherwise throws {@link java.lang.RuntimeException} 
	 * @return the element previously at the specified position.
	 * @throws IndexOutOfBoundsException if index out of range (index < 0 || index >= size()).
	 * @throws RuntimeException if given object does not implement the {@link Preferrable}-Interface
	 */

	public Object set(int pIndex, Object pObject)
	{
		if(pObject instanceof Preferrable)
			return super.set(pIndex, pObject);
		else
			throw new RuntimeException("Passed object does not implement the Preferrable-Interface");
	}
	
	/**
	 * Set the the object at the given index as preferred
	 * @param pIndex the index of the object to set as preferred
	 * @return one of the following int-values:<br />RETURN_PREF_CHANGED if preferred object changed in list, RETURN_PREF_UNCHANGED if not
	 * @throws IndexOutOfBoundsException if index out of range (index < 0 || index >= size()).
	 */

	public int setPreferredIndex(int pIndex)
	{
		if(pIndex >= size()) throw new IndexOutOfBoundsException();
		
		boolean valueChanged = false; 
		
		for(int i=0; i < size(); i++)
		{
			Preferrable pref = (Preferrable)get(i);
			if(i == pIndex)
			{
				valueChanged = !pref.isPreferred();
				pref.setPreferred(true);
			}
			else pref.setPreferred(false);
		}
		
		if(valueChanged) return RETURN_PREF_CHANGED;
		return RETURN_PREF_UNCHANGED;
	}

	/**
	 * sets the preferred object of the list
	 * 
	 * @param pPreferrable the preferred Object
	 * @return one of the following int-values:<br />RETURN_OBJECT_NOT_CONTAINED if object does not exist in list or is null<br />RETURN_PREF_CHANGED if preferred object changed in list, RETURN_PREF_UNCHANGED if not 
	 */

	public int setPreferredObject(Preferrable pPreferrable)
	{
		if(!contains(pPreferrable)) return RETURN_OBJECT_NOT_CONTAINED;
		
		return setPreferredIndex(indexOf(pPreferrable));
	}
}