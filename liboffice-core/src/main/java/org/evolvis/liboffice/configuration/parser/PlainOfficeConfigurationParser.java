/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.configuration.parser;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.evolvis.liboffice.configuration.office.ConfigurationAction;
import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethodParameterDescription;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethodParameterDescriptionValuePair;
import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.configuration.office.OfficeTypeGroup;
import org.evolvis.liboffice.officefileformats.FileFormatSupport;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.utils.SystemInfo;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * This class reads and saves the office-configuration-file and reads the office-type descriptions
 * 
 * TODO make a dynamic version
 * 
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 * @see AdvancedOfficeConfigurationParser
 */
public class PlainOfficeConfigurationParser
{
	private final static String configFilePath;
	
	static {
		if(SystemInfo.isWindowsSystem())
			configFilePath = System.getProperty("user.home") + File.separator + "tarent" + File.separator + "office_conf.xml";
		else
			configFilePath = System.getProperty("user.home") + File.separator + ".tarent" + File.separator + "office_conf.xml";
	}
	
	//private final static String officeTypesFilePath = System.getProperty("user.home") + File.separator + "tarent" + File.separator + "office_types.xml";
	private final static String officeTypesFilePath = "/conf/office_types.xml";
	//private final static String commonOOoInstallationDirectoriesFilePath = System.getProperty("user.home") + File.separator + "tarent" + File.separator + "ooo_installation_directories.xml";
	private final static String commonOOoInstallationDirectoriesFilePath = "/conf/ooo_installation_directories.xml";
	private static PlainOfficeConfigurationParser instance;
	private List offices;
	private List officeTypes;
	private List officeTypeGroups;
	
	private static Logger logger = Logger.getLogger(PlainOfficeConfigurationParser.class.getName());
	
	private PlainOfficeConfigurationParser()
	{
		// Do not instatiate. Use getInstance()-Method instead!
	}
	
	public static PlainOfficeConfigurationParser getInstance()
	{
		if(instance == null) instance = new PlainOfficeConfigurationParser();
		return instance;
	}
	
	public List getOfficeConfigurations()
	{
		if(offices == null) offices = parseConfig();
		return offices;
	}
	
	public List getOfficeTypes()
	{
		if(officeTypes == null) officeTypes = parseOfficeTypes();
		return officeTypes;
	}
	
	public List getOfficeTypeGroups()
	{
		if(officeTypeGroups == null) officeTypeGroups = parseOfficeTypeGroups();
		return officeTypeGroups;
	}
	
	public void saveConfig(List pConfigurations)
	{
		createConfig(pConfigurations);
		offices = pConfigurations;
	}
	
	
	// Java 1.5: public List<OfficeConfiguration> parseConfig()
	private List parseConfig()
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try
		{
			builder = factory.newDocumentBuilder();
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		Document document = null;
		try
		{
			document = builder.parse(new File(configFilePath));
		}
		catch(FileNotFoundException pExcp)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("Did not find configuration-file "+configFilePath);
			return null;
		}
		catch(IOException pExcp)
		{
			System.err.println("IO-Error");
			pExcp.printStackTrace();
			return null;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		
		NodeList officeSystemList = document.getElementsByTagName("office-system");
		// Java 1.5: List<OfficeConfiguration> officeSystems = new ArrayList<OfficeConfiguration>();
		List officeSystems = new ArrayList();
		
		for(int z=0; z < officeSystemList.getLength(); z++)
		{
			NodeList ndList = document.getElementsByTagName("office-system").item(z).getChildNodes();
			OfficeConfiguration conf = new OfficeConfiguration();
			
			Map connectionParams = new HashMap();
			
			for(int f=0; f < ndList.getLength(); f++)
			{
				Node thisNode = ndList.item(f);
				
				if("param".equals(thisNode.getNodeName()))
				{
					NamedNodeMap map = thisNode.getAttributes();
					if(map != null)
					{
						Node nameNode = map.getNamedItem("name");
						if(nameNode != null)
						{
							if("office-type".equals(nameNode.getNodeValue()))
							{
								String officeType = map.getNamedItem("value").getNodeValue();
								String shortName = officeType.substring(0, officeType.indexOf("-"));
								String versionName = officeType.substring(officeType.indexOf("-")+1);
								conf.setType(getOfficeType(shortName, versionName));
							}
							else if("path".equals(nameNode.getNodeValue()))
							{
								conf.setPath(map.getNamedItem("value").getNodeValue());
							}
							else if("checksum".equals(nameNode.getNodeValue()))
							{
								conf.setChecksum(map.getNamedItem("value").getNodeValue());
							}
							else if("connection-method".equals(nameNode.getNodeValue()))
							{							
								try
								{
									String connMethodClass = map.getNamedItem("value").getNodeValue();
									
									Object connMethod = Class.forName(connMethodClass).newInstance();
									if(connMethod instanceof OfficeConnectionMethod)
									{
										conf.setConnectionMethod((OfficeConnectionMethod)connMethod);
									}
								}
								catch(Exception pExcp)
								{
									if(logger.isLoggable(Level.FINE)) logger.fine(pExcp.getMessage());
									//conf.setConnectionMethod(OfficeType.DEFAULT_CONN_METHOD);
								}
							}
							else if("preferred".equals(nameNode.getNodeValue()))
							{
								// Java 1.5: conf.setPreferred(Boolean.parseBoolean(map.getNamedItem("value").getNodeValue()));
								if("true".equals(map.getNamedItem("value").getNodeValue())) conf.setPreferred(true);
								else conf.setPreferred(false);
							}
							else
							{
								// maybe individual connection-param
								String paramName = nameNode.getNodeValue();
								
								Iterator it = conf.getConnectionMethod().getAdditionalParameters().iterator();
								
								while(it.hasNext())
								{
									OfficeConnectionMethodParameterDescription desc = (OfficeConnectionMethodParameterDescription)it.next();
									if(desc.getID().equals(paramName))
									{
										Object value = null;
										if(desc.getType().equals(String.class))
											value = map.getNamedItem("value").getNodeValue();
										else if(desc.getType().equals(Integer.class))
										{
											try
											{
												value = Integer.decode(map.getNamedItem("value").getNodeValue());
											}
											catch(NumberFormatException pExcp)
											{
												logger.warning(paramName + "is not a valid Integer");
											}
										}
										connectionParams.put(paramName, new OfficeConnectionMethodParameterDescriptionValuePair(desc, value));
										break;
									}
								}
							}
						}
					}
					
				}
			}
			conf.setConnectionParameters(connectionParams);
			officeSystems.add(conf);
		}
		
		return officeSystems;
	}
	
	
	// Java 1.5: public List<OfficeType> getOfficeTypes()
	private List parseOfficeTypes()
	{
		// Java 1.5: List<OfficeType> officeTypes = new ArrayList<OfficeType>();
		List officeTypes = new ArrayList();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		
		try
		{
			builder = factory.newDocumentBuilder();
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		Document document = null;
		try
		{
			document = builder.parse(getClass().getResourceAsStream(officeTypesFilePath));
		}
		catch(IOException pExcp)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("File not found: "+officeTypesFilePath);
			return null;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		
		NodeList officeTypeClassList = document.getElementsByTagName("office-type-group");
		
		for(int q=0; q < officeTypeClassList.getLength(); q++)
		{
			OfficeTypeGroup officeTypeGroup = parseOfficeType(officeTypeClassList.item(q), null);
			
			NodeList officeTypesList = ((Element)officeTypeClassList.item(q)).getElementsByTagName("office-type");
			
			for(int f=0; f < officeTypesList.getLength(); f++)
			{
				// Make a clone first
				OfficeType type = (OfficeType)officeTypeGroup.clone();
				
				// Then overwrite parameters
				officeTypes.add(parseOfficeType(officeTypesList.item(f), type));
			}
		}
		
		return officeTypes;
	}
	
	// Java 1.5: public List<OfficeTypeGroup> parseOfficeTypeGroups()
	private List parseOfficeTypeGroups()
	{
		// Java 1.5: List<OfficeTypeGroup> officeTypeGroups = new ArrayList<OfficeTypeGroup>();
		List officeTypeGroups = new ArrayList();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		
		try
		{
			builder = factory.newDocumentBuilder();
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		Document document = null;
		try
		{
			document = builder.parse(getClass().getResourceAsStream(officeTypesFilePath));
		}
		catch(IOException pExcp)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("File not found: "+officeTypesFilePath);
			return null;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		
		NodeList officeTypeClassList = document.getElementsByTagName("office-type-group");
		
		for(int q=0; q < officeTypeClassList.getLength(); q++)
		{
			OfficeTypeGroup officeTypeGroup = parseOfficeType(officeTypeClassList.item(q), null);
			officeTypeGroups.add(officeTypeGroup);
		}
		
		return officeTypeGroups;
	}
	
	public OfficeType getOfficeType(String pOfficeTypeShortName, String pOfficeTypeVersionName)
	{
		// Java 1.5: List<OfficeType> officeTypes = getOfficeTypes();
		List officeTypes = getOfficeTypes();
		
		for(int i=0; i < officeTypes.size(); i++)
		{
			// Java 1.5: OfficeType type = officeTypes.get(i);
			OfficeType type = (OfficeType) officeTypes.get(i);
			if(type.getShortName().equals(pOfficeTypeShortName) && type.getVersionName().equals(pOfficeTypeVersionName))
			{
				return type;
			}
		}
		return null;
	}
	
	public OfficeType getOfficeTypeByName(String pOfficeTypeName, String pOfficeTypeVersionName)
	{
		// Java 1.5: List<OfficeType> officeTypes = getOfficeTypes();
		List officeTypes = getOfficeTypes();
		for(int i=0; i < officeTypes.size(); i++)
		{
			// Java 1.5: OfficeType type = officeTypes.get(i);
			OfficeType type = (OfficeType) officeTypes.get(i);
			if(type.getName().equals(pOfficeTypeName) && type.getVersionName().equals(pOfficeTypeVersionName))
			{
				return type;
			}
		}
		return null;
	}

	private OfficeType parseOfficeType(Node pNode, OfficeType pType)
	{
		if(pType == null) pType = new OfficeType();
		NodeList officeTypeList = pNode.getChildNodes();
		
		for(int z=0; z < officeTypeList.getLength(); z++)
		{
			Node thisNode = officeTypeList.item(z);
			
			if("param".equals(thisNode.getNodeName()))
			{
				NamedNodeMap map = thisNode.getAttributes();
				if(map != null)
				{
					Node nameNode = map.getNamedItem("name");
					if(nameNode != null)
					{
						if("short-name".equals(nameNode.getNodeValue()))
						{
							pType.setShortName(map.getNamedItem("value").getNodeValue());
						}
						else if("name".equals(nameNode.getNodeValue()))
						{
							pType.setName(map.getNamedItem("value").getNodeValue());
						}
						else if("version".equals(nameNode.getNodeValue()))
						{
							pType.setVersionName(map.getNamedItem("value").getNodeValue());
						}
						else if("display-version".equals(nameNode.getNodeValue()))
						{
							pType.setDisplayVersion(map.getNamedItem("value").getNodeValue());
						}
						else if("exec".equals(nameNode.getNodeValue()))
						{
							pType.setExec(map.getNamedItem("value").getNodeValue());
						}
						else if("exec-options".equals(nameNode.getNodeValue()))
						{
							NodeList childNodes = thisNode.getChildNodes();
							String[] options = new String[(childNodes.getLength()-1)/2];
							for(int q=0; q < childNodes.getLength(); q++)
							{
								Node childNode = childNodes.item(q);
								if("value".equals(childNode.getNodeName()))
								{
									options[(q-1)/2] = childNode.getFirstChild().getNodeValue();
								}
							}
							pType.setExecOptions(options);
						}
						else if("classpath-entries".equals(nameNode.getNodeValue()))
						{
							NodeList childNodes = thisNode.getChildNodes();
							String[] classPathEntries = new String[childNodes.getLength()-2];
							for(int q=0; q < childNodes.getLength(); q++)
							{
								Node childNode = childNodes.item(q);
								if("value".equals(childNode.getNodeName()))
								{
									classPathEntries[q-1] = childNode.getFirstChild().getNodeValue();
								}
							}
							pType.setClassPathEntries(classPathEntries);
						}
						else if("library-path-entries".equals(nameNode.getNodeValue()))
						{
							NodeList childNodes = thisNode.getChildNodes();
							String[] libraryPathEntries = new String[childNodes.getLength()-2];
							for(int q=0; q < childNodes.getLength(); q++)
							{
								Node childNode = childNodes.item(q);
								if("value".equals(childNode.getNodeName()))
								{
									libraryPathEntries[q-1] = childNode.getFirstChild().getNodeValue();
								}
							}
							pType.setLibraryPathEntries(libraryPathEntries);
						}
						else if("supported-connection-methods".equals(nameNode.getNodeValue()))
						{
							NodeList childNodes = thisNode.getChildNodes();
							List connectionMethods = new ArrayList();
							for(int q=0; q < childNodes.getLength(); q++)
							{
								Node childNode = childNodes.item(q);
								if("value".equals(childNode.getNodeName()))
								{
									try
									{
										connectionMethods.add(Class.forName(childNode.getFirstChild().getNodeValue()).newInstance());
									}
									catch(Exception pExcp)
									{
										pExcp.printStackTrace();
									}
								}
							}
							
							
							pType.setSupportedConnectionMethods(connectionMethods);
							
						}
						else if("main-office-factory-class".equals(nameNode.getNodeValue()))
						{
							pType.setMainOfficeFactoryClassName(map.getNamedItem("value").getNodeValue());
						}
						else if("file-filter-class".equals(nameNode.getNodeValue()))
						{
							pType.setFileFilterClassName(map.getNamedItem("value").getNodeValue());
						}
						else if("installation-finder-class".equals(nameNode.getNodeValue()))
						{
							pType.setInstallationFinderClassName(map.getNamedItem("value").getNodeValue());
						}
						else if("before-connect-actions".equals(nameNode.getNodeValue()))
						{
							NodeList childNodes = thisNode.getChildNodes();
							List beforeConnectActions = new ArrayList();
							for(int q=0; q < childNodes.getLength(); q++)
							{
								Node childNode = childNodes.item(q);
								if("param".equals(childNode.getNodeName()))
								{
									NodeList childNodes2 = childNode.getChildNodes();
									for(int r=0; r < childNodes2.getLength(); r++)
									{
										Node childNode2 = childNodes2.item(r);
										if("param".equals(childNode2.getNodeName()))
										{
											NamedNodeMap map2 = childNode2.getAttributes();
											if("configuration-action-class".equals(map2.getNamedItem("name").getNodeValue()))
											{
												try
												{
													beforeConnectActions.add((ConfigurationAction)Class.forName(map2.getNamedItem("value").getNodeValue()).newInstance());
												} catch (DOMException e)
												{
													e.printStackTrace();
												} catch (InstantiationException e)
												{
													e.printStackTrace();
												} catch (IllegalAccessException e)
												{
													e.printStackTrace();
												} catch (ClassNotFoundException e)
												{
													e.printStackTrace();
												}
											}
										}
									}
								}
							}
							pType.setBeforeConnectActions(beforeConnectActions);
						}
						else if("before-office-start-actions".equals(nameNode.getNodeValue()))
						{
							NodeList childNodes = thisNode.getChildNodes();
							List beforeOfficeStartActions = new ArrayList();
							for(int q=0; q < childNodes.getLength(); q++)
							{
								Node childNode = childNodes.item(q);
								if("param".equals(childNode.getNodeName()))
								{
									NodeList childNodes2 = childNode.getChildNodes();
									for(int r=0; r < childNodes2.getLength(); r++)
									{
										Node childNode2 = childNodes2.item(r);
										if("param".equals(childNode2.getNodeName()))
										{
											NamedNodeMap map2 = childNode2.getAttributes();
											if("configuration-action-class".equals(map2.getNamedItem("name").getNodeValue()))
											{
												try
												{
													beforeOfficeStartActions.add((ConfigurationAction)Class.forName(map2.getNamedItem("value").getNodeValue()).newInstance());
												}
												catch(Exception pExcp)
												{
													// TODO
												}
											}
										}
									}
								}
							}
							pType.setBeforeOfficeStartActions(beforeOfficeStartActions);
						}
						else if("supported-file-types".equals(nameNode.getNodeValue()))
						{
							NodeList childNodes = thisNode.getChildNodes();
							List supportedFileFormats = new ArrayList();
							for(int q=0; q < childNodes.getLength(); q++)
							{
								Node childNode = childNodes.item(q);
								if("param".equals(childNode.getNodeName()))
								{
									FileFormatSupport fileFormatSupport = new FileFormatSupport();
									NodeList childNodes2 = childNode.getChildNodes();
									for(int r=0; r < childNodes2.getLength(); r++)
									{
										Node childNode2 = childNodes2.item(r);
										if("param".equals(childNode2.getNodeName()))
										{
											NamedNodeMap map2 = childNode2.getAttributes();
											if("file-format-class".equals(map2.getNamedItem("name").getNodeValue()))
											{
													try
													{
														fileFormatSupport.setOfficeFileFormat((OfficeFileFormat)Class.forName(map2.getNamedItem("value").getNodeValue()).newInstance());
													} catch (DOMException e)
													{
														e.printStackTrace();
													} catch (InstantiationException e)
													{
														e.printStackTrace();
													} catch (IllegalAccessException e)
													{
														e.printStackTrace();
													} catch (ClassNotFoundException e)
													{
														e.printStackTrace();
													}
											}
											else if("readable".equals(map2.getNamedItem("name").getNodeValue()))
											{
												// Java 1.5: fileFormatSupport.setCanRead(Boolean.parseBoolean(map2.getNamedItem("value").getNodeValue()));
												if("true".equals(map2.getNamedItem("value").getNodeValue())) fileFormatSupport.setCanRead(true);
												else fileFormatSupport.setCanRead(false);
											}
											else if("writable".equals(map2.getNamedItem("name").getNodeValue()))
											{
												// Java 1.5: fileFormatSupport.setCanWrite(Boolean.parseBoolean(map2.getNamedItem("value").getNodeValue()));
												if("true".equals(map2.getNamedItem("value").getNodeValue())) fileFormatSupport.setCanWrite(true);
												else fileFormatSupport.setCanWrite(false);
											}
											
										}
									}
									supportedFileFormats.add(fileFormatSupport);
								}
							}
							pType.setSupportedOfficeFileFormats(supportedFileFormats);
						}
					}
				}
			}
		}
		return pType;
	}
	
	
	// Java 1.5: public void createConfig(List<OfficeConfiguration> pOffices)
	private void createConfig(List pOffices)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try
		{
			builder = factory.newDocumentBuilder();
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
		}
		
		Document document = builder.newDocument();
		
		Node officeConfigurationNode = document.createElement("office-configuration");
		
		// Java 1.5: Iterator<OfficeConfiguration> it = pOffices.iterator();
		Iterator it = pOffices.iterator();
		
		while(it.hasNext())
		{
			// Java 1.5: OfficeConfiguration conf = it.next();
			OfficeConfiguration conf = (OfficeConfiguration) it.next();
			
			Node officeSystemNode = document.createElement("office-system");
			Node typeNode = document.createElement("param");
			((Element)typeNode).setAttribute("name", "office-type");
			((Element)typeNode).setAttribute("value", conf.getType().getShortName()+"-"+conf.getType().getVersionName());
			Node pathNode = document.createElement("param");
			((Element)pathNode).setAttribute("name", "path");
			((Element)pathNode).setAttribute("value", conf.getPath());
			Node checksumNode = document.createElement("param");
			((Element)checksumNode).setAttribute("name", "checksum");
			((Element)checksumNode).setAttribute("value", conf.getChecksum());
			Node connTypeNode = document.createElement("param");
			((Element)connTypeNode).setAttribute("name", "connection-method");
			((Element)connTypeNode).setAttribute("value", conf.getConnectionMethod().getClass().getName());
			
			Node prefNode = document.createElement("param");
			((Element)prefNode).setAttribute("name", "preferred");
			((Element)prefNode).setAttribute("value", String.valueOf(conf.isPreferred()));

			officeSystemNode.appendChild(typeNode);
			officeSystemNode.appendChild(pathNode);
			officeSystemNode.appendChild(checksumNode);
			officeSystemNode.appendChild(connTypeNode);
			officeSystemNode.appendChild(prefNode);
			
			// write connection-params
			
			Iterator ite = conf.getConnectionParameters().values().iterator();
			
			while(ite.hasNext())
			{
				OfficeConnectionMethodParameterDescriptionValuePair descValuePair = (OfficeConnectionMethodParameterDescriptionValuePair)ite.next();
				Node paramNode = document.createElement("param");
				((Element)paramNode).setAttribute("name", descValuePair.getDescription().getID());
				if(descValuePair.getValue() != null)
					((Element)paramNode).setAttribute("value", descValuePair.getValue().toString());
				officeSystemNode.appendChild(paramNode);
			}
			
			officeConfigurationNode.appendChild(officeSystemNode);
		}
		
		document.appendChild(officeConfigurationNode);
		// Java 1.5: document.normalizeDocument();
		
		try
		{
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			DOMSource        source = new DOMSource( document );
			
			// Create config-directory if not existing
			File configDir = new File(configFilePath.substring(0, configFilePath.lastIndexOf(File.separator)));
			
			if(!configDir.exists()) configDir.mkdirs();
			
			FileOutputStream os     = new FileOutputStream( new File( configFilePath ) );
			StreamResult     result = new StreamResult( os );
			transformer.transform( source, result );
		}
		catch(FileNotFoundException pExcp)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning(pExcp.getLocalizedMessage());
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
		}
		
	}
	
	public List getCommonOOoInstallationDirectories()
	{
		List directories = new ArrayList();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		
		try
		{
			builder = factory.newDocumentBuilder();
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		Document document = null;
		try
		{
			document = builder.parse(getClass().getResourceAsStream(commonOOoInstallationDirectoriesFilePath));
		}
		catch(IOException pExcp)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("File not found: "+commonOOoInstallationDirectoriesFilePath);
			return null;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return null;
		}
		
		NodeList dirList = document.getElementsByTagName("ooo-installation-directory");
		
		for(int i=0; i < dirList.getLength(); i++)
		{
			Node node = dirList.item(i);
			directories.add(node.getFirstChild().getNodeValue());
		}
		
		return directories;
	}
}