/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.office;

/**
 * <p>
 * nice class name, isn't it ? :D
 * </p>
 * <p>
 * The class-name OfficeConnectionMethodParameterDescriptionValuePair is (like most of my classes) self-explanatory (but long).<br />
 * Nevertheless, I will describe it more in detail: 
 * </p>
 * <p>
 * As you can see in the constructor-documentation, an OfficeCon.. consists of two Objects, a OfficeConnectionMethodParameterDescription ("description") and a Value-Object.<br />
 * The idea is, that one can combine a parameter-value and a description on this value in one Object. For example for putting it into a List or Map.
 * </p>
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeConnectionMethodParameterDescriptionValuePair
{
	private OfficeConnectionMethodParameterDescription desc;
	private Object value;
	
	/**
	 * Constructs a OfficeConnectionMethodParameterDescriptionValuePair-Object, combining a parameter-description and a fitting value.
	 * 
	 * @param pDesc - the description for the parameter
	 * @param pValue - the value of the parameter
	 */
	
	public OfficeConnectionMethodParameterDescriptionValuePair(OfficeConnectionMethodParameterDescription pDesc, Object pValue)
	{
		desc = pDesc;
		value = pValue;
	}
	
	public OfficeConnectionMethodParameterDescription getDescription()
	{
		return desc;
	}
	
	public Object getValue()
	{
		return value;
	}
}
