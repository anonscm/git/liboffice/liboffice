/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.printer;

import java.util.List;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.evolvis.liboffice.configuration.AbstractConfigurationLoader;
import org.evolvis.liboffice.configuration.AbstractConfigurationSaver;
import org.evolvis.liboffice.configuration.PreferenceList;
import org.evolvis.liboffice.configuration.office.OfficeConfigurator;
import org.evolvis.liboffice.configuration.office.OfficeType;


/**
 * Manages printer-devices<br />
 * <br />
 * This class is a central place for handling all kinds of printers.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class PrinterConfigurator
{
	private static PrinterConfigurator instance;
	private PreferenceList printers;
	
	private PrinterConfigurator()
	{
		printers = new PreferenceList();
	}
	
	/**
	 * Returns an instance of this class
	 * @return an instance of this class
	 */
	
	public static PrinterConfigurator getInstance()
	{
		if(instance == null) instance = new PrinterConfigurator();
		return instance;
	}
	
	public int setup()
	{
		removeAllPrinters();
		addOOoPrintersToList(printers);
		addSystemPrintersToList(printers);
		
		setSystemStandardPrinterAsPreferred();
		
		return 0;
	}
	
	/**
	 * Removes all previously configured printers
	 * @return if the list was already empty before
	 */
	
	private boolean removeAllPrinters()
	{
		boolean wasAlreadyEmpty = printers.isEmpty();
		printers.clear();
		return wasAlreadyEmpty;
	}
	
	/**
	 * Searches for printers configured in an OpenOffice.org-compatible Office-Suite (on UNIX-Systems only and only if an OOo-compatible Office-Suite is configured as preferred one in {@link org.evolvis.liboffice.configuration.office.OfficeConfigurator}) and adds them to given list
	 * 
	 * @param pList the list the found printers should be added to
	 * @return true if any printers were found
	 */
	
	private boolean addOOoPrintersToList(List pList)
	{
		boolean found = false;
		
		// If the preferred office-system is an openoffice-compatible, search for OOo-Printers
		if(OfficeConfigurator.getInstance().getPreferredOffice() != null && OfficeConfigurator.getInstance().getPreferredOffice().getType().isCompatibleWith(new OfficeType("openoffice")))
		{
			// TODO fixme!
//			pList.addAll(OOoPrinterLookup.lookupAllPrintersAsList());
			found = true;
		}
		return found;
	}
	
	/**
	 * Searches for system-printers by Java-Printer-API and adds them to the given list
	 * 
	 * @param pList the list the found printers should be added to
	 * @return true if any printers were found
	 */
	
	private boolean addSystemPrintersToList(List pList)
	{
		boolean found = false;
		
		PrintService[] systemPrintServices = PrintServiceLookup.lookupPrintServices(null, null);
		
		for(int i=0; i < systemPrintServices.length; i++)
		{
			pList.add(new PrintServiceAbstractPrinterWrapper(systemPrintServices[i]));
			found = true;
		}
		
		return found;
	}
	
	/**
	 * Sets the system-standard-printer got by Java-Print-API to be the preferred one
	 * @return TODO
	 */
	
	public int setSystemStandardPrinterAsPreferred()
	{
		return setPreferredPrinter(new PrintServiceAbstractPrinterWrapper(PrintServiceLookup.lookupDefaultPrintService()));
	}
	
	/**
	 * Returns all available printers
	 * 
	 * @return a {@link PreferenceList} containing all configured printers as {@link AbstractPrinter}-Objects
	 */
	
	public PreferenceList getAvailablePrinters()
	{
		return printers;
	}
	
	/**
	 * Returns the names of all available printers
	 * 
	 * @return a String-Array containing the names of all available printers
	 */
	
	public String[] getAvailablePrinterNamesAsArray()
	{
		//if(printServices == null) return null;
		String[] printerNames = new String[printers.size()];
		
		for(int i=0; i < printers.size(); i++)
		{
			// Java 1.5: printerNames[i] = printServices.get(i).getName();
			printerNames[i] = ((AbstractPrinter) printers.get(i)).getName();
		}
		return printerNames;
	}
	
	/**
	 * returns the preferred printer
	 * 
	 * @return an {@link AbstractPrinter}-Object representing the preferred printer
	 */
	
	public AbstractPrinter getPreferredPrinter()
	{
		return (AbstractPrinter) printers.getPreferredObject();
	}
	
	/**
	 * returns the index of the preferred printer in the list
	 * 
	 * @return the index of the preferred printer; -1 if no printers configured
	 */
	
	public int getPreferredPrinterIndex()
	{
		return printers.indexOf(getPreferredPrinter());
	}
	
	/**
	 * Sets the preferred printer
	 * 
	 * @param pPrinter an {@link AbstractPrinter}-Object that should be the preferred printer. Have to be added before
	 * @return one of the following int-values:<br />PreferenceList.RETURN_OBJECT_NOT_CONTAINED if the printer is not contained in list<br />PreferenceList.RETURN_PREF_UNCHANGED if the printer was already preferred before, PreferenceList.RETURN_PREF_CHANGED if not
	 */
	
	public int setPreferredPrinter(AbstractPrinter pPrinter)
	{
		return printers.setPreferredObject(pPrinter);
	}
	
	
	public int saveConfiguration(AbstractConfigurationSaver pConfSaver)
	{
		return 0;
	}
	

	public int loadConfiguration(AbstractConfigurationLoader pConfLoader)
	{
		return 0;
	}
}
