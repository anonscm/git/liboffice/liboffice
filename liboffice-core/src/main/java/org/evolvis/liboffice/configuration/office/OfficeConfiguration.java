/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.configuration.office;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

/**
 * 
 * This class represents an office-configuration with attributs like installation-path, type of office-system, connection-type
 * and whether this system should be the preferred one to use for office-integration
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 * 
 *
 */
public class OfficeConfiguration
{		
	protected String path;
	protected OfficeType type;
	protected boolean preferred;
	protected String checksum;
	protected OfficeConnectionMethod connMethod;
	protected Map connectionParameters;
	
	private static final Logger logger = Logger.getLogger(OfficeConfiguration.class.getName());
	
	
	/**
	 * <p>Constructs a new OfficeConfiguration-object.</p>
	 *
	 * <p>Same as OfficeConfiguration(null, null, null, null, null, false)</p>
	 */

	public OfficeConfiguration()
	{
		this(null, null, null, null, null, false);
	}
	
	/**
	 * <p>Constructs a new OfficeConfiguration-object.</p>
	 * 
	 * @param pPath the path where the office is installed
	 * @param pType the type of the office
	 * @param pChecksum the checksum of the offices binary
	 * @param pConnMethod the preferred connection method for connecting to the office 
	 * @param pConnMethodParams a map of connection-parameters, containing OfficeConnectionParameterDescriptionValuePairs as values and the corresponding parameter's ID as keys
	 * @param pPreferred if the office is the preferred one
	 */

	public OfficeConfiguration(String pPath, OfficeType pType, String pChecksum, OfficeConnectionMethod pConnMethod, Map pConnMethodParams, boolean pPreferred)
	{
		path = pPath;
		type = pType;
		checksum = pChecksum;
		connMethod = pConnMethod;
		preferred = pPreferred;
		connectionParameters = pConnMethodParams;
	}
	
	/**
	 * Returns the installation-path of the office-installation
	 * 
	 * @return the installation-path of the office-installation
	 */
	
	public String getPath()
	{
		return path;
	}
	
	/**
	 * Returns the type of the office-installation
	 * 
	 * @return the type of the office-installation
	 */

	public OfficeType getType()
	{
		return type;
	}
	
	/**
	 * Returns the preferred connection-method for this office-installation
	 *  
	 * @return the preferred connection-method for htis office-installation
	 */

	public OfficeConnectionMethod getConnectionMethod()
	{
		if(connMethod == null && getType() != null) connMethod = getType().getDefaultConnectionMethod();
		else if(connMethod == null) connMethod = OfficeConnectionMethod.DEFAULT;
		return connMethod;
	}
	
	/**
	 * Returns a Map containing connection-parameters for this office-installation
	 * 
	 * @return a Map containing connection-parameters with OfficeConnectionParameterDescriptionValuePair-objects as values and the corresponding parameter-IDs as keys
	 */

	public Map getConnectionParameters()
	{
		// if no parameters set yet, init with default values
		if(connectionParameters == null)
		{
			connectionParameters = new HashMap();
			
			Iterator it = getConnectionMethod().getAdditionalParameters().iterator();
			while(it.hasNext())
			{
				OfficeConnectionMethodParameterDescription paramDesc = (OfficeConnectionMethodParameterDescription)it.next();
				connectionParameters.put(paramDesc.getID(), new OfficeConnectionMethodParameterDescriptionValuePair(paramDesc, paramDesc.getDefaultValue()));
			}
		}
		return connectionParameters;
	}
	
	/**
	 * Returns the value of a connection-parameter 
	 * 
	 * @param pID the ID of the connection-parameter
	 * @return the appropriate value for the given parameter or null, if it does not exist
	 */

	public Object getConnectionParameter(String pID)
	{
		OfficeConnectionMethodParameterDescriptionValuePair descValuePair = (OfficeConnectionMethodParameterDescriptionValuePair)getConnectionParameters().get(pID);
		if(descValuePair != null) return descValuePair.getValue();
		return null;
	}
	
	/**
	 * Returns the MD5-checksum of the office's binary
	 * 
	 * @return the MD5-checksum of the office's binary
	 */

	public String getChecksum()
	{
		return checksum;
	}

	/**
	 * Returns if this office-installation is the preferred one
	 * 
	 * @return true if this office-installation is the preferred one
	 */

	public boolean isPreferred()
	{
		return preferred;
	}

	/**
	 * Set the path to office-installation
	 * 
	 * @param pPath the absolute path to the office-installation
	 */
	
	public void setPath(String pPath)
	{
		path = pPath;
	}
	
	/**
	 * Sets the type of this office-installation
	 * 
	 * @param pType the type of the office-installation
	 */

	public void setType(OfficeType pType)
	{
		type = pType;
	}
	
	/**
	 * Sets the preferred connection-method for this office-installation
	 * 
	 * @param pConnMethod the connection-method for this office-installation
	 */

	public void setConnectionMethod(OfficeConnectionMethod pConnMethod)
	{
		connMethod = pConnMethod;
	}

	/**
	 * sets the connection-parameters for this office-installation
	 * 
	 * @param pConnectionParams a Map containing connection-parameters with OfficeConnectionParameterDescriptionValuePair-objects as values and the corresponding parameter-IDs as keys
	 */
	
	public void setConnectionParameters(Map pConnectionParams)
	{
		connectionParameters = pConnectionParams;
	}
	
	/**
	 * Sets a single connection-parameter for this office-installation
	 * 
	 * @param pDesc a OfficeConnectionMethodParameterDescription describing the parameter to be set
	 * @param pValue the value to set the given parameter to
	 */
	
	public void setConnectionParameter(OfficeConnectionMethodParameterDescription pDesc, Object pValue)
	{
		getConnectionParameters().put(pDesc.getID(), new OfficeConnectionMethodParameterDescriptionValuePair(pDesc, pValue));		
	}
	
	/**
	 * Set the MD5-checksum of this office-installation
	 * 
	 * @param pChecksum the MD5-checksum of this office-installation
	 */

	public void setChecksum(String pChecksum)
	{
		if(pChecksum != null)
			checksum = pChecksum.trim();
		else checksum = null;
	}
	
	/**
	 * Sets this office-installation to "preferred" .. or not
	 * 
	 * @param pPreferred whether this office-installation should be preferred
	 */

	public void setPreferred(boolean pPreferred)
	{
		preferred = pPreferred;
	}

//	/**
//	 * 
//	 * Sets up environment variables CLASSPATH and PATH (Windows) / LD_LIBRARY_PATH (Unix)
//	 * 
//	 * You can define entries which consist to add to these variables through office_types.xml
//	 * 
//	 * TODO this does not really belong here
//	 * 
//	 * @return if successful
//	 */
//
//	public boolean setupEnvironmentVariables()
//	{
//		// Add entries to CLASSPATH
//
//		String[] classPathEntries = type.getClassPathEntries();
//
//		if(classPathEntries != null)
//		{
//			for(int i=0; i < classPathEntries.length; i++)
//			{
//				String classPathEntry = getPath()+classPathEntries[i];
//				
//				File classPathEntryFile = new File(classPathEntry);
//				
//				if(classPathEntryFile.exists())
//				{
//					if(classPathEntryFile.isDirectory())
//					{
//						// Is a directory. Add the dir and all jars in this dir to the classpath
//						File[] files = classPathEntryFile.listFiles(new FileFilter() {
//
//							public boolean accept(File pathname)
//							{
//								return pathname.getName().endsWith(".jar");
//							}
//							
//						});
//						
//						for(int f=0; f < files.length; f++)
//						{
//							EnvironmentVariableTool.addEntryToClassPath(files[f].getAbsolutePath());
//						}
//					}
//					else
//					{
//						// Is a file. If it is a jar, add it.
//						if(classPathEntryFile.getName().endsWith(".jar"))
//							EnvironmentVariableTool.addEntryToClassPath(classPathEntryFile.getAbsolutePath());
//					}
//				}
//			}
//		}
//
//		// Add entries to PATH (Windows) or LD_LIBRARY_PATH (Unix)
//
//		String[] libraryPathEntries = type.getLibraryPathEntries();
//
//		if(libraryPathEntries != null)
//		{
//			for(int i=0; i < libraryPathEntries.length; i++)
//			{
//				String libraryPathEntry = this.getPath()+libraryPathEntries[i];
//				
//				File libraryPathEntryFile = new File(libraryPathEntry);
//				if(libraryPathEntryFile.exists())
//				{
//					logger.fine("Adding "+libraryPathEntryFile.getAbsolutePath()+" to library-path");
//					EnvironmentVariableTool.addEntryToLibraryPath(libraryPathEntryFile.getAbsolutePath());
//				}
//			}
//		}
//
//		return true;
//	}

	public String toString()
	{
		return getType().getName() + " " + getType().getDisplayVersion() + " in " + getPath();
	}
}