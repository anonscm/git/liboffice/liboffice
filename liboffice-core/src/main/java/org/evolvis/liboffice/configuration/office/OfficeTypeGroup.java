/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.office;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import org.evolvis.liboffice.filefilter.ExtendableFileFilter;
import org.evolvis.liboffice.officefileformats.FileFormatSupport;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.ui.Messages;
import org.evolvis.liboffice.utils.SystemInfo;

/**
 * 
 * This abstract class represents a group of office-types.
 * Office-Types can be releases of office-systems, for example: OpenOffice.org 2.0, StarOffice 8.0 or MS-Office 2003. Such Office-Types can be grouped so that derivates of Office-Systems (e.g. StarOffice 7.0, StarOffice 8.0, OpenOffice.org 2.0) form an Office-Type-Group.    
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public abstract class OfficeTypeGroup implements Cloneable
{
	protected String version;
	protected int defaultPort, defaultConnectionType, buildMin, buildMax;
	protected String shortName, name, displayVersion, mainOfficeFactoryClassName, installationFinderClassName, exec, fileFilterClassName;
	// Java 1.5: protected List<OfficeType> compatibleOffices;
	protected List compatibleOffices;
	protected String[] execOptions;
	protected List supportedConnectionMethods;
	protected String [] classPathEntries;
	protected String [] libraryPathEntries;
	protected FileFilter fileFilterLoad, fileFilterSave;
	protected List supportedOfficeFileFormats;
	protected List fileFiltersSave, fileFiltersLoad;
	protected List beforeConnectActions;
	protected List beforeOfficeStartActions;
	
	/**
	 * 
	 * <p>Constructs a new OfficeTypeGroup-object</p>
	 * 
	 * <p>Same as OfficeTypeGroup(null)</p>
	 *
	 */
	
	public OfficeTypeGroup()
	{
		this(null);
	}
	
	/**
	 * <p>Constructs a new OfficeTypeGroup-object.</p>
	 * 
	 * <p>Same as OfficeTypeGroup(pShortName, "")</p>
	 * 
	 * @param pShortName
	 */
	
	public OfficeTypeGroup(String pShortName)
	{
		this(pShortName, "" );
	}
	
	/**
	 * <p>Constructs a new OfficeTypeGroup-object.</p>
	 * 
	 * @param pShortName
	 * @param pVersion
	 */
	
	public OfficeTypeGroup(String pShortName, String pVersion)
	{
		compatibleOffices = new ArrayList();
		supportedOfficeFileFormats = new ArrayList();
		beforeConnectActions = new ArrayList();
		beforeOfficeStartActions = new ArrayList();
		shortName = pShortName;
		version = pVersion;
	}
	
	
	/* Getter */
	
	public String getVersion()
	{
		return version; 
	}
	
	public String getVersionName()
	{
		/*
		String versionName = "";
		for(int i=0; i < version.length; i++)
		{
			if(version[i] != -1)
			{
				if(versionName != "") versionName = versionName+"."+version[i];
				else versionName = String.valueOf(version[i]);
			}
		}
		return versionName;
		*/
		return version;
	}
	
	public String getDisplayVersion()
	{
		if(displayVersion == null || displayVersion.length() == 0) return getVersionName();
		return displayVersion;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getShortName()
	{
		return shortName;
	}
	
	public String getExec()
	{
		// Replace / by system-specific file-separator
		
		exec = 	exec.replace('/', File.separatorChar);
		
		// If Windows-Host, add .exe
		if(SystemInfo.isWindowsSystem())
		{
			if(exec.indexOf('.') == -1)
			{
				return exec+".exe";
			}
		}
		return exec;
	}
	
	public String[] getExecOptions()
	{
		if(execOptions != null) return execOptions;
		return new String[] { "" };
	}
	
	public String getOptionsString()
	{
		String optionsString = "";
		if(execOptions != null)
		{
			for(int i=0; i < execOptions.length; i++)
			{
				optionsString = optionsString+" "+execOptions[i];
			}
		}
		return optionsString;
	}

	public String[] getClassPathEntries()
	{
		return classPathEntries;
	}
	
	public String[] getLibraryPathEntries()
	{
		return libraryPathEntries;
	}
	
	// Java 1.5: public List<OfficeType> getCompatibleOffices()
	public List getCompatibleOffices()
	{
		return compatibleOffices;
	}
	
	public Class getMainOfficeFactoryClass()
	{
		try
		{
			Class mainFactoryClass = Class.forName(mainOfficeFactoryClassName);
			Class[] interfaces = mainFactoryClass.getInterfaces();
			
			for(int i=0; i < interfaces.length; i++)
			{
				if(interfaces[i].equals(Class.forName("de.tarent.documents.factory.MainOfficeFactory")))
				{
					return mainFactoryClass;
				}
			}
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			
		}
		return null;
	}
	
	public String getMainOfficeFactoryClassName()
	{
		return mainOfficeFactoryClassName;
	}
	
	public Class getInstallationFinderClass()
	{
		try
		{
			Class installationFinderClass = Class.forName(installationFinderClassName);
			Class superclass = installationFinderClass.getSuperclass();
			
			if(superclass.equals(Class.forName("de.tarent.documents.configuration.installationfinder.AbstractInstallationFinder")))
			{
				return installationFinderClass;
			}
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			
		}
		return null;
	}
	
	public String getInstallationFinderClassName()
	{
		return installationFinderClassName;
	}
	
	public FileFilter getFileFilterLoad()
	{
		if(fileFilterLoad == null)
		{
			fileFilterLoad = new ExtendableFileFilter();
			Iterator it = supportedOfficeFileFormats.iterator();
			while(it.hasNext())
			{
				FileFormatSupport fileFormatSupport = (FileFormatSupport) it.next();
				if(fileFormatSupport.canRead()) ((ExtendableFileFilter)fileFilterLoad).addFileFormat(fileFormatSupport.getOfficeFileFormat());
			}
		}
		if(fileFilterLoad instanceof ExtendableFileFilter) ((ExtendableFileFilter)fileFilterLoad).setDescription(Messages.getFormattedString("GUI_FILE_FILTER_FILETYPE_DESCRIPTION", getName()+" "+getDisplayVersion()));
		return fileFilterLoad;
	}
	
	public List getSeperateFileFiltersLoad()
	{
		if(fileFiltersLoad == null)
		{
			fileFiltersLoad = new ArrayList();
			
			Iterator it = supportedOfficeFileFormats.iterator();
			
			while(it.hasNext())
			{
				ExtendableFileFilter fileFilter = new ExtendableFileFilter();
				FileFormatSupport fileFormatSupport = (FileFormatSupport) it.next();
				if(fileFormatSupport.canRead()) fileFilter.addFileFormat(fileFormatSupport.getOfficeFileFormat());
				fileFilter.setDescription(fileFormatSupport.getOfficeFileFormat().getLongName() + " (" + fileFormatSupport.getOfficeFileFormat().getCommaSeperatedSuffixes() + ")");
				fileFiltersLoad.add(fileFilter);
			}
		}
		return fileFiltersLoad;
	}
	
	public FileFilter getFileFilterSave()
	{
		if(fileFilterSave == null)
		{
			fileFilterSave = new ExtendableFileFilter();
			Iterator it = supportedOfficeFileFormats.iterator();
			while(it.hasNext())
			{
				FileFormatSupport fileFormatSupport = (FileFormatSupport) it.next();
				if(fileFormatSupport.canWrite()) ((ExtendableFileFilter)fileFilterSave).addFileFormat(fileFormatSupport.getOfficeFileFormat());
			}
		}
		return fileFilterLoad;
	}
	
	public List getSeparateFileFiltersSave()
	{
		if(fileFiltersSave == null)
		{
			fileFiltersSave = new ArrayList();
		
			Iterator it = supportedOfficeFileFormats.iterator();
			
			while(it.hasNext())
			{
				ExtendableFileFilter fileFilter = new ExtendableFileFilter();
				FileFormatSupport fileFormatSupport = (FileFormatSupport) it.next();
				if(fileFormatSupport.canWrite()) fileFilter.addFileFormat(fileFormatSupport.getOfficeFileFormat());
				fileFilter.setDescription(fileFormatSupport.getOfficeFileFormat().getLongName() + " (" + fileFormatSupport.getOfficeFileFormat().getCommaSeperatedSuffixes() + ")");
				fileFiltersSave.add(fileFilter);
			}
		}
		return fileFiltersSave;
	}
	
	public List getSupportedOfficeFileFormats()
	{
		return supportedOfficeFileFormats;
	}
	
	public int getDefaultPort()
	{
		return defaultPort;
	}
	
	public int getDefaultConnectionType()
	{
		return defaultConnectionType;
	}
	
	public boolean isCompatibleHostOS(String pHostOS)
	{
		/* TODO implement */
		return true;
	}
	
	public int getBuildMin()
	{
		return buildMin;
	}
	
	public int getBuildMax()
	{
		return buildMax;
	}
	
	public boolean isCompatibleWith(OfficeTypeGroup pOfficeTypGroup)
	{
		/* TODO implement (Get information out of office_types.xmls */
		return true;
	}
	
	public OfficeConnectionMethod getDefaultConnectionMethod()
	{
		if(!getSupportedConnectionMethods().isEmpty())
			return (OfficeConnectionMethod)getSupportedConnectionMethods().get(0);
		return OfficeConnectionMethod.DEFAULT;
	}
	
	public List getSupportedConnectionMethods()
	{
		if(supportedConnectionMethods == null) supportedConnectionMethods = new ArrayList();
		return supportedConnectionMethods;
	}
	
	public boolean buildMatches(int pBuild)
	{
		return (pBuild >= buildMin && pBuild <= buildMax);
	}
	
	/* Setter */
	
	public void setVersion(String pVersion)
	{
		version = pVersion;
	}
	
	public void setVersionName(String pVersionName)
	{
		/*
		version = new int[] {-1, -1, -1 };
		try
		{
			for(int i=0; i < 3; i++)
			{
				if(pVersionName == "") version[i] = -1;
				else if(pVersionName.indexOf('.') == -1)
				{
					version[i] = Integer.parseInt(pVersionName);
					pVersionName = "";
				}
				else
				{
					version[i] = Integer.parseInt(pVersionName.substring(0, pVersionName.indexOf('.')));
					pVersionName = pVersionName.substring(pVersionName.indexOf('.')+1);
				}
			}
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
		}
		*/
		version = pVersionName;
	}
	
	public void setDisplayVersion(String pDisplayVersion)
	{
		displayVersion = pDisplayVersion;
	}
	
	public void setName(String pName)
	{
		name = pName;
	}
	
	public void setShortName(String pShortName)
	{
		shortName = pShortName;
	}
	
	public void setExec(String pExec)
	{
		exec = pExec;
	}
	
	public void setExecOptions(String[] pExecOptions)
	{
		execOptions = pExecOptions;
	}
	
	public void setClassPathEntries(String [] pClassPathEntries)
	{
		classPathEntries = pClassPathEntries;
	}
	
	public void setLibraryPathEntries(String [] pLibraryPathEntries)
	{
		libraryPathEntries = pLibraryPathEntries;
	}
	
	public boolean hasVersion()
	{
		if(version == null) return false;
		return true;
	}
	
	public void setMainOfficeFactoryClass(Class pClass)
	{
		setMainOfficeFactoryClassName(pClass.getName());
	}
	
	public void setMainOfficeFactoryClassName(String pClass)
	{
		mainOfficeFactoryClassName = pClass;
	}
	
	public void setInstallationFinderClass(Class pClass)
	{
		setInstallationFinderClassName(pClass.getName());
	}
	
	public void setInstallationFinderClassName(String pClass)
	{
		installationFinderClassName = pClass;
	}
	
	public void setFileFilterClass(Class pFileFilterClass)
	{
		fileFilterClassName = pFileFilterClass.getName();
	}
	
	public void setFileFilterClassName(String pFileFilterClassName)
	{
		fileFilterClassName = pFileFilterClassName;
	}
	
	public void setFileFilterLoad(FileFilter pFileFilter)
	{
		fileFilterLoad = pFileFilter;
	}
	
	public void setFileFilterSave(FileFilter pFileFilter)
	{
		fileFilterSave = pFileFilter;
	}
	
	public void setSupportedOfficeFileFormats(List pFileFormatSupports)
	{
		supportedOfficeFileFormats = pFileFormatSupports;
	}
	
	public void addSupportedOfficeFileFormat(OfficeFileFormat pOfficeFileFormat, boolean pCanRead, boolean pCanSave)
	{
		// reset file filters because they will not be up to date anymore
		fileFiltersLoad = null;
		fileFiltersSave = null;
		fileFilterSave = null;
		fileFilterLoad = null;
		
		if(supportedOfficeFileFormats == null) supportedOfficeFileFormats = new ArrayList();
		supportedOfficeFileFormats.add(new FileFormatSupport(pOfficeFileFormat, pCanRead, pCanSave));
	}
	
	public void setDefaultPort(int pPort)
	{
		defaultPort = pPort;
	}
	
	public void setDefaultConnectionType(int pConnType)
	{
		defaultConnectionType = pConnType;
	}
	
	public void setSupportedConnectionMethods(List pSupportedConnectionMethods)
	{
		supportedConnectionMethods = pSupportedConnectionMethods;
	}
	
	public void setBuildMin(int pMin)
	{
		buildMin = pMin;
	}
	
	public void setBuildMax(int pMax)
	{
		buildMax = pMax;
	}
	
	/**
	 * 
	 * Compares this OfficeType to an other. Can be used to compare only the names (binary) or optionally the versions by value. 
	 * 
	 * @param pOT
	 * @return -2 if name does not equal, -1 if version is lower, 0 if types are equal or pOT has no version, 1 if version is greater or this type has no version
	 * 
	 */
	
	public int compareTo(OfficeType pOT)
	{
		int returnValue = -2;
		/*
		if(this.getShortName().equals(pOT.getShortName()))
		{
			returnValue = 0;
			
			if(pOT.getVersion()[0] != -1)
			{
				for(int i = 0; i < pOT.getVersion().length; i++)
				{
					if(this.getVersion()[i] < pOT.getVersion()[i]) return returnValue-1;
					else if (this.getVersion()[i] > pOT.getVersion()[i]) return returnValue+1;
				}
			}
		}
		*/
		return returnValue;
	}
	
	public boolean compatibleTo(OfficeType pOT)
	{
		if(compatibleOffices.contains(pOT)) return true;
		return false;
	}
	
	public boolean isSupportedConnectionMethod(OfficeConnectionMethod pConnectionMethod)
	{
		Iterator it = supportedConnectionMethods.iterator();
		
		while(it.hasNext())
		{
			if(((OfficeConnectionMethod)it.next()).getClass().equals(pConnectionMethod.getClass())) return true;
		}

		return false;
	}
	
	public boolean isSupportedFileFormatRead(OfficeFileFormat pFileFormat)
	{
		Iterator it = supportedOfficeFileFormats.iterator();
		while(it.hasNext())
		{
			FileFormatSupport support = (FileFormatSupport) it.next();
			if(support.getOfficeFileFormat().getClass().equals(pFileFormat.getClass()) && support.canRead()) return true;
		}
		return false;
	}
	
	public boolean isSupportedFileFormatWrite(OfficeFileFormat pFileFormat)
	{
		Iterator it = supportedOfficeFileFormats.iterator();
		while(it.hasNext())
		{
			FileFormatSupport support = (FileFormatSupport) it.next();
			if(support.getOfficeFileFormat().getClass().equals(pFileFormat.getClass()) && support.canWrite()) return true;
		}
		return false;
	}
	
	public List getBeforeConnectActions()
	{
		return beforeConnectActions;
	}
	
	public void setBeforeConnectActions(List pBeforeConnectActions)
	{
		beforeConnectActions = pBeforeConnectActions;
	}
	
	public int runBeforeConnectActions(OfficeConfiguration pConf)
	{
		Iterator it = beforeConnectActions.iterator();
		
		while(it.hasNext())
		{
			if(((ConfigurationAction)it.next()).process(pConf) == ConfigurationAction.RETURN_ERROR)
				return ConfigurationAction.RETURN_ERROR;
		}
		return ConfigurationAction.RETURN_SUCCESS;
	}
	
	public List getBeforeOfficeStartActions()
	{
		return beforeOfficeStartActions;
	}
	
	public void setBeforeOfficeStartActions(List pBeforeOfficeStartActions)
	{
		beforeOfficeStartActions = pBeforeOfficeStartActions;
	}
	
	public int runBeforeOfficeStartActions(OfficeConfiguration pConf)
	{
		Iterator it = beforeOfficeStartActions.iterator();
		
		while(it.hasNext())
		{
			if(((ConfigurationAction)it.next()).process(pConf) == ConfigurationAction.RETURN_ERROR)
				return ConfigurationAction.RETURN_ERROR;
		}
		return ConfigurationAction.RETURN_SUCCESS;
	}
	
	/**
	 * This method implements the <code>Cloneable</code>-Interface
	 * 
	 */
	
	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch(Exception pExcp)
		{
			throw new InternalError();
		}
	}
	
	public String toString()
	{
		return getName() + " " + getDisplayVersion();
	}
}
