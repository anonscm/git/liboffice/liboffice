/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.office;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * <p>Implementations of this abstract class represent different methods to connect an office.</p>
 *   
 * <p>OpenOffice.org-derivates may for example be connected either by a named-pipe or a socket-connection.</p>
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public abstract class OfficeConnectionMethod implements Comparable
{
	/**
	 * instance of the DefaultOfficeConnectionMethod
	 */
	public static OfficeConnectionMethod DEFAULT = new DefaultOfficeConnectionMethod();
	
	/**
	 * Returns the ID of the OfficeConnectionMethod
	 * @return the ID of the OfficeConnectionMethod
	 */
	public abstract String getID();
	
	
	/**
	 * Returns the name of the OfficeConnectionMethod
	 * @return the name of the OfficeConnectionMethod
	 */
	public abstract String getName();
	
	/**
	 * Returns an description of the OfficeConnectionMethod
	 * 
	 * @return an description of the OfficeConnectionMethod
	 */
	public abstract String getDescription();
	
	/**
	 * Returns a List of @link{OfficeConnectionMethodParameterDescription}s for additional parameter supported by this OfficeConnectionMethod
	 * 
	 * @return a List of connection-parameters containing @link{OfficeConnectionMethodParameterDescription}-Objects
	 */
	
	public abstract List getAdditionalParameters();
	
	public int compareTo(Object pObject)
	{
		if(pObject.getClass().equals(this.getClass())) return 0;
		return -1;
	}
	
	public String toString()
	{
		return getName();
	}
	
	
	/**
	 * 
	 * This is the default OfficeConnectionMethod which maybe used if the user did not defined anything special or an office-system
	 * only knows a single connection-method.
	 * 
	 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
	 *
	 */
	
	private static class DefaultOfficeConnectionMethod extends OfficeConnectionMethod
	{
		public String getID()
		{
			return "DEFAULT";
		}
		
		public String getName()
		{
			return "Standard";
		}
		
		public String getDescription()
		{
			return "The Standard Connection Method";
		}
		
		public List getAdditionalParameters()
		{
			return new ArrayList();
		}
	}
}
