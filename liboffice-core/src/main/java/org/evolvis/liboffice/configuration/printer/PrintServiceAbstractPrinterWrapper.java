/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.printer;

import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.ServiceUIFactory;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintServiceAttribute;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.event.PrintServiceAttributeListener;

/**
 * 
 * This Class wrapps an PrintService-Object into an AbstractPrinter-Object
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class PrintServiceAbstractPrinterWrapper extends AbstractPrinter
{
	PrintService printService;
	
	public PrintServiceAbstractPrinterWrapper(PrintService pPrintService)
	{
		printService = pPrintService;
	}

	public void addPrintServiceAttributeListener(PrintServiceAttributeListener pPrintServiceAttributeListener)
	{
		printService.addPrintServiceAttributeListener(pPrintServiceAttributeListener);
	}

	public DocPrintJob createPrintJob()
	{
		return printService.createPrintJob();
	}

	public PrintServiceAttribute getAttribute(Class pClass)
	{
		return printService.getAttribute(pClass);
	}

	public PrintServiceAttributeSet getAttributes()
	{
		return printService.getAttributes();
	}

	public Object getDefaultAttributeValue(Class pClass)
	{
		return printService.getDefaultAttributeValue(pClass);
	}

	public String getName()
	{
		return printService.getName();
	}

	public ServiceUIFactory getServiceUIFactory()
	{
		return printService.getServiceUIFactory();
	}

	public Class[] getSupportedAttributeCategories()
	{
		return printService.getSupportedAttributeCategories();
	}

	public Object getSupportedAttributeValues(Class pClass, DocFlavor pDocFlavor, AttributeSet pAttributeSet)
	{
		return printService.getSupportedAttributeValues(pClass, pDocFlavor, pAttributeSet);
	}

	public DocFlavor[] getSupportedDocFlavors()
	{
		return printService.getSupportedDocFlavors();
	}

	public AttributeSet getUnsupportedAttributes(DocFlavor pDocFlavor, AttributeSet pAttributeSet)
	{
		return printService.getUnsupportedAttributes(pDocFlavor, pAttributeSet);
	}

	public boolean isAttributeCategorySupported(Class pClass)
	{
		return printService.isAttributeCategorySupported(pClass);
	}

	public boolean isAttributeValueSupported(Attribute pAttribute, DocFlavor pDocFlavor, AttributeSet pAttributeSet)
	{
		return printService.isAttributeValueSupported(pAttribute, pDocFlavor, pAttributeSet);
	}

	public boolean isDocFlavorSupported(DocFlavor pDocFlavor)
	{
		return printService.isDocFlavorSupported(pDocFlavor);
	}

	public void removePrintServiceAttributeListener(PrintServiceAttributeListener pPrintServiceAttributeListener)
	{
		printService.removePrintServiceAttributeListener(pPrintServiceAttributeListener);
	}
}
