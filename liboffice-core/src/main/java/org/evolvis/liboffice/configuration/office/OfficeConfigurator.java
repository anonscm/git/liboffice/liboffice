/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.configuration.office;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.liboffice.configuration.AbstractConfigurationLoader;
import org.evolvis.liboffice.configuration.AbstractConfigurationSaver;
import org.evolvis.liboffice.configuration.installationfinder.AbstractInstallationFinder;
import org.evolvis.liboffice.configuration.parser.PlainOfficeConfigurationParser;
import org.evolvis.liboffice.factory.MainOfficeFactory;
import org.evolvis.liboffice.ui.FirstRunWizard;
import org.evolvis.liboffice.utils.SystemInfo;
import org.evolvis.taskmanager.TaskManager.Context;

/**
 * FIXME functionality partly disabled, outsource to separate module
 * 
 * This class trys to automatically detect existing office-systems on a host-system and provides access to
 * the appropriate GeneralDocumentGenerator-classes 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */


public class OfficeConfigurator
{
	// Java 1.5: private List<OfficeConfiguration> offices;
	// Java 1.5: private List<PrintService> printServices;
	private List offices;
	private MainOfficeFactory factory;
	private static OfficeConfigurator instance;
	
	private static Logger logger = Logger.getLogger(OfficeConfigurator.class.getName());
	
	private OfficeConfigurator()
	{
		offices = new ArrayList();
	}
	
	/**
	 * Initializes the Office-Configuration. Trys to load an existing configuration or runs the given first-run-wizard if necessary.
	 * 
	 * 
	 * @param pFirstRunWizard - the first-run-wizard, to be launched when no configuration exists
	 * @return - if at least one office has been configured, either automatically or manually
	 */
	
	public boolean setup(FirstRunWizard pFirstRunWizard)
	{
		boolean success = true;
		if(!loadConfiguration(null)) // If no valid configuration exists
		{
			success = runWizard(pFirstRunWizard);
		}
		
		/*
		 * disable checksum-comparison for the moment
		 * 
		else if(!validateOffice(getPreferredOffice())) // If checksum comparison does not succeed
		{
			// TODO Userinteraction
			// TODO validate each registered configuration
			if(logger.isLoggable(Level.WARNING)) logger.warning("checksum comparison of office-system-binary did not succeed");
			//return false;
		}
		*/
		return success;
	}
	
	/**
	 * Runs a first-run-wizard
	 * 
	 * @param pFirstRunWizard - the first-run-wizard to be launched
	 * @return - if at least one office has been configured, either automatically or manually
	 */

	private boolean runWizard(FirstRunWizard pFirstRunWizard)
	{
		if(pFirstRunWizard == null)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("No FirstRunWizard defined... aborting");
			return false;
		}
		
		int n = pFirstRunWizard.methodChooseDialog();
		
		if(n == FirstRunWizard.RETURN_AUTO) // If the user selected 'automatically'
		{
			if (!autoInit()) // If the automatic search has no result
			{
				if(logger.isLoggable(Level.INFO)) logger.info("No matching office-system found. Requesting manual input");
				// Tell the user that the automatic search was not successful
				pFirstRunWizard.noInitSuccessDialog();
				// Show Dialog for manual configuration of office-system and add to the list of available offices
				OfficeConfiguration conf = pFirstRunWizard.manualInputDialog();
					
				// Ask for connection-method if additional existing
				if(conf != null)
				{
					if(conf.getType().getSupportedConnectionMethods().size() > 0) 
						pFirstRunWizard.connMethodChooseDialog(conf);
				
					getAvailableOffices().add(conf);
					return true;
				}
				else return false;
			}
			else
			{
				// If automatic search has found more than one office-system
				if(offices.size() > 1)
				{
					setPreferredOffice((OfficeConfiguration)pFirstRunWizard.officeSelectionDialog(offices));
				}
				else
					pFirstRunWizard.foundOfficeDialog(getPreferredOffice());
				
				// Ask for connection-method if additional existing
				if(getPreferredOffice().getType().getSupportedConnectionMethods().size() > 0)
					pFirstRunWizard.connMethodChooseDialog(getPreferredOffice());
				return true;
			}
		}
		else if(n == FirstRunWizard.RETURN_MANU) // If the user selected 'manually'
		{
			// Show Dialog for manual configuration of office-system and add to the list of available offices
			OfficeConfiguration conf = pFirstRunWizard.manualInputDialog();
			
			// Ask for connection-method if additional existing
			if(conf != null)
			{
				if(conf.getType().getSupportedConnectionMethods().size() > 0) 
					pFirstRunWizard.connMethodChooseDialog(conf);
			
				getAvailableOffices().add(conf);
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	public static OfficeConfigurator getInstance()
	{
		if(instance == null) instance = new OfficeConfigurator();
		return instance;
	}
	
	
	/* Getters */
	
	/**
	 * Returns a List of available offices
	 * 
	 * @return - a List containting the available offices
	 */
	public List getAvailableOffices()
	{
		//if(offices == null) offices = new ArrayList();
		return offices;
	}
	
	/**
	 * Returns the preferred office
	 * 
	 * @return the OfficeConfiguration-Object of the preferred office
	 */
	
	public OfficeConfiguration getPreferredOffice()
	{
		// Java 1.5: Iterator<OfficeConfiguration> it = offices.iterator();
		//if(offices == null) return null;
		
		Iterator it = offices.iterator();
		
		while(it.hasNext())
		{
			// Java 1.5: OfficeConfiguration conf = it.next();
			OfficeConfiguration conf = (OfficeConfiguration) it.next();
			if(conf.isPreferred()) return conf;
		}
		
		// If the user does not choose a preferred office, take the first in list
		// Java 1.5: if(offices.size() > 0) return offices.get(0);
		if(offices.size() > 0)
		{
			((OfficeConfiguration)offices.get(0)).setPreferred(true);
			// Update printers
			//searchPrinters();
			return (OfficeConfiguration)offices.get(0);
		}

		return null;
	}
	
	/**
	 * Sets the preferred-office
	 * 
	 * @param pOfficeConfiguration - the OfficeConfiguration-Object, representing the office which should be set preferred
	 * @return false, if the OfficeConfiguration is not in the list
	 */
	
	public boolean setPreferredOffice(OfficeConfiguration pOfficeConfiguration)
	{
		if(offices.contains(pOfficeConfiguration))
		{
			// First unset the old preferred office
			if(getPreferredOffice() != null) getPreferredOffice().setPreferred(false);
			
			// Then set the new office
			// Java 1.5: offices.get(offices.indexOf(pOfficeConfiguration)).setPreferred(true);
			((OfficeConfiguration)offices.get(offices.indexOf(pOfficeConfiguration))).setPreferred(true);
			// Setup new environment variables
			// FIXME this is currently not implemented
			// getPreferredOffice().setupEnvironmentVariables();
			// Update Printers
			//searchPrinters();
			return true;
		}
		return false;
	}
	
	
	/**
	 * Sets the list of available offices
	 * 
	 * @param pOffices a List containting OfficeConfiguration-objects
	 */
	public void setAvailableOffices(List pOffices)
	{
		if(pOffices == null) offices = new ArrayList();
		else offices = pOffices;
	}
	
	/**
	 * Adds an office to the list of available offices
	 * 
	 * @param pOfficeConfiguration - the OfficeConfiguration-Object to be added
	 */
	
	public void addOfficeConfiguration(OfficeConfiguration pOfficeConfiguration)
	{
		offices.add(pOfficeConfiguration);
	}
	
	
	/**
	 * 
	 * Removes an office from the list of available offices
	 * 
	 * @param conf - The OfficeConfiguration to remove
	 */
	
	public void removeOfficeConfiguration(OfficeConfiguration conf)
	{
		if(conf != null)
			offices.remove(conf);
	}
	
	/**
	 * Check if an special OfficeType is in the List of available offices. (e.g. MS-Office 2007, OpenOffice.org 2.1 etc.)
	 * 
	 * @param pType - the OfficeType-Object to be checked if is in the list
	 * @return true if it the OfficeType is in the list
	 */
	
	public boolean isOfficeTypeAvailable(OfficeType pType)
	{
		// Java 1.5: Iterator<OfficeConfiguration> it = offices.iterator();
		Iterator it = offices.iterator();
		
		while(it.hasNext())
		{
			// Java 1.5: OfficeConfiguration conf = it.next();
			OfficeConfiguration conf = (OfficeConfiguration) it.next();
			if(pType.compareTo(conf.getType()) == 0) return true;
		}
		return false;
	}
	
	/**
	 * Runs all available InstallationFinders to find installed offices
	 * 
	 * @return if at least one office has been found
	 */
	
	public boolean autoInit()
	{
		boolean found = false;
		// Java 1.5: offices = new ArrayList<OfficeConfiguration>();
		offices = new ArrayList();
		
				
		// Java 1.5: List<OfficeTypeGroup> officeTypeGroups = OfficeConfigurationParser.getInstance().getOfficeTypeGroups();
		List officeTypeGroups = PlainOfficeConfigurationParser.getInstance().getOfficeTypeGroups();
		
		for(int i=0; i <officeTypeGroups.size(); i++)
		{
			// Java 1.5: OfficeTypeGroup typeGroup = officeTypeGroups.get(i);
			OfficeTypeGroup typeGroup = (OfficeTypeGroup) officeTypeGroups.get(i);
			if(typeGroup.isCompatibleHostOS(System.getProperty("os.name")))
			{
				try
				{
					AbstractInstallationFinder finder = (AbstractInstallationFinder)typeGroup.getInstallationFinderClass().newInstance();
					// Java 1.5: List<OfficeConfiguration> configs = finder.getConfigurations();
					List configs = finder.getConfigurations();
					for(int z=0; z < configs.size(); z++)
					{
						// Java 1.5: OfficeConfiguration conf = configs.get(z);
						OfficeConfiguration conf = (OfficeConfiguration) configs.get(z);
						if(conf != null)
						{
							if(logger.isLoggable(Level.INFO)) logger.info("Found Office of typeGroup: "+conf.getType().getName()+" "+conf.getType().getVersionName());
							
							addOfficeConfiguration(conf);
							found = true;
						}
					}
				}
				catch(Exception pExcp)
				{
					pExcp.printStackTrace();
				}
			}
		}
		
		return found;
	}
	
	/**
	 * Returns the MainOfficeFactory-Object of the preffered office
	 * 
	 * @return the MainOfficeFactory-Object of the preferred office
	 */
	
	public MainOfficeFactory getMainOfficeFactory()
	{	
		startPreferredOffice(null);
		return factory;
	}
	
	/**
	 * Starts the preferred object
	 * 
	 * @return if the preferred object has been started successful
	 */
	
	public boolean startPreferredOffice(Context pContext)
	{	
		if(getPreferredOffice() == null) return false;
		
		getPreferredOffice().getType().runBeforeOfficeStartActions(getPreferredOffice());
		
		if(logger.isLoggable(Level.INFO)) logger.info("I will now try to connect to the office-system to see if it is already up. You may see an error-message here if its not");
		
		if(!isOfficeSystemAlreadyRunning())
		{			
			if(logger.isLoggable(Level.INFO)) logger.info("Office-System not up, will start it now and then try to connect. You may see a lot of error-messages here");
			
			Process process = null;
			
			try
			{
				process = Runtime.getRuntime().exec(getPreferredOffice().getPath()+getPreferredOffice().getType().getExec()+" "+getPreferredOffice().getType().getOptionsString());
				
				int timeout = 30;
				
				if(logger.isLoggable(Level.INFO)) logger.info("Timout is set to "+timeout+"  seconds");
				
				while(!isOfficeSystemAlreadyRunning() && timeout > 0)
				{
					// check if user wants to cancel this process
					if(pContext != null && pContext.isCancelled())
					{
						if(logger.isLoggable(Level.INFO)) logger.info("User cancelled Office-Startup-Process");
						break;
					}
								
					// Wait a second
					Thread.sleep(1000);
					timeout--;
				}
				if(timeout > 0 && !(pContext != null && pContext.isCancelled()))
				{
					if(logger.isLoggable(Level.INFO)) logger.info("Office-System is up now");
					factory.getControllerFactory().getOfficeController().getStarterDocument().setVisible(false);
					return true;
				}
			}
			catch(Exception pExcp)
			{
				pExcp.printStackTrace();
			}
			
			if(logger.isLoggable(Level.INFO)) logger.info("Process did not finish. Killing office-process.");
			// If we could not connect, kill the office-process (which might be running invisible in background)
			if(process != null) process.destroy();
			
			return false;
		}
		else if(logger.isLoggable(Level.INFO)) logger.info("Office-System is already up");
		
		return true;
	}
	
	/**
	 * Checks if the preferred office is already running
	 * 
	 * @return true if a connection to the office could be established
	 */
	
	public boolean isOfficeSystemAlreadyRunning()
	{
		//getPreferredOffice().getType().runBeforeConnectActions(getPreferredOffice());
		
		try
		{
			/* Instantiate Office-Factory */
			
			factory = (MainOfficeFactory)getPreferredOffice().getType().getMainOfficeFactoryClass().newInstance();
			
			
			/* If connection could be established return true */
			if(factory != null)
			{
				//if(logger.isLoggable(Level.INFO)) logger.info("Trying to connect to Office by method: "+getPreferredOffice().getConnectionMethod().getName());
				return factory.connect(getPreferredOffice().getConnectionMethod(), getPreferredOffice().getConnectionParameters());
			}
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Returns the directory where programs are installed. For example C:\Programme on a normal windows system.
	 * 
	 * @return the directory, where programs are installed
	 */
	
	public static String getProgramDir()
	{
		String progDir = System.getProperty("user.home");
		
		if(SystemInfo.isWindowsSystem())
		{
//			RegistryKey	r = new RegistryKey(RootKey.HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion");
						
//			if(r.exists()) progDir = r.getValue("ProgramFilesDir").getData().toString();
			
			throw new UnsupportedOperationException("This method is currently disfunctional.");
		}
		else if(SystemInfo.isLinuxSystem())
		{
			progDir = "/";
		}
		
		return progDir;
	}
	
	/**
	 * Loads an office-configuration from a file
	 * 
	 * @param pLoader - not used yet
	 * @return true, if the configuration was loaded successfully
	 */
	
	public boolean loadConfiguration(AbstractConfigurationLoader pLoader)
	{
		offices = PlainOfficeConfigurationParser.getInstance().getOfficeConfigurations();
		if(offices == null) offices = new ArrayList();
		if(!offices.isEmpty()) return true;
		return false;
	}
	
	/**
	 * saves an office-configuration to a file
	 * 
	 * @param pSaver - not used yet
	 * @return true, if the configuration was saved successfully
	 */
	
	public boolean saveConfiguration(AbstractConfigurationSaver pSaver)
	{
		PlainOfficeConfigurationParser.getInstance().saveConfig(offices);
		
		return true;
	}
	
	/**
	 * Validates an office by checksum-comparison. To be used for recognizing updates of an office-installation in future.
	 * 
	 * @param pOfficeConf the configuration which should be validated
	 * @return if the installation has not changed (is "valid")
	 */
	
//	public boolean validateOffice(OfficeConfiguration pOfficeConf)
//	{	
//		return ChecksumTool.validateFile(pOfficeConf.getPath()+pOfficeConf.getType().getExec(), pOfficeConf.getChecksum());
//	}
	
	public void disposeConnection()
	{
		factory = null;
	}
}