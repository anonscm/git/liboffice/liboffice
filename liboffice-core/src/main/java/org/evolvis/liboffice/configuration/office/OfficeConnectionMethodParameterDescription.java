/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * 
 */
package org.evolvis.liboffice.configuration.office;

/**
 * 
 * <p>
 * An OfficeConnectionMethodParameterDescription is (like the name tells) a description on parameters for office-connection-methods :)
 * </p>
 * <p>
 * Basically, you can define an ID, a name, give a description and define the parameter-type, e.g. String or Integer.<br />
 * An default-value is optional
 * </p> 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public final class OfficeConnectionMethodParameterDescription
{
	private String id;
	private String name;
	private String description;
	private Class type;
	private Object defaultValue;
	
	/**
	 * 
	 * Constructs an OfficeConnectionMethodParameterDescription-Object without default-value (will be null)
	 * 
	 * @param pID the ID of the parameter
	 * @param pName the name of the parameter
	 * @param pDesc an description for the parameter
	 * @param pType the type of the parameter
	 */
	
	public OfficeConnectionMethodParameterDescription(String pID, String pName, String pDesc, Class pType)
	{
		this(pID, pName, pDesc, pType, null);
	}
	
	
	/**
	 * 
	 * Constructs an OfficeConnectionMethodParameterDescription-Object with a given default-value
	 * 
	 * @param pID the ID of the parameter
	 * @param pName the name of the parameter
	 * @param pDesc an description for the parameter
	 * @param pType the type of the parameter
	 * @param pDefaultValue the default-value for the parameter. Must be an instance of pType
	 */
	
	public OfficeConnectionMethodParameterDescription(String pID, String pName, String pDesc, Class pType, Object pDefaultValue)
	{
		id = pID;
		name = pName;
		description = pDesc;
		type = pType;
		defaultValue = pDefaultValue;
	}
	
	/**
	 * Returns the ID of the OfficeConnectionParameter
	 * 
	 * @return the ID of the OfficeConnectionParameter
	 */
	
	public String getID()
	{
		return id;
	}
	
	/**
	 * 
	 * Returns the name of the OfficeConnectionParameter
	 * 
	 * @return the name of the OfficeConnectionParameter
	 */
	
	public String getName()
	{
		return name;
	}
	
	/**
	 * Returns an description of the OfficeConnectionParameter
	 * 
	 * @return an description of the OfficeConnectionParameter
	 */
	
	public String getDescription()
	{
		return description;
	}
	
	
	/**
	 * 
	 * <p>Returns the type of the OfficeConnectionParameter</p>
	 * <p>e.g. String, Integer etc..</p>
	 * 
	 * @return the type of the OfficeConnectionParameter
	 */
	
	public Class getType()
	{
		return type;
	}
	
	/**
	 * 
	 * <p>Returns the default value of the OfficeConnectionParameter</p>
	 * 
	 * <p>The returned value is an instance of the class returned by getType()</p>
	 * 
	 * @return the default value of the OfficeConnectionParameter
	 */
	
	public Object getDefaultValue()
	{
		// Ensure defined type and type of default value are equal
		if(defaultValue.getClass().equals(type))
			return defaultValue;
		
		return null;
	}
}
