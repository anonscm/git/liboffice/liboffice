/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.filefilter;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import org.evolvis.liboffice.officefileformats.OfficeFileFormat;

/**
 * 
 * This FileFilter can be extended with <code>OfficeFileFormat</code>-Objects at runtime
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class ExtendableFileFilter extends FileFilter
{
	private String description;
	private List fileFormats;
	
	public ExtendableFileFilter()
	{
		super();
		fileFormats = new ArrayList();
	}
	
	public ExtendableFileFilter(String pDescription)
	{
		super();
		description = pDescription;
		fileFormats = new ArrayList();
	}

	public boolean accept(File pFile)
	{
		if(pFile.isDirectory()) return true;
		Iterator it = fileFormats.iterator();
		while(it.hasNext())
		{
			OfficeFileFormat thisFileFormat = (OfficeFileFormat)it.next();
			if(thisFileFormat.getFileFilter().accept(pFile)) return true;
		}
		return false;
	}

	public String getDescription()
	{
		//return Messages.getFormattedString("GUI_FILE_FILTER_FILETYPE_DESCRIPTION", description);
		return description;
	}
	
	public void setDescription(String pDescription)
	{
		description = pDescription;
	}
	
	public void addFileFormat(OfficeFileFormat pOfficeFileFormat)
	{
		fileFormats.add(pOfficeFileFormat);
	}
	
	public void removeFileFormat(OfficeFileFormat pOfficeFileFormat)
	{
		fileFormats.remove(pOfficeFileFormat);
	}
	
	public boolean supportsFileFormat(OfficeFileFormat pOfficeFileFormat)
	{
		return fileFormats.contains(pOfficeFileFormat);
	}
	
	public List getSupportedFileFormats()
	{
		return fileFormats;
	}
	
	public void setSupportedFileFormats(List pFileFormats)
	{
		fileFormats = pFileFormats;
	}
}
