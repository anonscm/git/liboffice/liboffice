/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.filefilter;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.ui.Messages;


/**
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class ExecutableFileFilter extends FileFilter
{
	private OfficeType type;
	
	public ExecutableFileFilter(OfficeType pType)
	{
		type = pType;
	}
	
	public boolean accept(File pFile)
	{
		if(pFile.isDirectory()) return true;
		
		String exec = type.getExec();
		if(exec.indexOf(File.separatorChar) != -1) exec = exec.substring(exec.lastIndexOf(File.separatorChar)+1);
	
		if(pFile.getName().toLowerCase().equals(exec) || pFile.getName().toLowerCase().equals(exec+".exe")) return true;
		return false;
	}
	
	public String getDescription()
	{
		return Messages.getFormattedString("GUI_FILE_FILTER_EXECUTABLE_DESCRIPTION", type.getName());
	}
}
