package org.evolvis.liboffice.ui.swing.conf;

import java.awt.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeConfigurator;
import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.configuration.parser.PlainOfficeConfigurationParser;
import org.evolvis.liboffice.filefilter.ExecutableFileFilter;
import org.evolvis.liboffice.ui.FirstRunWizard;
import org.evolvis.liboffice.ui.swing.Messages;

import de.tarent.commons.utils.ChecksumTool;


/**
 * 
 * This class provides UI-Elements which are called when the office-integration has not yet been configured
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */

public class FirstRunWizardSwing implements FirstRunWizard
{	
	private Component parentComponent;
	
	/**
	 * 
	 * Shows a dialog which lets the user choose between automatic- and manual-configuration
	 * 
	 * @return the method the user has choosen
	 */
	
	public FirstRunWizardSwing(Component pParentComponent)
	{
		parentComponent = pParentComponent;
		//LookAndFeelTool.setupLaF();
	}

	public int methodChooseDialog()
	{
		Object[] options = {Messages.getString("GUI_FIRST_RUN_AUTO_BUTTON"), Messages.getString("GUI_FIRST_RUN_MANU_BUTTON"), Messages.getString("GUI_FIRST_RUN_CANCEL_BUTTON")};
		return JOptionPane.showOptionDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_START_TEXT"), Messages.getString("GUI_FIRST_RUN_START_TITLE"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	}
	
	public int methodChoosePlusRemoveDialog()
	{
		Object[] options = {"Automatisch neu einrichten", "Manuell hinzuf�gen", "Offices entfernen", "Abbrechen"};
		return JOptionPane.showOptionDialog(parentComponent, Messages.getString("Was m�chten Sie unternehmen?"), "Office-System verwalten", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	}
	
	/**
	 * 
	 * Shows a dialog which tells the user that the automatic search-routine did not succeed to find an office-system
	 *
	 */
	
	public void noInitSuccessDialog()
	{
		JOptionPane.showMessageDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_NO_INIT_SUCCESS_TEXT"), Messages.getString("GUI_FIRST_RUN_NO_INIT_SUCCESS_TITLE"), JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * 
	 * Shows a set of dialog with can be used to configure the office-integration manually
	 * 
	 * @return the manual office-configuration or <code>null</code> if process cancelled
	 */
	
	public OfficeConfiguration manualInputDialog()
	{
		OfficeConfiguration conf = new OfficeConfiguration();
		
		// First lets determine the OfficeType
		List officeTypes = PlainOfficeConfigurationParser.getInstance().getOfficeTypes();
		Map map = new HashMap();
		for(int i=0; i < officeTypes.size(); i++)
		{
			OfficeType type = (OfficeType)officeTypes.get(i);
			map.put(type.getName()+" "+type.getDisplayVersion(), type);
		}
		
		String s = (String)JOptionPane.showInputDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_MANUAL_INPUT_TYPE_TEXT"), Messages.getString("GUI_FIRST_RUN_MANUAL_INPUT_TYPE_TITLE"), JOptionPane.PLAIN_MESSAGE, null, map.keySet().toArray(), "OpenOffice.org 2.0");
		
		if(s == null || s.length() == 0) return null;
		
		conf.setType((OfficeType) map.get(s));
		
		// Then lets get the path to the executable
		MessageFileChooser fileChooser = new MessageFileChooser(OfficeConfigurator.getProgramDir(), Messages.getString("GUI_FIRST_RUN_MANUAL_INPUT_EXEC_TEXT"));	
		fileChooser.setFileFilter(new ExecutableFileFilter(conf.getType()));
		
		boolean valid = false;
		
		while(!valid)
		{
			int returnValue = fileChooser.showDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_MANUAL_INPUT_EXEC_TITLE"));
			
			if(returnValue == MessageFileChooser.APPROVE_OPTION)
			{
				String path = fileChooser.getSelectedFile().toString();
				
				if(path.toLowerCase().indexOf(conf.getType().getExec().toLowerCase()) != -1)
				{
					conf.setPath(path.substring(0, path.toLowerCase().lastIndexOf(conf.getType().getExec().toLowerCase())));
					valid = true;
				}
				else
				{
					JOptionPane.showMessageDialog(parentComponent, Messages.getFormattedString("GUI_FIRST_RUN_MANUAL_INPUT_EXEC_NOT_VALID", conf.getType().getExec()));
				}
			}
			else
			{
				return null;
			}
		}
		
		// Checksum this executable
		
		conf.setChecksum(ChecksumTool.createChecksum(fileChooser.getSelectedFile().toString()));
		
		// Add path of the office-system to the environment variable PATH (Windows) or LD_LIBRARY_PATH (Unix) for needed libraries
		/* TODO check if already in path */
		System.setProperty("java.library.path", System.getProperty("java.library.path")+System.getProperty("path.separator")+conf.getPath()+"program");
		
		return conf;
	}
	
	public OfficeConfiguration officeSelectionDialog(List pOfficeSystems)
	{
		Map map = new HashMap();
		for(int i=0; i < pOfficeSystems.size(); i++)
		{
			OfficeConfiguration conf = (OfficeConfiguration)pOfficeSystems.get(i);
			map.put(conf.getType().getName()+" "+conf.getType().getDisplayVersion()+" in "+conf.getPath(), conf);
		}
		
		// Check for two offices in one directory
		
		Object[] entries = map.entrySet().toArray();
		
		for(int i=0; i < map.size()-1; i++)
		{
			for(int z=i+1; z < map.size()-i; z++)
			{
				if(entries[i] != null && entries[i].equals(entries[z]))
				{
					JOptionPane.showMessageDialog(parentComponent, "I found more than one Office-System in directory \""+entries[i]+"\". Probably there is only one Office installed but more registered", "Duplicate", JOptionPane.WARNING_MESSAGE);
				}
			}
		}
		
		// Java 1.5: String s = (String)JOptionPane.showInputDialog(null, Messages.getFormattedString("GUI_FIRST_RUN_PREF_OFFICE_SELECT_TEXT", Integer.valueOf(pOfficeSystems.size())), Messages.getString("GUI_FIRST_RUN_PREF_OFFICE_SELECT_TITLE"), JOptionPane.PLAIN_MESSAGE, null, map.keySet().toArray(), null);
		String s = (String)JOptionPane.showInputDialog(parentComponent, Messages.getFormattedString("GUI_FIRST_RUN_PREF_OFFICE_SELECT_TEXT", new Integer(pOfficeSystems.size())), Messages.getString("GUI_FIRST_RUN_PREF_OFFICE_SELECT_TITLE"), JOptionPane.PLAIN_MESSAGE, null, map.keySet().toArray(), null);
		return (OfficeConfiguration)map.get(s);
	}
	
	public void foundOfficeDialog(OfficeConfiguration pOfficeConf)
	{
		JOptionPane.showMessageDialog(parentComponent, Messages.getFormattedString("GUI_FIRST_RUN_SELECTED_OFFICE_TEXT", new String[]{pOfficeConf.getType().getName(), pOfficeConf.getType().getDisplayVersion(), pOfficeConf.getPath()}), Messages.getString("GUI_FIRST_RUN_SELECTED_OFFICE_TITLE"), JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public int additionalOptionsMessage()
	{
		return JOptionPane.showConfirmDialog(null, Messages.getString("GUI_FIRST_RUN_ADD_OPTIONS_QUEST_TEXT"), Messages.getString("GUI_FIRST_RUN_ADD_OPTIONS_QUEST_TITLE"), JOptionPane.YES_NO_OPTION);
	}
	
	public OfficeConfiguration connMethodChooseDialog(OfficeConfiguration pConf)
	{
//		Map types = pConf.getType().getConnectionMethodNameMap();
//		
//		Object selected = JOptionPane.showInputDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_CONN_METHOD_CHOOSE_TEXT"), Messages.getString("GUI_FIRST_RUN_CONN_METHOD_CHOOSE_TITLE"), JOptionPane.QUESTION_MESSAGE, null, types.keySet().toArray(), Messages.getString("GUI_FIRST_RUN_CONN_METHOD_DEFAULT_OPTION"));
//		
//		if(selected == null) return pConf;
//		
//		// Java 1.5: if(types.get(selected).equals(Integer.valueOf(OfficeType.DEFAULT_CONN_METHOD))) pConf.setConnectionType(OfficeType.DEFAULT_CONN_METHOD);
//		if(types.get(selected).equals(new Integer(OfficeType.DEFAULT_CONN_METHOD))) pConf.setConnectionType(OfficeType.DEFAULT_CONN_METHOD);
//		// Java 1.5: else if(types.get(selected).equals(Integer.valueOf(OfficeType.PIPE_CONN_METHOD)))
//		else if(types.get(selected).equals(new Integer(OfficeType.PIPE_CONN_METHOD)))
//		{
//			pConf.setConnectionType(OfficeType.PIPE_CONN_METHOD);
//			try
//			{
//				pConf.setPipeName(JOptionPane.showInputDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_PIPE_INPUT_TEXT"), pConf.getPipeName()));
//			}
//			catch(Exception pExcp)
//			{
//				/* TODO validation */
//				/* TODO userinteraction */
//			}
//		}
//		// Java 1.5: else if(types.get(selected).equals(Integer.valueOf(OfficeType.SOCKET_CONN_METHOD)))
//		else if(types.get(selected).equals(new Integer(OfficeType.SOCKET_CONN_METHOD)))
//		{
//			pConf.setConnectionType(OfficeType.SOCKET_CONN_METHOD);
//			// Java 1.5: int port = Integer.parseInt(JOptionPane.showInputDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_PORT_INPUT_TEXT"), Integer.valueOf(pConf.getPort())));
//			int port = Integer.parseInt(JOptionPane.showInputDialog(parentComponent, Messages.getString("GUI_FIRST_RUN_PORT_INPUT_TEXT"), new Integer(pConf.getPort())));
//			pConf.setPort(port);
//		}
		
		return pConf;
	}
}
