/**
 * 
 */
package org.evolvis.liboffice.ui.swing.conf;

import java.util.List;
import java.util.Map;

import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface OfficeConfigurationDialogDataModel
{
	public List getAvailableOffices();
	
	public int getOfficeCount();
	
	public OfficeConfiguration getSelectedOffice();
	
	public List getConnectionMethodsForSelectedOffice();
	
	public OfficeConnectionMethod getSelectedConnectionMethod();
	
	public Map getParametersForSelectedConnectionMethod();
	
	public void fireOfficeSelectionChanged();
	
	public void fireConnectionMethodChanged();
}
