package org.evolvis.liboffice.ui.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.evolvis.liboffice.OfficeAutomat;

import de.tarent.commons.ui.ComboBoxMouseWheelNavigator;


/**
 * 
 * This Dialog is designed for the following main-tasks:
 * - Selecting a template-file which should be manipulated
 * - Selecting methods how to manipulate the template
 * - Selecting actions to do after manipulation has finished
 * 
 * 
 * @author niko
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 * 
 * TODO I18n, Tooltips, Keyboard-Usage
 * 
 */
public class TemplateSelectorDialog extends JFileChooser implements WindowListener, ActionListener
{
	public static final long serialVersionUID = 32345123;
	private JButton m_oButtonOK, m_oButtonCancel;
	
	private JCheckBox m_oCheckBox_Show;
	private JCheckBox m_oCheckBox_Print;
	private JCheckBox m_oCheckBox_SaveDoc;
	private JCheckBox m_oCheckBox_Invisibility;
	
	private JComboBox printerBox;
	private JComboBox modeBox;
	
	//private JLabel printerLabel;
	
	private JDialog m_oDialog;
	
	private int m_iReturnCode;
	public static final int RETURNCODE_UNKNOWN = 0;
	public static final int RETURNCODE_OK = 1;
	public static final int RETURNCODE_CANCEL = 2;
	
	private boolean m_bDocumentShow;
	private boolean m_bDocumentPrint;
	private boolean m_bDocumentSaveDoc;
	private boolean m_bDocumentInvisibility;
	private int m_iSelectedReplaceMode;
	private int count = -1;
	
	//private Color m_oBGColor; 
	//private Color m_oButtonColor; 
	//private Font m_oFont;
	
	private int m_iNumDocuments;
	
	private String[] printers;
	
	
	public TemplateSelectorDialog(String defaultdir, String[] pPrinters, boolean docshow, boolean docprint, boolean docsavepdf, boolean docsavedoc, int mode, boolean invisibility, int numdocs)
	{
		// Check if defaultdir is valid
		
		if (null != defaultdir && defaultdir.trim().length() > 0)
		{
			setCurrentDirectory( new File( defaultdir ) );
		}
		else setCurrentDirectory( new File( System.getProperty("user.home") + "/tarent/Vorlagen/office" ) );
		
		m_iReturnCode = RETURNCODE_UNKNOWN;
		
		// process preset values
		
		m_bDocumentShow = docshow;
		m_bDocumentPrint = docprint;
		//m_bDocumentSavePDF = docsavepdf;
		m_bDocumentSaveDoc = docsavedoc;
		m_iSelectedReplaceMode = mode;
		m_bDocumentInvisibility = invisibility;
		m_iNumDocuments = numdocs;
		printers = pPrinters;    
	}
	
	
	/**
	 * 
	 * Returns the RETURN-Code of the dialog
	 * 
	 * @return one of the following: RETURNCODE_UNKNOWN, RETURNCODE_OK, RETURNCODE_CANCEL
	 */
	
	public int getReturnCode()
	{
		return(m_iReturnCode);    
	}
	
	/**
	 * 
	 * Returns the filename of the file which the user has selected
	 * 
	 * @return filename of selected file or empty string (""), if no file selected 
	 */
	
	public String getSelectedFilename()
	{
		if (this.getSelectedFile() != null)
		{
			return(this.getSelectedFile().getAbsoluteFile().toString());
		}    
		else
		{
			return("");
		}
	}
	
	public String getSelectedPrinterName()
	{
		return ((String)printerBox.getSelectedItem());
	}
	
	public void approveSelection()
	{
		okClicked();
	}
	
	public void cancelSelection()
	{
	}
	
	public JDialog createDialog(Component parent) throws HeadlessException
	{
		m_oDialog = super.createDialog(parent);
		
		JPanel controlpanel = new JPanel();        
		controlpanel.setOpaque(true);
		controlpanel.setLayout(new BorderLayout());
		
		JPanel optionpanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		
		//printerLabel = new JLabel(Messages.getString("GUI_TEMPLATE_SELECTOR_PRINTER_LABEL"));
		printerBox = new JComboBox(printers);
		printerBox.addMouseWheelListener(new ComboBoxMouseWheelNavigator(printerBox));
		printerBox.setBackground(Color.WHITE);
		
		// If no printer configured, do not show printer-related elements
//		if(printers.length != 0)
//		{
//			//optionpanel.add(printerLabel);
//			//optionpanel.add(printerBox);
//		}
		
		
		JPanel allPanel = new JPanel();
		allPanel.setLayout(new BorderLayout());
		
		JPanel buttonpanel = new JPanel();
		buttonpanel.setOpaque(true);
		buttonpanel.setLayout(new BorderLayout());
		buttonpanel.setBorder(new EmptyBorder(5,5,5,5));
		
		m_oButtonOK = new JButton(Messages.getString("GUI_TEMPLATE_SELECTOR_EXECUTE_BUTTON"));
		m_oButtonOK.setEnabled(true);   
		m_oButtonOK.addActionListener(this);
//		m_oButtonOK.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/doctor/ui/resources/gfx/go-next.png"))));
//		m_oButtonOK.setHorizontalTextPosition(JButton.LEFT);
//		m_oButtonOK.setIconTextGap(6);
		
		m_oButtonCancel = new JButton(Messages.getString("GUI_TEMPLATE_SELECTOR_CANCEL_BUTTON"));
		m_oButtonCancel.addActionListener(this);
		m_oButtonCancel.setIconTextGap(6);
		m_oButtonCancel.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/doctor/ui/resources/gfx/process-stop.png"))));
		m_oButtonCancel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 10), "escapePressed");
		
		JPanel flagspanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		m_oCheckBox_Show = new JCheckBox(Messages.getString("GUI_TEMPLATE_SELECTOR_SHOW_BOX"));
		m_oCheckBox_Show.setSelected(m_bDocumentShow);
		m_oCheckBox_Show.setToolTipText(Messages.getString("GUI_TEMPLATE_SELECTOR_SHOW_BOX_TOOLTIP"));
		m_oCheckBox_Show.addActionListener( this );
		
		m_oCheckBox_Print = new JCheckBox(Messages.getString("GUI_TEMPLATE_SELECTOR_PRINT_BOX"));
		m_oCheckBox_Print.setSelected(m_bDocumentPrint);
		m_oCheckBox_Print.setToolTipText(Messages.getString("GUI_TEMPLATE_SELECTOR_PRINT_BOX_TOOLTIP"));
		m_oCheckBox_Print.addActionListener( this );
		
		m_oCheckBox_SaveDoc = new JCheckBox(Messages.getString("GUI_TEMPLATE_SELECTOR_DOC_EXPORT_BOX"));
		m_oCheckBox_SaveDoc.setSelected(m_bDocumentSaveDoc);
		m_oCheckBox_SaveDoc.setToolTipText(Messages.getString("GUI_TEMPLATE_SELECTOR_DOC_EXPORT_BOX_TOOLTIP"));
		m_oCheckBox_SaveDoc.addActionListener( this );
		
		JPanel modePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		String[] modes = { Messages.getString("GUI_TEMPLATE_SELECTOR_SINGLE_MODE"), Messages.getString("GUI_TEMPLATE_SELECTOR_MULTI_MODE"), Messages.getString("GUI_TEMPLATE_SELECTOR_LABEL_MODE") };
		modeBox = new JComboBox(modes);
		modeBox.setSelectedIndex(m_iSelectedReplaceMode);
		modeBox.setToolTipText(Messages.getString("GUI_TEMPLATE_SELECTOR_MODE_BOX_TOOLTIP"));
		modeBox.addMouseWheelListener(new ComboBoxMouseWheelNavigator(modeBox));
		
		JLabel modeLabel = new JLabel(Messages.getString("GUI_TEMPLATE_SELECTOR_LABEL_REPLACE_MODE"));
		if(m_iNumDocuments == 1) modeLabel.setEnabled(false);
		
		modePanel.add(modeLabel);
		modePanel.add(modeBox);
		
		// Disable Mode-Selection-Box when there is only one address. (Modes only make sens fore more addresses)
		if(m_iNumDocuments == 1) modeBox.setEnabled(false);
		
		/*
		 
		 m_oCheckBox_Invisibility = new JCheckBox(Messages.getString("GUI_TEMPLATE_SELECTOR_INVISIBLE_BOX"));
		 m_oCheckBox_Invisibility.setBackground(m_oBGColor);
		 m_oCheckBox_Invisibility.setFont(m_oFont);
		 m_oCheckBox_Invisibility.setSelected(m_bDocumentInvisibility);
		 m_oCheckBox_Invisibility.setToolTipText(Messages.getString("GUI_TEMPLATE_SELECTOR_INVISIBLE_BOX_TOOLTIP"));
		 m_oCheckBox_Invisibility.addActionListener( this );
		 
		 */
		
		flagspanel.add(m_oCheckBox_Show);
		// If no printer configured, do not show printer-related elements
		if(printers.length != 0)
		{
			flagspanel.add(m_oCheckBox_Print);
			flagspanel.add(printerBox);
		}
		//flagspanel.add(m_oCheckBox_SavePDF);
		flagspanel.add(m_oCheckBox_SaveDoc);
		//flagspanel.add(m_oCheckBox_Invisibility);
		//flagspanel.add(modeBox);
		
		
		buttonpanel.add(m_oButtonOK, BorderLayout.WEST);
		buttonpanel.add(m_oButtonCancel, BorderLayout.EAST);
		buttonpanel.add(optionpanel, BorderLayout.CENTER);
		allPanel.add(flagspanel, BorderLayout.NORTH);
		allPanel.add(modePanel, BorderLayout.CENTER);
		allPanel.add(buttonpanel, BorderLayout.SOUTH);
		
		controlpanel.add(allPanel, BorderLayout.SOUTH);
		
		this.setControlButtonsAreShown(false);        
		
		m_oDialog.getContentPane().add(controlpanel, BorderLayout.SOUTH);   
		
		if(m_iNumDocuments == 1)
			m_oDialog.setTitle(Messages.getString("GUI_TEMPLATE_SELECTOR_TITLE_SINGLE"));
		else
			// Java 1.5: dialog.setTitle(Messages.getFormattedString("GUI_TEMPLATE_SELECTOR_TITLE_MULTI", Integer.valueOf(m_iNumDocuments)));
			m_oDialog.setTitle(Messages.getFormattedString("GUI_TEMPLATE_SELECTOR_TITLE_MULTI", new Integer(m_iNumDocuments)));
		m_oDialog.setSize(800, 400);
		m_oDialog.setLocationRelativeTo(null);
		
		
		
		m_oDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		m_oDialog.addWindowListener(this);
		
		checkOkButtonState();
		checkPrinterBoxState();
		
		m_oDialog.getRootPane().setDefaultButton(m_oButtonOK);
		
		m_oDialog.setVisible( true );
		return(m_oDialog);
	}
	
	
	/**
	 * enables the OK-Button if on of the action-options (show, print...) is selected 
	 *
	 */
	
	private void checkOkButtonState()
	{
		m_oButtonOK.setEnabled(oneOptionSelected());
	}
	
	private boolean oneOptionSelected()
	{
		return m_oCheckBox_Print.isSelected() /* || m_oCheckBox_SavePDF.isSelected() */ || m_oCheckBox_Show.isSelected() || m_oCheckBox_SaveDoc.isSelected();
	}
	
	/**
	 * enables the Printer-ComboBox if the checkbox print is selected
	 *
	 */
	
	private void checkPrinterBoxState()
	{
		printerBox.setEnabled(m_oCheckBox_Print.isSelected());
		//printerLabel.setEnabled(m_oCheckBox_Print.isSelected());
	}
	
	
	/**
	 * Returns wether the checkbox "Show" is selected or not
	 * 
	 * @return - true if the checkbox "Show" is selected
	 */
	
	public boolean getDocumentShow()
	{
		m_bDocumentShow = m_oCheckBox_Show.isSelected();
		return(m_bDocumentShow);
	}
	
	
	/**
	 * Returns wether the checkbox "Print" is selected or not
	 * 
	 * @return - true if the checkbox "Print" is selected
	 */
	
	public boolean getDocumentPrint()
	{
		m_bDocumentPrint = m_oCheckBox_Print.isSelected();
		return(m_bDocumentPrint);
	}
	
	
	public boolean getDocumentSaveDoc()
	{
		m_bDocumentSaveDoc = m_oCheckBox_SaveDoc.isSelected();
		return m_bDocumentSaveDoc;
	}
	
	public int getSelectedReplaceMode()
	{
		return modeBox.getSelectedIndex();
	}
	
	public int getCount()
	{
		return count;
	}
	
	public boolean getInvisibility()
	{
		return m_bDocumentInvisibility;
	}
	
	/**
	 * Is invoked when the user klicks the OK-Button
	 *
	 */
	
	private void okClicked()
	{
		//m_oDialog.toFront();
		// At least one option must be selected to proceed
		if(!oneOptionSelected()) return;
		
		int selectedOption = JOptionPane.YES_OPTION;  
		
		if(getSelectedFile() == null || !getSelectedFile().exists())
		{
			JOptionPane.showMessageDialog(this, Messages.getString("GUI_TEMPLATE_SELECTOR_NO_VALID_FILE_TEXT"), Messages.getString("GUI_TEMPLATE_SELECTOR_NO_VALID_FILE_TITLE"), JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		if(m_iNumDocuments > 10 && getDocumentShow() && getSelectedReplaceMode() == OfficeAutomat.REPLACE_MODE_MULTI)
		{
			Object[] options = {Messages.getString("GUI_TEMPLATE_SELECTOR_DOCS_SHOW_ALL_BUTTON"), Messages.getString("GUI_TEMPLATE_SELECTOR_DOCS_SHOW_FIVE_BUTTON"), Messages.getString("GUI_TEMPLATE_SELECTOR_DOCS_SINGLE_DOCUMENT_BUTTON"), Messages.getString("GUI_TEMPLATE_SELECTOR_DOCS_CANCEL_BUTTON")};
			selectedOption = JOptionPane.showOptionDialog(this, Messages.getString("GUI_TEMPLATE_SELECTOR_DOCS_WARNING_TEXT"), Messages.getString("GUI_TEMPLATE_SELECTOR_DOCS_WARNING_TITLE"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[2]);	
		}
		
		if(selectedOption == 0)
		{
			// Just proceed
		}
		else if(selectedOption == 1)
		{
			count = 5;
		}
		else if(selectedOption == 2)
		{
			count = -1;
			modeBox.setSelectedIndex(OfficeAutomat.REPLACE_MODE_SINGLE);
		}
		else return;
		
		m_iReturnCode = RETURNCODE_OK;
		
		m_oDialog.setVisible(false);
		m_oDialog.setModal(false);
	}
	
	
	/* The methods implementing the DialogListener-Interface */
	
	public void windowActivated(WindowEvent e) {}
	public void windowClosed(WindowEvent e) {}
	
	public void windowClosing(WindowEvent pEvent)
	{
		m_iReturnCode = RETURNCODE_CANCEL;      
		m_oDialog.setVisible(false);
		m_oDialog.setModal(false);
	}
	
	public void windowDeactivated(WindowEvent e) {}
	public void windowDeiconified(WindowEvent e){}
	public void windowIconified(WindowEvent e) {}
	public void windowOpened(WindowEvent e) {}    
	
	
	/* The method implementing the ActionListener-Interface */
	
	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		
		if(source.equals(m_oButtonCancel))
		{
			m_oDialog.removeWindowListener(this);
			
			m_iReturnCode = RETURNCODE_CANCEL;      
			m_oDialog.setVisible(false);
			m_oDialog.setModal(false);
		}
		else if(source.equals(m_oButtonOK))
		{
			okClicked();
		}
		else if(source.equals(m_oCheckBox_Invisibility))
		{
			m_bDocumentInvisibility = m_oCheckBox_Invisibility.isSelected();
		}
		else if(source.equals(m_oCheckBox_Show) || source.equals(m_oCheckBox_Print) /* || source.equals(m_oCheckBox_SavePDF) */ || source.equals(m_oCheckBox_SaveDoc) )
		{
			checkOkButtonState();
			
			if(source.equals(m_oCheckBox_Print))
			{
				checkPrinterBoxState();
			}
		}
	}
}