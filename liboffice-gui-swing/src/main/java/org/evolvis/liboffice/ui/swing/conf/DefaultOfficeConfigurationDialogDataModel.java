/**
 * 
 */
package org.evolvis.liboffice.ui.swing.conf;

import java.util.List;
import java.util.Map;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class DefaultOfficeConfigurationDialogDataModel implements OfficeConfigurationDialogDataModel
{
	public DefaultOfficeConfigurationDialogDataModel()
	{
		OfficeAutomat.getPrinterConfigurator().setup();
	}

	public List getAvailableOffices()
	{
		return OfficeAutomat.getOfficeConfigurator().getAvailableOffices();
	}

	public List getConnectionMethodsForSelectedOffice()
	{
		return OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getSupportedConnectionMethods();
	}

	public int getOfficeCount()
	{
		return getAvailableOffices().size();
	}

	public Map getParametersForSelectedConnectionMethod()
	{
		return OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getConnectionParameters();
	}

	public OfficeConnectionMethod getSelectedConnectionMethod()
	{
		return OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getConnectionMethod();
	}

	public OfficeConfiguration getSelectedOffice()
	{
		return OfficeAutomat.getOfficeConfigurator().getPreferredOffice();
	}

	public void fireConnectionMethodChanged()
	{
		
	}

	public void fireOfficeSelectionChanged()
	{
		
	}
}
