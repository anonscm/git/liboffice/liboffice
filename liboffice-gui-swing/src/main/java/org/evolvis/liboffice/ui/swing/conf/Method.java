/**
 * 
 */
package org.evolvis.liboffice.ui.swing.conf;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class Method
{
	public String name;
	public String tooltip;
	public String description;
	public String iconPath;
	
	public Method(String name, String tooltip, String description, String iconPath)
	{
		this.name = name;
		this.tooltip = tooltip;
		this.description = description;
		this.iconPath = iconPath;
	}
}
