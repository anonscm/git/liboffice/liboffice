/**
 * 
 */
package org.evolvis.liboffice.ui.swing.conf;

import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import de.tarent.commons.ui.CommonDialogButtons;
import de.tarent.commons.ui.EscapeDialog;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MethodChooseDialog extends EscapeDialog
{
	protected JPanel mainPanel;
	protected ActionListener actionListener;
	protected Method[] methods;
	
	protected int selected = -1;
	
	
	public MethodChooseDialog(Frame owner, Method[] methods)
	{
		super(owner, true);
		
		init(methods);
	}
	
	public MethodChooseDialog(JDialog owner, Method[] methods)
	{
		super(owner, true);
		
		init(methods);
	}
	
	public int getSelected()
	{
		return selected;
	}
	
	protected void init(Method[] pMethods)
	{
		this.methods = pMethods;
		
		getContentPane().add(getMainPanel());
		
		pack();
		
		setLocationRelativeTo(getOwner());
	}
	
	protected JPanel getMainPanel()
	{
		if(this.mainPanel == null)
		{
			FormLayout layout = new FormLayout(
					"pref:grow"); // columns //$NON-NLS-1$
	
			PanelBuilder builder = new PanelBuilder(layout);
			
			builder.setDefaultDialogBorder();
			
			CellConstraints cc = new CellConstraints();
			
			RowSpec spaceRowSpec = new RowSpec("10dlu"); //$NON-NLS-1$
			RowSpec rowSpec = new RowSpec("pref"); //$NON-NLS-1$
			
			for(int i=0; i < getMethods().length; i++)
			{
				if(i > 0)
					builder.appendRow(spaceRowSpec);
				
				builder.appendRow(rowSpec);
				
				builder.add(createMethodPanel(getMethods()[i].name, getMethods()[i].tooltip, getMethods()[i].description, getMethods()[i].iconPath, i), cc.xy(1, 1+i*2));
			}
			
			builder.appendRow(new RowSpec("10dlu:grow")); //$NON-NLS-1$
			
			builder.appendRow(rowSpec);
			
			builder.add(CommonDialogButtons.getCancelButton(getActionListener()), cc.xy(1, 1+getMethods().length*2));
			
			this.mainPanel = builder.getPanel();
		}
		return this.mainPanel;
	}
	
	protected JPanel createMethodPanel(String methodName, String methodTooltip, String methodDescription, String iconPath, int id)
	{
		FormLayout layout = new FormLayout(
				"pref, pref:grow, right:pref", // columns //$NON-NLS-1$
		"pref, 3dlu, pref, 5dlu, pref"); // rows //$NON-NLS-1$

		PanelBuilder builder = new PanelBuilder(layout);

		CellConstraints cc = new CellConstraints();
		
		builder.addSeparator(methodName, cc.xyw(1, 1, 3));
		builder.add(createPlainLabel(methodDescription), cc.xyw(1, 3, 3));
		builder.add(createMethodButton(methodName, methodTooltip, iconPath, id), cc.xy(1, 5));
		
		
		return builder.getPanel();
	}
	
	private JLabel createPlainLabel(String text)
	{
		JLabel plainLabel = new JLabel(text);
		plainLabel.setFont(plainLabel.getFont().deriveFont(Font.PLAIN));
		return plainLabel;
	}
	
	private JButton createMethodButton(String label, String tooltip, String iconPath, int id)
	{
		JButton methodButton = new JButton();
		//methodButton.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/doctor/ui/resources/gfx/go-next.png"))));
		methodButton.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource(iconPath))));
		methodButton.setToolTipText(tooltip);
		methodButton.setActionCommand("method_"+id);
		methodButton.addActionListener(getActionListener());
		
		return methodButton;
	}
	
	protected ActionListener getActionListener()
	{
		if(this.actionListener == null)
		{
			this.actionListener = new MethodChooseActionListener();
		}
		return this.actionListener;
	}
	
	protected Method[] getMethods()
	{
		if(this.methods == null)
		{
			this.methods = new Method[0];
		}
		return this.methods;
	}
	
	protected class MethodChooseActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getActionCommand().equals("cancel"))
			{
				closeWindow();
			}
			else if(event.getActionCommand().startsWith("method_"))
			{
				MethodChooseDialog.this.selected = Integer.parseInt(event.getActionCommand().substring(7));
				
				closeWindow();
			}
		}
	}
}
