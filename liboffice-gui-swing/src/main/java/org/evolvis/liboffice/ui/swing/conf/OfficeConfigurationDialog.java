/**
 * 
 */
package org.evolvis.liboffice.ui.swing.conf;

import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeConfigurator;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethodParameterDescription;
import org.evolvis.liboffice.filefilter.ExecutableFileFilter;
import org.evolvis.liboffice.ui.FirstRunWizard;
import org.evolvis.liboffice.ui.swing.Messages;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import de.tarent.commons.ui.ComboBoxMouseWheelNavigator;
import de.tarent.commons.ui.CommonDialogButtons;
import de.tarent.commons.ui.EscapeDialog;

/**
 * 
 * An graphical Swing-Dialog for manipulating the Office-Configuration 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */ 		
public class OfficeConfigurationDialog extends EscapeDialog
{
	private static final long serialVersionUID = 2589492124483648039L;
	private JToggleButton advancedOptionsButton;
	private JTextField officePathField;
	private JComboBox connMethodBox;
	private JPanel officePanel;
	private JPanel useOfficePanel;
	private JPanel additionalConnOptsPanel;
	protected JPanel additionalOptionsPanel;
	private JComboBox officeSelBox;

	private ImageIcon upIcon;
	private ImageIcon downIcon;

	Map additionalConnOpts;

	boolean advancedOptionsButtonSelected;
	
	protected JButton officePathBrowseButton;
	protected JButton manageOfficesButton;
	
	protected OfficeConfigurationDialogDataModel model;
	protected ActionListener actionListener;
	protected ItemListener itemListener;
	
	public OfficeConfigurationDialog(Frame owner, FirstRunWizard firstRunWizard, OfficeConfigurationDialogDataModel model)
	{
		super(owner, true);
		
		this.model = model;

		this.additionalConnOpts = new HashMap();

		setTitle(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_TITLE")); //$NON-NLS-1$

		getContentPane().add(getOfficePanel());

		setupAdvancedOptionsState();

		// Register Listeners
		addWindowListener(new OfficeConfigurationDialogWindowListener());

		pack();
		setLocationRelativeTo(owner);
	}

	public OfficeConfigurationDialog(Frame owner, FirstRunWizard firstRunWizard)
	{
		this(owner, firstRunWizard, null);
	}
	
	public OfficeConfigurationDialogDataModel getModel()
	{
		if(this.model == null)
			this.model = new DefaultOfficeConfigurationDialogDataModel();

		return this.model;
	}
	
	public void setModel(OfficeConfigurationDialogDataModel model)
	{
		this.model = model;
	}

	private JPanel getOfficePanel()
	{
		if(this.officePanel == null)
		{
			FormLayout layout = new FormLayout(
					"pref:grow", // columns //$NON-NLS-1$
			"10dlu, pref, 10dlu, pref, 10dlu:grow, pref"); // rows //$NON-NLS-1$

			PanelBuilder builder = new PanelBuilder(layout);

			builder.setDefaultDialogBorder();

			CellConstraints cc = new CellConstraints();

			builder.add(getUseOfficePanel(), cc.xy(1, 2));
			builder.add(getAdditionalOptionsPanel(), cc.xy(1, 4));
			builder.add(CommonDialogButtons.getSubmitCancelAndActionButtons(getActionListener(), getManageOfficesButton()), cc.xy(1, 6));

			this.officePanel = builder.getPanel();
		}

		return this.officePanel;
	}
	
	protected ActionListener getActionListener()
	{
		if(this.actionListener == null)
		{
			this.actionListener = new OfficeConfigurationDialogActionListener();
		}
		return this.actionListener;
	}
	
	protected ItemListener getItemListener()
	{
		if(this.itemListener == null)
		{
			this.itemListener = new OfficeConfigurationDialogItemListener();
		}
		return this.itemListener;
	}
	
	protected JPanel getAdditionalOptionsPanel()
	{
		if(this.additionalOptionsPanel == null)
		{
			FormLayout layout = new FormLayout(
					"5dlu, pref, 5dlu, pref:grow, 5dlu, pref", // columns //$NON-NLS-1$
			"pref, 3dlu, pref, 3dlu, pref, 10dlu, pref, 5dlu, pref, 3dlu, pref"); // rows //$NON-NLS-1$
	
			PanelBuilder builder = new PanelBuilder(layout);
	
			CellConstraints cc = new CellConstraints();
			
			builder.addSeparator(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_PATH"), cc.xyw(1, 1, 6)); //$NON-NLS-1$
			builder.add(createPlainFontLabel(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_PATH_DESCRIPTION")), cc.xyw(2, 3, 5)); //$NON-NLS-1$
			builder.add(getOfficePathField(), cc.xyw(2, 5, 3));
			builder.add(getOfficePathBrowseButton(), cc.xy(6, 5));
			
			builder.addSeparator(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_CONN_METHOD"), cc.xyw(1, 7, 6)); //$NON-NLS-1$
			builder.add(createPlainFontLabel(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_CONN_METHOD_DESCRIPTION")), cc.xyw(2, 9, 5)); //$NON-NLS-1$
			builder.add(getConnectionMethodBox(), cc.xy(2, 11));

			//builder.add(getAdditionalConnOptsPanel(), cc.xyw(2, 5, 5));
			
			this.additionalOptionsPanel = builder.getPanel();
		}
		return this.additionalOptionsPanel;
	}

	private JPanel getUseOfficePanel()
	{
		if(this.useOfficePanel == null)
		{
			FormLayout layout = new FormLayout(
					"left:pref, 5dlu, max(80dlu;pref):grow, 10dlu, 110dlu", // columns //$NON-NLS-1$
			"pref"); // rows //$NON-NLS-1$

			PanelBuilder builder = new PanelBuilder(layout);

			CellConstraints cc = new CellConstraints();

			builder.addLabel(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_USE_OFFICE"), cc.xy(1, 1)); //$NON-NLS-1$
			builder.add(getOfficeSelectionBox(), cc.xy(3, 1));
			builder.add(getAdvancedOptionsButton(), cc.xy(5, 1));

			this.useOfficePanel = builder.getPanel();
		}
		return this.useOfficePanel;
	}

	private JPanel getAdditionalConnOptsPanel()
	{
		if(this.additionalConnOptsPanel == null)
		{
			this.additionalConnOptsPanel = new JPanel();

			fillAdditionalConnOptsPanel();
		}
		return this.additionalConnOptsPanel;
	}

	private void fillAdditionalConnOptsPanel()
	{
		if(getModel().getSelectedOffice() != null)
		{
			List params =  getModel().getSelectedOffice().getConnectionMethod().getAdditionalParameters();
	
			FormLayout layout = new FormLayout(
					"5dlu, 80dlu, 5dlu, 80dlu, 5dlu, right:pref"); // columns //$NON-NLS-1$
			
			CellConstraints cc = new CellConstraints();
			
			this.additionalConnOptsPanel.setLayout(layout);
			
			RowSpec spaceRowSpec = new RowSpec("5dlu"); //$NON-NLS-1$
			RowSpec rowSpec = new RowSpec("pref"); //$NON-NLS-1$
	
			for(int i=0; i < params.size(); i++)
			{
				// add some space between the rows
				if(i > 0)
					layout.appendRow(spaceRowSpec);
				layout.appendRow(rowSpec);
				
				OfficeConnectionMethodParameterDescription desc = (OfficeConnectionMethodParameterDescription)params.get(i);
	
				JLabel label = new JLabel(desc.getName());
				label.setToolTipText(desc.getDescription());
				this.additionalConnOptsPanel.add(label, cc.xy(2, 1+i*2));
	
				if(desc.getType().equals(String.class))
				{
					JTextField textField = new JTextField();
					String value = (String)getModel().getSelectedOffice().getConnectionParameter(desc.getID());
					if(value != null && value.length() > 0)
						textField.setText(value);
					else 
						textField.setText((String)desc.getDefaultValue());
	
					textField.setToolTipText(desc.getDescription());
					this.additionalConnOpts.put(desc, textField);
					this.additionalConnOptsPanel.add(textField, cc.xy(4, 1+i*2));
				}
				else if(desc.getType().equals(Integer.class))
				{
					JSpinner spinner = new JSpinner();
					Integer value = (Integer)getModel().getSelectedOffice().getConnectionParameter(desc.getID());
	
					if(value != null)
						spinner.getModel().setValue(value);
					else
						spinner.getModel().setValue(desc.getDefaultValue());
	
					spinner.setToolTipText(desc.getDescription());
					this.additionalConnOpts.put(desc, spinner);
					this.additionalConnOptsPanel.add(spinner, cc.xy(4, 1+i*2));
				}
	
				this.additionalConnOptsPanel.add(getSetToDefaultButton(desc.getID(), desc.getName()), cc.xy(6, 1+i*2));
			}
		}
	}

	private JButton getSetToDefaultButton(String pID, String pParamName)
	{
		JButton setToDefaultButton = new JButton(Messages.getFormattedString("GUI_OFFICE_CONFIGURATION_DIALOG_RESET_PARAM", pParamName)); //$NON-NLS-1$
		setToDefaultButton.addActionListener(getActionListener());
		setToDefaultButton.setActionCommand("default_"+pID); //$NON-NLS-1$
		return setToDefaultButton;
	}

	JComboBox getOfficeSelectionBox()
	{
		if(this.officeSelBox == null)
		{
			this.officeSelBox = new JComboBox(getModel().getAvailableOffices().toArray());
			this.officeSelBox.setSelectedItem(getModel().getSelectedOffice());
			this.officeSelBox.addMouseWheelListener(new ComboBoxMouseWheelNavigator(this.officeSelBox));
			this.officeSelBox.addItemListener(getItemListener());
		}
		return this.officeSelBox;
	}

	JTextField getOfficePathField()
	{
		if(this.officePathField == null)
		{
			this.officePathField = new JTextField();

			if(getOfficeSelectionBox().getSelectedItem() != null)
				this.officePathField.setText(((OfficeConfiguration)getOfficeSelectionBox().getSelectedItem()).getPath());
			else
				this.officePathField.setEnabled(false);
		}
		return this.officePathField;
	}
	
	protected JButton getOfficePathBrowseButton()
	{
		if(this.officePathBrowseButton == null)
		{
			this.officePathBrowseButton = new JButton(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_PATH_BROWSE")); //$NON-NLS-1$
			this.officePathBrowseButton.addActionListener(getActionListener());
		}
		return this.officePathBrowseButton;
	}
	
	protected JButton getManageOfficesButton()
	{
		if(this.manageOfficesButton == null)
		{
			this.manageOfficesButton = new JButton(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_MANAGE_BUTTON")); //$NON-NLS-1$
			this.manageOfficesButton.setToolTipText(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_MANAGE_BUTTON_TOOLTIP")); //$NON-NLS-1$
			this.manageOfficesButton.addActionListener(getActionListener());
		}
		return this.manageOfficesButton;
	}

	JComboBox getConnectionMethodBox()
	{
		if(this.connMethodBox == null)
		{
			if(getModel().getSelectedOffice() != null && getModel().getSelectedOffice().getType().getSupportedConnectionMethods().size() > 0)
			{
				this.connMethodBox = new JComboBox(getModel().getSelectedOffice().getType().getSupportedConnectionMethods().toArray());
				chooseSelectedConnectionMethod();

			}
			else
			{
				this.connMethodBox = new JComboBox(new OfficeConnectionMethod[] { OfficeConnectionMethod.DEFAULT });
				this.connMethodBox.setEnabled(false);
			}
			this.connMethodBox.addMouseWheelListener(new ComboBoxMouseWheelNavigator(this.connMethodBox));
			this.connMethodBox.addItemListener(getItemListener());
		}
		return this.connMethodBox;
	}
	
	void chooseSelectedConnectionMethod()
	{
		for(int i=0; i < this.connMethodBox.getItemCount(); i++)
		{
			if(this.connMethodBox.getItemAt(i).getClass().equals(getModel().getSelectedOffice().getConnectionMethod().getClass()))
			{
				this.connMethodBox.setSelectedIndex(i);
				break;
			}
		}
	}

	private static JLabel createPlainFontLabel(String pText)
	{
		JLabel label = new JLabel(pText);
		label.setFont(label.getFont().deriveFont(Font.PLAIN));
		return label;
	}

	JToggleButton getAdvancedOptionsButton()
	{
		if(this.advancedOptionsButton == null)
		{
			this.advancedOptionsButton = new JToggleButton();
			this.advancedOptionsButton.addActionListener(getActionListener());
			this.advancedOptionsButton.setSelected(this.advancedOptionsButtonSelected);
			this.advancedOptionsButton.setHorizontalTextPosition(SwingConstants.LEFT);
		}
		return this.advancedOptionsButton;
	}

	void setupAdvancedOptionsState()
	{
		if(this.advancedOptionsButtonSelected)
		{
			getAdvancedOptionsButton().setText(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_HIDE_OPTIONS_BUTTON")); //$NON-NLS-1$
			getAdvancedOptionsButton().setIcon(getUpIcon());
		}
		else
		{
			getAdvancedOptionsButton().setText(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_SHOW_OPTIONS_BUTTON")); //$NON-NLS-1$
			getAdvancedOptionsButton().setIcon(getDownIcon());
		}

		getAdditionalOptionsPanel().setVisible(this.advancedOptionsButtonSelected);
		
		repaint();
		pack();
	}

	private ImageIcon getUpIcon()
	{
		if(this.upIcon == null)
		{
			this.upIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/doctor/ui/resources/gfx/expanded.gif"))); //$NON-NLS-1$
		}
		return this.upIcon;
	}

	private ImageIcon getDownIcon()
	{
		if(this.downIcon == null)
		{
			this.downIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/doctor/ui/resources/gfx/collapsed.gif")));	 //$NON-NLS-1$
		}
		return this.downIcon;
	}

	public void setConnectionParameters()
	{
		Iterator it = this.additionalConnOpts.keySet().iterator();

		while(it.hasNext())
		{
			OfficeConnectionMethodParameterDescription desc = (OfficeConnectionMethodParameterDescription)it.next();
			Object value = null;

			if(this.additionalConnOpts.get(desc).getClass().equals(JTextField.class))
				value = ((JTextField)this.additionalConnOpts.get(desc)).getText();
			else if(this.additionalConnOpts.get(desc).getClass().equals(JSpinner.class))
				value = ((JSpinner)this.additionalConnOpts.get(desc)).getValue();

			getModel().getSelectedOffice().setConnectionParameter(desc, value);							
		}
	}

	void rebuildParameterPanel()
	{
		// remove all old components
		getAdditionalConnOptsPanel().removeAll();
		this.additionalConnOpts.clear();

		// rebuild
		fillAdditionalConnOptsPanel();
		
		pack();

		this.repaint();
	}

	public static void main(String [] args)
	{
		OfficeAutomat.getOfficeConfigurator().loadConfiguration(null);
		new OfficeConfigurationDialog(null, null).setVisible(true);
	}
	
	//	 Overides de.tarent.commons.ui.EscapeDialog.closeWindow()
	public void closeWindow()
	{
		cancelAndClose();

		super.closeWindow();
	}
	
	protected class OfficeConfigurationDialogWindowListener extends WindowAdapter
	{
		public void windowClosing(WindowEvent e)
		{
			OfficeConfigurationDialog.this.closeWindow();
		}
	}
	
	protected void cancelAndClose()
	{
		dispose();
		super.closeWindow();
	}
	
	protected void saveAndClose()
	{
		setConnectionParameters();
		if(getModel().getSelectedOffice() != null)
		{
			getModel().getSelectedOffice().setPath(getOfficePathField().getText().trim());
			getModel().getSelectedOffice().setConnectionMethod((OfficeConnectionMethod)getConnectionMethodBox().getSelectedItem());
		}
		OfficeAutomat.getOfficeConfigurator().saveConfiguration(null);
		OfficeAutomat.getInstance().dispose();
		dispose();
		
		super.closeWindow();
	}
	
	protected void fireOfficeDataChanged()
	{
		((DefaultComboBoxModel)getOfficeSelectionBox().getModel()).removeAllElements();
		Iterator it = OfficeAutomat.getOfficeConfigurator().getAvailableOffices().iterator();
		while(it.hasNext())
			((DefaultComboBoxModel)getOfficeSelectionBox().getModel()).addElement(it.next());
		
		getOfficeSelectionBox().setSelectedItem(OfficeAutomat.getOfficeConfigurator().getPreferredOffice());
		
		fireOfficeSelectionChanged();
	}
	
	protected void fireOfficeSelectionChanged()
	{
		getModel().fireOfficeSelectionChanged();
		
		setConnectionParameters();

		OfficeAutomat.getOfficeConfigurator().setPreferredOffice(((OfficeConfiguration)getOfficeSelectionBox().getSelectedItem()));
		if(getModel().getSelectedOffice() != null)
		{
			getOfficePathField().setText(getModel().getSelectedOffice().getPath());
			chooseSelectedConnectionMethod();
		}

		rebuildParameterPanel();
	}
	
	protected void fireConnectionMethodChanged()
	{
		getModel().fireConnectionMethodChanged();
		
		setConnectionParameters();
		rebuildParameterPanel();
		OfficeAutomat.getOfficeConfigurator().getPreferredOffice().setConnectionMethod((OfficeConnectionMethod)getConnectionMethodBox().getSelectedItem());
	}
	
	protected class OfficeConfigurationDialogItemListener implements ItemListener
	{
		public void itemStateChanged(ItemEvent pEvent)
		{
			if(pEvent.getSource().equals(getOfficeSelectionBox()))
				fireOfficeSelectionChanged();
			else if(pEvent.getSource().equals(getConnectionMethodBox()))
				fireConnectionMethodChanged();
		}
	}
	
	protected class OfficeConfigurationDialogActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource().equals(getAdvancedOptionsButton()))
			{
				OfficeConfigurationDialog.this.advancedOptionsButtonSelected = !OfficeConfigurationDialog.this.advancedOptionsButtonSelected;
				setupAdvancedOptionsState();
			}
			else if(event.getSource().equals(getManageOfficesButton()))
			{
				Method[] methods = new Method[3];
				methods[0] = new Method(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_AUTO"), Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_AUTO_TOOLTIP"), Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_AUTO_DESCRIPTION"), "/de/tarent/doctor/ui/resources/gfx/adept_reinstall.png"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				methods[1] = new Method(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_ADD_OFFICE"), Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_ADD_OFFICE_TOOLTIP"), Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_ADD_OFFICE_DESC"), "/de/tarent/doctor/ui/resources/gfx/list-add.png");  //$NON-NLS-1$ //$NON-NLS-2$//$NON-NLS-3$ //$NON-NLS-4$
				methods[2] = new Method(Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_REMOVE_OFFICE"), Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_REMOVE_OFFICE_TOOLTIP"), Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_REMOVE_OFFICE_DESC"), "/de/tarent/doctor/ui/resources/gfx/list-remove.png");   //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$ //$NON-NLS-4$
				MethodChooseDialog dialog = new MethodChooseDialog(OfficeConfigurationDialog.this, methods);
				// will block until user makes a decision
				dialog.setVisible(true);
				
				int selected = dialog.getSelected();
				
				switch(selected)
				{
					case -1: return;
					
					case 0:
					{
						OfficeAutomat.getOfficeConfigurator().autoInit();
						fireOfficeDataChanged();
						return;
					}
					case 1:
					{
						FirstRunWizard wizard = new FirstRunWizardSwing(OfficeConfigurationDialog.this);
						OfficeConfiguration conf = wizard.manualInputDialog();
						if(conf != null)
							OfficeAutomat.getOfficeConfigurator().addOfficeConfiguration(wizard.manualInputDialog());
						fireOfficeDataChanged();
						return;
					}
					case 2:
					{
						OfficeConfiguration conf = (OfficeConfiguration)JOptionPane.showInputDialog(OfficeConfigurationDialog.this, Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_DELETE_CONF"), Messages.getString("GUI_OFFICE_CONFIGURATION_DIALOG_DELETE_CONF_TITLE"), JOptionPane.QUESTION_MESSAGE, null, getModel().getAvailableOffices().toArray(), null);
						if(conf != null)
						{
							OfficeAutomat.getOfficeConfigurator().removeOfficeConfiguration(conf);
							fireOfficeDataChanged();
						}
					}
				}
			}
			else if(event.getSource().equals(getOfficePathBrowseButton()))
			{
				MessageFileChooser fileChooser = new MessageFileChooser(OfficeConfigurator.getProgramDir(), Messages.getString("GUI_FIRST_RUN_MANUAL_INPUT_EXEC_TEXT"));	 //$NON-NLS-1$
				fileChooser.setFileFilter(new ExecutableFileFilter(getModel().getSelectedOffice().getType()));
				
				boolean valid = false;
				
				while(!valid)
				{
					int returnValue = fileChooser.showDialog(OfficeConfigurationDialog.this, Messages.getString("GUI_FIRST_RUN_MANUAL_INPUT_EXEC_TITLE")); //$NON-NLS-1$
					
					if(returnValue != JFileChooser.APPROVE_OPTION) return;
					
					String path = fileChooser.getSelectedFile().toString();
					
					if(path.indexOf(getModel().getSelectedOffice().getType().getExec()) != -1)
					{
						getModel().getSelectedOffice().setPath(path.substring(0, path.lastIndexOf(getModel().getSelectedOffice().getType().getExec())));
						OfficeConfigurationDialog.this.getOfficePathField().setText(getModel().getSelectedOffice().getPath());
						valid = true;
					}
					else
					{
						JOptionPane.showMessageDialog(OfficeConfigurationDialog.this, Messages.getFormattedString("GUI_FIRST_RUN_MANUAL_INPUT_EXEC_NOT_VALID", getModel().getSelectedOffice().getType().getExec())); //$NON-NLS-1$
					}
				}
			}
			else if(event.getActionCommand().startsWith("default_")) //$NON-NLS-1$
			{
				// reset applying parameter to default-value
				Iterator it = OfficeConfigurationDialog.this.additionalConnOpts.keySet().iterator();
				while(it.hasNext())
				{
					OfficeConnectionMethodParameterDescription desc = (OfficeConnectionMethodParameterDescription)it.next();

					String id = event.getActionCommand().substring(8);
					if(desc.getID().equals(id))
					{
						if(OfficeConfigurationDialog.this.additionalConnOpts.get(desc).getClass().equals(JTextField.class))
							((JTextField)OfficeConfigurationDialog.this.additionalConnOpts.get(desc)).setText((String)desc.getDefaultValue());
						else if(OfficeConfigurationDialog.this.additionalConnOpts.get(desc).getClass().equals(JSpinner.class))
							((JSpinner)OfficeConfigurationDialog.this.additionalConnOpts.get(desc)).setValue(desc.getDefaultValue());
					}
				}
			}
			else if(event.getActionCommand().equals("submit")) //$NON-NLS-1$
			{
				saveAndClose();
			}
			else if(event.getActionCommand().equals("cancel")) //$NON-NLS-1$
			{
				cancelAndClose();
			}
		}		
	}
}
