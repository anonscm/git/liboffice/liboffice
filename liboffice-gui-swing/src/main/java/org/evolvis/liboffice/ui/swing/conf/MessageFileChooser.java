/**
 * 
 */
package org.evolvis.liboffice.ui.swing.conf;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.FormLayout;

/**
 * 
 * Simple subclass of JFileChooser which in addition has a JLabel at the top 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MessageFileChooser extends JFileChooser
{
	public final static long serialVersionUID = 233214343;
	private String message;
	
	public MessageFileChooser(String pPath, String pMessage)
	{
		super(pPath);
		//LookAndFeelTool.setupLaF();
		message = pMessage;
	}
	
	public JDialog createDialog(Component pParent)
	{
		JDialog dialog = super.createDialog(pParent);
		
		FormLayout layout = new FormLayout(
				"pref", // columns
		"pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);

		builder.setDefaultDialogBorder();
		
		JLabel label = builder.addLabel(message);
		label.setFont(label.getFont().deriveFont(15f));
			
		dialog.getContentPane().add(builder.getPanel(), BorderLayout.NORTH);
		
		return dialog;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public void setMessage(String pMessage)
	{
		message = pMessage;
	}
}