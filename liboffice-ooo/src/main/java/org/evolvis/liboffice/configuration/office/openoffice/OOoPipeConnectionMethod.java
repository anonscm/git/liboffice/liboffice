/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.office.openoffice;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethodParameterDescription;
import org.evolvis.liboffice.ui.Messages;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public final class OOoPipeConnectionMethod extends OfficeConnectionMethod
{
	public String getID()
	{
		return "pipe";
	}
	
	public String getDescription()
	{
		return Messages.getString("CONNECTION_METHOD_PIPE_DESC");
	}

	public String getName()
	{
		return Messages.getString("CONNECTION_METHOD_PIPE_NAME");
	}
	
	public List getAdditionalParameters()
	{
		List params = new ArrayList();
		
		OfficeConnectionMethodParameterDescription pipeNameParam = new OfficeConnectionMethodParameterDescription("pipe-name", "Pipe-Name", "Bla bla bla bla..", String.class, "doctopus_Office");
		params.add(pipeNameParam);
		
		return params;
	}
}
