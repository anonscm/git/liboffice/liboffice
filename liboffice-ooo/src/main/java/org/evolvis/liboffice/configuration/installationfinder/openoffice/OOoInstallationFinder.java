/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.installationfinder.openoffice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.liboffice.configuration.installationfinder.AbstractInstallationFinder;
import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.configuration.parser.PlainOfficeConfigurationParser;
import org.evolvis.liboffice.utils.SystemInfo;


/**
 * This class is an implementation of <code>AbstractInstallationFinder</code> which provides methods for getting OpenOffice.org/StarOffice-Installations
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoInstallationFinder extends AbstractInstallationFinder
{
	private final static String bootstrapFilePathUnix = "program"+File.separator+"bootstraprc";
	private final static String bootstrapFilePathWindows = "program"+File.separator+"bootstrap.ini";
	//private final static String versionFilePathUnix = "program/versionrc";
	//private final static String versionFilePathWindows = "program/version.ini";
	
	//private static final String UNO_PATH_PROPERTY_NAME = "com.sun.star.lib.loader.unopath";
    //private static final String UNO_PATH_ENVIRONMENT_VARIABLE = "UNO_PATH";
    private static final String EXEC = "soffice";
	
	private static Logger logger = Logger.getLogger(OOoInstallationFinder.class.getName());
	
	// Java 1.5: public List<OfficeConfiguration> getConfigurations()
	public List getConfigurations()
	{
		List configs = new ArrayList();
		
		Iterator pathsIt = getPaths().iterator();
		
		while(pathsIt.hasNext())
		{
			String path = (String)pathsIt.next();
			
			if(path != null && path.length() > 0)
			{
				OfficeConfiguration conf = new OfficeConfiguration();
				conf.setPath(path);
				
				String productKey = getProductKey(path);
				String officeName = null;
				String officeVersionName = null;
				
				if(productKey != null)
				{
					officeName = productKey.substring(0, productKey.indexOf(' '));
					officeVersionName = productKey.substring(productKey.indexOf(' ')+1, productKey.length());
					OfficeType type = PlainOfficeConfigurationParser.getInstance().getOfficeTypeByName(officeName, officeVersionName);
					
					if(type != null)
					{
						conf.setType(type);
						//conf.setChecksum(checksum(conf.getPath()+type.getExec()));
						
						// Add this configuration to the return list
						configs.add(conf);
					}
					else
					{
						if(logger.isLoggable(Level.WARNING)) logger.warning("Found unknown Office-System: "+officeName+" "+officeVersionName+" in "+path);
					}
				}
				else
				{
					/* TODO userinteraction */
					if(logger.isLoggable(Level.WARNING)) logger.warning("Found unknown Office-System in "+path);
				}
			}
		}
		
		return configs;
	}
	
	private List getPaths()
	{
		List paths = new ArrayList();
		
		if(SystemInfo.isWindowsSystem())
		{
			getPathsFromWindowsRegistry(paths);
		}
		else
		{
			getPathsFromPath(paths);
			getPathsFromCommonInstallationDirectories(paths);
		}
		return paths;
	}
	
	private void getPathsFromPath(List pListToAddTo)
	{
		String path = null;
		
		Runtime rt = Runtime.getRuntime();
		try
		{
			Process p = rt.exec("which "+EXEC);
			BufferedReader stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String output = stdout.readLine();
			if(output != null && output.indexOf(EXEC) != -1)
			{
				path = output.substring(0, output.indexOf(EXEC));
				File file = new File(path);
				if(file.exists())
				{
					// resolve symlink
					path = file.getCanonicalFile().getParent();
					
					if(logger.isLoggable(Level.INFO)) logger.info("executable: "+EXEC+" found in path: "+path);
					
					// Add to list with paths
					if(path != null && path.length() > 0) pListToAddTo.add(path);
				}
			}
		}
		catch (Exception pExcp)
		{
			
		}
	}
   
    private void getPathsFromWindowsRegistry(List pListToAddTo)
    {
    	String key = "Software\\OpenOffice.org\\UNO\\InstallPath";
    	
    	String path = null;
    	
    	throw new UnsupportedOperationException("This method is currently disfunctional");
    	
    	// Lookup registry
//		RegistryKey r = new RegistryKey(RootKey.HKEY_CURRENT_USER, key);
		
//		if(r.exists())
//		{
//			path = r.getValue("").getData().toString();
//			path = path.substring(0, path.lastIndexOf("program"));
//			
//			if(path != null && path.length() > 0)
//			{
//				pListToAddTo.add(path);
//				
//				if(logger.isLoggable(Level.INFO)) logger.info("executable: "+EXEC+" found in path: "+path+" by HKEY_CURRENT_USER");
//			}
//		}
//		else
//		{
//			r = new RegistryKey(RootKey.HKEY_LOCAL_MACHINE, key);
//			if(r.exists())
//			{
//				path = r.getValue("").getData().toString();
//				path = path.substring(0, path.lastIndexOf("program"));
//				if(path != null && path.length() > 0)
//				{
//					pListToAddTo.add(path);
//					
//					if(logger.isLoggable(Level.INFO)) logger.info("executable: "+EXEC+" found in path: "+path+" by HKEY_LOCAL_MACHINE");
//				}
//			}
//		}
    }
    
    public void getPathsFromCommonInstallationDirectories(List pListToAddTo)
    {
    	List directories = PlainOfficeConfigurationParser.getInstance().getCommonOOoInstallationDirectories();
    	
    	if(directories != null)
    	{
    		Iterator it = directories.iterator();
    		
    		while(it.hasNext())
    		{
    			String path = (String)it.next();
    			
    			String productKey = getProductKey(path);
    			String officeName = null;
    			String officeVersionName = null;
    			
    			if(productKey != null)
    			{
    				officeName = productKey.substring(0, productKey.indexOf(' '));
    				officeVersionName = productKey.substring(productKey.indexOf(' ')+1, productKey.length());
    				OfficeType type = PlainOfficeConfigurationParser.getInstance().getOfficeTypeByName(officeName, officeVersionName);
    				
    				if(type != null)
    				{
    					if(new File(path+type.getExec()).exists())
    					{
							// Add this configuration to the return list
							pListToAddTo.add(path);
    					}
    				}
    			}   			
    		}
    	}
    }
    
    
	private String getProductKey(String pPath)
	{
		BufferedReader in;
		
		try
		{
			if(SystemInfo.isWindowsSystem())
			{
				in = new BufferedReader(new InputStreamReader(new FileInputStream(pPath+bootstrapFilePathWindows)));
			}
			else
			{
				in = new BufferedReader(new InputStreamReader(new FileInputStream(pPath+bootstrapFilePathUnix)));
			}
			
			while(in.ready())
			{
				String line = in.readLine();
				if(line.startsWith("ProductKey"))
				{
					return line.substring(line.indexOf("=")+1);
				}
			}
		}
		catch(FileNotFoundException pExcp)
		{
			return null;
		}
		catch(IOException pExcp)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("Got IO-Error when trying to read OpenOffice.org Bootstrap-File!");
		}
		return null;
	}
}