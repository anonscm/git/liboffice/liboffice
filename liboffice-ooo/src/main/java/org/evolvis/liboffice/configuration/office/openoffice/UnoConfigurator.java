/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.configuration.office.openoffice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.evolvis.liboffice.configuration.office.ConfigurationAction;
import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class UnoConfigurator implements ConfigurationAction
{
    
    private static final String relativePathToConf = File.separator+"registry"+File.separator+"data"+File.separator+"org"+File.separator+"openoffice"+File.separator+"Setup.xcu";
    private static final String relativePathToUserConf = File.separator + "user" + relativePathToConf;
    private static final String relativePathToGlobalConf = File.separator + "share" + relativePathToConf;
    
    private static Logger logger = Logger.getLogger(UnoConfigurator.class.getName());
    
    public UnoConfigurator()
    {
    	
    }
    
    
    
    public int process(OfficeConfiguration pConf)
    {	
    	if(pConf.getConnectionMethod().getID().equals("socket"))
    	{
    		logger.info("Preparing "+pConf.getType().getName()+" "+pConf.getType().getDisplayVersion()+" for socket-connection on port "+ (Integer)pConf.getConnectionParameter("socket-port")+"...");
    		
    		if(!setUnoConnectionTypeToLocalhostSocket(new File(pConf.getPath()+relativePathToGlobalConf), (Integer)pConf.getConnectionParameter("socket-port")))
    			if(!setUnoConnectionTypeToLocalhostSocket(new File(System.getProperty("user.home")+File.separator+".openoffice.org2"+relativePathToUserConf), (Integer)pConf.getConnectionParameter("socket-port")))
    				return ConfigurationAction.RETURN_ERROR;
    	}
    	else if(pConf.getConnectionMethod().getID().equals("pipe"))
    	{
    		logger.info("Preparing "+pConf.getType().getName()+" "+pConf.getType().getDisplayVersion()+" for named-pipe-connection with pipe-name "+(String)pConf.getConnectionParameter("pipe-name")+"...");
    		if(!setUnoConnectionTypeToPipe(new File(pConf.getPath()+relativePathToGlobalConf), (String)pConf.getConnectionParameter("pipe-name")))
    			if(!setUnoConnectionTypeToPipe(new File(System.getProperty("user.home")+File.separator+".openoffice.org2"+relativePathToUserConf), (String)pConf.getConnectionParameter("pipe-name")))
    				return ConfigurationAction.RETURN_ERROR;
    	}
    	else
		{
    		logger.warning("Unknown connection-method set. using named-pipe");
    		logger.info("Preparing "+pConf.getType().getName()+" "+pConf.getType().getDisplayVersion()+" for named-pipe-connection...");
    		if(!setUnoConnectionTypeToPipe(new File(pConf.getPath()+relativePathToGlobalConf), (String)pConf.getConnectionParameter("pipe-name")))
    			if(!setUnoConnectionTypeToPipe(new File(System.getProperty("user.home")+File.separator+".openoffice.org2"+relativePathToUserConf), (String)pConf.getConnectionParameter("pipe-name")))
    				return ConfigurationAction.RETURN_ERROR;
		}
    	return ConfigurationAction.RETURN_SUCCESS;
    }
    
    public static boolean setUnoConnectionTypeToSocket(File pFile, String pHostname, Integer pPort)
    {
    	return setUnoConnectionType(pFile, "socket,host="+pHostname+",port="+pPort+";urp;StarOffice.ServiceManager");
    }
    
    public static boolean setUnoConnectionTypeToLocalhostSocket(File pFile, Integer pPort)
    {
    	return setUnoConnectionTypeToSocket(pFile, "127.0.0.1", pPort); 
    }
    
    public static boolean setUnoConnectionTypeToPipe(File pFile, String pPipeName)
    {
    	return setUnoConnectionType(pFile, "pipe,name="+pPipeName+";urp;StarOffice.ServiceManager");
    }
    
    public static boolean setUnoConnectionType(File pFile, String pNewValue)
    {
        if (!pFile.canWrite())
        {
        	logger.warning("Cannot write to the given file "+pFile.getAbsolutePath());
            return false;
        }
        
        try
        {
            DocumentBuilderFactory factory    = DocumentBuilderFactory.newInstance();
            //factory.setValidating( false );
            //factory.setNamespaceAware( false );
            DocumentBuilder builder     = factory.newDocumentBuilder();
            
            Document doc                     =  builder.parse( pFile );
            
            //NodeList nl                    = XPathAPI.selectNodeList(doc, "//*[@name='ooSetupConnectionURL']");
            boolean found                    = false;
            NodeList nl                      = doc.getElementsByTagName("prop");
            if (nl.getLength() > 0)
            {
                for (int i=0; i < nl.getLength(); i++)
                {
                    Element n   =  (Element)nl.item( i );
                    if ("ooSetupConnectionURL".equals(n.getAttribute("oor:name")))
                    {
                        NodeList nlValues =   n.getElementsByTagName("value");
                        if (nlValues.getLength() > 0)
                        {
                            Element elemValue = (Element)nlValues.item(0);
                            for (int j =0; j < elemValue.getChildNodes().getLength(); j++)
                            {
                                Node nText =  elemValue.getChildNodes().item(j);
                                if (nText.getNodeType() == Node.TEXT_NODE)
                                {
                                     nText.setNodeValue( pNewValue );  
                                }
                            }
                        }
                        found = true;
                        break;
                    }
                }
            }
            
            if (!found)
            {
                NodeList nlOffice                     = doc.getElementsByTagName("node");
                //NodeList nlOffice                            = XPathAPI.selectNodeList(doc, "//*[@name='Office']");
                if (nlOffice.getLength() > 0)
                {
                    for (int i=0; i < nlOffice.getLength(); i++)
                    {
                        Element elem = (Element)nlOffice.item( i );
                        
                        logger.fine(("search node "+elem.getAttribute("oor:name")));
                        
                        if ("Office".equals(elem.getAttribute("oor:name")))
                        {
                            Element elemInsert = doc.createElement("prop");
                            elemInsert.setAttribute("oor:name", "ooSetupConnectionURL");
                            elemInsert.setAttribute("oor:type", "xs:string");
                            Element elemValue = doc.createElement("value");
                            elemValue.appendChild( doc.createTextNode(pNewValue));
                            elemInsert.appendChild( elemValue );
                            elem.appendChild( elemInsert );
                            break;
                        }
                    }
                }
            }
            
            OutputStream out = new FileOutputStream(pFile);
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult( out ));
            return true;
        }
        catch(FileNotFoundException f)
        {
        	logger.warning("Error when writing document: "+f.getMessage());
        }
        catch (Exception pExcp)
        {
        	logger.warning("Error when parsing document: "+pExcp.getMessage());
        }
        return false;
    }
    
}
