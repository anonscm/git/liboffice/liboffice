/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.printer.openoffice;

import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.ServiceUIFactory;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintServiceAttribute;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.event.PrintServiceAttributeListener;

import org.evolvis.liboffice.configuration.printer.AbstractPrinter;


/**
 * A dummy PrintService Class for printers configured in OpenOffice.org. The current implementation only provides a name.
 * This basic implementation is sufficient for normal print-jobs and can be expanded if required.
 * 
 * 
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoPrinter extends AbstractPrinter
{
	String name;

	public OOoPrinter(String pName)
	{
		name = pName;
	}

	public void addPrintServiceAttributeListener(PrintServiceAttributeListener arg0)
	{
		// TODO Auto-generated method stub
	}

	public DocPrintJob createPrintJob()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public PrintServiceAttribute getAttribute(Class arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public PrintServiceAttributeSet getAttributes()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Object getDefaultAttributeValue(Class arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public String getName()
	{
		return name;
	}

	public ServiceUIFactory getServiceUIFactory()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Class[] getSupportedAttributeCategories()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Object getSupportedAttributeValues(Class arg0, DocFlavor arg1, AttributeSet arg2)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public DocFlavor[] getSupportedDocFlavors()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public AttributeSet getUnsupportedAttributes(DocFlavor arg0, AttributeSet arg1)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isAttributeCategorySupported(Class arg0)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isAttributeValueSupported(Attribute arg0, DocFlavor arg1, AttributeSet arg2)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isDocFlavorSupported(DocFlavor arg0)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public void removePrintServiceAttributeListener(PrintServiceAttributeListener arg0)
	{
		// TODO Auto-generated method stub
	}
	
}
