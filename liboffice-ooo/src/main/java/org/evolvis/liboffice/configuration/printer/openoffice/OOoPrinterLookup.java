/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.printer.openoffice;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.liboffice.configuration.PreferenceList;
import org.evolvis.liboffice.configuration.printer.AbstractPrinter;


/**
 * 
 * This class works quite similar to the PrintServiceLookup-class in javax.print but only returns the printers
 * configured in OpenOffice.org.
 * 
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoPrinterLookup
{
	/* TODO Does an UNO-command exist for this request? */ 
	public final static String USER_PSPRINT_FILE_PATH = System.getProperty("user.home")+"/.openoffice.org2/user/psprint/psprint.conf";
	public final static String SYSTEM_PSPRINT_FILE_PATH = "/etc/openoffice/psprint.conf";
	
	private static Logger logger = Logger.getLogger(OOoPrinterLookup.class.getName());

	/**
	 * Locates print services configured in OpenOffice.org
	 * 
	 * @return array of AbstractPrinters configured in OpenOffice.org. If no PrintService configured, the array is zero-length.
	 */
	
	public static AbstractPrinter[] lookupAllPrintersAsArray()
	{
		return (AbstractPrinter[]) lookupAllPrintersAsList().toArray();
	}
	
	public static AbstractPrinter[] lookupUserPrintersAsArray()
	{
		return (AbstractPrinter[]) makeAbstractPrinterObjectsOfNames(parseUserConf()).toArray();
	}
	
	public static AbstractPrinter[] lookupSystemPrintersAsArray()
	{
		return (AbstractPrinter[]) makeAbstractPrinterObjectsOfNames(parseSystemConf()).toArray();
	}
	
	private static PreferenceList makeAbstractPrinterObjectsOfNames(List pPrinterNames)
	{
		PreferenceList printerList = new PreferenceList();

		Iterator it = pPrinterNames.iterator();
		
		while(it.hasNext())
			printerList.add(new OOoPrinter(it.next().toString()));
		
		return printerList;
	}
	
	/**
	 * Locates print services configured in OpenOffice.org
	 * 
	 * @return List of AbstractPrinters configured in OpenOffice.org. If no PrintService configured, the List is zero-size.
	 */
	
	public static PreferenceList lookupAllPrintersAsList()
	{
		PreferenceList printerList = makeAbstractPrinterObjectsOfNames(parseUserConf());
		printerList.addAll(makeAbstractPrinterObjectsOfNames(parseSystemConf()));
		
		return printerList;
	}
	
	private static List parseUserConf()
	{
		return parseFile(USER_PSPRINT_FILE_PATH);
	}
	
	private static List parseSystemConf()
	{
		return parseFile(SYSTEM_PSPRINT_FILE_PATH);
	}
	
	private static List parseFile(String pFileName)
	{
		List printServiceNames = new ArrayList();
		try
		{
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(pFileName)));
			while(in.ready())
			{
				String line = in.readLine();
				// [Generic Printer] and [__Global_Printer_Defaults__] are some standard entries and do not represent existing printers. So leave them out here
				if(line.startsWith("[") && line.endsWith("]") && !line.equals("[Generic Printer]") && !line.equals("[__Global_Printer_Defaults__]"))
				{
					printServiceNames.add(line.substring(1, line.length()-1));
				}
			}
		}
		catch(FileNotFoundException pExcp)
		{
			if(logger.isLoggable(Level.INFO)) logger.info("OpenOffice.org Printer-Configuration-File "+pFileName+" not found");
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
		}
		return printServiceNames;
	}
}