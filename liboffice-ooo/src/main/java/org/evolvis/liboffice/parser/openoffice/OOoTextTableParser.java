/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.openoffice;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextFrame;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextFrame;
import org.evolvis.liboffice.document.openoffice.OOoTextTable;
import org.evolvis.liboffice.parser.TextTableCellParser;
import org.evolvis.liboffice.parser.TextTableParser;
import org.evolvis.liboffice.virtualdocument.VirtualMargin;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;

import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.container.XNamed;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.table.XTableColumns;
import com.sun.star.table.XTableRows;
import com.sun.star.text.HoriOrientation;
import com.sun.star.text.TableColumnSeparator;
import com.sun.star.text.TextContentAnchorType;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextFrame;
import com.sun.star.text.XTextFramesSupplier;
import com.sun.star.text.XTextTable;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextTableParser implements TextTableParser
{
  private OOoTextDocumentParser m_oOOoTextDocumentParser = null;
  private final static Map charsToDigit                  = new HashMap();
  private Logger logger                                  = Logger.getLogger(getClass());
  private TextTableCellParser m_oTextTableCellParser     = null;
  
  public OOoTextTableParser(OOoTextDocumentParser documentparser)
  {
    m_oOOoTextDocumentParser = documentparser;
    charsToDigit.put("A",new Integer(1));
    charsToDigit.put("B",new Integer(2));
    charsToDigit.put("C",new Integer(3));
    charsToDigit.put("D",new Integer(4));
    charsToDigit.put("E",new Integer(5));
    charsToDigit.put("F",new Integer(6));
    charsToDigit.put("G",new Integer(7));
    charsToDigit.put("H",new Integer(8));
    charsToDigit.put("I",new Integer(9));
    charsToDigit.put("J",new Integer(10));
    charsToDigit.put("K",new Integer(11));
    charsToDigit.put("L",new Integer(12));
    charsToDigit.put("M",new Integer(13));
    charsToDigit.put("N",new Integer(14));
    charsToDigit.put("O",new Integer(15));
    charsToDigit.put("P",new Integer(16));
    charsToDigit.put("Q",new Integer(17));
    charsToDigit.put("R",new Integer(18));
    charsToDigit.put("S",new Integer(19));
    charsToDigit.put("T",new Integer(20));
    charsToDigit.put("U",new Integer(21));
    charsToDigit.put("V",new Integer(22));
    charsToDigit.put("W",new Integer(23));
    charsToDigit.put("X",new Integer(24));
    charsToDigit.put("Y",new Integer(25));
    charsToDigit.put("Z",new Integer(26));
  }
  
  public OOoTextDocumentParser getOOoTextDocumentParser()
  {
    return m_oOOoTextDocumentParser;
  }
  
  
  private TextFrame getFrameOfTable(TextDocument document, TextTable table)
  {
    XTextTable tableToFind                              = ((OOoTextTable)table).getXTextTable();
    XTextFramesSupplier xTextFramesSupplier = (XTextFramesSupplier)UnoRuntime.queryInterface(XTextFramesSupplier.class, ((OOoTextDocument)document).getXModel());
    if (xTextFramesSupplier == null) return null;
    XNameAccess xNameAccess                      = xTextFramesSupplier.getTextFrames();
    if (xNameAccess == null) return null;
    String[] sTextFieldNames                           = xNameAccess.getElementNames();
    if (sTextFieldNames == null) return null;
    for(int i=0; i<(sTextFieldNames.length); i++)
    {
      try
      {
        Object textframeobject = xNameAccess.getByName(sTextFieldNames[i]);
        XTextFrame textframe = (XTextFrame) UnoRuntime.queryInterface(XTextFrame.class, textframeobject);

        XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textframe);
        if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
        {
          XText xFrameText = textframe.getText();
          XEnumerationAccess xParaAccess       = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xFrameText);
          XEnumeration xParaEnum                  = xParaAccess.createEnumeration();
          while (xParaEnum.hasMoreElements()) 
          {
            Object paraobj                              = xParaEnum.nextElement();
            XServiceInfo xFrameElementInfo     = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);
            //XTextContent xFrameElementContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
            
            if (xFrameElementInfo.supportsService("com.sun.star.text.TextTable"))
            {
              XTextTable xFrameTable               = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, paraobj);
              if (xFrameTable.equals(tableToFind)) return new OOoTextFrame((OOoTextDocument)document, textframe);
            }
          }
        }
      }
      catch (NoSuchElementException e) 
      {
        logger.error("getFrameOfTable(): Error", e );
      }
      catch (WrappedTargetException e) 
      {
        logger.error("getFrameOfTable(): Error", e );
      } 
    }
    return null;
  }

  
  
  
  public VirtualTextTable parseTextTable(TextDocument document, TextTable table)
  {    
    logger.debug("parseTextTable");
  	OOoTextTable tt 	= (OOoTextTable)table; 
    XTextTable xtable 	= tt.getXTextTable();
    //String tablename 	= getComponentName(xtable);
        
    
    XTableRows rows 	= xtable.getRows();    
    XTableColumns cols  = xtable.getColumns();
    int numrows 		= rows.getCount();    
    int numcols 		= cols.getCount();
    
    
    logger.debug("parseTextTable cols:" + numcols);
    VirtualTextTable vtable 		  = new VirtualTextTable();
    

    String[] cellNames =  xtable.getCellNames();
    for(int i=0; i < cellNames.length; i++)
    {
        logger.debug("cellName i"+ i + " name " + cellNames[i]);
    	int row 	=  Integer.parseInt(cellNames[i].substring(1,cellNames[i].length()))-1;
    	String sCol =  cellNames[i].substring(0,1);
    	int col 	= ((Integer)charsToDigit.get(sCol)).intValue()-1;
    	if((col+1) > numcols)
    	{
    		numcols = (col+1);
    	}
    	
    	VirtualTextTableCell cell = getTextTableCellParser().parseTextTableCell(document, tt, row, col);
        vtable.setTextTableCell(row, col, cell);
        logger.debug("insertCell row:"+row + " col:"+col + " cell:"+cell);
    }
    logger.debug("max cols: " + numcols);
    vtable.setSize(numrows, numcols);

    // column widths...
    XPropertySet xTableProps            = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xtable);
    try
    {
      TableColumnSeparator[] separators = null;
      Object separatorsObject           = (xTableProps.getPropertyValue("TableColumnSeparators"));
      
      short colRelativeSum              = ((Short)(xTableProps.getPropertyValue("TableColumnRelativeSum"))).shortValue();      
      int leftMargin                    = ((Integer)xTableProps.getPropertyValue("LeftMargin")).intValue();
      int rightMargin                   = ((Integer)xTableProps.getPropertyValue("RightMargin")).intValue();
      short horiOrientation             = ((Short)xTableProps.getPropertyValue("HoriOrient")).shortValue();
      
      logger.debug("horiOrientation " + horiOrientation + " " + HoriOrientation.CENTER);
      vtable.setMarginLeft(new VirtualMargin( leftMargin ));
      
      
      Object anchorType =  xTableProps.getPropertyValue("AnchorType");
      if (TextContentAnchorType.AT_FRAME.equals( anchorType ))
      {
          logger.debug("table is in frame");
      }else logger.debug("anchor type " +  ((TextContentAnchorType)anchorType).getValue());
      
       
          
      // debug
        /*XEnumerationAccess xParaAccess          = (XEnumerationAccess) 
        UnoRuntime.queryInterface(XEnumerationAccess.class, xtable.getAnchor().getText());
        XEnumeration xParaEnum                  = xParaAccess.createEnumeration();
        while (xParaEnum.hasMoreElements()) 
        {
            Object para                         = xParaEnum.nextElement();
            XServiceInfo xInfo                  = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, para);
            String[] services                   = xInfo.getSupportedServiceNames();
            
            for(int i=0; i < services.length; i++)
            {
                logger.debug("services "+ services[i]);
            }
        }*/
      
      
//      testen ob die Tabelle in einem Frame liegt...
        float tableWidth = -1;
        TextFrame frame = getFrameOfTable(document, table);
        if (frame != null)
        {
          // Tabelle liegt innerhalb eines Frames...
          tableWidth = frame.getWidth() - leftMargin - rightMargin;
        }
        else tableWidth = document.getPageWidth() - leftMargin - rightMargin - document.getLeftMargin() - document.getRightMargin(); 
      
      
      vtable.setWidth( tableWidth );
      if (separatorsObject instanceof TableColumnSeparator[])
      {
      	separators = (TableColumnSeparator[])(separatorsObject);
        int lastPosition = 0;
        for(int column=0; column<(separators.length); column++)
        {
          int pos = (separators[column].Position);
          double width = pos - lastPosition;  
          lastPosition = pos;

          double mmWidth =  (width / colRelativeSum) * tableWidth;
//        mmWidth = Math.round(width / 0.5882352941176471);
//        Faktor = 0.5882352941176471
          vtable.setColumnWidth(column, new Integer((int)mmWidth));
          logger.debug("parse col:"+column + " width " + mmWidth + " tableWidth " + tableWidth);
        }
      }
    }
    catch (Exception e) 
    {
      logger.error("Error", e );
    }
    
    return vtable;
  }
  
  
  
  public String getComponentName(XTextContent comp)
  {
    XNamed xNamed = (XNamed) UnoRuntime.queryInterface(XNamed.class, comp); 
    if (xNamed != null) return xNamed.getName();
    else return("*?*");
  }

  
  public TextTableCellParser getTextTableCellParser()
  {
    if(m_oTextTableCellParser == null)
    {
      m_oTextTableCellParser = new OOoTextTableCellParser(this);
    }
    return m_oTextTableCellParser;
  }
}
