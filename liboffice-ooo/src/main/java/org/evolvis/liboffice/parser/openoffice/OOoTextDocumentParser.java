/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 02.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.openoffice;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextGraphic;
import org.evolvis.liboffice.document.openoffice.OOoTextParagraph;
import org.evolvis.liboffice.document.openoffice.OOoTextTable;
import org.evolvis.liboffice.parser.TextDocumentParser;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;
import org.evolvis.liboffice.virtualdocument.graphic.VirtualGraphic;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.frame.XController;
import com.sun.star.frame.XModel;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XParagraphCursor;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextGraphicObjectsSupplier;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextRangeCompare;
import com.sun.star.text.XTextTable;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextDocumentParser implements TextDocumentParser
{
  private OOoTextParagraphParser m_oOOoTextParagraphParser = null;
  private OOoTextTableParser m_oOOoTextTableParser = null; 
  private OOoTextGraphicParser m_oOOoTextGraphicParser = null; 
  private Logger logger = Logger.getLogger(getClass());
  
  private XTextDocument getTextDocumentFromComponent(XComponent comp)
  {
    return(XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, comp);    
  }
  
  public OOoTextParagraphParser getOOoTextParagraphParser()
  {
    if (m_oOOoTextParagraphParser == null)
    {
      m_oOOoTextParagraphParser = new OOoTextParagraphParser(this);
    }
    return m_oOOoTextParagraphParser;    
  }
  
  public OOoTextGraphicParser getOOoTextGraphicParser()
  {
    if (m_oOOoTextGraphicParser == null)
    {
      m_oOOoTextGraphicParser = new OOoTextGraphicParser();
    }
    return m_oOOoTextGraphicParser;    
  }
  
  

  
  public OOoTextTableParser getOOoTextTableParser()
  {
    if (m_oOOoTextTableParser == null)
    {
      m_oOOoTextTableParser = new OOoTextTableParser(this);
    }
    return m_oOOoTextTableParser;  
  }
  
  
    
  
  private List getInternalGraphicHandles(XTextDocument tdoc)
  {
    List graphics = new ArrayList();    
    XTextGraphicObjectsSupplier xTextGraphicObjectsSupplier = (XTextGraphicObjectsSupplier) UnoRuntime.queryInterface(XTextGraphicObjectsSupplier.class, tdoc);    
    XNameAccess xNameAccess = xTextGraphicObjectsSupplier.getGraphicObjects();    
    String[] graphicnames = xNameAccess.getElementNames();
    for(int i=0; i<(graphicnames.length); i++)
    {
      logger.debug("graphicnames[" + i + "] = " + graphicnames[i]);
      try
      {
        Object oGfx = xNameAccess.getByName(graphicnames[i]);
        XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, oGfx);

        if (xInfo.supportsService("com.sun.star.text.TextGraphicObject"))
        {
        
          XTextContent gfxTextContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, oGfx);
          XTextRange range = gfxTextContent.getAnchor();
        
          XTextCursor cursor = range.getText().createTextCursor();
          XParagraphCursor paracursor = (XParagraphCursor) UnoRuntime.queryInterface(XParagraphCursor.class, cursor);
          paracursor.gotoRange(range, false);
          paracursor.gotoPreviousParagraph(true);
          XTextRange start = paracursor.getStart();
        
          graphics.add(new InternalGraphicHandle(gfxTextContent, start));        
        }        
      }
      catch (NoSuchElementException e)
      {
          logger.error("Error", e );
      }
      catch (WrappedTargetException e)
      {
          logger.error("Error", e );
      }
    }
    return graphics;    
  }
  
  
  public VirtualTextDocument parseTextDocument(TextDocument document)
  {
  	long timeStartParse = System.currentTimeMillis();
      logger.debug("parseTextDocument");
    OOoTextDocument otd = (OOoTextDocument)document;
    XTextDocument tdoc = getTextDocumentFromComponent(otd.getXModel());
    XText xText = tdoc.getText();
    
    long timeStartGraphic = System.currentTimeMillis();
    List graphics = getInternalGraphicHandles(tdoc);
    long timeEndGraphic = System.currentTimeMillis();
    logger.debug("parseTextDocument getGraphics elapsed time" + (timeEndGraphic - timeStartGraphic));
    
    long timeStartDoc   = System.currentTimeMillis();
    XController xController = otd.getXModel().getCurrentController();
    ParagraphList comps 	= parseTextDocumentInternal(otd, otd.getXModel(), xController, tdoc, xText, graphics);    
    VirtualTextDocument doc = new VirtualTextDocument();    
    for(int i=0; i<(comps.size()); i++)
    {
      VirtualTextComponent comp = (VirtualTextComponent)(comps.get(i));
      doc.addTextComponent(comp);  
    }
    long timeEndDoc = System.currentTimeMillis();
    logger.debug("parseTextDocument getComponents elapsed time" + (timeEndDoc - timeStartDoc));
    doc.setComponentContainingCursor(comps.getComponentContainingCursor());
    
    long timeEndParse = System.currentTimeMillis();
    logger.debug("parseTextDocument total elapsed time" + (timeEndParse - timeStartParse));
    return doc;     
  }
  
  // TODO: ueberpruefen ob hier nicht zu viel geparsed wird
  public VirtualStyledText parseTextDocument(TextDocument document, XText xText)
  {
    
  	XTextDocument oXTextDocument = ((OOoTextDocument)document).getXTextDocument();        
    VirtualStyledText txt 		 = new VirtualStyledText();   
    XController xController 	 = ((OOoTextDocument)document).getXModel().getCurrentController();
    List comps 					 = parseTextDocumentInternal((OOoTextDocument)document, ((OOoTextDocument)document).getXModel(), xController, oXTextDocument, xText, new ArrayList());    
    for(int i=0; i<(comps.size()); i++)
    {
      VirtualTextComponent comp = (VirtualTextComponent)(comps.get(i));
      logger.debug("parseTextDocument comp:"+comp);
      if (comp instanceof VirtualParagraph)
      {
        VirtualParagraph para = (VirtualParagraph)comp;
        txt.addParagraph(para);
      }
      else System.out.println("unknown component " + comp.getClass());
    }    
    return txt;
  }
  
  
  private class InternalGraphicHandle 
  {
    private XTextContent m_oXTextContent;
    private XTextRange m_oXTextRange;
    
    public InternalGraphicHandle(XTextContent gfxTextContent, XTextRange rangeStart)
    {
      m_oXTextContent = gfxTextContent;
      m_oXTextRange = rangeStart;
    }
    public XTextContent getXTextContent()
    {
      return m_oXTextContent;
    }
    public XTextRange getXTextRange()
    {
      return m_oXTextRange;
    }
  }
  

  
  public class ParagraphList extends ArrayList
  {
    /** serialVersionUID */
	private static final long serialVersionUID = -6275257911954339336L;
	private VirtualTextComponent m_oCursorComp = null;
    
    public void setComponentContainingCursor(VirtualTextComponent comp)
    {
      m_oCursorComp = comp;
    }

    public VirtualTextComponent getComponentContainingCursor()
    {
      return m_oCursorComp;
    }
  }
  
  // kleiner hack um tabellen-zellen parsen zu k�nnen...
  //private List parseTextDocumentInternal(XTextDocument xTextDocument, XText xText, List graphics)
  private ParagraphList parseTextDocumentInternal(OOoTextDocument doc, XModel xmodel, XController xController, XTextDocument xTextDocument, XText xText, List graphics)
  {    
    // aktuelle Cursor-Position ermitteln...
    XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
    XTextViewCursor xViewCursor 				= xViewCursorSupplier.getViewCursor();
    //XText xDocumentText 						= xViewCursor.getText();    
    XTextRange cursorRange 						= xViewCursor.getStart();

    // Range-Comparator holen...
    XTextRangeCompare xTextRangeCompare 		= (XTextRangeCompare)(UnoRuntime.queryInterface(XTextRangeCompare.class, xText)); 
    
    ParagraphList vd 							= new ParagraphList();
        
    try 
    {
      XEnumerationAccess xParaAccess 			= (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
      XEnumeration xParaEnum 					= xParaAccess.createEnumeration();
      while (xParaEnum.hasMoreElements()) 
      {
        Object paraobj 							= xParaEnum.nextElement();
        XServiceInfo xInfo 						= (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);
        XTextContent content 					= (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
        
        
        boolean bContainsCursor 				= false;
        XTextRange currentRange 				= content.getAnchor().getStart();
        try
        {
          if (xTextRangeCompare.compareRegionStarts(cursorRange, currentRange) == 0)
          {
            logger.debug("!!!!!!!!!!!!!!!!! FOUND CURSOR POSITION !!!!!!!!!!!!!!!!!!!");
            bContainsCursor = true;
          }
        }        
        catch(com.sun.star.lang.IllegalArgumentException iae) 
        {
            //logger.error("Error", iae );
        }
        
        if (xInfo.supportsService("com.sun.star.text.Paragraph"))
        {
          logger.debug("ist PARAGRAPH");          
          //XTextContent content = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
          
          OOoTextParagraph textparagraph 		= new OOoTextParagraph(doc, xmodel, content);          
          VirtualParagraph vp 					= getOOoTextParagraphParser().parseTextParagraph(textparagraph);
          if (bContainsCursor) vd.setComponentContainingCursor(vp);
          //TODO: testen ob dieser ParaBreak hier richtig ist...
          // auskommentiert und VirtualParagraphBreak in parseTextParagraph hinzugefuegt, damit der Break beim Parsen eines
          // einzelnen Paragraphen nicht verloren geht
          //vp.addTextPortion(new VirtualParagraphBreak());
          vd.add(vp);
          
          // testen ob nach diesem Paragraph ein Bild kam...
          XTextRange start = content.getAnchor().getStart();
          logger.debug("PARA start = " + start);
          for(int i=0; i<(graphics.size()); i++)
          {
            InternalGraphicHandle oInternalGraphicHandle = (InternalGraphicHandle)(graphics.get(i));
            try
            {
              short result = xTextRangeCompare.compareRegionStarts(start, oInternalGraphicHandle.getXTextRange());          
              if (result == 0)
              {
                logger.debug("FOUND GRAPHIC");
                XTextContent gfxTextContent = oInternalGraphicHandle.getXTextContent();              
                VirtualGraphic oVirtualGraphic = getOOoTextGraphicParser().parseTextGraphic(new OOoTextGraphic(xmodel, gfxTextContent));
                vd.add(oVirtualGraphic);              
              }
            }
            catch(com.sun.star.lang.IllegalArgumentException iae)
            {              
              //TODO: auch Graphiken in Tabellen ordentlich erkennen...
              logger.warn("Paragraph der Graphik konnte nicht ermittelt werden... befindet sich vermutlich in Tabelle... TODO !");
              logger.error("Error", iae );
            }
          }
          // ende graphic-test          
        }
        else if (xInfo.supportsService("com.sun.star.text.TextTable")) 
        {
          logger.debug("ist TABLE");
        	
          XTextTable table = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, paraobj);
          OOoTextTable texttable = new OOoTextTable(new OOoTextDocument(xTextDocument), table);          
        
          VirtualTextTable vt = getOOoTextTableParser().parseTextTable(new OOoTextDocument(xTextDocument), texttable);
          if (bContainsCursor) vd.setComponentContainingCursor(vt);          
          vd.add(vt);
        }
        else
        {        
          logger.debug(" --- UNKNOWN PARAGRAPH ---");          
          String[] services = xInfo.getSupportedServiceNames();
          for(int i=0; i<services.length; i++)
          {
              logger.debug("services[" + i + "] = " + services[i]);
          }  
          logger.debug(" -------------------------");
        }

      }
    } 
    catch (Exception e) 
    {
      logger.debug("EXCEPTION !!!! -------------------------");
      logger.error("Error", e );
      logger.debug(" -------------------------");
      return null;
    }
    
    return vd;
  }
}
