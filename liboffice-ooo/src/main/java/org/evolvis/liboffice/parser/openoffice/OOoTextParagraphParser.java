/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 03.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextParagraph;
import org.evolvis.liboffice.document.openoffice.OOoTextParagraph;
import org.evolvis.liboffice.parser.TextParagraphParser;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraphBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextFont;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyleType;

import com.sun.star.awt.FontSlant;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextRange;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextParagraphParser implements TextParagraphParser
{
  private Logger logger = Logger.getLogger(getClass());
  public OOoTextParagraphParser(OOoTextDocumentParser documentparser)
  {
  }
  
  
  public VirtualParagraph parseTextParagraph(TextParagraph paragraph)
  {
    OOoTextParagraph textparagraph = (OOoTextParagraph)paragraph;
    VirtualParagraph vp = new VirtualParagraph();
    
    XTextRange xTextRange = textparagraph.getXTextContent().getAnchor();
    XText xDocumentText = xTextRange.getText();
    
    XTextCursor xModelCursor = xDocumentText.createTextCursorByRange(xTextRange.getStart());
    XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);
    
    
    String stylename = null;
    VirtualTextStyleType styletype = null;
    try
    {
      stylename = (String)(xCursorProps.getPropertyValue("ParaStyleName"));      
      styletype = VirtualTextStyleType.STYLE_PARAGRAPH;     
    }
    catch (UnknownPropertyException e) 
    {
        logger.error("Error", e );
    }
    catch (WrappedTargetException e) 
    {
        logger.error("Error", e );
    }

    if (styletype == null)
    {
      try
      {
        stylename = (String)(xCursorProps.getPropertyValue("CharStyleName"));      
        styletype = VirtualTextStyleType.STYLE_CHARACTER;     
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("Error", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("Error", e );
      }
    }
    
    vp.setStyle(new VirtualTextStyle(stylename, styletype));    

    // create another enumeration to get all text portions of the paragraph
    XEnumerationAccess xParaEnumerationAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, textparagraph.getXTextContent());
    XEnumeration xTextPortionEnum = xParaEnumerationAccess.createEnumeration();
    
    while(xTextPortionEnum.hasMoreElements()) 
    {
      try
      {        
        Object portionobj = xTextPortionEnum.nextElement();
                
        XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, portionobj);
        if (xInfo.supportsService("com.sun.star.text.TextPortion")) 
        {
          XPropertySet portionPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);
          String portiontype = (String)(portionPropertySet.getPropertyValue("TextPortionType"));

          logger.debug("scanning TextPortion: " + portiontype);          
          
          if ("TextField".equals(portiontype))
          {
            if (xInfo.supportsService("com.sun.star.text.TextField")) 
            {
              //TODO: auch den hint ermitteln!
              XTextContent tf = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, portionobj);
              //XServiceInfo xInfo2 = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, tf);
              String fieldname = tf.getAnchor().getString();
              if (fieldname.startsWith("<") && fieldname.endsWith(">"))
              {
                fieldname = fieldname.substring(1, fieldname.length()-1);
                VirtualTextPortionMarker marker = new VirtualTextPortionMarker(fieldname, "?");
                vp.addTextPortion(marker);            
              }
            }                          
          } // ende TextField
          else if ("Text".equals(portiontype))
          {
            XTextRange xTextPortion = (XTextRange) UnoRuntime.queryInterface(XTextRange.class, portionobj);                                
            String portiontext = xTextPortion.getString();

            logger.debug("scanning portiontext: >" + portiontext+"<");          
            
            VirtualTextPortionText portion = new VirtualTextPortionText(portiontext);
            
            XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xTextPortion);
            
            
            String fontname      = (String)(xPropertySet.getPropertyValue("CharFontName"));
            
            if (fontname != null)
            {
              portion.setFont(new VirtualTextFont(fontname));
              double fontheight = ((Float)(xPropertySet.getPropertyValue("CharHeight"))).doubleValue();
              //double fontweight = ((Float)(xPropertySet.getPropertyValue("CharWeight"))).doubleValue();
              portion.getFont().setFontHeight(fontheight);
            }
            
            String charStyleName = (String)(xPropertySet.getPropertyValue("CharStyleName"));
            if (null != charStyleName)
            {
                portion.setStyle(new VirtualTextStyle(charStyleName, VirtualTextStyleType.STYLE_CHARACTER));
            }
            
            Float bold = (Float)(xPropertySet.getPropertyValue("CharWeight"));
            if (bold != null)
            {
              portion.setBold((bold.floatValue() == com.sun.star.awt.FontWeight.BOLD));
            }
            
            FontSlant fontslant = (FontSlant)(xPropertySet.getPropertyValue("CharPosture"));
            if (fontslant != null)
            {
              portion.setItalic((fontslant.getValue() == com.sun.star.awt.FontSlant.ITALIC_value));
            }
            
            Short underline = (Short)(xPropertySet.getPropertyValue("CharUnderline"));
            if (underline != null)
            {
              portion.setUnderline((underline.shortValue() == com.sun.star.awt.FontUnderline.SINGLE));
            }
            
            vp.addTextPortion(portion);            
          } // ende Text
          else logger.warn("UNKNOWN TextPortion: " + portiontype);
          
        } // ende if TextPortion        
        else 
        {
            logger.warn("ERROR not an TextPortion in TextPortionEnumeration portionobj=" + portionobj + " ||||| xInfo=" + xInfo);
        }
        
      }
      catch(WrappedTargetException e)
      {        
        logger.error("Error", e );
      }
      catch (NoSuchElementException e)
      {
          logger.error("Error", e );
      }
      catch (UnknownPropertyException e)
      {
          logger.error("Error", e );
      }
    } // ende while
    vp.addTextPortion(new VirtualParagraphBreak());
    return vp;
  }

  
  
  
  
  
  
  
  
  
  
  public VirtualParagraph parseTextParagraph(XText xtextcontent)
  {
    VirtualParagraph vp = new VirtualParagraph();
    
    XTextRange xTextRange = xtextcontent;
    XText xDocumentText = xTextRange.getText();
    
    XTextCursor xModelCursor = xDocumentText.createTextCursorByRange(xTextRange.getStart());
    XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);
        
    String stylename = null;
    VirtualTextStyleType styletype = null;
    try
    {
      stylename = (String)(xCursorProps.getPropertyValue("ParaStyleName"));      
      styletype = VirtualTextStyleType.STYLE_PARAGRAPH;     
    }
    catch (UnknownPropertyException e) 
    {
        logger.error("Error", e );
    }
    catch (WrappedTargetException e) 
    {
        logger.error("Error", e );
    }

    if (styletype == null)
    {
      try
      {
        stylename = (String)(xCursorProps.getPropertyValue("CharStyleName"));      
        styletype = VirtualTextStyleType.STYLE_CHARACTER;     
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("Error", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("Error", e );
      }
    }
    
    if ((stylename != null) && (styletype != null))
    {
      vp.setStyle(new VirtualTextStyle(stylename, styletype));
    }
    
    // create another enumeration to get all text portions of the paragraph
    XEnumerationAccess xParaEnumerationAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xtextcontent);
    XEnumeration xTextPortionEnum = xParaEnumerationAccess.createEnumeration();
    
    while(xTextPortionEnum.hasMoreElements()) 
    {
      try
      {        
        Object portionobj = xTextPortionEnum.nextElement();
        
        XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, portionobj);
        
//        logger.debug("portionobj = " + portionobj);
//        String[] services = xInfo.getSupportedServiceNames();
//        for(int i=0; i<(services.length); i++)
//        {
//          logger.debug("services[" + i + "] = " + services[i]);
//        }
        
        if (xInfo.supportsService("com.sun.star.text.TextPortion")) 
        {
          XPropertySet portionPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);
          String portiontype = (String)(portionPropertySet.getPropertyValue("TextPortionType"));
          
          if ("TextField".equals(portiontype))
          {
            if (xInfo.supportsService("com.sun.star.text.TextField")) 
            {
              //TODO: auch den hint ermitteln!
              XTextContent tf = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, portionobj);
              //XServiceInfo xInfo2 = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, tf);
              String fieldname = tf.getAnchor().getString();
              if (fieldname.startsWith("<") && fieldname.endsWith(">"))
              {
                fieldname = fieldname.substring(1, fieldname.length()-1);
                VirtualTextPortionMarker marker = new VirtualTextPortionMarker(fieldname, "?");
                vp.addTextPortion(marker);            
              }
            }              
          }
          else if ("Text".equals(portiontype))
          {
            XTextRange xTextPortion = (XTextRange) UnoRuntime.queryInterface(XTextRange.class, portionobj);                                
            String portiontext = xTextPortion.getString();
            VirtualTextPortionText portion = new VirtualTextPortionText(portiontext);
            XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xTextPortion);

            String fontname = (String)(xPropertySet.getPropertyValue("CharFontName"));
            if (fontname != null)
            {
              portion.setFont(new VirtualTextFont(fontname));
              double fontheight = ((Float)(xPropertySet.getPropertyValue("CharHeight"))).doubleValue();
              //double fontweight = ((Float)(xPropertySet.getPropertyValue("CharWeight"))).doubleValue();
              portion.getFont().setFontHeight(fontheight);
            }
            
            Float bold = (Float)(xPropertySet.getPropertyValue("CharWeight"));
            if (bold != null)
            {
              portion.setBold((bold.floatValue() == com.sun.star.awt.FontWeight.BOLD));
            }
            
            FontSlant fontslant = (FontSlant)(xPropertySet.getPropertyValue("CharPosture"));
            if (fontslant != null)
            {
              portion.setItalic((fontslant.getValue() == com.sun.star.awt.FontSlant.ITALIC_value));
            }
            
            Short underline = (Short)(xPropertySet.getPropertyValue("CharUnderline"));
            if (underline != null)
            {
              portion.setUnderline((underline.shortValue() == com.sun.star.awt.FontUnderline.SINGLE));
            }
            
            vp.addTextPortion(portion);            
          }
        }        
      }
      catch(WrappedTargetException e)
      {        
          logger.error("Error", e );
      }
      catch (NoSuchElementException e)
      {
          logger.error("Error", e );
      }
      catch (UnknownPropertyException e)
      {
          logger.error("Error", e );
      }
    }
    
    return vp;
  }
  
  
  
}
