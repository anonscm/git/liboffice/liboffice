/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 13.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextGraphic;
import org.evolvis.liboffice.document.openoffice.OOoTextGraphic;
import org.evolvis.liboffice.parser.TextGraphicParser;
import org.evolvis.liboffice.virtualdocument.graphic.VirtualGraphic;

import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.text.XTextContent;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextGraphicParser implements TextGraphicParser
{
  private Logger logger = Logger.getLogger(getClass());
  public VirtualGraphic parseTextGraphic(TextGraphic graphic)
  {
    OOoTextGraphic ooographic = (OOoTextGraphic)graphic;    
    XTextContent gfxTextContent = ooographic.getXTextContent();
    
    VirtualGraphic oVirtualGraphic = new VirtualGraphic();
    
    XPropertySet xGfxProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, gfxTextContent);
    try
    {
      String url = (String)(xGfxProps.getPropertyValue("GraphicURL"));
      if (url != null)
      {
        oVirtualGraphic.setFilename(url);
      }
      
      Boolean print = (Boolean)(xGfxProps.getPropertyValue("Print"));
      if (print != null)
      {
        oVirtualGraphic.setPrintable(print.booleanValue());
      }

      com.sun.star.awt.Size size = (com.sun.star.awt.Size)(xGfxProps.getPropertyValue("Size"));
      if (size != null)
      {
        oVirtualGraphic.setSize(size.Width, size.Height);
      }
      else
      {
        Integer height = (Integer)(xGfxProps.getPropertyValue("Height"));
        Integer width = (Integer)(xGfxProps.getPropertyValue("Width"));
        if ( (height != null) && (width != null))
        {
          oVirtualGraphic.setSize(width.intValue(), height.intValue());        
        }
      }
      
      
//Property[] props = xGfxProps.getPropertySetInfo().getProperties();
//for(int b=0; b<(props.length); b++)
//{
//  logger.debug("gfx props[" + b + "] = " + props[b].Name);
//}
      
      
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("Error", e1 );
        return null;
    }
    catch (WrappedTargetException e)
    {
        logger.error("Error", e );
        return null;
    }
    
    return oVirtualGraphic;
  }

}
