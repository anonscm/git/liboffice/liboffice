/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.TextTableCell;
import org.evolvis.liboffice.document.openoffice.OOoTextTable;
import org.evolvis.liboffice.document.openoffice.OOoTextTableCell;
import org.evolvis.liboffice.parser.TextTableCellParser;
import org.evolvis.liboffice.virtualdocument.VirtualBorder;
import org.evolvis.liboffice.virtualdocument.VirtualMargin;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.table.BorderLine;
import com.sun.star.table.XCellRange;
import com.sun.star.text.XText;
import com.sun.star.text.XTextTable;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextTableCellParser implements TextTableCellParser
{
  private OOoTextTableParser m_oOOoTextTableParser = null;
  private Logger logger = Logger.getLogger(getClass());
  
  public OOoTextTableCellParser(OOoTextTableParser tableparser)
  {
    m_oOOoTextTableParser = tableparser;
  }
  
  
  public VirtualTextTableCell parseTextTableCell(TextDocument document, TextTable table, int row, int column)
  {
  	
  	logger.debug("parseTextTableCell");
  	XText xtext = getCellTextHandle(((OOoTextTable)table).getXTextTable(), column, row);
        
    OOoTextDocumentParser docparser = m_oOOoTextTableParser.getOOoTextDocumentParser();    
    VirtualStyledText stext 		= docparser.parseTextDocument(document, xtext);
    stext.cleanParagraphBreaks();
    VirtualTextTableCell vcell 		= new VirtualTextTableCell(stext);
    
    
    XPropertySet xCellProps = null;
    try
    {
        XCellRange xCellRange         = (XCellRange)UnoRuntime.queryInterface(XCellRange.class, ((OOoTextTable)table).getXTextTable());
        XCellRange xCellRangeSingle = xCellRange.getCellRangeByPosition(column, row, column, row);
        xCellProps = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xCellRangeSingle);
    }
    catch (IndexOutOfBoundsException e)
    {
        e.printStackTrace();
    }      
    
    vcell.setBorderTop(parseCellBorder(xCellProps,  TextTableCell.BORDER_TOP));
    vcell.setBorderLeft(parseCellBorder(xCellProps, TextTableCell.BORDER_LEFT));
    vcell.setBorderRight(parseCellBorder(xCellProps, TextTableCell.BORDER_RIGHT));
    vcell.setBorderBottom(parseCellBorder(xCellProps, TextTableCell.BORDER_BOTTOM));
    
    vcell.setMarginTop(parseCellMargin(xCellProps, TextTableCell.BORDER_TOP));
    vcell.setMarginLeft(parseCellMargin(xCellProps, TextTableCell.BORDER_LEFT));
    vcell.setMarginRight(parseCellMargin(xCellProps, TextTableCell.BORDER_RIGHT));
    vcell.setMarginBottom(parseCellMargin(xCellProps, TextTableCell.BORDER_BOTTOM));
    
    return vcell;
  }
  

  private VirtualMargin parseCellMargin(XPropertySet xCellProps, Object line)
  {
    try
    {
      String linename = null;
      if (TextTableCell.BORDER_LEFT.equals(line))
      {
        linename = "LeftBorderDistance";
      }    
      else if (TextTableCell.BORDER_RIGHT.equals(line))
      {
        linename = "RightBorderDistance";
      }
      else if (TextTableCell.BORDER_TOP.equals(line))
      {
        linename = "TopBorderDistance";
      }
      else if (TextTableCell.BORDER_BOTTOM.equals(line))
      {
        linename = "BottomBorderDistance";
      }
      else return null;

      Integer margin = (Integer)(xCellProps.getPropertyValue(linename));
      if (margin != null)
      {
        VirtualMargin vMargin = new VirtualMargin();        
        vMargin.setMargin((margin.intValue()));
        return vMargin;
      }
    }
    catch (UnknownPropertyException e) 
    {
        //logger.error("Error", e );
    }
    catch (WrappedTargetException e) 
    {
        logger.error("Error", e );
    } 
    return null;
  }
  
  
  
  
  
  
  private VirtualBorder parseCellBorder(XPropertySet xCellProps, Object line)
  {
    try
    {
       String linename = null;
      if (TextTableCell.BORDER_LEFT.equals(line))
      {
        linename = "LeftBorder";
      }    
      else if (TextTableCell.BORDER_RIGHT.equals(line))
      {
        linename = "RightBorder";
      }
      else if (TextTableCell.BORDER_TOP.equals(line))
      {
        linename = "TopBorder";
      }
      else if (TextTableCell.BORDER_BOTTOM.equals(line))
      {
        linename = "BottomBorder";
      }
      else return null;

      BorderLine borderline = (BorderLine)(xCellProps.getPropertyValue(linename));
      // borderline.Color = 0x000000;
      // borderline.OuterLineWidth = 5;
      if (borderline != null)
      {
        VirtualBorder vBorder = new VirtualBorder();        
        vBorder.setVisible((borderline.OuterLineWidth > 0));
        return vBorder;
      }
    }
    catch (UnknownPropertyException e) 
    {
        //logger.error("Error", e );
    }
    catch (WrappedTargetException e) 
    {
        logger.error("Error", e );
    } 
    return null;
  }
  
  private XText getCellTextHandle(XTextTable TT1, int xp, int yp)
  {
    String cellName = OOoTextTableCell.getCellName(xp, yp);
    return (XText) UnoRuntime.queryInterface(XText.class, TT1.getCellByName(cellName));
  }  
}
