/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.connection.openoffice;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.liboffice.connection.OfficeConnection;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.factory.openoffice.OOoMainOfficeFactory;

import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.connection.NoConnectException;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import com.sun.star.uno.XNamingService;


/**
 * 
 * Initializes the connection OpenOffice.org (or derivates)
 * 
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoOfficeConnection implements OfficeConnection
{
	private String connectString = null;
	private XMultiServiceFactory multiServiceFactory = null;    
	private boolean isConnected = false;
	private OOoMainOfficeFactory mainOfficeFactory = null;
	private Logger logger = Logger.getLogger(getClass().getName());
	
	
	public OOoOfficeConnection(OOoMainOfficeFactory mainfactory, String connectstring)
	{
		mainOfficeFactory = mainfactory;
		connectString = connectstring;

		XMultiServiceFactory oXMSF = connect(connectString); 
		if (oXMSF != null)
		{
			multiServiceFactory = oXMSF;
			isConnected = true;
		}
		else
		{
			//throw new NoOfficeException();
		}
	}
	
	public OOoOfficeConnection(XMultiServiceFactory pMultiServiceFactory) throws NoOfficeException
	{
		if(pMultiServiceFactory != null)
		{
			multiServiceFactory = pMultiServiceFactory;
			isConnected = true;
		}
		else throw new NoOfficeException();
	}

	public OOoMainOfficeFactory getMainOfficeFactory()
	{
		return mainOfficeFactory;
	}

	public boolean isConnected()
	{
		return isConnected;
	}

	public XMultiServiceFactory getXMultiServiceFactory()
	{
		return multiServiceFactory;
	}

	public OOoTextDocument getStarterDocument() throws NoOfficeException
	{
		OOoTextDocument doc = new OOoTextDocument(this);
		if (doc == null) throw new NoOfficeException("no starterdocument");
		return doc;     
	}


	private XMultiServiceFactory connect(String connectStr) 
	{
		if(logger.isLoggable(Level.FINE)) logger.info("connecting using connect-string "+connectStr);
		XMultiServiceFactory xMSF = null;
		try
		{
			XComponentContext xcomponentcontext = com.sun.star.comp.helper.Bootstrap.createInitialComponentContext(null);
			XMultiComponentFactory xLocalServiceManager = xcomponentcontext.getServiceManager();
			Object xUrlResolver  = xLocalServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", xcomponentcontext);
			XUnoUrlResolver oUrlResolver = (XUnoUrlResolver)UnoRuntime.queryInterface(XUnoUrlResolver.class, xUrlResolver);
			Object rInitialObject = oUrlResolver.resolve(connectStr);
			if (rInitialObject != null)
			{
				XServiceInfo xServiceInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, rInitialObject);
				if (xServiceInfo.supportsService("com.sun.star.lang.MultiServiceFactory"))
				{
					xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, rInitialObject);                  
				}
				else
				{
					XNamingService oXNamingService = (XNamingService)UnoRuntime.queryInterface(XNamingService.class, rInitialObject);
					if (oXNamingService != null) 
					{
						Object rXsmgr = oXNamingService.getRegisteredObject("StarOffice.ServiceManager");
						xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, rXsmgr);        
					} 
					else 
					{
						if(logger.isLoggable(Level.WARNING)) logger.warning("Fehler: Keinen Naming Service erhalten.");
					}
				}
			}
			else
			{
				if(logger.isLoggable(Level.WARNING)) logger.warning("Verbindungsversuch fehlgeschlagen!");
			}
		}
		catch (NoConnectException nce)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("Verbindungsversuch abgelehnt!");
		}    
		catch (Exception e)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("genereller Fehler beim Verbinden!");
			e.printStackTrace();
		}
		catch (UnsatisfiedLinkError e)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("Dynamic Libraries for OpenOffice.org/StarOffice could not be loaded\r\njava.library.path: "+System.getProperty("java.library.path"));
		}                
		return(xMSF);
	}
}
