/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.factory.openoffice;

import org.evolvis.liboffice.connection.openoffice.OOoOfficeConnection;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.controller.OfficeControllerToolKit;
import org.evolvis.liboffice.controller.ReplacementToolKit;
import org.evolvis.liboffice.controller.openoffice.OOoOfficeController;
import org.evolvis.liboffice.controller.openoffice.OOoOfficeControllerToolKit;
import org.evolvis.liboffice.controller.openoffice.OOoReplacementToolKit;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.locator.TextContentLocator;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocator;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoControllerFactory extends MainOfficeFactoryFetcherAdapter implements ControllerFactory  
{
  private OOoOfficeConnection officeConnection;
  private OOoOfficeController m_oOOoOfficeController = null;
  private OfficeControllerToolKit m_oOfficeControllerToolKit = null;
  private OOoTextContentLocator m_oOOoTextContentLocator = null;
  private ReplacementToolKit replacementToolKit;
  
  public OOoControllerFactory(OOoOfficeConnection connection)
  {
    officeConnection = connection;
  }
  
  
  public OfficeController getOfficeController()
  {
    if (m_oOOoOfficeController == null)
    {
      m_oOOoOfficeController =  new OOoOfficeController(officeConnection);
      m_oOOoOfficeController.setControllerFactory(this);
      m_oOOoOfficeController.setMainOfficeFactory(getMainOfficeFactory());
    }
    return m_oOOoOfficeController;
  }

  public OfficeControllerToolKit getOfficeControllerToolKit()
  {
    if (m_oOfficeControllerToolKit == null)
    {
      m_oOfficeControllerToolKit = new OOoOfficeControllerToolKit(); 
      m_oOfficeControllerToolKit.setMainOfficeFactory(getMainOfficeFactory());
    }
    return m_oOfficeControllerToolKit;
  }
  
  public ReplacementToolKit getReplacementToolKit()
  {
	  if(replacementToolKit == null) replacementToolKit = new OOoReplacementToolKit();
	  return replacementToolKit;
  }

  public TextContentLocator getTextContentLocator()
  {    
    if (m_oOOoTextContentLocator == null)
    {
      m_oOOoTextContentLocator = new OOoTextContentLocator(officeConnection);  
      m_oOOoTextContentLocator.setMainOfficeFactory(getMainOfficeFactory());
    }
    return m_oOOoTextContentLocator;
  }
}
