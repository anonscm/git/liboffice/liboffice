/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.factory.openoffice;

import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethodParameterDescriptionValuePair;
import org.evolvis.liboffice.connection.OfficeConnection;
import org.evolvis.liboffice.connection.openoffice.OOoOfficeConnection;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactory;
import org.evolvis.liboffice.factory.ParserFactory;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn, Steffen Kriese, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoMainOfficeFactory implements MainOfficeFactory
{
	private String connectString;
	private OOoOfficeConnection officeConnection;
	private ControllerFactory controllerFactory;
	private ParserFactory parserFactory;
	private Logger logger = Logger.getLogger(getClass().getName());
	
	public boolean connect(OfficeConnectionMethod pConnectionMethod, Map pConnectParams)
	{	
		// TODO check if connection-method is supported
		
		if(pConnectionMethod.getID().equals("socket"))
		{
			// initialize with default value
			Integer port = new Integer(8001);
			
			// try to get individually configured value 
			OfficeConnectionMethodParameterDescriptionValuePair descValuePair = (OfficeConnectionMethodParameterDescriptionValuePair)pConnectParams.get("socket-port");
			if(descValuePair != null)
				port = (Integer) descValuePair.getValue();
				
			connectString = "uno:socket,host=127.0.0.1,port="+port+";urp;StarOffice.ServiceManager";
			logger.info("Connecting to Office using "+pConnectionMethod.getName() + " and port " + port);
		}
		// use pipe-method as fallback
		else //if(pConnectionMethod.getID().equals("pipe"))
		{
			String pipeName = null;
			
			// try to get individually configured value 
			OfficeConnectionMethodParameterDescriptionValuePair descValuePair = (OfficeConnectionMethodParameterDescriptionValuePair)pConnectParams.get("pipe-name");
			if(descValuePair != null)
				pipeName = (String) descValuePair.getValue();
			
			connectString = "uno:pipe,name="+ pipeName +";urp;StarOffice.ServiceManager";
			logger.info("Connecting to Office using "+pConnectionMethod.getName() + " and pipe " + pipeName);
		}

		officeConnection = new OOoOfficeConnection(this, connectString);
		if (!officeConnection.isConnected())
		{
			officeConnection = null;
			return false;
		}
		return true;
	}
	
	public boolean useExistingConnection(OOoOfficeConnection pConnection)
	{
		officeConnection = pConnection;
		if(getConnection() == null || ! getConnection().isConnected()) return false;
		return true;
	}

	public ControllerFactory getControllerFactory()  
	{
		if (controllerFactory == null)
		{
			controllerFactory = new OOoControllerFactory(officeConnection);
			controllerFactory.setMainOfficeFactory(this);
		}
		return controllerFactory; 
	}

	public ParserFactory getParserFactory()
	{
		if (parserFactory == null)
		{
			parserFactory = new OOoParserFactory(); 
			parserFactory.setMainOfficeFactory(this);
		}
		return parserFactory;
	}
	
	public OfficeConnection getConnection()
	{
		return officeConnection;
	}
}
