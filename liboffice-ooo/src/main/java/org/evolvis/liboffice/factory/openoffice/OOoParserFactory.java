/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 02.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.factory.openoffice;

import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.factory.ParserFactory;
import org.evolvis.liboffice.parser.TextDocumentParser;
import org.evolvis.liboffice.parser.TextFrameParser;
import org.evolvis.liboffice.parser.TextGraphicParser;
import org.evolvis.liboffice.parser.TextParagraphParser;
import org.evolvis.liboffice.parser.TextTableParser;
import org.evolvis.liboffice.parser.openoffice.OOoTextDocumentParser;
import org.evolvis.liboffice.parser.openoffice.OOoTextFrameParser;
import org.evolvis.liboffice.parser.openoffice.OOoTextGraphicParser;
import org.evolvis.liboffice.parser.openoffice.OOoTextParagraphParser;
import org.evolvis.liboffice.parser.openoffice.OOoTextTableParser;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoParserFactory extends MainOfficeFactoryFetcherAdapter implements ParserFactory 
{
  private OOoTextDocumentParser m_oOOoTextDocumentParser = null;
  private OOoTextTableParser m_oOOoTextTableParser = null;
  private OOoTextParagraphParser m_oOOoTextParagraphParser = null;
  private OOoTextGraphicParser m_oOOoTextGraphicParser = null;
  private OOoTextFrameParser m_oOOoTextFrameParser = null;
  
  public TextDocumentParser getTextDocumentParser()
  {
    if (m_oOOoTextDocumentParser == null)
    {
      m_oOOoTextDocumentParser = new OOoTextDocumentParser(); 
    }
    return m_oOOoTextDocumentParser;
  }



  
  public TextTableParser getTextTableParser()
  {
    if (m_oOOoTextTableParser == null)
    {
      m_oOOoTextTableParser = new OOoTextTableParser((OOoTextDocumentParser)getTextDocumentParser()); 
    }
    return m_oOOoTextTableParser;
  }


  public TextParagraphParser getTextParagraphParser()
  {
    if (m_oOOoTextParagraphParser == null)
    {
      m_oOOoTextParagraphParser = new OOoTextParagraphParser((OOoTextDocumentParser)getTextDocumentParser()); 
    }    
    return m_oOOoTextParagraphParser;
  }


  public TextGraphicParser getTextGraphicParser()
  {
    if (m_oOOoTextGraphicParser == null)
    {
      m_oOOoTextGraphicParser = new OOoTextGraphicParser(); 
    }
    return m_oOOoTextGraphicParser;
  }


  public TextFrameParser getTextFrameParser()
  {
    if (m_oOOoTextFrameParser == null)
    {
      m_oOOoTextFrameParser = new OOoTextFrameParser((OOoTextDocumentParser)getTextDocumentParser()); 
    }    
    return m_oOOoTextFrameParser;
  }
}
