/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextParagraph;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextDocumentComponent;
import org.evolvis.liboffice.document.openoffice.OOoTextGraphic;
import org.evolvis.liboffice.document.openoffice.OOoTextMarker;
import org.evolvis.liboffice.document.openoffice.OOoTextParagraph;
import org.evolvis.liboffice.document.openoffice.OOoTextTable;
import org.evolvis.liboffice.document.openoffice.OOoTextTableCell;
import org.evolvis.liboffice.locator.TextContentLocation;

import com.sun.star.beans.PropertyValue;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.frame.XController;
import com.sun.star.frame.XFrame;
import com.sun.star.frame.XModel;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XParagraphCursor;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextRangeCompare;
import com.sun.star.text.XTextTable;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.view.XViewCursor;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextContentLocation implements TextContentLocation
{
  private XModel m_oXModel;
  private boolean m_bIsStarOffice7 = true;
  
  private XTextCursor m_oXTextCursor;
  private Logger logger = Logger.getLogger(getClass());

  public OOoTextContentLocation(XModel oXModel, XTextCursor cursor)
  {
    m_oXModel = oXModel;
    m_oXTextCursor = cursor;    
  }  
  
  public OOoTextContentLocation(XModel oXModel, XTextRange range)
  {
    m_oXModel = oXModel;
    XText xDocumentText = range.getText();
    m_oXTextCursor = xDocumentText.createTextCursorByRange(range.getStart());
  }  
  
  
  public OOoTextContentLocation(XModel oXModel, TextDocumentComponent component)
  {
    m_oXModel = oXModel;
    m_oXTextCursor = getXTextCursorForComponent(oXModel, component);
    if (m_oXTextCursor == null) throw new RuntimeException("es wurde versucht eine OOoTextContentLocation f�r ein unbekanntes Objekt zu ermitteln.");
  }

  public OOoTextContentLocation(OOoTextContentLocation location)
  {
    m_oXModel = location.m_oXModel;
    m_oXTextCursor = location.m_oXTextCursor;
  }  
  
  
    
  
  
  private XTextCursor getXTextCursorForComponent(XModel oXModel, TextDocumentComponent component)
  {
    XTextRange xTextRange = null;
    if (component instanceof OOoTextMarker)
    {
      XTextContent content= ((OOoTextMarker)component).getXTextContent();
      xTextRange = content.getAnchor();
      XText xDocumentText = xTextRange.getText();
      return xDocumentText.createTextCursorByRange(xTextRange.getStart());
    }
    else if (component instanceof OOoTextTable)
    {
      XTextTable table = ((OOoTextTable)component).getXTextTable();
      XTextContent oTableContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, table);
      xTextRange = oTableContent.getAnchor();
      XText xText = ((OOoTextTable)component).getOOoTextDocument().getXTextDocument().getText();

      XTextContent lastContent = null;
      XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
      XEnumeration xParaEnum = xParaAccess.createEnumeration();
      while (xParaEnum.hasMoreElements()) 
      {
        try
        {
          Object paraobj = xParaEnum.nextElement();
          XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);
          XTextContent content = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);

          if (xInfo.supportsService("com.sun.star.text.TextTable")) 
          {
            XTextTable docTable = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, paraobj);
            if (docTable.equals(table))
            {
              if (lastContent != null)
              {
                XTextRange lastRange = lastContent.getAnchor().getEnd(); 
                XText xDocumentText = lastRange.getText();
                return xDocumentText.createTextCursorByRange(lastRange.getEnd());
              }
              else
              {
                return xText.createTextCursor();
              }
            }
          }
          else
          {
            // die letzte Komponente die keine Tabelle war merken...
            lastContent = content;            
          }          
        }
        catch (NoSuchElementException e)
        {
            logger.error("Error", e );
        }
        catch (WrappedTargetException e)
        {
            logger.error("Error", e );
        }
      }      
    }
    else if (component instanceof OOoTextTableCell)
    {
      XTextTable table = ((OOoTextTableCell)component).getOOoTextTable().getXTextTable();
      int row =  ((OOoTextTableCell)component).getRow();
      int column =  ((OOoTextTableCell)component).getColumn();
      XText text = getCellTextHandle(table, column, row);
      return text.createTextCursor();          
    }
    else if (component instanceof OOoTextGraphic)
    {
      xTextRange = ((OOoTextGraphic)component).getXTextContent().getAnchor();
      XText xDocumentText = xTextRange.getText();
      return xDocumentText.createTextCursorByRange(xTextRange.getStart());
    }
    else if (component instanceof OOoTextParagraph)
    {
      if (m_bIsStarOffice7)
      {
        xTextRange = ((OOoTextParagraph)component).getXTextContent().getAnchor();
        XText xDocumentText = xTextRange.getText();        
        m_oXTextCursor = xDocumentText.createTextCursorByRange(xTextRange.getStart());
        XParagraphCursor xParaCursor = (XParagraphCursor)UnoRuntime.queryInterface(XParagraphCursor.class, m_oXTextCursor);
        xParaCursor.gotoStartOfParagraph(false);
        return (XTextCursor)UnoRuntime.queryInterface(XTextCursor.class, xParaCursor);
      }
      else
      {
        xTextRange = ((OOoTextParagraph)component).getXTextContent().getAnchor();
        XText xDocumentText = xTextRange.getText();
        return xDocumentText.createTextCursorByRange(xTextRange.getStart());        
      }
    }
    else if (component instanceof OOoTextDocument)
    {
      xTextRange = ((OOoTextDocument)component).getXTextDocument().getText();
      XText xDocumentText = xTextRange.getText();
      return xDocumentText.createTextCursorByRange(xTextRange.getStart());
    }

    return null;
  }

  
  private XTextContent getComponentAfter(XTextContent aftercontent)  
  {
    XText xText                       = aftercontent.getAnchor().getText();    
    
    XEnumerationAccess xParaAccess    = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
    XEnumeration xParaEnum            = xParaAccess.createEnumeration();
    while (xParaEnum.hasMoreElements()) 
    {
      try
      {
        Object paraobj                = xParaEnum.nextElement();
        XTextContent content          = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
        if (content.equals(aftercontent))
        {
          if (xParaEnum.hasMoreElements())
          {
            Object nextObj            = xParaEnum.nextElement();
            XTextContent afterContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, nextObj);
            return afterContent;
          }
          else return null;
        }
      }
      catch (NoSuchElementException e)
      {
        logger.error("Error", e );
      }
      catch (WrappedTargetException e)
      {
        logger.error("Error", e );
      }
    }
    return null;
  }
  
  /*
  private void showServices(String text, XTextContent content)
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, content);
    String[] services = xInfo.getSupportedServiceNames();
    for(int i=0; i<(services.length); i++) System.out.println(text + " service[" + i + "] = " + services[i]);
  }
  */
  
  
  
  public boolean positionAfterComponent(TextDocumentComponent component, TextDocument doc)
  {
    if (component instanceof TextParagraph)
    {
        XFrame xFrame = m_oXModel.getCurrentController().getFrame();
        if (xFrame != null)
        {
            XTextContent xContent = ((OOoTextDocumentComponent)component).getXTextContent();
            if (xContent != null)
            {
              XTextRange xTextRange = xContent.getAnchor();      
              XText xDocumentText = xTextRange.getText();
              m_oXTextCursor = xDocumentText.createTextCursorByRange(xTextRange.getStart());
           
             ((OOoTextDocument)doc).dispatchUnoCommand(xFrame, ".uno:GoToNextPara", new PropertyValue[0]);
            ((OOoTextDocument)doc).dispatchUnoCommand(xFrame, ".uno:GoToEndOfPara", new PropertyValue[0]);
             return true;
            }
        }
    }
    else
    {
        try
        {
          XTextContent xContent = getComponentAfter( ((OOoTextDocumentComponent)component).getXTextContent());
          if (xContent != null)
          {
            //OOoTextTable table = (OOoTextTable) component;
            
//           XController xController                     = m_oXModel.getCurrentController();    
//            XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
//            XTextViewCursor xViewCursor                 = xViewCursorSupplier.getViewCursor();
//            String cellName                             = OOoTextTableCell.getCellName(0,0);
//            XTextRange  rangeStart                      = (XTextRange)UnoRuntime.queryInterface(XTextRange.class, table.getXTextTable().getCellByName(cellName));
//            xViewCursor.gotoRange(rangeStart, false);
//            xViewCursor.gotoEnd(true);
//        
            
            //table.select();
            //doc.collapseSelection(true);
            
            
            XTextRange xTextRange = xContent.getAnchor();      
            XText xDocumentText = xTextRange.getText();
            
            //TODO: hier scheint es noch Probleme zu geben!...
            m_oXTextCursor = xDocumentText.createTextCursorByRange(xTextRange.getStart());
            return true;
          }
        } 
        catch(Exception e) 
        {
          logger.error("positionAfterComponent() failed: bad component type.", e);
          //e.printStackTrace();
        }
    }
        return false;
    //}
    //return false;
  }
  
  /*
  private void testProperty(XPropertySet xCursorProps, String key)
  {
    try
    {
      Object obj = xCursorProps.getPropertyValue(key);
      
      if (((Any)obj).getObject() != null)
      
//      if (!(obj instanceof Any))
      {
        logger.debug("Property(" + key + ") = " + obj);
      }
    }
    catch (UnknownPropertyException e)
    {
    }
    catch (WrappedTargetException e)
    {
    }
    
  }
  */
  
  
  
  
  
  
  
  public XTextCursor getXTextCursor()
  {    
    return m_oXTextCursor;
  }
  
  // -------------------------------------------
  
  private XText getCellTextHandle(XTextTable TT1, int xp, int yp)
  {
    String cellName = OOoTextTableCell.getCellName(xp, yp);
    return (XText) UnoRuntime.queryInterface(XText.class, TT1.getCellByName(cellName));
  }

  public void collapseToStart()
  {
    m_oXTextCursor.collapseToStart();
  }

  public void collapseToEnd()
  {
    m_oXTextCursor.collapseToEnd();
  }

  public void gotoStart(boolean collapse)
  {
    m_oXTextCursor.gotoStart(collapse);
  }

  public void gotoEnd(boolean collapse)
  {
    m_oXTextCursor.gotoEnd(collapse);
  }

  public void goLeft(int steps, boolean collapse)
  {
    m_oXTextCursor.goLeft((short)steps, collapse);
  }
  
  public void goRight(int steps, boolean collapse)
  {
    m_oXTextCursor.goRight((short)steps, collapse);    
  }

  public boolean goUp(int steps, boolean collapse)
  {
    XController xController           = m_oXModel.getCurrentController();
    XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
    XTextViewCursor xTextViewCursor         = xViewCursorSupplier.getViewCursor();
    xTextViewCursor.gotoRange(m_oXTextCursor, false);
    
    XViewCursor xViewCursor = (XViewCursor)UnoRuntime.queryInterface(XViewCursor.class, xTextViewCursor);
    xViewCursor.goUp((short)steps, collapse);

    XTextCursor oXTextCursor = (XTextCursor)UnoRuntime.queryInterface(XTextCursor.class, xViewCursor);    
    try
    {
      m_oXTextCursor.gotoRange(oXTextCursor, false);
    }
    catch(java.lang.Exception e)
    {
      return false;
    }
    return true;
  }
  
  public boolean goDown(int steps, boolean collapse)
  { 
    XController xController           = m_oXModel.getCurrentController();
    XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
    XTextViewCursor xTextViewCursor         = xViewCursorSupplier.getViewCursor();
    xTextViewCursor.gotoRange(m_oXTextCursor, false);
    
    XViewCursor xViewCursor = (XViewCursor)UnoRuntime.queryInterface(XViewCursor.class, xTextViewCursor);
    xViewCursor.goDown((short)steps, collapse);

    XTextCursor oXTextCursor = (XTextCursor)UnoRuntime.queryInterface(XTextCursor.class, xViewCursor);    
    try
    {
      m_oXTextCursor.gotoRange(oXTextCursor, false);
    }
    catch(Exception e)
    {
      return false;
    }
    return true;
  }
  
  
  
  public int compare(TextContentLocation location)
  {
    XTextRangeCompare xTextRangeCompare = (XTextRangeCompare)(UnoRuntime.queryInterface(XTextRangeCompare.class, m_oXTextCursor.getText()));
    if (xTextRangeCompare != null)
    {
      try
      {
          short result = xTextRangeCompare.compareRegionStarts(m_oXTextCursor, ((OOoTextContentLocation)location).getXTextCursor());
        return result;
      }
      catch (IllegalArgumentException e)
      {
          logger.error("Error", e );
      }          
    }
    return -1000;
  }
  
  
}
