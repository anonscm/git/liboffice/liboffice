/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator.openoffice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.connection.openoffice.OOoOfficeConnection;
import org.evolvis.liboffice.descriptors.AbstractSectionDescriptor;
import org.evolvis.liboffice.descriptors.AbstractTableDescriptor;
import org.evolvis.liboffice.descriptors.CellTextTableDescriptor;
import org.evolvis.liboffice.descriptors.FormattedTextDescriptor;
import org.evolvis.liboffice.descriptors.FrameDescriptor;
import org.evolvis.liboffice.descriptors.GraphicDescriptor;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.descriptors.StyleTableDescriptor;
import org.evolvis.liboffice.descriptors.StyledParagraphDescriptor;
import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.descriptors.TextDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextFrame;
import org.evolvis.liboffice.document.openoffice.OOoTextGraphic;
import org.evolvis.liboffice.document.openoffice.OOoTextMarker;
import org.evolvis.liboffice.document.openoffice.OOoTextParagraph;
import org.evolvis.liboffice.document.openoffice.OOoTextTable;
import org.evolvis.liboffice.document.openoffice.OOoTextTableCell;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.locator.TextContentDescriptorContainer;
import org.evolvis.liboffice.locator.TextContentLocator;
import org.evolvis.liboffice.locator.TextContentDescriptorContainer.DescriptorWrapper;

import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XIndexAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.container.XNamed;
import com.sun.star.frame.XModel;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextFieldsSupplier;
import com.sun.star.text.XTextFrame;
import com.sun.star.text.XTextFramesSupplier;
import com.sun.star.text.XTextGraphicObjectsSupplier;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextTable;
import com.sun.star.text.XTextTablesSupplier;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 */
public class OOoTextContentLocator extends MainOfficeFactoryFetcherAdapter implements TextContentLocator
{
	private static Logger logger            = Logger.getLogger(OOoTextContentLocator.class);
	private final static String DEFAULT_KEY = "*_*default_key*_*";

	public OOoTextContentLocator(OOoOfficeConnection connection)
	{
	}

	public boolean supportsDescriptor(TextContentDescriptor descriptor)
	{
		if (descriptor instanceof NameHintMarkerDescriptor) return true;
		if (descriptor instanceof StyleTableDescriptor) return true;
		if (descriptor instanceof CellTextTableDescriptor) return true;
		if (descriptor instanceof StyledParagraphDescriptor) return true;
		return false;
	}


	public Map getComponents(TextDocument document, TextContentDescriptorContainer cont)
	{
		// TODO uberpruefen ob sich die Descriptoren hier gegenseitig ueberschreiben koennen
		Map out  = getTable(((OOoTextDocument)document) , cont.getTables());
		mergeMaps(out, getText(document, cont.getParagraphs()));
		mergeMaps(out, getMarker((OOoTextDocument)document, cont.getMarkers()));
		mergeMaps(out, getFrame((OOoTextDocument)document, cont.getFrames() ));
		mergeMaps(out, getGraphic((OOoTextDocument)document, cont.getGraphics()));
		return out;
	}


	public TextDocumentComponent locateComponent(TextDocument document, TextContentDescriptor descriptor)
	{
		if (descriptor instanceof NameHintMarkerDescriptor)
		{
			Map in   = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
			Map out = getMarker((OOoTextDocument)document, in);
			if(null != out)
			{
				if(! out.isEmpty())
				{
					return (TextDocumentComponent)out.get(DEFAULT_KEY);
				}
			}
			return null;
		}
		else if (descriptor instanceof AbstractTableDescriptor) 
		{
			return getTable(document, (AbstractTableDescriptor)descriptor);
		}
		else if (descriptor instanceof AbstractSectionDescriptor)
		{
			Map in = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
			Map out = getText(document, in);
			logger.debug("found " + out.get(DEFAULT_KEY));
			return (TextDocumentComponent) out.get(DEFAULT_KEY);
		}
		else if (descriptor instanceof GraphicDescriptor)
		{
			Map in = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
			Map out = getGraphic((OOoTextDocument)document, in);
			return (TextDocumentComponent)out.get(DEFAULT_KEY);
		}
		else if (descriptor instanceof FrameDescriptor)
		{ 
			Map in = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new  DescriptorWrapper(descriptor, true));
			Map out = getFrame((OOoTextDocument)document, in);
			return (TextDocumentComponent)out.get(DEFAULT_KEY);
		}
		else throw new RuntimeException("Unknown Descriptor in OOOTextContentLocator.locateComponent :"+descriptor.getClass());
	}

	public List collectComponents(TextDocument document, TextContentDescriptor descriptor)
	{
		if (descriptor instanceof NameHintMarkerDescriptor)
		{
			Map in   = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false));
			Map out = getMarker((OOoTextDocument)document, in);
			if(null != out)
			{
				if(! out.isEmpty())
				{
					List l = (List)out.get(DEFAULT_KEY);
					return (l == null) ? new LinkedList() : l;
				}
			}
			return new LinkedList();
		}
		else if (descriptor instanceof AbstractTableDescriptor)
		{ 
			return collectTables(document, (AbstractTableDescriptor)descriptor);
		}
		else if (descriptor instanceof AbstractSectionDescriptor) 
		{
			Map in = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false));
			Map out = getText(document, in);
			return (List)out.get(DEFAULT_KEY);
		}
		else if (descriptor instanceof GraphicDescriptor)
		{
			Map in   = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false));
			Map out = getGraphic((OOoTextDocument)document, in);
			if(null != out)
			{
				if(! out.isEmpty())
				{
					List l = (List)out.get(DEFAULT_KEY);
					return (l == null) ? new LinkedList() : l;
				}
			}
			return new LinkedList();
		}
		else if (descriptor instanceof FrameDescriptor)
		{
			Map in = new HashMap();
			in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false));
			Map out = getFrame((OOoTextDocument)document, in);
			List l = (List)out.get(DEFAULT_KEY);
			return (l == null) ? new LinkedList() : l;
		}
		throw new RuntimeException("Unknown Descriptor in OOOTextContentLocator.collectComponents :"+descriptor.getClass());
	}



	// ################################################################################################


	private TextTable getTable(TextDocument document, AbstractTableDescriptor descriptor)
	{
		Map in = new HashMap();
		in.put(DEFAULT_KEY,  new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
		Map out = getTable(((OOoTextDocument)document), in);
		if (out != null && ! out.isEmpty())
		{
			return (TextTable)out.get(DEFAULT_KEY);
		}

		return null;
	}

	private List collectTables(TextDocument document, AbstractTableDescriptor descriptor)
	{
		Map in = new HashMap();
		in.put(DEFAULT_KEY, new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false));
		Map out = getTable((OOoTextDocument) document, in);
		List l = (List) out.get(DEFAULT_KEY);
		return (l == null ) ? new ArrayList() : l;
	}


	// -----------------------------------

	private XText getCellTextHandle(XTextTable TT1, String CellName)
	{
		if (CellName != null)
		{
			XText oTableText = (XText) UnoRuntime.queryInterface(XText.class, TT1.getCellByName(CellName));
			return oTableText;
		}
		return(null);
	}

	// TODO Xpropertyset in OooTextTableCell  speichern
	// Beim Parsen der Zelle das gecachte xpropertyset holen und damit die virtuelle Zelle aufbauen

	private Map getTable(OOoTextDocument doc,  Map descs)
	{    
		Map out                             = new HashMap();
		if (descs.isEmpty()) return out;
		XTextTablesSupplier xTablesSupplier = (XTextTablesSupplier) UnoRuntime.queryInterface(XTextTablesSupplier.class, doc.getXModel());
		XNameAccess xNamedTables            = xTablesSupplier.getTextTables();
		XIndexAccess xIndexedTables         = (XIndexAccess) UnoRuntime.queryInterface(XIndexAccess.class, xNamedTables);
		for (int i = 0; i < xIndexedTables.getCount(); i++) 
		{
			Object table = null;
			try
			{
				table = xIndexedTables.getByIndex(i);
				if (table != null)
				{
					XTextTable texttable = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, table);
					OOoTextTable ooTable= new OOoTextTable(doc, texttable);

					Iterator it = descs.entrySet().iterator();
					Map cellPropertyCache = new HashMap();
					while (it.hasNext())
					{
						Map.Entry entry         = (Map.Entry) it.next();
						Object key                 = entry.getKey();
						DescriptorWrapper dw = (DescriptorWrapper) entry.getValue();
						if (! dw.isLocate() && out.get(key) == null) out.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird  
						AbstractTableDescriptor desc  = (AbstractTableDescriptor) dw.getDescriptor();

						if (desc instanceof AbstractTableDescriptor)
						{
							int row = desc.getRow();
							int col = desc.getColumn();

							if (row == -1 || col == -1)
							{
								boolean ret = checkAllCells(cellPropertyCache, ooTable, out, desc, dw.isLocate(), key);
								if (ret)
								{
									it.remove();
									break;
								}
							}
							else
							{
								boolean ret = false;
								if (desc instanceof StyleTableDescriptor) ret = checkCell(cellPropertyCache, row, col, ooTable, out, (StyleTableDescriptor)desc, dw.isLocate(), key);
								else  if(desc instanceof CellTextTableDescriptor) ret = checkCell(cellPropertyCache, row, col, ooTable, out, (CellTextTableDescriptor)desc, dw.isLocate(), key);
								else throw new RuntimeException("Unsupported TableDescriptor " + desc.getClass());

								if (ret)
								{
									it.remove();
									break;
								}
							}
						}
						else throw new RuntimeException("Unsupported TableDescriptor " + desc.getClass());
					}
					// if (! out.isEmpty() && returnFirst) return out;
				}
			}
			catch (IndexOutOfBoundsException e) 
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e) 
			{
				logger.error("Error", e );
			}
		}
		return out;         
	}


	private boolean checkAllCells(Map cellPropertyCache, OOoTextTable ooTable, Map out, AbstractTableDescriptor st, boolean returnFirst, Object key)
	{
		int numrows = ooTable.getNumberOfRows();
		int numcols   = ooTable.getNumberOfColumns();

		for(int rowIndex=0; rowIndex<numrows; rowIndex++)
		{
			for(int colIndex=0; colIndex<numcols; colIndex++)
			{
				boolean ret = false;
				if(st instanceof StyleTableDescriptor)
				{
					ret = checkCell(cellPropertyCache, rowIndex, colIndex, ooTable, out, (StyleTableDescriptor)st, returnFirst, key);
				}
				else  if (st instanceof CellTextTableDescriptor) ret = checkCell(cellPropertyCache, rowIndex, colIndex, ooTable, out, (CellTextTableDescriptor)st, returnFirst, key);
				else throw new RuntimeException("Unsupported TableDescriptor " + st.getClass());
				if (ret) return true;
			}          
		}
		return false;
	}

	private boolean checkCell(Map cellPropertyCache, int row, int col, OOoTextTable ooTable, Map out, StyleTableDescriptor st, boolean returnFirst, Object key)
	{
		XPropertySet set = (XPropertySet)cellPropertyCache.get("style"+row+"|"+col);
		if ( null == set)
		{
			OOoTextTableCell cell = (OOoTextTableCell)ooTable.getCell(row, col);
			set = cell.getTextCursorProperties(ooTable.getXTextTable(), row, col);
			if (null != set)
			{
				cellPropertyCache.put("style"+row+"|"+col, set);
			}
		}

		if (containsStyle(set, st.getStyleName()))
		{
			//out.put(key, ooTable);
			/*boolean ret =*/ addTextDocumentComponent(ooTable, returnFirst, out , key);
			if (returnFirst) return true;
		}
		return false;
	}

	private boolean checkCell(Map cellPropertyCache, int row, int col, OOoTextTable ooTable, Map out, CellTextTableDescriptor st, boolean returnFirst, Object key)
	{
		String text = (String)cellPropertyCache.get("cell"+row+"|"+col);
		if ( null == text)
		{
			OOoTextTableCell cell = (OOoTextTableCell)ooTable.getCell(row, col);
			text = cell.getPlainText();
			cellPropertyCache.put("cell"+row+"|"+col, text);
		}

		if (null != text && text.equals(st.getText()))
		{
			/*boolean ret =*/ addTextDocumentComponent(ooTable, returnFirst, out , key);
			if (returnFirst) return true;
		}
		return false;
	}

	private Map getGraphic(OOoTextDocument oooDoc,  Map descs)
	{
		if(descs.isEmpty()) return new HashMap();

		Map graphics                                                                 = new HashMap();
		XTextGraphicObjectsSupplier xTextGraphicObjectsSupplier = (XTextGraphicObjectsSupplier) UnoRuntime.queryInterface(XTextGraphicObjectsSupplier.class, oooDoc.getXTextDocument());    
		XNameAccess xNameAccess                                            = xTextGraphicObjectsSupplier.getGraphicObjects();    
		Object oGfx;
		try
		{
			Iterator it                                                                   = descs.entrySet().iterator();
			while (it.hasNext())
			{
				Map.Entry entry                                                       = (Map.Entry) it.next();
				Object key                                                               = entry.getKey();
				DescriptorWrapper dw                                              = (DescriptorWrapper)entry.getValue();
				GraphicDescriptor descGraphic                                  = (GraphicDescriptor) dw.getDescriptor();
				boolean returnFirst                                                 = dw.isLocate();

				if (! dw.isLocate() && graphics.get(key) == null) graphics.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird

				if (descGraphic.getName() != null)
				{
					oGfx                                                                      = xNameAccess.getByName(descGraphic.getName());
					XServiceInfo xInfo                                                   = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, oGfx);
					if (xInfo.supportsService("com.sun.star.text.TextGraphicObject"))
					{
						XTextContent gfxTextContent                               = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, oGfx);
						boolean ret = addTextDocumentComponent(new OOoTextGraphic(oooDoc.getXModel(), gfxTextContent), returnFirst, graphics, key);
						if (ret) break;
					}
				}
				else
				{
					String[] names =  xNameAccess.getElementNames();
					for(int i=0; i < names.length; i++)
					{
						oGfx                                                                      = xNameAccess.getByName(names[i]);
						XServiceInfo xInfo                                                   = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, oGfx);
						if (xInfo.supportsService("com.sun.star.text.TextGraphicObject"))
						{
							XTextContent gfxTextContent                              = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, oGfx);
							XPropertySet xGfxProps                                      = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, gfxTextContent);

							String url                                                           = (String)(xGfxProps.getPropertyValue("GraphicURL"));
							Boolean print                                                      = (Boolean)(xGfxProps.getPropertyValue("Print"));

							int gfxWidth                                                       = -1;
							int gfxHeight                                                       = -1;
							com.sun.star.awt.Size size                                    = (com.sun.star.awt.Size)(xGfxProps.getPropertyValue("Size"));
							if (size != null)
							{
								gfxWidth = size.Width;
								gfxHeight = size.Height;
							}
							else
							{
								Integer oheight = (Integer)(xGfxProps.getPropertyValue("Height"));
								Integer owidth = (Integer)(xGfxProps.getPropertyValue("Width"));
								if ( (oheight != null) && (owidth != null))
								{
									gfxWidth = owidth.intValue();
									gfxHeight = oheight.intValue();                
								}
							}

							if(null != descGraphic.getFileName() && descGraphic.getFileName().equals(url) && 
									null != descGraphic.getPrintable() && descGraphic.getPrintable().booleanValue() == print.booleanValue() &&
									null != descGraphic.getWidth() && descGraphic.getWidth().intValue() == gfxWidth && 
									null != descGraphic.getHeight() && descGraphic.getHeight().intValue() == gfxHeight)
							{
								boolean ret = addTextDocumentComponent(new OOoTextGraphic(oooDoc.getXModel(), gfxTextContent), returnFirst, graphics, key);
								if (ret) break;
							}
						}
					}
				}
			}
		}
		catch(UnknownPropertyException e)
		{
			logger.warn("Error findGraphic(tdoc=" + oooDoc  , e );
		}
		catch (NoSuchElementException e)
		{
			logger.warn("Error findGraphic(tdoc=" + oooDoc  , e );
		}
		catch (WrappedTargetException e)
		{
			logger.warn("Error findGraphic(tdoc=" + oooDoc  , e );
		}

		return graphics;
	}


	private Map getMarker(OOoTextDocument document, Map descs)
	{
		if(descs.isEmpty()) return new HashMap();
		Map markers                                           = new HashMap();
		XTextFieldsSupplier xTextFieldsSupplier      = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, document.getXModel());
		if (xTextFieldsSupplier == null) return null;
		XEnumerationAccess xEnumerationAccess  = xTextFieldsSupplier.getTextFields();
		if (xEnumerationAccess == null) return null;
		XEnumeration xTextFieldEnum                   = xEnumerationAccess.createEnumeration();
		if (xTextFieldEnum == null) return null;

		while (xTextFieldEnum.hasMoreElements()) 
		{
			try
			{
				Object textfieldobject                           = xTextFieldEnum.nextElement();
				XTextContent textfield                         = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldobject);

				XServiceInfo xInfo                               = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textfield);
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					XPropertySet xPropertySet                 = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
					String fieldname                                 = (String)(xPropertySet.getPropertyValue("PlaceHolder"));            
					String fieldhint                                   = (String)(xPropertySet.getPropertyValue("Hint"));            


					Iterator it                                          = descs.entrySet().iterator();
					while (it.hasNext())
					{
						Map.Entry entry                            = (Map.Entry) it.next();
						Object key                                    = entry.getKey();
						DescriptorWrapper dw                   = (DescriptorWrapper) entry.getValue();
						NameHintMarkerDescriptor desc      = (NameHintMarkerDescriptor) dw.getDescriptor();
						if (! dw.isLocate() && markers.get(key) == null) markers.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird
						boolean returnFirst                      = dw.isLocate(); 
						String name                                  = desc.getName();
						String hint                                    = desc.getHint();

						if (name != null)
						{
							if (hint != null)
							{
								// nach name & hint suchen
								if ( (name.equalsIgnoreCase(fieldname)) && (hint.equalsIgnoreCase(fieldhint)))
								{

									boolean ret = addTextDocumentComponent(new OOoTextMarker(document.getXModel(), textfield), returnFirst, markers, key);
									if (ret ) break;
								}
							} // nur nach name suchen
							else if (name.equalsIgnoreCase(fieldname))
							{
								boolean ret = addTextDocumentComponent(new OOoTextMarker(document.getXModel(), textfield), returnFirst, markers, key);
								if (ret ) break;
							}
						}
						else
						{
							if (hint != null)
							{
								// nur nach hint suchen                
								if (hint.equalsIgnoreCase(fieldhint))
								{
									boolean ret = addTextDocumentComponent(new OOoTextMarker(document.getXModel(), textfield), returnFirst, markers, key);
									if (ret ) break;
								}
							}
							else
							{
								// nach nichts suchen... Sinn? //Ja, wenn man alle Marker haben will ;-)
								boolean ret = addTextDocumentComponent(new OOoTextMarker(document.getXModel(), textfield), returnFirst, markers, key);
								if (ret ) break;
							}
						}
					}
				}
			}
			catch (NoSuchElementException e)
			{          
				logger.error("Error", e );
			}
			catch (WrappedTargetException e)
			{         
				logger.error("Error", e );
			} 
			catch (UnknownPropertyException e)
			{        
				logger.error("Error", e );
			} 
		}
		return markers;
	}



	public static  boolean addTextDocumentComponent(TextDocumentComponent comp, boolean returnFirst, Map out, Object key)
	{
		if (returnFirst)
		{
			out.put(key, comp);
			return true;
		}
		((List)out.get(key)).add( comp );
		return false;
	}


	private XTextDocument getTextDocumentFromComponent(XComponent comp)
	{
		return(XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, comp);    
	}


	private Map getText(TextDocument document, Map descriptors)
	{
		if (descriptors.isEmpty()) return new HashMap();

		OOoTextDocument otd   = (OOoTextDocument)document;
		XTextDocument tdoc      = getTextDocumentFromComponent(otd.getXModel());
		XText xText                  = tdoc.getText();

		Map all                          = new HashMap();

		if (checkMap(descriptors, AbstractSectionDescriptor.SECTION_PARAS))
		{
			Map list                         =  collectTextParagraphs(otd, xText, otd.getXModel(), descriptors);
			all.putAll( list );
		}

		if (checkMap(descriptors, AbstractSectionDescriptor.SECTION_TABLES))
		{
			Map tablelist                  =  collectTextParagraphsInTables(otd, otd.getXModel(), descriptors);
			mergeMaps(all, tablelist);
		}

		if (checkMap(descriptors, AbstractSectionDescriptor.SECTION_FRAMES))
		{
			Map framelist                =  collectTextParagraphsInFrames(otd, otd.getXModel(), descriptors);
			mergeMaps(all, framelist );
		}    
		return all;
	}

	private static boolean checkMap(Map in, int section)
	{
		Iterator it = in.values().iterator();
		while (it.hasNext())
		{
			AbstractSectionDescriptor  desc =    (AbstractSectionDescriptor) ((DescriptorWrapper) it.next()).getDescriptor();
			if (desc.searchForSection(section) || desc.searchForAllSections()) return true;
		}
		return false;
	}


	private static void mergeMaps(Map all, Map in)
	{
		Iterator it = in.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry entry = (Map.Entry) it.next();
			Object key      = entry.getKey();
			Object value    = entry.getValue();

			Object allValue = all.get(key);
			if (allValue != null && allValue instanceof List)
			{
				((List)allValue).addAll( (List)value);
			}
			else all.put(key, value);
		}
	}


	// ---------------------------------------------------------  


	private Map collectTextParagraphs(OOoTextDocument doc, XText xText, XModel xmodel, Map descs)
	{
		if (descs.isEmpty()) return new HashMap();
		XEnumerationAccess xParaAccess   = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
		XEnumeration xParaEnum              = xParaAccess.createEnumeration();
		Map out                                       = new HashMap();
		Map lastMatchedDescs                   = new HashMap();
		while (xParaEnum.hasMoreElements()) 
		{
			Object paraobj;
			try
			{
				paraobj                                = xParaEnum.nextElement();
				XServiceInfo xInfo                 = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);        
				if (xInfo.supportsService("com.sun.star.text.Paragraph"))
				{
					XTextContent content         = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
					OOoTextParagraph oooPara = new OOoTextParagraph(doc, xmodel, content);

					/*boolean ret =*/  checkParagraph(oooPara, descs, out, lastMatchedDescs);
					//if (ret && returnFirstParagraph) return out; // es wurde der erste Paragraph im Dokument gefunden, also wird die Suche beendet
				}        
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error", e );
			}
		}
		return out;
	}

	/**
	 * @param oooPara
	 * @param descs
	 * @param out
	 * @param lastMatchedDescs
	 * @return true wenn der erste Paragraph gefunden wurde und die Methode verlassen werden soll 
	 */
	private boolean checkParagraph(OOoTextParagraph oooPara, Map descs, Map out, Map lastMatchedDescs)
	{
		Iterator it                    = descs.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry entry          = (Map.Entry) it.next();
			Object key               = entry.getKey();
			DescriptorWrapper dw     = (DescriptorWrapper) entry.getValue();
			if (! dw.isLocate() && out.get(key) == null)
			{
				out.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird
			}
			TextDescriptor desc = (TextDescriptor) dw.getDescriptor();
			if (desc instanceof FormattedTextDescriptor)
			{
				FormattedTextDescriptor descFor = (FormattedTextDescriptor) desc;
				if ( oooPara.containsFormattedText(descFor.getText(), descFor.isBold(), descFor.isItalic(), descFor.isUnderline()))
				{
					if (dw.isLocate())
					{
						out.put(key, oooPara);
						it.remove();
					}
					else ((List)out.get(key)).add(oooPara);
				}
			}
			else
				if (desc instanceof StyledParagraphDescriptor)
				{
					StyledParagraphDescriptor descPara = (StyledParagraphDescriptor) desc;
					if ( oooPara.containsStyle(descPara.getStyle(), descPara.getText()))
					{
						if (dw.isLocate())
						{
							out.put(key, oooPara);
							it.remove();
						}
						else ((List)out.get(key)).add(oooPara);
					}
				}
		}
		return false;
	}

	private Map collectTextParagraphsInFrames(OOoTextDocument otd, XModel xModel, Map descs)
	{
		if (descs.isEmpty()) return new HashMap();
		logger.debug("collectTextParagraphsInFrames()");          
		Map out                              = new HashMap();
		XTextFramesSupplier xTextFramesSupplier = (XTextFramesSupplier)UnoRuntime.queryInterface(XTextFramesSupplier.class, xModel);
		if (xTextFramesSupplier == null) return null;
		XNameAccess xNameAccess                 = xTextFramesSupplier.getTextFrames();
		if (xNameAccess == null) return null;
		String[] sTextFieldNames                = xNameAccess.getElementNames();
		if (sTextFieldNames == null) return null;
		for(int i=0; i<(sTextFieldNames.length); i++)
		{
			try
			{
				Object textframeobject = xNameAccess.getByName(sTextFieldNames[i]);
				XTextFrame textframe = (XTextFrame) UnoRuntime.queryInterface(XTextFrame.class, textframeobject);        
				XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textframe);
				if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
				{
					XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, textframe.getText());
					XEnumeration xParaEnum = xParaAccess.createEnumeration();
					Map lastMatchedDescs                   = new HashMap();
					while (xParaEnum.hasMoreElements()) 
					{
						Object paraobj;
						try
						{
							paraobj                                = xParaEnum.nextElement();
							XServiceInfo xParaInfo           = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);        
							if (xParaInfo.supportsService("com.sun.star.text.Paragraph"))
							{
								XTextContent content         = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
								OOoTextParagraph oooPara = new OOoTextParagraph(otd, xModel, content);

								/*boolean ret =*/  checkParagraph(oooPara, descs, out, lastMatchedDescs);
								//if (ret && returnFirstParagraph) return out;
							}        
						}
						catch (NoSuchElementException e)
						{
							logger.error("Error", e );
						}
						catch (WrappedTargetException e)
						{
							logger.error("Error", e );
						}
					}
				}        
			}
			catch (NoSuchElementException e) 
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e) 
			{
				logger.error("Error", e );
			} 
		}

		return out;
	}

	private Map collectTextParagraphsInTables(OOoTextDocument otd, XModel xModel, Map descs)
	{
		if (descs.isEmpty()) return new HashMap();
		XTextTablesSupplier xTablesSupplier = (XTextTablesSupplier) UnoRuntime.queryInterface(XTextTablesSupplier.class, xModel);
		XNameAccess xNamedTables           = xTablesSupplier.getTextTables();
		XIndexAccess xIndexedTables         = (XIndexAccess) UnoRuntime.queryInterface(XIndexAccess.class, xNamedTables);
		Map out                                        = new HashMap();
		for (int i = 0; i < xIndexedTables.getCount(); i++) 
		{
			Object table = null;
			try
			{
				table = xIndexedTables.getByIndex(i);
				if (table != null)
				{
					XTextTable texttable = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, table);
					if (texttable != null)            
					{
						String[] cellNames = texttable.getCellNames();

						for(int cellIndex=0; cellIndex<cellNames.length; cellIndex++)
						{
							XText xtext = getCellTextHandle(texttable, cellNames[cellIndex]);        
							if (xtext != null)
							{
								XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xtext);
								XEnumeration xParaEnum            = xParaAccess.createEnumeration();
								Map lastMatchedDescs                   = new HashMap();
								while (xParaEnum.hasMoreElements()) 
								{
									Object paraobj;
									try
									{
										paraobj                                = xParaEnum.nextElement();
										XServiceInfo xParaInfo           = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);        
										if (xParaInfo.supportsService("com.sun.star.text.Paragraph"))
										{

											XTextContent content         = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
											OOoTextParagraph oooPara = new OOoTextParagraph(otd, xModel, content, xtext);
											/*boolean ret =*/  checkParagraph(oooPara, descs, out, lastMatchedDescs);
											//if (ret && returnFirstParagraph) return out;
										}        
									}
									catch (NoSuchElementException e)
									{
										logger.error("Error", e );
									}
									catch (WrappedTargetException e)
									{
										logger.error("Error", e );
									}
								}
							}
						}
					}
				}
			}
			catch (IndexOutOfBoundsException e) 
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e) 
			{
				logger.error("Error", e );
			}
		}

		return out;
	}


	private List collectTextParagraphs(OOoTextDocument otd, XText xText, XModel xmodel, String text, String style, boolean returnFirstParagraph)
	{
		logger.debug("collectTextParagraphs "+style + " returnFirst " + returnFirstParagraph);
		List list                                       = new ArrayList();
		XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
		XEnumeration xParaEnum            = xParaAccess.createEnumeration();
		while (xParaEnum.hasMoreElements()) 
		{
			Object paraobj;
			try
			{
				paraobj = xParaEnum.nextElement();

				XServiceInfo xInfo                 = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);        
				if (xInfo.supportsService("com.sun.star.text.Paragraph"))
				{
					XTextContent content         = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
					if (findTextInParagraph(content, text, style))
					{            
						list.add(new OOoTextParagraph(otd, xmodel, content));
						if (returnFirstParagraph) return list;
					}
				}        
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error", e );
			}
		}
		return list;
	}


	private boolean findTextInParagraph(XTextContent content, String sText, String sStyle) 
	{
		XTextRange xTextRange      = content.getAnchor();
		XText xDocumentText         = xTextRange.getText();
		XTextCursor xModelCursor  = xDocumentText.createTextCursorByRange(xTextRange.getStart());
		XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);

		String stylename = null;
		try
		{
			stylename = (String)(xCursorProps.getPropertyValue("ParaStyleName"));      
			if (stylename != null)
			{
				if (stylename.equals(sStyle)) return true;
			}
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}

		// create another enumeration to get all text portions of the paragraph
		XEnumerationAccess xParaEnumerationAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, content);
		XEnumeration xTextPortionEnum                    = xParaEnumerationAccess.createEnumeration();

		while(xTextPortionEnum.hasMoreElements()) 
		{
			try
			{        
				Object portionobj = xTextPortionEnum.nextElement();
				XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, portionobj);

				if (xInfo.supportsService("com.sun.star.text.TextPortion")) 
				{
					XPropertySet portionPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);
					String portiontype = (String)(portionPropertySet.getPropertyValue("TextPortionType"));
					if ("Text".equals(portiontype))
					{
						XTextRange xTextPortion = (XTextRange) UnoRuntime.queryInterface(XTextRange.class, portionobj);                                

						String portiontext = xTextPortion.getString();            
						if (sText != null)
						{
							if (sText.toUpperCase().indexOf(portiontext.toUpperCase()) != -1)
							{
								if (sStyle != null)
								{
									try
									{
										String charstylename = (String)(portionPropertySet.getPropertyValue("CharStyleName"));
										if (sStyle.equals(charstylename)) return true;
									}
									catch(UnknownPropertyException upe) {}
									catch(WrappedTargetException wte) {}

									try
									{
										String parastylename = (String)(portionPropertySet.getPropertyValue("ParaStyleName"));
										if (sStyle.equals(parastylename)) return true;
									}
									catch(UnknownPropertyException upe) {}
									catch(WrappedTargetException wte) {}
								}
								else return true;
							}
						}
						else
						{
							if (sStyle != null)
							{
								try
								{
									String charstylename = (String)(portionPropertySet.getPropertyValue("CharStyleName"));
									if (sStyle.equals(charstylename)) return true;
								}
								catch(UnknownPropertyException upe) {}
								catch(WrappedTargetException wte) {}

								try
								{
									String parastylename = (String)(portionPropertySet.getPropertyValue("ParaStyleName"));
									if (sStyle.equals(parastylename)) return true;
								}
								catch(UnknownPropertyException upe) {}
								catch(WrappedTargetException wte) {}
							}
							else 
							{
								return true;              
							}
						}
					}
				}        
			}
			catch(WrappedTargetException e) 
			{
				logger.error("Error", e );
			}
			catch (NoSuchElementException e) 
			{
				logger.error("Error", e );
			}
			catch (UnknownPropertyException e) 
			{
				logger.error("Error", e );
			}
		}
		return false;
	}


	private Map getFrame(OOoTextDocument document, Map descs)
	{
		if(descs.isEmpty()) return new HashMap();
		XTextFramesSupplier xTextFramesSupplier = (XTextFramesSupplier)UnoRuntime.queryInterface(XTextFramesSupplier.class, document.getXModel());
		if (xTextFramesSupplier == null) return null;
		XNameAccess xNameAccess                     = xTextFramesSupplier.getTextFrames();
		if (xNameAccess == null) return null;
		String[] sTextFieldNames                         = xNameAccess.getElementNames();
		if (sTextFieldNames == null) return null;
		Map frames                                            = new HashMap();

		for(int i=0; i<(sTextFieldNames.length); i++)
		{
			try
			{
				Object textframeobject                      = xNameAccess.getByName(sTextFieldNames[i]);
				XTextFrame textframe                       = (XTextFrame) UnoRuntime.queryInterface(XTextFrame.class, textframeobject);
				XServiceInfo xInfo                             = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textframe);
				if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
				{
					List paras = null;
					Iterator it                                    = descs.entrySet().iterator();
					while (it.hasNext())
					{
						Map.Entry entry                        = (Map.Entry) it.next();
						Object key                                = entry.getKey();
						DescriptorWrapper dw               = (DescriptorWrapper) entry.getValue();
						FrameDescriptor desc                = (FrameDescriptor) dw.getDescriptor();
						if (! dw.isLocate() && frames.get(key) == null) frames.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird
						boolean returnFirst                  = dw.isLocate();

						TextContentDescriptor textDesc = desc.getDescriptor();

						if (null != textDesc)
						{
							XText xText                       = textframe.getText();
							if (textDesc instanceof StyledParagraphDescriptor) 
							{
								if (null == paras)
								{
									paras                          = collectTextParagraphs(document, xText, document.getXModel(), ((StyledParagraphDescriptor)textDesc).getText(),  ((StyledParagraphDescriptor)textDesc).getStyle(), true); 
								}

								if (null != paras && paras.size() > 0)
								{
									boolean ret                 = addTextDocumentComponent(new OOoTextFrame(document, textframe), returnFirst, frames, key);
									if ( ret ) break; 
								}              
							}
							else throw new RuntimeException("Unsupported embedded descriptor \"" + textDesc.getClass() + "\" in FrameDescriptor");
						}
						else if (isFrameValid(textframe, desc))
						{
							boolean ret = addTextDocumentComponent(new OOoTextFrame(document, textframe), returnFirst, frames, key);
							if ( ret ) break;
						}
					}
				}
			}
			catch (NoSuchElementException e) 
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e) 
			{
				logger.error("Error", e );
			} 
		}
		return frames;
	}

	private static boolean containsStyle(XPropertySet set, String styleName)
	{
		//logger.debug("set " + set + "isNull: " +(null == set));
		if (null == set) return false;
		String style = null;
		try
		{
			style = (String)(set.getPropertyValue("ParaStyleName"));
			if (style != null)
			{
				if (style.equals(styleName)) return(true);          
			}

			style = (String)(set.getPropertyValue("CharStyleName"));
			if (style != null)
			{
				if (style.equals(styleName)) return(true);          
			}
		}
		catch (UnknownPropertyException e) 
		{
			logger.error("Error", e);
		}
		catch (WrappedTargetException e) 
		{
			logger.error("Error", e);
		}            
		return false;
	}


	private boolean isFrameValid(XTextFrame textframe, FrameDescriptor descriptor)
	{
		XNamed xNamed = (XNamed) UnoRuntime.queryInterface(XNamed.class, textframe);
		if (xNamed != null)
		{
			String frameName = xNamed.getName();          
			if (frameName.equalsIgnoreCase(descriptor.getName())) return true;
		}
		return false;
	}

	public TextMarker getNextTextMarker(TextDocument pDocument)
	{
		// TODO implement
		return null;
	}
}