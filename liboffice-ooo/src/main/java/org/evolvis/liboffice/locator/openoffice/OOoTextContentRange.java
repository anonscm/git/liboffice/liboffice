/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 21.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator.openoffice;

import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.TextContentRange;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

import com.sun.star.frame.XController;
import com.sun.star.frame.XModel;
import com.sun.star.text.XText;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextContentRange implements TextContentRange
{
  private XModel m_oXModel;
  private XTextCursor m_oXTextCursor = null;
  private OOoTextDocument doc;
  
  public OOoTextContentRange(OOoTextDocument doc)
  {
      this.doc = doc;
      m_oXModel = doc.getXModel();
  }
  
  public OOoTextContentRange(OOoTextDocument doc, XModel xModel, XTextCursor cursor)
  {
    this.doc = doc;
    m_oXModel = xModel;
    m_oXTextCursor = cursor;
  }
  
  public OOoTextContentRange(OOoTextDocument doc, XModel xModel, XTextRange range)
  {
    this.doc = doc;
    m_oXModel = xModel;
    XText xDocumentText = range.getText();
    m_oXTextCursor = xDocumentText.createTextCursorByRange(range.getStart());
  }

  public OOoTextContentRange(OOoTextDocument doc, XModel xModel, TextContentLocation start)
  {
    this.doc      = doc;
    m_oXModel = xModel;
    m_oXTextCursor = ((OOoTextContentLocation)start).getXTextCursor();    
  }
  
  public TextContentLocation getStartLocation()
  {
    return new OOoTextContentLocation(m_oXModel, m_oXTextCursor.getStart());
  }

  public TextContentLocation getEndLocation()
  {
    return new OOoTextContentLocation(m_oXModel, m_oXTextCursor.getEnd());
  }

  public void select(boolean append)
  {   
    XController xController                                   = m_oXModel.getCurrentController();
    XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
    XTextViewCursor xViewCursor                        = xViewCursorSupplier.getViewCursor();
    xViewCursor.gotoRange(m_oXTextCursor, append);
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.locator.TextContentRange#setRange(de.tarent.documents.locator.TextContentLocation, de.tarent.documents.locator.TextContentLocation)
   */
  public void setRange(TextContentLocation start, TextContentLocation end)
  {
    XText xDocumentText = ((OOoTextContentLocation)start).getXTextCursor().getText();
    if (m_oXTextCursor == null)
    {
      m_oXTextCursor = xDocumentText.createTextCursorByRange(((OOoTextContentLocation)start).getXTextCursor().getStart());
    }

    XTextRange range =  ((OOoTextContentLocation)start).getXTextCursor().getStart(); 
    XTextCursor cursor = xDocumentText.createTextCursorByRange(range);
    m_oXTextCursor.gotoRange(cursor, false);
    m_oXTextCursor.gotoRange(xDocumentText.createTextCursorByRange(((OOoTextContentLocation)end).getXTextCursor().getEnd()), true);
  }

    public String getPlainText()
  {
    return m_oXTextCursor.getString();
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.locator.TextContentRange#getStyledText()
   */
  public VirtualStyledText getStyledText()
  {
      throw new UnsupportedOperationException("Method is not implemented");
  }

  public void copy()
  {
      doc.copy();
  }
  
  public void select()
  {
      select( false );
  }
}
