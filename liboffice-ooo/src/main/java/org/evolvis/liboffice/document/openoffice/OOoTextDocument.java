/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.connection.openoffice.OOoOfficeConnection;
import org.evolvis.liboffice.controller.KnownOfficeFileFormats;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.controller.OfficeWindowListener;
import org.evolvis.liboffice.controller.openoffice.OOoDeferredDocumentCloser;
import org.evolvis.liboffice.document.AbstractTextDocument;
import org.evolvis.liboffice.document.Span;
import org.evolvis.liboffice.document.TextBookmark;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextFrame;
import org.evolvis.liboffice.document.TextGraphic;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.document.TextStyle;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.TextTableCell;
import org.evolvis.liboffice.document.TextVariable;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.TextContentLocator;
import org.evolvis.liboffice.locator.TextContentRange;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocation;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocator;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentRange;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.officefileformats.UnsupportedOfficeFileTypeException;
import org.evolvis.liboffice.parser.openoffice.OOoTextDocumentParser;
import org.evolvis.liboffice.virtualdocument.VirtualDocumentInformation;
import org.evolvis.liboffice.virtualdocument.VirtualDocumentInformationStorage;
import org.evolvis.liboffice.virtualdocument.VirtualMargin;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.VirtualTextFrame;
import org.evolvis.liboffice.virtualdocument.graphic.VirtualGraphic;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualPageBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraphBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextFont;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortion;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyleType;

import com.sun.star.awt.FontDescriptor;
import com.sun.star.awt.FontSlant;
import com.sun.star.awt.XDevice;
import com.sun.star.awt.XTopWindow;
import com.sun.star.awt.XTopWindowListener;
import com.sun.star.awt.XWindow;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.ElementExistException;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.container.XNameContainer;
import com.sun.star.document.MacroExecMode;
import com.sun.star.document.UpdateDocMode;
import com.sun.star.document.XDocumentInfo;
import com.sun.star.document.XDocumentInfoSupplier;
import com.sun.star.document.XDocumentInsertable;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XController;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XDispatch;
import com.sun.star.frame.XDispatchProvider;
import com.sun.star.frame.XFrame;
import com.sun.star.frame.XModel;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.DisposedException;
import com.sun.star.lang.EventObject;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.style.ParagraphAdjust;
import com.sun.star.style.XStyle;
import com.sun.star.style.XStyleFamiliesSupplier;
import com.sun.star.table.BorderLine;
import com.sun.star.table.TableBorder;
import com.sun.star.table.XCellRange;
import com.sun.star.text.ControlCharacter;
import com.sun.star.text.TableColumnSeparator;
import com.sun.star.text.XPageCursor;
import com.sun.star.text.XParagraphCursor;
import com.sun.star.text.XRelativeTextContentInsert;
import com.sun.star.text.XRelativeTextContentRemove;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextFieldsSupplier;
import com.sun.star.text.XTextFrame;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextTable;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.AnyConverter;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XInterface;
import com.sun.star.util.XCloseable;
import com.sun.star.util.XURLTransformer;
import com.sun.star.view.XPrintable;

import de.tarent.commons.utils.SystemInfo;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoTextDocument extends AbstractTextDocument implements OOoTextDocumentComponent
{
	private OOoOfficeConnection m_oOfficeConnection                    = null;
	private XModel m_oXModel                                           = null;  
	private boolean m_bIsLocked                                        = false;
	private XTextDocument m_oXTextDocument                             = null;
	private List m_oOfficeWindowListener                               = new ArrayList();  
	private OpenOfficeTopWindowListener m_oOpenOfficeTopWindowListener = null;
	private XTopWindow m_oTopWin                                       = null;
	private OOoTextDocumentParser parser                               = null;
	private final Logger logger                                        = Logger.getLogger(getClass());

	private XURLTransformer xParser;

	public final static int PASTE_FORMAT_WRITER 	= 85;
	public final static int PASTE_FORMAT_RTF		= 10;
	public final static int PASTE_FORMAT_HTML		= 51;
	public final static int PASTE_FORMAT_PLAIN	= 1;

	public void finalize() throws Throwable
	{
		super.finalize();
		setVisualUpdateActive(true);
		removeAllOfficeWindowListener();
	}

	public OOoTextDocument(OOoOfficeConnection connection) throws NoOfficeException
	{
		m_oOfficeConnection = connection;
		if (! m_oOfficeConnection.isConnected()) throw new NoOfficeException();
		refine();
	}

	private void refine() throws NoOfficeException
	{
		XComponent xcomponent =  getCurrentWriterDocument(m_oOfficeConnection.getXMultiServiceFactory());
		if (xcomponent == null) throw new NoOfficeException();
		m_oXTextDocument = ((XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, xcomponent));            
		if (m_oXTextDocument == null) throw new NoOfficeException();
		m_oXModel = (XModel) UnoRuntime.queryInterface(XModel.class, m_oXTextDocument);      
		if (m_oXModel == null) throw new NoOfficeException();
	}

	public OOoTextDocument(OOoOfficeConnection connection, XModel xmodel, XTextDocument xDoc)
	{
		m_oOfficeConnection = connection;
		m_oXModel           = xmodel;
		m_oXTextDocument    = xDoc;
	}

	public OOoTextDocument(XTextDocument oXTextDocument)
	{
		m_oOfficeConnection = null;
		m_oXTextDocument = oXTextDocument;
		m_oXModel = (XModel) UnoRuntime.queryInterface(XModel.class, m_oXTextDocument);      
	}

	public XModel getXModel()
	{
		if (m_oXModel == null)
		{
			if (m_oXTextDocument != null)
			{
				m_oXModel = (XModel) UnoRuntime.queryInterface(XModel.class, m_oXTextDocument);
			}
		}

		return m_oXModel;
	}

	public XTextDocument getXTextDocument()
	{
		if (m_oXTextDocument == null)
		{    
			if (m_oXModel != null)
			{
				m_oXTextDocument = (XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, m_oXModel);
			}      
		}

		return m_oXTextDocument;
	}


	public OOoOfficeConnection getOfficeConnection()
	{
		return m_oOfficeConnection;
	}

	public void setLocked(boolean locked)
	{
		m_bIsLocked = locked;
	}

	public boolean isLocked()
	{
		return m_bIsLocked;
	}

	public void setVisualUpdateActive(boolean active)
	{
		if (active)
		{
			if (isLocked())
			{
				getXModel().unlockControllers();
				m_bIsLocked = false;
			}
		}
		else
		{
			if (! isLocked())
			{
				getXModel().lockControllers();
				m_bIsLocked = true;
			}      
		}     
	}

	private XComponent getCurrentWriterDocument(XMultiServiceFactory oXMSF) 
	{
		if (oXMSF == null) return(null);    

		XComponent aDoc = null;
		try 
		{            
			XInterface oInterface = (XInterface) oXMSF.createInstance( "com.sun.star.frame.Desktop" );
			XDesktop xDesktop = ( XDesktop ) UnoRuntime.queryInterface( XDesktop.class, oInterface );
			
			XComponentLoader xComponentLoader = (XComponentLoader) UnoRuntime.queryInterface(XComponentLoader.class, xDesktop);
			
			PropertyValue[] oArgs = new PropertyValue[3];
			oArgs[0] = new PropertyValue();
			oArgs[0].Name = "UpdateDocMode";
			oArgs[0].Value = new Short(UpdateDocMode.FULL_UPDATE);

			oArgs[1] = new PropertyValue();
			oArgs[1].Name = "MacroExecutionMode";
			oArgs[1].Value = new Short(MacroExecMode.ALWAYS_EXECUTE_NO_WARN);
			oArgs[2] = new PropertyValue();
			oArgs[2].Name = "Hidden";
			oArgs[2].Value = new Boolean(true);
			
			aDoc = xComponentLoader.loadComponentFromURL("private:factory/swriter", "_blank", 0, oArgs);
			
			if (aDoc == null)
			{      
				aDoc = findDocument(xDesktop);
			}
		}        
		catch(Exception e)
		{
			logger.error("Error", e );
		}               
		return aDoc;
	}


	private XComponent findDocument(XDesktop oDesktop)
	{
		XEnumerationAccess xEnumerationAccess = oDesktop.getComponents();
		XEnumeration xDocumentEnum = xEnumerationAccess.createEnumeration();
		while (xDocumentEnum.hasMoreElements()) 
		{
			try
			{
				Object documentobject = xDocumentEnum.nextElement();
				XComponent document = (XComponent) UnoRuntime.queryInterface(XComponent.class, documentobject);
				// das erste gefundene Dokument zur�ckgeben
				return document;    
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error", e );
			} 
		}
		return null;
	}

	public TextContentLocation getStart()
	{
		return new OOoTextContentLocation(m_oXModel, getXTextDocument().getText().getStart());
	}

	public TextContentLocation getEnd()
	{
		return new OOoTextContentLocation(m_oXModel, getXTextDocument().getText().getEnd());
	}



	private class OpenOfficeTopWindowListener implements  XTopWindowListener
	{
		public void windowActivated(EventObject arg0)
		{
			for(int i=0; i<(m_oOfficeWindowListener.size()); i++)
			{
				OfficeWindowListener listener = (OfficeWindowListener)(m_oOfficeWindowListener.get(i));
				listener.windowActivated();
			}
		}

		public void windowClosing(EventObject arg0) 
		{
			for(int i=0; i<(m_oOfficeWindowListener.size()); i++)
			{
				OfficeWindowListener listener = (OfficeWindowListener)(m_oOfficeWindowListener.get(i));
				listener.windowClosing();
			}      
		}

		public void windowDeactivated(EventObject arg0) 
		{
			for(int i=0; i<(m_oOfficeWindowListener.size()); i++)
			{
				OfficeWindowListener listener = (OfficeWindowListener)(m_oOfficeWindowListener.get(i));
				listener.windowDeactivated();
			}      
		}

		public void windowOpened(EventObject arg0) {}
		public void windowClosed(EventObject arg0) {}
		public void windowMinimized(EventObject arg0) {}
		public void windowNormalized(EventObject arg0) {}
		public void disposing(EventObject arg0) {}
	}


	private XTopWindow getXTopWindow()
	{
		if (m_oTopWin == null)
		{
			XModel xmodel = getXModel();
			XFrame frame = xmodel.getCurrentController().getFrame();
			XWindow xWindow = frame.getContainerWindow();
			m_oTopWin = (XTopWindow) UnoRuntime.queryInterface(XTopWindow.class, xWindow);
		}
		return m_oTopWin;
	}

	private void releaseXTopWindow()
	{
		m_oTopWin = null;
	}



	public boolean addOfficeWindowListener(OfficeWindowListener listener)
	{
		m_oOfficeWindowListener.add(listener);

		if (m_oOpenOfficeTopWindowListener == null)
		{
			m_oOpenOfficeTopWindowListener = new OpenOfficeTopWindowListener(); 
			getXTopWindow().addTopWindowListener(m_oOpenOfficeTopWindowListener);
		}
		return true;
	}


	private void removeTopWindowListener()
	{
		if (m_oOpenOfficeTopWindowListener != null)
		{
			if (getXTopWindow() != null)
			{
				getXTopWindow().removeTopWindowListener(m_oOpenOfficeTopWindowListener);
				releaseXTopWindow();
				m_oOpenOfficeTopWindowListener = null;
			}
		}
	}


	public boolean removeOfficeWindowListener(OfficeWindowListener listener)
	{
		if (listener != null) m_oOfficeWindowListener.remove(listener);

		if (m_oOfficeWindowListener.size() == 0)
		{
			removeTopWindowListener();
		}
		return true;
	}


	public void removeAllOfficeWindowListener()
	{
		m_oOfficeWindowListener.clear();
		removeTopWindowListener();
	}

	public float getLeftMargin() 
	{
		return getPagePropertyValueInteger("LeftMargin", -1);
	}


	public float getRightMargin() 
	{
		return getPagePropertyValueInteger("RightMargin", -1);
	}

	public float getTopMargin() 
	{
		return getPagePropertyValueInteger("TopMargin", -1);
	}

	public float getBottomMargin() 
	{
		return getPagePropertyValueInteger("BottomMargin", -1);
	}

	public float getPageWidth() 
	{
		return getPagePropertyValueInteger("Width", -1);
	}

	public float getPageHeight() 
	{
		return getPagePropertyValueInteger("Height", -1);
	}


	private int getPagePropertyValueInteger(String key, int defaultValue)
	{
		try
		{
			Object value = getPageProperties().getPropertyValue(key);
			if (value instanceof Integer)
			{
				return ((Integer)value).intValue();
			}
			else if (value instanceof Short)
			{
				return ((Short)value).intValue();
			}
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}

		return defaultValue;
	}


	private XPropertySet m_oXPropertySet = null;
	private OOoDeferredDocumentCloser m_oOOoDeferredDocumentCloser;


	private XPropertySet getPageProperties()
	{
		if (m_oXPropertySet == null) m_oXPropertySet = getPagePropertiesInternal();
		return m_oXPropertySet;
	}


	private XPropertySet getPagePropertiesInternal()
	{
		XTextRange range = getXTextDocument().getText().getStart();
		XText xDocumentText = range.getText();              
		XTextCursor xModelCursor = xDocumentText.createTextCursorByRange(range);
		XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);
		try
		{
			String sPageStyleName = (String)(xCursorProps.getPropertyValue("PageStyleName"));
			// create a supplier to get the styles-collection
			com.sun.star.style.XStyleFamiliesSupplier xSupplier = ( com.sun.star.style.XStyleFamiliesSupplier ) UnoRuntime.queryInterface(com.sun.star.style.XStyleFamiliesSupplier.class, getXTextDocument());
			// use the name access from the collection
			com.sun.star.container.XNameAccess xNameAccess = xSupplier.getStyleFamilies();
			com.sun.star.container.XNameContainer xPageStyleCollection = (com.sun.star.container.XNameContainer) UnoRuntime.queryInterface(com.sun.star.container.XNameContainer.class, xNameAccess.getByName( "PageStyles" ));
			Object pageStyleObject = xPageStyleCollection.getByName(sPageStyleName);
			com.sun.star.style.XStyle pageStyle = (com.sun.star.style.XStyle) UnoRuntime.queryInterface(com.sun.star.style.XStyle.class, pageStyleObject);
			XPropertySet xStyleProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, pageStyle);
			return xStyleProps;
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}
		catch (NoSuchElementException e)
		{
			logger.error("Error", e );
		}
		return null;
	}


	public TextContentLocation getStartOfPage(int page)
	{
		XController xController = getXModel().getCurrentController();
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();
		if (xViewCursor != null)
		{
			XPageCursor xPageCursor = (XPageCursor)UnoRuntime.queryInterface(XPageCursor.class, xViewCursor);
			if (xPageCursor != null)
			{
				xPageCursor.jumpToPage((short)page);
				xPageCursor.jumpToStartOfPage();
				XTextCursor xTextCursor = (XTextCursor)UnoRuntime.queryInterface(XTextCursor.class, xPageCursor);
				if (xTextCursor != null)
				{
					return new OOoTextContentLocation(m_oXModel, xTextCursor);
				}
			}
		}
		return null;
	}


	public void setVisible(boolean pVisibility)
	{
		/* Does only set document visibility, not for the whole window.
		 * TODO clear API
		XWindow xComponentWindow = getXTextDocument().getCurrentController().getFrame().getComponentWindow();
		XWindow xWindow = (XWindow)UnoRuntime.queryInterface(XWindow.class, xComponentWindow);
		xWindow.setVisible(visible);
		 */

		getXTextDocument().getCurrentController().getFrame().getContainerWindow().setVisible(pVisibility);
	}


	public void toFront()
	{
		toFront(false);
	}
	

	/**
	 * @see org.evolvis.liboffice.document.TextDocument#toFront(boolean)
	 */
	public void toFront(boolean force) {
		
		getXTopWindow().toFront();
	}

	public void toBack()
	{
		getXTopWindow().toBack();
	}


	public boolean close()
	{
		return close(false);
	}


	public TextContentRange getDocumentRange()
	{
		return new OOoTextContentRange(this, m_oXModel, getXTextDocument().getText().getStart());    
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.msoffice.OOoTextDocumentComponent#getXTextContent()
	 */
	public XTextContent getXTextContent()
	{
		return (XTextContent)UnoRuntime.queryInterface(XTextContent.class, m_oXTextDocument);
	}

	public TextContentLocation getTextContentLocation()
	{
		return new OOoTextContentLocation(m_oXModel, this);
	}

	public TextContentLocation getTextContentLocationAfter()
	{
		OOoTextContentLocation loc =  new OOoTextContentLocation(m_oXModel, this);
		loc.positionAfterComponent(this, this);
		return loc;
	}

	public static String ensureFileName(String name)
	{
		if (SystemInfo.isWindowsSystem())
		{
			return "file:/" + name.replaceAll("\\\\", "/");   
		}
		else
		{
			if (! name.startsWith("file://")) return("file://"+ name); else return(name);
		}
	}

	private String getFiletypeString(OfficeFileFormat type)
	{
		// TODO new and nice and dynamic file-type handling
		if (KnownOfficeFileFormats.FILE_TYPE_HTML.equals(type)) return "swriter: HTML (StarWriter)";
		if (KnownOfficeFileFormats.FILE_TYPE_SXW.equals(type)) return "StarOffice XML (Writer)";
		if (KnownOfficeFileFormats.FILE_TYPE_PDF.equals(type)) return "writer_pdf_Export"; 
		if (KnownOfficeFileFormats.FILE_TYPE_DOC.equals(type)) return "swriter: MS Word 97";
		if (KnownOfficeFileFormats.FILE_TYPE_ODT.equals(type)) return "writer8";
		//if (KnownOfficeFileFormats.FILETYPE_ODT.equals(type)) return "OpenDocument Text-Document";
		//if (KnownOfficeFileFormats.FILETYPE_RTF.equals(type)) return "RTF (rich text format)";
		return null;
	}

	public boolean save(String infilename, OfficeFileFormat filetype, boolean overwrite) throws UnsupportedOfficeFileTypeException
	{
		try
		{
			// Objekte neu holen, damit das Speichern von sxw und doc hintereinander funktioniert
			refine();
		}
		catch (NoOfficeException e1)
		{
			logger.error("NofficeExeption " + e1);
		}

		String filename = ensureFileName(infilename);

		// Setup save properties
		PropertyValue overWrite = new PropertyValue();
		overWrite.Name = new String("Overwrite");
		overWrite.Value = new Boolean(overwrite);

		PropertyValue filterName = new PropertyValue();
		filterName.Name = ("FilterName");

		String filetypestring = getFiletypeString(filetype);
		if (filetypestring == null) throw new UnsupportedOfficeFileTypeException(filetype);

		filterName.Value = filetypestring;

		PropertyValue[] props;

		if (KnownOfficeFileFormats.FILE_TYPE_PDF.equals(filetype))
		{
			PropertyValue compressionMode = new PropertyValue();
			compressionMode.Name = "CompressionMode";
			compressionMode.Value = "1"; 

			props = new PropertyValue[3];
			props[0] = overWrite;
			props[1] = filterName;
			props[2] = compressionMode;
		}
		else
		{
			props = new PropertyValue[2];
			props[0] = overWrite;
			props[1] = filterName;
		}

		// Save copy of doc as...
		XStorable storedDoc = (XStorable) UnoRuntime.queryInterface(XStorable.class, m_oXModel);

		try
		{
			if (KnownOfficeFileFormats.FILE_TYPE_PDF.equals(filetype)) 
			{
				storedDoc.storeToURL(filename, props);
			} 
			else
			{        
				storedDoc.storeAsURL(filename, props);
			} 
		}
		catch (com.sun.star.io.IOException e)
		{
			logger.error("Error in SaveDocument", e );
			return(false);
		}
		catch(DisposedException e)
		{
			logger.error("DispposedException Error in SaveDocument ", e );
		}

		return true;
	}

	public boolean close(boolean forceCloseOnChanges)
	{
		removeAllOfficeWindowListener();    

		if(m_oXModel != null)
		{
			// It is a full featured office document.
			// Try to use close mechanism instead of a hard dispose().
			// But maybe such service is not available on this model.
			XCloseable xCloseable = (XCloseable)UnoRuntime.queryInterface(XCloseable.class, m_oXModel);

			if(xCloseable!=null)
			{
				try
				{
					// use close(boolean DeliverOwnership)
					// The boolean parameter DeliverOwnership tells objects vetoing the close process that they may
					// assume ownership if they object the closure by throwing a CloseVetoException

					// Here we give up ownership. To be on the safe side, catch possible veto exception anyway. 
					//xCloseable.close(true);

					// Ownership behalten auch wenn es schief geht...
					xCloseable.close(false);
				}
				catch(com.sun.star.util.CloseVetoException exCloseVeto)
				{
					//logger.error( exCloseVeto );
					logger.info("Dokument konnte nicht geschlossen werden da es noch verwendet wird. Merke Dokument zum zeitversetzten Schlie�en vor...");

					if (m_oOOoDeferredDocumentCloser == null)
					{
						m_oOOoDeferredDocumentCloser = new OOoDeferredDocumentCloser();
					}

					m_oOOoDeferredDocumentCloser.addDocumentToClose(xCloseable);

					return false;
				} 
			}
			// If close is not supported by this model - try to dispose it.
			// But if the model disagree with a reset request for the modify state
			// we shouldn't do so. Otherwhise some strange things can happen.
			else
			{
				logger.debug("closing is not supported by document model... trying to dispose it.");
				com.sun.star.lang.XComponent xDisposeable = (com.sun.star.lang.XComponent)UnoRuntime.queryInterface(com.sun.star.lang.XComponent.class, m_oXModel);      
				xDisposeable.dispose();
			}
		}
		return true;
	}

	public boolean print(String printer, int frompage, int topage)
	{
		// Querying for the interface XPrintable on the loaded document
		XPrintable xprintable = ( XPrintable ) UnoRuntime.queryInterface(XPrintable.class, m_oXModel);

		if (printer != null) 
		{        
			// Setting the property "Name" for the favoured printer (name of IP address)
			PropertyValue[] printerpropertyvalues = new PropertyValue[1];
			printerpropertyvalues[0] = new PropertyValue();
			printerpropertyvalues[0].Name = "Name";
			printerpropertyvalues[0].Value = printer;

			try
			{
				// Setting the name of the printer
				xprintable.setPrinter(printerpropertyvalues);
			}
			catch(com.sun.star.lang.IllegalArgumentException iae)
			{
				logger.error( iae );
				return(false);
			}
		} 

		int numprops = 0;    
		String pages = null;
		if ((frompage != 0) && (topage != 0))
		{
			if (frompage <= topage)
			{
				pages = frompage + "-" + topage;
			}
			else
			{
				pages = topage + "-" + frompage;        
			}
		}    
		if (pages != null) numprops ++;

		PropertyValue[] propertyvalue = new PropertyValue[numprops];
		int useprop = 0;

		if (pages != null)
		{
			// Setting the property "Pages" so that only the desired pages will be printed.
			propertyvalue[useprop] = new PropertyValue();
			propertyvalue[useprop].Name = "Pages";
			propertyvalue[useprop].Value = pages;
			useprop ++;
		}

		// Printing the loaded document
		try
		{
			xprintable.print(propertyvalue);
		}
		catch(com.sun.star.lang.IllegalArgumentException iae)
		{
			logger.error( iae );
			return(false);
		}
		return(true);
	}

	public int getPageNumber(TextContentLocation location)
	{
		if (location == null) { logger.warn("getPageNumber(): location is null!"); return -1; }

		XTextCursor cursor = ((OOoTextContentLocation)location).getXTextCursor();
		if (cursor != null)
		{    
			// den (sichtbaren) Cursor positionieren... 
			XController xController = m_oXModel.getCurrentController();
			if (xController != null)
			{
				XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
				if (xViewCursorSupplier != null)
				{
					XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();
					if (xViewCursor != null)
					{
						xViewCursor.gotoRange(cursor, false);
						XPageCursor xPageCursor = (XPageCursor)UnoRuntime.queryInterface(XPageCursor.class, xViewCursor);
						return xPageCursor.getPage();
					}
				}
			}
		}
		return -1;
	}

	public String[] getAvailableFontNames()
	{
		String[] names = new String[0];
		XWindow xwindow = m_oXModel.getCurrentController().getFrame().getContainerWindow();

		if (xwindow != null)
		{
			XDevice xdev = (XDevice)UnoRuntime.queryInterface(XDevice.class, xwindow);
			if (xdev != null)
			{
				FontDescriptor[] fd = xdev.getFontDescriptors();
				if (fd != null)
				{
					names = new String[fd.length];
					for(int i=0; i<(fd.length); i++)
					{
						names[i] = fd[i].Name;          
					}
				}        
			}
		}

		return names;

	}

	public String getDocumentFilename()
	{
		if(m_oXModel != null)
		{
			XStorable xStore = (XStorable)UnoRuntime.queryInterface(XStorable.class, m_oXModel);
			if (xStore != null)
			{    
				String filename = xStore.getLocation();
				if (filename != null)
				{
					String prefix = "file://";
					if (filename.startsWith(prefix))
					{
						filename = filename.substring(prefix.length());
					}        
					try {
						return URLDecoder.decode(filename, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new RuntimeException(e.getLocalizedMessage());
					}
				}
			}
		}
		return null;
	}

	public boolean setDocumentInformations(List virtualdocumentinformations)
	{
		if(m_oXModel != null)
		{
			String comment = "";
			Iterator it = virtualdocumentinformations.iterator();
			while(it.hasNext())
			{
				Object obj = it.next();
				if (obj != null)
				{
					if (obj instanceof VirtualDocumentInformation)
					{
						VirtualDocumentInformation info = (VirtualDocumentInformation)obj;
						if (VirtualDocumentInformationStorage.STORAGE_FIELD == info.getStorageLocation())
						{
							setDocumentDataFieldMasters(m_oXModel, info.getKey(), info.getValue());              
						}
						else if (VirtualDocumentInformationStorage.STORAGE_COMMENT == info.getStorageLocation())
						{
							comment += info.getKey() + "=" + info.getValue() + "|";              
						}            
					}
				}
			}

			if (comment.length() > 0)
			{
				setDocumentComment(m_oXModel, comment);
			}

			return true;
		}
		return false;
	}


	private String getDocumentComment(XModel xmodel)
	{  
		String comment = null;
		String keywords = null;
		XDocumentInfoSupplier docInfoSupplier = (XDocumentInfoSupplier)UnoRuntime.queryInterface(XDocumentInfoSupplier.class, xmodel);
		XDocumentInfo docInfo = docInfoSupplier.getDocumentInfo();
		XPropertySet docProp= (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, docInfo);

		Object commentAny;
		Object keywordsAny;
		try
		{
			keywordsAny = docProp.getPropertyValue("Keywords");
			if (keywordsAny != null)
			{
				try
				{
					keywords = AnyConverter.toString(keywordsAny);
				}
				catch (IllegalArgumentException e1) 
				{
					logger.error("Error in getDocumentComment", e1 );
				}
			}

			commentAny = docProp.getPropertyValue("Description");
			if (commentAny != null)
			{
				try
				{
					comment = AnyConverter.toString(commentAny);
				}
				catch (IllegalArgumentException e1) 
				{
					logger.error("Error in getDocumentComment", e1 );
				}
			}
		}
		catch (UnknownPropertyException e) 
		{
			logger.error("Error in getDocumentComment", e );
		}
		catch (WrappedTargetException e) 
		{
			logger.error("Error in getDocumentComment", e );
		}

		return(comment + keywords);
	}


	private boolean setDocumentComment(XModel xmodel, String comment)
	{    
		String oldComment = getDocumentComment(xmodel);
		String newComment = oldComment + comment;

		// Set doc comment 
		XDocumentInfoSupplier docInfoSupplier = (XDocumentInfoSupplier)UnoRuntime.queryInterface(XDocumentInfoSupplier.class, xmodel);
		XDocumentInfo docInfo = docInfoSupplier.getDocumentInfo();
		XPropertySet docProp= (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, docInfo);

		try
		{
			if (newComment.length() > 255)
			{
				String s1 = newComment.substring(0, 255);
				String s2 = newComment.substring(255);

				docProp.setPropertyValue("Description", s1);        
				docProp.setPropertyValue("Keywords", s2);
			}
			else
			{
				docProp.setPropertyValue("Description", newComment);        
			}

			return true;
		}
		catch (UnknownPropertyException e) 
		{
			logger.error("Error in setDocumentComment", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error in setDocumentComment", e );
		}
		catch (PropertyVetoException e)
		{
			logger.error("Error in setDocumentComment", e );
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error in setDocumentComment", e );
		}
		return false;
	}


	private boolean setDocumentDataFieldMasters(XModel xmodel, String key, String data)
	{    
		//XComponent xTemplateComponent = xmodel;

		//XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, xTemplateComponent);
		//XNameAccess xNamedFieldMasters = xTextFieldsSupplier.getTextFieldMasters();
		//XEnumerationAccess xEnumeratedFields = xTextFieldsSupplier.getTextFields();

		// access corresponding field master
		//Object fieldMaster;
		try
		{
			XMultiServiceFactory xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, xmodel);         
			XPropertySet xMasterPropSet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xMSF.createInstance("com.sun.star.text.FieldMaster.User"));
			xMasterPropSet.setPropertyValue ("Name", key);
			xMasterPropSet.setPropertyValue ("Content", data);
		}
		catch (NoSuchElementException e)
		{
			logger.error("Error in SetDocumentDataFieldsMaster", e ); 
			return false;
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error in SetDocumentDataFieldsMaster", e );
			return false;
		} 
		catch (UnknownPropertyException e)
		{
			logger.error("Error in SetDocumentDataFieldsMaster", e );
			return false;
		} 
		catch (PropertyVetoException e)
		{
			logger.error("Error in SetDocumentDataFieldsMaster", e );
			return false;
		} 
		catch (IllegalArgumentException e)
		{
			logger.error("Error in SetDocumentDataFieldsMaster", e );
			return false;
		} 
		catch (Exception e)
		{
			logger.error("Error in SetDocumentDataFieldsMaster", e );
			return false;
		}
		return true;
	}


	public List getDocumentInformations()
	{
		List list = new ArrayList();

		if(m_oXModel != null)
		{
			list.addAll(getDocumentDataFieldMasters(m_oXModel));

			// Kommentar-Feld lesen...
			String comment = getDocumentComment(m_oXModel);
			if (comment != null)
			{
				if (comment.trim().length() > 0)
				{
					String[] keyvals = comment.split("\\|");
					for(int i=0; i<(keyvals.length); i++)
					{
						String[] keyval = keyvals[i].split("=");
						if (keyval.length == 2)
						{
							String key = keyval[0];
							String val = keyval[1];
							VirtualDocumentInformation info = new VirtualDocumentInformation(key, val, VirtualDocumentInformationStorage.STORAGE_COMMENT);
							list.add(info);
						}
					}
				}
			}
		} 
		return list;
	}


	private List getDocumentDataFieldMasters(XModel xmodel)
	{
		XComponent xTemplateComponent = xmodel;
		List list = new ArrayList();

		XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, xTemplateComponent);
		XNameAccess xNamedFieldMasters = xTextFieldsSupplier.getTextFieldMasters();
		String[] names = xNamedFieldMasters.getElementNames();

		for(int i=0; i<(names.length); i++)
		{
			String name = names[i];
			try
			{
				Object fieldMaster = xNamedFieldMasters.getByName(name);
				if (fieldMaster != null)
				{
					XPropertySet xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, fieldMaster);
					String text = (String)(xPropertySet.getPropertyValue("Content"));
					if (text != null)
					{
						if (name.startsWith("com.sun.star.text.FieldMaster.User."))
						{
							name = name.substring("com.sun.star.text.FieldMaster.User.".length());
						}

						VirtualDocumentInformation info = new VirtualDocumentInformation(name, text, VirtualDocumentInformationStorage.STORAGE_FIELD);
						if (info != null) list.add(info);
					}
				}
			}
			catch (NoSuchElementException e) 
			{
				logger.error("Error in getDocumentDataFieldsMaster", e );
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error in getDocumentDataFieldsMaster", e );
			}
			catch (UnknownPropertyException e)
			{
				//logger.error("Error", e );
			}
		}
		return list;
	}

	public boolean dispatchUnoCommand(XFrame xFrame, String command, PropertyValue[] oProperties)
	{
		try
		{
			com.sun.star.util.URL[] oParseURL = new com.sun.star.util.URL[1];
			oParseURL[0]          = new com.sun.star.util.URL();
			oParseURL[0].Complete = command;
			getUrlTransformer().parseStrict (oParseURL);

			XDispatchProvider xProvider = (XDispatchProvider)UnoRuntime.queryInterface(XDispatchProvider.class, xFrame);
			XDispatch xDispatcher = xProvider.queryDispatch(oParseURL[0], "", 0);
			if(xDispatcher != null)
			{
				xDispatcher.dispatch(oParseURL[0], oProperties);
				return true;
			}
		}
		catch (Exception e)
		{
			logger.error("Error", e );
		}
		return false;
	}

	public boolean dispatchUnoCommand(XFrame xFrame, String command)
	{
		return dispatchUnoCommand(xFrame, command, new PropertyValue[0]);
	}

	public boolean dispatchUnoCommand(String command)
	{
		return dispatchUnoCommand(m_oXModel.getCurrentController().getFrame(), command);
	}

	private XURLTransformer getUrlTransformer() throws Exception
	{
		if (null == xParser)
		{
			xParser = (XURLTransformer)UnoRuntime.queryInterface(XURLTransformer.class, m_oOfficeConnection.getXMultiServiceFactory().createInstance("com.sun.star.util.URLTransformer"));
		}
		return xParser;
	}


	public boolean copy()
	{
		//      ".uno:Copy"
		XFrame xFrame = m_oXModel.getCurrentController().getFrame();
		if (xFrame != null)
		{
			return dispatchUnoCommand(xFrame, ".uno:Copy", new PropertyValue[0]);
		}
		return false;
	}

	public boolean paste()
	{
		//      ".uno:Paste"
		XFrame xFrame = m_oXModel.getCurrentController().getFrame();
		if (xFrame != null)
		{
			return dispatchUnoCommand(xFrame, ".uno:Paste", new PropertyValue[0]);    
		}
		return false;
	}

	public boolean pasteSpecial(int pPasteFormat)
	{
		XFrame xFrame = m_oXModel.getCurrentController().getFrame();
		if(xFrame != null && (pPasteFormat == OOoTextDocument.PASTE_FORMAT_WRITER || pPasteFormat == OOoTextDocument.PASTE_FORMAT_RTF || pPasteFormat == OOoTextDocument.PASTE_FORMAT_HTML || pPasteFormat == OOoTextDocument.PASTE_FORMAT_PLAIN))
		{
			PropertyValue[] pasteFormat = new PropertyValue[1];
			pasteFormat[0] = new PropertyValue();
			pasteFormat[0].Name = new String("SelectedFormat");
			pasteFormat[0].Value = new Integer(pPasteFormat);

			return this.dispatchUnoCommand(xFrame, ".uno:ClipboardFormatItems", pasteFormat);
		}

		return false;
	}



	private XTextTable getTableAtStart(XText xText)
	{
		XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
		XEnumeration xParaEnum = xParaAccess.createEnumeration();
		if (xParaEnum.hasMoreElements()) 
		{
			try
			{
				Object paraobj = xParaEnum.nextElement();
				XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);
				if (xInfo.supportsService("com.sun.star.text.TextTable"))
				{
					XTextTable table = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, paraobj);        
					return table; 
				}
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error", e );
			}
		}
		return null;
	}



	public boolean insertDocumentAtStart(String filename)
	{
		try
		{
			refine();
		}
		catch (NoOfficeException e2)
		{
			e2.printStackTrace();
		}

		//      System.out.println("insertDocumentAtStart() A");    
		XTextContent newContent = null;
		XText xText = m_oXTextDocument.getText();
		//XTextRangeCompare xTextRangeCompare = (XTextRangeCompare)(UnoRuntime.queryInterface(XTextRangeCompare.class, xText)); 

		// jetzt mu� kontrolliert werden ob es nicht noch eine Tabelle gibt
		// die VOR dieser Position steht (am Anfang des Dokuments)...
		XTextTable firstTable = getTableAtStart(xText);
		if (firstTable != null)
		{
			XRelativeTextContentInsert xRelativeTextContentInsert = (XRelativeTextContentInsert)UnoRuntime.queryInterface(XRelativeTextContentInsert.class, xText);

			try
			{       
				XMultiServiceFactory xMSF = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, m_oXModel);
				Object oInt = xMSF.createInstance("com.sun.star.text.Paragraph");
				newContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, oInt);
				xRelativeTextContentInsert.insertTextContentBefore(newContent, firstTable);
			}
			catch (IllegalArgumentException e)
			{
				logger.error("Error", e );
			}
			catch (Exception e)
			{
				logger.error("Error", e );
			}      
		}

		XTextCursor xStartModelCursor = null;
//		System.out.println("insertDocumentAtStart() newContent=" + newContent);    
		if (newContent != null)
		{
			XTextRange newContentRange = newContent.getAnchor();
			xStartModelCursor = xText.createTextCursorByRange(newContentRange.getStart());          
		}
		else
		{
			XTextCursor xModelCursor = xText.createTextCursor();
			xModelCursor.gotoStart(false);
			xModelCursor.collapseToStart();
			xStartModelCursor = xText.createTextCursorByRange(xModelCursor.getStart());    
			try
			{
//				System.out.println("insertDocumentAtStart() insertControlCharacter()...");    
				xText.insertControlCharacter(xStartModelCursor, ControlCharacter.PARAGRAPH_BREAK, false);

				XParagraphCursor xParaCursor = (XParagraphCursor)UnoRuntime.queryInterface(XParagraphCursor.class, xStartModelCursor);
				xParaCursor.gotoPreviousParagraph(false);        
				xParaCursor.collapseToStart();
				xStartModelCursor = (XTextCursor)UnoRuntime.queryInterface(XTextCursor.class, xParaCursor);
			}
			catch (IllegalArgumentException e1)
			{
				e1.printStackTrace();
			}
		}
		xStartModelCursor.collapseToStart();

		XController xController = m_oXModel.getCurrentController();
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   
		//XText xDocumentText = xViewCursor.getText();
		xViewCursor.gotoRange(xStartModelCursor.getStart(), false);

//		System.out.println("insertDocumentAtStart() inserting document...");    

		try
		{      
			XDocumentInsertable xDocumentInsertable = (XDocumentInsertable)UnoRuntime.queryInterface(XDocumentInsertable.class, xStartModelCursor);
			xDocumentInsertable.insertDocumentFromURL(ensureFileName(filename), new PropertyValue[0]);
		}
		catch (com.sun.star.io.IOException e)
		{
			logger.error("Error", e );
			return false;
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error", e );
			return false;
		}  


		XParagraphCursor xParaCursor = (XParagraphCursor)UnoRuntime.queryInterface(XParagraphCursor.class, xStartModelCursor);
		xParaCursor.gotoNextParagraph(false);        
		xParaCursor.collapseToStart();
		xStartModelCursor = (XTextCursor)UnoRuntime.queryInterface(XTextCursor.class, xParaCursor);
		xViewCursor.gotoRange(xStartModelCursor, false);

		// System.out.println("insertDocumentAtStart() inserting pagebreak...");    

		//InsertPagebreak AMT FN_INSERT_PAGEBREAK 20323
		//XFrame xFrame = m_oXModel.getCurrentController().getFrame();
		//dispatchUnoCommand(xFrame, ".uno:InsertPagebreak", new PropertyValue[0]);

		insertPageBreak();

		XText xNewDocumentText = xViewCursor.getText();
		XTextCursor xNewModelCursor = xNewDocumentText.createTextCursorByRange(xViewCursor.getStart());
		xViewCursor.gotoRange(xNewModelCursor.getStart(), false);

//		System.out.println("insertDocumentAtStart() inserting pagebreak done.");    

		// falls gerade ein absatz eingef�gt werden musste: weg damit!
//		System.out.println("insertDocumentAtStart() firstTable=" + firstTable);    
		if (firstTable != null) 
		{
			try
			{
				XRelativeTextContentRemove xRelativeTextContentRemove = (XRelativeTextContentRemove)UnoRuntime.queryInterface(XRelativeTextContentRemove.class, xText);        
				xRelativeTextContentRemove.removeTextContentBefore(firstTable);
			}
			catch (IllegalArgumentException e1)
			{
				logger.error( e1 );
				return false;        
			}     
		}

		XTextCursor xEndModelCursor = xText.createTextCursor();
		xEndModelCursor.gotoEnd(false);
		xEndModelCursor.collapseToEnd();
		xViewCursor.gotoRange(xEndModelCursor, false);

//		System.out.println("insertDocumentAtStart() EXIT");    
		return true;

	}

	public boolean insertDocumentAtEnd(String filename)
	{
		XTextContent newContent = null;

		XText xText = m_oXTextDocument.getText();
		//XTextRangeCompare xTextRangeCompare = (XTextRangeCompare)(UnoRuntime.queryInterface(XTextRangeCompare.class, xText)); 

		// jetzt mu� kontrolliert werden ob es nicht noch eine Tabelle gibt
		// die NACH dieser Position steht (am Ende des Dokuments)...
		XTextTable firstTable = getTableAtEnd(xText);

		if (firstTable != null)
		{
			XRelativeTextContentInsert xRelativeTextContentInsert = (XRelativeTextContentInsert)UnoRuntime.queryInterface(XRelativeTextContentInsert.class, xText);

			try
			{       
				XMultiServiceFactory xMSF = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, m_oXModel);
				Object oInt = xMSF.createInstance("com.sun.star.text.Paragraph");
				newContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, oInt);
				xRelativeTextContentInsert.insertTextContentAfter(newContent, firstTable);
			}
			catch (IllegalArgumentException e)
			{
				logger.error("Error", e );
			}
			catch (Exception e)
			{
				logger.error("Error", e );
			}      
		}

		XTextCursor xStartModelCursor = null;
		if (newContent != null)
		{
			XTextRange newContentRange = newContent.getAnchor();
			xStartModelCursor = xText.createTextCursorByRange(newContentRange.getEnd());          
		}
		else
		{
			XTextCursor xModelCursor = xText.createTextCursor();
			xModelCursor.gotoEnd(false);
			xModelCursor.collapseToEnd();
			xStartModelCursor = xText.createTextCursorByRange(xModelCursor.getEnd());          
		}
		xStartModelCursor.collapseToEnd();

		XController xController = m_oXModel.getCurrentController();
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   
		//XText xDocumentText = xViewCursor.getText();
		xViewCursor.gotoRange(xStartModelCursor.getEnd(), true);

		insertPageBreak();

		try
		{      
			XDocumentInsertable xDocumentInsertable = (XDocumentInsertable)UnoRuntime.queryInterface(XDocumentInsertable.class, xStartModelCursor);
			xDocumentInsertable.insertDocumentFromURL(ensureFileName(filename), new PropertyValue[0]);
		}
		catch (com.sun.star.io.IOException e)
		{
			logger.error("Error", e );
			return false;
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error", e );
			return false;
		}  


		XText xNewDocumentText = xViewCursor.getText();
		XTextCursor xNewModelCursor = xNewDocumentText.createTextCursorByRange(xViewCursor.getEnd());
		xViewCursor.gotoRange(xNewModelCursor.getEnd(), false);

		if (firstTable != null) 
		{
			try
			{
				XRelativeTextContentRemove xRelativeTextContentRemove = (XRelativeTextContentRemove)UnoRuntime.queryInterface(XRelativeTextContentRemove.class, xText);        
				xRelativeTextContentRemove.removeTextContentAfter(firstTable);
			}
			catch (IllegalArgumentException e1)
			{
				logger.error( e1 );
				return false;        
			}     
		}

		XTextCursor xEndModelCursor = xText.createTextCursor();
		xEndModelCursor.gotoStart(false);
		xEndModelCursor.collapseToStart();
		xViewCursor.gotoRange(xEndModelCursor, false);
		return true;

	}

	private XTextTable getTableAtEnd(XText xText)
	{
		XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
		XEnumeration xParaEnum = xParaAccess.createEnumeration();
		Object paraobj = null;
		try
		{
			while(xParaEnum.hasMoreElements()) 
			{
				paraobj = xParaEnum.nextElement();
			}
		}
		catch (NoSuchElementException e) 
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e) 
		{
			logger.error("Error", e );
		}

		if (paraobj != null) 
		{
			XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);
			if (xInfo.supportsService("com.sun.star.text.TextTable"))
			{
				XTextTable table = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, paraobj);        
				return table; 
			}
		}
		return null;
	}

	public boolean insertPageBreak()
	{  
		return dispatchUnoCommand(".uno:InsertPagebreak");

		/* Does not work for me
		 try
		 {
		 XTextCursor xModelCursor = getXTextCursorOfViewCursor();
		 xModelCursor.goRight((short)1, true);
		 XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);
		 try
		 {
		 xCursorProps.setPropertyValue("PageDescName", "Standard");
		 return true;
		 }
		 catch (UnknownPropertyException e)
		 {
		 logger.error("Error in insertPageBreak", e );
		 }
		 catch (PropertyVetoException e)
		 {
		 logger.error("Error in insertPageBreak", e );
		 }
		 catch (WrappedTargetException e)
		 {
		 logger.error("Error in insertPageBreak", e );
		 }
		 }
		 catch (IllegalArgumentException e1)
		 {
		 logger.error("Error in insertPageBreak", e1 );
		 }

		 return false;
		 */
	}

	public void insertParaBreak()
	{
		//return dispatchUnoCommand(".uno:InsertPara");
		dispatchUnoCommand(".uno:InsertColumnBreak");
	}

	public boolean insertLineBreak()
	{
		return dispatchUnoCommand(".uno:InsertLinebreak");
	}
	
	public boolean insertDocumentFromFile(String filename)
	{
		XController xController = m_oXModel.getCurrentController();
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   

		XText xDocumentText = xViewCursor.getText();
		XTextCursor xModelCursor = xDocumentText.createTextCursorByRange(xViewCursor.getStart());

		try
		{      
			XDocumentInsertable xDocumentInsertable = (XDocumentInsertable)UnoRuntime.queryInterface(XDocumentInsertable.class, xModelCursor);
			xDocumentInsertable.insertDocumentFromURL(ensureFileName(filename), new PropertyValue[0]);
			return true;
		}
		catch (com.sun.star.io.IOException e)
		{
			logger.error("Error in InsertDocumentFromFile", e );
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error in InsertDocumentFromFile", e );
		}  
		return false;
	}

	public boolean jumpToLocation(TextContentLocation location)
	{
		XTextCursor cursor = ((OOoTextContentLocation)location).getXTextCursor();
		if (cursor != null)
		{    
			// den (sichtbaren) Cursor positionieren... 
			XController xController = m_oXModel.getCurrentController();
			if (xController != null)
			{
				XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
				if (xViewCursorSupplier != null)
				{
					XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();
					if (xViewCursor != null)
					{
						xViewCursor.gotoRange(cursor, false);
						return true;
					}
				}
			}
		}    
		return false;
	}

	public boolean remove()
	{
		throw new UnsupportedOperationException("Method is not implemented");
	}

	public String getActivePrinterName()
	{
		// Querying for the interface XPrintable on the loaded document
		XPrintable xprintable = ( XPrintable ) UnoRuntime.queryInterface(XPrintable.class, m_oXModel);

		// getting the name of the printer
		PropertyValue[] printerpropertyvalues = xprintable.getPrinter();
		for(int i=0; i<(printerpropertyvalues.length); i++)
		{
			if ("Name".equals(printerpropertyvalues[i].Name))
			{
				return (String)(printerpropertyvalues[i].Value);
			}
		}
		return null;
	}

	public TextContentLocation getCurrentCursorLocation()
	{
		XController xController = m_oXModel.getCurrentController();
		if (xController != null)
		{
			XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
			if (xViewCursorSupplier != null)
			{
				XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();
				if (xViewCursor != null)
				{
					return new OOoTextContentLocation(m_oXModel, xViewCursor.getStart());          
				}
			}      
		}
		return null;
	}

	public boolean existsStyle(VirtualTextStyle style)
	{
		if (getParaStyleByName(m_oXTextDocument, style.getStyleName()) != null)
		{
			return true;
		}    
		else return (getCharStyleByName(m_oXTextDocument, style.getStyleName()) != null);

	}

	public TextStyle getTextStyle(String stylename)
	{
		XStyle style = getParaStyleByName(m_oXTextDocument, stylename);
		if (style != null)
		{
			return new OOoTextStyle(style, TextStyle.STYLE_PARAGRAPH);      
		}

		style = getCharStyleByName(m_oXTextDocument, stylename);
		if (style != null)
		{
			return new OOoTextStyle(style, TextStyle.STYLE_CHARACTER);      
		}

		return null;

	}

	public TextStyle copyTextStyle(VirtualTextStyle oldStyle, String newStyleName)
	{
		com.sun.star.style.XStyle newStyle = null;
		if (oldStyle.getStyleType() == VirtualTextStyleType.STYLE_PARAGRAPH)
		{
			newStyle = copyParagraphStyle(m_oXTextDocument, null != oldStyle ? oldStyle.getStyleName() : null, newStyleName);
			if (newStyle != null)
			{
				return new OOoTextStyle(newStyle, TextStyle.STYLE_PARAGRAPH);
			}
		}
		else
		{
			newStyle = copyCharacterStyle(m_oXModel, null != oldStyle ?  oldStyle.getStyleName() : null, newStyleName);
			if (newStyle != null)
			{
				return new OOoTextStyle(newStyle, TextStyle.STYLE_CHARACTER);
			}
		}
		return null;

	}

	public void removeTextStyle(VirtualTextStyle style)
	{
		if (null == style.getStyleName()) return;

		XStyleFamiliesSupplier xSupplier = (XStyleFamiliesSupplier)UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, m_oXTextDocument);
		XNameAccess xNameAccess          = xSupplier.getStyleFamilies();
		String name = style.getStyleType() == VirtualTextStyleType.STYLE_PARAGRAPH ? "ParagraphStyles" : "CharacterStyles";
		XNameContainer xParaStyleCollection;
		try
		{
			xParaStyleCollection = (XNameContainer)UnoRuntime.queryInterface(XNameContainer.class, xNameAccess.getByName( name ));
			xParaStyleCollection.removeByName( style.getStyleName() );
		}
		catch (NoSuchElementException e)
		{
			logger.warn(e);
		}
		catch (WrappedTargetException e)
		{
			logger.warn(e);
		}
	}

	private XStyle copyParagraphStyle(XModel xmodel, String sourcestylename, String destinationstylename)
	{
		XTextDocument xTextDocument = getTextDocumentFromComponent(xmodel);
		XStyle xStyle = getParaStyleByName(xTextDocument, sourcestylename);
		XPropertySet xSourcePropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xStyle );

		XMultiServiceFactory xDocMSF = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, xTextDocument);
		if (xDocMSF == null) return null;    

		// use the service 'com.sun.star.style.ParagraphStyle'
		XInterface xInterface;
		try
		{
			xInterface = (XInterface)xDocMSF.createInstance("com.sun.star.style.ParagraphStyle");
		}
		catch (Exception e)
		{
			logger.error("Error", e );
			return null;
		}
		if (xInterface == null) return null;    

		// create a supplier to get the Style family collection
		XStyleFamiliesSupplier xSupplier = (XStyleFamiliesSupplier)UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xTextDocument);
		if (xSupplier == null) return null;    

		// get the NameAccess interface from the Style family collection
		XNameAccess xNameAccess = xSupplier.getStyleFamilies();
		if (xNameAccess == null) return null;    

		// select the Paragraph styles, you get the Paragraph style collection
		XNameContainer xParaStyleCollection = null;
		try
		{
			xParaStyleCollection = (XNameContainer)UnoRuntime.queryInterface(XNameContainer.class, xNameAccess.getByName("ParagraphStyles"));
		}
		catch (NoSuchElementException e1)
		{
			logger.error( e1 );
			return null;
		}
		catch (WrappedTargetException e1)
		{
			logger.error( e1 );
			return null;
		}
		if (xParaStyleCollection == null) return null;    

		// create a PropertySet to set the properties for the new Paragraphstyle
		XPropertySet xDestinationPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInterface );  

		// copy property values...

		if (null != xSourcePropertySet)
		{
			try
			{
				xDestinationPropertySet.setPropertyValue("CharFontName", xSourcePropertySet.getPropertyValue("CharFontName"));
				xDestinationPropertySet.setPropertyValue("CharHeight", xSourcePropertySet.getPropertyValue("CharHeight"));
				xDestinationPropertySet.setPropertyValue("CharWeight", xSourcePropertySet.getPropertyValue("CharWeight"));
				xDestinationPropertySet.setPropertyValue("CharAutoKerning", xSourcePropertySet.getPropertyValue("CharAutoKerning"));
				xDestinationPropertySet.setPropertyValue("ParaAdjust", xSourcePropertySet.getPropertyValue("ParaAdjust"));
				xDestinationPropertySet.setPropertyValue("ParaFirstLineIndent", xSourcePropertySet.getPropertyValue("ParaFirstLineIndent"));
				xDestinationPropertySet.setPropertyValue("BreakType", xSourcePropertySet.getPropertyValue("BreakType"));
			}
			catch (UnknownPropertyException e2) 
			{
				logger.error( e2 );
			}
			catch (PropertyVetoException e2)
			{
				logger.error( e2 );
			}
			catch (IllegalArgumentException e2)
			{
				logger.error( e2 );
			}
			catch (WrappedTargetException e2)
			{
				logger.error( e2 );
			}
		}


		// insert the new Paragraph style in the Paragraph style collection
		try
		{
			removeTextStyle(new VirtualTextStyle(destinationstylename, VirtualTextStyleType.STYLE_CHARACTER));
			xParaStyleCollection.insertByName(destinationstylename, xDestinationPropertySet );
		}
		catch (IllegalArgumentException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (ElementExistException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (WrappedTargetException e3)
		{
			logger.error( e3 );
			return null;
		}

		XStyle xNewStyle = (XStyle)UnoRuntime.queryInterface(XStyle.class, xInterface);      
		return xNewStyle;
	}


	private XStyle copyCharacterStyle(XModel xmodel, String sourcestylename, String destinationstylename)
	{
		XTextDocument xTextDocument      = getTextDocumentFromComponent(xmodel);
		com.sun.star.style.XStyle xStyle = getCharStyleByName(xTextDocument, sourcestylename);
		XPropertySet xSourcePropertySet  = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xStyle );

		XMultiServiceFactory xDocMSF     = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, xTextDocument);
		if (xDocMSF == null) return null;    

		XInterface xInterface;
		try
		{
			xInterface = (XInterface)xDocMSF.createInstance("com.sun.star.style.CharacterStyle");
		}
		catch (Exception e)
		{
			logger.error("Error", e );
			return null;
		}
		if (xInterface == null) return null;    

		// create a supplier to get the Style family collection
		XStyleFamiliesSupplier xSupplier = (XStyleFamiliesSupplier)UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xTextDocument);
		if (xSupplier == null) return null;    

		// get the NameAccess interface from the Style family collection
		XNameAccess xNameAccess = xSupplier.getStyleFamilies();
		if (xNameAccess == null) return null;    

		// select the Paragraph styles, you get the Paragraph style collection
		XNameContainer xCharStyleCollection = null;
		try
		{
			xCharStyleCollection = (XNameContainer)UnoRuntime.queryInterface(XNameContainer.class, xNameAccess.getByName("CharacterStyles"));
		}
		catch (NoSuchElementException e1)
		{
			logger.error( e1 );
			return null;
		}
		catch (WrappedTargetException e1)
		{
			logger.error( e1 );
			return null;
		}
		if (xCharStyleCollection == null) return null;    

		// create a PropertySet to set the properties for the new Paragraphstyle
		XPropertySet xDestinationPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInterface );  

		// copy property values...

		if (null != xSourcePropertySet)
		{
			try
			{
				xDestinationPropertySet.setPropertyValue("CharFontName", xSourcePropertySet.getPropertyValue("CharFontName"));
				xDestinationPropertySet.setPropertyValue("CharHeight", xSourcePropertySet.getPropertyValue("CharHeight"));
				xDestinationPropertySet.setPropertyValue("CharWeight", xSourcePropertySet.getPropertyValue("CharWeight"));
				xDestinationPropertySet.setPropertyValue("CharAutoKerning", xSourcePropertySet.getPropertyValue("CharAutoKerning"));
				//xDestinationPropertySet.setPropertyValue("ParaAdjust", xSourcePropertySet.getPropertyValue("ParaAdjust"));
				//xDestinationPropertySet.setPropertyValue("ParaFirstLineIndent", xSourcePropertySet.getPropertyValue("ParaFirstLineIndent"));
				//xDestinationPropertySet.setPropertyValue("BreakType", xSourcePropertySet.getPropertyValue("BreakType"));
			}
			catch (UnknownPropertyException e2) 
			{
				logger.error( e2 );
			}
			catch (PropertyVetoException e2) 
			{
				logger.error( e2 );
			}
			catch (IllegalArgumentException e2) 
			{
				logger.error( e2 );
			}
			catch (WrappedTargetException e2) 
			{
				logger.error( e2 );
			}
		}

		// insert the new character style in the Paragraph style collection
		try
		{
			removeTextStyle(new VirtualTextStyle(destinationstylename, VirtualTextStyleType.STYLE_PARAGRAPH));
			xCharStyleCollection.insertByName(destinationstylename, xDestinationPropertySet );
		}
		catch (IllegalArgumentException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (ElementExistException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (WrappedTargetException e3)
		{
			logger.error( e3 );
			return null;
		}

		XStyle xNewStyle = (XStyle)UnoRuntime.queryInterface(XStyle.class, xInterface);      
		return xNewStyle;
	}


	private XStyle getParaStyleByName(XTextDocument xTextDocument, String stylename)
	{

		// craete a supplier to get the styles-collection
		XStyleFamiliesSupplier xSupplier = null;
		xSupplier = ( com.sun.star.style.XStyleFamiliesSupplier ) UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xTextDocument);

		// use the name access from the collection
		XNameAccess xNameAccess = null;
		logger.debug("xTextDoc: " + xTextDocument + " style: "+stylename + " xSupplier: "+xSupplier);
		xNameAccess = xSupplier.getStyleFamilies();
		try
		{
			XNameContainer xParaStyleCollection = (XNameContainer) UnoRuntime.queryInterface(XNameContainer.class, xNameAccess.getByName( "ParagraphStyles" ));
			String[] sParaElementNames = xParaStyleCollection.getElementNames();
			for( int iCounter = 0;  iCounter < sParaElementNames.length; iCounter++ ) 
			{
				if (sParaElementNames[iCounter].equals(stylename)) return (XStyle) UnoRuntime.queryInterface(XStyle.class, xParaStyleCollection.getByName(sParaElementNames[iCounter]));
			}
		}
		catch (NoSuchElementException e1) 
		{
			logger.error( e1 );
		}
		catch (WrappedTargetException e1) 
		{
			logger.error( e1 );
		}
		return null;
	}

	private com.sun.star.style.XStyle getCharStyleByName(XTextDocument xTextDocument, String stylename)
	{
		// craete a supplier to get the styles-collection
		XStyleFamiliesSupplier xSupplier = null;
		xSupplier = (XStyleFamiliesSupplier ) UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xTextDocument);

		// use the name access from the collection
		XNameAccess xNameAccess = null;
		xNameAccess = xSupplier.getStyleFamilies();
		try
		{
			XNameContainer xCharStyleCollection = (XNameContainer) UnoRuntime.queryInterface(XNameContainer.class, xNameAccess.getByName( "CharacterStyles" ));
			String[] sCharElementNames = xCharStyleCollection.getElementNames();
			for( int iCounter = 0;  iCounter < sCharElementNames.length; iCounter++ ) 
			{
				if (sCharElementNames[iCounter].equals(stylename)) return (XStyle) UnoRuntime.queryInterface(XStyle.class, xCharStyleCollection.getByName(sCharElementNames[iCounter]));
			}
		}
		catch (NoSuchElementException e1) 
		{
			logger.error( e1 );
		}
		catch (WrappedTargetException e1) 
		{
			logger.error( e1 );
		}

		return null;
	}


	private XStyle addParaStyle(XTextDocument xTextDocument, VirtualTextStyle style)
	{
		XMultiServiceFactory xDocMSF = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, xTextDocument);
		if (xDocMSF == null) return null;    

		// use the service 'com.sun.star.style.ParagraphStyle'
		XInterface xInterface;
		try
		{
			xInterface = (XInterface)xDocMSF.createInstance("com.sun.star.style.ParagraphStyle");
		}
		catch (Exception e)
		{
			logger.error("Error", e );
			return null;
		}
		if (xInterface == null) return null;    

		// create a supplier to get the Style family collection
		XStyleFamiliesSupplier xSupplier = (XStyleFamiliesSupplier)UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xTextDocument);
		if (xSupplier == null) return null;    

		// get the NameAccess interface from the Style family collection
		XNameAccess xNameAccess = xSupplier.getStyleFamilies();
		if (xNameAccess == null) return null;    

		// select the Paragraph styles, you get the Paragraph style collection
		XNameContainer xParaStyleCollection = null;
		try
		{
			xParaStyleCollection = (XNameContainer)UnoRuntime.queryInterface(XNameContainer.class, xNameAccess.getByName("ParagraphStyles"));
		}
		catch (NoSuchElementException e1)
		{
			logger.error( e1 );
			return null;
		}
		catch (WrappedTargetException e1)
		{
			logger.error( e1 );
			return null;
		}
		if (xParaStyleCollection == null) return null;    

		// create a PropertySet to set the properties for the new Paragraphstyle
		XPropertySet xDestinationPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInterface );  

		// copy property values...
		try
		{
			VirtualTextFont font = style.getFont();
			if (font != null)
			{      
				xDestinationPropertySet.setPropertyValue("CharFontName", font.getFontName());
				xDestinationPropertySet.setPropertyValue("CharHeight", new Float(font.getFontHeight()));
				xDestinationPropertySet.setPropertyValue("CharWeight", new Float(font.isBold() ? com.sun.star.awt.FontWeight.BOLD : com.sun.star.awt.FontWeight.NORMAL));
				xDestinationPropertySet.setPropertyValue("CharPosture", (font.isItalic() ? FontSlant.fromInt(com.sun.star.awt.FontSlant.ITALIC_value) : FontSlant.fromInt(com.sun.star.awt.FontSlant.NONE_value)));
				xDestinationPropertySet.setPropertyValue("CharUnderline", new Short(font.isUnderline() ? com.sun.star.awt.FontUnderline.SINGLE : com.sun.star.awt.FontUnderline.NONE));
			}
		}
		catch (UnknownPropertyException e2) 
		{
			logger.error( e2 );
		}
		catch (PropertyVetoException e2) 
		{
			logger.error( e2 );
		}
		catch (IllegalArgumentException e2) 
		{
			logger.error( e2 );
		}
		catch (WrappedTargetException e2) 
		{
			logger.error( e2 );
		}

		// insert the new Paragraph style in the Paragraph style collection
		try
		{
			xParaStyleCollection.insertByName(style.getStyleName(), xDestinationPropertySet );
		}
		catch (IllegalArgumentException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (ElementExistException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (WrappedTargetException e3)
		{
			logger.error( e3 );
			return null;
		}

		XStyle xNewStyle = (XStyle)UnoRuntime.queryInterface(XStyle.class, xInterface);      
		return xNewStyle;
	}


	private XStyle addCharacterStyle(XTextDocument xTextDocument, VirtualTextStyle style)
	{
		XMultiServiceFactory xDocMSF = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, xTextDocument);
		if (xDocMSF == null) return null;    

		// use the service 'com.sun.star.style.ParagraphStyle'
		XInterface xInterface;
		try
		{
			xInterface = (XInterface)xDocMSF.createInstance("com.sun.star.style.CharacterStyle");
		}
		catch (Exception e)
		{
			logger.error("Error", e );
			return null;
		}
		if (xInterface == null) return null;    

		// create a supplier to get the Style family collection
		XStyleFamiliesSupplier xSupplier = (XStyleFamiliesSupplier)UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xTextDocument);
		if (xSupplier == null) return null;    

		// get the NameAccess interface from the Style family collection
		XNameAccess xNameAccess = xSupplier.getStyleFamilies();
		if (xNameAccess == null) return null;    

		// select the Paragraph styles, you get the Character style collection
		XNameContainer xCharacterStyleCollection = null;
		try
		{
			xCharacterStyleCollection = (XNameContainer)UnoRuntime.queryInterface(XNameContainer.class, xNameAccess.getByName("CharacterStyles"));
		}
		catch (NoSuchElementException e1)
		{
			logger.error( e1 );
			return null;
		}
		catch (WrappedTargetException e1)
		{
			logger.error( e1 );
			return null;
		}
		if (xCharacterStyleCollection == null) return null;    

		// create a PropertySet to set the properties for the new Characterstyle
		XPropertySet xDestinationPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInterface );  

		// copy property values...
		try
		{
			VirtualTextFont font = style.getFont();

			xDestinationPropertySet.setPropertyValue("CharFontName", font.getFontName());
			xDestinationPropertySet.setPropertyValue("CharHeight", new Float(font.getFontHeight()));
			xDestinationPropertySet.setPropertyValue("CharWeight", new Float(font.isBold() ? com.sun.star.awt.FontWeight.BOLD : com.sun.star.awt.FontWeight.NORMAL));
			xDestinationPropertySet.setPropertyValue("CharPosture", (font.isItalic() ? FontSlant.fromInt(com.sun.star.awt.FontSlant.ITALIC_value) : FontSlant.fromInt(com.sun.star.awt.FontSlant.NONE_value)));
			xDestinationPropertySet.setPropertyValue("CharUnderline", new Short(font.isUnderline() ? com.sun.star.awt.FontUnderline.SINGLE : com.sun.star.awt.FontUnderline.NONE));
		}
		catch (UnknownPropertyException e2) 
		{
			logger.error( e2 );
		}
		catch (PropertyVetoException e2) 
		{
			logger.error( e2 );
		}
		catch (IllegalArgumentException e2) 
		{
			logger.error( e2 );
		}
		catch (WrappedTargetException e2) 
		{
			logger.error( e2 );
		}

		// insert the new Paragraph style in the Paragraph style collection
		try
		{
			xCharacterStyleCollection.insertByName(style.getStyleName(), xDestinationPropertySet );
		}
		catch (IllegalArgumentException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (ElementExistException e3)
		{
			logger.error( e3 );
			return null;
		}
		catch (WrappedTargetException e3)
		{
			logger.error( e3 );
			return null;
		}

		XStyle xNewStyle = (XStyle)UnoRuntime.queryInterface(XStyle.class, xInterface);      
		return xNewStyle;
	}

	public boolean insertText(VirtualStyledText text)
	{
		XController xController                     = m_oXModel.getCurrentController();
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor                 = xViewCursorSupplier.getViewCursor();
		XText xDocumentText                         = xViewCursor.getText();
		XTextCursor xModelCursor                    = xDocumentText.createTextCursorByRange(xViewCursor.getStart());

		return (insertText(xDocumentText, xModelCursor, text));
	}


	public boolean insertText( XText xDocumentText, XTextCursor xModelCursor, VirtualStyledText text)
	{
		logger.debug("insertText " + text);
		boolean result = true;

		if(text == null) return false;

		for(int i = 0; i<(text.getNumberOfParagraphs()); i++)
		{
			VirtualParagraph para     = text.getParagraph(i);
			XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);
			try
			{ 
				setStyle(para.getStyle(), xCursorProps);
			}
			catch (UnknownPropertyException e) 
			{ 
				logger.error("Error in insertText", e );
				result = false; 
			}
			catch (PropertyVetoException e) 
			{ 
				logger.error("Error in insertText", e );
				result = false; 
			}
			catch (IllegalArgumentException e) 
			{ 
				logger.error("Error in insertText para:"+para, e );
				result = false; 
			}
			catch (WrappedTargetException e) 
			{ 
				logger.error("Error in insertText", e );
				result = false; 
			}

			for(int n=0; n<(para.getNumberOfTextPortions()); n++)
			{
				VirtualTextPortion tvtp = para.getTextPortion(n);
				if (tvtp instanceof VirtualTextPortionText)
				{

					VirtualTextPortionText vtp = (VirtualTextPortionText)tvtp;

					try
					{
						if (vtp.getFont() != null)
						{
							xCursorProps.setPropertyValue("CharFontName", vtp.getFont().getFontName());
							xCursorProps.setPropertyValue("CharHeight", new Float(vtp.getFont().getFontHeight()));
						}

						if (vtp.useBold())
						{
							if (vtp.isBold()) xCursorProps.setPropertyValue("CharWeight", new Float(com.sun.star.awt.FontWeight.BOLD));
							else              xCursorProps.setPropertyValue("CharWeight", new Float(com.sun.star.awt.FontWeight.NORMAL));
						}

						if (vtp.useUnderline())
						{
							if (vtp.isUnderline()) xCursorProps.setPropertyValue("CharUnderline", new Short(com.sun.star.awt.FontUnderline.SINGLE));
							else                   xCursorProps.setPropertyValue("CharUnderline", new Short(com.sun.star.awt.FontUnderline.NONE));
						}

						if (vtp.useItalic())
						{
							if (vtp.isItalic()) xCursorProps.setPropertyValue("CharPosture", new Integer(com.sun.star.awt.FontSlant.ITALIC_value));
							else                xCursorProps.setPropertyValue("CharPosture", new Integer(com.sun.star.awt.FontSlant.NONE_value));
						}
						setStyle(vtp.getStyle(), xCursorProps);
					}
					catch (UnknownPropertyException e) 
					{ 
						logger.error("Error in insertText vtp", e );
						result = false; 
					}
					catch (PropertyVetoException e)
					{ 
						logger.error("Error in insertText vtp", e );
						result = false; 
					}
					catch (IllegalArgumentException e)// wird geworfen, wenn der CharacterStyle im Dokument nicht vorhanden ist
					{ 
						logger.error("Error in insertText vtp: " +vtp, e );
						result = false; 
					}
					catch (WrappedTargetException e)
					{ 
						logger.error("Error in insertText vtp", e );
						result = false; 
					}

					String rawtext = vtp.getText();

					logger.debug("rawText >"+rawtext+"<");

					xDocumentText.insertString(xModelCursor, rawtext, false);
				}
				else if (tvtp instanceof VirtualTextPortionMarker)
				{
					VirtualTextPortionMarker vtp = (VirtualTextPortionMarker)tvtp;
					try
					{
						xDocumentText.insertTextContent(xModelCursor, new OOoTextMarker(this, vtp.getName(), vtp.getHint()).getXTextContent(), false);
					}
					catch (IllegalArgumentException e)
					{
						logger.error("Error in insertText", e );
					}
				}        
				else if (tvtp instanceof VirtualParagraphBreak)
				{
					//VirtualParagraphBreak vtp = (VirtualParagraphBreak)tvtp;
					try
					{
						xDocumentText.insertControlCharacter(xModelCursor, ControlCharacter.PARAGRAPH_BREAK, false);
					}
					catch (IllegalArgumentException e)
					{
						logger.error("Error in insertText", e );
					}
				}        
				else if (tvtp instanceof VirtualPageBreak)
				{
					//VirtualPageBreak vtp = (VirtualPageBreak)tvtp;
				}
			}

			XController xController                     = m_oXModel.getCurrentController();
			XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
			XTextViewCursor xViewCursor                 = xViewCursorSupplier.getViewCursor();
			xViewCursor.gotoRange(xModelCursor.getEnd(), false);
		}

		return(result);
	}


	public TextFrame insertTextFrame(VirtualTextFrame frame)
	{
		try
		{
			XTextDocument doc               = getXTextDocument();
			XMultiServiceFactory xDocMSF    = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, doc);
			XInterface xTextFrameInterface  = (XInterface) xDocMSF.createInstance("com.sun.star.text.TextFrame");
			XTextFrame xTextFrame           = (XTextFrame) UnoRuntime.queryInterface(XTextFrame.class, xTextFrameInterface);
			XPropertySet xFramePropSet      = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xTextFrameInterface);      
			XController xController         = m_oXModel.getCurrentController();    
			XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
			XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   

			OOoTextFrame oTextFrame = new OOoTextFrame(this, xTextFrame);
			oTextFrame.setWidth(frame.getWidth());
			oTextFrame.setHeight(frame.getHeight());
			oTextFrame.setPositionLeft(frame.getPositionLeft());
			oTextFrame.setPositionTop(frame.getPositionTop());

			doc.getText().insertTextContent(xViewCursor, xTextFrame, false);

			xFramePropSet.setPropertyValue("FrameIsAutomaticHeight", new Boolean(false));

			if (frame.getText() != null)
			{
				jumpToLocation(oTextFrame.getFrameContentLocation());
				insertText(frame.getText());
			}

			return oTextFrame;
		}
		catch (java.lang.Exception e)
		{
			logger.error("Error", e );
		} 
		return null;
	}

	public TextTable insertTable(VirtualTextTable table)
	{
		XController xController                     = m_oXModel.getCurrentController();
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor                 = xViewCursorSupplier.getViewCursor();
		XText xDocumentText                         = xViewCursor.getText();
		XTextCursor xModelCursor                    = xDocumentText.createTextCursorByRange(xViewCursor.getStart());

		logger.debug("insertTable table:"+table);

		try
		{
			OOoTextTable tt = new OOoTextTable(this, table.getNumberOfRows(), table.getNumberOfColumns());
			xDocumentText.insertTextContent(xModelCursor, tt.getXTextTable(), false);
			tt.setVirtualTextTable(this, table);
			if (!(table.getShowLines())) setTableBorderInvisible(tt.getXTextTable());


			//XCellRange xCellRange = (XCellRange) UnoRuntime.queryInterface(XCellRange.class, tt.getXTextTable());



			for(int row=0; row<(table.getNumberOfRows()); row++)
			{
				for(int col=0; col<(table.getNumberOfColumns()); col++)
				{
					VirtualTextTableCell cell = table.getTextTableCell(row, col);

					if(null != cell)
					{
						logger.debug("OOoOfficeController insertCell "+cell);
						if (cell.getBorderTop()    != null) tt.setCellBorderLineVisible(row, col, TextTableCell.BORDER_TOP,    cell.getBorderTop().isVisible());
						if (cell.getBorderLeft()   != null) tt.setCellBorderLineVisible( row, col, TextTableCell.BORDER_LEFT,   cell.getBorderLeft().isVisible());
						if (cell.getBorderRight()  != null) tt.setCellBorderLineVisible( row, col, TextTableCell.BORDER_RIGHT,  cell.getBorderRight().isVisible());
						if (cell.getBorderBottom() != null) tt.setCellBorderLineVisible( row, col, TextTableCell.BORDER_BOTTOM, cell.getBorderBottom().isVisible());
						if (cell.getMarginTop()    != null) setCellMargin(tt, row, col, TextTableCell.BORDER_TOP,    cell.getMarginTop().getMargin());
						if (cell.getMarginLeft()   != null) setCellMargin(tt, row, col, TextTableCell.BORDER_LEFT,   cell.getMarginLeft().getMargin());
						if (cell.getMarginRight()  != null) setCellMargin(tt, row, col, TextTableCell.BORDER_RIGHT,  cell.getMarginRight().getMargin());
						if (cell.getMarginBottom() != null) setCellMargin(tt, row, col, TextTableCell.BORDER_BOTTOM, cell.getMarginBottom().getMargin());


						XParagraphCursor xParaCursor = null;
						XText xCellText              = OOoTextTableCell.getCellTextHandle(tt.getXTextTable(), col, row);
						XTextCursor textCursor       = xCellText.createTextCursor();
						xParaCursor                  = (XParagraphCursor) UnoRuntime.queryInterface(XParagraphCursor.class, textCursor );


						Object horiz = cell.getHorizontalJustification();

						Object horizjust = null;
						if      (VirtualTextTableCell.HORIZONTAL_JUSTIFY_LEFT.equals(horiz)) horizjust = new Integer(ParagraphAdjust.LEFT_value);
						else if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_RIGHT.equals(horiz)) horizjust = new Integer(ParagraphAdjust.RIGHT_value);
						else if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_CENTER.equals(horiz)) horizjust = new Integer(ParagraphAdjust.CENTER_value);
						else horizjust = new Integer(ParagraphAdjust.LEFT_value);
						XPropertySet paraProps = (XPropertySet)  UnoRuntime.queryInterface(XPropertySet.class, xParaCursor);

						paraProps.setPropertyValue ( "ParaAdjust", horizjust ); 

						//TODO: cell-justification fixen...                               

					}
				}        
			}


			// Spaltenbreiten setzen
			XPropertySet xTableProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, tt.getXTextTable());
			Object separatorsObject  = xTableProps.getPropertyValue("TableColumnSeparators");

			short colRelativeSum     = ((Short)(xTableProps.getPropertyValue("TableColumnRelativeSum"))).shortValue();      
			//int leftMargin           = ((Integer)xTableProps.getPropertyValue("LeftMargin")).intValue();
			//int rightMargin          = ((Integer)xTableProps.getPropertyValue("RightMargin")).intValue();




			if(separatorsObject instanceof TableColumnSeparator[])
			{
				TableColumnSeparator[] sep =  ((TableColumnSeparator[])separatorsObject);


				double widthSum = 0;
				for (int i=0; i< sep.length; i++)
				{
					Object o = table.getColumnWidth( i );
					if(o instanceof Integer)
					{
						int pos = ((Integer)o).intValue();
						widthSum += pos;  
						sep[i].Position = (short)(colRelativeSum * widthSum / table.getWidth()); 
						logger.debug("setColumnWidth col:"+i + " mmWidth "+ pos);
					}
				}

				//xTableProps.setPropertyValue("HoriOrient", new Short(HoriOrientation.LEFT_AND_WIDTH));


				if(table.getWidth() > -1)
				{
					xTableProps.setPropertyValue("Width", new Integer( (int)table.getWidth() ));
				}
				else
				{
					VirtualMargin marg = table.getMarginLeft();
					float width        = getPageWidth();
					width             -= getLeftMargin();
					width             -= getRightMargin();
					if (null != marg)
					{
						width -= marg.getMargin();
					}
					xTableProps.setPropertyValue("Width", new Integer(  (int)width ));
				}
				xTableProps.setPropertyValue("TableColumnSeparators", sep);
			}


			xViewCursor.gotoRange(xModelCursor.getEnd(), false);

			//XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);      
			//xCursorProps.setPropertyValue("ParaStyleName", STYLE_STANDARD);

			if(table.getMarginLeft() != null)
			{
				tt.setTableLeftMargin(table.getMarginLeft().getMargin());
			}
			return(tt);
		}
		catch (Exception e) 
		{
			logger.error("Error in insertTable", e );
		}

		return(null);

	}

	public TextGraphic insertGraphic(VirtualGraphic graphic)
	{
		try
		{
			XMultiServiceFactory xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, m_oXModel);    
			Object textGfx = xMSF.createInstance("com.sun.star.text.TextGraphicObject");

			try
			{
				XPropertySet xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, textGfx);
				xPropertySet.setPropertyValue("Print", new Boolean(graphic.isPrintable()));      
				xPropertySet.setPropertyValue("GraphicURL", graphic.getFilename());      
				xPropertySet.setPropertyValue("Size", new com.sun.star.awt.Size((int)graphic.getWidth(), (int)graphic.getHeight()));
				xPropertySet.setPropertyValue("Width", new Integer((int)graphic.getWidth()));
				xPropertySet.setPropertyValue("Height", new Integer((int)graphic.getHeight()));
			}
			catch(UnknownPropertyException upe)
			{
				logger.error("Error in insertGraphic", upe );
			}
			catch(PropertyVetoException pve) 
			{
				logger.error("Error in insertGraphic", pve );
			}
			catch(IllegalArgumentException iae) 
			{
				logger.error("Error in insertGraphic", iae );
			} 
			catch(WrappedTargetException wte) 
			{
				logger.error("Error in insertGraphic", wte );
			}

			XController xController = m_oXModel.getCurrentController();    
			XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
			XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   
			XText xText = xViewCursor.getText();

			try
			{
				XTextContent xGfxTextContent = (XTextContent)UnoRuntime.queryInterface(XTextContent.class, textGfx);
				xText.insertTextContent(xViewCursor.getStart(), xGfxTextContent, false);
				return new OOoTextGraphic(m_oXModel, xGfxTextContent);
			}
			catch (IllegalArgumentException e1) 
			{
				logger.error("Error in insertGraphic", e1 );
			}
		}
		catch (Exception e) 
		{
			logger.error("Error in insertGraphic", e );
		}
		return null;
	}

	public TextMarker insertMarker(VirtualTextPortionMarker marker)
	{
		XMultiServiceFactory xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, m_oXModel);

		XPropertySet xPropertySet = null;
		try
		{
			xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xMSF.createInstance("com.sun.star.text.TextField.JumpEdit"));
			if (marker.getName() != null) xPropertySet.setPropertyValue ("PlaceHolder", marker.getName());
			if (marker.getHint() != null) xPropertySet.setPropertyValue ("Hint", marker.getHint());
			xPropertySet.setPropertyValue ("PlaceHolderType", new Integer(0));
		}
		catch (Exception e)
		{
			logger.error("Error in insertMarker", e );
			return null;
		}

		XController xController = m_oXModel.getCurrentController();    
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   
		XText xText = xViewCursor.getText();

		if (xPropertySet != null)
		{
			XTextContent xHiddenText = (XTextContent)UnoRuntime.queryInterface(XTextContent.class, xPropertySet);
			try
			{
				xText.insertTextContent(xViewCursor.getStart(), xHiddenText, false);
				return new OOoTextMarker(m_oXModel, xHiddenText);
			}
			catch (IllegalArgumentException e1) 
			{
				logger.error( e1 );
			}
		}
		return null;
	}

	public boolean removeMarker(VirtualTextPortionMarker marker)
	{

		//XMultiServiceFactory xMSF               = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, m_oXModel);
		XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, m_oXModel);
		XEnumerationAccess xEnumerationAccess   = xTextFieldsSupplier.getTextFields();
		XEnumeration xTextFieldEnum             = xEnumerationAccess.createEnumeration();

		while (xTextFieldEnum.hasMoreElements()) 
		{
			try
			{
				Object textfieldobject = xTextFieldEnum.nextElement();
				XTextContent textfield = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldobject);

				XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textfield);
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);

					String hint = (String)(xPropertySet.getPropertyValue("Hint"));
					String name = (String)(xPropertySet.getPropertyValue("PlaceHolder"));
					//int type = ((Short)(xPropertySet.getPropertyValue("PlaceHolderType"))).shortValue();

					boolean hintequal = false;
					if (marker.getHint() == null) 
					{
						hintequal = true;
					}
					else
					{
						hintequal = marker.getHint().equals(hint);
					}

					if ((name.equals(marker.getName())) && (hintequal))
					{                           
						XTextRange xLineTextRange = textfield.getAnchor();
						XText xDocumentText = xLineTextRange.getText();              
						XTextCursor xModelCursor = xDocumentText.createTextCursorByRange(xLineTextRange.getStart());

						XParagraphCursor xParaCursor = (XParagraphCursor)UnoRuntime.queryInterface(XParagraphCursor.class, xModelCursor);

						xParaCursor.gotoStartOfParagraph(false);
						xParaCursor.gotoEndOfParagraph(true);

						XTextRange xTextRange = textfield.getAnchor();
						xTextRange.setString("");
					}
				}
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error in removeMarker", e );
				return false;
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error in removeMarker", e );
				return false;
			} 
			catch (UnknownPropertyException e)
			{
				logger.error("Error in removeMarker", e );
				return false;
			} 
		}
		return true;
	}


	private XTextDocument getTextDocumentFromComponent(XComponent comp)
	{
		return(XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, comp);    
	}

	public boolean insertStyle(VirtualTextStyle style)
	{
		if (VirtualTextStyleType.STYLE_PARAGRAPH == style.getStyleType())
		{
			return addParaStyle(m_oXTextDocument, style) != null;
		}
		else if (VirtualTextStyleType.STYLE_CHARACTER == style.getStyleType())
		{
			return addCharacterStyle(m_oXTextDocument, style) != null;
		}
		return false;
	}


	private boolean setTableBorderInvisible(XTextContent xTextContent)
	{    
		XServiceInfo xServiceInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, xTextContent);
		if (xServiceInfo.supportsService("com.sun.star.text.TextTable")) 
		{
			XCellRange xCellRange = (XCellRange)UnoRuntime.queryInterface(XCellRange.class, xTextContent);    
			XPropertySet xTableProps = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xCellRange);

			BorderLine theLine = new BorderLine();
			theLine.Color = 0x000099;
			theLine.OuterLineWidth = 0;

			TableBorder bord = new TableBorder();
			bord.VerticalLine = bord.HorizontalLine = bord.LeftLine = bord.RightLine = bord.TopLine = bord.BottomLine = theLine;
			bord.IsVerticalLineValid = bord.IsHorizontalLineValid = bord.IsLeftLineValid = bord.IsRightLineValid = bord.IsTopLineValid = bord.IsBottomLineValid = true;

			try
			{
				xTableProps.setPropertyValue("TableBorder", bord);
			}
			catch (UnknownPropertyException e)
			{
				logger.error("Error in setTableBorders", e );
				return false;
			}
			catch (PropertyVetoException e)
			{
				logger.error("Error in setTableBorders", e );
				return false;
			}
			catch (IllegalArgumentException e)
			{
				logger.error("Error in setTableBorders", e );
				return false;
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error in setTableBorders", e );
				return false;
			}
			return true;
		}
		return false;
	}

	public boolean setCellMargin(TextTable oTextTable, int row, int col, Object line, float margin)
	{
		OOoTextTable table = (OOoTextTable)oTextTable;
		XTextTable xtexttable = table.getXTextTable();

		String linename = null;
		if (TextTableCell.BORDER_LEFT.equals(line))
		{
			linename = "LeftBorderDistance";
		}    
		else if (TextTableCell.BORDER_RIGHT.equals(line))
		{
			linename = "RightBorderDistance";
		}
		else if (TextTableCell.BORDER_TOP.equals(line))
		{
			linename = "TopBorderDistance";
		}
		else if (TextTableCell.BORDER_BOTTOM.equals(line))
		{
			linename = "BottomBorderDistance";
		}
		else return false;

		return setCellMargin(xtexttable, row, col, linename, margin);      
	}




	private boolean setCellMargin(XTextContent xTextContent, int row, int col, String linename, float margin)
	{
		try
		{
			XCellRange xCellRange = (XCellRange)UnoRuntime.queryInterface(XCellRange.class, xTextContent);
			XCellRange xCellRangeSingle = xCellRange.getCellRangeByPosition(col, row, col, row);      
			XPropertySet xCellProps = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xCellRangeSingle);
			xCellProps.setPropertyValue(linename, new Integer((int)margin));
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error in setCellMargin", e );
			return false;
		}
		catch (PropertyVetoException e)
		{
			logger.error("Error in setCellMargin", e );
			return false;
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error in setCellMargin", e );
			return false;
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error in setCellMargin", e );
			return false;
		} 
		catch (IndexOutOfBoundsException e)
		{
			logger.error("Error in setCellMargin", e );
			return false;
		}
		return true;
	}

	public boolean removeMarker(TextMarker marker)
	{
		XTextContent textfield              = ((OOoTextMarker)marker).getXTextContent();
		if (textfield != null)
		{
			XServiceInfo xInfo                = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textfield);
			if (xInfo != null)
			{
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					XTextRange xLineTextRange     = textfield.getAnchor();
					XText xDocumentText           = xLineTextRange.getText();              
					XTextCursor xModelCursor      = xDocumentText.createTextCursorByRange(xLineTextRange.getStart());
					XParagraphCursor xParaCursor  = (XParagraphCursor)UnoRuntime.queryInterface(XParagraphCursor.class, xModelCursor);
					xParaCursor.gotoStartOfParagraph(false);
					xParaCursor.gotoEndOfParagraph(true);
					XTextRange xTextRange         = textfield.getAnchor();
					xTextRange.setString("");
					return true;
				}
			}
		}
		return false;
	}

	private void setStyle(VirtualTextStyle style, XPropertySet xCursorProps) throws UnknownPropertyException, PropertyVetoException, IllegalArgumentException, WrappedTargetException
	{
		if (style != null && style.getStyleName() != null && style.getStyleName().length() > 0)
		{
			if (existsStyle(style))
			{
				if (VirtualTextStyleType.STYLE_PARAGRAPH == style.getStyleType())
				{
					xCursorProps.setPropertyValue("ParaStyleName", style.getStyleName());            
				}
				else if (VirtualTextStyleType.STYLE_CHARACTER == style.getStyleType())
				{
					xCursorProps.setPropertyValue("CharStyleName", style.getStyleName());            
				}
			}
			else
			{
				logger.warn("unable to set style \"" + style.getStyleName() + "\" because style does not exist.");
			}
		}
		else
			//warn would produce to much output in unit-tests
			logger.debug("unable to set style because its invalid");
	}

	public TextDocument cloneDocument(OfficeController controller)
	{
		File file;
		try
		{
			file = File.createTempFile("cloneDocument", "sxw");
			String tmpFilename = file.getAbsolutePath();
			try
			{
				save( tmpFilename, KnownOfficeFileFormats.FILE_TYPE_SXW, true);
				TextDocument clonedDocument = controller.loadDocumentAsCopy(tmpFilename);
				file.delete();
				return clonedDocument;        
			}
			catch (UnsupportedOfficeFileTypeException e)
			{
				e.printStackTrace();
			}
		}
		catch (java.io.IOException e1)
		{
			e1.printStackTrace();
		}
		return null;
	}

	protected TextContentLocator getTextContentLocator()
	{
		if (null == locator)
		{
			locator = new OOoTextContentLocator( m_oOfficeConnection);
		}
		return locator;
	}

	private OOoTextDocumentParser getParser()
	{
		if (null == parser)
		{
			parser = new OOoTextDocumentParser();
		}
		return parser;
	}

	public VirtualTextComponent parse()
	{
		return getParser().parseTextDocument(this);
	}


	public void select()
	{
		// TODO WARNING: does not select the entire document, if it contains a table

		/*XController xController                     = m_oXModel.getCurrentController();    
		 XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		 XTextViewCursor xViewCursor                 = xViewCursorSupplier.getViewCursor();
		 XTextRange  rangeStart                      = (XTextRange)UnoRuntime.queryInterface(XTextRange.class, m_oXTextDocument.getText());
		 xViewCursor.gotoRange(rangeStart, false);
		 xViewCursor.gotoEnd(true);*/

		/* TODO Why two times?? */
		dispatchUnoCommand(".uno:SelectAll");
		dispatchUnoCommand(".uno:SelectAll");
	}

	public void collapseSelection(boolean collapseToEnd)
	{
		XController xController                                   = m_oXModel.getCurrentController();    
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor                        = xViewCursorSupplier.getViewCursor();
		if (collapseToEnd)
		{
			xViewCursor.collapseToEnd();
		}
		else xViewCursor.collapseToStart();
	}

	public void activate() {
		// TODO Auto-generated method stub
		
	}

	public void copyContent(TextBookmark startBookmark, TextBookmark endBookmark) {
		// TODO Auto-generated method stub
		
	}

	public void copyContentAfterBookmark(String bookmarkName) {
		// TODO Auto-generated method stub
		
	}

	public void delete() {
		// TODO Auto-generated method stub
		
	}

	public boolean deleteParagraph(String bookmarkName) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean existsBookmark(String bookmarkName) {
		// TODO Auto-generated method stub
		return false;
	}

	public TextBookmark getBookmark(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<String> getBookmarkNames(boolean sortByName) {
		// TODO Auto-generated method stub
		return null;
	}

	public List getBookmarks(boolean sortByName) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDocumentText() {
		// TODO Auto-generated method stub
		return null;
	}

	public TextVariable getTextVariable(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TextVariable> getTextVariables() {
		// TODO Auto-generated method stub
		return null;
	}

	public TextBookmark insertTextBookmark(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public void insertTextBookmark(String name, Span span) {
		// TODO Auto-generated method stub
		
	}

	public void insertVariable(String name, String value) {
		// TODO Auto-generated method stub
	}

	public void insertSectionBreakNextPage() {
		// TODO Auto-generated method stub
		
	}


}
