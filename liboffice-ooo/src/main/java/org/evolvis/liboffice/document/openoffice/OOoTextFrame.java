/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextFrame;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocation;

import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.frame.XModel;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.RelOrientation;
import com.sun.star.text.TextContentAnchorType;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextFrame;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextFrame implements TextFrame, OOoTextDocumentComponent
{
  private XTextContent m_oXTextContent       = null;
  private XTextFrame m_oXTextFrame           = null;
  private OOoTextDocument m_oOOoTextDocument = null;
  private Logger logger                      = Logger.getLogger(getClass());
  
  public OOoTextFrame(OOoTextDocument document, XTextFrame frame)
  {
    m_oOOoTextDocument = document;
    m_oXTextFrame = frame;
    m_oXTextContent = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, m_oXTextFrame);
  }
  
  public XTextContent getXTextContent()
  {
    return m_oXTextContent;
  }

  public XTextFrame getXTextFrame()
  {
    return m_oXTextFrame;
  }

  public float getWidth()
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        //Integer frameWidth = (Integer)(xPropertySet.getPropertyValue("FrameWidthAbsolute"));
        Integer frameWidth = (Integer)(xPropertySet.getPropertyValue("Width"));
        if (frameWidth != null) return frameWidth.intValue();
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("getWidth()", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("getWidth()", e );
      }            
    }    
    return -1;
  }

  public float getHeight()
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        Integer frameWidth = (Integer)(xPropertySet.getPropertyValue("Height"));
        if (frameWidth != null) return frameWidth.intValue();
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("getHeight()", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("getHeight()", e );
      }            
    }    
    return -1;
  }

  public float getPositionTop()
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        Short frameOrient = (Short)(xPropertySet.getPropertyValue("VertOrient"));
        if (frameOrient != null)
        {          
          if (frameOrient.shortValue() == com.sun.star.text.VertOrientation.NONE)
          {
            Integer frameTop = (Integer)(xPropertySet.getPropertyValue("VertOrientPosition"));
            if (frameTop != null) return frameTop.intValue();            
          }
        }
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("getPositionTop()", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("getPositionTop()", e );
      }            
    }    
    return -1;
  }

  public float getPositionLeft()
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        Short frameOrient = (Short)(xPropertySet.getPropertyValue("HoriOrient"));
        if (frameOrient != null)
        {          
          if (frameOrient.shortValue() == com.sun.star.text.HoriOrientation.NONE)
          {
            Integer frameLeft = (Integer)(xPropertySet.getPropertyValue("HoriOrientPosition"));
            if (frameLeft != null) return frameLeft.intValue();            
          }
        }            
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("getPositionLeft()", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("getPositionLeft()", e );
      }            
    }    
    return -1;
  }

  public boolean setWidth(float width)
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        xPropertySet.setPropertyValue("Width", new Integer((int)width));
        return true;
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("setWidth()", e );
      }
      catch (WrappedTargetException e) 
      {
           logger.error("setWidth()", e );
      }
      catch (PropertyVetoException e) 
      {
          logger.error("setWidth()", e );
      }
      catch (IllegalArgumentException e) 
      {
          logger.error("setWidth()", e );
      }            
    }    
    return false;
  }

  public boolean setHeight(float height)
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        xPropertySet.setPropertyValue("Height", new Integer((int)height));
        return true;
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("setHeight()", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("setHeight()", e );
      }
      catch (PropertyVetoException e) 
      {
          logger.error("setHeight()", e );
      }
      catch (IllegalArgumentException e) 
      {
          logger.error("setHeight()", e );
      }            
    }    
    return false;
  }

  public boolean setPositionTop(float top)
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        xPropertySet.setPropertyValue("AnchorType", new Integer(TextContentAnchorType.AT_PAGE_value));
        xPropertySet.setPropertyValue("VertOrientRelation", new Short(RelOrientation.PAGE_FRAME));
        xPropertySet.setPropertyValue("VertOrient", new Short(com.sun.star.text.VertOrientation.NONE));
        xPropertySet.setPropertyValue("VertOrientPosition", new Integer(Math.round(top)));
        return true;
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("setPositionTop()", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("setPositionTop()", e );
      }
      catch (PropertyVetoException e) 
      {
          logger.error("setPositionTop()", e );
      }
      catch (IllegalArgumentException e) 
      {
          logger.error("setPositionTop()", e );
      }            
    }    
    return false;
  }

  public boolean setPositionLeft(float left)
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        xPropertySet.setPropertyValue("AnchorType", new Integer(TextContentAnchorType.AT_PAGE_value));
        xPropertySet.setPropertyValue("HoriOrientRelation", new Short(RelOrientation.PAGE_FRAME));
        xPropertySet.setPropertyValue("HoriOrient", new Short(com.sun.star.text.HoriOrientation.NONE));
        xPropertySet.setPropertyValue("HoriOrientPosition", new Integer(Math.round(left)));
        return true;
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("setPositionLeft()", e );
      }
      catch (WrappedTargetException e) 
      {
          logger.error("setPositionLeft()", e );
      }
      catch (PropertyVetoException e) 
      {
          logger.error("setPositionLeft()", e );
      }
      catch (IllegalArgumentException e) 
      {
          logger.error("setPositionLeft()", e );
      }            
    }    
    return false;
  }


  public TextContentLocation getFrameContentLocation()
  {
    return new OOoTextContentLocation(getXModel(), getXTextFrame().getText().createTextCursor());
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.document.msoffice.OOoTextDocumentComponent#getXModel()
   */
  public XModel getXModel()
  {
    return m_oOOoTextDocument.getXModel();
  }
  
  
  public boolean setPrintable(boolean printable)
  {
    XPropertySet xFrameProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
    try
    {
      xFrameProps.setPropertyValue("Print", new Boolean(printable));
      return true;
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("setPrintable()", e1 );
    }
    catch (WrappedTargetException e)
    {
        logger.error("setPrintable()", e );
    }
    catch (PropertyVetoException e)
    {
        logger.error("setPrintable()", e );
    }
    catch (IllegalArgumentException e)
    {
        logger.error("setPrintable()", e );
    }
    
    return false;
  }


  public boolean setSize(float width, float height)
  {
    XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, m_oXTextFrame);
    if (xInfo.supportsService("com.sun.star.text.TextFrame")) 
    {
      XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
      try
      {
        xPropertySet.setPropertyValue("Width", new Integer((int)width));
        xPropertySet.setPropertyValue("Height", new Integer((int)height));        
        return true;
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("setSize()", e );
      }
      catch (WrappedTargetException e) 
      {
           logger.error("setSize()", e );
      }
      catch (PropertyVetoException e) 
      {
          logger.error("setSize()", e );
      }
      catch (IllegalArgumentException e) 
      {
          logger.error("setSize()", e );
      }            
    }    
    return false;
  }


  public boolean setPosition(float top, float left)
  {
	  boolean success = setPositionTop(top);
	  if(!setPositionLeft(left)) success = false;
	  return success;
  }

  public TextContentLocation getTextContentLocation()
  {
   return new OOoTextContentLocation(getXModel(), this);
  }

  public TextContentLocation getTextContentLocationAfter()
  {
      OOoTextContentLocation loc =  new OOoTextContentLocation(getXModel(), this);
      loc.positionAfterComponent(this,m_oOOoTextDocument);
      return loc;
  }

    public boolean remove()
    {
        XText xText = m_oXTextFrame.getText();
        try
        {
          xText.removeTextContent(m_oXTextContent);
          return true;
        }
        catch(NoSuchElementException ne) 
        {
            logger.error( ne );
        }
        return false;
    }
    
}
