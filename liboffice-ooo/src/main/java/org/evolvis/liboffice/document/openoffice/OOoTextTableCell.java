/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextTableCell;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocation;

import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextTable;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH
 * @author Christoph Jerolimov, tarent GmbH
 */
public class OOoTextTableCell implements TextTableCell {
	private OOoTextTable m_oOOoTextTable;
	private int m_iRow;
	private int m_iColumn;
	private OOoTextDocument m_doc;
	private static Logger logger = Logger.getLogger(OOoTextTableCell.class);

	public OOoTextTableCell(OOoTextDocument doc, OOoTextTable table, int row, int column) {
		m_oOOoTextTable = table;
		m_iRow = row;
		m_iColumn = column;
		m_doc = doc;
	}

	public OOoTextTable getOOoTextTable() {
		return m_oOOoTextTable;
	}

	public int getRow() {
		return m_iRow;
	}

	public int getColumn() {
		return m_iColumn;
	}

	/*
	 * @see de.tarent.documents.document.TextTableCell#getPlainText()
	 */
	public String getPlainText() {
		return getTextOfCell(getOOoTextTable().getXTextTable(), m_iRow, m_iColumn);
	}

	/*
	 * @see de.tarent.documents.document.TextTableCell#containsStyle(java.lang.String)
	 */
	public boolean containsStyle(String styleName) {
		return testCellForStyle(getOOoTextTable().getXTextTable(), m_iRow, m_iColumn, styleName);
	}

	public XPropertySet getTextCursorProperties(XTextTable texttable, int row, int column) {
		if (texttable != null) {
			int numrows = texttable.getRows().getCount();
			int numcols = texttable.getColumns().getCount();
			if ((numcols > column) && (numrows > row)) {
				XText xtext = getCellTextHandle(texttable, column, row);
				if (xtext != null) {
					XTextCursor cursor = xtext.createTextCursor();
					if (cursor != null) {
						XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, cursor);
						return xCursorProps;
					}
				}
			}
		}
		return null;
	}

	private boolean testCellForStyle(XTextTable texttable, int row, int column, String stylename) {
		if (texttable != null) {
			int numrows = texttable.getRows().getCount();
			int numcols = texttable.getColumns().getCount();
			if ((numcols > column) && (numrows > row)) {
				XText xtext = getCellTextHandle(texttable, column, row);
				if (xtext != null) {
					XTextCursor cursor = xtext.createTextCursor();
					if (cursor != null) {
						XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, cursor);
						String style = null;
						try {
							style = (String) (xCursorProps.getPropertyValue("ParaStyleName"));
							if (style != null) {
								if (style.equals(stylename))
									return (true);
							}
							
							style = (String) (xCursorProps.getPropertyValue("CharStyleName"));
							if (style != null) {
								if (style.equals(stylename))
									return (true);
							}
						} catch (UnknownPropertyException e) {
							logger.error("Error", e);
						} catch (WrappedTargetException e) {
							logger.error("Error", e);
						}
					}
				}
			}
		}
		return false;
	}

	public List getParagraphs(boolean returnFirst) {
		List paras = new LinkedList();
		XText xText = getCellTextHandle(m_oOOoTextTable.getXTextTable(), m_iColumn, m_iRow);
		XEnumerationAccess xParaAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, xText);
		XEnumeration xParaEnum = xParaAccess.createEnumeration();
		
		while (xParaEnum.hasMoreElements()) {
			Object paraobj = null;
			try {
				paraobj = xParaEnum.nextElement();
			} catch (NoSuchElementException e) {
				logger.error("Error", e);
			} catch (WrappedTargetException e) {
				logger.error("Error", e);
			}
			XTextContent content = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, paraobj);
			XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, paraobj);
			
			if (xInfo.supportsService("com.sun.star.text.Paragraph")) {
				logger.debug("ist PARAGRAPH");
				// XTextContent content = (XTextContent)
				// UnoRuntime.queryInterface(XTextContent.class, paraobj);
				
				paras.add(new OOoTextParagraph(m_doc, m_oOOoTextTable.getXModel(), content));
				if (returnFirst) {
					return paras;
				}
			}
		}
		return paras;
	}

	private String getTextOfCell(XTextTable texttable, int row, int column) {
		if (texttable != null) {
			int numrows = texttable.getRows().getCount();
			int numcols = texttable.getColumns().getCount();
			if ((numcols > column) && (numrows > row)) {
				XText xtext = getCellTextHandle(texttable, column, row);
				if (xtext != null) {
					return xtext.getString();
				}
			}
		}
		return null;
	}

	public static XText getCellTextHandle(XTextTable TT1, int xp, int yp) {
		String CellName = getCellName(xp, yp);
		if (CellName != null) {
			XText oTableText = (XText) UnoRuntime.queryInterface(XText.class, TT1.getCellByName(CellName));
			return oTableText;
		}
		return (null);
	}

	public static String getCellName(int xp, int yp) {
		if (xp < 0 || yp < 0)
			throw new IllegalArgumentException();
		if (xp < 26)
			return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".substring(xp, xp + 1) + (yp + 1);
		
		int pos1 = xp / 26 - 1;
		int pos2 = xp % 26;
		return
				"ABCDEFGHIJKLMNOPQRSTUVWXYZ".substring(pos1, pos1 + 1) +
				"ABCDEFGHIJKLMNOPQRSTUVWXYZ".substring(pos2, pos2 + 1) +
				(yp + 1);
	}

	public TextContentLocation getTextContentLocation() {
		return new OOoTextContentLocation(m_oOOoTextTable.getXModel(), this);
	}

	public TextContentLocation getTextContentLocationAfter() {
		OOoTextContentLocation loc = new OOoTextContentLocation(m_oOOoTextTable.getXModel(), this);
		loc.positionAfterComponent(this, m_doc);
		return loc;
	}

	public boolean remove() {
		throw new UnsupportedOperationException("Method is not implemented");
	}
}
