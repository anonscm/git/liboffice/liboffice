/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 03.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextParagraph;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocation;

import com.sun.star.awt.FontSlant;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.frame.XController;
import com.sun.star.frame.XModel;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn, Steffen Kriese, tarent GmbH Bonn
 *
 */
public class OOoTextParagraph implements TextParagraph, OOoTextDocumentComponent
{
  private XModel m_oXMmodel;
  private XTextContent m_oXTextContent;
  private XPropertySet xCursorProps;
  private List textPortions;
  private final Logger logger = Logger.getLogger(getClass());
  private OOoTextDocument doc;
  private XText cellXtext;
  
  public OOoTextParagraph(OOoTextDocument doc, XModel xmodel, XTextContent oXTextContent)
  {
    this.doc = doc;
    m_oXMmodel = xmodel;
    m_oXTextContent = oXTextContent;
    textPortions = new LinkedList();
  }
  
  public OOoTextParagraph(OOoTextDocument doc, XModel xmodel, XTextContent oXTextContent, XText xtext)
  {
    this(doc, xmodel, oXTextContent);
    this.cellXtext = xtext;  
  }
  
  public XTextContent getXTextContent()
  {
    return m_oXTextContent;
  }    
  
  
  public String getText()
  {
    String text = "";
    // create another enumeration to get all text portions of the paragraph
    XEnumerationAccess xParaEnumerationAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, m_oXTextContent);
    XEnumeration xTextPortionEnum = xParaEnumerationAccess.createEnumeration();
    
    while(xTextPortionEnum.hasMoreElements()) 
    {
      try
      {        
        Object portionobj = xTextPortionEnum.nextElement();
        XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, portionobj);
        
        if (xInfo.supportsService("com.sun.star.text.TextPortion")) 
        {
          XPropertySet portionPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);
          String portiontype = (String)(portionPropertySet.getPropertyValue("TextPortionType"));

          if ("Text".equals(portiontype))
          {
            XTextRange xTextPortion = (XTextRange) UnoRuntime.queryInterface(XTextRange.class, portionobj);                                
            String portiontext = xTextPortion.getString();
            text += portiontext;
          }
        }        
      }
      catch(WrappedTargetException e) 
      {
          logger.error("Error", e );
      }
      catch (NoSuchElementException e) 
      {
          logger.error("Error", e );
      }
      catch (UnknownPropertyException e) 
      {
          logger.error("Error", e );
      }
    }
    return text;
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.document.msoffice.OOoTextDocumentComponent#getXModel()
   */
  public XModel getXModel()
  {
    return m_oXMmodel;
  }
  
  
  
  
  public TextContentLocation getTextContentLocation()
  {
   return new OOoTextContentLocation(getXModel(), this);
  }

  public TextContentLocation getTextContentLocationAfter()
  {
      OOoTextContentLocation loc =  new OOoTextContentLocation(getXModel(), this);
      boolean ret = loc.positionAfterComponent(this, doc);
      System.out.println("setLocationAfter " + ret);
      return loc;
  }

  public boolean remove()
  {
    m_oXTextContent.dispose();
    return true;
  }
  
  
  private XPropertySet getCursorProperties()
  {
      if (null == xCursorProps)
      {
          XTextRange xTextRange      = m_oXTextContent.getAnchor();
          XText xDocumentText         = xTextRange.getText();
          XTextCursor xModelCursor  = xDocumentText.createTextCursorByRange(xTextRange.getStart());
          xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xModelCursor);
      }
      return xCursorProps; 
  }
  
  private class OOoTextPortion
  {
      private XTextRange range;
      private String text;
      private String characterStyleName;
      private String paragraphStyleName;
      private boolean isBold;
      private boolean isItalic;
      private boolean isUnderline;
      public OOoTextPortion(Object portionobj)
      {
          XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, portionobj);
          if (xInfo.supportsService("com.sun.star.text.TextPortion")) 
          {
                XPropertySet portionPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);
                String portiontype;
                try
                {
                    portiontype = (String)(portionPropertySet.getPropertyValue("TextPortionType"));
                    if ("Text".equals(portiontype))
                    {
                        range                              = (XTextRange) UnoRuntime.queryInterface(XTextRange.class, portionobj);                                
                        text                                = range.getString();
                        characterStyleName          = (String)(portionPropertySet.getPropertyValue("CharStyleName"));
                        paragraphStyleName         = (String)(portionPropertySet.getPropertyValue("ParaStyleName"));
                        Float bold                        = (Float)(portionPropertySet.getPropertyValue("CharWeight"));
                        if (bold != null)
                        {
                            isBold = ((bold.floatValue() == com.sun.star.awt.FontWeight.BOLD));
                        }
                        
                        FontSlant fontslant          = (FontSlant)(portionPropertySet.getPropertyValue("CharPosture"));
                        if (fontslant != null)
                        {
                          isItalic = (fontslant.getValue() == com.sun.star.awt.FontSlant.ITALIC_value);
                        }
                        
                        Short underline                = (Short)(portionPropertySet.getPropertyValue("CharUnderline"));
                        if (underline != null)
                        {
                          isUnderline = (underline.shortValue() == com.sun.star.awt.FontUnderline.SINGLE);
                        }
                    }
                }
                catch (UnknownPropertyException e)
                {
                    logger.error("Error", e );
                }
                catch (WrappedTargetException e)
                {
                    logger.error("Error", e );
                }
          }
     }
      
    public String getCharacterStyleName()
    {
        return this.characterStyleName;
    }

    public String getParagraphStyleName()
    {
        return this.paragraphStyleName;
    }
    
    public XTextRange getRange()
    {
        return this.range;
    }
    
    public String getText()
    {
        return this.text;
    }

    public boolean isBold()
    {
        return this.isBold;
    }

    public boolean isItalic()
    {
        return this.isItalic;
    }

    public boolean isUnderline()
    {
        return this.isUnderline;
    }
  }
  
  public boolean containsStyle(String sStyle, String sText)
  {
      String stylename = null;
      try
      {
        stylename = (String)(getCursorProperties().getPropertyValue("ParaStyleName"));      
        if (stylename != null)
        {
          if (stylename.equals(sStyle)) return true;
        }
      }
      catch (UnknownPropertyException e)
      {
          logger.error("Error", e );
      }
      catch (WrappedTargetException e)
      {
          logger.error("Error", e );
      }

      String text = "";
      
       Iterator it = getTextPortions().iterator();
       while (it.hasNext())
       {
           OOoTextPortion por = (OOoTextPortion) it.next();
           text+=por.getText();
           if (null != sText &&  sText.toUpperCase().indexOf(por.getText().toUpperCase()) != -1)
           {
                if(por.getParagraphStyleName().equals(sStyle) || por.getCharacterStyleName().equals(sStyle)) return true;
           }
           else if(sStyle != null &&  ( sStyle.equals( por.getParagraphStyleName()) || sStyle.equals(por.getCharacterStyleName()))) return true;
       }
       if (text.equals(sText)) return true;
       return false;
  }
      
  public boolean containsFormattedText(String sText, boolean bold, boolean italic, boolean underline)
  {
      Iterator it = getTextPortions().iterator();
      while (it.hasNext())
      {
          OOoTextPortion por = (OOoTextPortion) it.next();
          if (null != sText &&  (sText.toUpperCase().indexOf(por.getText().toUpperCase()) != -1) && 
              por.isBold == bold && por.isItalic == italic && por.isUnderline == underline)
          {
               return true;
                   
          }
      }
      return false;
  }
  
  
  private List getTextPortions()
  {
      if (textPortions.isEmpty())
      {
          XEnumerationAccess xParaEnumerationAccess = (XEnumerationAccess) UnoRuntime.queryInterface(XEnumerationAccess.class, m_oXTextContent);
          XEnumeration xTextPortionEnum                    = xParaEnumerationAccess.createEnumeration();
          
          while(xTextPortionEnum.hasMoreElements()) 
          {
            try
            {
                textPortions.add(new OOoTextPortion( xTextPortionEnum.nextElement()));
            }
            catch (NoSuchElementException e)
            {
                logger.error("Error", e );
            }
            catch (WrappedTargetException e)
            {
                logger.error("Error", e );
            }
          }
      }
      return textPortions;
  }
  
  public void select(boolean append)
  {
          XController xController                                   = m_oXMmodel.getCurrentController();    
          XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
          XTextViewCursor xViewCursor                        = xViewCursorSupplier.getViewCursor();
          
          if (null != cellXtext) // workaround um Absaetze in Tabellen sicher zu markieren
          {
              XTextRange rangeStart                              = (XTextRange)UnoRuntime.queryInterface(XTextRange.class, cellXtext );
              xViewCursor.gotoRange(rangeStart.getStart(), append);
              xViewCursor.gotoEnd(true);
          }
          else // normaler Absatz im Text
          {
              xViewCursor.gotoRange(m_oXTextContent.getAnchor().getStart(), append);
              xViewCursor.gotoRange(m_oXTextContent.getAnchor().getEnd(), true);
          }
  }

    public void copy()
    {
        doc.copy();
    }
  
    
    public void select()
    {
        select( false );
    }
}
