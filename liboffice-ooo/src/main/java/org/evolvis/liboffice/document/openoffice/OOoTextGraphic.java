/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 13.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextGraphic;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocation;

import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XNamed;
import com.sun.star.frame.XController;
import com.sun.star.frame.XModel;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextGraphic implements TextGraphic, OOoTextDocumentComponent
{
  private XModel m_oXModel;
  private XTextContent m_oXTextContent = null;
  private Logger logger = Logger.getLogger(getClass());
  
  public OOoTextGraphic(XModel xmodel, XTextContent graphic)
  {
    m_oXTextContent = graphic;
    m_oXModel = xmodel;
  }

  public XTextContent getXTextContent()
  {
    return m_oXTextContent;
  }

  public float getWidth()
  {
    try
    {
      XPropertySet xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
      Integer graphicWidth = (Integer)(xPropertySet.getPropertyValue("Width"));
      if (graphicWidth != null) return graphicWidth.intValue();
    }
    catch(UnknownPropertyException e) 
    {
        logger.error("Error", e );
    }
    catch(WrappedTargetException e) 
    {
        logger.error("Error", e );
    }
    
    return -1;
  }

  public float getHeight()
  {
    try
    {
      XPropertySet xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
      Integer graphicHeight = (Integer)(xPropertySet.getPropertyValue("Height"));
      if (graphicHeight != null) return graphicHeight.intValue();
    }
    catch(UnknownPropertyException e) 
    {
        logger.error("Error", e );
    }
    catch(WrappedTargetException e) 
    {
        logger.error("Error", e );
    }
    
    return -1;
  }


  public String getURL()
  {
    try
    {
      XPropertySet xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
      return (String)(xPropertySet.getPropertyValue("GraphicURL"));
    }
    catch(UnknownPropertyException e) 
    {
        logger.error("Error", e );
    }
    catch(WrappedTargetException e) 
    {
        logger.error("Error", e );
    }
    return null;
  }
  
  
  public boolean isPrintable()
  {
    XPropertySet xGfxProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
    try
    {
      Boolean print = (Boolean)(xGfxProps.getPropertyValue("Print"));
      if (print != null)
      {
        return print.booleanValue();
      }
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("Error", e1 );
    }
    catch (WrappedTargetException e1)
    {
        logger.error("Error", e1 );
    }
    
    return false;
  }
  
  
  public boolean setPrintable(boolean printable)
  {
    XPropertySet xGfxProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
    try
    {
      xGfxProps.setPropertyValue("Print", new Boolean(printable));
      return true;
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("Error", e1 );
    }
    catch (WrappedTargetException e)
    {
        logger.error("Error", e );
    }
    catch (PropertyVetoException e)
    {
        logger.error("Error", e );
    }
    catch (IllegalArgumentException e)
    {
        logger.error("Error", e );
    }
    
    return false;
  }


  public String getName()
  {    
    XNamed xNamed = (XNamed) UnoRuntime.queryInterface(XNamed.class, m_oXTextContent);
    if (xNamed != null) return xNamed.getName();
    
    return null;
  }


  public boolean setContrast(double contrast)
  {
    XPropertySet xGfxProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
    try
    {
      short contrastValue = (short)((contrast * 200.0) - 100.0);
      xGfxProps.setPropertyValue("AdjustContrast", new Short(contrastValue));
      return true;
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("Error", e1 );
    }
    catch (WrappedTargetException e)
    {
        logger.error("Error", e );
    }
    catch (PropertyVetoException e)
    {
        logger.error("Error", e );
    }
    catch (IllegalArgumentException e)
    {
        logger.error("Error", e );
    }
    return false;
  }


  public boolean setBrightness(double brightness)
  {
    XPropertySet xGfxProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
    try
    {
      short brightnessValue = (short)((brightness * 200.0) - 100.0);
      xGfxProps.setPropertyValue("AdjustLuminance", new Short(brightnessValue));
      return true;
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("Error", e1 );
    }
    catch (WrappedTargetException e)
    {
        logger.error("Error", e );
    }
    catch (PropertyVetoException e)
    {
        logger.error("Error", e );
    }
    catch (IllegalArgumentException e)
    {
        logger.error("Error", e );
    }
    return false;
  }



  public double getContrast()
  {
    XPropertySet xGfxProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
    try
    {
      Short contrast = (Short)(xGfxProps.getPropertyValue("AdjustContrast"));
      if (contrast != null)
      {
        return (contrast.shortValue() + 100.0) / 200.0;
      }
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("Error", e1 );
    }
    catch (WrappedTargetException e)
    {
        logger.error("Error", e );
    }
    return Double.NaN;
  }


  public double getBrightness()
  {
    XPropertySet xGfxProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextContent);
    try
    {
      Short brightness = (Short)(xGfxProps.getPropertyValue("AdjustLuminance"));
      if (brightness != null)
      {
        return (brightness.shortValue() + 100.0) / 200.0;
      }
    }
    catch (UnknownPropertyException e1)
    {
        logger.error("Error", e1 );
    }
    catch (WrappedTargetException e)
    {
        logger.error("Error", e );
    }
    return Double.NaN;
  }

  public XModel getXModel()
  {
    return m_oXModel;
  }
  
  public TextContentLocation getTextContentLocation()
  {
   return new OOoTextContentLocation(getXModel(), this);
  }

  public TextContentLocation getTextContentLocationAfter()
  {
      OOoTextContentLocation loc =  new OOoTextContentLocation(getXModel(), this);
      loc.positionAfterComponent(this,null);
      return loc;
  }

    public boolean remove()
    {
        XController xController                     = m_oXModel.getCurrentController();    
        XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
        XTextViewCursor xViewCursor                 = xViewCursorSupplier.getViewCursor();   
        XText xText                                 = xViewCursor.getText();
        
        try
        {
          XTextContent xGfxTextContent = getXTextContent();
          xText.removeTextContent(xGfxTextContent);
          return true;
        }
        catch(NoSuchElementException ne)
        {
            logger.error( ne );
        }
        return false;
    }
}
