/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.03.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextFont;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextFont;

import com.sun.star.awt.FontSlant;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.WrappedTargetException;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextFont implements TextFont
{
	private XPropertySet m_oXPropertySet = null;
	private Logger logger = Logger.getLogger(getClass());

	public OOoTextFont(XPropertySet props)
	{
		m_oXPropertySet = props;
	}


	public String getFontName()
	{    
		try
		{
			return (String)(m_oXPropertySet.getPropertyValue("CharFontName"));
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}

		return null;
	}

	public double getFontSize()
	{
		try
		{
			Float fontSize = (Float)(m_oXPropertySet.getPropertyValue("CharHeight"));
			if (fontSize != null)
			{
				return fontSize.intValue();
			}
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}

		return Double.NaN;
	}


	public boolean isBold()
	{
		try
		{
			Float bold = (Float)(m_oXPropertySet.getPropertyValue("CharWeight"));
			if (bold != null)
			{
				return ((bold.floatValue() == com.sun.star.awt.FontWeight.BOLD));
			}
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}

		return false;
	}


	public boolean isItalic()
	{
		try
		{
			FontSlant fontslant = (FontSlant)(m_oXPropertySet.getPropertyValue("CharPosture"));
			if (fontslant != null)
			{
				return ((fontslant.getValue() == com.sun.star.awt.FontSlant.ITALIC_value));
			}
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}

		return false;
	}


	public boolean isUnderline()
	{
		try
		{
			Short underline = (Short)(m_oXPropertySet.getPropertyValue("CharUnderline"));
			if (underline != null)
			{
				return ((underline.shortValue() == com.sun.star.awt.FontUnderline.SINGLE));
			}
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}

		return false;
	}


	public VirtualTextFont parseFont()
	{
		VirtualTextFont vfont = new VirtualTextFont();
		vfont.setFontName(getFontName());
		vfont.setFontHeight(getFontSize());
		vfont.setBold(isBold());
		vfont.setItalic(isItalic());
		vfont.setUnderline(isUnderline());    
		return vfont;
	}

}
