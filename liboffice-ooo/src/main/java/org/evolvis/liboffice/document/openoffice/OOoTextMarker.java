/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 28.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocation;

import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.frame.XModel;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextRange;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextMarker implements TextMarker, OOoTextDocumentComponent
{
  private XTextContent m_oXTextContent = null;
  private XModel m_oXModel;
  private Logger logger = Logger.getLogger(getClass());
  
  public OOoTextMarker(XModel xmodel, XTextContent marker)
  {
    m_oXModel = xmodel;
    m_oXTextContent = marker;
  }
  
  public OOoTextMarker(OOoTextDocument doc, String name)
  {
    m_oXModel = doc.getXModel();
    m_oXTextContent = createMarker(doc, name, null, null);
  }

  public OOoTextMarker(OOoTextDocument doc, String name, String hint)
  {
    m_oXModel = doc.getXModel();
    m_oXTextContent = createMarker(doc, name, hint, null);
  }
  
  public OOoTextMarker(OOoTextDocument doc, String name, String hint, short type)
  {
    m_oXModel = doc.getXModel();
    m_oXTextContent = createMarker(doc, name, null, new Integer(type));
  }
  
  private XTextContent createMarker(OOoTextDocument document, String name, String hint, Integer type)
  {
    XMultiServiceFactory xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, document.getXModel());
    
    XPropertySet xPropertySet = null;
    try
    {
      Object textfieldobject = xMSF.createInstance("com.sun.star.text.TextField.JumpEdit");
      if (textfieldobject != null)
      {
        XTextContent textfield = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldobject);      
        xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, textfield);
        if (hint != null) xPropertySet.setPropertyValue ("Hint", hint);
        if (name != null) xPropertySet.setPropertyValue ("PlaceHolder", name);
        if (type != null) xPropertySet.setPropertyValue ("PlaceHolderType", type);
        return textfield;
      }
    }
    catch (Exception e)
    {
        logger.error("Error in createMarker", e);
    }
    return null;
  }
  
  
  
  public XTextContent getXTextContent()
  {
    return m_oXTextContent;
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.document.msoffice.OOoTextDocumentComponent#getXModel()
   */
  public XModel getXModel()
  {
    return m_oXModel;
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.document.TextMarker#getName()
   */
  public String getName()
  {
    XPropertySet xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, getXTextContent());
    Object obj;
    try
    {
      obj = xPropertySet.getPropertyValue("PlaceHolder");
      if (obj != null)
      {
        if (obj instanceof String)
        {
          return (String)obj;
        }
      }
    }
    catch (UnknownPropertyException e)
    {
      e.printStackTrace();
    }
    catch (WrappedTargetException e)
    {
      e.printStackTrace();
    }
    return null;
  }

  public String getHint()
  {
    XPropertySet xPropertySet = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, getXTextContent());
    Object obj;
    try
    {
      obj = xPropertySet.getPropertyValue("Hint");
      if (obj != null)
      {
        if (obj instanceof String)
        {
          return (String)obj;
        }
      }
    }
    catch (UnknownPropertyException e)
    {
      e.printStackTrace();
    }
    catch (WrappedTargetException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  
  public TextContentLocation getTextContentLocation()
  {
   return new OOoTextContentLocation(getXModel(), this);
  }

  public TextContentLocation getTextContentLocationAfter()
  {
      OOoTextContentLocation loc =  new OOoTextContentLocation(getXModel(), this);
      loc.positionAfterComponent(this, null);
      return loc;
  }

    public boolean remove()
    {
        XTextContent content = m_oXTextContent;
        XTextRange range = content.getAnchor();      
        range.setString("");
        return true;
    }
}
