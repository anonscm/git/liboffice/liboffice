/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 26.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.TextTableCell;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.openoffice.OOoTextContentLocation;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;

import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.frame.XController;
import com.sun.star.frame.XDispatchHelper;
import com.sun.star.frame.XDispatchProvider;
import com.sun.star.frame.XModel;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.table.BorderLine;
import com.sun.star.table.CellHoriJustify;
import com.sun.star.table.CellVertJustify;
import com.sun.star.table.XCell;
import com.sun.star.table.XCellRange;
import com.sun.star.text.TableColumnSeparator;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextTable;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;


/**
 * eine TextTable f&uml;r OpenOffice...
 * 
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 */
public class OOoTextTable extends TextTable implements OOoTextDocumentComponent
{
	private XTextTable m_oXTextTable;
	private XModel m_oXModel;  
	private boolean m_bSetJustification = false;
	private OOoTextDocument m_oOOoTextDocument = null;
	private Logger logger = Logger.getLogger(getClass());
	
	public OOoTextTable(OOoTextDocument doc, XTextTable oXTextTable)
	{
		m_oXModel                  = doc.getXModel();
		m_oXTextTable             = oXTextTable;
		m_oOOoTextDocument = doc;
	}
	
	public OOoTextTable(OOoTextDocument doc, int rows, int columns)
	{
		m_oXModel                  = doc.getXModel();
		m_oOOoTextDocument = doc;
		Object oInt;
		try
		{
			XMultiServiceFactory xMSF = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, doc.getXModel());
			oInt = xMSF.createInstance("com.sun.star.text.TextTable");
			m_oXTextTable = (XTextTable) UnoRuntime.queryInterface(XTextTable.class, oInt);
			m_oXTextTable.initialize(rows, columns);
			
			XPropertySet xTableProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextTable);
			try
			{
				xTableProps.setPropertyValue("HoriOrient", new Short((short)com.sun.star.text.HoriOrientation.FULL));
			}
			catch (UnknownPropertyException e) 
			{
				logger.error("Error", e );
			}
			catch (PropertyVetoException e) 
			{
				logger.error("Error", e );
			}
			catch (IllegalArgumentException e) 
			{
				logger.error("Error", e );
			}
			catch (WrappedTargetException e) 
			{
				logger.error("Error", e );
			}    
		}
		catch (Exception e)
		{
			logger.error("Error", e );
		}
	}
	
	private boolean setCellBorderLine(XTextContent xTextContent, int row, int col, String linename, BorderLine line)
	{
		try
		{
			XCellRange xCellRange       = (XCellRange)UnoRuntime.queryInterface(XCellRange.class, xTextContent);
			XCellRange xCellRangeSingle = xCellRange.getCellRangeByPosition(col, row, col, row);      
			XPropertySet xCellProps     = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xCellRangeSingle);
			xCellProps.setPropertyValue(linename, line);
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error in setCellBorderLine", e );
			return false;
		}
		catch (PropertyVetoException e)
		{
			logger.error("Error in setCellBorderLine", e );
			return false;
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error in setCellBorderLine", e );
			return false;
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error in setCellBorderLine", e );
			return false;
		} 
		catch (IndexOutOfBoundsException e)
		{
			logger.error("Error in setCellBorderLine", e );
			return false;
		}
		return true;
	}
	
	private XText getCellTextHandle(XTextTable TT1, int xp, int yp)
	{
		XCell cell = getCell(TT1, xp, yp);    
		if (cell != null)
		{
			XText oTableText = (XText) UnoRuntime.queryInterface(XText.class, cell);
			return oTableText;
		}
		return (null);
	}
	
	private XCell getCell(XTextTable TT1, int xp, int yp)
	{
		String cellName = OOoTextTableCell.getCellName(xp, yp);
		return TT1.getCellByName(cellName);
	}
	
	
	
	public XTextTable getXTextTable()
	{
		return m_oXTextTable;
	}
	
	public OOoTextDocument getOOoTextDocument()
	{
		return m_oOOoTextDocument;
	}
	
	
	// in cm * 1000 !
	private boolean setColumnWidth(OOoTextDocument textdoc, int column, int width)  
	{
		boolean result = false;
		
		int numrows = m_oXTextTable.getRows().getCount();
		int numcols = m_oXTextTable.getColumns().getCount();
		
		if ((numrows > 0) && (numcols > 0))
		{
			XPropertySet xTableProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextTable);
			try
			{
				TableColumnSeparator[] separators = (TableColumnSeparator[])(xTableProps.getPropertyValue("TableColumnSeparators"));
				if (separators != null)
				{
					if (column < separators.length)
					{
						// Faktor: 0.5882352941176471
						int newwidth = (int)(((double)width * 0.5882352941176471) /** val*/);        
						
						int offset = 0;
						if (column > 0) offset = (separators[column - 1].Position);
						
						short total = (short)(newwidth + offset);        
						
						separators[column].Position = total;        
						xTableProps.setPropertyValue("TableColumnSeparators", separators);
						result = true;
					}
				}        
			}
			catch (Exception e) 
			{
				logger.error("Error", e );
			}
		}
		
		return result;
	}
	
	private void resizeTable(XTextTable table, int leftmargin, int rightmargin)
	{
		// 1000 = 1cm
		XPropertySet xTableProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, table);
		try
		{
			xTableProps.setPropertyValue("HoriOrient", new Short((short)0));
			xTableProps.setPropertyValue("LeftMargin", new Integer(leftmargin));
			xTableProps.setPropertyValue("RightMargin", new Integer(rightmargin));
		}
		catch (UnknownPropertyException e) 
		{
			logger.error("Error", e );
		}
		catch (PropertyVetoException e) 
		{
			logger.error("Error", e );
		}
		catch (IllegalArgumentException e) 
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e) 
		{
			logger.error("Error", e );
		}    
	}
	
	
	
	private boolean dispatchUnoCommand(OOoTextDocument textdoc, String command)
	{
		try
		{
			XMultiServiceFactory xMSF = (XMultiServiceFactory) textdoc.getOfficeConnection().getXMultiServiceFactory();
			XDispatchProvider oDispatchProvider = (XDispatchProvider)UnoRuntime.queryInterface(XDispatchProvider.class, textdoc.getXModel().getCurrentController().getFrame());
			
			Object dho = xMSF.createInstance("com.sun.star.frame.DispatchHelper");
			if (dho != null)
			{
				XDispatchHelper oDispatchHelper = (XDispatchHelper)UnoRuntime.queryInterface(XDispatchHelper.class, dho);
				if (oDispatchHelper != null)
				{
					PropertyValue[] props = new PropertyValue[0];     
					oDispatchHelper.executeDispatch(oDispatchProvider, ".uno:" + command, "", 0, props);
					return true;
				}
			}
		}
		catch (Throwable t)
		{
			logger.error("Error", t );
		}
		return false;
	}
	
	
	private boolean setColumnOptimalWidth(OOoTextDocument textdoc, int column)  
	{
		boolean result = false;
		
		int numrows = m_oXTextTable.getRows().getCount();
		int numcols = m_oXTextTable.getColumns().getCount();
		
		if ((numrows > 0) && (numcols > 0))
		{
			// Handle auf Zelle in der ersten Spalte der Tabelle erlangen... 
			XText xtext = getCellTextHandle(m_oXTextTable, column, 0);
			XTextCursor cursor = xtext.createTextCursor();         
			
			// den (sichtbaren) Cursor in die Spalte der Tabelle positionieren... 
			XController xController = textdoc.getXModel().getCurrentController();
			XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
			XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();
			xViewCursor.gotoRange(cursor, false);
			
			dispatchUnoCommand(textdoc, "EntireColumn");
			result = dispatchUnoCommand(textdoc, "SetOptimalColumnWidth");    
			
			if (result)
			{
				resizeTable(m_oXTextTable, 0, 0);
			}
		}
		return result;
	}
	
	/*
	 private void showServices(Object object)
	 {
	 XServiceInfo xServiceInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, object);
	 if (xServiceInfo != null) 
	 {  
	 String[] services = xServiceInfo.getSupportedServiceNames();
	 String text = "";
	 for(int i=0; i<(services.length); i++)
	 {
	 text += "#" + i + ": " + services[i] + "\n";
	 }
	 JOptionPane.showMessageDialog(null, "Services of " + object + ":\n\n" + text);
	 }
	 else JOptionPane.showMessageDialog(null, "NULL Services");    
	 }
	 */
	
	public void setVirtualTextTable(OOoTextDocument textdoc, VirtualTextTable table)
	{    
		if (table == null) return;
		//XMultiServiceFactory xMSF = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, textdoc.getXModel());
		
		
		// fill table with text...
		for (int y = 0; y < table.getNumberOfRows(); y++)
		{            
			for (int x = 0; x < table.getNumberOfColumns(); x++)
			{
				VirtualTextTableCell cell = table.getTextTableCell(y, x);
				insertCell(y, x, cell);
			}
		}
		
		
		for (int column = 0; column < table.getNumberOfColumns(); column++)
		{          
			Object width = table.getColumnWidth(column);
			if (!(VirtualTextTable.COLUMNWIDTH_NONE.equals(width)))
			{
				if (VirtualTextTable.COLUMNWIDTH_AUTOMATIC.equals(width))
				{
					setColumnOptimalWidth(textdoc, column);                  
				}
				else
				{
					if (width instanceof Integer)
					{
						setColumnWidth(textdoc, column, ((Integer)width).intValue());                    
					}
				}
			}              
		}
	}
	
	public void addRows(int index, int count)
	{
		m_oXTextTable.getRows().insertByIndex(index, count);
	}
	
	public void removeRows(int index, int count)
	{
		m_oXTextTable.getRows().removeByIndex(index, count);
	}  
	
	public int getNumberOfRows()
	{
		return m_oXTextTable.getRows().getCount();
	}
	
	public int getNumberOfColumns()
	{
		return m_oXTextTable.getColumns().getCount();
	}
	
	public void insertCell(int row, int column, VirtualTextTableCell cell)
	{
		if (cell != null)
		{
			if (m_bSetJustification)
			{
				// set justification of cell           
				XCell xcell = getCell(m_oXTextTable, column, row);    
				XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, xcell);        
				if (xInfo.supportsService("com.sun.star.text.CellProperties"))          
				{          
					XPropertySet xCellProps = null;            
					
					XCellRange xCellRange = (XCellRange) UnoRuntime.queryInterface(XCellRange.class, m_oXTextTable);
					try
					{
						XCellRange xSmallCellRange = xCellRange.getCellRangeByPosition(row, column, row, column);
						xCellProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xSmallCellRange);
					}
					catch (IndexOutOfBoundsException e1)
					{
						logger.error("Error", e1 );
					}
					
					try
					{
						Object horiz = cell.getHorizontalJustification();
						
						Object horizjust = null;
						if      (VirtualTextTableCell.HORIZONTAL_JUSTIFY_LEFT.equals(horiz)) horizjust = new Integer(CellHoriJustify.LEFT_value);
						else if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_RIGHT.equals(horiz)) horizjust = new Integer(CellHoriJustify.RIGHT_value);
						else if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_CENTER.equals(horiz)) horizjust = new Integer(CellHoriJustify.CENTER_value);
						else horizjust = new Integer(CellHoriJustify.STANDARD_value);
						
						Object vertjust = null;
						if      (VirtualTextTableCell.VERTICAL_JUSTIFY_TOP.equals(horiz)) vertjust = new Integer(CellVertJustify.TOP_value);
						else if (VirtualTextTableCell.VERTICAL_JUSTIFY_BOTTOM.equals(horiz)) vertjust = new Integer(CellVertJustify.BOTTOM_value);
						else if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_CENTER.equals(horiz)) vertjust = new Integer(CellVertJustify.CENTER_value);
						else vertjust = new Integer(CellVertJustify.STANDARD_value);
						
						//TODO: cell-justification fixen...                               
						xCellProps.setPropertyValue("HoriJustify", horizjust);
						xCellProps.setPropertyValue("VertJustify", vertjust);
					}
					catch (UnknownPropertyException e) 
					{
						logger.error("Error", e );
					}
					catch (PropertyVetoException e) 
					{
						logger.error("Error", e );
					}
					catch (IllegalArgumentException e) 
					{
						logger.error("Error", e );
					}
					catch (WrappedTargetException e) 
					{
						logger.error("Error", e );
					}
				}
				else
				{
					logger.warn("setVirtualTextTable(): bad service");
				}
			}
			
			//create a cursor object
			XText text = getCellTextHandle(m_oXTextTable, column, row);          
			XTextCursor cursor = text.createTextCursor();
			m_oOOoTextDocument.insertText(text, cursor, cell.getStyledText());
		}
	}
	
	
	public void clearCell(int row, int column)
	{
		XCell cell = getCell(m_oXTextTable, column, row);
		XText xCellText = (XText) UnoRuntime.queryInterface(XText.class, cell);
		xCellText.setString("");
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.document.msoffice.OOoTextDocumentComponent#getXModel()
	 */
	public XModel getXModel()
	{
		return m_oXModel;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.document.msoffice.OOoTextDocumentComponent#getXTextContent()
	 */
	public XTextContent getXTextContent()
	{
		return (XTextContent)UnoRuntime.queryInterface(XTextContent.class, m_oXTextTable);    
	}
	
	
	public TextTableCell getCell(int row, int column)
	{
		return new OOoTextTableCell(m_oOOoTextDocument, this, row, column);
	}
	
	public boolean setCellBorderLineVisible(int row, int col, Object line, boolean visible)
	{
		
		String linename = null;
		if (TextTableCell.BORDER_LEFT.equals(line))
		{
			linename = "LeftBorder";
		}    
		else if (TextTableCell.BORDER_RIGHT.equals(line))
		{
			linename = "RightBorder";
		}
		else if (TextTableCell.BORDER_TOP.equals(line))
		{
			linename = "TopBorder";
		}
		else if (TextTableCell.BORDER_BOTTOM.equals(line))
		{
			linename = "BottomBorder";
		}
		else return false;
		
		BorderLine borderline = null;
		if (visible)
		{
			borderline = new BorderLine();
			borderline.Color = 0x000000;
			borderline.OuterLineWidth = 5;
		}
		else
		{    
			borderline = new BorderLine();
			borderline.Color = 0x000000;
			borderline.OuterLineWidth = 0;
		}
		
		if (borderline != null)
		{
			return setCellBorderLine(m_oXTextTable, row, col, linename, borderline);      
		}
		return false;    
	}
	
	public boolean setTableLeftMargin(float margin)
	{
		//    1000 = 1cm
		XPropertySet xTableProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextTable);
		try
		{
			xTableProps.setPropertyValue("HoriOrient", new Short(com.sun.star.text.HoriOrientation.LEFT_AND_WIDTH));      
			xTableProps.setPropertyValue("LeftMargin", new Integer((int)margin));
			return true;
		}
		catch (UnknownPropertyException e) 
		{
			logger.error("Error", e );
		}
		catch (PropertyVetoException e)
		{
			logger.error("Error", e );
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}
		return false;
	}
	
	public boolean setTableWidth(float width)
	{
		//      1000 = 1cm
		XPropertySet xTableProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, m_oXTextTable);
		try
		{
			xTableProps.setPropertyValue("HoriOrient", new Short(com.sun.star.text.HoriOrientation.LEFT_AND_WIDTH));      
			xTableProps.setPropertyValue("Width", new Integer((int)width));
			return true;
		}
		catch (UnknownPropertyException e)
		{
			logger.error("Error", e );
		}
		catch (PropertyVetoException e)
		{
			logger.error("Error", e );
		}
		catch (IllegalArgumentException e)
		{
			logger.error("Error", e );
		}
		catch (WrappedTargetException e)
		{
			logger.error("Error", e );
		}
		return false;
	}
	
	public TextContentLocation getTextContentLocation()
	{
		return new OOoTextContentLocation(getXModel(), this);
	}
	
	public TextContentLocation getTextContentLocationAfter()
	{
		OOoTextContentLocation loc =  new OOoTextContentLocation(getXModel(), this);
		loc.positionAfterComponent(this, m_oOOoTextDocument);
		return loc;
	}
	
	public boolean remove()
	{
		XController xController = m_oXModel.getCurrentController();    
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   
		XText xText = xViewCursor.getText();
		
		try
		{
			XTextContent xTableTextContent = getXTextContent();
			xText.removeTextContent(xTableTextContent);
			return true;
		}
		catch(NoSuchElementException ne)
		{
			logger.error( ne );
		}
		return false;
	}
	
	
	public void select(boolean append)
	{
		XController xController                                   = m_oXModel.getCurrentController();    
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		XTextViewCursor xViewCursor                        = xViewCursorSupplier.getViewCursor();
		String cellName                                              = OOoTextTableCell.getCellName(0,0);
		XTextRange  rangeStart                                 = (XTextRange)UnoRuntime.queryInterface(XTextRange.class, m_oXTextTable.getCellByName(cellName));
		xViewCursor.gotoRange(rangeStart, append);
		xViewCursor.gotoEnd(true);
	}
	
	public void copy()
	{
		m_oOOoTextDocument.copy();
	}
	
	public void select()
	{
		select( false );
	}

	@Override
	public void addColumns(int index, int count) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeColumns(int index, int count) {
		// TODO Auto-generated method stub
		
	}
}