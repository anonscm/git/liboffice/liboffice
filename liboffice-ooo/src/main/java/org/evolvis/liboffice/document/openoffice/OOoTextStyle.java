/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.03.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.openoffice;

import org.evolvis.liboffice.document.TextFont;
import org.evolvis.liboffice.document.TextStyle;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyleType;

import com.sun.star.beans.XPropertySet;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoTextStyle implements TextStyle
{
	private Object m_oStyleType = null;
	private com.sun.star.style.XStyle m_oXStyle = null;

	public OOoTextStyle(com.sun.star.style.XStyle oXStyle)
	{
		m_oXStyle = oXStyle;
	}

	public OOoTextStyle(com.sun.star.style.XStyle oXStyle, Object type)
	{
		m_oXStyle = oXStyle;
		m_oStyleType = type;
	}

	public com.sun.star.style.XStyle getXStyle()
	{
		return m_oXStyle;
	}

	public String getStyleName()
	{
		return m_oXStyle.getName();
	}


	public TextFont getTextFont()
	{
		XPropertySet xSourcePropertySet = (com.sun.star.beans.XPropertySet) UnoRuntime.queryInterface(com.sun.star.beans.XPropertySet.class, m_oXStyle);
		if (xSourcePropertySet != null)
		{
			return new OOoTextFont(xSourcePropertySet);
		}
		return null;
	}

	public Object getStyleType()
	{
		return m_oStyleType;
	}

	public VirtualTextStyle parseStyle()
	{
		VirtualTextStyleType vstyletype = null;
		if (TextStyle.STYLE_PARAGRAPH.equals(getStyleType())) vstyletype = VirtualTextStyleType.STYLE_PARAGRAPH;
		else if (TextStyle.STYLE_CHARACTER.equals(getStyleType())) vstyletype = VirtualTextStyleType.STYLE_CHARACTER;

		VirtualTextStyle vstyle = new VirtualTextStyle(getStyleName(), vstyletype);

		TextFont font = getTextFont();
		if (font != null)
		{
			vstyle.setFont(font.parseFont());
		}
		return vstyle;
	}  
}
