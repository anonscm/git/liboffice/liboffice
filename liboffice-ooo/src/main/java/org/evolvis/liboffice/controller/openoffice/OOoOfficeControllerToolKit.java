/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.controller.openoffice;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.controller.OfficeControllerToolKit;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextTable;
import org.evolvis.liboffice.document.openoffice.OOoTextTableCell;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.virtualdocument.VirtualDocumentInformation;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualPageBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;

import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.frame.XController;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XParagraphCursor;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextFieldsSupplier;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextTable;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 */
public class OOoOfficeControllerToolKit extends MainOfficeFactoryFetcherAdapter implements OfficeControllerToolKit
{
	private Logger logger = Logger.getLogger(getClass());
	
	public int getNumberOfMarkerOccurrences(TextDocument document, String markername, String hint)
	{
		if (((OOoTextDocument)document).getXModel() == null) return -1;
		XMultiServiceFactory xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, ((OOoTextDocument)document).getXModel());
		if (xMSF == null) return -1;    
		XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, ((OOoTextDocument)document).getXModel());
		if (xTextFieldsSupplier == null) return -1;    
		XEnumerationAccess xEnumerationAccess = xTextFieldsSupplier.getTextFields();
		if (xEnumerationAccess == null) return -1;    
		XEnumeration xTextFieldEnum = xEnumerationAccess.createEnumeration();
		if (xTextFieldEnum == null) return -1;    
		
		int counter = 0;
		while (xTextFieldEnum.hasMoreElements()) 
		{
			try
			{
				Object textfieldobject = xTextFieldEnum.nextElement();
				XTextContent textfield = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldobject);
				
				XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textfield);
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
					String markerName = (String)(xPropertySet.getPropertyValue("PlaceHolder"));
					String markerHint = (String)(xPropertySet.getPropertyValue("Hint"));
					
					if (markerName.equals(markername))
					{       
						if (hint != null)
						{
							if (hint.equals(markerHint)) counter ++; 
						} else counter ++;
					}
				}
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error", e);
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error", e);
			} 
			catch (UnknownPropertyException e)
			{
				logger.error("Error", e);
			} 
		}
		return(counter);
	}
	
	public int getNumberOfMarkers(TextDocument document)
	{
		if (((OOoTextDocument)document).getXModel() == null) return -1;
		XMultiServiceFactory xMSF = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, ((OOoTextDocument)document).getXModel());
		if (xMSF == null) return -1;    
		XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, ((OOoTextDocument)document).getXModel());
		if (xTextFieldsSupplier == null) return -1;    
		XEnumerationAccess xEnumerationAccess = xTextFieldsSupplier.getTextFields();
		if (xEnumerationAccess == null) return -1;    
		XEnumeration xTextFieldEnum = xEnumerationAccess.createEnumeration();
		if (xTextFieldEnum == null) return -1;    
		
		int counter = 0;    
		while (xTextFieldEnum.hasMoreElements()) 
		{
			try
			{
				Object textfieldobject = xTextFieldEnum.nextElement();
				XTextContent textfield = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldobject);
				
				XServiceInfo xInfo = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textfield);
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					//XPropertySet xPropertySet = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
					counter ++;
				}
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error", e);
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error", e);
			} 
		}
		return(counter);
	}
	
	public boolean insertDocument(TextDocument document, VirtualTextDocument documenttoinsert)
	{  
		boolean retValue = true;
		for(int i=0; i<(documenttoinsert.getNumberOfTextComponents()); i++)
		{
			VirtualTextComponent vtc = documenttoinsert.getTextComponent(i);
			if (vtc instanceof VirtualParagraph)
			{
				boolean ret =  document.insertText( new VirtualStyledText((VirtualParagraph)vtc));
				if(! ret ) retValue = ret;
			}
			else if (vtc instanceof VirtualTextTable)
			{
				TextTable table = document.insertTable((VirtualTextTable)vtc);
				if(null == table) retValue = false;
			}
			else if (vtc instanceof VirtualPageBreak)
			{
				boolean ret =  document.insertPageBreak();
				if(! ret ) retValue = false;
			}
			else if (vtc instanceof VirtualStyledText)
			{
				boolean ret =  document.insertText((VirtualStyledText)vtc);
				if(! ret ) retValue = ret;
			}
			else logger.warn("Unknown component in insertDocument: " + vtc.getClass());
		}
		return retValue;
	}
	
	public boolean insertVirtualTextComponent(TextDocument document, VirtualTextComponent componenttoinsert)
	{  
		if (componenttoinsert instanceof VirtualParagraph)
		{
			document.insertText( new VirtualStyledText((VirtualParagraph)componenttoinsert));
			return true;
		}
		if (componenttoinsert instanceof VirtualStyledText)
		{
			document.insertText( (VirtualStyledText)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextTable)
		{
			document.insertTable( (VirtualTextTable)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextDocument)
		{
			insertDocument(document, (VirtualTextDocument)componenttoinsert);
			return true;
		}
		else logger.warn("Unsupported VirtualTextComponent: " + componenttoinsert.getClass());
		return false;
	}
	
	public boolean jumpToMarker(TextDocument document, String markername)
	{
		TextDocumentComponent marker = document.locateTextDocumentComponent(new NameHintMarkerDescriptor(markername));
		if (marker != null)
		{
			return document.jumpToLocation(marker.getTextContentLocation());
		}
		return false;
	}
	
	
	public boolean jumpToTable(TextDocument document, TextTable table)
	{
		return document.jumpToLocation(table.getTextContentLocation());
	}
	
	
	public boolean jumpToTableCell(TextDocument document, TextTable table, int row, int column)
	{
		XText text = getCellTextHandle(((OOoTextTable)table).getXTextTable(), column, row);
		if (text == null) return false;
		//create a cursor object
		XTextCursor xModelCursor = text.createTextCursor();          
		if (xModelCursor == null) return false;
		
		// den (sichtbaren) Cursor positionieren... 
		XController xController = ((OOoTextDocument)document).getXModel().getCurrentController();
		if (xController == null) return false;
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		if (xViewCursorSupplier == null) return false;
		XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();
		if (xViewCursor == null) return false;
		xViewCursor.gotoRange(xModelCursor, false);    
		return true;
	}
	
	private XText getCellTextHandle(XTextTable TT1, int xp, int yp)
	{
		String cellName = OOoTextTableCell.getCellName(xp, yp);
		return (XText) UnoRuntime.queryInterface(XText.class, TT1.getCellByName(cellName));
	}
	
	
	
	public boolean removeAllMarkers(TextDocument document)
	{
		//XMultiServiceFactory xMSF             = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, ((OOoTextDocument)document).getXModel());
		XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, ((OOoTextDocument)document).getXModel());
		XEnumerationAccess xEnumerationAccess   = xTextFieldsSupplier.getTextFields();
		XEnumeration xTextFieldEnum             = xEnumerationAccess.createEnumeration();
		
		while (xTextFieldEnum.hasMoreElements()) 
		{
			try
			{
				Object textfieldobject              = xTextFieldEnum.nextElement();
				XTextContent textfield              = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldobject);
				
				XServiceInfo xInfo                  = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textfield);
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					//XPropertySet xPropertySet       = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);
					
					XTextRange xLineTextRange         = textfield.getAnchor();
					XText xDocumentText               = xLineTextRange.getText();              
					XTextCursor xModelCursor          = xDocumentText.createTextCursorByRange(xLineTextRange.getStart());
					
					XParagraphCursor xParaCursor      = (XParagraphCursor)UnoRuntime.queryInterface(XParagraphCursor.class, xModelCursor);
					
					xParaCursor.gotoStartOfParagraph(false);
					xParaCursor.gotoEndOfParagraph(true);
					
					XTextRange xTextRange             = textfield.getAnchor();
					xTextRange.setString("");
				}
			}
			catch (NoSuchElementException e)
			{
				logger.error("Error", e);
				return false;
			}
			catch (WrappedTargetException e)
			{
				logger.error("Error", e);
				return false;
			} 
		}
		return true;
	}
	
	
	public boolean insertText(TextDocument document, String text)
	{
		return document.insertText( new VirtualStyledText(text));
	}
	
	public boolean removeMarker(TextDocument document, String markername, String option)
	{
		return document.removeMarker(new VirtualTextPortionMarker(markername, option));
	}
	
	public int getVersion()
	{
		return 0;
	}
	
	public boolean setCrossTableBorders(TextTable table)
	{
		throw new UnsupportedOperationException("Method is not implemented");
	}
	
	
	public String getDocumentInformation(TextDocument document, String key)
	{
		List infos = document.getDocumentInformations();
		for(int i=0; i<(infos.size()); i++)
		{
			VirtualDocumentInformation info = (VirtualDocumentInformation)(infos.get(i));
			if (key.equals(info.getKey())) return info.getValue();
		}
		return null;
	}
	
	public boolean closeFirstDocument()
	{
		try
		{
			List docs = getMainOfficeFactory().getControllerFactory().getOfficeController().getOpenDocuments();
			
			if(docs.size() > 0)
			{
				OOoTextDocument doc = (OOoTextDocument) docs.get(0);
				doc.close(false);
			}
			return true;
		}
		catch(Exception pExcp)
		{
			return false;
		}
	}
	
	public boolean closeAllDocuments()
	{
		try
		{
			List docs = getMainOfficeFactory().getControllerFactory().getOfficeController().getOpenDocuments();
			
			Iterator it = docs.iterator();
			
			while(it.hasNext())
			{
				OOoTextDocument doc = (OOoTextDocument) it.next();
				doc.close(true);
			}
			return true;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return false;
		}
	}
}