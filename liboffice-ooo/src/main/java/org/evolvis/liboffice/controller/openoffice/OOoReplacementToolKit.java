/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.controller.openoffice;

import java.util.logging.Logger;

import org.evolvis.liboffice.controller.ComponentReplaceJob;
import org.evolvis.liboffice.controller.ComponentReplaceSet;
import org.evolvis.liboffice.controller.ReplacementToolKit;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.document.Selectable;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.ui.Messages;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.graphic.VirtualGraphic;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

import com.sun.star.beans.XPropertySet;
import com.sun.star.container.XEnumeration;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.text.XTextContent;
import com.sun.star.uno.UnoRuntime;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.utils.TaskManager.Context;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoReplacementToolKit implements ReplacementToolKit
{
	private final static Logger logger = Logger.getLogger(OOoReplacementToolKit.class.getName());
	
	public boolean replaceAllMarkersByComponent(TextDocument pDocument, ComponentReplaceSet pReplaceSet)
	{
		boolean success = true;
		XEnumeration xTextFieldEnum = OOoHelper.getTextFieldEnumeration(pDocument);
		
		while(xTextFieldEnum.hasMoreElements())
		{
			try
			{
				Object textfieldObject                  = xTextFieldEnum.nextElement();
				XTextContent textField                  = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldObject);
				
				XServiceInfo xInfo                      = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textField);
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					XPropertySet xPropertySet            = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
					String markerName                    = (String)(xPropertySet.getPropertyValue("PlaceHolder"));
					// String markerHint                  = (String)(xPropertySet.getPropertyValue("Hint"));
					
					ComponentReplaceJob job              =  pReplaceSet.getJob(markerName.trim());
					if(job != null)
					{
						Object replaceObject = job.getComponent();
						if(!OOoHelper.replaceMarkerByObject(pDocument, textField, replaceObject, job.removeLine())) success = false;
					}
				}
			}
			catch (Exception pExcp)
			{
				pExcp.printStackTrace();
				return false;
			}
		}
		return success;
	}

	public boolean replaceAllMarkersByEntity(TextDocument pDocument, Entity pEntity, boolean pRemoveLine)
	{
		XEnumeration xTextFieldEnum = OOoHelper.getTextFieldEnumeration(pDocument);
		
		while(xTextFieldEnum.hasMoreElements())
		{
			try
			{
				Object textfieldObject                  = xTextFieldEnum.nextElement();
				XTextContent textField                  = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldObject);
				
				XServiceInfo xInfo                      = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textField);
				if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
				{
					XPropertySet xPropertySet            = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
					String markerName                    = (String)(xPropertySet.getPropertyValue("PlaceHolder"));
					// String markerHint                  = (String)(xPropertySet.getPropertyValue("Hint"));
					
					Object replaceObject = pEntity.getAttribute(markerName.trim());
					
					boolean returnCode = OOoHelper.replaceMarkerByObject(pDocument, textField, replaceObject, pRemoveLine);
					if(!returnCode) return false;
				}
			}
			catch (Exception pExcp)
			{
				pExcp.printStackTrace();
				return false;
			}
		}
		
		new OOoOfficeControllerToolKit().removeAllMarkers(pDocument);
		
		return true;
	}

	public boolean replaceAllMarkersByEntityListInLabelDocument(TextDocument pDocument, String pFileName, EntityList pEntityList, boolean pRemoveLine, Context pContext)
	{
		int n = 0;
		
		for(int i=0; i < pEntityList.getSize(); i++)
		{
			if(pContext.isCancelled()) break;
			
			pContext.setCurrent(i);
			pContext.setActivityDescription(Messages.getFormattedString("GUI_REPLACE_PROGRESS_DIALOG_NOTE_REPLACE", new Integer(i), new Integer(pEntityList.getSize())));
			
			XEnumeration xTextFieldEnum = OOoHelper.getTextFieldEnumeration(pDocument);
			
			Entity thisEntity;
			
			if(pEntityList.getEntityAt(i) instanceof Entity)
			{
				thisEntity = (Entity)pEntityList.getEntityAt(i);
			}
			else
			{
				logger.warning("Data in EntityList does not implement Entity-Interface.");
				return false;
			}
			
			while(xTextFieldEnum.hasMoreElements()) 
			{
				try
				{
					Object textfieldobject                  = xTextFieldEnum.nextElement();
					XTextContent textField                  = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, textfieldobject);
					
					XServiceInfo xInfo                      = (XServiceInfo) UnoRuntime.queryInterface(XServiceInfo.class, textField);
					if (xInfo.supportsService("com.sun.star.text.TextField.JumpEdit")) 
					{
						XPropertySet xPropertySet            = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xInfo);             
						String markerName                    = (String)(xPropertySet.getPropertyValue("PlaceHolder"));
						String markerHint                  = (String)(xPropertySet.getPropertyValue("Hint"));
						
						if(String.valueOf(n).equals(markerHint.trim()) || "".equals(markerHint.trim()))
						{
							Object replaceObject = thisEntity.getAttribute(markerName.trim());
							
							boolean returnCode = OOoHelper.replaceMarkerByObject(pDocument, textField, replaceObject, pRemoveLine);

							if(! returnCode ) return false;
						}
					}
				}
				catch(Exception pExcp)
				{
					pExcp.printStackTrace();
				}
			}
			
			xTextFieldEnum = OOoHelper.getTextFieldEnumeration(pDocument);
			n++;
			
			// If there are no more markers in document and pEntityList has got more elements 
			if(!xTextFieldEnum.hasMoreElements() && pEntityList.getSize()-i >= 2)
			{	
				// Append template
				pDocument.jumpToLocation(pDocument.getEnd());
				pDocument.insertDocumentFromFile(pFileName);
				n = 0;
			}
		}
		
		pContext.setActivityDescription(Messages.getString("GUI_REPLACE_PROGRESS_DIALOG_NOTE_REMOVE_MARKERS"));
		
		new OOoOfficeControllerToolKit().removeAllMarkers(pDocument);
		
		return true;
	}

	public boolean replaceAllMarkersByEntityListInSingleDocument(TextDocument pDocument, String pFileName, EntityList pEntityList, boolean pRemoveLine, boolean pPageBreak,	Context pContext)
	{
		boolean success = true;
		for(int i=0; i < pEntityList.getSize(); i++)
		{
			if(pContext != null)
			{
				if(pContext.isCancelled()) break;
				pContext.setCurrent(i);
				pContext.setActivityDescription(Messages.getFormattedString("GUI_REPLACE_PROGRESS_DIALOG_NOTE_REPLACE", new Integer(i), new Integer(pEntityList.getSize())));
			}
			
			
			Entity thisEntity;
			
			if(pEntityList.getEntityAt(i) instanceof Entity)
			{
				thisEntity = (Entity)pEntityList.getEntityAt(i);
			}
			else
			{
				logger.warning("Data in EntityList does not implement Entity-Interface.");
				return false;
			}
			
			if(!replaceAllMarkersByEntity(pDocument, thisEntity, pRemoveLine)) success = false;
			
			
			// If another is in the list, add a new document
			if(pEntityList.getSize()-1 > i)
			{
				pDocument.jumpToLocation(pDocument.getEnd());
				pDocument.insertDocumentAtEnd(pFileName);
			}
		}
		
		new OOoOfficeControllerToolKit().removeAllMarkers(pDocument);
		
		return success;
	}

	public boolean replaceFirstMarkerByComponent(TextDocument pDocument, String pMarkerName, Selectable pSelectable)
	{
		TextDocumentComponent marker = pDocument.locateTextDocumentComponent(new NameHintMarkerDescriptor(pMarkerName));
		if (marker != null)
		{
			pDocument.jumpToLocation(marker.getTextContentLocation());
			pSelectable.select();
			pSelectable.copy();
			marker.remove();
			pDocument.paste();
			return true;
		}
		return false;
	}

	public boolean replaceFirstMarkerByComponent(TextDocument pDocument, String pMarkerName, VirtualTextComponent pComponent)
	{
		if(pComponent instanceof VirtualTextTable)
			replaceFirstMarkerByTable(pDocument, pMarkerName, (VirtualTextTable)pComponent);
		
		else if(pComponent instanceof VirtualGraphic)
			replaceFirstMarkerByGraphic(pDocument, pMarkerName, (VirtualGraphic)pComponent);
		
		else if(pComponent instanceof VirtualStyledText)
			replaceFirstMarkerByText(pDocument, pMarkerName, (VirtualStyledText)pComponent);
		
		else
			throw new UnsupportedOperationException("OOoOfficeControllerToolkit.replaceFirstMarkerByComponent() Unknown VirtualTextComponent class:"+pComponent.getClass());
		
		return true;
	}
	
	private boolean replaceFirstMarkerByText(TextDocument document, String markername, VirtualStyledText replaceby)
	{
		TextDocumentComponent marker = document.locateTextDocumentComponent(new NameHintMarkerDescriptor(markername));
		if (marker != null)
		{
			document.jumpToLocation(marker.getTextContentLocation());
			document.insertText( replaceby);
			new OOoOfficeControllerToolKit().removeMarker(document, markername, null);
			//marker = getMainOfficeFactory().getControllerFactory().getOfficeController().locateTextDocumentComponent(document, new NameMarkerDescriptor(markername));
		}    
		return true;    
	}
	
	private boolean replaceFirstMarkerByGraphic(TextDocument document, String markername, VirtualGraphic graphic)
	{
		TextDocumentComponent marker = document.locateTextDocumentComponent( new NameHintMarkerDescriptor(markername));
		if (marker != null)
		{
			document.jumpToLocation(marker.getTextContentLocation());
			document.insertGraphic(graphic);
			new OOoOfficeControllerToolKit().removeMarker(document, markername, null);
			//marker = getMainOfficeFactory().getControllerFactory().getOfficeController().locateTextDocumentComponent(document, new NameMarkerDescriptor(markername));
		}    
		return true;    
	}
	
	private boolean replaceFirstMarkerByTable(TextDocument document, String markername, VirtualTextTable table)
	{
		TextDocumentComponent marker = document.locateTextDocumentComponent(new NameHintMarkerDescriptor(markername));
		if (marker != null)
		{
			document.jumpToLocation(marker.getTextContentLocation());
			document.insertTable(table);
			new OOoOfficeControllerToolKit().removeMarker(document, markername, null);
		}    
		return true;    
	}
}