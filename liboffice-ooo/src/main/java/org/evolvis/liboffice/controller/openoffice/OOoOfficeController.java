/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.controller.openoffice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.configuration.office.openoffice.OOoPipeConnectionMethod;
import org.evolvis.liboffice.configuration.office.openoffice.OOoSocketConnectionMethod;
import org.evolvis.liboffice.connection.openoffice.OOoOfficeConnection;
import org.evolvis.liboffice.controller.KnownOfficeFileFormats;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextFrame;
import org.evolvis.liboffice.document.openoffice.OOoTextGraphic;
import org.evolvis.liboffice.document.openoffice.OOoTextMarker;
import org.evolvis.liboffice.document.openoffice.OOoTextTable;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatDOC;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatHTML;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatPDF;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatSXW;

import com.sun.star.beans.PropertyValue;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.document.MacroExecMode;
import com.sun.star.document.UpdateDocMode;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XController;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XDispatch;
import com.sun.star.frame.XDispatchProvider;
import com.sun.star.frame.XFrame;
import com.sun.star.frame.XModel;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.text.XText;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XInterface;
import com.sun.star.util.XURLTransformer;

import de.tarent.commons.utils.SystemInfo;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoOfficeController extends MainOfficeFactoryFetcherAdapter implements OfficeController
{
	public static final String STYLE_STANDARD = "Standard";
	
	public final static OOoPipeConnectionMethod PIPE_CONN_METHOD = new OOoPipeConnectionMethod();
	public final static OOoSocketConnectionMethod SOCKET_CONN_METHOD = new OOoSocketConnectionMethod();

	private OOoOfficeConnection m_oOfficeConnection;

	private Logger logger = Logger.getLogger(getClass());

	private OOoDeferredDocumentCloser m_oOOoDeferredDocumentCloser = null;

	public OOoOfficeController(OOoOfficeConnection connection)
	{
		m_oOfficeConnection = connection;
	}

	public void cleanUp() 
	{
		if (m_oOOoDeferredDocumentCloser != null)
		{
			m_oOOoDeferredDocumentCloser.waitForCloseAll();
		}
	}

	public TextDocument newDocument()
	{
		return loadDocument(null);
	}


	public TextDocument getStarterDocument() throws NoOfficeException
	{
		return m_oOfficeConnection.getStarterDocument();
	}

	public TextDocument loadDocument(String pFilename)
	{
		return loadDocument(pFilename, true);
	}

	public TextDocument loadDocument(String filename, boolean pVisible)
	{
		XComponentLoader oCLoader;
		XComponent oDoc = null;

		if (filename == null) filename = "private:factory/swriter";        
		else                  filename = ensureFileName(filename);

		String target = "_blank";     

		try 
		{   
			oCLoader = (XComponentLoader)UnoRuntime.queryInterface(XComponentLoader.class, m_oOfficeConnection.getStarterDocument().getXModel().getCurrentController().getFrame());
			PropertyValue[] oArgs = new PropertyValue[3];
			oArgs[0] = new PropertyValue();
			oArgs[0].Name = "UpdateDocMode";
			oArgs[0].Value = new Short(UpdateDocMode.FULL_UPDATE);

			oArgs[1] = new PropertyValue();
			oArgs[1].Name = "MacroExecutionMode";
			oArgs[1].Value = new Short(MacroExecMode.ALWAYS_EXECUTE_NO_WARN);
			oArgs[2] = new PropertyValue();
			oArgs[2].Name = "Hidden";
			oArgs[2].Value = new Boolean(!pVisible);
			oDoc = oCLoader.loadComponentFromURL(filename, target, 0, oArgs);
		}        
		catch(java.lang.Exception e) 
		{
			logger.warn("ERROR: unable to load URL " + filename);
			logger.error("Error in loadDocument", e );
			return(null);
		}

		if (oDoc == null)
		{
			logger.warn("ERROR: unable to load URL " + filename);
			return(null);
		}

		XModel xmodel               = (XModel) UnoRuntime.queryInterface(XModel.class, oDoc);
		XTextDocument xTextDocument = ((XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, oDoc));
		return new OOoTextDocument(m_oOfficeConnection, xmodel, xTextDocument);
	}


	public TextDocument loadDocumentAsCopy(String pFilename)
	{
		return loadDocumentAsCopy(pFilename, true);
	}

	public TextDocument loadDocumentAsCopy(String filename, boolean pVisible)
	{
		XComponentLoader oCLoader;
		XComponent oDoc = null;

		if (filename == null) filename = "private:factory/swriter";        
		else                  filename = ensureFileName(filename);

		String target = "_blank";     
		logger.debug("fileName " + filename);
		try 
		{   
			oCLoader = (XComponentLoader)UnoRuntime.queryInterface(XComponentLoader.class, m_oOfficeConnection.getStarterDocument().getXModel().getCurrentController().getFrame());
			PropertyValue[] oArgs = new PropertyValue[4];
			oArgs[0] = new PropertyValue();
			oArgs[0].Name = "AsTemplate";
			oArgs[0].Value = new Boolean(true);

			oArgs[1] = new PropertyValue();
			oArgs[1].Name = "UpdateDocMode";
			oArgs[1].Value = new Short(UpdateDocMode.FULL_UPDATE);

			oArgs[2] = new PropertyValue();
			oArgs[2].Name = "MacroExecutionMode";
			oArgs[2].Value = new Short(MacroExecMode.ALWAYS_EXECUTE_NO_WARN);
			
			oArgs[3] = new PropertyValue();
			oArgs[3].Name = "Hidden";
			oArgs[3].Value = new Boolean(!pVisible);

			oDoc = oCLoader.loadComponentFromURL(filename, target, 0, oArgs);
		}        
		catch(java.lang.Exception e) 
		{
			logger.warn("ERROR: unable to load URL " + filename);
			logger.error("Error in LoadDocumentAsCopy", e );
			return(null);
		}

		if (oDoc == null)
		{
			logger.warn("ERROR: unable to load URL " + filename);
			return(null);
		}

		XModel xmodel = (XModel) UnoRuntime.queryInterface(XModel.class, oDoc);
		XTextDocument xTextDocument = ((XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, oDoc));
		return new OOoTextDocument(m_oOfficeConnection, xmodel, xTextDocument);
	}

	// ------------------------------------


	private List m_oSupportedFileFormats = null; 

	public List getSupportedFileFormats()
	{
		if (m_oSupportedFileFormats == null)
		{
			// TODO use static KnownOfficeTypes-Objects
			m_oSupportedFileFormats = new ArrayList();
			m_oSupportedFileFormats.add(new OfficeFileFormatSXW());
			m_oSupportedFileFormats.add(new OfficeFileFormatDOC());
			m_oSupportedFileFormats.add(KnownOfficeFileFormats.FILE_TYPE_ODT);

			OfficeFileFormat pdfFormat = new OfficeFileFormatPDF();
			//pdfFormat.setCanSave(true);
			//pdfFormat.setCanLoad(false);      
			m_oSupportedFileFormats.add(pdfFormat);
			m_oSupportedFileFormats.add(new OfficeFileFormatHTML());
		}
		return m_oSupportedFileFormats;
	}

	public boolean isFileFormatSupported(Object key)
	{
		Iterator it = getSupportedFileFormats().iterator();
		while(it.hasNext())
		{
			OfficeFileFormat ff = (OfficeFileFormat)(it.next());
			if (ff.getKey().equals(key)) return true;
		}
		return false;
	}

	public boolean isFileFormatSupported(String pKey)
	{
		return isFileFormatSupported(pKey);
	}

	public boolean isFileFormatSupported(OfficeFileFormat pFileFormat)
	{
		return isFileFormatSupported(pFileFormat.getKey());
	}


	private ControllerFactory m_oControllerFactory = null;
	public ControllerFactory getControllerFactory()
	{
		return m_oControllerFactory;
	}

	public void setControllerFactory(ControllerFactory factory)
	{
		m_oControllerFactory = factory;
	}
	
	public static String ensureFileName(String name)
	{
		if (SystemInfo.isWindowsSystem())
		{
			return "file:///" + name.replaceAll("\\\\", "/");
		}
		else
		{
			if (! name.startsWith("file://")) return("file://"+ name); else return(name);
		}
	}

	public OfficeFileFormat getFileFormat(Object pKey)
	{
		if(pKey instanceof String)
		{
			return getFileFormat((String)pKey);
		}
		return null;
	}

	public OfficeFileFormat getFileFormat(String pKey)
	{
		Iterator it = getSupportedFileFormats().iterator();
		while(it.hasNext())
		{
			OfficeFileFormat format = (OfficeFileFormat)(it.next());
			if (format.getKey().equals(pKey)) return format;
		}    
		return null;
	}


	public int getVersion()
	{
		return 1;
	}



	private XTextDocument getTextDocumentFromComponent(XComponent comp)
	{
		return(XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, comp);    
	}



	public boolean removeTextDocumentComponent(TextDocument document, TextDocumentComponent component)
	{
		if (document == null) { logger.warn("removeTextDocumentComponent(): document is null!"); return false; }

		if (component instanceof OOoTextMarker)
		{
			XTextContent content = ((OOoTextMarker)component).getXTextContent();
			XTextRange range = content.getAnchor();      
			range.setString("");
			return true;
		}
		else if (component instanceof OOoTextTable)
		{
			XController xController = ((OOoTextDocument)document).getXModel().getCurrentController();    
			XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
			XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   
			XText xText = xViewCursor.getText();

			try
			{
				XTextContent xTableTextContent = ((OOoTextTable)component).getXTextContent();
				xText.removeTextContent(xTableTextContent);
				return true;
			}
			catch(NoSuchElementException ne)
			{
				logger.error( ne );
			}
		}
		else if (component instanceof OOoTextGraphic)
		{
			XController xController = ((OOoTextDocument)document).getXModel().getCurrentController();    
			XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
			XTextViewCursor xViewCursor = xViewCursorSupplier.getViewCursor();   
			XText xText = xViewCursor.getText();

			try
			{
				XTextContent xGfxTextContent = ((OOoTextGraphic)component).getXTextContent();
				xText.removeTextContent(xGfxTextContent);
				return true;
			}
			catch(NoSuchElementException ne)
			{
				logger.error( ne );
			}
		}
		else if (component instanceof OOoTextFrame)
		{
			XText xText = ((OOoTextDocument)document).getXTextDocument().getText();

			try
			{
				XTextContent xFrameTextContent = ((OOoTextFrame)component).getXTextContent();
				xText.removeTextContent(xFrameTextContent);
				return true;
			}
			catch(NoSuchElementException ne) 
			{
				logger.error( ne );
			}
		}

		return false;
	}




	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeController#getOpenDocuments()
	 */
	public List getOpenDocuments() throws NoOfficeException
	{
		//define variables
		XInterface oInterface = null;
		XDesktop oDesktop = null;
		XComponent xComp = null;   
		ArrayList documents = new ArrayList();    

		XMultiServiceFactory xMSF =  m_oOfficeConnection.getXMultiServiceFactory();

		if (xMSF == null) return(documents);    

		try 
		{            
			oInterface = (XInterface) xMSF.createInstance("com.sun.star.frame.Desktop");
			oDesktop   = (XDesktop) UnoRuntime.queryInterface(XDesktop.class, oInterface);

			XEnumerationAccess enumacc = oDesktop.getComponents();
			XEnumeration xenum = enumacc.createEnumeration();

			while(xenum.hasMoreElements())
			{
				Object xCompObj = xenum.nextElement();
				xComp   = (XComponent) UnoRuntime.queryInterface(XComponent.class, xCompObj);
				XTextDocument textdoc = getTextDocumentFromComponent(xComp);          
				if (textdoc != null)
				{
					OOoTextDocument foundDoc = new OOoTextDocument(textdoc);
					documents.add(foundDoc);
				} // end if textdoc
			} // end of while
		} // end of try       
		catch(Exception e)
		{      
			e.printStackTrace();      
		} // end of catch

		return(documents);
	}



	protected boolean dispatchUnoCommand(XFrame xFrame, String command, PropertyValue[] oProperties)
	{
		XDispatch xDispatcher = null;
		XURLTransformer xParser;

		try
		{
			xParser = (XURLTransformer)UnoRuntime.queryInterface(XURLTransformer.class, m_oOfficeConnection.getXMultiServiceFactory().createInstance("com.sun.star.util.URLTransformer"));
			com.sun.star.util.URL[] oParseURL = new com.sun.star.util.URL[1];
			oParseURL[0]          = new com.sun.star.util.URL();
			oParseURL[0].Complete = command;
			xParser.parseStrict (oParseURL);

			try
			{
				XDispatchProvider xProvider = (XDispatchProvider)UnoRuntime.queryInterface(XDispatchProvider.class, xFrame);
				xDispatcher = xProvider.queryDispatch(oParseURL[0], "", 0);
				if(xDispatcher != null)
				{
					xDispatcher.dispatch(oParseURL[0], oProperties);
					return true;
				}
			}
			catch(com.sun.star.uno.RuntimeException rte)
			{
				rte.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public TextDocument openDocument(String filename, boolean visible,
			boolean readOnly) {
		// TODO Auto-generated method stub
		return null;
	}

	public TextDocument newDocument(boolean visible) {
		// TODO Auto-generated method stub
		return null;
	}

	public TextDocument getActiveDocument() {
		// TODO Auto-generated method stub
		return null;
	}

}
