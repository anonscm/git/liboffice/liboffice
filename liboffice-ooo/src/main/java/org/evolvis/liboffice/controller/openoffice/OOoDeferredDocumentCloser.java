/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 19.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.controller.openoffice;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.sun.star.util.XCloseable;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class OOoDeferredDocumentCloser
{
	private List m_oDocumentsToClose;
	private Timer m_oTimer = null;
	private TimerTask m_oTimerTask = null;

	public OOoDeferredDocumentCloser()
	{    
		m_oDocumentsToClose = new ArrayList();
	}

	public void waitForCloseAll()
	{
		while(doClose());
	}

	public void addDocumentToClose(XCloseable xCloseable)
	{
		m_oDocumentsToClose.add(xCloseable);
		if (m_oTimer == null) startTimerTask(); 
	}

	private void startTimerTask()
	{
		m_oTimer = new Timer(false); 
		m_oTimerTask = new DocumetCloserTask();
		m_oTimer.scheduleAtFixedRate(m_oTimerTask, 1000L, 1000L);    
	}

	private void stopTimerTask()
	{
		if (m_oTimer != null)
		{
			m_oTimer.cancel();
			m_oTimer = null;
			m_oTimerTask = null;
		}
	}


	private boolean doClose()
	{
		if (m_oDocumentsToClose.size() > 0)
		{
			XCloseable xCloseable = (XCloseable)(m_oDocumentsToClose.remove(0));
			try
			{
				xCloseable.close(false);
			}
			catch(com.sun.star.util.CloseVetoException exCloseVeto)
			{
				m_oDocumentsToClose.add(xCloseable);
			} 
			return true;
		}
		else
		{
			// keine Dokumente zum Schliessen vorhanden...
			stopTimerTask();
			return false;
		}    
	}

	private class DocumetCloserTask extends TimerTask
	{
		public void run()
		{
			doClose();
		}
	}
}
