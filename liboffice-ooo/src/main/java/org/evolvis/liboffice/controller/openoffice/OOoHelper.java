/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.controller.openoffice;

import java.util.Iterator;
import java.util.List;

import org.evolvis.liboffice.document.Selectable;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.openoffice.OOoTextDocument;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;

import com.sun.star.beans.PropertyValue;
import com.sun.star.container.XEnumeration;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.frame.XController;
import com.sun.star.frame.XFrame;
import com.sun.star.frame.XModel;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextFieldsSupplier;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.uno.UnoRuntime;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OOoHelper
{
	public static  XEnumeration getTextFieldEnumeration(TextDocument pDocument)
	{
		if(pDocument == null) return null;
		
		XMultiServiceFactory xMultiServiceFactory = (XMultiServiceFactory)UnoRuntime.queryInterface(XMultiServiceFactory.class, ((OOoTextDocument)pDocument).getXModel());
		if (xMultiServiceFactory == null) return null;
		
		XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier)UnoRuntime.queryInterface(XTextFieldsSupplier.class, ((OOoTextDocument)pDocument).getXModel());
		if (xTextFieldsSupplier == null) return null;
		
		XEnumerationAccess xEnumerationAccess = xTextFieldsSupplier.getTextFields();
		if (xEnumerationAccess == null) return null;
		
		return xEnumerationAccess.createEnumeration();
	}
	
	public static XFrame getXFrame(TextDocument pDocument)
	{
		XController xController = getCurrentXController(pDocument);
		if(xController == null) return null;
		
		return xController.getFrame();
	}
	
	public static XController getCurrentXController(TextDocument pDocument)
	{
		if(pDocument == null) return null;
		
		XModel xModel = ((OOoTextDocument)pDocument).getXModel();
		if(xModel == null) return null;
		
		return xModel.getCurrentController();
	}
	
	public static XTextViewCursor getXTextViewCursor(TextDocument pDocument)
	{
		XController xController = getCurrentXController(pDocument);
		if(xController == null) return null;
		
		XTextViewCursorSupplier xViewCursorSupplier = (XTextViewCursorSupplier)UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xController);
		if(xViewCursorSupplier == null) return null;
		
		return xViewCursorSupplier.getViewCursor();
	}
	
	public static boolean insertObject(XFrame xframe, TextDocument document, Object pObject)
	{
		if (pObject instanceof VirtualTextComponent)
		{
			return new OOoOfficeControllerToolKit().insertVirtualTextComponent(document, (VirtualTextComponent)pObject);
		}
		else if (pObject instanceof List)
		{
			List l = (List)pObject;
			Iterator it = l.iterator();
			while (it.hasNext())
			{
				Object o = it.next();
				if (o instanceof Selectable)
				{
					Selectable s = (Selectable)o;
					s.select();
					s.copy();
					//document.paste();
					((OOoTextDocument)document).pasteSpecial(OOoTextDocument.PASTE_FORMAT_WRITER);
				}
				else throw new RuntimeException("OOoOfficeControllerToolkit.getRangeFromList() listitem is not instance of Selectable");
			}
			return true;
		}
		else if (pObject instanceof Selectable)
		{
			Selectable sel = (Selectable) pObject;
			sel.select();
			sel.copy();
			if (xframe != null)
			{
				//((OOoTextDocument)document).dispatchUnoCommand(xframe, ".uno:Delete", new PropertyValue[0]);
			}
			document.paste();
		}
		else if (pObject instanceof String)
		{
			return new OOoOfficeControllerToolKit().insertVirtualTextComponent(document, new VirtualParagraph((String)pObject));
		}
		else if(pObject == null)
		{
			return true;
		}
		return false;
	}
	
	public static boolean replaceMarkerByObject(TextDocument pDocument, XTextContent pTextField, Object pReplaceObject, boolean pRemoveLine)
	{
		if(pDocument != null && pTextField != null && pReplaceObject != null)
		{
			OOoHelper.getXTextViewCursor(pDocument).gotoRange(pTextField.getAnchor().getStart(), false);
			
			boolean returnValue = OOoHelper.insertObject(OOoHelper.getXFrame(pDocument), pDocument, pReplaceObject);
			((OOoTextDocument)pDocument).dispatchUnoCommand(OOoHelper.getXFrame(pDocument), ".uno:Delete", new PropertyValue[0]);
			
			if (pRemoveLine)
				((OOoTextDocument)pDocument).dispatchUnoCommand(OOoHelper.getXFrame(pDocument), ".uno:Delete", new PropertyValue[0]);
			
			if(!returnValue) return false;
		}
		return true;
	}
}