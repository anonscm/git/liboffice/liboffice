/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 03.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.msoffice;

import java.util.logging.Logger;

import org.evolvis.liboffice.document.TextParagraph;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextParagraph;
import org.evolvis.liboffice.msword11.Characters;
import org.evolvis.liboffice.msword11.Font;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Style;
import org.evolvis.liboffice.msword11.WdStyleType;
import org.evolvis.liboffice.parser.TextParagraphParser;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraphBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextFont;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyleType;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextParagraphParser implements TextParagraphParser
{
  private final boolean m_bconvertCRtoParagraphBreak = true;
  private final static Logger logger = Logger.getLogger(MsOfficeTextParagraphParser.class.getName());
  
  public VirtualParagraph parseTextParagraph(TextParagraph paragraph)
  {
    MsOfficeTextParagraph textparagraph = (MsOfficeTextParagraph)paragraph;
    VirtualParagraph vp = new VirtualParagraph();
    
    Range range = null;
    if (textparagraph.getParagraph() != null)
    {
      range = textparagraph.getParagraph().getRange();      
    }
    else if (textparagraph.getRange() != null)
    {
      range = textparagraph.getRange();      
    }
    
    Style style = getStyle(range);
    if (style != null)
    {
      String styleName = style.getNameLocal();
      if (styleName != null)
      {
        int type = style.getType();
        VirtualTextStyleType styleType = null;
        if      (type == WdStyleType.wdStyleTypeParagraph) styleType = VirtualTextStyleType.STYLE_PARAGRAPH;
        else if (type == WdStyleType.wdStyleTypeCharacter) styleType = VirtualTextStyleType.STYLE_CHARACTER;
      	vp.setStyle(new VirtualTextStyle(styleName, styleType));
      }
    }
    
    //Range range = textparagraph.getRange();
    //if ((range.get_Bold() != 0) || (range.get_Italic() != 0) || (range.get_Underline() != 0))
    if (true)
    {
        
      
      Characters characters = range.getCharacters();
      if (characters.getCount() > 0)
      {      
        VirtualTextPortionText vtp = new VirtualTextPortionText();
        for(int c=1; c<=(characters.getCount()); c++)
        {
          Range characterrange = characters.item(c);
          logger.fine("characters[" + c + "] = " + characterrange.getText() + " (bold=" + characterrange.getBold() + " italic=" + characterrange.getItalic() + " underline=" + characterrange.getUnderline() + ")");        
          
          String text = characterrange.getText();
          text = text.replaceAll("\\x07", "");
          boolean isBold = (characterrange.getBold() != 0);
          boolean isItalic = (characterrange.getItalic() != 0);
          boolean isUnderline = (characterrange.getUnderline() != 0);
          VirtualTextFont vF = null;
          String fontName = "";
          double fontSize = Double.NaN;
          Font font = characterrange.getFont();
          
          if (font != null)
          {
          	vF = (new VirtualTextFont(font.getName(), font.getSize()));
          	fontName = font.getName();
          	fontSize = font.getSize();
          }
          
          String newFontName = "";
          double newFontSize = Double.NaN;
          if (vtp.getFont() != null)
          {
          	newFontName = vtp.getFont().getFontName();
          	newFontSize = vtp.getFont().getFontHeight();
          }

          //if ((vtp.isBold() != isBold) || (vtp.isItalic() != isItalic) || (vtp.isUnderline() != isUnderline))
          if ((vtp.isBold() != isBold) || (vtp.isItalic() != isItalic) || (vtp.isUnderline() != isUnderline) || (!(fontName.equals(newFontName))) || (fontSize != newFontSize))
          {
            if (vtp.getText().length() > 0) vp.addTextPortion(vtp);
            vtp = new VirtualTextPortionText("", (String)null, isBold, isItalic, isUnderline);
            vtp.setFont(vF);
          }          
          vtp.setText(vtp.getText() + text);
        }
        if (vtp.getText().length() > 0) 
        {
          if ((vtp.getText().endsWith("\r")) && (m_bconvertCRtoParagraphBreak))
          {
            vtp.setText(vtp.getText().substring(0, vtp.getText().length() -1));
            vp.addTextPortion(vtp);
            vp.addTextPortion(new VirtualParagraphBreak());
          }
          else
          {
            vp.addTextPortion(vtp);
          }
        }
      }
    }
    else
    {
      String paragraphText = range.getText();
      paragraphText = paragraphText.replaceAll("\\x07", "");

      VirtualTextFont vFont = null;
      Font font = range.getFont();
      if (font != null)
      {
        vFont = new VirtualTextFont(font.getName(), font.getSize());
      }
      
      if ((paragraphText.endsWith("\r")) && (m_bconvertCRtoParagraphBreak))
      {
        paragraphText = paragraphText.substring(0, paragraphText.length() -1);        
        
        VirtualTextPortionText vtpt = new VirtualTextPortionText(paragraphText);
        if (vFont != null) vtpt.setFont(vFont);
        vp.addTextPortion(vtpt);
        
        vp.addTextPortion(new VirtualParagraphBreak());
      }
      else
      {
        VirtualTextPortionText vtpt = new VirtualTextPortionText(paragraphText);
        if (vFont != null) vtpt.setFont(vFont);
        vp.addTextPortion(vtpt);
      }
    }
    return vp;
  }
  
  
  private static Style getStyle(Range range)
  {
	  /*
  	int val = ((Integer) range.getStyle()).intValue();
  	if (val != 0) return new Style(val);
  	else return null;
  	*/
	  return null;
  }
  
  
}
