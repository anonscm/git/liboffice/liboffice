/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 24.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.msoffice;

//import java.util.List;

import java.util.logging.Logger;

import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextParagraph;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextTable;
import org.evolvis.liboffice.msword11.Paragraph;
import org.evolvis.liboffice.msword11.Paragraphs;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.msword11.Tables;
import org.evolvis.liboffice.parser.TextDocumentParser;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextDocumentParser implements TextDocumentParser
{
	  private MsOfficeTextParagraphParser m_oMsTextParagraphParser = null;
	  private MsOfficeTextTableParser m_oMsTextTableParser = null;
      private Logger logger = Logger.getLogger(MsOfficeTextDocumentParser.class.getName());
	  
	  public MsOfficeTextParagraphParser getMsTextParagraphParser()
	  {
	  	if (m_oMsTextParagraphParser == null)
	  	{
	  		m_oMsTextParagraphParser  = new MsOfficeTextParagraphParser();
	  	}
	    return m_oMsTextParagraphParser;  	
	  }
		
	  
	  public MsOfficeTextTableParser getMsTextTableParser()
	  {
	  	if (m_oMsTextTableParser == null)
	  	{
	  		m_oMsTextTableParser  = new MsOfficeTextTableParser(this);
	  	}
	    return m_oMsTextTableParser;  	
	  }
		
  
  
	
  public VirtualTextDocument parseTextDocument(TextDocument document)
  {
    logger.fine("parseTextDocument(" + document + ")");
    MsOfficeTextDocument mstd = (MsOfficeTextDocument)document;
    logger.fine("mstd = " + mstd);

    // die aktuelle Cursor-Position ermitteln...
    Range cursorRange = mstd.getConnection().getApplication().getSelection().getRange();
    VirtualTextDocument vd = new VirtualTextDocument();
    Table tableBuffer = null;
    Paragraphs paragraphs = mstd.getDocument().getParagraphs();
    for(int pi=1; pi<=(paragraphs.getCount()); pi++)
    {
      Paragraph paragraph = paragraphs.item(pi);
      Range paragraphRange = paragraph.getRange();
      
      if (paragraphRange.getTables().getCount() > 0)
      {
      	Tables rangeTables = paragraphRange.getTables();
      	for(int tableIndex=1; tableIndex<=rangeTables.getCount(); tableIndex++)
      	{
      		if (tableBuffer != null)
      		{
      			if(!(tableBuffer.getRange().isEqual(rangeTables.item(tableIndex).getRange())))
      			{
      				// ist Table!
    	        VirtualTextTable vt = getMsTextTableParser().parseTextTable(document, new MsOfficeTextTable(mstd, rangeTables.item(tableIndex)));
    	        vd.addTextComponent(vt);
      				tableBuffer = rangeTables.item(tableIndex);
      			}
      		}
      		else
      		{
      				// ist Table!
  	        VirtualTextTable vt = getMsTextTableParser().parseTextTable(document, new MsOfficeTextTable(mstd, rangeTables.item(tableIndex)));
  	        vd.addTextComponent(vt);
     				tableBuffer = rangeTables.item(tableIndex);
      		}
      	}
      }
      else
      {
      	// text!
        VirtualParagraph vp = getMsTextParagraphParser().parseTextParagraph(new MsOfficeTextParagraph(mstd, paragraph));
        
        Range currentRange = paragraph.getRange();
        if ((cursorRange.getStart() > currentRange.getStart()) && (cursorRange.getEnd() < currentRange.getEnd()))
        {
          vd.setComponentContainingCursor(vp);
        }            
        vd.addTextComponent(vp);          	
      }
      
    }

    return vd;
  }

  
  
  
  
  
  public VirtualStyledText parseTextDocument(TextDocument doc, Paragraphs paragraphs)
  {
    //MsTextDocument mstd = (MsTextDocument)document;
    VirtualStyledText vd = new VirtualStyledText();
    Table tableBuffer = null;
    
    for(int pi=1; pi<=(paragraphs.getCount()); pi++)
    {
      Paragraph paragraph = paragraphs.item(pi);
      Range paragraphRange = paragraph.getRange();
      
      if (paragraphRange.getTables().getCount() > 0)
      {
        Tables rangeTables = paragraphRange.getTables();
        for(int tableIndex=1; tableIndex<=rangeTables.getCount(); tableIndex++)
        {
          if (tableBuffer != null)
          {
            if(tableBuffer.getRange().isEqual(rangeTables.item(tableIndex).getRange()))
            {
              // ist Table!
              //VirtualTextTable vt = getMsTextTableParser().parseTextTable(document, new MsTextTable(mstd, rangeTables.Item(tableIndex)));
              //vd.addParagraph(vt);
              tableBuffer = rangeTables.item(tableIndex);
            }
          }
          else
          {
              // ist Table!
            //VirtualTextTable vt = getMsTextTableParser().parseTextTable(document, new MsTextTable(mstd, rangeTables.Item(tableIndex)));
            //vd.addTextComponent(vt);
            tableBuffer = rangeTables.item(tableIndex);
          }
        }
      }
      else
      {
        // text!
        VirtualParagraph vp = getMsTextParagraphParser().parseTextParagraph(new MsOfficeTextParagraph((MsOfficeTextDocument)doc, paragraph));
        vd.addParagraph(vp);            
      }      
    }

    return vd;
  }

  
  
  
  
  /*
    Set oDoc = ActiveDocument
    Set oParas = oDoc.Paragraphs
    For Each oPara In oParas
        Set oRange = oPara.Range
            If oRange.Tables.Count > 0 Then
                Set oTables = oRange.Tables
                For i = 1 To oTables.Count
                    If Not oTableBuf Is Nothing Then
                        If Not oTableBuf.Range.IsEqual(oTables(i).Range) Then
                            mTables.Add oTables(i).Range
                            Set oTableBuf = oTables(i)
                        End If
                        Else
                        mTables.Add oTables(i).Range
                        Set oTableBuf = oTables(i)
                    End If
                Next
            Else
                mParas.Add oRange
            End If
    Next

   */
  
  
  
  
  /*
  private int getIndexOfTable(Range range, List tableList)
  {
      logger.debug("getIndexOfTable() range=" + range.get_Text());  	
  	for(int i=0; i<(tableList.size()); i++)
  	{
      Table currTable = ((Table)(tableList.get(i)));            
      if (currTable.get_Range().IsEqual(range)) 
      {
          logger.debug("getIndexOfTable() TESTING Table range Found=" + i);  	
      	return i;
      }
      
	    Rows rows = currTable.get_Rows();
	    for(int rowIndex=1; rowIndex<=(rows.get_Count()); rowIndex++)
	    {
  	  	Row row = rows.Item(rowIndex);
	    	Cells cells = row.get_Cells();	  	
		    for(int colIndex=1; colIndex<=(cells.get_Count()); colIndex++)
		    {
		      Cell cell = cells.Item(colIndex);
		      Range cellRange = cell.get_Range();
		      
              logger.debug("getIndexOfTable() TESTING CELL index=" + i);  	
		      
		      if (cellRange.IsEqual(range))
		      {
                  logger.debug("getIndexOfTable() FOUND CELL  index=" + i);  	
		      	return i;
		      }
		    }  		  	
	    }
  	}  	
  	return -1;
  }
  */
  
  
  /*
  private String getStoryTypeName(int storyType)
  {
    switch(storyType)
    {
      case(WdStoryType.wdCommentsStory): return("CommentsStory");
      case(WdStoryType.wdEndnotesStory): return("EndnotesStory");
      case(WdStoryType.wdEvenPagesFooterStory): return("EvenPagesFooterStory");
      case(WdStoryType.wdEvenPagesHeaderStory): return("EvenPagesHeaderStory");
      case(WdStoryType.wdFirstPageFooterStory): return("FirstPageFooterStory");
      case(WdStoryType.wdFirstPageHeaderStory): return("FirstPageHeaderStory");
      case(WdStoryType.wdFootnotesStory): return("FootnotesStory");
      case(WdStoryType.wdMainTextStory): return("MainTextStory");
      case(WdStoryType.wdPrimaryFooterStory): return("PrimaryFooterStory");
      case(WdStoryType.wdPrimaryHeaderStory): return("PrimaryHeaderStory");
      case(WdStoryType.wdTextFrameStory): return("TextFrameStory");
    }
    return "*unknown*";
  }
  */
  
  
}
