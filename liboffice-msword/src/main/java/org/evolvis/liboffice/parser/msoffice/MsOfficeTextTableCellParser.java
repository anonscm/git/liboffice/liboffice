/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 06.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.msoffice;

import java.util.logging.Logger;

import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextParagraph;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextTable;
import org.evolvis.liboffice.msword11.Cell;
import org.evolvis.liboffice.msword11.Cells;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Row;
import org.evolvis.liboffice.msword11.Rows;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.parser.TextTableCellParser;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

/**
 * @author Administrator
 *
 */
public class MsOfficeTextTableCellParser implements TextTableCellParser 
{
	private MsOfficeTextTableParser m_oMsTextTableParser = null;
	private final static Logger logger = Logger.getLogger(MsOfficeTextTableCellParser.class.getName());
    
	public MsOfficeTextTableCellParser(MsOfficeTextTableParser oMsTextTableParser)
	{
		m_oMsTextTableParser = oMsTextTableParser;
	}
	
  public VirtualTextTableCell parseTextTableCell(TextDocument document, TextTable table, int row, int column)
  {  	
      logger.fine("parseTextTableCell(doc, table, " + row + ", " + column + ")");  	
	  MsOfficeTextDocument mstd = (MsOfficeTextDocument)document;
	  MsOfficeTextTable mstt = (MsOfficeTextTable)table;	  
	  Table wordTable = mstt.getTable();
  	
	  Rows tableRows = wordTable.getRows();
  	  Row tableRow = tableRows.item(row + 1);
	  Cells cells = tableRow.getCells();	  	
	  Cell cell = cells.item(column + 1);
	  Range cellRange = cell.getRange();
	  VirtualParagraph virtualParagraph = m_oMsTextTableParser.getOfficeTextDocumentParser().getMsTextParagraphParser().parseTextParagraph(new MsOfficeTextParagraph(mstd, cellRange));
	  VirtualStyledText virtualText = new VirtualStyledText();
	  virtualText.addParagraph(virtualParagraph);
	  return new VirtualTextTableCell(virtualText);
  }
}
