/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.msoffice;

import java.util.logging.Logger;

import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextFrame;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextFrame;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Shape;
import org.evolvis.liboffice.parser.TextFrameParser;
import org.evolvis.liboffice.virtualdocument.VirtualTextFrame;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 */
public class MsOfficeTextFrameParser implements TextFrameParser
{
  private MsOfficeTextDocumentParser m_oMsOfficeTextDocumentParser = null;
  private Logger logger = Logger.getLogger(MsOfficeTextFrameParser.class.getName());
  
  public MsOfficeTextFrameParser(MsOfficeTextDocumentParser docParser)
  {
    m_oMsOfficeTextDocumentParser = docParser;
  }

	/** TODO: VirtualStyledText wird noch nicht geparsed
	 * @see org.evolvis.liboffice.parser.TextFrameParser#parseTextFrame(org.evolvis.liboffice.document.TextDocument, org.evolvis.liboffice.document.TextFrame)
	 */
	public VirtualTextFrame parseTextFrame(TextDocument document, TextFrame frame) 
	{
		logger.fine("MsTextFrameParser parseTextFrame");
		Shape msFrame 							 = ((MsOfficeTextFrame)frame).getFrame();
		VirtualTextFrame vFrame 		         = new VirtualTextFrame( );
		Range frameRange                         = msFrame.getTextFrame().getTextRange();
		VirtualStyledText styledText             = m_oMsOfficeTextDocumentParser.parseTextDocument(document, frameRange.getParagraphs());
		vFrame.setText(  styledText );
		vFrame.setHeight( (int)msFrame.getHeight() );
		vFrame.setWidth( (int)msFrame.getWidth() );
		vFrame.setPositionLeft( (int)msFrame.getLeft() );
		vFrame.setPositionTop( (int)msFrame.getTop() );
		return vFrame;
	}
}
