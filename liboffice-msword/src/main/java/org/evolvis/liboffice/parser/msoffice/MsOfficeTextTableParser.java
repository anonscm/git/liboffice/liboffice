/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 05.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.parser.msoffice;

import org.evolvis.liboffice.controller.Util;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextParagraph;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextTable;
import org.evolvis.liboffice.msword11.Cell;
import org.evolvis.liboffice.msword11.Cells;
import org.evolvis.liboffice.msword11.Column;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Row;
import org.evolvis.liboffice.msword11.Rows;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.msword11.WdBorderType;
import org.evolvis.liboffice.parser.TextTableCellParser;
import org.evolvis.liboffice.parser.TextTableParser;
import org.evolvis.liboffice.virtualdocument.VirtualBorder;
import org.evolvis.liboffice.virtualdocument.VirtualMargin;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

/**
 * @author Administrator
 *
 */
public class MsOfficeTextTableParser implements TextTableParser 
{
	private MsOfficeTextDocumentParser m_oMsTextDocumentParser = null;
	private MsOfficeTextTableCellParser m_oMsOfficeTextTableCellParser = null;
	
	public MsOfficeTextTableParser(MsOfficeTextDocumentParser oMsTextDocumentParser)
	{
      m_oMsTextDocumentParser = oMsTextDocumentParser;		
	}
	
	public MsOfficeTextDocumentParser getOfficeTextDocumentParser()
	{
		return m_oMsTextDocumentParser;
	}
	
	public TextTableCellParser getTextTableCellParser()
	{
		if (m_oMsOfficeTextTableCellParser == null)
		{
			m_oMsOfficeTextTableCellParser = new MsOfficeTextTableCellParser(this);
		}
		return m_oMsOfficeTextTableCellParser;
	}
	


	public VirtualTextTable parseTextTable(TextDocument document, TextTable table) 
	{
	  MsOfficeTextTable mstt = (MsOfficeTextTable)table;	  
	  Table wordTable  = mstt.getTable();
	  
	  VirtualTextTable virtualTable = new VirtualTextTable();
	  virtualTable.setShowLines( true );
	  virtualTable.setSize(wordTable.getRows().getCount(), wordTable.getColumns().getCount());
	  
	  Rows rows = wordTable.getRows();
	  for(int rowIndex=1; rowIndex<=(rows.getCount()); rowIndex++)
	  {
	  	Row row = rows.item(rowIndex);
	  	Cells cells = row.getCells();	  	
			for(int colIndex=1; colIndex<=(cells.getCount()); colIndex++)
			{
			  Cell cell = cells.item(colIndex);
			  Range cellRange = cell.getRange();
			  VirtualParagraph virtualParagraph = m_oMsTextDocumentParser.getMsTextParagraphParser().parseTextParagraph(new MsOfficeTextParagraph((MsOfficeTextDocument)document, cellRange));
			  VirtualStyledText virtualText = new VirtualStyledText();
			  virtualText.addParagraph(virtualParagraph);
			  VirtualTextTableCell virtualCell = new VirtualTextTableCell(virtualText);
			  
			  // Border setzen
			  virtualCell.setBorderBottom(new VirtualBorder( cell.getBorders().item(WdBorderType.wdBorderBottom).getVisible()  ));
			  virtualCell.setBorderTop(new VirtualBorder( cell.getBorders().item(WdBorderType.wdBorderTop).getVisible()  ));
			  virtualCell.setBorderLeft(new VirtualBorder( cell.getBorders().item(WdBorderType.wdBorderLeft).getVisible()  ));
			  virtualCell.setBorderRight(new VirtualBorder( cell.getBorders().item(WdBorderType.wdBorderRight).getVisible()  ));
			  
			  // das Letzte Return aus der Zelle entfernen
			  virtualCell.getStyledText().cleanParagraphBreaks();
			  
			  
			  
			  // Margin setzen
			  //cell.get_Borders().set_DistanceFromBottom(5);
			  
			  virtualTable.setTextTableCell(rowIndex-1, colIndex-1, virtualCell);
			  virtualTable.setMarginLeft(new VirtualMargin( (int)Util.convertPicaToNanoMeters( mstt.getTable().getRows().getLeftIndent() )  )); 
			}  		  	
  	}	  
    
	  int tableWidth = 0;
      for (int column = 1; column <= wordTable.getColumns().getCount(); column++) 
	  {
	  	Column col       = wordTable.getColumns().item(column);
	  	float colWidth   = col.getWidth();
	  	int realColWidth = (int)Util.convertPicaToNanoMeters( colWidth );
        tableWidth      += realColWidth;
        virtualTable.setColumnWidth(column-1, new Integer( realColWidth ));
	  }
      virtualTable.setWidth( tableWidth );
      
      
	  return virtualTable;
	}

	
	
	
	
}
