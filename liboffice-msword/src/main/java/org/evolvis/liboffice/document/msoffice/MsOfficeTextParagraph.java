/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.document.TextParagraph;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocation;
import org.evolvis.liboffice.msword11.Paragraph;
import org.evolvis.liboffice.msword11.Range;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn, Steffen Kriese, tarent GmbH Bonn
 *
 */
public class MsOfficeTextParagraph extends MsOfficeTextComponent implements TextParagraph
{
  private Paragraph m_oParagraph = null;
  private MsOfficeTextDocument doc; 
  
  /**
   * @param range
   */
  public MsOfficeTextParagraph(MsOfficeTextDocument doc,  Range range)
  {
    super(range);
    this.doc = doc;
  }

  public MsOfficeTextParagraph(MsOfficeTextDocument doc,  MsOfficeTextComponent comp)
  {
    super(comp);
    this.doc = doc;
  }
  
  public MsOfficeTextParagraph(MsOfficeTextDocument doc,  Paragraph paragraph)
  {
    super(paragraph.getRange());
    m_oParagraph = paragraph;
    this.doc = doc;
  }
  
  public Paragraph getParagraph()
  {
    return m_oParagraph;
  }

  public String getText()
  {
    //TODO: testen ob das so geht:
    return m_oParagraph.getRange().getText();
  }
  
  public TextContentLocation getTextContentLocation()
  {
    return new MsOfficeTextContentLocation( this, false);
  }
  
  public TextContentLocation getTextContentLocationAfter()
  {
    return new MsOfficeTextContentLocation( this, true);
  }

  public boolean remove()
  {
     m_oParagraph.getRange().delete();
     return true;
  }

    public void select(boolean append)
    {
        System.err.println("MsTextParagraph.select() is not tested yet");
        // TODO append implementieren
        m_oParagraph.getRange().select();
    }

    public void copy()
    {
        doc.copy();
    }
    
    public void select()
    {
        select( false );
    }
}
