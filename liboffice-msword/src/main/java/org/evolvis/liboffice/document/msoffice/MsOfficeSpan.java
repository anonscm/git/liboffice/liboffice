/**
 * 
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.document.Span;
import org.evolvis.liboffice.msword11.Range;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class MsOfficeSpan implements Span {
	
	Range range;
	
	public MsOfficeSpan(Range range) {
		
		this.range = range;
	}
	
	public MsOfficeSpan() {
		
		this(new Range());
	}

	/**
	 * @see org.evolvis.liboffice.document.Span#getEnd()
	 */
	public int getEnd() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.evolvis.liboffice.document.Span#getStart()
	 */
	public int getStart() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.evolvis.liboffice.document.Span#setEnd(int)
	 */
	public void setEnd(int start) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.evolvis.liboffice.document.Span#setStart(int)
	 */
	public void setStart(int start) {
		// TODO Auto-generated method stub

	}

	public Range getRange() {
		
		return range;
	}
}
