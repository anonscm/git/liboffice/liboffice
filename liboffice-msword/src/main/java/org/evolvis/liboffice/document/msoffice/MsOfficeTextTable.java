/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import java.util.logging.Logger;

import org.evolvis.liboffice.controller.Util;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.TextTableCell;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocation;
import org.evolvis.liboffice.msword11.Cell;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.msword11.WdBorderType;
import org.evolvis.liboffice.msword11.WdParagraphAlignment;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

import com.jacob.com.Variant;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextTable extends TextTable
{
  private Table m_oTable = null;
  private MsOfficeTextDocument m_oMsTextDocument = null;
  private Range range;
  private final static Logger logger = Logger.getLogger(MsOfficeTextTable.class.getName());
  public MsOfficeTextTable(MsOfficeTextDocument document, Table wordTable)
  {
    range = wordTable.getRange();
    m_oTable = wordTable;
    m_oMsTextDocument = document;
  }
  
  public MsOfficeTextTable(MsOfficeTextDocument document, MsOfficeTextTable table)
  {
    range =  table.getRange();
    m_oTable = table.getTable();
    m_oMsTextDocument = document;
  }
  
  public Table getTable()
  {
    return m_oTable;
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.document.TextTable#addRows(int, int)
   */
  public void addRows(int index, int count)
  {
    int numRows = m_oTable.getRows().getCount();
    
    if (index >= numRows)
    {
      for(int i=0; i<count; i++)
      {
        m_oTable.getRows().add();      
      }
    }
    else
    {
    	for(int i=0; i < count; i++)
    	{
    		m_oTable.getRows().add(new Variant(index));
    	}
    	/*
      Row row = m_oTable.getRows().item(index + 1);      
      for(int i=0; i<count; i++)
      {
        row = m_oTable.getRows().add(row);
      }
      */
    }
  }


  public void removeRows(int index, int count)
  {
    for(int i=0; i<count; i++)
    {
      m_oTable.getRows().item(index + 1).delete();
    }
  }

  public int getNumberOfRows()
  {
    return m_oTable.getRows().getCount();
  }
  
	@Override
	public void addColumns(int index, int count) {
		int numColumns = getNumberOfColumns();
		if(index>= numColumns){
		      for(int i=0; i<count; i++)
		      {
		        m_oTable.getColumns().add();      
		      }
		} else  {
	    	for(int i=0; i < count; i++)
	    	{
	    		m_oTable.getColumns().add(new Variant(index));
	    	}
		}
	}
  
  public int getNumberOfColumns()
  {
    return m_oTable.getColumns().getCount();
  }
  
  public void insertCell(int row, int column, VirtualTextTableCell cell)
  {
    Cell wordCell = m_oTable.cell(row + 1, column + 1);        
    wordCell.getRange().select();
    setHorizontalAlignment( cell );
    VirtualStyledText vst = cell.getStyledText();
    m_oMsTextDocument.insertText(vst);
  }

  
  public void setHorizontalAlignment( VirtualTextTableCell cell)
  {
      if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_CENTER == cell.getHorizontalJustification())
      {
          m_oMsTextDocument.getDocument().getApplication().getSelection().getParagraphFormat().setAlignment( WdParagraphAlignment.wdAlignParagraphCenter );
      }
      else if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_LEFT == cell.getHorizontalJustification())
      {
          m_oMsTextDocument.getDocument().getApplication().getSelection().getParagraphFormat().setAlignment( WdParagraphAlignment.wdAlignParagraphLeft );
      }
      else if (VirtualTextTableCell.HORIZONTAL_JUSTIFY_RIGHT == cell.getHorizontalJustification())
      {
          m_oMsTextDocument.getDocument().getApplication().getSelection().getParagraphFormat().setAlignment( WdParagraphAlignment.wdAlignParagraphRight );
      }
//      else logger.warn("unknown horizontal justification: " + cell.getHorizontalJustification());
  }

  public void clearCell(int row, int column)
  {
    Cell wordCell = m_oTable.cell(row + 1, column + 1);        
    Range range = wordCell.getRange();
    range.select();
    range.setText("");    
  }


  public TextTableCell getCell(int row, int column)
  {
    Cell wordCell = m_oTable.cell(row + 1, column + 1);        
    Range range = wordCell.getRange();
    return new MsOfficeTextTableCell(range);
  }

  public boolean setCellBorderLineVisible(int row, int col, Object line, boolean visible)
  {
      
      if (m_oTable != null)
      {    
        int border;
        if (TextTableCell.BORDER_LEFT.equals(line))
        {
          border = WdBorderType.wdBorderLeft;
        }    
        else if (TextTableCell.BORDER_RIGHT.equals(line))
        {
          border = WdBorderType.wdBorderRight;
        }
        else if (TextTableCell.BORDER_TOP.equals(line))
        {
          border = WdBorderType.wdBorderTop;
        }
        else if (TextTableCell.BORDER_BOTTOM.equals(line))
        {
          border = WdBorderType.wdBorderBottom;
        }
        else return false;
    
        m_oTable.getRows().item(row + 1).getCells().item(col + 1).getBorders().item(border).setVisible(visible);
        return true;
      }
      return false;

  }

    public boolean setTableLeftMargin(float margin)
    {
        if (m_oTable != null)
        {
          m_oTable.getRows().setLeftIndent( (float) Util.convertNanoMetersToPica(margin) );
          return true;
        }
        return false;
    }

    public boolean setTableWidth(float width)
    {
        if (m_oTable != null)
        {
          int numColumns = m_oTable.getColumns().getCount();
            
          m_oTable.getColumns().setWidth((float)((width / numColumns) / 35.1));
          return true;
        }
        return false;
    }
    
    public void setTableHeight(float height){    
        m_oTable.getRows().setHeight(height);
    }
    
    public TextContentLocation getTextContentLocation()
    {
      return new MsOfficeTextContentLocation( this, false);
    }
    
    public TextContentLocation getTextContentLocationAfter()
    {
      return new MsOfficeTextContentLocation( this, true);
    }

    public boolean remove()
    {
        m_oTable.delete();
        return true;
    }

    public void select(boolean append)
    {
         // TODO append implementieren
        m_oTable.select();
    }
    
    public Range getRange()
    {
      return range;
    }

    public void copy()
    {
        m_oMsTextDocument.copy();
    }

    public void select()
    {
        select( false );
    }

	@Override
	public void removeColumns(int index, int count) {
	    for(int i=0; i<count; i++)
	    {
	      m_oTable.getColumns().item(index + 1).delete();
	    }
		
	}


}

