/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;


import org.evolvis.liboffice.msword11.Range;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextComponent
{
  private Range m_oRange;
  private MsOfficeTextDocument m_oDocument;

  public MsOfficeTextComponent(Range range)
  {
    m_oRange = range;
  }
  
  public MsOfficeTextComponent(MsOfficeTextComponent component)
  {
    m_oRange = component.getRange();
  }
  
  
  public Range getRange()
  {
    return m_oRange;
  }
  
  
  public MsOfficeTextDocument getMsOfficeTextDocument(){
	  return m_oDocument;
  }

}
