/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import org.evolvis.liboffice.connection.msoffice.MsOfficeConnection;
import org.evolvis.liboffice.controller.KnownOfficeFileFormats;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.controller.OfficeWindowListener;
import org.evolvis.liboffice.controller.Util;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.document.AbstractTextDocument;
import org.evolvis.liboffice.document.Span;
import org.evolvis.liboffice.document.TextBookmark;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextFrame;
import org.evolvis.liboffice.document.TextGraphic;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.document.TextStyle;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.TextVariable;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.TextContentLocator;
import org.evolvis.liboffice.locator.TextContentRange;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocation;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocator;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentRange;
import org.evolvis.liboffice.msword11.Application;
import org.evolvis.liboffice.msword11.Bookmark;
import org.evolvis.liboffice.msword11.Bookmarks;
import org.evolvis.liboffice.msword11.Cell;
import org.evolvis.liboffice.msword11.Column;
import org.evolvis.liboffice.msword11.Document;
import org.evolvis.liboffice.msword11.Field;
import org.evolvis.liboffice.msword11.Font;
import org.evolvis.liboffice.msword11.FontNames;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Selection;
import org.evolvis.liboffice.msword11.Style;
import org.evolvis.liboffice.msword11.Styles;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.msword11.Variable;
import org.evolvis.liboffice.msword11.WdBorderType;
import org.evolvis.liboffice.msword11.WdBreakType;
import org.evolvis.liboffice.msword11.WdCollapseDirection;
import org.evolvis.liboffice.msword11.WdFieldType;
import org.evolvis.liboffice.msword11.WdGoToDirection;
import org.evolvis.liboffice.msword11.WdGoToItem;
import org.evolvis.liboffice.msword11.WdInformation;
import org.evolvis.liboffice.msword11.WdLineStyle;
import org.evolvis.liboffice.msword11.WdPrintOutItem;
import org.evolvis.liboffice.msword11.WdPrintOutPages;
import org.evolvis.liboffice.msword11.WdPrintOutRange;
import org.evolvis.liboffice.msword11.WdSaveFormat;
import org.evolvis.liboffice.msword11.WdStyleType;
import org.evolvis.liboffice.msword11.WdUnderline;
import org.evolvis.liboffice.msword11.WdUnits;
import org.evolvis.liboffice.msword11.WdWindowState;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.officefileformats.UnsupportedOfficeFileTypeException;
import org.evolvis.liboffice.ui.Messages;
import org.evolvis.liboffice.virtualdocument.VirtualDocumentInformation;
import org.evolvis.liboffice.virtualdocument.VirtualDocumentInformationStorage;
import org.evolvis.liboffice.virtualdocument.VirtualMargin;
import org.evolvis.liboffice.virtualdocument.VirtualTextFrame;
import org.evolvis.liboffice.virtualdocument.graphic.VirtualGraphic;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraphBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortion;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyleType;
import org.xvolks.jnative.JNative;
import org.xvolks.jnative.Type;

import com.jacob.com.ComException;
import com.jacob.com.Variant;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MsOfficeTextDocument extends AbstractTextDocument
{
	private Document m_oDocument;
	private MsOfficeConnection m_oMsOfficeConnection;
	//private int m_iOldWindowState = -1;
	private final static Logger logger = Logger.getLogger(MsOfficeTextDocument.class.getName());
	private MsOfficeTextContentLocator locator;

	private Map textStyles = null;

	public MsOfficeTextDocument(MsOfficeConnection oMsOfficeConnection, Document document)
	{
		m_oMsOfficeConnection = oMsOfficeConnection;
		m_oDocument = document;
		/*
    m_oDocument.addDocumentEventsListener(new DocumentEvents()
    {
        public void Open(DocumentEvents_OpenEvent arg0)
        {
            logger.info("open document");
        }

        public void Close(DocumentEvents_CloseEvent arg0)
        {
        	logger.info("close document");
        }

        public void New(DocumentEvents_NewEvent arg0)
        {
        	logger.info("new document");
        }
    }); */

		//m_oDocument.addListener("",new EventListener(){});
	}

	public MsOfficeConnection getConnection()
	{
		return m_oMsOfficeConnection;
	}

	public Document getDocument()
	{
		return m_oDocument;
	}


	public TextContentLocation getStart()
	{
		// Implementation untested!
		Selection sel =  m_oDocument.getApplication().getSelection();
		sel.homeKey(new Variant(WdUnits.wdStory));
		return new MsOfficeTextContentLocation(sel.getRange());
	}

	public TextContentLocation getEnd()
	{
		// Implementation untested!
		Selection sel =  m_oDocument.getApplication().getSelection();
		sel.endKey(new Variant(WdUnits.wdStory));
		return new MsOfficeTextContentLocation(sel.getRange());
	}


	private List m_oOfficeWindowListener = new ArrayList();  


	private class WordWindowActionListener implements ActionListener
	{
		private final int m_iNumTicks = 3;    
		private int m_iCounter;

		public WordWindowActionListener()
		{
			m_iCounter = m_iNumTicks;

		}


		public void actionPerformed(ActionEvent evt)
		{
			if (m_oOfficeWindowListener != null)
			{
				for(int i=0; i<(m_oOfficeWindowListener.size()); i++)
				{
					OfficeWindowListener listener = (OfficeWindowListener)(m_oOfficeWindowListener.get(i));
					listener.windowActivated();
				}          

				m_iCounter --;
				if (m_iCounter <= 0)
				{
					m_oWordWindowTimer.stop();
					m_oWordWindowTimer = null;          
				}
			}
		}
	}




	private Timer m_oWordWindowTimer = null;

	public boolean addOfficeWindowListener(OfficeWindowListener listener)
	{
		m_oOfficeWindowListener.add(listener);

		if (m_oWordWindowTimer == null)
		{
			m_oWordWindowTimer = new Timer(1000, new WordWindowActionListener());
			m_oWordWindowTimer.start();
		}

		return true;
	}

	public boolean removeOfficeWindowListener(OfficeWindowListener listener)
	{
		m_oOfficeWindowListener.remove(listener);

		if (m_oOfficeWindowListener.size() == 0)
		{
			if (m_oWordWindowTimer != null)
			{
				m_oWordWindowTimer.stop();
				m_oWordWindowTimer = null;
			}
		}
		return true;
	}


	public float getLeftMargin()
	{
		return (float)(Util.convertNanoMetersToPica( m_oDocument.getPageSetup().getLeftMargin() * 1000 ));  	
	}

	public float getRightMargin()
	{
		return (float)(Util.convertNanoMetersToPica( m_oDocument.getPageSetup().getRightMargin() * 1000 ));  	
	}

	public float getTopMargin()
	{
		return (float)(Util.convertNanoMetersToPica( m_oDocument.getPageSetup().getTopMargin() * 1000 ));  	
	}

	public float getBottomMargin()
	{
		return (float)(Util.convertNanoMetersToPica( m_oDocument.getPageSetup().getBottomMargin() * 1000 ));  	
	}

	public float getPageWidth()
	{
		return (float)(Util.convertNanoMetersToPica( m_oDocument.getPageSetup().getPageWidth() * 1000 ));
	}

	public float getPageHeight()
	{
		return (float)(Util.convertNanoMetersToPica( m_oDocument.getPageSetup().getPageHeight() * 1000 ));
	}


	public TextContentLocation getStartOfPage(int page)
	{
		Range range = m_oDocument.getContent();
		range.m_goTo(new Variant(WdGoToItem.wdGoToPage), new Variant(WdGoToDirection.wdGoToFirst), new Variant(page));
		return new MsOfficeTextContentLocation(range);
	}



	public void setVisible(boolean visible)
	{
//		getDocument().getApplication().getActiveWindow().getView().setType(WdViewTypeOld.wdPageView);

//		getDocument().getApplication().setVisible(visible);
		m_oDocument.activate();
		m_oDocument.getActiveWindow().setVisible(visible);
		
		
		
		//Update field values (date, time etc)
//		getDocument().getFields().update();
		
		/*
	   if (visible) 
	   {
	   if (m_iOldWindowState == -1)
	   {
	   m_oDocument.get_ActiveWindow().set_WindowState(WdWindowState.wdWindowStateNormal);
	   }
	   else
	   {
	   m_oDocument.get_ActiveWindow().set_WindowState(m_iOldWindowState);
	   }
	   }
	   else
	   {
	   m_iOldWindowState = m_oDocument.get_ActiveWindow().get_WindowState();
	   m_oDocument.get_ActiveWindow().set_WindowState(WdWindowState.wdWindowStateMinimize);
	   }*/
	}


	public void toFront()
	{	
		toFront(false);
	}
	
	/**
	 * @see org.evolvis.liboffice.document.TextDocument#toFront(boolean)
	 */
	public void toFront(boolean force) {
//		m_oDocument.getApplication().activate();

		// If the document is minimized we need to restore the state first because
		// windows xp does not allow minimized windows to directly pop up
		
		if(force && m_oDocument.getActiveWindow().getWindowState() == WdWindowState.wdWindowStateMinimize)
			m_oDocument.getActiveWindow().setWindowState(WdWindowState.wdWindowStateNormal);

		// There is now way to make the document's window the top of _all_ windows (not only Office-Windows) via the Word-API.
		// So we need to call functions in user32:
		
		try {
			// Get the document's window handle first
			JNative _FindWindow = new JNative("user32.DLL", "FindWindowA");
			_FindWindow.setRetVal(Type.INT);  
			_FindWindow.setParameter(0, Type.INT, "0");  
			_FindWindow.setParameter(1, Type.STRING, m_oDocument.getActiveWindow().getCaption() + " - " + m_oDocument.getActiveWindow().getApplication().getName());  
			_FindWindow.invoke();  
			int windowHandle = _FindWindow.getRetValAsInt();
			
//			JNative _SetActiveWindow = new JNative("user32.DLL", "SetActiveWindow");
//			_SetActiveWindow.setRetVal(Type.INT);
//			_SetActiveWindow.setParameter(0, Type.INT, String.valueOf(windowHandle));
//			_SetActiveWindow.invoke();
//			
			// set window to be the forgeground window
			JNative _SetForegroundWindow = new JNative("user32.DLL", "SetForegroundWindow");
			_SetForegroundWindow.setParameter(0, Type.INT, String.valueOf(windowHandle));
			_SetForegroundWindow.invoke();
//			
//			// now bring the window to top
//			JNative _BringWindowToTop = new JNative("user32.DLL", "BringWindowToTop");
//			_BringWindowToTop.setRetVal(Type.INT);
//			_BringWindowToTop.setParameter(0, Type.INT, String.valueOf(windowHandle));
//			_BringWindowToTop.invoke();
//			
			// Now get the window handle of the current foreground window
			JNative _GetForegroundWindow = new JNative("user32.DLL", "GetForegroundWindow");
			_GetForegroundWindow.setRetVal(Type.INT);
			_GetForegroundWindow.invoke();
			
			int foregroundWindowHandle = _GetForegroundWindow.getRetValAsInt();
			
			// Set the Z-Order of this window to be the topmost of all inactive windows
			JNative _SetWindowPos = new JNative("user32.DLL", "SetWindowPos");
			_SetWindowPos.setRetVal(Type.VOID);
			_SetWindowPos.setParameter(0, Type.INT, String.valueOf(windowHandle));
			_SetWindowPos.setParameter(1, Type.INT, String.valueOf(foregroundWindowHandle));
			_SetWindowPos.setParameter(2, Type.INT, "0");
			_SetWindowPos.setParameter(3, Type.INT, "0");
			_SetWindowPos.setParameter(4, Type.INT, "0");
			_SetWindowPos.setParameter(5, Type.INT, "0");
			_SetWindowPos.setParameter(6, Type.INT, String.valueOf((int)(0x2 | 0x1)));
			_SetWindowPos.invoke();
			
//			// Give the windows the focus
//			JNative _SetFocus = new JNative("user32.DLL", "SetFocus");
//			_SetFocus.setRetVal(Type.INT);
//			_SetFocus.setParameter(0, Type.INT, String.valueOf(windowHandle));
//			_SetFocus.invoke();
//		
			
		} catch (Exception e) {
			
			throw new RuntimeException("An error occured when trying to bring document to front", e);	
		}

		m_oDocument.activate();
		
	}

	public void toBack()
	{
		throw new UnsupportedOperationException("Method is not implemented");
	}


	public boolean close()
	{
		return close( false );
	}

	public TextContentRange getDocumentRange()
	{
		Range range = m_oDocument.getContent();
		return new MsOfficeTextContentRange(this, range);
	}

	public TextContentLocation getTextContentLocation()
	{
		return new MsOfficeTextContentLocation( this, false);
	}

	public TextContentLocation getTextContentLocationAfter()
	{
		return new MsOfficeTextContentLocation( this, true);
	}

	private Integer getFiletypeInteger(OfficeFileFormat type)
	{
		// TODO add DOCX, DOT and DOTX formats if necessary
		if (KnownOfficeFileFormats.FILE_TYPE_RTF.equals(type)) return new Integer(WdSaveFormat.wdFormatRTF);
		else if (KnownOfficeFileFormats.FILE_TYPE_DOC.equals(type)) return new Integer(WdSaveFormat.wdFormatDocument);
		else if(KnownOfficeFileFormats.FILE_TYPE_DOT.equals(type)) return new Integer(WdSaveFormat.wdFormatTemplate);
		//else if (KnownOfficeFileFormats.FILETYPE_HTML.equals(type)) return new Integer( 100 );
		return null;
	}

	public boolean save(String filename, OfficeFileFormat filetype, boolean overwrite) throws UnsupportedOfficeFileTypeException
	{
		logger.fine("fileType " + filetype + " fileName " + filename);
		Integer fileTypeInt = getFiletypeInteger(filetype);
		if (fileTypeInt == null) throw new UnsupportedOfficeFileTypeException(filetype);

		try 
		{        
			logger.fine("filename: " + filename + " type: " + fileTypeInt);

			/*
			 *  Does not work yet
            int saveMode = WdSaveOptions.wdPromptToSaveChanges;
            if(overwrite) saveMode = WdSaveOptions.wdSaveChanges;
            m_oDocument.saveAs(new Variant(filename), new Variant(fileTypeInt), Variant.VT_MISSING, Variant.VT_MISSING, Variant.VT_MISSING, Variant.VT_MISSING, Variant.VT_MISSING, Variant.VT_MISSING, Variant.VT_MISSING, Variant.VT_MISSING, new Variant(WdSaveOptions.wdPromptToSaveChanges));
			 */

			int selectedOption = JOptionPane.YES_OPTION;

			if(!overwrite && new File(filename).exists())
				selectedOption = JOptionPane.showConfirmDialog(new JFrame(Messages.getString("GUI_OVERWRITE_FILE_TITLE")), Messages.getFormattedString("GUI_OVERWRITE_FILE_TEXT", filename), Messages.getString("GUI_OVERWRITE_FILE_TITLE"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

			if(selectedOption == JOptionPane.YES_OPTION)
			{
				m_oDocument.saveAs(new Variant(filename), new Variant(fileTypeInt));
				return true;
			}
			return false;
		} 
		catch (ComException e) 
		{
			logger.log(Level.SEVERE, "Error", e );
		}      
		return false;
	}

	public boolean close(boolean forceCloseOnChanges)
	{
		try 
		{
			m_oDocument.close(new Variant(!forceCloseOnChanges));

			if(this.m_oMsOfficeConnection.getApplication().getDocuments().getCount() == 0)
				this.m_oMsOfficeConnection.getApplication().quit();

			return true;
		} 
		catch (ComException e) 
		{
			logger.log(Level.SEVERE, "Fehler beim Speichern", e);
		}
		return false;
	}

	public boolean print(String printer, int frompage, int topage)
	{
		Application application = m_oDocument.getApplication();
		String activePrinter = application.getActivePrinter();
		if (printer != null)
		{
			if (!(printer.equals(activePrinter)))
			{
				application.setActivePrinter(printer);  
			}
		}

		if ((frompage != 0) && (topage != 0))
		{
			int startPage = 0;
			int endPage = 0;

			if (frompage <= topage)
			{
				startPage = frompage;
				endPage = topage;
			}
			else
			{
				startPage = topage;
				endPage = frompage;
			}

			try
			{
				application.getActiveDocument().printOut(new Variant(false), new Variant(false), new Variant(WdPrintOutRange.wdPrintFromTo), new Variant(""), new Variant(startPage), new Variant(endPage), new Variant(WdPrintOutItem.wdPrintDocumentContent), new Variant(1), new Variant(""), new Variant(WdPrintOutPages.wdPrintAllPages));
			}
			catch(ComException pExcp)
			{
				logger.log(Level.SEVERE, "Error", pExcp.getLocalizedMessage());
				return false;
			}
		}
		else
		{
			try
			{
				application.getActiveDocument().printOut();
			}
			catch(ComException pExcp)
			{
				logger.log(Level.SEVERE, "Error", pExcp.getLocalizedMessage());
				return false;
			}
		}
		//application.PrintOut(new Boolean(false), new Boolean(false), new Integer(WdPrintOutRange.wdPrintAllDocument), "", new Integer(0), new Integer(0), new Integer(WdPrintOutItem.wdPrintDocumentContent), new Integer(1), "", new Integer(WdPrintOutPages.wdPrintAllPages));    
		return true;

	}

	public int getPageNumber(TextContentLocation location)
	{
		if(location instanceof MsOfficeTextContentLocation)
		{
			MsOfficeTextContentLocation mslocation = (MsOfficeTextContentLocation) location;

			mslocation.getRange().select();
			Selection selection = m_oDocument.getApplication().getSelection();
			Object pageNumberObject = selection.getInformation(WdInformation.wdActiveEndPageNumber);
			if(pageNumberObject instanceof Integer)
			{
				return ((Integer)pageNumberObject).intValue();
			}
		}
		return -1;
	}

	public void setVisualUpdateActive(boolean active)
	{
		m_oDocument.activate();
		m_oDocument.getApplication().setScreenUpdating(active);
	}

	public String[] getAvailableFontNames()
	{
		FontNames fontnames = m_oDocument.getApplication().getFontNames();
		if (fontnames != null)
		{
			String[] names = new String[fontnames.getCount()];
			for(int z=1; z<(fontnames.getCount() + 1); z++)
			{
				String fontname = fontnames.item(z);
				names[z-1] = fontname;
			}
			return names;
		}
		return new String[0];
	}

	public String getDocumentFilename()
	{
		return m_oDocument.getFullName();
	}

	/*

    private Dispatch getDocumentPropertyDispatch(Integer documentPropertiesIDispatch, int property) throws ComException 
    {
      Dispatch dispatch = new Dispatch(documentPropertiesIDispatch.intValue());
      Jvariant args[] = {new Jvariant(property, COMconstants.VT_I4)};
      Jvariant variant = dispatch.invoke_method(args, dispatch.getIDsOfNames("Item"), COMconstants.DISPATCH_PROPERTYGET);
      return new Dispatch(variant.intVal());
    }

    private String getBuiltInProperty(Document document, int property) throws ComException 
    {
      Integer builtInPropertiesIDispatch = (Integer) document.getBuiltInDocumentProperties();
      return getDocumentProperty(builtInPropertiesIDispatch, property);
    }

    private String getDocumentProperty(Integer documentPropertiesIDispatch, int property) throws ComException 
    {
      Dispatch dispatch = getDocumentPropertyDispatch(documentPropertiesIDispatch, property);
      return dispatch.invoke_method(new Jvariant[0], dispatch.GetIDsOfNames("Value"), COMconstants.DISPATCH_PROPERTYGET).StringVal();
    }


    private void setDocumentProperty(Integer documentPropertiesIDispatch, int property, String value) throws ComException 
    {
      Dispatch dispatch = getDocumentPropertyDispatch(documentPropertiesIDispatch, property);
      dispatch.invoke_method(new Jvariant[] {new Jvariant(value, COMconstants.VT_BSTR)}, dispatch.GetIDsOfNames("Value"), COMconstants.DISPATCH_PROPERTYPUT);
    }

    private void setBuiltInProperty(Document document, int property, String value) throws ComException 
    {
      Integer builtInPropertiesIDispatch = (Integer) document.get_BuiltInDocumentProperties();
      setDocumentProperty(builtInPropertiesIDispatch, property, value);
    }
	 */

	/**
	 * Diese Methode setzt den Kommentar des Dokuments.
	 * 
	 * @param comment der gew&uml;nschte neue Kommentar
	 * @return Erfolgsflag
	 */
	private boolean setDocumentComment(String comment) 
	{
		try 
		{
			//setBuiltInProperty(m_oDocument, WdBuiltInProperty.wdPropertyComments, comment);
			return true;
		} 
		catch (ComException e) 
		{
		}
		return false;
	}


	public boolean setDocumentInformations(List virtualdocumentinformations)
	{
		String infotext = "";
		Iterator it = virtualdocumentinformations.iterator();
		while(it.hasNext())
		{
			Object obj = it.next();
			if (obj != null)
			{
				if (obj instanceof VirtualDocumentInformation)
				{
					VirtualDocumentInformation info = (VirtualDocumentInformation)obj;
					if (VirtualDocumentInformationStorage.STORAGE_COMMENT.equals(info.getStorageLocation()))
					{
						infotext += ((info.getKey() + "=" + info.getValue()) + ";");
					}            
				}
			}
		}
		return setDocumentComment(infotext);
	}

	/**
	 * Diese Methode holt den Kommentar des Dokuments erweitert um die Schl&uml;sselw�rter.
	 * 
	 * @return Kommentar + Schl&uml;sselw�rter des Dokuments.
	 */
	private String getDocumentComment(Document document) 
	{
		try 
		{
			//return getBuiltInProperty(document, WdBuiltInProperty.wdPropertyComments) + getBuiltInProperty(document, WdBuiltInProperty.wdPropertyKeywords);
		} 
		catch (ComException e) 
		{
		}
		return null;
	}

	public List getDocumentInformations()
	{
		List list = new ArrayList();
		String infotext = getDocumentComment(m_oDocument);
		String[] infos = infotext.split(";");
		if (infos != null)
		{
			for(int i=0; i<(infos.length); i++)
			{
				String info = infos[i];
				if (info != null)
				{
					String[] keyval = info.split("=");
					if (keyval != null)
					{
						if (keyval.length == 2)
						{
							String key = keyval[0];
							String val = keyval[1];
							VirtualDocumentInformation vdi = new VirtualDocumentInformation(key, val, VirtualDocumentInformationStorage.STORAGE_COMMENT);
							list.add(vdi);
						}
					}
				}
			}
		}
		return list; 
	}

	public boolean copy()
	{
		// TODO: TESTEN! Sowohl mit auch ohne eingeschalteten ClipboardBuffer
		m_oDocument.activate();
		m_oDocument.getApplication().getSelection().copy();
		return true;
	}

	public boolean paste()
	{
		// TODO: TESTEN! Sowohl mit als auch ohne eingeschalteten ClipboardBuffer
		m_oDocument.activate();
		m_oDocument.getApplication().getSelection().paste();
		return true;
	}

	public boolean pasteSpecial(int pPasteFormat)
	{
		// Not implemented yet
		return paste();
	}

	public boolean insertDocumentAtStart(String filename)
	{
		jumpToLocation(getStart());
//		insertPageBreak();
		jumpToLocation(getStart());
		insertDocumentFromFile(filename);
		return true;
	}

	public boolean insertDocumentAtEnd(String filename)
	{
		jumpToLocation(getEnd());
		insertPageBreak();
		jumpToLocation(getEnd());
		insertDocumentFromFile(filename);
		return true;
	}

	public boolean insertPageBreak()
	{
		Application application = m_oDocument.getApplication();
		application.getSelection().insertBreak();
		return true;
	}

	public void insertParaBreak()
	{	
		m_oDocument.getApplication().getSelection().typeParagraph();
	}

	public boolean insertLineBreak()
	{
		throw new UnsupportedOperationException("Method is not implemented");
	}

	public boolean insertDocumentFromFile(String fileName)
	{
		Application application = m_oDocument.getApplication();
//		application.getSelection().insertFile( fileName );
		application.getSelection().insertFile(fileName, new Variant(application.getSelection().getRange()), new Variant(false), new Variant(false), new Variant(true));
//		m_oDocument.getApplication().getSelection().getRange().insertFile(fileName);

		return true;
	}

	private void positionCursorAtRangeStart(Application application, Range range)
	{
		if (range != null)
		{
			if (application != null)
			{
				range.select();
				application.getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseStart));
			}
		}
	}

	public boolean jumpToLocation(TextContentLocation location)
	{
		if (location instanceof MsOfficeTextContentLocation)
		{
			MsOfficeTextContentLocation mslocation = (MsOfficeTextContentLocation)location; 
			positionCursorAtRangeStart(getConnection().getApplication(), mslocation.getRange());
			return true;
		}
		return false;
	}

	public boolean remove()
	{
		return false;
	}

	public String getActivePrinterName()
	{
		Application application = m_oDocument.getApplication();
		return application.getActivePrinter();
	}

	public TextContentLocation getCurrentCursorLocation()
	{
		if (m_oDocument != null)
		{
			m_oDocument.activate();            
			Selection selection = m_oDocument.getApplication().getSelection();
			if (selection != null)
			{
				Range range = selection.getRange();
				if (range != null)
				{
					return new MsOfficeTextContentLocation(range);                      
				}
			}
		}
		return null;
	}

	public boolean existsStyle(VirtualTextStyle style)
	{
		Styles styles = m_oDocument.getStyles();

		for (int i=1; i<=(styles.getCount()); i++)
		{
			Style s = styles.item(new Variant(i));     
			if (s != null)
			{
				if ( s.getNameLocal().equals(style.getStyleName()))
				{
					return (
							(s.getType() == WdStyleType.wdStyleTypeCharacter && 
									style.getStyleType() == VirtualTextStyleType.STYLE_CHARACTER)
									||
									(s.getType() == WdStyleType.wdStyleTypeParagraph && 
											style.getStyleType() == VirtualTextStyleType.STYLE_PARAGRAPH));

				}
			}
		}
		return false;
	}

	public TextStyle getTextStyle(String stylename)
	{
		return getTextStyle(stylename, true);
	}

	public TextStyle getTextStyle(String pStylename, boolean pReInit)
	{
		if(pReInit || textStyles == null) initTextStyleMap();

		return (MsOfficeTextStyle) textStyles.get(pStylename);
	}

	private void initTextStyleMap()
	{
		textStyles = new HashMap();

		Styles styles = m_oDocument.getStyles();

		for(int i=1; i <= styles.getCount(); i++)
		{
			Style style = styles.item(new Variant(i));
			if(style != null)
			{
				textStyles.put(style.getNameLocal(), new MsOfficeTextStyle(style));
			}
		}
	}

	public TextStyle copyTextStyle(VirtualTextStyle oldStyle, String newStyleName)
	{
		Style style = copyStyle(oldStyle, newStyleName);
		if (style != null)
		{
			return new MsOfficeTextStyle(style);
		}
		return null;
	}

	private Style copyStyle(VirtualTextStyle sourceStyle, String destinationStyleName) 
	{
		if (m_oDocument != null)
		{
			VirtualTextStyleType type = sourceStyle.getStyleType();

			removeTextStyle(new VirtualTextStyle(destinationStyleName, sourceStyle.getStyleType() == VirtualTextStyleType.STYLE_CHARACTER ? VirtualTextStyleType.STYLE_PARAGRAPH : VirtualTextStyleType.STYLE_CHARACTER) );
			Style newStyle            = m_oDocument.getStyles().add(destinationStyleName, new Variant(type == VirtualTextStyleType.STYLE_CHARACTER ? WdStyleType.wdStyleTypeCharacter : WdStyleType.wdStyleTypeParagraph)); 

			if (null != sourceStyle.getStyleName())
			{
				newStyle.setBaseStyle(new Variant(sourceStyle.getStyleName()));
			}
			return newStyle;
		}
		return null;
	}

	public void removeTextStyle(VirtualTextStyle style)
	{

		Styles styles = m_oDocument.getStyles();

		for (int i=1; i<=(styles.getCount()); i++)
		{
			Style s = styles.item(new Variant(i));     
			if (style != null)
			{
				if (s.getNameLocal().equals(style.getStyleName()))
				{
					if (   (s.getType() == WdStyleType.wdStyleTypeCharacter && 
							style.getStyleType() == VirtualTextStyleType.STYLE_CHARACTER)
							||
							(s.getType() == WdStyleType.wdStyleTypeParagraph && 
									style.getStyleType() == VirtualTextStyleType.STYLE_PARAGRAPH))
					{
						s.delete();
					}
				}
			}
		}    
	}


	public TextFrame insertTextFrame(VirtualTextFrame vFrame)
	{
//		org.evolvis.liboffice.msword11.Shape frame         =  m_oDocument.getShapes().addTextbox(new Variant(1), (float)Util.convertNanoMetersToPica(vFrame.getPositionLeft()), 
//				(float)Util.convertNanoMetersToPica(vFrame.getPositionTop()), 
//				(float)Util.convertNanoMetersToPica(vFrame.getWidth()),
//				(float)Util.convertNanoMetersToPica(vFrame.getHeight()));
//		MsOfficeTextFrame msFrame = new MsOfficeTextFrame( frame );
//		jumpToLocation(msFrame.getFrameContentLocation() );
//		insertText(vFrame.getText());
//		return msFrame;
		return null;
	}

	public TextTable insertTable(VirtualTextTable table)
	{
		if (table == null || table.getNumberOfRows() == 0) return null;
		int rowCount = table.getNumberOfRows();
		int columnCount = table.getNumberOfColumns();

		m_oDocument.activate();

		Selection selection = m_oDocument.getApplication().getSelection();
		Range range = selection.getRange();
		// Absatz als Style-Standard formatieren um Probleme beim einfuegen in Absaetze mit auto-Listen zu vermeiden

		setSelectionStyle("Standard",selection, null);

		Table wordTable = range.getTables().add(range, rowCount, columnCount);
		MsOfficeTextTable msTable = new MsOfficeTextTable(this, wordTable);
		for (int row = 0; row < rowCount; row++) 
		{
			for (int column = 0; column < columnCount; column++) 
			{
				VirtualTextTableCell cell = table.getTextTableCell(row, column); 
				if (cell != null)
				{
					Cell wordCell = wordTable.cell(row + 1, column + 1);
					if (wordCell != null)
					{
						wordCell.getRange().select();

						if(null != cell && null != wordCell.getBorders())
						{
							if(null != cell.getBorderBottom()) wordCell.getBorders().item(WdBorderType.wdBorderBottom).setVisible(  cell.getBorderBottom().isVisible() );
							if(null != cell.getBorderTop())    wordCell.getBorders().item(WdBorderType.wdBorderTop).setVisible(  cell.getBorderTop().isVisible() );
							if(null != cell.getBorderLeft())   wordCell.getBorders().item(WdBorderType.wdBorderLeft).setVisible(  cell.getBorderLeft().isVisible() );
							if(null != cell.getBorderRight())  wordCell.getBorders().item(WdBorderType.wdBorderRight).setVisible(  cell.getBorderRight().isVisible() );
						}

						VirtualStyledText vst = cell.getStyledText();
						if (vst != null)
						{
							insertText(vst);              
						}

						msTable.setHorizontalAlignment( cell );
					}
				}
			}
		}

		// Tabelle soll per default _unsichtbare_ Linien haben
		if (!(table.getShowLines())) removeBorders(wordTable); 

		for (int column = 0; column < columnCount; column++) 
		{
			Column col = wordTable.getColumns().item(column + 1);

			Object columnWidth = table.getColumnWidth(column);
			if (!(VirtualTextTable.COLUMNWIDTH_NONE.equals(columnWidth)))
			{
				if (columnWidth instanceof Integer)
				{
					logger.fine("columnWidth "+ columnWidth);
					logger.fine("setWidth " + (float)Util.convertNanoMetersToPica(    ((Integer)columnWidth).intValue() ));
					col.setWidth( (float)Util.convertNanoMetersToPica(    ((Integer)columnWidth).intValue() ));
				}
				else if (VirtualTextTable.COLUMNWIDTH_AUTOMATIC.equals(columnWidth))
				{
					col.autoFit();
				}
			}
		}

		if(table.getMarginLeft() != null)
		{
			wordTable.getRows().setLeftIndent( (float) Util.convertNanoMetersToPica(table.getMarginLeft().getMargin()) );
		}

		if (table.getWidth() != -1)
		{
			if (table.getNumberOfColumns() > 1)
			{

				Object col = table.getColumnWidth( 0 );
				if (col instanceof Integer)
				{
					float colWidth     = ((Integer)col).floatValue();
					float pageWidth    = getPageWidth();
					VirtualMargin marg = table.getMarginLeft();
					if (null != marg)
					{
						pageWidth -= marg.getMargin();
					}
					wordTable.getColumns().item( 2 ).setWidth( (float) Util.convertNanoMetersToPica( pageWidth - colWidth ) );
				}

			}
		}

		//TODO: This two lines were added for the baz-doctor project.
		msTable.getTable().getColumns().setWidth(table.getWidth());
		msTable.getTable().getRows().setAlignment(table.getAlighment());
		//TODO: test!
		//Selection endSelection = doc.getDocument().get_Application().get_Selection();
		//Range endRange = endSelection.get_Range();

		range = wordTable.getRange();

		range.expand(new Variant(WdUnits.wdCharacter)); // oRange2.Expand Unit:=wdCharacter
		range.collapse(new Variant(WdCollapseDirection.wdCollapseEnd)); // oRange2.Collapse wdCollapseEnd
		// end test    
		range.select();    
		
//		msTable.getTable().getColumns().setWidth(table.getWidth());
		
		return msTable;
	}

	public TextGraphic insertGraphic(VirtualGraphic graphic)
	{
		String fileName = graphic.getFilename();
		if(null != fileName)
		{
			Range range = m_oDocument.getApplication().getSelection().getRange();
			return new MsOfficeTextGraphic( m_oDocument.getShapes().addPicture( fileName,  new Variant(false), new Variant(true), new Variant(range)));
		}
		return null;

	}

	public TextMarker insertMarker(VirtualTextPortionMarker marker)
	{
		org.evolvis.liboffice.msword11.Fields fields = m_oDocument.getFields();
		String name   = marker.getName();
		if (null != marker.getHint())
		{
			name += MsOfficeTextMarker.CONST_MARKER_HINT_SEPARATOR+marker.getHint();
		}
		fields.getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseStart));
		Field field   = fields.add(m_oDocument.getApplication().getSelection().getRange(), new Variant(WdFieldType.wdFieldMergeField), new Variant(name), new Variant(true));     // Range, Type, Text, PreserveFormatting    
		return new MsOfficeTextMarker(this, field);

	}

	public boolean removeMarker(VirtualTextPortionMarker marker)
	{
		TextDocumentComponent comp = getTextContentLocator().locateComponent(this, new NameHintMarkerDescriptor(marker.getName(), marker.getHint()));
		if (comp != null)
		{
			return removeMarker((TextMarker)comp);      
		}    
		return false;
	}

	public boolean removeMarker(TextMarker marker)
	{
		Field field = ((MsOfficeTextMarker)marker).getField();
		if (field != null)
		{
			if (field.getType() == 59)
			{
				field.delete();
				return true;
			}
		}
		return false;
	}

	public boolean insertStyle(VirtualTextStyle style)
	{
		if (existsStyle( style)) return false;
		Style newStyle = m_oDocument.getStyles().add(style.getStyleName());
		if (newStyle != null)
		{
			if (style.getFont() != null)
			{
				Font font = null;
				if (newStyle.getFont() != null)
				{
					font = newStyle.getFont();          
				}
				else
				{
					//font = new Font(style.getFont().getFontName());
				}

				if (font != null)
				{
					if (!(font.getName().equals(style.getFont().getFontName())))
					{
						font.setName(style.getFont().getFontName());            
					}

					font.setBold(style.getFont().isBold() ? 1: 0);
					font.setItalic(style.getFont().isItalic() ? 1: 0);
					font.setUnderline(style.getFont().isUnderline() ? 1: 0);
				}
			}
			return true;
		}      
		return false;
	}




	public boolean insertText(VirtualStyledText text)
	{
		if (null == text ) return false;
		boolean result = true;

		Selection selection = m_oDocument.getApplication().getSelection();


		String standardStyle = "";

		for(int i = 0; i<(text.getNumberOfParagraphs()); i++)
		{
			VirtualParagraph para = text.getParagraph(i);

			// den Style als Default setzen... 
			selection = m_oDocument.getApplication().getSelection();    
			setSelectionStyle(standardStyle, selection, para.getStyle());

			for(int n=0; n<(para.getNumberOfTextPortions()); n++)
			{
				VirtualTextPortion tvtp = para.getTextPortion(n);
				if (tvtp instanceof VirtualTextPortionText)
				{
					// die Portion ist ein normaler Text
					VirtualTextPortionText vtp = (VirtualTextPortionText)tvtp;
					selection = m_oDocument.getApplication().getSelection();    

					if (null != vtp.getStyle())
					{
						setSelectionStyle(standardStyle, selection, vtp.getStyle());
					}

					try 
					{
						// Textauszeichnungen (Font, Bold, Italic, Underline) setzen...
						setSelectionFormat(selection, vtp);
						// den eigentlichen Text ausgeben...
						selection.typeText(vtp.getText());
					} 
					catch (ComException e) 
					{
						logger.log(Level.SEVERE, "Error", e );
					}
				}
				else if (tvtp instanceof VirtualTextPortionMarker)
				{
					// die Portion ist ein Marker
					VirtualTextPortionMarker vtpm = (VirtualTextPortionMarker)tvtp;
					insertMarker(vtpm);
				}
				else if (tvtp instanceof VirtualParagraphBreak)
				{           
					// die Portion ist ein ParagraphBreak
					//VirtualParagraphBreak vtpm = (VirtualParagraphBreak)tvtp;
					selection.typeParagraph();
					selection = m_oDocument.getApplication().getSelection();
				}

			} // ende for(int n=0; n<(para.getNumberOfTextPortions()); n++)
		} // ende for(int i = 0; i<(text.getNumberOfParagraphs()); i++)

		return(result);

	}
	private void setSelectionFormat(Selection selection, VirtualTextPortionText vtp)
	{
		if (vtp.getFont() != null)
		{
			selection.getFont().setName(vtp.getFont().getFontName());
			selection.getFont().setSize((float)(vtp.getFont().getFontHeight()));
		}

		if (vtp.useBold())
		{
			if (vtp.isBold()) selection.getFont().setBold(1);
			else              selection.getFont().setBold(0);
		}

		if (vtp.useUnderline())
		{
			if (vtp.isUnderline()) selection.getFont().setUnderline(WdUnderline.wdUnderlineSingle);
			else                   selection.getFont().setUnderline(WdUnderline.wdUnderlineNone);
		}

		if (vtp.useItalic())
		{
			if (vtp.isItalic()) selection.getFont().setItalic(1);
			else                selection.getFont().setItalic(0);
		}
	}


	private void setSelectionStyle(String sStandardCharacterStylename, Selection selection, VirtualTextStyle style)
	{     
		if (style != null)
		{
			String styleName = style.getStyleName();          
			logger.fine("styleName = " + styleName);        
			MsOfficeTextStyle ts = (MsOfficeTextStyle)getTextStyle( styleName, false);
			if (ts != null)
			{
				selection.setStyle(new Variant(ts.getStyle()));
			}
		}
		else
		{
			MsOfficeTextStyle ts = (MsOfficeTextStyle)(getTextStyle(sStandardCharacterStylename, false));
			if (ts != null)
			{
				selection.setStyle(new Variant(ts.getStyle()));
			}
		}
	}

	private void removeBorders(Table table) 
	{
		table.getBorders().setInsideLineStyle(WdLineStyle.wdLineStyleNone);
		table.getBorders().setOutsideLineStyle(WdLineStyle.wdLineStyleNone);
	}

	public TextDocument cloneDocument(OfficeController controller)
	{
		File file;
		try
		{
			file = File.createTempFile("cloneDocument", "doc");
			String tmpFilename = file.getAbsolutePath();
			try
			{
				save(tmpFilename, KnownOfficeFileFormats.FILE_TYPE_DOC, true);
				TextDocument clonedDocument = controller.loadDocumentAsCopy(tmpFilename);
				file.delete();
				return clonedDocument;        
			}
			catch (UnsupportedOfficeFileTypeException e)
			{
				e.printStackTrace();
			}
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
		return null;
	}

	protected TextContentLocator getTextContentLocator()
	{
		if (null == locator)
		{
			locator = new MsOfficeTextContentLocator( m_oMsOfficeConnection);
		}
		return locator;
	}

	public void select()
	{
		m_oDocument.activate();
		m_oDocument.getApplication().getSelection().wholeStory();
	}

	public void collapseSelection(boolean collapseToEnd)
	{
		m_oDocument.activate();
		if(collapseToEnd){
			m_oDocument.getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
		} else {
			m_oDocument.getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseStart));
		}
	}



	public TextBookmark insertTextBookmark(String name) {
			m_oDocument.activate();		
			Range range = m_oDocument.getApplication().getSelection().getRange();
			insertTextBookmark(name, new MsOfficeSpan(range));
			return new MsOfficeTextBookmark(this, m_oDocument.getBookmarks().item(new Variant(name)));
	}
	
	public void insertTextBookmark(String name, Span span) {
		m_oDocument.activate();		
		m_oDocument.getBookmarks().add(name, new Variant(((MsOfficeSpan)span).getRange()));		

}
	
	public List<TextBookmark> getBookmarks(boolean sortByName) {	

		List<TextBookmark> out = new ArrayList<TextBookmark>();
		Bookmarks bookmarks;
		m_oDocument.activate();
		
		if(!sortByName){
			bookmarks = m_oDocument.getContent().getBookmarks();
		} else {
			bookmarks = m_oDocument.getBookmarks();
		}
		int amountBookmarks = bookmarks.getCount();
		
		for(int i=1; i<= amountBookmarks; i++) {
			Bookmark bookmark = bookmarks.item(new Variant(i));
			MsOfficeTextBookmark ms_bookmark = new MsOfficeTextBookmark(this, bookmark);
			out.add(ms_bookmark);
//			checkBookmark(descs, mstextdoc, bookmark, out);
		}
		
		m_oDocument.getApplication();
		return out;
	}



	public List<String> getBookmarkNames(boolean sortByName) {
		List<String> out = new ArrayList<String>();
		Bookmarks bookmarks;
		m_oDocument.activate();
		
		if(!sortByName){
			bookmarks = m_oDocument.getContent().getBookmarks();
		} else {
			bookmarks = m_oDocument.getBookmarks();
		}
		int amountBookmarks = bookmarks.getCount();
		
		for(int i=1; i<= amountBookmarks; i++) {
			out.add(bookmarks.item(new Variant(i)).getName());
		}
		return out;
	}

	public TextBookmark getBookmark(String name){
		m_oDocument.activate();
		if(m_oDocument.getBookmarks().exists(name)){
			Bookmark bookmark = m_oDocument.getBookmarks().item(new Variant(name));
			TextBookmark ms_bookmark = new MsOfficeTextBookmark(this, bookmark);
			return ms_bookmark;
		}
		return null;
	}

	public void copyContent(TextBookmark startBookmark, TextBookmark endBookmark) {
		m_oDocument.activate();
		Range range = ((MsOfficeSpan)startBookmark.getSpan()).getRange();
		startBookmark.setCursorAtEnd();
		range.setStart(m_oDocument.getApplication().getSelection().getStart());
		range.setEnd(endBookmark.getStart());
		range.select();
		range.copy();
//		Selection s = m_oDocument.getApplication().getSelection();
//		s.copy() ;		
	}

	public void insertVariable(String name, String value) {
		Variable variable = null;
		for(int i = 1; i<=m_oDocument.getVariables().getCount(); i++){
			Variable v = m_oDocument.getVariables().item(new Variant(i));
			if(v.getName().equals(name)){
				variable = v;
			}
		}	
		if(variable==null){
			variable = m_oDocument.getVariables().add(name);
			variable.setValue(value);
		} else {
			variable.setValue(value);
		}


	}

	public TextVariable getTextVariable(String name) {		
		m_oDocument.activate();
		List<TextVariable> list = getTextVariables();
		
		// FIXME merge MS-Word 'Variable' object to liboffice 'TextVariable' object (or create  new abstract structure)
		throw new UnsupportedOperationException("This method needs to be re-implemented!");
		
//		for(TextVariable v : list){
//			if(v.getName().equals(name)){
//				return new MsOfficeTextVariable(this, m_oDocument.getVariables().item(new Variant(name)));
//			}
//		}
//		return null;
	}

	public boolean existsBookmark(String bookmarkName) {
		return m_oDocument.getBookmarks().exists(bookmarkName);
	}

	public void copyContentAfterBookmark(String bookmarkName) {
		m_oDocument.activate();
		TextBookmark bookmark = getBookmark(bookmarkName);
		bookmark.remove();
		m_oDocument.getParagraphs().getFirst().getRange().delete();
		m_oDocument.getApplication().getSelection().wholeStory();
		copy();
		
	}

	public void delete() {
		m_oDocument.getApplication().getSelection().delete();
		
	}

	public String getDocumentText() {
		m_oDocument.getApplication().getSelection().wholeStory();
		
		return m_oDocument.getApplication().getSelection().getText();
	}

	public List<TextVariable> getTextVariables() {
		m_oDocument.activate();
		int variablesCount = m_oDocument.getVariables().getCount();
		//TODO: Variables List starts with 1 ???????
		List<TextVariable> variables = new ArrayList<TextVariable>();
		
		// FIXME merge MS-Word 'Variable' object to liboffice 'TextVariable' object (or create  new abstract structure)
		throw new UnsupportedOperationException("This method needs to be re-implemented!");
//		for(int i = 0; i<variablesCount; i++)
//			variables.add(m_oDocument.getVariables().item(new Variant(i+1)));
		
//		return variables;
	}

	public boolean deleteParagraph(String bookmarkName) {
		TextBookmark bookmark = getBookmark(bookmarkName);
		if(bookmark==null){
			return false;
		}
		bookmark.setCursorAtEnd();
		m_oDocument.getApplication().getSelection().startOf(new Variant(WdUnits.wdParagraph));
		m_oDocument.getApplication().getSelection().moveEnd(new Variant(WdUnits.wdParagraph));
		m_oDocument.getApplication().getSelection().delete(new Variant(WdUnits.wdCharacter), new Variant(1));
		return true;
	}

	public void activate() {
		m_oDocument.activate();		
	}

	public void insertSectionBreakNextPage() {
		m_oDocument.activate();
		m_oDocument.getApplication().getSelection().insertBreak(new Variant(WdBreakType.wdSectionBreakNextPage));
		
	}
	
	
}
