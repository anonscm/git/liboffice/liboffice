/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.controller.Util;
import org.evolvis.liboffice.document.TextFrame;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocation;
import org.evolvis.liboffice.msword11.Shape;



/**
 * @author Steffen
 *
 */
public class MsOfficeTextFrame implements TextFrame
{
	private Shape frame;
	public MsOfficeTextFrame(Shape frame)
	{
		this.frame = frame;
	}


	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#getWidth()
	 */
	public float getWidth() 
	{
		return (float)Util.convertPicaToNanoMeters(frame.getWidth());
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#getHeight()
	 */
	public float getHeight() 
	{
		return (float)Util.convertPicaToNanoMeters(frame.getHeight());
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#getPositionTop()
	 */
	public float getPositionTop() 
	{
		return (float)Util.convertPicaToNanoMeters(frame.getTop());
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#getPositionLeft()
	 */
	public float getPositionLeft() 
	{
		return (float)Util.convertPicaToNanoMeters( frame.getLeft());
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#setWidth(int)
	 */
	public boolean setWidth(float width) 
	{
		frame.setWidth(width);
		return true;
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#setHeight(int)
	 */
	public boolean setHeight(float height) 
	{
		frame.setHeight( height );
		return true;
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#setPositionTop(int)
	 */
	public boolean setPositionTop(float top) 
	{
		frame.setTop( top );
		return true;
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#setPositionLeft(int)
	 */
	public boolean setPositionLeft(float left) 
	{
		frame.setLeft( left );
		return true;
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#getText()
	 */
	/*public VirtualStyledText getText() 
	{
		return new VirtualStyledText( frame.get_TextFrame().get_TextRange().get_Text());
	}*/

	/**
	 * @return Returns the real frame.
	 */
	public Shape getFrame() 
	{
		return frame;
	}

	public String toString()
	{
		StringBuffer b = new StringBuffer();
		b.append("MsTextFrame ");
		b.append("width: ");
		b.append(getWidth());

		b.append("height: ");
		b.append(getHeight());

		b.append("top: ");
		b.append(getPositionTop());

		b.append("left: ");
		b.append(getPositionLeft());
		return b.toString();
	}

	public boolean setPrintable(boolean printable)
	{
		throw new UnsupportedOperationException("Method is not implemented");
	}


	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#getFrameContentLocation()
	 */
	public TextContentLocation getFrameContentLocation() 
	{
		return new MsOfficeTextContentLocation(frame.getTextFrame().getTextRange());
	}


	/* (non-Javadoc)
	 * @see de.tarent.documents.document.TextFrame#setSize(int, int)
	 */
	public boolean setSize(float width, float height)
	{
		frame.setWidth((float)Util.convertNanoMetersToPica( width ));
		frame.setHeight((float)Util.convertNanoMetersToPica( height) );    
		return true;
	}



	public boolean setPosition(float top, float left)
	{
		frame.setTop((float)Util.convertNanoMetersToPica( top) );
		frame.setLeft((float)Util.convertNanoMetersToPica( left) );
		return true;
	}

	public TextContentLocation getTextContentLocation()
	{
		return new MsOfficeTextContentLocation(this, false);
	}

	public TextContentLocation getTextContentLocationAfter()
	{
		return new MsOfficeTextContentLocation(this, true);
	}


	public boolean remove()
	{
		frame.delete();
		return true;
	}
}
