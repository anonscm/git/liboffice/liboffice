/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.03.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.document.TextFont;
import org.evolvis.liboffice.msword11.Font;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextFont;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextFont implements TextFont
{
  private Font m_oFont = null;
  
  public MsOfficeTextFont(Font font)
  {
    m_oFont = font;
  }
  
  public String getFontName()
  {
    return m_oFont.getName();
  }

  public double getFontSize()
  {
    return m_oFont.getSize();
  }

  public boolean isBold()
  {
    return (m_oFont.getBold() != 0);
  }

  public boolean isItalic()
  {
    return (m_oFont.getItalic() != 0);
  }

  public boolean isUnderline()
  {
    return (m_oFont.getUnderline() != 0);
  }

    public VirtualTextFont parseFont()
    {
        VirtualTextFont vfont = new VirtualTextFont();
        vfont.setFontName(getFontName());
        vfont.setFontHeight(getFontSize());
        vfont.setBold(isBold());
        vfont.setItalic(isItalic());
        vfont.setUnderline(isUnderline());    
        return vfont;
    }

}
