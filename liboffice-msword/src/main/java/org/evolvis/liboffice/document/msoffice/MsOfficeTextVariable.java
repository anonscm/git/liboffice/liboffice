package org.evolvis.liboffice.document.msoffice;


import org.evolvis.liboffice.document.TextVariable;
import org.evolvis.liboffice.msword11.Variable;

public class MsOfficeTextVariable  implements TextVariable{
	
	private Variable variable;
	private MsOfficeTextDocument document;
	
	public MsOfficeTextVariable(MsOfficeTextDocument document, Variable variable){
		this.document = document;
		this.variable = variable;
	}

	public void delete() {
		variable.delete();
		
	}

	public String getName() {
		return variable.getName();
	}

	public String getValue() {
		return variable.getValue();
	}

	public void setValue(String value) {
		variable.setValue(value);
		
	}


}
