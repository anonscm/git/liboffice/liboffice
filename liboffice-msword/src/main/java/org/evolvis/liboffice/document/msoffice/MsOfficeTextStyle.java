/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 29.03.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.document.TextFont;
import org.evolvis.liboffice.document.TextStyle;
import org.evolvis.liboffice.msword11.Font;
import org.evolvis.liboffice.msword11.Style;
import org.evolvis.liboffice.msword11.WdStyleType;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyleType;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextStyle implements TextStyle
{
  private Style m_oStyle = null;
  
  public MsOfficeTextStyle(Style style)
  {
    m_oStyle = style;
  }
  
  public MsOfficeTextStyle(Style style, Object type)
  {
    m_oStyle = style;
  }
  
  public Style getStyle()
  {
    return m_oStyle;
  }

  public String getStyleName()
  {
    return m_oStyle.getNameLocal();
  }

  public TextFont getTextFont()
  {
    Font font = m_oStyle.getFont();
    if (font != null)
    {
      return new MsOfficeTextFont(font);
    }
    return null;
  }

  public Object getStyleType()
  {
    int type = m_oStyle.getType();
    if      (type == WdStyleType.wdStyleTypeParagraph) return TextStyle.STYLE_PARAGRAPH;
    else if (type == WdStyleType.wdStyleTypeCharacter) return TextStyle.STYLE_CHARACTER;
    else return null;
  }

    public VirtualTextStyle parseStyle()
    {
        VirtualTextStyleType vstyletype = null;
        if (TextStyle.STYLE_PARAGRAPH.equals(getStyleType())) vstyletype = VirtualTextStyleType.STYLE_PARAGRAPH;
        else if (TextStyle.STYLE_CHARACTER.equals(getStyleType())) vstyletype = VirtualTextStyleType.STYLE_CHARACTER;
        
        VirtualTextStyle vstyle = new VirtualTextStyle(getStyleName(), vstyletype);

        TextFont font = getTextFont();
        if (font != null)
        {
          vstyle.setFont(font.parseFont());
        }
        return vstyle;
    }
  
}
