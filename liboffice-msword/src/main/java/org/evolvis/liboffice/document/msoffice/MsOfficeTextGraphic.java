/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 17.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.document.TextGraphic;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocation;
import org.evolvis.liboffice.msword11.LinkFormat;
import org.evolvis.liboffice.msword11.PictureFormat;
import org.evolvis.liboffice.msword11.Shape;
import org.evolvis.liboffice.msword11.WdLinkType;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextGraphic implements TextGraphic
{
	private Shape m_oShape = null;
	public MsOfficeTextGraphic(Shape graphic)
	{
		this.m_oShape = graphic;
	}
	
	public Shape getShape()
	{
		return m_oShape;
	}


  public float getWidth()
  {
    return m_oShape.getWidth();
  }


  public float getHeight()
  {
    return m_oShape.getHeight();
  }


  //TODO: testen!
  public String getURL()
  {
    LinkFormat linkFormat = m_oShape.getLinkFormat();
    if (linkFormat.getType() == WdLinkType.wdLinkTypePicture)
    {
      return linkFormat.getSourceFullName();
    }
    
//    if (m_oShape.get_Type() == WdPictureLinkType.wdLinkDataInDoc)
//    {
//    }
//    else if (m_oShape.get_Type() == WdPictureLinkType.wdLinkDataOnDisk)
//    {
//    }
//    else if (m_oShape.get_Type() == WdPictureLinkType.wdLinkNone)
//    {
//    }
    
    return null;
  }


  public boolean isPrintable()
  {
    return true;
  }


  public boolean setPrintable(boolean printable)
  {
      throw new UnsupportedOperationException("Method is not implemented");
  }

  public String getName()
  {
    return m_oShape.getName();
  }
	
  
  public boolean setContrast(double contrast)
  {
    PictureFormat pictureFormat = m_oShape.getPictureFormat();
    if (pictureFormat != null)
    {
      pictureFormat.setContrast((float)contrast);
      return true;
    }
    return false;
  }
  
  public boolean setBrightness(double brightness)
  {
    PictureFormat pictureFormat = m_oShape.getPictureFormat();
    if (pictureFormat != null)
    {
      pictureFormat.setBrightness((float)brightness);
      return true;
    }
    return false;
  }
  
  public double getContrast()
  {
    PictureFormat pictureFormat = m_oShape.getPictureFormat();
    if (pictureFormat != null) return ((TextGraphic) pictureFormat).getContrast();
    else return Double.NaN;
  }
  
  public double getBrightness()
  {
    PictureFormat pictureFormat = m_oShape.getPictureFormat();
    if (pictureFormat != null) return pictureFormat.getBrightness();
    else return Double.NaN;
  }

  public TextContentLocation getTextContentLocation()
  {
    return new MsOfficeTextContentLocation( this, false);
  }
  
  public TextContentLocation getTextContentLocationAfter()
  {
    return new MsOfficeTextContentLocation( this, true);
  }

  public boolean remove()
  {
     m_oShape.delete();
     return true;
  }
    
}
