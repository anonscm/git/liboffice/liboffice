/**
 * 
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.document.Span;
import org.evolvis.liboffice.document.TextBookmark;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocation;
import org.evolvis.liboffice.msword11.Bookmark;
import org.evolvis.liboffice.msword11.Document;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.WdCollapseDirection;

import com.jacob.com.Variant;


/**
 * This class represents a bookmark in MS-Office
 * 
 * @author Andrei Boulgakov tarent GmbH Bonn
 *
 */
public class MsOfficeTextBookmark extends MsOfficeTextComponent implements TextBookmark{
	
	Bookmark bookmark;
	MsOfficeTextDocument document;

	
	public static Range getRange(MsOfficeTextDocument document, Bookmark bookmark)
	{
		bookmark.select();
		Range range = document.getDocument().getApplication().getSelection().getRange();
		document.getDocument().getApplication().getSelection().collapse();
		return range;    
	}
	
	public MsOfficeTextBookmark(MsOfficeTextDocument document , Bookmark bookmark)
	{	
		super(getRange(document, bookmark));
		this.bookmark = bookmark;
		this.document=document;
		
	}	
	
	public TextBookmark copy(String newName){
		return new MsOfficeTextBookmark(document, bookmark.copy(newName));
	}
	
	public String getName() {
		return bookmark.getName();
	}

	public TextContentLocation getTextContentLocation() {
		return new MsOfficeTextContentLocation(this, false);
	}

	public TextContentLocation getTextContentLocationAfter() {
		return new MsOfficeTextContentLocation(this, true);
	}

	public boolean remove() {
		bookmark.delete();	
		return true;
	}

	public int getEnd() {
		return bookmark.getEnd();
	}

	public int getStart() {
		return bookmark.getStart();
	}

	public boolean getEmpty() {
		
		return bookmark.getEmpty();
	}

	public void select() {
		
		bookmark.select();
		
	}
	
	public void collapse() {
		
//		bookmark.getApplication().getSelection().collapse();
//		bookmark.getRange().select();
//		document.getDocument().getApplication().getSystem().setCursor(getEnd());
//		bookmark.getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
		
//		Range range = document.getDocument().getApplication().getSelection().getRange();

		document.getDocument().getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
		
	}

	public void setStart(int position) {
		Document doc = document.getDocument();
		doc.activate();
		bookmark.select();
		doc.getApplication().getSelection().setStart(position);	
		
	}
	
	public void setEnd(int position) {
		Document doc = document.getDocument();
		doc.activate();
		bookmark.select();
		doc.getApplication().getSelection().setEnd(position);		
	}
	
	public String getText(){
		Document doc = document.getDocument();
		doc.activate();
		if(!bookmark.getEmpty()){
			bookmark.select();
			String result = doc.getApplication().getSelection().getText();
			collapse();
			return result;
		}
		return null;
	}
	
	public void setText(String content){
		Document doc = document.getDocument();
		doc.activate();
		Range range = getRange();
		String name = getName();
		range.setText(content);
		bookmark = doc.getBookmarks().add(name, new Variant(range));
		
	}
	
	
	public Bookmark getBookmark(){
		return bookmark;
	}

	public void setCursorAtEnd() {
		bookmark.select();
		document.getDocument().getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
		
	}

	public void setCursorAtStart() {
		bookmark.select();
		document.getDocument().getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseStart));
		
	}

	public void setName(String newName) {
		Document doc = document.getDocument();
		doc.activate();
		Range range = getRange();
		String oldText = getText();
		remove();
		bookmark = doc.getBookmarks().add(newName, new Variant(range));
	}

	public void insertTextAfter(String text) {
		Document doc = document.getDocument();
		doc.activate();
		Range range = getRange();
		range.insertAfter(text);
		
	}

	public void insertTextBefore(String text) {
		Document doc = document.getDocument();
		doc.activate();
		Range range = getRange();
		range.insertBefore(text);		
	}

	public Span getSpan() {
		
		return new MsOfficeSpan(getRange());
	}	
}
