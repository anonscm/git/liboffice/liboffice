/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.document.msoffice;

import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocation;
import org.evolvis.liboffice.msword11.Field;
import org.evolvis.liboffice.msword11.Range;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextMarker extends MsOfficeTextComponent implements TextMarker
{
	private Field m_oField;
	
	public final static String CONST_FIELD_IDENTIFIER1 = "SERIENDRUCKFELD";
	public final static String CONST_FIELD_IDENTIFIER2 = "MERGEFIELD";
	public final static String CONST_MARKER_HINT_SEPARATOR = "-";
	
	private static Range getRange(MsOfficeTextDocument doc, Field field)
	{
		field.select();
		Range range = doc.getDocument().getApplication().getSelection().getRange();
		doc.getDocument().getApplication().getSelection().collapse();
		return range;    
	}
	
	public MsOfficeTextMarker(MsOfficeTextDocument doc, Field field)
	{
		super(getRange(doc, field));
		m_oField = field;
	}
	
	public Field getField()
	{
		return m_oField;
	}
	
	
	public String getName()
	{
		if (getField() != null)
		{
			return (getMarkerNameHint(getField()))[0];
		}
		return null;
	}
	
	
	public String getHint()
	{
		if (getField() != null)
		{
			return (getMarkerNameHint(getField()))[1];
		}
		return null;
	}
	
	
	private String[] getMarkerNameHint(Field field)
	{
		if (field.getType() == 59)
		{                
			String fieldText = field.getCode().getText();        
			
			String keyWord = null;
			if (fieldText.indexOf(CONST_FIELD_IDENTIFIER2) != -1)
			{
				// Keyword for current MS-Words
				keyWord = " "+CONST_FIELD_IDENTIFIER2;
			}
			else if (fieldText.indexOf(CONST_FIELD_IDENTIFIER1) != -1)
			{
				// Keyword for MS-Word 97
				keyWord = " "+CONST_FIELD_IDENTIFIER1;
			}
			else
			{
				//logger.warn("findMarker(): unbekannte Version von Word.");
				return new String[] {null, null};
			}
			
			String fieldName = null;
			if (fieldText.startsWith(keyWord))
			{
				fieldText = fieldText.substring(keyWord.length() + 1);
				int endIndex = fieldText.indexOf("\\*");
				if (endIndex != -1)
				{
					fieldName = fieldText.substring(0, endIndex - 1);
				}          
				else
				{
					fieldName = fieldText.trim();
				}          
			}
			
			if (fieldName != null)
			{
				if (fieldName.indexOf('-') != -1)
				{
					String[] nameParts = fieldName.split(CONST_MARKER_HINT_SEPARATOR);
					if (nameParts != null)
					{
						if (nameParts.length == 2)
						{
							return nameParts;
						}
						else
						{
							String[] nameHint = new String[2];
							if (nameParts.length > 1)
							{
								nameHint[0] = nameParts[0];
								nameHint[1] = nameParts[1];
							}
							else
							{
								nameHint[0] = nameParts[0];
								nameHint[1] = null;                
							}
							return nameHint;
						}
					}
				}
				else return new String[]{fieldName, null}; 
			}
		}
		return new String[] {null, null};
	}
	
	
	
	
	
	
	public TextContentLocation getTextContentLocation()
	{
		return new MsOfficeTextContentLocation( this, false);
	}
	
	public TextContentLocation getTextContentLocationAfter()
	{
		return new MsOfficeTextContentLocation( this, true);
	}
	
	public boolean remove()
	{
		m_oField.delete();
		return true;
	}  
}
