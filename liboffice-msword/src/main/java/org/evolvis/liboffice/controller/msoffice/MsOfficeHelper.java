/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.controller.msoffice;

import java.util.Iterator;
import java.util.List;

import org.evolvis.liboffice.document.Selectable;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualPageBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MsOfficeHelper
{
	public static boolean insertObject(TextDocument document, Object comp)
	{
		if (comp instanceof VirtualTextComponent)
		{
			return insertVirtualTextComponent(document, (VirtualTextComponent)comp);
		}
		else if (comp instanceof List)
		{
			List l        =  (List)comp;
			Iterator it   = l.iterator();
			while (it.hasNext())
			{
				Object o = it.next();
				if (o instanceof Selectable)
				{
					Selectable sel = (Selectable) comp;
					sel.select();
					sel.copy();
					((MsOfficeTextDocument)document).getDocument().activate();
					document.paste();
				}
				else throw new RuntimeException("OOoOfficeControllerToolkit.getRangeFromList() listitem is not instance of Selectable");
			}
			return true;
		}
		if (comp instanceof Selectable)
		{
			Selectable sel = (Selectable) comp;
			sel.select();
			sel.copy();
			((MsOfficeTextDocument)document).getDocument().activate();
			document.paste();
			return true;
		}
		return false;
	}

	public static boolean insertVirtualTextComponent(TextDocument document, VirtualTextComponent componenttoinsert)
	{
		if (componenttoinsert instanceof VirtualParagraph)
		{
			document.insertText(new VirtualStyledText((VirtualParagraph)componenttoinsert));
			return true;
		}
		else if (componenttoinsert instanceof VirtualPageBreak)
		{
			document.insertPageBreak();
			return true;
		}
		if (componenttoinsert instanceof VirtualStyledText)
		{
			document.insertText((VirtualStyledText)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextTable)
		{
			document.insertTable( (VirtualTextTable)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextDocument)
		{
			insertDocument(document, (VirtualTextDocument)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextPortionMarker)
		{
			document.insertMarker((VirtualTextPortionMarker)componenttoinsert);
			return true;
		}
		return false;
	}

	public static boolean insertDocument(TextDocument document, VirtualTextDocument documenttoinsert)
	{
		boolean result = true;
		for(int i=0; i<(documenttoinsert.getNumberOfTextComponents()); i++)
		{
			VirtualTextComponent vtc = documenttoinsert.getTextComponent(i);
			if (vtc instanceof VirtualParagraph)
			{
				boolean ret = document.insertText(new VirtualStyledText((VirtualParagraph)vtc));
				if (! ret) result = false;
			}
			else if (vtc instanceof VirtualTextTable)
			{
				TextTable tab = document.insertTable((VirtualTextTable)vtc);
				if (null == tab) result = false;
			}
		}
		return result;
	}
}
