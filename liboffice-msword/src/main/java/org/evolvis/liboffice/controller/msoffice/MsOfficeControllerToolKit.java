/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.controller.msoffice;

import java.util.Iterator;
import java.util.List;

import org.evolvis.liboffice.controller.OfficeControllerToolKit;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.descriptors.StyledParagraphDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextTable;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocator;
import org.evolvis.liboffice.msword11.Application;
import org.evolvis.liboffice.msword11.Columns;
import org.evolvis.liboffice.msword11.Field;
import org.evolvis.liboffice.msword11.Fields;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Rows;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.msword11.WdBorderType;
import org.evolvis.liboffice.msword11.WdCollapseDirection;
import org.evolvis.liboffice.virtualdocument.VirtualDocumentInformation;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualPageBreak;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;

import com.jacob.com.Variant;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn, Steffen Kriese, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MsOfficeControllerToolKit extends MainOfficeFactoryFetcherAdapter implements OfficeControllerToolKit
{
	public MsOfficeControllerToolKit()
	{
	}

	public int getNumberOfMarkerOccurrences(TextDocument document, String markername, String hint)
	{
		int counter = 0;

		String text = markername;
		if (hint != null)
		{
			text += "-" + hint;
		}

		Fields fields = ((MsOfficeTextDocument)document).getDocument().getFields();
		for(int i=1; i<=(fields.getCount()); i++)
		{
			Field field = fields.item(i);
			/*if (field.get_Type() == 59)
      {
        logger.debug("feld data " + field.get_Data());
        if (text.equals(field.get_Data()))
        {
          counter++;
        }
      }*/
			if (MsOfficeTextContentLocator.isMarker(field, markername, hint))
			{
				counter++;
			}
		}

//		for(int i=1; i<(((MsTextDocument)document).getDocument().get_Bookmarks().get_Count() + 1); i++)      
//		{
//		Bookmark bookmark = ((MsTextDocument)document).getDocument().get_Bookmarks().Item(new Integer(i));
//		if (markername.equalsIgnoreCase(bookmark.get_Name())) counter++;
//		}
		return counter;
	}

	public int getNumberOfMarkers(TextDocument document)
	{
		int numFields = 0; 
		Fields fields = ((MsOfficeTextDocument)document).getDocument().getFields();          
		for(int i=1; i<=(fields.getCount()); i++)
		{
			Field field = fields.item(i);
			if (field.getType() == 59)
			{
				numFields++;
			}
		}
		return numFields;

		//return ((MsTextDocument)document).getDocument().get_Bookmarks().get_Count();          
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeControllerToolKit#insertDocument(de.tarent.documents.document.TextDocument, de.tarent.documents.virtualdocument.VirtualTextDocument)
	 */
	public boolean insertDocument(TextDocument document, VirtualTextDocument documenttoinsert)
	{
		boolean result = true;
		for(int i=0; i<(documenttoinsert.getNumberOfTextComponents()); i++)
		{
			VirtualTextComponent vtc = documenttoinsert.getTextComponent(i);
			if (vtc instanceof VirtualParagraph)
			{
				boolean ret = document.insertText(new VirtualStyledText((VirtualParagraph)vtc));
				if (! ret) result = false;
			}
			else if (vtc instanceof VirtualTextTable)
			{
				TextTable tab = document.insertTable((VirtualTextTable)vtc);
				if (null == tab) result = false;
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeControllerToolKit#insertComponent(de.tarent.documents.document.TextDocument, de.tarent.documents.virtualdocument.VirtualTextComponent)
	 */
	public boolean insertVirtualTextComponent(TextDocument document, VirtualTextComponent componenttoinsert)
	{
		if (componenttoinsert instanceof VirtualParagraph)
		{
			document.insertText(new VirtualStyledText((VirtualParagraph)componenttoinsert));
			return true;
		}
		else if (componenttoinsert instanceof VirtualPageBreak)
		{
			document.insertPageBreak();
			return true;
		}
		if (componenttoinsert instanceof VirtualStyledText)
		{
			document.insertText((VirtualStyledText)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextTable)
		{
			document.insertTable( (VirtualTextTable)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextDocument)
		{
			insertDocument(document, (VirtualTextDocument)componenttoinsert);
			return true;
		}
		else if (componenttoinsert instanceof VirtualTextPortionMarker)
		{
			document.insertMarker((VirtualTextPortionMarker)componenttoinsert);
			return true;
		}
		return false;
	}


	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeControllerToolKit#insertText(de.tarent.documents.document.TextDocument, java.lang.String)
	 */
	public boolean insertText(TextDocument document, String text)
	{
		return document.insertText(new VirtualStyledText(text));
	}




	public boolean jumpToMarker(TextDocument document, String markername)
	{
		TextDocumentComponent marker = document.locateTextDocumentComponent( new NameHintMarkerDescriptor(markername));

		if (marker != null)
		{
			document.jumpToLocation(((TextMarker)marker).getTextContentLocation() );
			return true;
		}
		return false;
	}

	public boolean jumpToText(TextDocument document, String text)
	{
		TextDocumentComponent portion = document.locateTextDocumentComponent( new StyledParagraphDescriptor(text,null));
		if (portion != null)
		{
			document.jumpToLocation(portion.getTextContentLocation() );
			return true;
		}
		return false;
	}

	public boolean jumpToTableCell(TextDocument document, TextTable table, int row, int column)
	{
		document.jumpToLocation(table.getTextContentLocation());
		Table mstable = ((MsOfficeTextTable)table).getTable();
		Range range   = mstable.getRows().item(row+1).getCells().item(column+1).getRange();
		positionCursorAtRangeStart(mstable.getApplication(), range);
		return true;
	}


	private void positionCursorAtRangeStart(Application application, Range range)
	{
		if (range != null)
		{
			if (application != null)
			{
				range.select();
				application.getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseStart));
			}
		}
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeControllerToolKit#jumpToTable(de.tarent.documents.document.TextDocument, de.tarent.documents.document.TextTable)
	 */
	public boolean jumpToTable(TextDocument document, TextTable table)
	{
		return document.jumpToLocation(table.getTextContentLocation());
	}

	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeControllerToolKit#removeAllMarkers(de.tarent.documents.document.TextDocument)
	 */
	public boolean removeAllMarkers(TextDocument document)
	{
		Fields fields = ((MsOfficeTextDocument)document).getDocument().getFields();
		int amountFields = fields.getCount();
		for(int i=amountFields; i >= 1; i--)
		{
			Field field = fields.item(i);
			if (field.getType() == 59)
			{
				field.delete();
			}
		}
		return true;
	}

	public boolean removeMarker(TextDocument document, String markername, String option)
	{
		return document.removeMarker(new VirtualTextPortionMarker(markername, option));
	}

	public int getVersion()
	{
		return 0;
	}


	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeControllerToolKit#setCrossTableBorders(de.tarent.documents.document.TextTable)
	 */
	public boolean setCrossTableBorders(TextTable texttable)
	{
		Table table = ((MsOfficeTextTable)texttable).getTable();
		if (table != null)
		{
			Rows rows = table.getRows();
			if (rows.getCount() < 1) return false;
			rows.getFirst().getBorders().item(WdBorderType.wdBorderBottom).setVisible(true);
			Columns columns = table.getColumns();
			int columnCount = columns.getCount();
			for (int column = 1; column < columnCount; column++)
			{
				columns.item(column + 1).getBorders().item(WdBorderType.wdBorderLeft).setVisible(true);
			}
			return true;
		}    
		return false;
	}


	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeControllerToolKit#getDocumentInformation(de.tarent.documents.document.TextDocument, java.lang.String)
	 */
	public String getDocumentInformation(TextDocument document, String key)
	{
		List infos = document.getDocumentInformations();
		for(int i=0; i<(infos.size()); i++)
		{
			VirtualDocumentInformation info = (VirtualDocumentInformation)(infos.get(i));
			if (key.equals(info.getKey())) return info.getValue();
		}
		return null;
	}

	public boolean closeFirstDocument()
	{
		try
		{
			List docs = getMainOfficeFactory().getControllerFactory().getOfficeController().getOpenDocuments();

			if(docs.size() > 0)
			{
				MsOfficeTextDocument doc = (MsOfficeTextDocument) docs.get(docs.size()-1);
				doc.close(false);
			}
			return true;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return false;
		}
	}

	public boolean closeAllDocuments()
	{
		try
		{
			List docs = getMainOfficeFactory().getControllerFactory().getOfficeController().getOpenDocuments();

			Iterator it = docs.iterator();

			while(it.hasNext())
			{
				MsOfficeTextDocument doc = (MsOfficeTextDocument) it.next();
				doc.close(true);
			}
			return true;
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
			return false;
		}
	}
}
