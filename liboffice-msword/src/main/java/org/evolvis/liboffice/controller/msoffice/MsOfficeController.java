/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.controller.msoffice;


import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.liboffice.connection.OfficeConnection;
import org.evolvis.liboffice.connection.msoffice.MsOfficeConnection;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.msword11.Application;
import org.evolvis.liboffice.msword11.Document;
import org.evolvis.liboffice.msword11.Documents;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.officefileformats.OfficeFileFormatDOC;

import com.jacob.com.ComException;
import com.jacob.com.Variant;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn, Steffen Kriese, tarent GmbH Bonn
 *
 */

public class MsOfficeController extends MainOfficeFactoryFetcherAdapter implements OfficeController
{
	private MsOfficeConnection m_oMsOfficeConnection;
	private List m_oSupportedFileFormats = null; 
	
	private final static Logger logger = Logger.getLogger(MsOfficeController.class.getName());
	
	//TODO: Variable to solve the two documents "problem". 
	private static boolean starter = true;
	
	
	public MsOfficeController(MsOfficeConnection connection)
	{
//		System.out.println("new cotroller created");
		m_oMsOfficeConnection = connection;
	}
	
	
	
	/**
	 * Diese Methode pr&uml;ft, ob die &uml;bergebene {@link OfficeConnection}eine {@link MsOfficeConnection}ist und gibt sie
	 * als solche zur&uml;ck.
	 * 
	 * @param connection
	 *            zu &uml;berpr&uml;fende {@link OfficeConnection}.
	 * @return der Parameter als {@link MsOfficeConnection}.
	 * @throws IllegalArgumentException
	 *             wenn das &uml;bergebene Objekt keine {@link MsOfficeConnection}ist.
	 */
	private MsOfficeConnection refineConnection(OfficeConnection connection) 
	{
		if (connection instanceof MsOfficeConnection)
			return (MsOfficeConnection) connection;
		else
			throw new IllegalArgumentException("MsOfficeController ben�tigt eine MsOfficeConnection");
	}
	
	
	
	
	public TextDocument loadDocument(String pFilename)
	{
		return loadDocument(pFilename, true);
	}
	
	public TextDocument loadDocument(String pFilename, boolean pVisible)
	{
		MsOfficeConnection msConnection = refineConnection(m_oMsOfficeConnection);
		if (!msConnection.isValid()) return null;
		try 
		{
			Application application = msConnection.getApplication();
			Document newDocument = application.getDocuments().add(new Variant(pFilename), new Variant(false), new Variant(null), new Variant(pVisible));
			
			if (newDocument.m_pDispatch == 0) 
			{
				logger.warning("Fehler beim Laden des Dokuments '" + pFilename + "': IDispatch 0 zur�ckerhalten");
				return null;
			}
			return new MsOfficeTextDocument(msConnection, newDocument);
		} 
		catch (ComException e) 
		{
			logger.log(Level.SEVERE, "Error", e);
			logger.warning("Fehler beim Laden eines Dokuments '" + pFilename + '\'');
			return null;
		} /*catch (NoOfficeException e) {
			logger.error("Error", e );
			logger.warn("Fehler beim Schliessen des Starterdokuments von '" + pFilename + '\'');
			return null;
		}*/
	}
	
	public TextDocument openDocument(String pFilename, boolean pVisible, boolean readOnly)
	{
		MsOfficeConnection msConnection = refineConnection(m_oMsOfficeConnection);
		if (!msConnection.isValid()) return null;
		try 
		{
			Application application = msConnection.getApplication();
//			//TODO: TWO documents problem NOT SOLVED YET
//			if(starter){
//				application.getActiveDocument().close();
//				starter=false;
//			}
			
			if(!pVisible)
				application.setVisible(pVisible);
			
			Document newDocument = application.getDocuments().open(new Variant(pFilename), new Variant(false), new Variant(readOnly));
			if (newDocument.m_pDispatch == 0) 
			{
				logger.warning("Fehler beim Laden des Dokuments '" + pFilename + "': IDispatch 0 returned.");
				return null;
			}
			return new MsOfficeTextDocument(msConnection, newDocument);
		} 
		catch (ComException e) 
		{
			logger.log(Level.SEVERE, "Error", e );
			logger.warning("Fehler beim Laden eines Dokuments '" + pFilename + '\'');
			return null;
		}
	}
	
	public TextDocument loadDocumentAsCopy(String pFilename)
	{
		return loadDocumentAsCopy(pFilename, true);
	}
	
	
	
	public TextDocument loadDocumentAsCopy(String filename, boolean pVisible)
	{
		//m_oMsOfficeConnection = new MsOfficeConnection((MsMainOfficeFactory)this.getMainOfficeFactory());
		MsOfficeConnection msConnection = refineConnection(m_oMsOfficeConnection);
		if (!msConnection.isValid()) return null;
		try 
		{
			Application application = msConnection.getApplication();
			application.activate();
			//TODO: TWO documents problem NOT SOLVED YET
			if(starter){
				application.getActiveDocument().close();
				starter=false;
			}
			application.setVisible(pVisible);
			
			Document newDocument = application.getDocuments().add(new Variant(filename), new Variant(true));

			if (newDocument.m_pDispatch == 0) 
			{
				return null;
			}

			String tempDir = System.getProperty("java.io.tmpdir");
			String id = String.valueOf(new Date().getTime());
			String tempFile = tempDir + File.separator + "template_" + id + ".doc";

			newDocument.saveAs(new Variant(tempFile));
			newDocument.close();
			newDocument = application.getDocuments().add(new Variant(tempFile));
			if (newDocument.m_pDispatch == 0) 
			{
				return null;
			}

			new File(tempFile).delete();

			return new MsOfficeTextDocument(msConnection, newDocument);
		} 
		catch (ComException e) 
		{
			logger.log(Level.SEVERE, "Error", e );
			return null;
		}
	}
	
	public TextDocument newDocument() {
		
		return newDocument(true);
	}
	
	public TextDocument newDocument(boolean visible)
	{
		MsOfficeConnection msConnection = refineConnection(m_oMsOfficeConnection);
		if (!msConnection.isValid()) return null;
		try 
		{
			Application application = msConnection.getApplication();
			//TODO: TWO documents problem NOT SOLVED YET
			if(starter){
				application.getActiveDocument().close();
				starter=false;
			}
			application.setVisible(visible);
			Document newDocument = application.getDocuments().add();
			if (newDocument.m_pDispatch == 0) 
			{
				return null;
			}
			return new MsOfficeTextDocument(msConnection, newDocument);
		} 
		catch (ComException e) 
		{
			logger.log(Level.SEVERE, "Error", e );
			return null;
		}
	}
	
	
	
	
	public List getSupportedFileFormats()
	{
		if (m_oSupportedFileFormats == null)
		{
			m_oSupportedFileFormats = new ArrayList();
			m_oSupportedFileFormats.add(new OfficeFileFormatDOC());
		}
		return m_oSupportedFileFormats;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeController#isFileFormatSupported(java.lang.Object)
	 */
	public boolean isFileFormatSupported(Object key)
	{
		return (getFileFormat(key) != null);
	}
	
	public boolean isFileFormatSupported(OfficeFileFormat pFileFormat)
	{
		return isFileFormatSupported(pFileFormat.getKey());
	}
	
	public boolean isFileFormatSupported(String pKey)
	{
		return isFileFormatSupported(pKey);
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeController#getFileFormat(java.lang.Object)
	 */
	public OfficeFileFormat getFileFormat(Object pKey)
	{
		if(pKey instanceof String)
		{
			return getFileFormat((String)pKey);
		}
		return null;
	}
	
	public OfficeFileFormat getFileFormat(String pKey)
	{
		List list = getSupportedFileFormats();
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			OfficeFileFormat format = (OfficeFileFormat)(it.next());
			if (pKey.equals(format.getKey())) return format;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeController#getControllerFactory()
	 */
	public ControllerFactory getControllerFactory()
	{
		return getMainOfficeFactory().getControllerFactory();
	}
	
	
	
	public TextDocument getStarterDocument() throws NoOfficeException 
	{
		return new MsOfficeTextDocument(m_oMsOfficeConnection, m_oMsOfficeConnection.getCurrentDocument());
	}
	
	
	public int getVersion()
	{
		return 1;
	}
	
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeController#cleanUp()
	 */
	public void cleanUp()
	{
		throw new UnsupportedOperationException("Method is not implemented");
	}
	
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.controller.OfficeController#getOpenDocuments()
	 */
	public List getOpenDocuments() throws NoOfficeException
	{
		List documents = new ArrayList();
		Documents wordDocuments = m_oMsOfficeConnection.getApplication().getDocuments();
		for(int i=1; i<=(wordDocuments.getCount()); i++)
		{
			Document wordDocument = wordDocuments.item(new Variant(i));
			documents.add(new MsOfficeTextDocument(m_oMsOfficeConnection, wordDocument));
		}
		return documents;
	}



	public TextDocument getActiveDocument() {
		// TODO Auto-generated method stub
		if(m_oMsOfficeConnection.getApplication().getDocuments().getCount()==0){
			return null;
		}
		Document wordDocument = m_oMsOfficeConnection.getApplication().getActiveDocument();
		TextDocument document = new MsOfficeTextDocument(m_oMsOfficeConnection, wordDocument);
//		MsOfficeConnection msConnection = refineConnection(m_oMsOfficeConnection);
//		msConnection.getApplication().getActiveDocument().close();
//		Application application = msConnection.getApplication();
		//TODO: TWO documents problem NOT SOLVED YET
//		if(starter){
//			m_oMsOfficeConnection.getApplication().getActiveDocument().close();
//			starter=false;
//		}
//		application.setVisible(true);
//		return String.valueOf(m_oMsOfficeConnection.getApplication().getActiveDocument().getName());
		return document;
	}




}
