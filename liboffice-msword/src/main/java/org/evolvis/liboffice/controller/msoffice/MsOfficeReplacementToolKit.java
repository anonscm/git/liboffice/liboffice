/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.controller.msoffice;

import java.util.logging.Logger;

import org.evolvis.liboffice.controller.ComponentReplaceJob;
import org.evolvis.liboffice.controller.ComponentReplaceSet;
import org.evolvis.liboffice.controller.ReplacementToolKit;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.document.Selectable;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextMarker;
import org.evolvis.liboffice.msword11.Field;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Selection;
import org.evolvis.liboffice.msword11.WdCollapseDirection;
import org.evolvis.liboffice.msword11.WdMovementType;
import org.evolvis.liboffice.msword11.WdUnits;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.graphic.VirtualGraphic;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

import com.jacob.com.Variant;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MsOfficeReplacementToolKit implements ReplacementToolKit
{
	
	private final static Logger logger = Logger.getLogger(MsOfficeReplacementToolKit.class.getName());

	public boolean replaceAllMarkersByComponent(TextDocument pDocument, ComponentReplaceSet pReplaceSet)
	{
		for(int i=0; i<(pReplaceSet.getNumberOfJobs()); i++)
		{
			ComponentReplaceJob job = pReplaceSet.getJob(i);

			if (job != null)
			{
				boolean running = true;
				while(running)
				{
					TextMarker marker = (TextMarker)(pDocument.locateTextDocumentComponent( new NameHintMarkerDescriptor(job.getKey())));
					if (marker != null)
					{
						if (job.removeLine())
						{
							Range range = ((MsOfficeTextMarker)marker).getRange();
							range.select();

							Field msField         = ((MsOfficeTextMarker)marker).getField();
							Selection msSelection = msField.getApplication().getSelection();
							msSelection.collapse(new Variant(WdCollapseDirection.wdCollapseStart));
							//msSelection.MoveDown(new Integer(WdUnits.wdLine), new Integer(1), new Integer(WdMovementType.wdExtend));
							//msSelection.TypeBackspace();

							//msSelection.Delete(new Integer(WdUnits.wdCharacter), new Integer( 0 ));
							boolean ret = MsOfficeHelper.insertObject(pDocument, job.getComponent());
							if(! ret) return false;

							msField.select();
							msSelection = msField.getApplication().getSelection();
							msSelection.moveRight(new Variant(WdUnits.wdCharacter), new Variant(1), new Variant(WdMovementType.wdExtend));
							msSelection.typeBackspace();
						}
						else
						{
							new MsOfficeControllerToolKit().jumpToMarker(pDocument, marker.getName());
							boolean ret = MsOfficeHelper.insertObject(pDocument, job.getComponent());
							if(! ret) return false;
							pDocument.removeMarker(marker);                
						}

					} else running = false;
				}
			}          
		}

		return true;
	}

//	public boolean replaceAllMarkersByEntity(TextDocument pDocument, Entity pEntity, boolean pRemoveLine)
//	{
//		boolean running = true;
//
//		while(running)
//		{
//			// Get next text-marker
//			//TextMarker marker = (TextMarker) pDocument.locateTextDocumentComponent(new NameHintMarkerDescriptor(null));
//
//			TextMarker marker = pDocument.getNextTextMarker();
//
//			if(marker != null)
//			{
//				Object replaceObject = null;
//				try
//				{
//					replaceObject = pEntity.getAttribute(marker.getName().trim());
//					
//					// if we do not have replace-data (replaceObject is null), we replace with an empty text.
//					if(replaceObject == null)
//					{
//						logger.warning("No replacement for tag \""+marker.getName().trim()+"\" found. replacing with an empty string.");
//						replaceObject = "";
//					}
//					
//					if(replaceObject instanceof String) replaceObject = new VirtualParagraph((String)replaceObject);
//				}
//				catch(Exception pExcp)
//				{
//					return false;
//				}
//
//
//				if(pRemoveLine)
//				{
//					Range range = ((MsOfficeTextMarker)marker).getRange();
//					range.select();
//					Field msField = ((MsOfficeTextMarker)marker).getField();
//					Selection msSelection = msField.getApplication().getSelection();
//					msSelection.collapse(new Variant(WdCollapseDirection.wdCollapseStart));
//
//					boolean returnCode = MsOfficeHelper.insertObject(pDocument, replaceObject);
//					if(!returnCode) return false;
//
//					msField.select();
//
//					msSelection = msField.getApplication().getSelection();
//					msSelection.moveRight(new Variant(WdUnits.wdCharacter), new Variant(1), new Variant(WdMovementType.wdExtend));
//					msSelection.typeBackspace();
//				}
//				else
//				{	
//					//jumpToMarker(pDocument, marker.getName());
//					pDocument.jumpToLocation(marker.getTextContentLocation());
//					boolean ret = MsOfficeHelper.insertObject(pDocument, replaceObject);
//					if(! ret) return false;
//					pDocument.removeMarker(marker);
//				}
//			}
//			else running = false;
//		}
//		return true;
//	}

//	public boolean replaceAllMarkersByEntityListInSingleDocument(TextDocument pDocument, String pFileName, EntityList pEntityList, boolean pRemoveLine, boolean pPageBreak, Context pProgressStatus)
//	{
//		boolean success = true;
//
//		for(int i=0; i < pEntityList.getSize(); i++)
//		{
//			pProgressStatus.setCurrent(i);
//			pProgressStatus.setActivityDescription(Messages.getFormattedString("GUI_REPLACE_PROGRESS_DIALOG_NOTE_REPLACE", new Integer(i), new Integer(pEntityList.getSize())));
//
//			Entity thisEntity;
//			
//			if(pEntityList.getEntityAt(i) instanceof Entity)
//			{
//				thisEntity = (Entity)pEntityList.getEntityAt(i);
//			}
//			else
//			{
//				logger.warning("Data in EntityList does not implement Entity-Interface.");
//				return false;
//			}
//
//			replaceAllMarkersByEntity(pDocument, thisEntity, pRemoveLine);
//
//			if(pProgressStatus.isCancelled())
//			{
//				success = false;
//				break;
//			}
//
//			if(pEntityList.getSize()-1 > i)
//			{
//				pDocument.insertDocumentAtEnd(pFileName);
//				/*
//				  if(pPageBreak) pDocument.insertDocumentAtStart(pFileName);
//				  else pDocument.insertDocumentAtEnd(pFileName); // TODO test
//				 */
//			}
//		}
//
//		return success;
//	}

//	public boolean replaceAllMarkersByEntityListInLabelDocument(TextDocument pDocument, String pFileName, EntityList pEntityList, boolean pRemoveLine, Context pProgressStatus)
//	{
//		boolean success = true;
//
//		int n=0;
//
//		for(int i=0; i < pEntityList.getSize(); i++)
//		{
//			pProgressStatus.setCurrent(i);
//			pProgressStatus.setActivityDescription(Messages.getFormattedString("GUI_REPLACE_PROGRESS_DIALOG_NOTE_REPLACE", new Integer(i), new Integer(pEntityList.getSize())));
//
//			Entity thisEntity;
//			
//			if(pEntityList.getEntityAt(i) instanceof Entity)
//			{
//				thisEntity = (Entity)pEntityList.getEntityAt(i);
//			}
//			else
//			{
//				logger.warning("Data in EntityList does not implement Entity-Interface.");
//				return false;
//			}
//
//			TextMarker marker = (TextMarker) pDocument.locateTextDocumentComponent(new NameHintMarkerDescriptor(null, String.valueOf(n)));
//
//			while(marker != null)
//			{
//				Object replaceObject = null;
//				try
//				{
//					replaceObject = thisEntity.getAttribute(marker.getName().trim());
//					if(replaceObject instanceof String) replaceObject = new VirtualParagraph((String)replaceObject);
//				}
//				catch(Exception pExcp)
//				{
//					return false;
//				}
//
//				Field msField = null;
//				Selection msSelection = null;
//
//				if(pRemoveLine)
//				{
//					Range range = ((MsOfficeTextMarker)marker).getRange();
//					range.select();
//					msField = ((MsOfficeTextMarker)marker).getField();
//					msSelection = msField.getApplication().getSelection();
//					msSelection.collapse(new Variant(WdCollapseDirection.wdCollapseStart));
//				}
//				else new MsOfficeControllerToolKit().jumpToMarker(pDocument, marker.getName());
//
//
//				if(replaceObject != null)
//				{
//					boolean returnCode = MsOfficeHelper.insertObject(pDocument, replaceObject);
//					if(!returnCode) return false;
//				}
//
//				if(pRemoveLine)
//				{
//					msField.select();
//					msSelection = msField.getApplication().getSelection();
//					msSelection.moveRight(new Variant(WdUnits.wdCharacter), new Variant(1), new Variant(WdMovementType.wdExtend));
//					msSelection.typeBackspace();
//				}
//				else
//				{
//					pDocument.removeMarker(marker); 
//				}
//
//				marker = (TextMarker) pDocument.locateTextDocumentComponent(new NameHintMarkerDescriptor(null, String.valueOf(n)));
//			}
//
//			TextMarker markerHintless = (TextMarker) pDocument.locateTextDocumentComponent(new NameHintMarkerDescriptor(null, ""));
//
//			while(markerHintless != null)
//			{
//				Object replaceObject = null;
//				try
//				{
//					replaceObject = thisEntity.getAttribute(markerHintless.getName().trim());
//					if(replaceObject instanceof String) replaceObject = new VirtualParagraph((String)replaceObject);
//				}
//				catch(Exception pExcp)
//				{
//					return false;
//				}
//
//				Field msField = null;
//				Selection msSelection = null;
//
//				if(pRemoveLine)
//				{
//					Range range = ((MsOfficeTextMarker)markerHintless).getRange();
//					range.select();
//					msField = ((MsOfficeTextMarker)markerHintless).getField();
//					msSelection = msField.getApplication().getSelection();
//					msSelection.collapse(new Variant(WdCollapseDirection.wdCollapseStart));
//				}
//				else new MsOfficeControllerToolKit().jumpToMarker(pDocument, markerHintless.getName());
//
//
//				if(replaceObject != null)
//				{
//					boolean returnCode = MsOfficeHelper.insertObject(pDocument, replaceObject);
//					if(!returnCode) return false;
//				}
//
//				if(pRemoveLine)
//				{
//					msField.select();
//					msSelection = msField.getApplication().getSelection();
//					msSelection.moveRight(new Variant(WdUnits.wdCharacter), new Variant(1), new Variant(WdMovementType.wdExtend));
//					msSelection.typeBackspace();
//				}
//				else
//				{
//					pDocument.removeMarker(markerHintless); 
//				}
//
//				markerHintless = (TextMarker) pDocument.locateTextDocumentComponent(new NameHintMarkerDescriptor(null, ""));
//			}
//
//			//TextMarker anyMarker = (TextMarker) pDocument.locateTextDocumentComponent(new NameHintMarkerDescriptor(null));
//			TextMarker anyMarker = pDocument.getNextTextMarker();
//			n++;
//
//			if(pProgressStatus.isCancelled())
//			{
//				success = false;			  
//				break;
//			}
//
//			// If there are no more markers in document and pEntityList has got more elements 
//			if(anyMarker == null && pEntityList.getSize()-i >= 2)
//			{	
//				// Append template
//				//pDocument.jumpToLocation(pDocument.getEnd());
//				//pDocument.insertDocumentFromFile(pFileName);
//				//pDocument.insertDocumentAtStart(pFileName);
//				pDocument.insertDocumentAtEnd(pFileName);
//				n = 0;
//			}
//		}
//
//		pProgressStatus.setActivityDescription(Messages.getString("GUI_REPLACE_PROGRESS_DIALOG_NOTE_REMOVE_MARKERS"));
//
//		new MsOfficeControllerToolKit().removeAllMarkers(pDocument);
//
//		return success;
//	}

	public boolean replaceFirstMarkerByComponent(TextDocument document,	String markername, VirtualTextComponent component)
	{
		if(component instanceof VirtualTextTable)
		{
			new MsOfficeControllerToolKit().jumpToMarker(document, markername);
			document.insertTable( (VirtualTextTable)component);
			new MsOfficeControllerToolKit().removeMarker(document, markername, null);
			return true;
		}
		else if(component instanceof VirtualStyledText)
		{
			new MsOfficeControllerToolKit().jumpToMarker(document, markername);
			document.insertText((VirtualStyledText) component);
			new MsOfficeControllerToolKit().removeMarker(document, markername, null);
			return true;
		}
		else if(component instanceof VirtualGraphic)
		{
			new MsOfficeControllerToolKit().jumpToMarker(document, markername);
			document.insertGraphic((VirtualGraphic)component);
			new MsOfficeControllerToolKit().removeMarker(document, markername, null);
			return true;
		}
		return false;
	}

	public boolean replaceFirstMarkerByComponent(TextDocument document,	String markername, Selectable s)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
