/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.factory.msoffice;

import org.evolvis.liboffice.connection.msoffice.MsOfficeConnection;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.controller.OfficeControllerToolKit;
import org.evolvis.liboffice.controller.ReplacementToolKit;
import org.evolvis.liboffice.controller.msoffice.MsOfficeController;
import org.evolvis.liboffice.controller.msoffice.MsOfficeControllerToolKit;
import org.evolvis.liboffice.controller.msoffice.MsOfficeReplacementToolKit;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.locator.TextContentLocator;
import org.evolvis.liboffice.locator.msoffice.MsOfficeTextContentLocator;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeControllerFactory extends MainOfficeFactoryFetcherAdapter implements ControllerFactory
{
	private MsOfficeConnection m_oMsOfficeConnection;
	private MsOfficeController m_oMsOfficeController = null; 
	private MsOfficeControllerToolKit m_oMsOfficeControllerToolKit = null;
	private ReplacementToolKit replacementToolKit = null;
	private MsOfficeTextContentLocator m_oMsTextContentLocator = null;
	
	public MsOfficeControllerFactory(MsOfficeConnection connection)
	{
		m_oMsOfficeConnection = connection;
	}
	
	public OfficeController getOfficeController()
	{
		if (m_oMsOfficeController == null)
		{
			m_oMsOfficeController = new MsOfficeController(m_oMsOfficeConnection);
			m_oMsOfficeController.setMainOfficeFactory(getMainOfficeFactory());
		}
		return m_oMsOfficeController;
	}
	
	public OfficeControllerToolKit getOfficeControllerToolKit()
	{
		if (m_oMsOfficeControllerToolKit == null)
		{
			m_oMsOfficeControllerToolKit = new MsOfficeControllerToolKit();
			m_oMsOfficeControllerToolKit.setMainOfficeFactory(getMainOfficeFactory());
		}
		return m_oMsOfficeControllerToolKit;
	}
	
	public ReplacementToolKit getReplacementToolKit()
	{
		if(replacementToolKit == null) replacementToolKit = new MsOfficeReplacementToolKit();
		return replacementToolKit;
	}
	
	public TextContentLocator getTextContentLocator()
	{
		if (m_oMsTextContentLocator == null)
		{
			m_oMsTextContentLocator = new MsOfficeTextContentLocator(m_oMsOfficeConnection);
			m_oMsTextContentLocator.setMainOfficeFactory(getMainOfficeFactory());
		}
		return m_oMsTextContentLocator;
	}
	
}
