/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */
package org.evolvis.liboffice.factory.msoffice;

import java.util.NoSuchElementException;

import org.evolvis.liboffice.msword11.Find;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.WdFindWrap;

/**
 * Dieser Iterator iteriert �ber das Vorkommen einer Variablen in einer
 * Range.
 * 
 * @author mikel
 */
public class VariableRangeIterator implements RangeIterator {
	//
	// Konstruktor
	//
	/**
	 * Dieser Konstruktor �bernimmt Such-Range und Variablenname.
	 * 
	 * @param range Range, in der gesucht werden soll.
	 * @param variableName Name der zu suchenden Variablen.
	 */
	public VariableRangeIterator(Range range, String variableName) {
		super();
		searchRange = range;
		find = range.getFind();
		find.setMatchCase(false);
		find.setMatchWholeWord(false);
		find.setText(MsWordHelper.variable(variableName));
		find.setForward(true);
		find.setWrap(WdFindWrap.wdFindContinue);
	}
	
	//
	// Schnittstelle RangeIterator
	//
	/**
	 * Returns the next Range in the iteration.
	 * 
	 * @return the next Range in the iteration.
	 * @exception NoSuchElementException
	 *                iteration has no more elements.
	 * @see RangeIterator#nextRange()
	 */
	public Range nextRange() {
		if (hasNext()) { // positioniert die Range automatisch
			positionedOnNext = false;
			return searchRange;
		} else
			throw new NoSuchElementException();
	}
	
	//
	// Schnittstelle Iterator
	//
	/**
	 * Removes from the underlying collection the last element returned by the
	 * iterator (optional operation).  This method can be called only once per
	 * call to <tt>next</tt>.  The behavior of an iterator is unspecified if
	 * the underlying collection is modified while the iteration is in
	 * progress in any way other than by calling this method.
	 *
	 * @exception UnsupportedOperationException if the <tt>remove</tt>
	 *        operation is not supported by this Iterator.
	 *
	 * @exception IllegalStateException if the <tt>next</tt> method has not
	 *        yet been called, or the <tt>remove</tt> method has already
	 *        been called after the last call to the <tt>next</tt>
	 *        method.
	 * @see java.util.Iterator#remove()
	 */
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns <tt>true</tt> if the iteration has more elements. (In other
	 * words, returns <tt>true</tt> if <tt>next</tt> would return an element
	 * rather than throwing an exception.)<br>
	 * Positioniert automatisch das interne Range-Objekt auf den n&auml;chsten Fund.
	 *
	 * @return <tt>true</tt> if the iterator has more elements.
	 * @see java.util.Iterator#hasNext()
	 */
	public boolean hasNext() {
		return positionedOnNext ? true : (positionedOnNext = find.execute());
	}
	
	/**
	 * Returns the next element in the iteration.
	 *
	 * @return the next element in the iteration.
	 * @exception NoSuchElementException iteration has no more elements.
	 * @see java.util.Iterator#next()
	 * @see #nextRange()
	 */
	public Object next() {
		return nextRange();
	}
	
	//
	// private Member-Variablen
	//
	/** die Such-Range */
	private Range searchRange = null;
	/** das Find-Objekt zur Variablensuche */
	private Find find = null;
	/** Flag, ob mittels find schon auf den n&auml;chsten Fund positioniert wurde */
	private boolean positionedOnNext = false;
}
