/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */
package org.evolvis.liboffice.factory.msoffice;

import java.util.Iterator;

import org.evolvis.liboffice.msword11.Range;

/**
 * Diese Schnittstelle erweitert den Iterator so, dass ein typisiertes next vorliegt.
 * 
 * @author mikel
 */
public interface RangeIterator extends Iterator {
	/**
	 * Returns the next Range in the iteration.
	 * 
	 * @return the next Range in the iteration.
	 * @exception java.util.NoSuchElementException
	 *                iteration has no more elements.
	 * @see Iterator#next()
	 */
	public Range nextRange();
}
