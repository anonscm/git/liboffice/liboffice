/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.factory.msoffice;

import org.evolvis.liboffice.connection.msoffice.MsOfficeConnection;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.factory.ParserFactory;
import org.evolvis.liboffice.parser.TextDocumentParser;
import org.evolvis.liboffice.parser.TextFrameParser;
import org.evolvis.liboffice.parser.TextGraphicParser;
import org.evolvis.liboffice.parser.TextParagraphParser;
import org.evolvis.liboffice.parser.TextTableCellParser;
import org.evolvis.liboffice.parser.TextTableParser;
import org.evolvis.liboffice.parser.msoffice.MsOfficeTextDocumentParser;
import org.evolvis.liboffice.parser.msoffice.MsOfficeTextFrameParser;
import org.evolvis.liboffice.parser.msoffice.MsOfficeTextGraphicParser;
import org.evolvis.liboffice.parser.msoffice.MsOfficeTextTableParser;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeParserFactory extends MainOfficeFactoryFetcherAdapter implements ParserFactory
{
	public MsOfficeParserFactory(MsOfficeConnection connection)
	{
	}
	
	private MsOfficeTextDocumentParser m_oMsOfficeTextDocumentParser = null;
	public TextDocumentParser getTextDocumentParser()
	{
		if (m_oMsOfficeTextDocumentParser == null)
		{
			m_oMsOfficeTextDocumentParser = new MsOfficeTextDocumentParser();
		}
		return m_oMsOfficeTextDocumentParser;
	}
	
	public TextTableCellParser getTextTableCellParser()
	{
		return ((MsOfficeTextTableParser)getTextTableParser()).getTextTableCellParser();
	}
	
	public TextTableParser getTextTableParser()
	{
		return ((MsOfficeTextDocumentParser)getTextDocumentParser()).getMsTextTableParser();
	}
	
	public TextParagraphParser getTextParagraphParser()
	{
		return ((MsOfficeTextDocumentParser)getTextDocumentParser()).getMsTextParagraphParser();
	}
	
	public TextGraphicParser getTextGraphicParser()
	{
		return new MsOfficeTextGraphicParser();
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.factory.ParserFactory#getTextFrameParser()
	 */
	public TextFrameParser getTextFrameParser() 
	{
		return new MsOfficeTextFrameParser((MsOfficeTextDocumentParser)getTextDocumentParser());
	}
	
}
