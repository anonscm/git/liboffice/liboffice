/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */
package org.evolvis.liboffice.factory.msoffice;

import java.util.NoSuchElementException;

import org.evolvis.liboffice.msword11.Paragraphs;
import org.evolvis.liboffice.msword11.Range;

/**
 * Dieser Iterator iteriert �ber Paragraphen-Ranges, die von Paragraphen mit bestimmten Stil eingerahmt sind, wobei im
 * f�hrenden zus&auml;tzlich ein Signaltext gesucht wird.
 * 
 * @author mikel
 */
public class ParagraphRangeIterator implements RangeIterator {
	//
	// Konstruktor
	//
	/**
	 * Der Konstruktor �bernimmt nur die �bergebenen Parameter.
	 * 
	 * @param paragraphs
	 *            Paragraphen-Sammlung, die zu untersuchen ist.
	 * @param styleName
	 *            der Start- und Endpunktstil.
	 * @param startText
	 *            der Starttext, der zus&auml;tzlich im Startstilabschnitt als Signal gesucht wird.
	 */
	public ParagraphRangeIterator(Paragraphs paragraphs, String styleName, String startText) {
		super();
		this.paragraphs = paragraphs;
		this.styleName = styleName;
		this.startText = startText;
	}
	
	//
	// Schnittstelle RangeIterator
	//
	/**
	 * Returns the next Range in the iteration.
	 * 
	 * @return the next Range in the iteration.
	 * @exception NoSuchElementException
	 *                iteration has no more elements.
	 * @see RangeIterator#nextRange()
	 */
	public Range nextRange() {
		if (hasNext()) { // positioniert die nextParagraph automatisch
			Range result = nextRange;
			nextRange = null;
			return result;
		} else
			throw new NoSuchElementException();
	}
	
	//
	// Schnittstelle Iterator
	//
	/**
	 * Removes from the underlying collection the last element returned by the iterator (optional operation). This
	 * method can be called only once per call to <tt>next</tt>. The behavior of an iterator is unspecified if the
	 * underlying collection is modified while the iteration is in progress in any way other than by calling this
	 * method.
	 * 
	 * @exception UnsupportedOperationException
	 *                if the <tt>remove</tt> operation is not supported by this Iterator.
	 * 
	 * @exception IllegalStateException
	 *                if the <tt>next</tt> method has not yet been called, or the <tt>remove</tt> method has
	 *                already been called after the last call to the <tt>next</tt> method.
	 * @see java.util.Iterator#remove()
	 */
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns <tt>true</tt> if the iteration has more elements. (In other words, returns <tt>true</tt> if <tt>next</tt>
	 * would return an element rather than throwing an exception.) <br>
	 * Positioniert automatisch auf den n&auml;chsten Fund.
	 * 
	 * @return <tt>true</tt> if the iterator has more elements.
	 * @see java.util.Iterator#hasNext()
	 */
	public boolean hasNext() {
		if (nextRange != null) return true;
		int paragraphCount = paragraphs.getCount();
		while (paragraphIndex < paragraphCount) {
			Range paragraphRange = paragraphs.item(++paragraphIndex).getRange();
			
			if (styleName.equals(MsWordHelper.getStyle(paragraphRange).getNameLocal()))
				insideArea = (paragraphRange.getText().indexOf(startText) >= 0);
			else if (insideArea) {
				nextRange = paragraphRange;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns the next element in the iteration.
	 * 
	 * @return the next element in the iteration.
	 * @exception NoSuchElementException
	 *                iteration has no more elements.
	 * @see java.util.Iterator#next()
	 * @see #nextRange()
	 */
	public Object next() {
		return nextRange();
	}
	
	//
	// private Member-Variablen
	//
	/** Kollektion der Paragraphengrundmenge */
	private final Paragraphs paragraphs;
	
	/** Start- und Endpunktstil */
	private final String styleName;
	
	/** Starttext, der zus&auml;tzlich im Startstilabschnitt als Signal gesucht wird */
	private final String startText;
	
	/** Index des zuletzt untersuchten Paragraphen */
	private int paragraphIndex = 0;
	
	/** n&auml;chster Fundparagraph, falls schon positioniert */
	private Range nextRange = null;
	
	/** Flag, ob der zuletzt untersuchte Paragraph im Fundbereich liegt */
	private boolean insideArea = false;
}
