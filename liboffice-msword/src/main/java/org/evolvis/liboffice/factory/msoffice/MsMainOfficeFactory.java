/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.factory.msoffice;

import java.util.Map;

import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;
import org.evolvis.liboffice.connection.OfficeConnection;
import org.evolvis.liboffice.connection.msoffice.MsOfficeConnection;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactory;
import org.evolvis.liboffice.factory.ParserFactory;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MsMainOfficeFactory implements MainOfficeFactory
{
	private MsOfficeConnection officeConnection;
	private MsOfficeControllerFactory controllerFactory = null; 
	private MsOfficeParserFactory parserFactory = null;
	
	/**
	 * Connects to the existing Word process (if any existing) or creates a new one.
	 * @return true winword.exe process were found. False if new process were started.
	 */
	public boolean connect()
	{
		officeConnection = new MsOfficeConnection(this);
		
		return officeConnection.init();
	}
	
	public boolean connect(OfficeConnectionMethod pConnectionMethod, Map pConnectParams) {
		return connect();
	}
	
	public OfficeConnection getConnection()
	{
		return getMsOfficeConnection();
	}
	
	public MsOfficeConnection getMsOfficeConnection()
	{
		return officeConnection;
	}
	
	public ControllerFactory getControllerFactory()
	{
		if (controllerFactory == null)
		{
			controllerFactory = new MsOfficeControllerFactory(getMsOfficeConnection());
			controllerFactory.setMainOfficeFactory(this);
		}
		return controllerFactory;
	}
	
	public ParserFactory getParserFactory()
	{
		if (parserFactory == null)
		{
			parserFactory = new MsOfficeParserFactory(getMsOfficeConnection());
			parserFactory.setMainOfficeFactory(this);
		}
		return parserFactory;
	}
}