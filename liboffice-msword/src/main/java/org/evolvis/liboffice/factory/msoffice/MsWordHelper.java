/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */
package org.evolvis.liboffice.factory.msoffice;

import java.util.List;

import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.msword11.Cell;
import org.evolvis.liboffice.msword11.Columns;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Rows;
import org.evolvis.liboffice.msword11.Selection;
import org.evolvis.liboffice.msword11.Style;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.msword11.WdBorderType;
import org.evolvis.liboffice.msword11.WdCollapseDirection;
import org.evolvis.liboffice.msword11.WdLineStyle;

import com.jacob.com.ComException;
import com.jacob.com.Variant;

/**
 * Diese Klasse stellt statische Hilfsmethoden f�r den Umgang mit MS-Word zur Verf�gung.
 * 
 * @author mikel
 */
abstract class MsWordHelper {
	//
	// Auslese-Operationen
	//
	/**
	 * Diese Methode liest aus einer Word-Tabelle den textuellen Inhalt aus.
	 * 
	 * @param table
	 *            MS-Word-Tabelle.
	 * @param textTable
	 *            Daten�bergabehalde f�r die Tabelleninhalte; falls dieser Parameter <code>null</code> ist, wird eine
	 *            neue erzeugt.
	 * @return gef�llte Daten�bergabehalde.
	 */
	static TextTable extract(Table table, TextTable textTable) {
//		if (textTable == null) textTable = new TextTable("n/a");
//		Rows rows = table.get_Rows();
//		int rowCount = rows.get_Count();
//		for (int rowIndex = 1; rowIndex <= rowCount; rowIndex++) {
//		Cells rowCells = rows.Item(rowIndex).get_Cells();
//		TextTableRow textRow = new TextTableRow();
//		int colCount = rowCells.get_Count();
//		for (int colIndex = 1; colIndex <= colCount; colIndex++) {
//		Range cellRange = rowCells.Item(colIndex).get_Range();
//		
//		String cellText = cellRange.get_Text().replace('\r', '\n');
//		while (cellText.length() > 0 && 0 <= "\n\07".indexOf(cellText.charAt(cellText.length() - 1)))
//		cellText = cellText.substring(0, cellText.length() - 1);
//		
//		TextTableCell textCell = new TextTableCell(cellText, getStyle(cellRange).get_NameLocal());
//		textRow.addColumn(textCell);
//		}
//		textTable.addRow(textRow);
//		}
		return textTable;
	}
	
	/**
	 * Diese Methode liefert zu einer Range das zugeh�rige Style-Objekt.
	 * 
	 * @param range
	 *            zu untersuchende Range.
	 * @return zugeh�riges Style-Objekt.
	 */
	static Style getStyle(Range range) {
		//return new Style(range.getStyle().getInt());
		return null;
	}
	
	//
	// Einf�ge-Operationen
	//
	/**
	 * Diese Methode f�gt in ein Document an der angegebenen Position eine neue Tabelle mit den �bergebenen Inhalten
	 * gef�llt ein. Kein Rahmenteil ist sichtbar.
	 * 
	 * @param range
	 *            Position, an der die Tabelle eingef�gt werden soll.
	 * @param table
	 *            Tabelleninhaltsdaten.
	 * @return die neu erzeugte Tabelle.
	 */
	static Table insertInRange(Range range, TextTable table) {
//		if (table == null || table.getNumberOfRows() == 0) return null;
//		int rowCount = table.getNumberOfRows();
//		int columnCount = table.getRow(0).getNumberOfColumns();
//		
//		Table wordTable = range.get_Tables().Add(range, rowCount, columnCount);
//		for (int row = 0; row < rowCount; row++) {
//		TextTableRow tableRow = table.getRow(row);
//		for (int column = 0; column < columnCount && column < tableRow.getNumberOfColumns(); column++) {
//		Cell wordCell = wordTable.Cell(row + 1, column + 1);
//		TextTableCell tableCell = tableRow.getColumn(column);
//		insertInRange(wordCell.get_Range(), tableCell.getText(), tableCell.getStyle());
//		// telefonisch zwischen Niko und Lindstedt am 29.10.2004 gekl�rt:
//		// vertikale Zentrierung wird doch nicht mehr gew�nscht, da in Reinschrift
//		// unangemessen.
//		//wordCell.set_VerticalAlignment(WdVerticalAlignment.wdAlignVerticalCenter);
//		}
//		}
//		
//		if (table.usesMargins()) {
//		// Umrechnung int (1e-5 m) nach short (Pica-Point, 0.351 mm)
//		float leftIndent = 0, rightIndent = 0;
//		if (table.getLeftMargin() >= 0)
//		leftIndent = (float) (((double)table.getLeftMargin()) / 35.1);
//		if (table.getRightMargin() >= 0)
//		rightIndent = (float) (((double)table.getRightMargin()) / 35.1);
//		float all = columnCount * wordTable.get_Columns().get_Width();
//		if (all < leftIndent + rightIndent) {
//		if (all < leftIndent) {
//		rightIndent = 0;
//		leftIndent = all;
//		} else
//		rightIndent = all - leftIndent;
//		}
//		wordTable.get_Rows().set_LeftIndent(leftIndent);
//		wordTable.get_Columns().set_Width((all - rightIndent - leftIndent) / columnCount);
//		}
//		
//		removeBorders(wordTable);
//		return wordTable;
		return null;
	}
	
	/**
	 * Diese Methode f�gt im Range eine Liste von Paragraphen ein.
	 * 
	 * @param range
	 *            Dokumentbereich.
	 * @param paragraphList
	 *            Liste von global.Paragraph-Instanzen.
	 */
	static void insertInRange(Range range, List paragraphList) {
//		List reverseList = new ArrayList(paragraphList);
//		Collections.reverse(reverseList);
//		Iterator itParagraphs = reverseList.iterator();
//		while (itParagraphs.hasNext()) {
//		Object paragraphObject = itParagraphs.next();
//		if (paragraphObject instanceof global.Paragraph) {
//		global.Paragraph paragraph = (global.Paragraph) paragraphObject;
//		Range paragraphRange = range.get_Paragraphs().Add(range).get_Range();
//		paragraphRange.set_Text(paragraph.getText());
//		paragraphRange.set_Style(paragraph.getStyle());
//		} else if (paragraphObject instanceof Range) {
//		Range paragraph = (Range) paragraphObject;
//		Range paragraphRange = range.get_Paragraphs().Add(range).get_Range();
//		paragraphRange.set_FormattedText(paragraph);
//		}
//		}
	}
	
	/**
	 * Diese Methode f�gt im Range Text ein, gegebenenfalls mit einem Style.
	 * 
	 * @param range
	 *            Dokumentbereich
	 * @param text
	 *            Text
	 * @param style
	 *            Stil
	 */
	static void insertInRange(Range range, String text, String style) throws ComException {
		if (text != null) range.setText(text);
		//if (style != null) range.setStyle(style);
	}
	
	/**
	 * Diese Methode l�scht in einer Tabelle alle Tabellenlinien. 
	 * 
	 * @param table
	 */
	static void removeBorders(Table table) {
		table.getBorders().setInsideLineStyle(WdLineStyle.wdLineStyleNone);
		table.getBorders().setOutsideLineStyle(WdLineStyle.wdLineStyleNone);
	}
	
	/**
	 * Diese Methode f�gt in einer Tabelle Kreuztabellenlinien ein, das heisst Linien unter der ersten Zeile und
	 * zwischen den Spalten.
	 * 
	 * @param table
	 */
	static void insertCrossBorders(Table table) {
		Rows rows = table.getRows();
		if (rows.getCount() < 1) return;
		rows.getFirst().getBorders().item(WdBorderType.wdBorderBottom).setVisible(true);
		Columns columns = table.getColumns();
		int columnCount = columns.getCount();
		for (int column = 1; column < columnCount; column++)
			columns.item(column + 1).getBorders().item(WdBorderType.wdBorderLeft).setVisible(true);
	}
	
	//
	// Test-Methoden
	//
	/**
	 * Diese Methode �berpr�ft, ob die �bergebene Tabelle in einer Pivot-Zelle einen bestimmten Style hat.
	 * 
	 * @param table
	 *            zu testende Tabelle
	 * @param pivotRow
	 *            Testzeile
	 * @param pivotColumn
	 *            Testspalte
	 * @param markerStyle
	 *            Markierungs-Style
	 * @return Flag, ob in der entsprechenden Tabellenzelle der korrekte Stil benutzt wird.
	 */
	static boolean hasMarkerStyle(Table table, int pivotRow, int pivotColumn, String markerStyle) {
		if (table.getRows().getCount() < pivotRow + 1) return false;
		if (table.getColumns().getCount() < pivotColumn + 1) return false;
		Cell cell = table.cell(pivotRow + 1, pivotColumn + 1);
		return markerStyle.equalsIgnoreCase(getStyle(cell.getRange()).getNameLocal());
	}
	
	//
	// Selektion-Verwaltung
	//
	/**
	 * Diese Methode bewegt die Selektion hinter das aktuell selektierte Objekt.
	 * 
	 * @param selection Selektions-Objekt
	 */
	static void moveBehind(Selection selection) {
		selection.collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
		//selection.MoveRight(new Integer(WdUnits.wdCharacter), INTEGER_1);
	}
	
	//
	// Property-Verwaltung
	//
	/**
	 * Diese Methode setzt ein eingebautes Property auf einen String-Wert.
	 * 
	 * @param document
	 *            Dokument, in dem das Property gespeichert werden soll.
	 * @param property
	 *            Property-ID aus {@link org.evolvis.liboffice.msword8.WdBuiltInProperty}.
	 * @param value
	 *            String-Wert des Properties.
	 * @throws ComException
	 */
	/*
	 static void setBuiltInProperty(Document document, int property, String value) throws ComException {
	 Integer builtInPropertiesIDispatch = (Integer) document.getBuiltInDocumentProperties();
	 setDocumentProperty(builtInPropertiesIDispatch, property, value);
	 }
	 
	 /**
	  * Diese Methode holt ein eingebautes Property als String.
	  * 
	  * @param document
	  *            Dokument, in dem das Property gespeichert werden soll.
	  * @param property
	  *            Property-ID aus {@link org.evolvis.liboffice.msword8.WdBuiltInProperty}.
	  * @return String-Wert des Properties.
	  * @throws ComException
	  */
	/*
	 static String getBuiltInProperty(Document document, int property) throws ComException {
	 Integer builtInPropertiesIDispatch = (Integer) document.getBuiltInDocumentProperties();
	 return getDocumentProperty(builtInPropertiesIDispatch, property);
	 }
	 
	 /**
	  * Diese Methode setzt ein beliebiges Dokument-Property auf einen String-Wert.
	  * 
	  * @param documentPropertiesIDispatch
	  *            DocumentProperties-IDispatch.
	  * @param property
	  *            Property-ID.
	  * @param value
	  *            String-Wert des Properties.
	  * @throws ComException
	  */
	/*
	 static private void setDocumentProperty(Integer documentPropertiesIDispatch, int property, String value)
	 throws ComException {
	 Dispatch dispatch = getDocumentPropertyDispatch(documentPropertiesIDispatch, property);
	 dispatch.invoke_method(new Jvariant[] {new Jvariant(value, COMconstants.VT_BSTR)}, dispatch
	 .GetIDsOfNames("Value"), COMconstants.DISPATCH_PROPERTYPUT);
	 }
	 
	 /**
	  * Diese Methode holt ein beliebiges Dokument-Property als String.
	  * 
	  * @param documentPropertiesIDispatch
	  *            DocumentProperties-IDispatch.
	  * @param property
	  *            Property-ID.
	  * @return String-Wert des Properties.
	  * @throws ComException
	  */
	/*
	 static private String getDocumentProperty(Integer documentPropertiesIDispatch, int property) throws ComException {
	 Dispatch dispatch = getDocumentPropertyDispatch(documentPropertiesIDispatch, property);
	 return dispatch.invoke_method(new Jvariant[0], dispatch.GetIDsOfNames("Value"),
	 COMconstants.DISPATCH_PROPERTYGET).StringVal();
	 }
	 
	 /**
	  * Diese Methode holt ein Dispatch f�r ein Dokument-Property.
	  * 
	  * @param documentPropertiesIDispatch
	  *            DocumentProperties-IDispatch.
	  * @param property
	  *            Property-ID.
	  * @return DocumentProperty-Dispatch
	  * @throws ComException
	  */
	/*
	 static private Dispatch getDocumentPropertyDispatch(Integer documentPropertiesIDispatch, int property)
	 throws ComException {
	 Dispatch dispatch = new Dispatch(documentPropertiesIDispatch.intValue());
	 Jvariant args[] = {new Jvariant(property, COMconstants.VT_I4)};
	 Jvariant variant = dispatch.invoke_method(args, dispatch.GetIDsOfNames("Item"),
	 COMconstants.DISPATCH_PROPERTYGET);
	 return new Dispatch(variant.intVal());
	 }
	 */
	
	//
	// Variablen-Verwaltung
	//
	/**
	 * Diese Methode erzeugt eine Variable zu einem Namen.
	 * 
	 * @param variableName
	 *            Name der Variablen
	 * @return vollst&auml;ndige Variable
	 */
	static String variable(String variableName) {
		return '<' + variableName + '>';
	}
	
	//
	// Konstanten
	//
	/** Integer(1) */
	final static Integer INTEGER_1 = new Integer(1);
	
}
