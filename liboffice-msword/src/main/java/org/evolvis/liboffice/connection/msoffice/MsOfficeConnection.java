/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.connection.msoffice;

import java.util.logging.Logger;

import org.evolvis.liboffice.connection.OfficeConnection;
import org.evolvis.liboffice.factory.msoffice.MsMainOfficeFactory;
import org.evolvis.liboffice.msword11.Application;
import org.evolvis.liboffice.msword11.Document;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComException;
import com.jacob.com.Dispatch;

/**
 * Initializes the connection MS-Word
 * 
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 * 
 */
public class MsOfficeConnection implements OfficeConnection
{
	Logger logger = Logger.getLogger(MsOfficeConnection.class.getName());
	/** Microsoft Office Application Object */
	Application application = null;
	/** Aktives Dokument */
	Document currentDocument = null;
	private MsMainOfficeFactory mainOfficeFactory = null;

	public MsOfficeConnection(MsMainOfficeFactory mainfactory)
	{
		mainOfficeFactory = mainfactory;
//		lookupCurrentDocument();
	}
	
	/**
	 * Connects to the existing Word process (if any existing) or creates a new one.
	 * @return true winword.exe process were found. False if new process were started.
	 */
	public boolean init()
	{
		if (application == null)
		{
			/*
          Document doc = new Document();
          application = doc.getApplication();
          application.setVisible(true);
          doc.close(new Variant(false));
			 */
			//application = new Application();

			//wordComponent = new ActiveXComponent("Word.Application");
			//wordComponent.setProperty("Visible", new Variant(false));
//			application = new Application(new Dispatch("Word.Application"));
			
			// Check if an word process is already running 
			ActiveXComponent comp = ActiveXComponent.connectToActiveInstance("Word.Application");
			
//			System.out.println("Active Instance: "+comp);
			
			if(comp == null) {
//				System.out.println("Creating new Word.Application");
				application = new Application(new Dispatch("Word.Application"));
//				System.out.println("Open Documents: " + application.getDocuments().getCount());
				
				application.setVisible(true);
				return false;
			}
			else {
//				System.out.println("Reusing existant Word.Application");
				application = new Application(comp.getObject());
//				currentDocument = application.getDocuments().add();
				return true;
			}
		}
		return false;
	}

	public boolean isConnected()
	{
		return (mainOfficeFactory != null && application != null);
	}

	public MsMainOfficeFactory getMsMainOfficeFactory()
	{
		return mainOfficeFactory;
	}

	public Application getApplication()
	{
		return application;
	}

	public boolean isValid()
	{
		return application != null;
	}

	public Document getCurrentDocument()
	{
		init();
		lookupCurrentDocument();
		return currentDocument;
	}

	/**
	 * This method sets the current document for this connection on the real active document in MS-Word
	 */
	protected void lookupCurrentDocument() 
	{
		try 
		{

			currentDocument = application.getActiveDocument();
			
		} 
		catch(ComException ce) 
		{
			currentDocument = application.getDocuments().add();
		}
	}
}
