/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator.msoffice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.liboffice.connection.msoffice.MsOfficeConnection;
import org.evolvis.liboffice.descriptors.AbstractSectionDescriptor;
import org.evolvis.liboffice.descriptors.AbstractTableDescriptor;
import org.evolvis.liboffice.descriptors.BookmarkDescriptor;
import org.evolvis.liboffice.descriptors.CellTextTableDescriptor;
import org.evolvis.liboffice.descriptors.FormattedTextDescriptor;
import org.evolvis.liboffice.descriptors.FrameDescriptor;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.descriptors.StyleTableDescriptor;
import org.evolvis.liboffice.descriptors.StyledParagraphDescriptor;
import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextBookmark;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextFrame;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextMarker;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextParagraph;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextTable;
import org.evolvis.liboffice.factory.MainOfficeFactoryFetcherAdapter;
import org.evolvis.liboffice.locator.TextContentDescriptorContainer;
import org.evolvis.liboffice.locator.TextContentLocator;
import org.evolvis.liboffice.locator.TextContentDescriptorContainer.DescriptorWrapper;
import org.evolvis.liboffice.msword11.Bookmark;
import org.evolvis.liboffice.msword11.Bookmarks;
import org.evolvis.liboffice.msword11.Cell;
import org.evolvis.liboffice.msword11.Document;
import org.evolvis.liboffice.msword11.Field;
import org.evolvis.liboffice.msword11.Fields;
import org.evolvis.liboffice.msword11.Find;
import org.evolvis.liboffice.msword11.Paragraph;
import org.evolvis.liboffice.msword11.Paragraphs;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Shape;
import org.evolvis.liboffice.msword11.Shapes;
import org.evolvis.liboffice.msword11.Style;
import org.evolvis.liboffice.msword11.Table;
import org.evolvis.liboffice.msword11.Tables;
import org.evolvis.liboffice.msword11.WdCollapseDirection;
import org.evolvis.liboffice.msword11.WdFindWrap;
import org.evolvis.liboffice.msword11.WdUnderline;
import org.evolvis.liboffice.msword11.WdViewTypeOld;

import com.jacob.com.ComException;
import com.jacob.com.Variant;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 */
public class MsOfficeTextContentLocator extends MainOfficeFactoryFetcherAdapter implements TextContentLocator
{
	private MsOfficeConnection m_oMsOfficeConnection;
	private final static Logger logger                                         = Logger.getLogger(MsOfficeTextContentLocator.class.getName());
	private final static String DEFAULT_KEY                               = "*_*default_key*_*";
	
	public MsOfficeTextContentLocator(MsOfficeConnection connection)
	{
		m_oMsOfficeConnection = connection;
	}
	
	
	public Map getComponents(TextDocument document, TextContentDescriptorContainer container)
	{
		Map out = getText(document, container.getParagraphs());
		out.putAll( getFrames(document, container.getFrames()));
		out.putAll( getTables(document, container.getTables()));
		out.putAll( getMarker(document, container.getMarkers()));
		return out;
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.locator.TextContentLocator#locateComponent(de.tarent.documents.document.TextDocument, de.tarent.documents.descriptors.TextContentDescriptor)
	 */
	public TextDocumentComponent locateComponent(TextDocument oDocument, TextContentDescriptor descriptor)
	{    
		if (oDocument instanceof MsOfficeTextDocument)
		{      
			if (descriptor instanceof NameHintMarkerDescriptor)
			{ 
				Map in = new HashMap();
				in.put(DEFAULT_KEY, new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
				Map out = getMarker(oDocument, in );
				return (TextDocumentComponent)out.get(DEFAULT_KEY);
			}
			else if (descriptor instanceof AbstractTableDescriptor)
			{
				Map in = new HashMap();
				in.put(DEFAULT_KEY, new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
				Map out = getTables(oDocument, in );
				return (TextDocumentComponent)out.get(DEFAULT_KEY);
			}
			else if (descriptor instanceof FrameDescriptor)
			{
				Map in = new HashMap();
				in.put(DEFAULT_KEY, new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
				Map out = getFrames(oDocument, in );
				return (TextDocumentComponent)out.get(DEFAULT_KEY);
			}
			else if (descriptor instanceof AbstractSectionDescriptor)
			{
				DescriptorWrapper dw = new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true);
				List paras = collectText((MsOfficeTextDocument)oDocument, dw );
				if (! paras.isEmpty()) return (TextDocumentComponent)paras.get(0);
			}
			else if(descriptor instanceof BookmarkDescriptor) {
				Map in = new HashMap();
				in.put(DEFAULT_KEY, new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, true));
				Map out = getBookmarks(oDocument, in);
				return (TextDocumentComponent)out.get(DEFAULT_KEY);
			}
			else throw new RuntimeException("Unknown descriptor in MsTextContentLocator.locateComponents : "+descriptor.getClass());
		}
		return null;
	}
	
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.locator.TextContentLocator#collectComponents(de.tarent.documents.document.TextDocument, de.tarent.documents.descriptors.TextContentDescriptor)
	 */
	public List collectComponents(TextDocument oDocument, TextContentDescriptor descriptor)
	{
		if (oDocument instanceof MsOfficeTextDocument)
		{      
			MsOfficeTextDocument document = (MsOfficeTextDocument)oDocument;
			if (descriptor instanceof AbstractTableDescriptor)
			{
				Map in = new HashMap();
				in.put(DEFAULT_KEY, new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false));
				Map out = getTables(oDocument, in );
				return (List)out.get(DEFAULT_KEY);
			}
			else if (descriptor instanceof AbstractSectionDescriptor)
			{
				DescriptorWrapper dw = new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false);
				return collectText(document, dw);
			}
			else if (descriptor instanceof NameHintMarkerDescriptor)
			{
				Map in = new HashMap();
				in.put(DEFAULT_KEY, new TextContentDescriptorContainer().new DescriptorWrapper(descriptor, false));
				Map out = getMarker(oDocument, in );
				return (List)out.get(DEFAULT_KEY);
			}
			else throw new RuntimeException("Unknown descriptor in MsTextContentLocator.collectComponents : "+descriptor.getClass());
		}
		return null;
	}
	
	
	// ######################################################################################################################
	
	private Map getText(TextDocument doc, Map descs)
	{
		Map out = new HashMap();
		
		Iterator it = descs.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry entry = (Map.Entry) it.next();
			out.put(entry.getKey(), collectText((MsOfficeTextDocument)doc, (DescriptorWrapper) entry.getValue()));
		}
		return out;
	}
	
	private List collectText(MsOfficeTextDocument mstextdoc, DescriptorWrapper dw)
	{
		boolean locate                        = dw.isLocate();
		AbstractSectionDescriptor desc        = (AbstractSectionDescriptor)dw.getDescriptor();
		List comps                            = new ArrayList();
		Document doc                          = mstextdoc.getDocument();
		try
		{
			m_oMsOfficeConnection.getApplication().getSelection().homeKey(new Variant(6));
			m_oMsOfficeConnection.getApplication().getActiveWindow().getView().setType(WdViewTypeOld.wdPageView);
			doc.activate();
			
			
			Shapes shapes                     = doc.getShapes();
			
			for (int i=0; i <= shapes.getCount(); i++)
			{
				
				if (i > 0)
				{
					Shape shape                   = shapes.item(new Variant(i));
					shape.select();
				}
				
				Find find                     = mstextdoc.getConnection().getApplication().getSelection().getFind();
				find.clearFormatting();
				
				//boolean setFormat             = false;
				
				if (desc instanceof StyledParagraphDescriptor)
				{
					if(((StyledParagraphDescriptor)desc).getStyle() != null)
					{
						find.setStyle(new Variant(mstextdoc.getConnection().getApplication().getActiveDocument().getStyles().item(new Variant(((StyledParagraphDescriptor)desc).getStyle()))));
					}
				}
				else if(desc instanceof FormattedTextDescriptor)
				{
					FormattedTextDescriptor f = (FormattedTextDescriptor) desc;
					find.getFont().setBold(f.isBold() ? 1 : 0);
					find.getFont().setItalic(f.isItalic() ? 1 : 0);
					if (f.isUnderline())
					{
						find.getFont().setUnderline(WdUnderline.wdUnderlineSingle);
					}
					//setFormat                       = true;
				}
				else throw new RuntimeException("Unsupported Descriptor in collectTexts " + desc.getClass());
				
				if(null != desc.getText())
				{
					find.setText( desc.getText() );
				}
				
				find.setFormat( true );
				
				find.setForward( true );
				find.setWrap( locate ? WdFindWrap.wdFindContinue : WdFindWrap.wdFindStop );
				//find.set_Wrap( WdFindWrap.wdFindStop );
				find.setMatchCase( false );
				find.setMatchWholeWord( false );
				find.setMatchWildcards( false );
				find.setMatchAllWordForms( false );
				
				if (locate)
				{
					if (find.execute())
					{
						return Arrays.asList(new Object[]{ new MsOfficeTextParagraph(mstextdoc, mstextdoc.getConnection().getApplication().getSelection().getRange()) });
					}
				}
				else
				{
					find.execute();
					while (find.getFound())
					{
						comps.add( new MsOfficeTextParagraph(mstextdoc, mstextdoc.getConnection().getApplication().getSelection().getRange()));
						mstextdoc.getConnection().getApplication().getSelection().collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
						find.execute();
					}
					return comps;
				}
			}
		}
		catch (ComException e) 
		{
			logger.log(Level.SEVERE, "Error", e );
		}
		
		return new ArrayList();
	}
	
	
	// #######################################################################################################################
	
	private Map getFrames(TextDocument doc, Map descs)
	{
		if (descs.isEmpty()) return new HashMap();
		Map out = new HashMap();
		Shapes shapes            = ((MsOfficeTextDocument)doc).getDocument().getApplication().getActiveDocument().getShapes();
		for(int i=1; i<=(shapes.getCount()); i++)
		{
			Shape shape            = shapes.item(new Variant(i));
			Range range            = shape.getTextFrame().getTextRange();
			
			Paragraphs paras       = range.getParagraphs();
			
			for(int j = 1;  j <= paras.getCount(); j++)
			{
				Paragraph para     = paras.item(j);
				Style style           = getStyle( para.getRange() );
				String text          = para.getRange().getText();
				if (style != null)
				{         
					if (descs.isEmpty()) break;
					Iterator it = descs.entrySet().iterator();
					while (it.hasNext())
					{
						Map.Entry entry = (Map.Entry) it.next();
						Object key        = entry.getKey();
						DescriptorWrapper dw = (DescriptorWrapper)entry.getValue();
						if (! dw.isLocate() && out.get(key) == null) out.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird
						TextContentDescriptor desc = dw.getDescriptor();
						
						TextContentDescriptor embeddedDesc =  ((FrameDescriptor)desc).getDescriptor();
						
						if (embeddedDesc instanceof StyledParagraphDescriptor)
						{
							String lookupStyle = ((StyledParagraphDescriptor) embeddedDesc).getStyle();
							String lookupText  = ((StyledParagraphDescriptor) embeddedDesc).getText();
							if ((null != lookupStyle && lookupStyle.equals(style.getNameLocal())) || (null != text && text.trim().equals( lookupText )))
							{
								addTextDocumentComponent(new MsOfficeTextFrame( shape ), dw.isLocate(), out, key);
								if ( dw.isLocate()) it.remove();
							}
						}
						else throw new RuntimeException("Unsupported embedded descriptor in getFrames: " + embeddedDesc.getClass());
					}   
				}
			}
		}
		return out;
	}
	
	
	
	// #############################################################################################################
	
	private static void checkField(Map descs, MsOfficeTextDocument mstextdoc, Field field, Map out)
	{
		Iterator it = descs.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry entry                    =  (Map.Entry) it.next();
			Object key                           = entry.getKey();
			DescriptorWrapper dw          = (DescriptorWrapper) entry.getValue();
			if (! dw.isLocate() && out.get(key) == null) out.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird
			NameHintMarkerDescriptor m = (NameHintMarkerDescriptor) dw.getDescriptor();
			
			if (isMarker(field,m.getName(), m.getHint() ))
			{
				addTextDocumentComponent(new MsOfficeTextMarker(mstextdoc, field), dw.isLocate(), out, key);
				if ( dw.isLocate()) it.remove();
			}
		}
	}
	
	private static void checkBookmark(Map descs, MsOfficeTextDocument mstextdoc, Bookmark bookmark, Map out)
	{
		Iterator it = descs.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			Object key = entry.getKey();
			DescriptorWrapper dw = (DescriptorWrapper) entry.getValue();
			if (! dw.isLocate() && out.get(key) == null) out.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird
			BookmarkDescriptor m = (BookmarkDescriptor) dw.getDescriptor();

			if (isBookmark(bookmark, m.getName())) {
				
				addTextDocumentComponent(new MsOfficeTextBookmark(mstextdoc, bookmark), dw.isLocate(), out, key);
				if ( dw.isLocate()) it.remove();
			}
		}
	}
	
	public static boolean isBookmark(Bookmark bookmark, String pName)
	{
		return (pName == null || pName.equals(bookmark.getName()));	
	}
	
	private Map getMarker(TextDocument textdoc, Map descs)
	{
		Map out = new HashMap();
		if (descs.isEmpty()) return out;
		
		MsOfficeTextDocument mstextdoc = (MsOfficeTextDocument) textdoc;
		Document doc = mstextdoc.getDocument();
		doc.activate();
		
		Fields fields = doc.getFields();
		int amountFields = fields.getCount();
		for(int i=1; i<= amountFields; i++)
		{
			Field field = fields.item(i);
			checkField(descs, mstextdoc, field, out);
		}
		
		/*
		Shapes shapes = doc.getApplication().getActiveDocument().getShapes();
		int amountShapes = shapes.getCount();
		for(int i=1; i<=amountShapes; i++)
		{
			Shape shape = shapes.item(new Variant(i));
			Fields shapeFields = shape.getTextFrame().getTextRange().getFields();
			int amountShapeFields = shapeFields.getCount(); 
			if(amountShapeFields > 0)
			{
				for(int n=1; n<=amountShapeFields; n++)
				{
					Field shapeField = shapeFields.item(n);
					checkField(descs, mstextdoc, shapeField, out);
				}
			}
		}
		*/
		
		//collectMarkersInHeaderFooters(mstextdoc, doc.getSections().item(1).getHeaders(), descs, out);
		//collectMarkersInHeaderFooters(mstextdoc, doc.get_Sections().Item(1).get_Footers(), descs, out);
		
		//Application ap =  ((MsTextDocument)textdoc).getDocument().getApplication();
		//ap.getActiveWindow().getActivePane().close();
		//ap.getActiveWindow().getView().setType(WdViewType.wdPageView);
		
		return out;
	}
	
	public Map getBookmarks(TextDocument textdoc, Map descs) {
		Map out = new HashMap();
		if (descs.isEmpty()) return out;
		
		MsOfficeTextDocument mstextdoc = (MsOfficeTextDocument) textdoc;
		Document doc = mstextdoc.getDocument();
		doc.activate();
		
		Bookmarks bookmarks = doc.getBookmarks();
		int amountBookmarks = bookmarks.getCount();
		
		for(int i=1; i<= amountBookmarks; i++) {
			Bookmark bookmark = bookmarks.item(new Variant(i));
			checkBookmark(descs, mstextdoc, bookmark, out);
		}		
		return out;
	}
	
	/*
	/** 
	 * Sammelt alle Marker in allen Kopf oder Fu[sz]zeilen eines Dokuments ein
	 * @param mstextdoc das zu durchsuchende Dokument
	 * @param headers die zu durduchende Kopf oder Fu[sz]zeile
	 */
	
	/*
	private static void collectMarkersInHeaderFooters(MsOfficeTextDocument mstextdoc, HeadersFooters headers, Map descs, Map out)
	{
		Document doc        = mstextdoc.getDocument();
		int amountHeaders = headers.getCount();
		for (int i=1; i <= amountHeaders; i++)
		{
			logger.debug("header: " + i);
			headers.item( i ).getRange().select();
			Selection sel       =  doc.getApplication().getSelection();
			Fields headerFields = sel.getFields();
			
			for(int j=1; j<=(headerFields.getCount()); j++)
			{
				Field field = headerFields.item( j );
				checkField(descs, mstextdoc, field,out);
			}
			sel.collapse();
		}
	}
	*/
	
	public static boolean isMarker(Field field, String name, String hint)
	{
		return isMarker2(field, name, hint);
		
		/*
		String text = name;
		if (hint != null)
		{
			text += MsTextMarker.CONST_MARKER_HINT_SEPARATOR + hint;
		}
		
		if (field.get_Type() == 59)
		{                
			String fieldText = field.get_Code().get_Text();        
			
			String keyWord = null;
			if (fieldText.indexOf(MsTextMarker.CONST_FIELD_IDENTIFIER2) != -1)
			{
				keyWord = " "+MsTextMarker.CONST_FIELD_IDENTIFIER2;
			}
			else if (fieldText.indexOf(MsTextMarker.CONST_FIELD_IDENTIFIER1) != -1)
			{
				keyWord = " "+MsTextMarker.CONST_FIELD_IDENTIFIER1;
			}
			else
			{
				logger.warn("findMarker(): unbekannte Version von Word.");
				return false;
			}
			
			if (fieldText.startsWith(keyWord))
			{
				fieldText = fieldText.substring(keyWord.length() + 1);
				int endIndex = fieldText.indexOf("\\*");
				
				logger.debug("text " + text  + " fieldText " +fieldText);
				if(null == text) return true;
				
				if (endIndex != -1)
				{
					String fieldName = fieldText.substring(0, endIndex - 1).trim();
					if (text.equalsIgnoreCase(fieldName)) return true;
				}          
				else
				{
					String fieldName = fieldText.trim();
					if (text.equalsIgnoreCase(fieldName)) return true;
				}          
			}
		}
		return false;
		*/
	}
	
	public static boolean isMarker2(Field pField, String pName, String pHint)
	{
		boolean isMarker = false;
		
		if(pField.getType() == 59)
		{
			String fieldText = pField.getCode().getText();
			
			String keyWord = null;
			if (fieldText.indexOf(MsOfficeTextMarker.CONST_FIELD_IDENTIFIER2) != -1)
			{
				keyWord = " "+MsOfficeTextMarker.CONST_FIELD_IDENTIFIER2;
			}
			else if (fieldText.indexOf(MsOfficeTextMarker.CONST_FIELD_IDENTIFIER1) != -1)
			{
				keyWord = " "+MsOfficeTextMarker.CONST_FIELD_IDENTIFIER1;
			}
			else
			{
				logger.warning("findMarker(): unbekannte Version von Word.");
				return false;
			}
			
			if (fieldText.startsWith(keyWord))
			{
				fieldText = fieldText.substring(keyWord.length() + 1);
				int endIndex = fieldText.indexOf("\\*");
				
				if(endIndex != -1)
				{
					fieldText = fieldText.substring(0, endIndex - 1).trim();
				}
				else
				{
					fieldText = fieldText.trim();
				}
				
				String[] nameHintPair = null;
				
				if(fieldText.indexOf(MsOfficeTextMarker.CONST_MARKER_HINT_SEPARATOR) != -1)
				{
					nameHintPair = fieldText.split(MsOfficeTextMarker.CONST_MARKER_HINT_SEPARATOR); 
				}
				
				if(nameHintPair == null)
				{
					nameHintPair = new String[2];
					nameHintPair[0] = fieldText;
					nameHintPair[1] = null;
				}
				
				if(pName != null)
				{
					if(nameHintPair[0] != null)
					{
						isMarker = nameHintPair[0].equalsIgnoreCase(pName);
					}
					else isMarker = false;
				}
				
				if(pHint != null)
				{
					if(nameHintPair[1] != null)
					{
						isMarker = nameHintPair[1].equalsIgnoreCase(pHint);
					}
					else isMarker = false;
				}
				
				if(pName == null && pHint == null) isMarker = true;
			}
			
		}
		
		return isMarker;	
	}

	// #############################################################################################################
	
	
	private boolean checkAllCells(Map cellPropertyCache, MsOfficeTextTable ooTable, Map out, AbstractTableDescriptor st, boolean returnFirst, Object key, MsOfficeTextDocument doc)
	{
		int numrows = ooTable.getNumberOfRows();
		int numcols   = ooTable.getNumberOfColumns();
		
		for(int rowIndex=0; rowIndex < numrows; rowIndex++)
		{
			for(int colIndex=0; colIndex < numcols; colIndex++)
			{
				boolean ret = false;
				if(st instanceof StyleTableDescriptor)
				{
					ret = checkCell(cellPropertyCache, rowIndex, colIndex, ooTable, out, (StyleTableDescriptor)st, returnFirst, key);
				}
				else  if (st instanceof CellTextTableDescriptor) ret = checkCell(cellPropertyCache, rowIndex, colIndex, ooTable, out, (CellTextTableDescriptor)st, returnFirst, key);
				else throw new RuntimeException("Unsupported TableDescriptor " + st.getClass());
				if (ret) return true;
			}          
		}
		return false;
	}
	
	private boolean checkCell(Map cellPropertyCache, int row, int col, MsOfficeTextTable msTable, Map out, StyleTableDescriptor st, boolean returnFirst, Object key)
	{
		String style = null;
		
		if(cellPropertyCache != null) style = (String) cellPropertyCache.get("cell"+row+"|"+col);
		else logger.fine("NULL cellPropertyCache is null");
		
		if (null == style)
		{
			Table tab = null;
			Cell cell = null;
			Range cellRange = null;
			Style cellStyle = null;
			
			if(msTable != null) tab           = msTable.getTable();
			else logger.fine("NULL msTable is null");
			if(tab != null) cell              = tab.getRows().item(row+1).getCells().item(col+1);
			else logger.fine("NULL tab is null");
			if(cell != null) cellRange		= cell.getRange();
			else logger.fine("NULL cell is null");
			if(cellRange != null) cellStyle   = getStyle(cellRange);
			else logger.fine("NULL cellRange is null");
			if(cellStyle != null) style       = cellStyle.getNameLocal();
			else logger.fine("NULL cellStyle is null");
			if(style != null) cellPropertyCache.put("cell"+row+"|"+col, style);
			else logger.fine("NULL style is null");
		}
		
		String styleName = null;
		if(st != null) styleName = st.getStyleName();
		else logger.fine("NULL st is null");
		
		if (style != null && null != styleName && styleName.equals(style)) 
		{
			boolean ret = addTextDocumentComponent(msTable, returnFirst, out, key );
			if (ret) return true;
		}
		return false;
	}
	
	
	
	private boolean checkCell(Map cellPropertyCache, int row, int col, MsOfficeTextTable msTable, Map out, CellTextTableDescriptor st, boolean returnFirst, Object key)
	{
		String text = null;
		if(cellPropertyCache != null) text = (String)cellPropertyCache.get("cell"+row+"|"+col);
		else logger.fine("NULL cellPropertyCache is null");
		
		if (null == text)
		{
			Table tab = null;
			Cell cell = null;
			Range cellRange = null;
			
			
			if(msTable != null) tab                          =  msTable.getTable();
			else logger.fine("NULL msTable is null");
			if(tab != null) cell                            =  tab.getRows().item(row+1).getCells().item(col+1);
			else logger.fine("NULL tab is null");
			if(cell != null) cellRange              = cell.getRange();
			else logger.fine("NULL cell is null");
			if(cellRange != null) text                               = cellRange.getText().trim();
			else logger.fine("NULL cellRange is null");
			cellPropertyCache.put("cell"+row+"|"+col, text);
		}
		
		if(text == null) logger.fine("NULL text is null");
		if(st == null) logger.fine("NULL text is null");
		
		if (null != text && st != null && text.equals(st.getText()))
		{
			//boolean ret = addTextDocumentComponent(msTable, returnFirst, out , key);
			if (returnFirst) return true;
		}
		return false;
	}
	
	
	private void checkTable(Map descs, MsOfficeTextDocument document, Table table, Map out)
	{
		Map cellPropertyCache                          =  new HashMap();
		Iterator it                                           = descs.entrySet().iterator();
		MsOfficeTextTable  msTable                           = new MsOfficeTextTable((MsOfficeTextDocument)document, table);
		while (it.hasNext())
		{
			Map.Entry entry                                = (Map.Entry) it.next();
			Object key                                        = entry.getKey();
			DescriptorWrapper dw                       = (DescriptorWrapper)entry.getValue();
			if (! dw.isLocate() && out.get(key) == null) out.put(key, new LinkedList()); // sicherstellen, da[sz] fuer jeden Descriptor zumindest eine leere Liste zureuck gegeben wird  
			AbstractTableDescriptor desc             = (AbstractTableDescriptor) dw.getDescriptor();
			
			if (desc instanceof AbstractTableDescriptor)
			{
				int row = desc.getRow();
				int col = desc.getColumn();
				
				if (row == -1 || col == -1)
				{
					boolean ret = checkAllCells(cellPropertyCache, msTable, out, desc, dw.isLocate(), key, (MsOfficeTextDocument)document);
					if (ret)
					{
						it.remove();
						break;
					}
				}
				else
				{
					boolean ret = false;
					if (desc instanceof StyleTableDescriptor) ret = checkCell(cellPropertyCache, row, col, msTable, out, (StyleTableDescriptor)desc, dw.isLocate(), key);
					else  if(desc instanceof CellTextTableDescriptor) ret = checkCell(cellPropertyCache, row, col, msTable, out, (CellTextTableDescriptor)desc, dw.isLocate(), key);
					else throw new RuntimeException("Unsupported TableDescriptor " + desc.getClass());
					
					if (ret)
					{
						it.remove();
						break;
					}
				}
			}
			else throw new RuntimeException("Unsupported TableDescriptor " + desc.getClass());
		}
	}      
	
	
	private Map getTables(TextDocument document, Map descs)
	{
		if (descs.isEmpty()) return new HashMap();
		Map out                                                 = new HashMap();
		Document doc                                        = ((MsOfficeTextDocument)document).getDocument();
		doc.activate();
		
		Tables tables                                          = doc.getTables();
		int amountTables                                   = tables.getCount();
		for(int i=1; i<= amountTables ; i++)
		{
			
			Table table                                          = tables.item(i);
			checkTable(descs, (MsOfficeTextDocument)document, table, out );
			if (descs.isEmpty()) break;
		}         
		
		Shapes shapes = doc.getApplication().getActiveDocument().getShapes();
		for(int i=1; i<=(shapes.getCount()); i++)
		{
			Shape shape = shapes.item(new Variant(i));
			Tables shapeTables = shape.getTextFrame().getTextRange().getTables();
			if(shapeTables.getCount() > 0)
			{
				for(int n=1; n<=(shapeTables.getCount()); n++)
				{
					Table shapeTable = shapeTables.item(n);
					checkTable(descs, (MsOfficeTextDocument)document, shapeTable, out );
					if (descs.isEmpty()) break;
				}        
			}
		}
		return out;
	}
	
	
	private static Style getStyle(Range range)
	{
		/*
		int val = ((Integer) range.getStyle()).intValue();
		if (val != 0) return new Style(val);
		*/
		return null;  		
	}
	
	private static boolean addTextDocumentComponent(TextDocumentComponent comp, boolean returnFirst, Map out, Object key)
	{
		if (returnFirst)
		{
			out.put(key, comp);
			return true;
		}
		((List)out.get(key)).add( comp );
		return false;
	}
	
	
	// #################################################################################################
	
	/* (non-Javadoc)
	 * @see de.tarent.documents.locator.TextContentLocator#supportsDescriptor(de.tarent.documents.descriptors.TextContentDescriptor)
	 */
	public boolean supportsDescriptor(TextContentDescriptor descriptor)
	{
		if (descriptor instanceof NameHintMarkerDescriptor) return true;
		else if (descriptor instanceof StyleTableDescriptor) return true;
		else if (descriptor instanceof CellTextTableDescriptor) return true;
		else if (descriptor instanceof FrameDescriptor) return true;
		return false;
	}
	
	public TextMarker getNextTextMarker(TextDocument pDocument)
	{
		MsOfficeTextDocument mstextdoc = (MsOfficeTextDocument) pDocument;
		Document doc = mstextdoc.getDocument();
		doc.activate();
		
		Fields fields = doc.getFields();
		int amountFields = fields.getCount();
		for(int i=1; i<= amountFields; i++)
		{
			Field field = fields.item(i);
			if(isMarker(field, null, null)) return new MsOfficeTextMarker(mstextdoc, field);
		}
		
		return null;
	}
}
