/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 21.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator.msoffice;

import org.evolvis.liboffice.document.msoffice.MsOfficeTextDocument;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.TextContentRange;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextContentRange implements TextContentRange 
{
  private Range m_oRange = null;
  private MsOfficeTextDocument doc;  
  
  public MsOfficeTextContentRange()
  {    
  }
  
  public MsOfficeTextContentRange(MsOfficeTextDocument doc, Range range)
  {    
    m_oRange = range;
    this.doc    = doc;
  }
  
  public void select(boolean append)
  {
     // TODO append implementieren
      m_oRange.select();
  }

  public TextContentLocation getStartLocation()
  {
    MsOfficeTextContentLocation location = new MsOfficeTextContentLocation(m_oRange);
    location.getRange().setEnd(location.getRange().getStart());
    return location;
  }

  public TextContentLocation getEndLocation()
  {
    MsOfficeTextContentLocation location = new MsOfficeTextContentLocation(m_oRange);
    location.getRange().setStart(location.getRange().getEnd());
    return location;
  }

  public void setRange(TextContentLocation start, TextContentLocation end)
  {
      if(null != start)
      {
          Range msStart = ((MsOfficeTextContentLocation)start).getRange();
          if(null != end)
          {
              Range msEnd   = ((MsOfficeTextContentLocation)end).getRange();
              
              m_oRange.setStart( msStart.getEnd());
              System.out.println("msEnd: "+msEnd);
              m_oRange.setEnd( msEnd.getStart() );
          }
          else System.out.println("end is null");
      }
      else System.out.println("start is null");
  }


  public String getPlainText()
  {
    return m_oRange.getText();
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.locator.TextContentRange#getStyledText()
   */
  public VirtualStyledText getStyledText()
  {
      throw new UnsupportedOperationException("Method is not implemented"); 
  }

    public void copy()
    {
        doc.copy();
    }
    
    public void select()
    {
        select( false );
    }

}
