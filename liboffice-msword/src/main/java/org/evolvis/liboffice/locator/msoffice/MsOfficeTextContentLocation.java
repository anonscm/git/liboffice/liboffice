/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.locator.msoffice;

//import org.evolvis.liboffice.msword8.Document;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextBookmark;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextMarker;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextParagraph;
import org.evolvis.liboffice.document.msoffice.MsOfficeTextTable;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.msword11.Range;
import org.evolvis.liboffice.msword11.Tables;
import org.evolvis.liboffice.msword11.WdCollapseDirection;

import com.jacob.com.Variant;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 */
public class MsOfficeTextContentLocation implements TextContentLocation
{
  private Range m_oRange;
  
  public MsOfficeTextContentLocation(Range range)
  {
    m_oRange = range;
  }

  public MsOfficeTextContentLocation(TextDocumentComponent component, boolean after)
  {
    if (component instanceof MsOfficeTextMarker)
    {
        if(after)
        {
            throw new java.lang.RuntimeException("MsTextContentLocation constructor getTextContentLocation after TextMarker is not implemented");
        }
        else m_oRange = (((MsOfficeTextMarker)component).getRange());
    }
    else if (component instanceof MsOfficeTextTable)
    {
      if(after)
      {
          m_oRange      = (((MsOfficeTextTable)component).getRange());
          m_oRange.select();
          m_oRange.collapse(new Variant(WdCollapseDirection.wdCollapseEnd ));
          m_oRange = m_oRange.getApplication().getSelection().getRange();
          
      }
      else
      {
          m_oRange = (((MsOfficeTextTable)component).getRange());
          m_oRange.select();
          m_oRange.getApplication().getSelection().moveUp();
          m_oRange = m_oRange.getApplication().getSelection().getRange();
      }
    }
    else if(component instanceof MsOfficeTextParagraph)
    {
       if(after)
       {
           // wenn der paragrahp in einer Tabelle ist, wird der Cursor nach der Tabelle positioniert
           MsOfficeTextParagraph para =  ((MsOfficeTextParagraph)component);
           Tables tables        = para.getRange().getTables();
           if (null != tables && tables.getCount() > 0)
           {
        	   //System.out.println("found table " + tables.get_Count());
               Range tableRange =  tables.item(1).getRange();
               tableRange.select();
               tableRange.collapse(new Variant(WdCollapseDirection.wdCollapseEnd ));
               m_oRange = para.getRange().getApplication().getSelection().getRange();
           }
           else
           {
               para.getRange().select();
               para.getRange().collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
               m_oRange =  para.getRange().getApplication().getSelection().getRange();
           }
           
       } 
       else  m_oRange =   ((MsOfficeTextParagraph)component).getRange();
       
    } else if(component instanceof MsOfficeTextBookmark){
 	   
 	   
	   	m_oRange = (((MsOfficeTextBookmark)component).getRange());
	   
   }
    else throw new java.lang.RuntimeException("MsTextContentLocation constructor getTextContentLocation type is not supported "+ component.getClass());
  }
  
  /*
  private void findText(Document document) 
  {
    MsOfficeConnection msConnection = null;
    if (msConnection.isValid()) 
    {
      String findString = null;
      final Integer WD_FIND_CONTINUE = new Integer(WdFindWrap.wdFindContinue);
      final Integer WD_REPLACE_ALL = new Integer(WdReplace.wdReplaceAll);
      try 
      {
        StoryRanges storyRanges = document.get_StoryRanges();
        
        String variableName = "";
        String replaceString = "";
         
        StoryRangeIterator itStoryRanges = new StoryRangeIterator(storyRanges);           
        while (itStoryRanges.hasNext()) 
        {
          RangeIterator itVariables = new VariableRangeIterator(itStoryRanges.nextRange(), variableName);
          while (itVariables.hasNext()) 
          {
            Range range = itVariables.nextRange();
            if (replaceString != null && replaceString.length() > 0) range.set_Text(replaceString);
            else if (range.get_Paragraphs().get_First().get_Range().get_Text().trim().equals(findString)) range.get_Paragraphs().get_First().get_Range().set_Text("");
            else range.set_Text("");
          }
        }
      } 
      catch (com.ibm.bridge2java.ComException e) 
      {
        String msg = "Fehler beim Ersetzen von Variablen durch Text";
      }
    }
  }
  */
  
  
  public Range getRange()
  {
    return m_oRange;
  }

  public void collapseToStart()
  {
    m_oRange.collapse(new Variant(WdCollapseDirection.wdCollapseStart));
  }

  public void collapseToEnd()
  {
    m_oRange.collapse(new Variant(WdCollapseDirection.wdCollapseEnd));
  }

  public void gotoStart(boolean collapse)
  {
    m_oRange.m_goTo(new Variant(WdCollapseDirection.wdCollapseStart));
  }

  public void gotoEnd(boolean collapse)
  {
    m_oRange.m_goTo(new Variant(WdCollapseDirection.wdCollapseEnd));
  }

  //TODO: testen ob das so geht...
  public void goLeft(int steps, boolean collapse)
  {
    m_oRange.goToPrevious(steps);
  }

  public void goRight(int steps, boolean collapse)
  {
    m_oRange.goToNext(steps);
  }

  
  public boolean goUp(int steps, boolean collapse)
  {
      throw new UnsupportedOperationException("Method is not implemented");
  }

  public boolean goDown(int steps, boolean collapse)
  {
      throw new UnsupportedOperationException("Method is not implemented");    
  }    
  
  
  public int compare(TextContentLocation location)
  {
      throw new UnsupportedOperationException("Method is not implemented");
  }

  /* (non-Javadoc)
   * @see de.tarent.documents.locator.TextContentLocation#positionAfterComponent(de.tarent.documents.document.TextDocumentComponent)
   */
  public boolean positionAfterComponent(TextDocumentComponent component, TextDocument doc)
  {
      throw new UnsupportedOperationException("Method is not implemented");
  }

}
