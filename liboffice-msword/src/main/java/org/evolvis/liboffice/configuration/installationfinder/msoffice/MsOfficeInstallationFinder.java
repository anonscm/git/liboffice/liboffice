/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.configuration.installationfinder.msoffice;


import java.util.ArrayList;
import java.util.List;

import org.evolvis.liboffice.configuration.installationfinder.AbstractInstallationFinder;
import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.configuration.parser.PlainOfficeConfigurationParser;
import org.evolvis.liboffice.utils.SystemInfo;

import ca.beq.util.win32.registry.RegistryKey;
import ca.beq.util.win32.registry.RootKey;

/**
 * 
 * This class is an installation-finder for MS-Office-Systems. It does a registry-lookup in order to get the relevant information.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MsOfficeInstallationFinder extends AbstractInstallationFinder
{
	// Java 1.5: public List<OfficeConfiguration> getConfigurations()
	public List getConfigurations()
	{
		// Java 1.5: List<OfficeConfiguration> configs = new ArrayList<OfficeConfiguration>();
		List configs = new ArrayList();
		
		if(SystemInfo.isWindowsSystem())
		{		
			List officeTypes = PlainOfficeConfigurationParser.getInstance().getOfficeTypes(); // Get a list of all office types
			
			for(int i=0; i < officeTypes.size(); i++)
			{
				OfficeType type = (OfficeType)officeTypes.get(i);
				
				if(type.getShortName().equals("msoffice")) // Only look at MS-Office-Types of course
				{
					// Lookup registry
					String path = getPathFromWindowsRegistry(type.getVersionName());
					
					if(path != null && path.length() > 0)
					{
						OfficeConfiguration conf = new OfficeConfiguration();
						conf.setPath(path);
						conf.setType(type);
						//conf.setChecksum(checksum(conf.getPath()+type.getExec()));
						configs.add(conf);
					}
				}
			}
		}
		return configs;
	}
	
	private String getPathFromWindowsRegistry(String pVersionName)
	{
		String path = null;
		
		// First try the registry of the current user

		RegistryKey	r = new RegistryKey(RootKey.HKEY_CURRENT_USER, "Software\\Microsoft\\Office\\"+pVersionName+"\\Word\\Options");
		
		if(r.exists()) path = r.getValue("PROGRAMDIR").getData().toString();
		else
		{
			// If nothing found, try global reg
			r = new RegistryKey(RootKey.HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Office\\"+pVersionName+"\\Word\\InstallRoot");
			if(r.exists()) path = r.getValue("Path").getData().toString();
		}
		return path;		
	}
}