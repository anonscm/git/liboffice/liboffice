/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 14.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.testhelper;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.controller.KnownOfficeFileFormats;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.controller.OfficeControllerToolKit;
import org.evolvis.liboffice.controller.ReplacementToolKit;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.factory.ControllerFactory;
import org.evolvis.liboffice.factory.MainOfficeFactory;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TestHelper
{
	
	public final static String CONST_TESTDOC_1                        = "TestDocToolkitTest_1";
	public final static String CONST_TESTDOC_2                        = "TestDocToolkitTest_2";
	
	public final static Object OPENOFFICE                             = "OPENOFFICE"; 
	public final static Object MSWORD                                 = "MSWORD"; 
	
	private static Object m_oOfficeSuite                              = null;
	//private static String m_sTempPath                                 = null;
	private static MainOfficeFactory m_oMainOfficeFactory             = null;
	private static ControllerFactory m_oControllerFactory             = null;
	private static OfficeController m_oOfficeController               = null;
	private static OfficeControllerToolKit m_oOfficeControllerToolKit = null;
	private static ReplacementToolKit replacementToolKit				= null;
	
	private final static String CONST_TESTDOC_PATH                    = "/de/tarent/documents/testhelper/resources/";
	
	private final static Logger logger                                = Logger.getLogger(TestHelper.class);
	
	//private static boolean USE_NAMED_PIPE                             = true;
	//private static final String PIPE_NAME                             = "doctopus_Office";
	
	/*
	static
	{
		Properties oProperties = new Properties();
		try
		{
			InputStream inputStream = TestHelper.class.getResourceAsStream("/de/tarent/documents/testhelper/testhelper.properties");
			if (inputStream != null)
			{
				oProperties.load(inputStream);
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String oOfficeSuite = oProperties.getProperty("OfficeSuite", OPENOFFICE.toString());
		if (oOfficeSuite.equalsIgnoreCase(OPENOFFICE.toString()))
		{
			m_oOfficeSuite = OPENOFFICE;
		}
		else if (oOfficeSuite.equalsIgnoreCase(MSWORD.toString()))
		{
			m_oOfficeSuite = MSWORD;
		}
		else throw new RuntimeException("Unknown Officesuite in TestHelper possible values are (OPENOFFICE, MSWORD)");
		
		m_sTempPath = oProperties.getProperty("TempPath", "/tmp/");
		
		if (!(m_sTempPath.endsWith(File.separator))) m_sTempPath += File.separator; 
	}
	*/
	
	private static void initOffice()
	{
		if(!OfficeAutomat.getOfficeConfigurator().loadConfiguration(null))
		{
			OfficeAutomat.getOfficeConfigurator().setup(null);
			OfficeAutomat.getOfficeConfigurator().saveConfiguration(null);
		}
		
		if(OfficeAutomat.getOfficeConfigurator().getPreferredOffice() == null)
		{
			logger.error("You did not properly configure an office-environment! exiting...");
			System.exit(1);
		}
		
		OfficeAutomat.getOfficeConfigurator().getPreferredOffice().setupEnvironmentVariables();
	}
	
	public static Object getOfficeSuite()
	{
		if(m_oOfficeSuite == null)
		{
			initOffice();
			if(OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().compareTo(new OfficeType("msoffice")) == 0)
				m_oOfficeSuite = TestHelper.MSWORD;
			else
				m_oOfficeSuite = TestHelper.OPENOFFICE;
		}
		return m_oOfficeSuite;
	}
	
	public static OfficeFileFormat getOfficeFileFormat()
	{
		if (MSWORD.equals(getOfficeSuite()))
		{
			return KnownOfficeFileFormats.FILE_TYPE_DOC;
		}
		else return KnownOfficeFileFormats.FILE_TYPE_SXW;
	}
	
	public static MainOfficeFactory getMainOfficeFactory() throws NoOfficeException
	{
		if (m_oMainOfficeFactory == null)
		{
			initOffice();
			m_oMainOfficeFactory = OfficeAutomat.getInstance().getMainOfficeFactory();
		}
		return m_oMainOfficeFactory;    
	}
	
	public static ControllerFactory getControllerFactory() throws NoOfficeException
	{
		if (m_oControllerFactory == null)
		{
			m_oControllerFactory = getMainOfficeFactory().getControllerFactory();
		}
		return m_oControllerFactory;    
	}
	
	public static OfficeController getOfficeController() throws NoOfficeException
	{
		if (m_oOfficeController == null)
		{
			m_oOfficeController = getControllerFactory().getOfficeController();
		}
		return m_oOfficeController;    
	}
	
	public static OfficeControllerToolKit getOfficeControllerToolKit() throws NoOfficeException
	{
		if (m_oOfficeControllerToolKit == null)
		{
			m_oOfficeControllerToolKit = getControllerFactory().getOfficeControllerToolKit();
		}
		return m_oOfficeControllerToolKit;    
	}
	
	public static ReplacementToolKit getReplacementToolKit() throws NoOfficeException
	{
		if(replacementToolKit == null) replacementToolKit = getControllerFactory().getReplacementToolKit();
		return replacementToolKit;
	}
	
	public static String getTempPath()
	{
		return System.getProperty("java.io.tmpdir");
	}
	
	public static String getDocumentFileExtension()
	{
		return getOfficeSuite().equals(TestHelper.MSWORD) ? ".doc" : ".sxw";
	}
	
	
	
	/**
	 * Laedt ein TextDocument anhand des uebergebenen Dateinamens aus dem Classpath(/de/tarent/documents/testhelper/resources/). 
	 * @param fileName der Dateiname ohne Extension
	 * @return oDocument das TestDokument
	 * @throws NoOfficeException
	 */
	public static TextDocument loadTestDocument(String fileName) throws NoOfficeException
	{
		String file = getTestDocumentFileName(fileName);
		TextDocument oDocument = TestHelper.getOfficeController().loadDocument( file );
		return oDocument;
	}
	
	
	public static String getTestDocumentFileName(String fileName)
	{
		String path = CONST_TESTDOC_PATH + fileName + TestHelper.getDocumentFileExtension();
		logger.debug("path: " + path);
		URL url =  TestHelper.class.getResource(path);
		
		if (null == url) throw new java.lang.RuntimeException("Testfile not found filename:"+path);
		try
		{
			String file = new File(URLDecoder.decode(url.getPath(), "UTF-8")).getAbsolutePath();
			logger.debug("file: "+ file);
			return file;
		} 
		catch (UnsupportedEncodingException e)
		{
			throw new java.lang.RuntimeException(e);
		}
	}
}
