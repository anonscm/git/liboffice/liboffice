/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 18.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.virtualdocument.text;

import junit.framework.TestCase;

import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VirtualStyledTextTest extends TestCase
{
    private final static String CONST_PARA_1 = "hallo";
    private final static String CONST_PARA_2 = "welt";
    
    private final static String CONST_STYLE_1 = "style1";
    private final static String CONST_STYLE_2 = "style2";
    
    private VirtualStyledText txt;
    protected void setUp() throws Exception
    {
        txt = new VirtualStyledText();
    }
    
    public void testRemoveParagraph()
    {
        VirtualParagraph vPara1 = new VirtualParagraph("hallo");
        txt.addParagraph( vPara1 );
        VirtualParagraph vPara2 =  new VirtualParagraph("welt");
        txt.addParagraph( vPara2 );
        
        txt.removeParagraph( vPara2 );
        assertEquals(txt.getNumberOfParagraphs(), 1);
        txt.removeParagraph( vPara1 );
    }
    
    
    public void testGetText()
    {
        txt.addParagraph(new VirtualParagraph( CONST_PARA_1 ));
        txt.addParagraph(new VirtualParagraph( CONST_PARA_2 ));
        
        String out = txt.getText();
        assertEquals(out, CONST_PARA_1+" "+CONST_PARA_2);
        
    }
    
    public void testFindComponentByStyle()
    {
        txt.addParagraph(new VirtualParagraph(new VirtualTextPortionText(CONST_PARA_1, CONST_STYLE_1)));
        txt.addParagraph(new VirtualParagraph(new VirtualTextPortionText(CONST_PARA_2, CONST_STYLE_2)));
        
        assertNotNull(txt.findComponentByStyle(CONST_STYLE_1));
        assertTrue(txt.containsStyle(CONST_STYLE_1));
        assertNotNull(txt.findComponentByStyle(CONST_STYLE_2));
        assertTrue(txt.containsStyle(CONST_STYLE_2));
    }

}
