/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 25.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.controller;



import junit.framework.TestCase;

import org.evolvis.liboffice.descriptors.StyleTableDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.locator.TextContentRange;
import org.evolvis.liboffice.test.testhelper.TestHelper;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OfficeControllerReplaceTest extends TestCase
{
	private TextDocument m_oDocument = null;
	
	public OfficeControllerReplaceTest(String name)
	{
		super(name);
	}
	
	protected void setUp()
	{    
	}
	
	protected void tearDown() throws NoOfficeException
	{   
		if (m_oDocument != null) m_oDocument.close( true);
	}
	
	public void testInsertTablesDocument() throws NoOfficeException
	{    
		m_oDocument = createTableTestDocument();
		m_oDocument.insertStyle( new VirtualTextStyle("Table1"));
		m_oDocument.insertStyle( new VirtualTextStyle("Table2"));
		
		VirtualTextTable table1 = createVirtualTextTable(3,3, "Table1");
		VirtualTextTable table2 = createVirtualTextTable(4,4, "Table2");
		TestHelper.getReplacementToolKit().replaceFirstMarkerByComponent(m_oDocument, "MARKER1", table1);
		TestHelper.getReplacementToolKit().replaceFirstMarkerByComponent(m_oDocument, "MARKER2", table2);    
		
		
		// testen was da nun zwischen liegt...
		TextDocumentComponent compB1 = m_oDocument.locateTextDocumentComponent( new StyleTableDescriptor("Table1"));
		TextDocumentComponent compB2 = m_oDocument.locateTextDocumentComponent( new StyleTableDescriptor("Table2"));
		TextContentLocation locationB1 = compB1.getTextContentLocationAfter();
		TextContentLocation locationB2 = compB2.getTextContentLocation();
		TextContentRange range2 = m_oDocument.getDocumentRange();
		range2.setRange(locationB1, locationB2);
		range2.select();
		String plainText2 = range2.getPlainText();
		
		int length2 = plainText2.length();
		assertEquals("zus�tzliche Zeichen zwischen den Tabellen", 0, length2);
	}
	
	private VirtualTextTable createVirtualTextTable(int rows, int columns, String styleName)
	{
		VirtualTextTable table = new VirtualTextTable();
		table.setSize(rows, columns);
		for(int rowIndex=0; rowIndex<rows; rowIndex++)
		{
			for(int colIndex=0; colIndex<rows; colIndex++)
			{
				VirtualTextTableCell cell = new VirtualTextTableCell("Zelle " + rowIndex + ", " + colIndex, styleName); 
				table.setTextTableCell(rowIndex, colIndex, cell);
			}      
		}
		
		return table;
	}
	
	private TextDocument createTableTestDocument() throws NoOfficeException
	{
		TextDocument oDocument = TestHelper.getOfficeController().newDocument();
		assertNotNull(oDocument);
		
		oDocument.jumpToLocation( oDocument.getEnd());
		
		oDocument.insertText( new VirtualStyledText("Markertest Beginn", true));
		oDocument.insertMarker( new VirtualTextPortionMarker("MARKER1", null));
		//oDocument.insertText( new VirtualStyledText("\n"));
		oDocument.insertMarker( new VirtualTextPortionMarker("MARKER2", null));
		oDocument.insertText( new VirtualStyledText("\n"));
		oDocument.insertText( new VirtualStyledText("Markertest Ende", true));
		
		return oDocument;
	}
}