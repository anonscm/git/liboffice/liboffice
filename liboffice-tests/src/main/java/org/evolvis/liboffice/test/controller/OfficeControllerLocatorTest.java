/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 14.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.controller;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.descriptors.CellTextTableDescriptor;
import org.evolvis.liboffice.descriptors.FrameDescriptor;
import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.descriptors.StyleTableDescriptor;
import org.evolvis.liboffice.descriptors.StyledParagraphDescriptor;
import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextFrame;
import org.evolvis.liboffice.document.TextMarker;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.test.testhelper.TestHelper;
import org.evolvis.liboffice.virtualdocument.VirtualTextFrame;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;


/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OfficeControllerLocatorTest extends TestCase
{
	private TextDocument m_oDocument = null;
	private VirtualTextStyle m_oTestStyle;
	private VirtualStyledText m_oTestText; 
	private String m_sTestString  = "suchMich";
	private String m_sMarkerName  = "testMarker";
	private String m_sMarkerHint  = "testHint";
	
	private final Logger logger = Logger.getLogger( getClass() );
	
	public OfficeControllerLocatorTest(String name)
	{
		super(name);
	}
	
	protected void setUp() throws NoOfficeException
	{    
		logger.debug("setup called");
		
		m_oDocument = TestHelper.getOfficeController().newDocument();
		assertNotNull(m_oDocument);
		
		m_oTestStyle = new VirtualTextStyle("TESTSTYLE");
		assertNotNull(m_oTestStyle);
		
		boolean wasInserted = m_oDocument.insertStyle( m_oTestStyle);
		assertTrue(wasInserted);
		
		m_oTestText = new VirtualStyledText(m_sTestString, m_oTestStyle.getStyleName()); 
		assertNotNull(m_oTestText);
	}
	
	protected void tearDown() throws NoOfficeException
	{   
		logger.debug("tearDown");
		if (m_oDocument != null)
		{
			boolean wasClosed = m_oDocument.close(true);
			assertTrue(wasClosed);
			m_oDocument = null;      
		}
	}
	
	
	// ---------------------- TESTS ----------------------
	
	public void testFindStyledText() throws NoOfficeException
	{    
		insertTestText();
		findStyledText();
	}  
	
	public void testFindText() throws NoOfficeException
	{    
		insertTestText();
		findText();
	}
	
	// -----------------------------------------------------------
	// -----------------------------------------------------------------
	
	public void testFindTextTableInUnknownCell() throws NoOfficeException
	{    
		insertTestTable();
		findTextTableInUnknownCell();
		
	}  
	
	public void testFindTextTableInKnownCell() throws NoOfficeException
	{    
		insertTestTable();
		findTextTableInKnownCell();
	}  
	
	public void testFindStyledTableInUnknownCell() throws NoOfficeException
	{    
		insertTestTable();
		findStyledTableInUnknownCell();
	}  
	
	public void testFindStyledTableInKnownCell() throws NoOfficeException
	{    
		insertTestTable();
		findStyledTableInKnownCell();
	}
	
	
	public void testFindTextInFrame() throws NoOfficeException
	{    
		insertFrame();
		insertTestText();
		findText();
	}
	
	public void testFindStyledTextInFrame() throws NoOfficeException
	{    
		insertFrame();
		insertTestText();
		findStyledText();
	}
	
	public void testFindTableInFrameInKnownCell() throws NoOfficeException
	{    
		insertFrame();
		insertTestTable();
		findTextTableInKnownCell();
	}
	
	public void testFindTableInFrameInUnknownCell() throws NoOfficeException
	{    
		insertFrame();
		insertTestTable();
		findTextTableInUnknownCell();
	}
	
	
	public void testFindFrameByText() throws NoOfficeException
	{    
		insertFrame();
		insertTestText();
		findFrameByText();
	}
	
	
	public void testFindFrameByStyledText() throws NoOfficeException
	{    
		insertFrame();
		insertTestText();
		findFrameByStyledText();
	}
	
	
	public void testFindSimpleMarker() throws NoOfficeException
	{    
		insertSimpleTestMarker();
		findSimpleMarker();
	}
	
	public void testFindExtendedMarker() throws NoOfficeException
	{    
		insertExtendedTestMarker();
		findExtendedMarker();
	}
	
	public void testFindSimpleMarkerInFrame() throws NoOfficeException
	{    
		insertFrame();
		insertSimpleTestMarker();
		findSimpleMarker();
	}
	
	public void testFindExtendedMarkerInFrame() throws NoOfficeException
	{    
		insertFrame();
		insertExtendedTestMarker();
		findExtendedMarker();
	}
	
	public void testFindSimpleMarkerInTable() throws NoOfficeException
	{    
		insertTestSimpleMarkerTable();
		findSimpleMarker();
	}
	
	public void testFindExtendedMarkerInTable() throws NoOfficeException
	{    
		insertTestExtendedMarkerTable();
		findExtendedMarker();
	}
	
	
	// -----------------------------------
	
	public void findStyledText() throws NoOfficeException
	{    
		TextContentDescriptor descriptor = new StyledParagraphDescriptor(m_sTestString, m_oTestStyle.getStyleName()); 
		assertNotNull(descriptor);
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( descriptor);
		assertNotNull(component);
	}
	
	
	public void findText() throws NoOfficeException
	{    
		TextContentDescriptor descriptor = new StyledParagraphDescriptor(m_sTestString, null); 
		assertNotNull(descriptor);
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( descriptor);
		assertNotNull(component);
	}
	
	
	
	
	public void findFrameByText() throws NoOfficeException
	{        
		TextContentDescriptor descriptor = new StyledParagraphDescriptor(m_sTestString, null); 
		assertNotNull(descriptor);
		
		FrameDescriptor frameDescriptor = new FrameDescriptor(descriptor); 
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( frameDescriptor);
		assertNotNull(component);
	}
	
	public void findFrameByStyledText() throws NoOfficeException
	{        
		TextContentDescriptor descriptor = new StyledParagraphDescriptor(m_sTestString, m_oTestStyle.getStyleName()); 
		assertNotNull(descriptor);
		
		FrameDescriptor frameDescriptor = new FrameDescriptor(descriptor); 
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( frameDescriptor);
		assertNotNull(component);
	}
	
	
	// -----------------------------------
	
	
	public void findTextTableInUnknownCell() throws NoOfficeException
	{    
		TextContentDescriptor descriptor = new CellTextTableDescriptor(m_sTestString,-1,-1); 
		assertNotNull(descriptor);
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent(descriptor);
		assertNotNull(component);
	}
	
	
	public void findTextTableInKnownCell() throws NoOfficeException
	{    
		// nun haben wir eine Tabelle in deren Zelle 1,1 der 
		// gesuchte Text steht... alle anderen Zellen sind "uninteressant"
		
		TextContentDescriptor descriptorA = new CellTextTableDescriptor(m_sTestString, 0,0); 
		assertNotNull(descriptorA);
		
		TextContentDescriptor descriptorB = new CellTextTableDescriptor(m_sTestString, 0,1); 
		assertNotNull(descriptorB);
		
		TextContentDescriptor descriptorC = new CellTextTableDescriptor(m_sTestString, 1,0); 
		assertNotNull(descriptorC);
		
		TextContentDescriptor descriptorD = new CellTextTableDescriptor(m_sTestString, 1,1); 
		assertNotNull(descriptorD);
		
		TextDocumentComponent component;
		
		// sollte hier nicht gefunden werden...
		component = m_oDocument.locateTextDocumentComponent( descriptorA);
		assertNull(component);
		
		// sollte hier nicht gefunden werden...
		component = m_oDocument.locateTextDocumentComponent( descriptorB);
		assertNull(component);
		
		// sollte hier nicht gefunden werden...
		component = m_oDocument.locateTextDocumentComponent( descriptorC);
		assertNull(component);
		
		// hier sollte etwas gefunden werden...
		component = m_oDocument.locateTextDocumentComponent( descriptorD);
		assertNotNull(component);
	}
	
	public void findStyledTableInUnknownCell() throws NoOfficeException
	{    
		TextContentDescriptor descriptor = new StyleTableDescriptor(m_oTestStyle.getStyleName()); 
		assertNotNull(descriptor);
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent(descriptor);
		assertNotNull(component);
	}
	
	
	public void findStyledTableInKnownCell() throws NoOfficeException
	{    
		TextContentDescriptor descriptorA = new StyleTableDescriptor(m_oTestStyle.getStyleName(), 0,0); 
		assertNotNull(descriptorA);
		
		TextContentDescriptor descriptorB = new StyleTableDescriptor(m_oTestStyle.getStyleName(), 0,1); 
		assertNotNull(descriptorB);
		
		TextContentDescriptor descriptorC = new StyleTableDescriptor(m_oTestStyle.getStyleName(), 1,0); 
		assertNotNull(descriptorC);
		
		TextContentDescriptor descriptorD = new StyleTableDescriptor(m_oTestStyle.getStyleName(), 1,1); 
		assertNotNull(descriptorD);
		
		TextDocumentComponent component;
		
		component = m_oDocument.locateTextDocumentComponent( descriptorA);
		assertNull(component);
		
		component = m_oDocument.locateTextDocumentComponent( descriptorB);
		assertNull(component);
		
		component = m_oDocument.locateTextDocumentComponent( descriptorC);
		assertNull(component);
		
		component = m_oDocument.locateTextDocumentComponent( descriptorD);
		assertNotNull(component);
	}
	
	// -----------------------------------------------------------------
	
	public void findSimpleMarker() throws NoOfficeException
	{    
		TextContentDescriptor descriptor = new NameHintMarkerDescriptor(m_sMarkerName); 
		assertNotNull(descriptor);
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( descriptor);
		assertNotNull(component);
	}
	
	
	public void findExtendedMarker() throws NoOfficeException
	{    
		TextContentDescriptor descriptor = new NameHintMarkerDescriptor(m_sMarkerName, m_sMarkerHint); 
		assertNotNull(descriptor);
		
		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( descriptor);
		assertNotNull(component);
	}
	
	
	
	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	
	private void insertTestText() throws NoOfficeException
	{
		logger.debug("insertTestText start");
		boolean wasInserted = m_oDocument.insertText( m_oTestText);
		assertTrue(wasInserted);        
		logger.debug("insertTestText end");
	}
	
	private void insertTestTable() throws NoOfficeException
	{
		VirtualTextTable table = new VirtualTextTable();
		table.setSize(2,2);
		table.setTextTableCell(0,0, new VirtualTextTableCell("Hallo"));
		table.setTextTableCell(0,1, new VirtualTextTableCell("Toll"));
		table.setTextTableCell(1,0, new VirtualTextTableCell("Super"));
		table.setTextTableCell(1,1, new VirtualTextTableCell(m_oTestText));
		
		TextTable realTable = m_oDocument.insertTable( table);
		assertNotNull(realTable);        
	}
	
	
	private void insertTestSimpleMarkerTable() throws NoOfficeException
	{
		VirtualTextTable table = new VirtualTextTable();
		table.setSize(2,2);
		table.setTextTableCell(0,0, new VirtualTextTableCell("Hallo"));
		table.setTextTableCell(0,1, new VirtualTextTableCell("Toll"));
		table.setTextTableCell(1,0, new VirtualTextTableCell("Super"));
		table.setTextTableCell(1,1, new VirtualTextTableCell(new VirtualTextPortionMarker(m_sMarkerName, null)));
		
		TextTable realTable = m_oDocument.insertTable( table);
		assertNotNull(realTable);        
	}
	
	private void insertTestExtendedMarkerTable() throws NoOfficeException
	{
		VirtualTextTable table = new VirtualTextTable();
		table.setSize(2,2);
		table.setTextTableCell(0,0, new VirtualTextTableCell("Hallo"));
		table.setTextTableCell(0,1, new VirtualTextTableCell("Toll"));
		table.setTextTableCell(1,0, new VirtualTextTableCell("Super"));
		table.setTextTableCell(1,1, new VirtualTextTableCell(new VirtualTextPortionMarker(m_sMarkerName, m_sMarkerHint)));
		
		TextTable realTable = m_oDocument.insertTable(table);
		assertNotNull(realTable);        
	}
	
	
	private TextFrame insertFrame() throws NoOfficeException
	{
		VirtualTextFrame vFrame = new VirtualTextFrame(2000, 2000, 2000, 2000); 
		TextFrame frame = m_oDocument.insertTextFrame( vFrame);
		assertNotNull(frame);
		
		TextContentLocation location = frame.getFrameContentLocation();
		assertNotNull(location);
		
		boolean jumped = m_oDocument.jumpToLocation( location);
		assertTrue(jumped);
		
		return frame;
	}
	
	
	private void insertSimpleTestMarker() throws NoOfficeException
	{
		VirtualTextPortionMarker vMarker = new VirtualTextPortionMarker(m_sMarkerName, null); 
		TextMarker marker = m_oDocument.insertMarker( vMarker);
		assertNotNull(marker);        
	}
	
	private void insertExtendedTestMarker() throws NoOfficeException
	{
		VirtualTextPortionMarker vMarker = new VirtualTextPortionMarker(m_sMarkerName, m_sMarkerHint); 
		TextMarker marker = m_oDocument.insertMarker( vMarker);
		assertNotNull(marker);        
	}
	
	
	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	
	
	// ---------------------------------------------------
	
	public static void main(String[] args)
	{
	}
}
