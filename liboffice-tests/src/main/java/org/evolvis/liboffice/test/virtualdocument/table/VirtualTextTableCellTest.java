/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 18.07.2005
 *
 */
package org.evolvis.liboffice.test.virtualdocument.table;

import junit.framework.TestCase;

import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 */
public class VirtualTextTableCellTest extends TestCase
{
    private final static String CONST_CELL_1   = "hallo";
    private final static String CONST_CELL_2   = "welt";
    
    private final static String CONST_STYLE_1 = "style1";
    private final static String CONST_STYLE_2 = "style2";
    
    
    private VirtualTextTableCell cell;
    
    public void testContainsStyle()
    {
        cell = new VirtualTextTableCell(CONST_CELL_1, CONST_STYLE_1);
        assertTrue(cell.containsStyle(CONST_STYLE_1));
        cell = new VirtualTextTableCell();
        cell.setStyledText(new VirtualStyledText(new VirtualParagraph(new VirtualTextPortionText(CONST_CELL_2, CONST_STYLE_2))));
        assertTrue(cell.containsStyle(CONST_STYLE_2));
    }
    
    public void testFindComponentByStyle()
    {
        cell = new VirtualTextTableCell(CONST_CELL_1, CONST_STYLE_1);
        assertNotNull(cell.findComponentByStyle(CONST_STYLE_1));
        cell = new VirtualTextTableCell();
        cell.setStyledText(new VirtualStyledText(new VirtualParagraph(new VirtualTextPortionText(CONST_CELL_2, CONST_STYLE_2))));
        assertNotNull(cell.findComponentByStyle(CONST_STYLE_2));
    }
}
