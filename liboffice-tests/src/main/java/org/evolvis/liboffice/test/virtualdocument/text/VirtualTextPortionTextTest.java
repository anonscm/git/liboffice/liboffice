/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 18.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.virtualdocument.text;

import junit.framework.TestCase;

import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VirtualTextPortionTextTest extends TestCase
{
    private final static String CONST_POR_1 = "hallo";
    private final static String CONST_POR_2 = "welt";
    
    private final static String CONST_STYLE_1 = "style1";
    private final static String CONST_STYLE_2 = "style2"; 
    
    private VirtualTextPortionText por;
    
    
    
    public void testFindComponentByStyle()
    {
        por = new VirtualTextPortionText(CONST_POR_1, CONST_STYLE_1);
        assertTrue(por.containsStyle(CONST_STYLE_1));
        assertNotNull(por.findComponentByStyle(CONST_STYLE_1));
        
        por = new VirtualTextPortionText(CONST_POR_2, new VirtualTextStyle( CONST_STYLE_2));
        assertTrue(por.containsStyle(CONST_STYLE_2));
        assertNotNull( por.findComponentByStyle(CONST_STYLE_2));
    }
    
}
