/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 18.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.virtualdocument.table;

import junit.framework.TestCase;

import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VirtualTextTableTest extends TestCase
{
    private final static String CONST_CELL_1 = "cell 1";
    private final static String CONST_CELL_2 = "cell 2";
    private final static String CONST_CELL_3 = "cell 3";
    private final static String CONST_CELL_4 = "cell 4";
    private final static String CONST_CELL_5 = "cell 5";
    private final static String CONST_CELL_6 = "cell 6";
    
    
    private final static String CONST_STYLE_1 = "style1";
    private final static String CONST_STYLE_2 = "style2";
    private final static String CONST_STYLE_3 = "style3";
    private final static String CONST_STYLE_4 = "style4";
    private final static String CONST_STYLE_5 = "style5";
    private final static String CONST_STYLE_6 = "style6";
    
    private VirtualTextTable table;
    
    private void initSimpleTable()
    {
        table = new VirtualTextTable();
        table.setSize(2,2);
        table.setTextTableCell(0, 0, new VirtualTextTableCell(CONST_CELL_1, CONST_STYLE_1) );
        table.setTextTableCell(0, 1, new VirtualTextTableCell(CONST_CELL_2, CONST_STYLE_2) );
        table.setTextTableCell(1, 0, new VirtualTextTableCell(CONST_CELL_3, CONST_STYLE_3) );
        table.setTextTableCell(1, 1, new VirtualTextTableCell(CONST_CELL_4, CONST_STYLE_4) );
    }
    
    private void initExtendedTable()
    {
        table = new VirtualTextTable();
        table.setSize(3,3);
        table.setTextTableCell(0, 0, new VirtualTextTableCell(CONST_CELL_1, CONST_STYLE_1, 1, 3) );
        table.setTextTableCell(1, 0, new VirtualTextTableCell(CONST_CELL_2, CONST_STYLE_2, 2, 1) );
        table.setTextTableCell(1, 1, new VirtualTextTableCell(CONST_CELL_3, CONST_STYLE_3) );
        table.setTextTableCell(1, 2, new VirtualTextTableCell(CONST_CELL_4, CONST_STYLE_4) );
        table.setTextTableCell(2, 1, new VirtualTextTableCell(CONST_CELL_5, CONST_STYLE_5) );
        table.setTextTableCell(2, 2, new VirtualTextTableCell(CONST_CELL_6, CONST_STYLE_6) );
        
    }
    
    
    
    public void testFindComponentByStyle()
    {
        initSimpleTable();
        assertNotNull( table.findComponentByStyle(CONST_STYLE_1) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_2) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_3) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_4) );
        
        initExtendedTable();
        assertNotNull( table.findComponentByStyle(CONST_STYLE_1) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_2) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_3) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_4) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_5) );
        assertNotNull( table.findComponentByStyle(CONST_STYLE_6) );
    }
    
    
    public void testContainsStyle()
    {
        initSimpleTable();
        assertTrue( table.containsStyle(CONST_STYLE_1) );
        assertTrue( table.containsStyle(CONST_STYLE_2) );
        assertTrue( table.containsStyle(CONST_STYLE_3) );
        assertTrue( table.containsStyle(CONST_STYLE_4) );
        
        initExtendedTable();
        assertTrue( table.containsStyle(CONST_STYLE_1) );
        assertTrue( table.containsStyle(CONST_STYLE_2) );
        assertTrue( table.containsStyle(CONST_STYLE_3) );
        assertTrue( table.containsStyle(CONST_STYLE_4) );
        assertTrue( table.containsStyle(CONST_STYLE_5) );
        assertTrue( table.containsStyle(CONST_STYLE_6) );
        
    }
    
    
    
}
