/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.test.controller;

import java.util.Date;

import junit.framework.TestCase;

import org.evolvis.liboffice.controller.ComponentReplaceJob;
import org.evolvis.liboffice.controller.ComponentReplaceSet;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.test.testhelper.TestHelper;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;


/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OfficeControllerToolkitTest extends TestCase
{
	private final static String CONST_KEY_1        = "key1";
	private final static String CONST_KEY_2        = "key2";
	private final static String CONST_KEY_3        = "key3";
	private final static String CONST_KEY_4        = "key4";

	private final static String CONST_DUMMY_1      = "foo";
	private final static String CONST_DUMMY_2      = "bar";
	private final static String CONST_DUMMY_3      = "baz";
	private final static String CONST_DUMMY_4      = "quux";

	private final static String CONST_STYLE_1      = "style1";
	private final static String CONST_STYLE_2      = "style2";

	private TextDocument starterdoc                = null;

	protected void tearDown() throws NoOfficeException
	{   
		if (starterdoc != null) starterdoc.close( true);
	}




	private TextDocument getTestDocument(String fileName,String comment) throws NoOfficeException
	{
		starterdoc = TestHelper.loadTestDocument( fileName );

		String dateString = (new Date()).toString(); 
		assertNotNull("Testdocument is null", starterdoc );
		starterdoc.jumpToLocation( starterdoc.getEnd());
		starterdoc.insertText( new VirtualStyledText("Erstellungsdatum: " + dateString, true));

		if (comment != null) starterdoc.insertText( new VirtualStyledText("Kommentar: " + comment));

		assertNotNull(starterdoc);
		return starterdoc;
	}


	private VirtualTextTable getDummyTable(int cols, int rows, String text, String styleName)
	{
		VirtualTextTable table = new VirtualTextTable();
		table.setSize(rows, cols);
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < rows; j++)
			{
				table.setTextTableCell(i, j, new VirtualTextTableCell(i + " : " + j + text, styleName));
			}
		}
		return table;
	}

	public void testGetNumberOfMarkerOccurrences() throws NoOfficeException
	{
		starterdoc             = getTestDocument(TestHelper.CONST_TESTDOC_1,  "getNumberOfMarkerOccurences");
		int result             = TestHelper.getOfficeControllerToolKit().getNumberOfMarkerOccurrences(starterdoc, CONST_KEY_1, null);
		assertEquals(1, result);
	}


	public void testReplaceAllMarkersByComponent() throws NoOfficeException
	{
		starterdoc             = getTestDocument(TestHelper.CONST_TESTDOC_1,  "replaceAllMarkersByComponent");

		ComponentReplaceSet set = new ComponentReplaceSet();
		set.addJob(new ComponentReplaceJob(CONST_KEY_1, getDummyTable(1,1, CONST_DUMMY_1, CONST_STYLE_1)));
		set.addJob(new ComponentReplaceJob(CONST_KEY_2, getDummyTable(3,3, CONST_DUMMY_2, CONST_STYLE_2)));
		set.addJob(new ComponentReplaceJob(CONST_KEY_3, new VirtualParagraph(CONST_DUMMY_3 )));
		set.addJob(new ComponentReplaceJob(CONST_KEY_4, new VirtualParagraph(CONST_DUMMY_4 )));

		boolean result           = TestHelper.getReplacementToolKit().replaceAllMarkersByComponent(starterdoc, set);
		assertTrue( result );

		int resultKey1           =  TestHelper.getOfficeControllerToolKit().getNumberOfMarkerOccurrences(starterdoc, CONST_KEY_1, null);
		int resultKey2           =  TestHelper.getOfficeControllerToolKit().getNumberOfMarkerOccurrences(starterdoc, CONST_KEY_2, null);
		int resultKey3           =  TestHelper.getOfficeControllerToolKit().getNumberOfMarkerOccurrences(starterdoc, CONST_KEY_3, null);
		int resultKey4           =  TestHelper.getOfficeControllerToolKit().getNumberOfMarkerOccurrences(starterdoc, CONST_KEY_4, null);

		assertEquals(0, resultKey1);
		assertEquals(0, resultKey2);
		assertEquals(0, resultKey3);
		assertEquals(0, resultKey4);

	}


	public void testInsertDocument() throws NoOfficeException
	{
		starterdoc        = TestHelper.loadTestDocument( TestHelper.CONST_TESTDOC_1 );
		assertNotNull("Das Starterdoc ist null", starterdoc );
		// TODO copy styles from startedoc to docInsert
		TextDocument docInsert         = TestHelper.loadTestDocument( TestHelper.CONST_TESTDOC_2 );
		assertNotNull("Das einzufuegende Dokument ist null", docInsert);
		VirtualTextDocument vDocInsert = TestHelper.getMainOfficeFactory().getParserFactory().getTextDocumentParser().parseTextDocument( docInsert );
		docInsert.close();
		boolean returnValue            = TestHelper.getOfficeControllerToolKit().insertDocument(starterdoc, vDocInsert);
		assertTrue("insertDocument hat false zurueck geliefert", returnValue);
	}


	public void testRemoveAllMarkers() throws NoOfficeException
	{
		starterdoc        =  TestHelper.loadTestDocument( TestHelper.CONST_TESTDOC_1 );
		assertNotNull("starterdoc is null", starterdoc);
		boolean ret                    =  TestHelper.getOfficeControllerToolKit().removeAllMarkers(starterdoc);
		assertTrue("RemoveAllMarkers hat false zurueck geliefert", ret);
		int amountMarkers              = TestHelper.getOfficeControllerToolKit().getNumberOfMarkers( starterdoc );
		assertEquals("Es wurden nicht alle Marker aus dem Dokument entfernt", amountMarkers, 0);

	}

	public void testRemoveMarker() throws NoOfficeException
	{
		starterdoc = TestHelper.loadTestDocument( TestHelper.CONST_TESTDOC_1 );
		boolean ret             = TestHelper.getOfficeControllerToolKit().removeMarker( starterdoc,  CONST_KEY_1, null);
		assertTrue("RemoveMarker hat false zurueck geliefert", ret);
		int amount              = TestHelper.getOfficeControllerToolKit().getNumberOfMarkerOccurrences(starterdoc, CONST_KEY_1, null);
		assertEquals("Der Marker wurde nicht aus dem Dokument entfernt", 0, amount);
	}

	public void testJumpToMarker() throws NoOfficeException
	{
		starterdoc  = TestHelper.loadTestDocument( TestHelper.CONST_TESTDOC_1 );
		boolean ret             = TestHelper.getOfficeControllerToolKit().jumpToMarker( starterdoc,  CONST_KEY_1);
		assertTrue("jumpToMarker 1 hat false zurueck geliefert", ret);

		ret             = TestHelper.getOfficeControllerToolKit().jumpToMarker( starterdoc,  CONST_KEY_2);
		assertTrue("jumpToMarker 2 hat false zurueck geliefert", ret);

		ret             = TestHelper.getOfficeControllerToolKit().jumpToMarker( starterdoc,  CONST_KEY_3);
		assertTrue("jumpToMarker 3 hat false zurueck geliefert", ret);

		ret             = TestHelper.getOfficeControllerToolKit().jumpToMarker( starterdoc,  CONST_KEY_4);
		assertTrue("jumpToMarker 4 hat false zurueck geliefert", ret);

		ret             = TestHelper.getOfficeControllerToolKit().jumpToMarker( starterdoc,  "gibts bestimmt nicht");
		assertFalse("jumpToMarker auf nicht vorhandenen Marker hat true zurueck geliefert", ret);
	}
}
