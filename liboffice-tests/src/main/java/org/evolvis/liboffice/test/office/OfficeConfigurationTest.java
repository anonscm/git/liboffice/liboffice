/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/**
 * 
 */
package org.evolvis.liboffice.test.office;

import junit.framework.TestCase;

import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;
import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.controller.openoffice.OOoOfficeController;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeConfigurationTest extends TestCase
{
	private OfficeConfiguration conf;

	protected void setUp() throws Exception
	{
		conf = new OfficeConfiguration();
		
		// Fail test-case if conf is null
		assertNotNull(conf);		
	}

	protected void tearDown() throws Exception
	{
		
	}
	
	public void testSetPath() throws Exception
	{
		String testPath = "testPath";
		
		// Set Path to testPath
		conf.setPath(testPath);
		
		// Check if getPath returns testPath
		assertEquals(conf.getPath(), testPath);
		
		conf.setPath(null);
		
		assertEquals(conf.getPath(), null);
	}
	
	public void testSetType() throws Exception
	{
		OfficeType testType = new OfficeType();
		
		conf.setType(testType);
		
		assertEquals(conf.getType(), testType);
		
		conf.setType(null);
		
		assertEquals(conf.getType(), null);
	}
	
	public void testSetConnectionMethod() throws Exception
	{		
		OfficeConnectionMethod testConnMethod = OOoOfficeController.PIPE_CONN_METHOD;
		
		conf.setConnectionMethod(testConnMethod);
		
		assertEquals(conf.getConnectionMethod(), testConnMethod);
		
		assertEquals(conf.getConnectionMethod().getID(), "pipe");
	}
	
	public void testSetChecksum() throws Exception
	{
		String testChecksum = "40da4Dfjkl324fdvFp2Q43r4";
		
		conf.setChecksum(testChecksum);
		
		assertEquals(conf.getChecksum(), testChecksum);
		
		conf.setChecksum(null);
		
		assertEquals(conf.getChecksum(), null);
	}
	
	public void testSetPreferred() throws Exception
	{
		assertFalse(conf.isPreferred());
		
		conf.setPreferred(true);
		
		assertTrue(conf.isPreferred());
		
		conf.setPreferred(false);
		
		assertFalse(conf.isPreferred());
	}
	
	
}
