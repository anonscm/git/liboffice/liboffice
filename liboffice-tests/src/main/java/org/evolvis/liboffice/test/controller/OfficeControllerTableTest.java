/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 14.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.controller;

import junit.framework.TestCase;

import org.evolvis.liboffice.descriptors.CellTextTableDescriptor;
import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.document.TextTable;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.parser.TextTableParser;
import org.evolvis.liboffice.test.testhelper.TestHelper;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OfficeControllerTableTest extends TestCase
{
  private TextDocument m_oDocument = null;
  
  public OfficeControllerTableTest(String name)
  {
    super(name);
  }

  protected void setUp() throws NoOfficeException
  {    
      if (null == m_oDocument) m_oDocument = TestHelper.getOfficeController().newDocument();
  }
  
  protected void tearDown() throws NoOfficeException
  {   
    if (m_oDocument != null) m_oDocument.close(true);
  }

  
  // ---------------------- TESTS ----------------------
  
  public void testSetTableColumnWidth() throws NoOfficeException
  {    
    testSetTableColumnWidth(1000);
  }
  
  private void testSetTableColumnWidth(int columnWidth) throws NoOfficeException
  {    
    int columnIndex = 0;
    String searchString = "suchMich";
    
    //TextDocument oDocument = TestHelper.getOfficeController().newDocument();
    
    VirtualTextTable vTable = new VirtualTextTable();
    vTable.setSize(2,2);
    vTable.setTextTableCell(0,0, new VirtualTextTableCell("Zelle 1"));
    vTable.setTextTableCell(0,1, new VirtualTextTableCell("Zelle 2"));
    vTable.setTextTableCell(1,0, new VirtualTextTableCell("Zelle 3"));
    vTable.setTextTableCell(1,1, new VirtualTextTableCell(searchString));

    vTable.setColumnWidth(columnIndex, new Integer(columnWidth));
    m_oDocument.insertTable( vTable);
    
    TextContentDescriptor descriptorA = new CellTextTableDescriptor(searchString, -1, -1); 
    TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( descriptorA);

    if (component != null)
    {
      if (component instanceof TextTable)
      {
        TextTable textTable = (TextTable)component;
        TextTableParser tableParser = TestHelper.getMainOfficeFactory().getParserFactory().getTextTableParser();
        VirtualTextTable virtualTable = tableParser.parseTextTable(m_oDocument, textTable);
        Object realColumnWidthObj = virtualTable.getColumnWidth(columnIndex);
        if (realColumnWidthObj != null)
        {
          if (realColumnWidthObj instanceof Integer)
          {
            int realColumnWidth = ((Integer)realColumnWidthObj).intValue();
            assertEquals(columnWidth, realColumnWidth, 1);
            m_oDocument.close( true);
          }          
        }
      }
    }
    // kaputtes Dokument offen lassen...
    assertTrue(true);
  }
  
  
  // ---------------------------------------------------
  
  public static void main(String[] args)
  {
  }
}
