/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 14.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.controller;

import java.io.File;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.evolvis.liboffice.descriptors.FormattedTextDescriptor;
import org.evolvis.liboffice.descriptors.TextContentDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.officefileformats.UnsupportedOfficeFileTypeException;
import org.evolvis.liboffice.test.testhelper.TestHelper;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;

/**
 * @author Niko R&uuml;ther, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OfficeControllerInsertDocumentTest extends TestCase
{
	private final static String  CONST_TEST_FILE = "InsertTestDoc";
	private TextDocument m_oDocument = null;
	private Logger logger = Logger.getLogger(getClass());
	
	private VirtualTextPortionText m_oFetterText = new VirtualTextPortionText("fetter Text", true, false, false);
	
	
	
	public OfficeControllerInsertDocumentTest(String name)
	{
		super(name);
	}
	
	protected void setUp() throws NoOfficeException
	{    
		logger.debug("setUp " + m_oDocument);
		if (null == m_oDocument)
		{
			m_oDocument = TestHelper.getOfficeController().newDocument();
		}
	}
	
	protected void tearDown() throws NoOfficeException
	{   
		if (m_oDocument != null) m_oDocument.close( true);
	}
	
	
	// ---------------------- TESTS ----------------------
	
	public void doInsertDocumentTest(boolean emptyLine) throws NoOfficeException, UnsupportedOfficeFileTypeException
	{
		TextDocument oSourceDocument = createSourceTestDocument("SourceTestDocument", false);
		assertNotNull(oSourceDocument);    
		
		TextDocument oInsertDocument = createInsertTestDocument("InsertTestDocument");
		assertNotNull(oInsertDocument);    
		
		String filename =  TestHelper.getTestDocumentFileName(CONST_TEST_FILE);
		assertTrue(new File(filename).exists());
		oInsertDocument.save(filename, TestHelper.getOfficeFileFormat(), true);
		oInsertDocument.close( true);
		
		
		oSourceDocument.insertDocumentAtStart(filename);
		boolean isGood = isInDocument(oSourceDocument, m_oFetterText);
		
		assertTrue(isGood);
		
		oSourceDocument.close( true);
	}
	
	
	public void testInsertDocument() throws NoOfficeException, UnsupportedOfficeFileTypeException
	{
		doInsertDocumentTest(false);
	}
	
	public void testInsertDocumentEmptyLine() throws NoOfficeException, UnsupportedOfficeFileTypeException
	{
		doInsertDocumentTest(true);
	}
	
	private boolean isInDocument(TextDocument oDocument, VirtualTextPortionText testText) throws NoOfficeException
	{
		TextContentDescriptor descriptor = new FormattedTextDescriptor(testText.getText(), testText.isBold(), testText.isItalic(), testText.isUnderline());
		TextDocumentComponent locatedComponent = oDocument.locateTextDocumentComponent( descriptor);
		return locatedComponent != null;
	}
	
	
	// ---------------------------------------------------
	
	
	private TextDocument createSourceTestDocument(String comment, boolean insertEmptyLine) throws NoOfficeException
	{
		TextDocument doc = TestHelper.getOfficeController().newDocument();
		
		VirtualStyledText fetterText = new VirtualStyledText(new VirtualParagraph(m_oFetterText));
		assertNotNull(fetterText);
		
		boolean jumped = doc.jumpToLocation(doc.getStart());
		assertTrue(jumped);
		
		if (insertEmptyLine)
		{
			VirtualStyledText emptyLineText = new VirtualStyledText("", true);
			boolean insertedEmptyLine = doc.insertText( emptyLineText);
			assertTrue(insertedEmptyLine);
		}
		
		boolean inserted = doc.insertText( fetterText);
		assertTrue(inserted);
		
		return doc;
	}
	
	private TextDocument createInsertTestDocument(String comment) throws NoOfficeException
	{
		TextDocument doc = TestHelper.getOfficeController().newDocument();
		
		VirtualStyledText testText = new VirtualStyledText(new VirtualParagraph(new VirtualTextPortionText("eingef�gter Text", false, true, false)));
		assertNotNull(testText);
		
		boolean jumped = doc.jumpToLocation( doc.getStart());
		assertTrue(jumped);
		
		boolean inserted = doc.insertText( testText);
		assertTrue(inserted);
		
		return doc;
	}
	
	
	
	// ---------------------------------------------------
	
	public static void main(String[] args)
	{
	}
}
