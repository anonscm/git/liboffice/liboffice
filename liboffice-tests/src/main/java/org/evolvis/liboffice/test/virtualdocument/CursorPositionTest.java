/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 19.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.virtualdocument;

import junit.framework.TestCase;

import org.evolvis.liboffice.descriptors.NameHintMarkerDescriptor;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.document.TextDocumentComponent;
import org.evolvis.liboffice.exceptions.NoOfficeException;
import org.evolvis.liboffice.locator.TextContentLocation;
import org.evolvis.liboffice.parser.TextDocumentParser;
import org.evolvis.liboffice.test.testhelper.TestHelper;
import org.evolvis.liboffice.virtualdocument.VirtualTextComponent;
import org.evolvis.liboffice.virtualdocument.VirtualTextDocument;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTable;
import org.evolvis.liboffice.virtualdocument.table.VirtualTextTableCell;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionMarker;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextStyle;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CursorPositionTest extends TestCase
{
	private TextDocument m_oDocument = null;
	private TextDocumentParser m_oTextDocumentParser;

	public CursorPositionTest(String name) throws NoOfficeException
	{
		super(name);
		m_oTextDocumentParser = TestHelper.getOfficeController().getControllerFactory().getMainOfficeFactory().getParserFactory().getTextDocumentParser();
	}

	protected void setUp() throws NoOfficeException
	{    
		//if (null == m_oDocument) m_oDocument = TestHelper.getOfficeController().newDocument();
	}

	protected void tearDown() throws NoOfficeException
	{   
		//if (m_oDocument != null) m_oDocument.close(false);
	}

	public void testFindCursorInParagraph() throws NoOfficeException
	{
		m_oDocument = createCursorPositionTestDocument();

		m_oDocument.jumpToLocation( m_oDocument.getStart());    
		VirtualTextDocument vDoc = m_oTextDocumentParser.parseTextDocument(m_oDocument);
		VirtualTextComponent cursorComp = vDoc.getComponentContainingCursor();    
		if (cursorComp != null)
		{
			assertTrue("Cursorposition in Start-Absatz nicht gefunden.", cursorComp.containsStyle("START"));
		}
		if (m_oDocument != null) m_oDocument.close(false);
	}

	public void testFindCursorInTableCell() throws NoOfficeException
	{
		m_oDocument = createCursorPositionTestDocument();

		TextDocumentComponent component = m_oDocument.locateTextDocumentComponent( new NameHintMarkerDescriptor("suchMich"));
		assertNotNull("Tabellenkomponente �ber Marker nicht gefunden.", component);
		TextContentLocation location = component.getTextContentLocation();    
		m_oDocument.jumpToLocation( location);    
		VirtualTextDocument vDoc = m_oTextDocumentParser.parseTextDocument(m_oDocument);
		VirtualTextComponent cursorComp = vDoc.getComponentContainingCursor();    
		if (cursorComp != null)
		{
			assertTrue("Cursorposition in Tabelle nicht gefunden.", cursorComp.containsStyle("TABLESTYLE"));      
		}
		//m_oDocument.cloneDocument( TestHelper.getOfficeController());
		if (m_oDocument != null) m_oDocument.close(false);
	}


	private TextDocument createCursorPositionTestDocument() throws NoOfficeException
	{
		TextDocument oDocument = TestHelper.getOfficeController().newDocument();
		assertNotNull(oDocument);

		oDocument.insertStyle( new VirtualTextStyle("START"));
		oDocument.insertStyle( new VirtualTextStyle("TESTSTYLE"));
		oDocument.insertStyle( new VirtualTextStyle("TABLESTYLE"));
		oDocument.insertStyle( new VirtualTextStyle("ENDE"));

		oDocument.jumpToLocation( oDocument.getEnd());

		oDocument.insertText( new VirtualStyledText("Beginn des Testdokuments.", "START", true));

		oDocument.insertText( new VirtualStyledText("Testabsatz", "TESTSTYLE", true));

		VirtualTextTable table = createVirtualTextTable(2, 2, "TABLESTYLE");
		table.setTextTableCell(1,1, new VirtualTextTableCell(new VirtualTextPortionMarker("suchMich", null)));

		oDocument.insertTable( table);

		oDocument.insertText( new VirtualStyledText("Ende des Testdokuments.", "ENDE", true));

		return oDocument;
	}

	private VirtualTextTable createVirtualTextTable(int rows, int columns, String styleName)
	{
		VirtualTextTable table = new VirtualTextTable();
		table.setSize(rows, columns);
		for(int rowIndex=0; rowIndex<rows; rowIndex++)
		{
			for(int colIndex=0; colIndex<rows; colIndex++)
			{
				VirtualTextTableCell cell = new VirtualTextTableCell("Zelle " + rowIndex + ", " + colIndex, styleName); 
				table.setTextTableCell(rowIndex, colIndex, cell);
			}      
		}

		return table;
	}
}
