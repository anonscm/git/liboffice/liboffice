/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 14.07.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.liboffice.test.virtualdocument.text;

import java.util.Iterator;

import junit.framework.TestCase;

import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;

/**
 * @author Steffen Kriese, tarent GmbH Bonn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VirtualParagraphTest extends TestCase
{
    private final static String CONST_VPORTION1         = "hallo";
    private final static String CONST_VPORTION2         = "welt";
    private final static int CONST_NUMBER_TEXT_PORTIONS = 2;
    private final static String CONST_STYLE_NAME        = "myStyle";
    
    private VirtualParagraph vPara;
    
    
    private void clearTextPortions()
    {
        Iterator it =  vPara.iterator();
        while (it.hasNext())
        {
            it.next();
            it.remove();
        }
    }
    
    protected void setUp() throws Exception
    {
        vPara = new VirtualParagraph( CONST_VPORTION1 );
    }
    
    public void testGetTextPortion()
    {
        assertEquals(((VirtualTextPortionText) vPara.getTextPortion(0)).getText(), CONST_VPORTION1);
    }
    
    public void testAddTextPortion()
    {
        vPara.addTextPortion(new VirtualTextPortionText( CONST_VPORTION2 ));
    }
    
    public void testGetNumberOfTextPortions()
    {
        clearTextPortions();
        vPara.addTextPortion(new VirtualTextPortionText(CONST_VPORTION1));
        vPara.addTextPortion(new VirtualTextPortionText(CONST_VPORTION2));
        
        assertEquals(vPara.getNumberOfTextPortions(), CONST_NUMBER_TEXT_PORTIONS);
        
    }
    
    
    public void testContainsStyle()
    {
        vPara.addTextPortion(new VirtualTextPortionText(CONST_VPORTION1, CONST_STYLE_NAME));
        assertTrue(vPara.containsStyle(CONST_STYLE_NAME));
        assertNotNull(vPara.findComponentByStyle(CONST_STYLE_NAME));
        
        vPara = new VirtualParagraph(new VirtualTextPortionText(CONST_VPORTION1, CONST_STYLE_NAME));
        assertTrue(vPara.containsStyle(CONST_STYLE_NAME));
        assertNotNull(vPara.findComponentByStyle(CONST_STYLE_NAME));
    }
    
}
