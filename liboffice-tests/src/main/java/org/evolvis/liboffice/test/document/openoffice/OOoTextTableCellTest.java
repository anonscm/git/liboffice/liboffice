/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

package org.evolvis.liboffice.test.document.openoffice;

import junit.framework.TestCase;

import org.evolvis.liboffice.document.openoffice.OOoTextTableCell;

public class OOoTextTableCellTest extends TestCase {
	public void testGetCellName() {
		// simple
		assertCellName(0, 0, "A1");
		assertCellName(1, 0, "B1");
		assertCellName(2, 0, "C1");
		assertCellName(3, 0, "D1");
		
		assertCellName(0, 1, "A2");
		assertCellName(1, 1, "B2");
		assertCellName(2, 1, "C2");
		assertCellName(3, 1, "D2");
		
		assertCellName(0, 2, "A3");
		assertCellName(1, 2, "B3");
		assertCellName(2, 2, "C3");
		assertCellName(3, 2, "D3");
		
		assertCellName(0, 3, "A4");
		assertCellName(1, 3, "B4");
		assertCellName(2, 3, "C4");
		assertCellName(3, 3, "D4");
		
		// around 26 (alphabet-end)
		assertCellName(25, 0, "Z1");
		assertCellName(26, 0, "AA1");
		assertCellName(27, 0, "AB1");
		assertCellName(28, 0, "AC1");
		
		assertCellName(0, 25, "A26");
		assertCellName(0, 26, "A27");
		assertCellName(0, 27, "A28");
		
		assertCellName(26, 25, "AA26");
		assertCellName(26, 26, "AA27");
		assertCellName(26, 27, "AA28");
	}

	private void assertCellName(int x, int y, String expected) {
		assertEquals("Wrong cell name,", expected, OOoTextTableCell.getCellName(x, y));
	}
}
