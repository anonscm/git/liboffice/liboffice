/*
 * General Document Generator,
 * Library for accessing Office-Systems and generating and
		manipulating documents
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'gdg'
 * (which makes passes at compilers) written
 * by Elmar Geese.
 */

/*
 * Created on 14.07.2005
 *
 */
package org.evolvis.liboffice.test.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Random;

import junit.framework.TestCase;

import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.document.TextDocument;
import org.evolvis.liboffice.test.testhelper.TestHelper;
import org.evolvis.liboffice.virtualdocument.text.VirtualParagraph;
import org.evolvis.liboffice.virtualdocument.text.VirtualStyledText;
import org.evolvis.liboffice.virtualdocument.text.VirtualTextPortionText;

/**
 * Some small basic tests.
 * 
 * @author Niko
 * @author Steffen
 * @author Christoph Jerolimov
 */
public class OfficeControllerBasicsTest extends TestCase {
	public void testVersion() throws Exception {
		OfficeController controller = TestHelper.getOfficeController();
		assertTrue(controller.getVersion() > 0);
	}

	/*
	public void testSupportedFileFormats() throws Exception {
		OfficeController controller = TestHelper.getOfficeController();
		assertTrue(controller.getSupportedFileFormats().size() > 0);
	}
	*/

	public void testOpenDocuments() throws Exception {
		OfficeController controller = TestHelper.getOfficeController();
		List documents = controller.getOpenDocuments();
		assertNotNull(documents);
		if (documents.isEmpty()) {
			assertEquals(0, documents.size());
		} else {
			assertTrue(documents.size() > 0);
			for (int i = 0; i < documents.size(); i++) {
				TextDocument document = (TextDocument)documents.get(i);
				assertNotNull(document);
				// do not do this.
				// openoffice 2 will close the connection when all
				// documents are closed.
//				assertTrue(document.close(true));
			}
		}
	}

	public void testNewDocument() throws Exception {
		TextDocument document = TestHelper.getOfficeController().newDocument();
		assertNotNull(document);
		assertTrue(document.close(true));
	}

	public void testSimpleDocumentActions() throws Exception {
		TextDocument document = TestHelper.getOfficeController().newDocument();
		assertNotNull(document);
		
		VirtualStyledText text = new VirtualStyledText();
		
		VirtualTextPortionText portionText = new VirtualTextPortionText();
		portionText.setText("Hallo Welt!");
		portionText.setBold(true);
		
		text.addParagraph(new VirtualParagraph(portionText));
		text.addParagraph(new VirtualParagraph(" This is just a small doctor test."));
		
		
		VirtualStyledText text2 = new VirtualStyledText();
		
		VirtualTextPortionText portionText2 = new VirtualTextPortionText();
		portionText2.setText("Hallo Welt!");
		portionText2.setBold(false);
		
		text2.addParagraph(new VirtualParagraph(portionText2));
		text2.addParagraph(new VirtualParagraph(" This is just a small doctor test."));
		
		assertTrue(document.insertText(text));
		assertTrue(document.insertPageBreak());
		assertTrue(document.insertText(text2));
		
		assertEquals("Hallo Welt! This is just a small doctor test.", text.getText());
		assertEquals("Hallo Welt! This is just a small doctor test.", text2.getText());
		
		assertTrue(document.close(true));
	}

	public void testLoadDocument() throws Exception {
		// copy file
		String filename = "TestDocToolkitTest_1" + TestHelper.getDocumentFileExtension();
		InputStream is = getClass().getResourceAsStream("/de/tarent/documents/testhelper/resources/" + filename);
		assertNotNull(is);
		
		filename = TestHelper.getTempPath() + "/" + filename;
		OutputStream os = new FileOutputStream(filename);
		int i;
		byte[] b = new byte[1024];
		while ((i = is.read(b)) > 0) {
			os.write(b, 0, i);
		}
		is.close();
		os.close();
		
		// open file
		TextDocument document = TestHelper.getOfficeController().loadDocument(filename);
		assertNotNull(document);
		assertTrue(document.close(true));
	}

	public void testSaveDocument() throws Exception {
		String filename = null;
		do {
			filename = TestHelper.getTempPath() + "/doctor-junit-test_" +
				new Random().nextLong() + TestHelper.getDocumentFileExtension();
		} while (new File(filename).exists());
		assertFalse(new File(filename).exists());
		
		TextDocument document = TestHelper.getOfficeController().newDocument();
		assertTrue(document.save(filename, TestHelper.getOfficeFileFormat(), true));
		assertTrue(new File(filename).exists());
		assertTrue(document.close(true));
		assertTrue(new File(filename).delete());
	}

	public void testPrintDocument() throws Exception {
		TextDocument document = TestHelper.getOfficeController().newDocument();
		
		String printer = document.getActivePrinterName();
		assertNotNull(printer);
		assertNotSame("", printer);
		
		// for environmental reasons inactivated
//		assertTrue(document.print(printer, 0, 0));
		
		// FIXME print with unknown printer do not return exptected value
//		assertFalse(document.print("unknown printer", 0, 0));
		
		assertTrue(document.close(true));
	}
}
